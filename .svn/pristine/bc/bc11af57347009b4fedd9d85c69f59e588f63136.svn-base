<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE History CONTROLLER FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 30/12/2014.
 * UPDATED: 30/12/2014.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use Individual\Form\SaveCon;
use Individual\Form\SaveConValidator;

class HistoryController extends \Zend\Mvc\Controller\AbstractActionController {

    protected $IndividualUserTable;
    protected $IndividualConditionTable;
    protected $_sessionObj;

    public function __construct() {
        $this->_sessionObj = new Container('frontend');
    }

    public function indexAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $IndUserConditionTableObj = $this->getServiceLocator()->get("IndividualUsersConditionTable");
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $whereArr = array("user_id" => $UserID, 'con.status' => 1);
        $conditionslist = $IndUserConditionTableObj->getUserAllConditions($whereArr);
        $finalRes = array();
        $result = array();
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $message = "";
        $this->flashMessenger()->addMessage(array('success' => $message));
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  History',
                    'custom_plugin' => $CusConPlug, // custome plugin
                    'user_Conditions' => $conditionslist
        ));
    }

    /*
      @ This action is used to add the conditons and subcontios
      @ Created:31/12/14
     */

    public function addhistoryAction() {

        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $message = "";
        $this->flashMessenger()->addMessage(array('success' => $message));
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $IndCondiTableObj = $this->getServiceLocator()->get("IndividualConditionTable");
        $IndUserConditionTableObj = $this->getServiceLocator()->get("IndividualUsersConditionTable");
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $error = "";
        $form = new SaveCon('selectedcondition');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $whereArr = array("user_id" => $UserID, 'condition_id' => $postDataArr['selected_conditions'], 'status' => 1);
            $res = $IndUserConditionTableObj->getUserConditionsList($whereArr);

            $existCountCondition = $IndUserConditionTableObj->getUserConditionsList(array("user_id" => $UserID, 'us.status' => 1));
            if (count($postDataArr['condition_list'])) {
                foreach ($postDataArr['condition_list'] as $k => $val) {
                    $conditionStatusID = $postDataArr['condtion_status'][$k];
                    $SubconditionId = $postDataArr['subcondtion'][$k];
                    $conditonArr = array('user_id' => $UserID, 'condition_id' => $val,
                        'sub_condition_id' => $SubconditionId, 'condition_status' => $conditionStatusID, 'creation_date' => round(microtime(true) * 1000),
                        'updation_date' => round(microtime(true) * 1000));
                    $IndUserConditionTableObj->SaveUserCondions($conditonArr);
                }
                $message = "Data added successfully. ";
                /*                 * ********Start Code to get connected and send notification**************** */
                $this->NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
                $FollowerUserObj = $this->getServiceLocator()->get("IndividualFollowerUsersTable");
                $whereConnected['follow.user_id'] = $UserID;
                $whereConnected['follow.status'] = 1;
                $ConnectedPeople = $FollowerUserObj->getUserFollowerDetails($whereConnected, array('follower_id'));
                if (count($ConnectedPeople)) {
                    foreach ($ConnectedPeople as $val) {
                        $type = 'condition_add';
                        $notDataArr = array("sender_id" => $UserID, "receiver_id" => $val['follower_id'], "created_date" => round(microtime(true) * 1000));
                        $saveNoti = $this->NotificationTableObj->SaveNotification($notDataArr, $type);
                    }
                }

                /*                 * ********End Code to get connected and send notification**************** */

                $this->flashMessenger()->addMessage(array('success' => $message));
            } else {
                $error = "<font color='red'>Please select condition first.</font>";
            }
        }
        $searchContent = array();
        $columns = array("id", "common_name");
        $whereArr = array("status" => 1);
        $conditionsArr = $IndCondiTableObj->getConditionsList($whereArr, $columns, "0", $searchContent);
        foreach ($conditionsArr as $Data) {
            $conditionlist[$Data['id']] = $Data['common_name'];
        }
        array_unshift($conditionlist, "Select Condtion");
        $sendArr = array('pageTitle' => 'Individual  History',
            'condition_option' => $conditionlist,
            'error' => $error, 'user_Conditions' => '', 'form' => $form);
        return new ViewModel($sendArr);
    }

    /*
      @ This action is used to edit the conditons and subcontios
      @ Created:31/12/14
     */

    public function edithistoryAction() {
        $condtionID = base64_decode($this->params('id'));
        $this->_sessionObj = new Container('frontend');
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $IndCondiTableObj = $this->getServiceLocator()->get("IndividualConditionTable");
        $IndSymtomsTableObj = $this->getServiceLocator()->get("IndividualSymtomsTable");
        $IndTreatmentTableObj = $this->getServiceLocator()->get("IndividualTreatmentTable");

        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $error = "";
        $IndUserConditionTableObj = $this->getServiceLocator()->get("IndividualUsersConditionTable");
        $IndUserOtherSymObj = $this->getServiceLocator()->get("IndividualUsersOtherSymtomsTable");
        $IndUserOtherTreatObj = $this->getServiceLocator()->get("IndividualUsersOtherTreatmentsTable");
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();

            $IndUserSymtomsTableObj = $this->getServiceLocator()->get("IndividualUsersSymtomsTable");
            $IndUserTreatmentTableObj = $this->getServiceLocator()->get("IndividualUsersTreatmentsTable");
            $conditionStatusID = $postDataArr['condtion_status'];
            $SubconditionId = $postDataArr['subcondtion'];
            $where = array('user_id' => $UserID, 'status' => 1, 'condition_id' => $condtionID);
            $data = array('sub_condition_id' => $SubconditionId, 'condition_status' => $conditionStatusID);
            $IndUserConditionTableObj->updateUserSubcondition($where, $data);
            $message = "Data Updated successfully. ";
            $this->flashMessenger()->addMessage(array('success' => $message));

            /*             * ********Start Code to get connected and send notification**************** */
            $this->NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
            $FollowerUserObj = $this->getServiceLocator()->get("IndividualFollowerUsersTable");
            $whereConnected['follow.user_id'] = $UserID;
            $whereConnected['follow.status'] = 1;
            $ConnectedPeople = $FollowerUserObj->getUserFollowerDetails($whereConnected, array('follower_id'));
            if (count($ConnectedPeople)) {
                foreach ($ConnectedPeople as $val) {
                    $type = 'condition_edit';
                    $notDataArr = array("sender_id" => $UserID, "receiver_id" => $val['follower_id'], "created_date" => round(microtime(true) * 1000));
                    $saveNoti = $this->NotificationTableObj->SaveNotification($notDataArr, $type);
                }
            }

            /*             * ********End Code to get connected and send notification**************** */
            return $this->redirect()->toRoute('indiv_history');
        }
        $searchContent = array();
        $columns = array("id", "common_name");
        $whereArr = array("status" => 1);
        $conditionsArr = $IndCondiTableObj->getConditionsList($whereArr, $columns, "0", $searchContent);
        $whereArr = array("user_id" => $UserID, 'con.status' => 1, "con.condition_id" => $condtionID);
        $conditionslist = $IndUserConditionTableObj->getUserAllConditionsForEdit($whereArr);
        $statusArr = $IndUserConditionTableObj->getConditionSymptomsTreatmentStatus();
        $finalRes = array();
        $result = array();
        if (count($conditionslist)) {
            foreach ($conditionslist as $val) {
                $result['condition_name'] = $val['common_name'];
                $result['status_id'] = $val['status_id'];
                $result['condition_id'] = $val['condition_id'];
                $result['sub_condition_id'] = $val['sub_condition_id'];
                $result['sub_condition_Arr'] = $IndCondiTableObj->getConditionsList(array("status" => 1, 'condition_id' => $val['condition_id']), $columns, "1", $searchContent);
                $finalRes[] = $result;
            }
        }
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array('pageTitle' => 'Individual  History',
                    'condition_option' => $conditionsArr,
                    'error' => $error, 'user_Conditions' => $finalRes, 'statusArr' => $statusArr));
    }

    /*
      @ Used: This method is used for condition description
      @ Created: 7/1/15
     */

    public function conditiondescriptionAction() {
        $this->_sessionObj = new Container('frontend');
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $IndUserConditionTableObj = $this->getServiceLocator()->get("IndividualUsersConditionTable");
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $condition_id = base64_decode($this->params('id'));
        $whereArr = array("user_id" => $UserID, 'con.status' => 1, 'con.condition_id' => $condition_id);
        $conditionslist = $IndUserConditionTableObj->getUserAllConditions($whereArr);
        $finalRes = array();
        $result = array();
        if (count($conditionslist)) {
            foreach ($conditionslist as $val) {
                $result['condition_name'] = $val['common_name'];
                $result['condition_id'] = $val['condition_id'];
                $result['sub_condition_name'] = $val['sub_condition_name'];
                $result['sub_condition_description'] = $val['sub_description'];
                $result['condition_description'] = $val['description'];
                $result['status'] = $val['conStatus'];
                $whereSynArr = array("user_id" => $UserID, 'condition_id' => $val['condition_id'], 'user_sym.status' => 1);
                $whereTreArr = array("user_id" => $UserID, 'condition_id' => $val['condition_id'], 'user_tre.status' => 1);
                $finalRes[] = $result;
            }
        }
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  History',
                    'custom_plugin' => $CusConPlug, // custome plugin
                    'user_Conditions' => $finalRes[0], 'condition_id' => $condition_id
        ));
    }

    /*
      @ Used: This method is used for symtoms description
      @ Created: 7/1/15
     */

    public function symtomsdescriptionAction() {
        $this->_sessionObj = new Container('frontend');
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $IndUserConditionTableObj = $this->getServiceLocator()->get("IndividualUsersConditionTable");
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $condition_id = $this->params()->fromQuery('id');
        $whereArr = array("user_id" => $UserID, 'user_sym.status' => 1, 'user_sym.condition_id' => $condition_id);
        $symtomslist = $IndUserConditionTableObj->getUserAllSymtomsAccordingCondion($whereArr);
        $finalRes = array();
        $resultS = array();
        if (count($symtomslist)) {
            foreach ($symtomslist as $val) {
                $resultS['symtom_name'] = $val['symtoms_name'];
                $resultS['symtom_description'] = $val['description'];
                $resultS['sub_symtom_description'] = $val['sub_description'];
                $resultS['sub_symtom_name'] = $val['sub_symtoms_name'];
                $finalRes[] = $resultS;
            }
        }
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  History',
                    'custom_plugin' => $CusConPlug, // custome plugin
                    'user_Symtoms' => $finalRes, 'condition_id' => $condition_id
        ));
    }

    /*
      @ Used: This method is used for treatment description
      @ Created: 7/1/15
     */

    public function treatmentdescriptionAction() {
        $this->_sessionObj = new Container('frontend');
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $IndUserConditionTableObj = $this->getServiceLocator()->get("IndividualUsersConditionTable");
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $condition_id = $this->params()->fromQuery('id');
        $whereArr = array("user_id" => $UserID, 'user_tre.status' => 1, 'user_tre.condition_id' => $condition_id);
        $treatmentlist = $IndUserConditionTableObj->getUserAllTreatmentAccordingCondion($whereArr);
        $finalRes = array();
        $finalTreamentRes = array();
        if (count($treatmentlist)) {
            foreach ($treatmentlist as $val) {
                $resultTr['treatment_name'] = $val['treatment_name'];
                $resultTr['treatment_description'] = $val['description'];
                $resultTr['sub_treatment_description'] = $val['sub_description'];
                $resultTr['sub_treatment_name'] = $val['sub_treatment_name'];
                $finalTreamentRes[] = $resultTr;
            }
        }
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  History',
                    'custom_plugin' => $CusConPlug, // custome plugin
                    'user_Treatment' => $finalTreamentRes, 'condition_id' => $condition_id
        ));
    }

}
