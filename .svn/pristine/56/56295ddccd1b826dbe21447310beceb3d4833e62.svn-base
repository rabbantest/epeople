<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE CONDITION PART OF ADMIN.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 2/3/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use Admin\Form\Study;
use Admin\Form\StudyValidator;
use Admin\Form\Demographic;

class PassController extends AbstractActionController {

    protected $_passDomObj;

    public function init() {
        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');
        $this->_sessionObj = new Container('super_admin');
        if($this->_sessionObj->admin['id']!=1){
            return $this->redirect()->toRoute('admin');
        }
    }

    /*     * *****
     * Action:- Check on ajax call for type and email existance.
     * @author: Hemant.
     * Created Date: 26-04-2015.
     * *** */

    public function checkAvailableAction() {

        $this->_passDomObj = $this->getServiceLocator()->get("UserTable");
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();

        switch ($postDataArr['type']) {
            case 'name':
                $whereCountArr = array('subdomain' => $postDataArr['subdomain'], 'status' => 1);
                if (!empty($postDataArr['PassId'])) {
                    $domCountArr = $this->_passDomObj->checkAvilableDomain($whereCountArr, $postDataArr['PassId']);
                } else {
                    $domCountArr = $this->_passDomObj->checkAvilableDomain($whereCountArr);
                }
                echo ($domCountArr) ? 'true' : 'false';
                die;
                break;
            case 'email':
                $whereCountArr = array('admin_user_name' => $postDataArr['admin_user_name'], 'status' => 1);
                if (!empty($postDataArr['PassId'])) {
                    $domCountArr = $this->_passDomObj->checkAvilableDomain($whereCountArr, $postDataArr['PassId']);
                } else {
                    $domCountArr = $this->_passDomObj->checkAvilableDomain($whereCountArr);
                }
                echo ($domCountArr) ? 'true' : 'false';
                die;
                break;
        }die;
    }

    /*     * *****
     * Action:- Change status on ajax call for pass admin.
     * @author: Hemant.
     * Created Date: 28-04-2015.
     * *** */

    function changeStatusAction() {
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable");
        if (($request->isXmlHttpRequest())) {
            $status = ($postDataArr['status'] == 0) ? 1 : 0;
            if ($this->_passDomObj->updateDomain(array('id' => $postDataArr['id']), array('status' => $status))) {
                echo json_encode(array('true', $postDataArr['id'], $status));
            } else {
                echo json_encode(array('false'));
            }
        }
        die;
    }

    /*     * *****
     * Action:- Pass admin listing with lazy loading paging.
     * @author: Hemant.
     * Created Date: 26-04-2015.
     * *** */

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable"); //CODE FOR SCROLL PAGINATION
        $columns = array('id', 'admin_user_name', 'creation_date', 'contact_number', 'subdomain', 'status');
        $where = array();
        $limit = array(0, 10);
        $page = 1;
        if (base64_decode($this->params()->fromQuery('page')) > 0) {
            $page = base64_decode($this->params()->fromQuery('page'));
            $limit = array($limit[1] * $page, $limit[1]);
            $page++;
        }
        $Pass = $this->_passDomObj->getDomain($where, $columns, "0", array(), $limit); //CODE FOR SCROLL PAGINATION
        $request = $this->getRequest();
        $sendArr = new ViewModel();

        ($request->isXmlHttpRequest()) ? $sendArr->setTerminal(true) : '';
        return $sendArr->setVariables(array('Pass' => $Pass, 'page' => $page, 'session' => $this->_sessionObj->admin));
    }

    /*     * *****
     * Action:- study detail popup.
     * @author: Hemant.
     * Created Date: 28-04-2015.
     * *** */

    function detailAction() {
        $request = $this->getRequest();
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable");
        if (($request->isXmlHttpRequest())) {
            $columns = array('*');
            $where = array('id' => $sid);
            $Pass = $this->_passDomObj->getDomain($where, $columns, "0"); //CODE FOR SCROLL PAGINATION
            echo json_encode($Pass[0]);
        }die;
    }

    /*     * *****
     * Action:- Add/Edit Study with client and server side validation.
     * @author: Hemant.
     * Created Date: 26-04-2015.
     * *** */

    function addAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $form = new \Admin\Form\Pass();
        $request = $this->getRequest();
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $Pass = array();
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable");
        //check Pass edit to prefill edit form
        if ($sid) {
            $form->setAttribute('id', 'EditPassForm');
            $form->setAttribute('action', 'admin-passDomain-add?sid=' . base64_encode($sid));
            $ColArr = array('id', 'subdomain', 'admin_user_name', 'admin_password', 'admin_name', 'logo', 'contact_number', 'panel_member', 'study_survey', 'survey_que_per_mon');
            $Pass = $this->_passDomObj->getDomain(array('id' => $sid), $ColArr, '0');
            foreach ($Pass[0] as $key => $index) {
                $form->get($key)->setValue($index);
            }
        }

        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $PassValidator = new \Admin\Form\PassValidator();
            $form->setInputFilter($PassValidator->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $postDataArr;
                //function to save file
                if (!empty($_FILES['logo']['name'])) {
                    $path = BASE_PATH . "/img/";
                    $ext = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
                    $file_name = md5(microtime());
                    $des = $path . $file_name . '.' . $ext;
                    $allowed = array('jpg', 'jpeg');
                    $src = $_FILES['logo']["tmp_name"];
                    if (in_array($ext, $allowed)) {
                        $Resize = $this->ResizeController();
                        //New Code to Resize
                        if (move_uploaded_file($src, $des)) {
                            list($width, $height, $type, $attr) = getimagesize($des);
                            // creating medium pictures
                            $smallImgSizw = $Resize->getSquireImageSize($width, $height, 200);

                            $Resize->load($des);
                            $Resize->resize('400', '100');
                            $Resize->save($des);

                            $data['logo'] = $file_name . '.' . $ext;

                            if (!empty($Pass[0]['logo'])) {
                                @unlink($path . $Pass[0]['logo']);
                            }
                        }
                        //New Code to Resize
                    }
                }
                //function to save file
                unset($data['submit']);
                if ($sid) {
                    if ($this->_passDomObj->updateDomain(array('id' => $sid), $data)) {
                        return $this->redirect()->toUrl('admin-passDomain');
                    } else {
                        $message = 'Error occurred while saving please try again.';
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                } else {
                    $data['creation_date'] = date('Y-m-d');
                    $data['admin_password'] = md5($data['admin_password']);
                    $sid = $this->_passDomObj->insertDomain($data);
                    if ($sid) {
                        return $this->redirect()->toUrl('admin-passDomain');
                    } else {
                        $message = 'Error occurred while saving please try again.';
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                }
            } else {
                $message = 'Please Provide valid data.';

                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }

        $sendArr = array(
            'form' => $form,
            'Pass' => $Pass,
            'sid' => $sid,
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

}
