<?php
/* * ***********************
 * PAGE: USE TO MANAGE THE MODULE CONFIG FOR APPLICATION.
 * #COPYRIGHT: Affle
 * @AUTHOR: Rabban Ahmad
 * CREATOR: 27/3/2015.
 * UPDATED: 27/3/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Caregiver;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Caregiver\Model\CaregiverUsersTable;
use Caregiver\Model\TempCaregiverUsersTable;
use Caregiver\Model\CareBodyMeasurementTable;
use Caregiver\Model\CareBmiTable;
use Caregiver\Model\CareCalorieTable;
use Caregiver\Model\CareCholestrolTable;
use Caregiver\Model\CareDiastolicTable;
use Caregiver\Model\CareDistanceTable;
use Caregiver\Model\CareEthnicityTable;
use Caregiver\Model\CareGlucoseTable;
use Caregiver\Model\CarePulseTable;
use Caregiver\Model\CareSleepTable;
use Caregiver\Model\CareStepsTable;
use Caregiver\Model\CareSystolicTable;
use Caregiver\Model\CareTempTable;
use Caregiver\Model\CareTimeTable;
use Caregiver\Model\CareBeaconTable;
use Caregiver\Model\CareBeaconRuleTable;
use Caregiver\Model\CareBeaconAssocTable;
use Caregiver\Model\CareBeaconDataTable;
class Module implements AutoloaderProviderInterface, ConfigProviderInterface {

 public function onBootstrap(MvcEvent $e) {
        date_default_timezone_set('UTC');
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, function($e) {
            $result = $e->getResult();
            $result->setTerminal(TRUE);
        });
            $eventManager->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($event) {
            $controller      = $event->getTarget();
            $controllerName  = get_class($controller);
            $moduleNamespace = substr($controllerName, 0, strpos($controllerName, '\\'));
            $configs         = $event->getApplication()->getServiceManager()->get('config');
            if (isset($configs['module_layouts'][$moduleNamespace])) {
                $controller->layout($configs['module_layouts'][$moduleNamespace]);
            }
        }, 100);
    
    }
	public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }
	 public function getServiceConfig() {
		 return array('factories'=>array(
				'CaregiverUsers'=>function ($serviceManager) {
					return new CaregiverUsersTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				},
				'TempCareUsers'=>function ($serviceManager) {
					return new TempCaregiverUsersTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				},
                                'CareBodyMeasurement'=>function ($serviceManager) {
					return new CareBodyMeasurementTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				},
                                'CareBmi'=>function ($serviceManager) {
					return new CareBmiTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				},
                                'CareCalorie'=>function ($serviceManager) {
					return new CareCalorieTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				}
                                ,
                                'CareCholestrol'=>function ($serviceManager) {
					return new CareCholestrolTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				}
                                ,
                                'CareDiastolic'=>function ($serviceManager) {
					return new CareDiastolicTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				}
                                ,
                                'CareDistance'=>function ($serviceManager) {
					return new CareDistanceTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				}
                                ,
                                'CareEthnicity'=>function ($serviceManager) {
					return new CareEthnicityTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				}
                                ,
                                'CareGlucose'=>function ($serviceManager) {
					return new CareGlucoseTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				}
                                ,
                                'CarePulse'=>function ($serviceManager) {
					return new CarePulseTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				}
                                ,
                                'CareSleep'=>function ($serviceManager) {
					return new CareSleepTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				}
                                ,
                                'CareSteps'=>function ($serviceManager) {
					return new CareStepsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				}
                                ,
                                'CareSystolic'=>function ($serviceManager) {
					return new CareSystolicTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				}
                                ,
                                'CareTemp'=>function ($serviceManager) {
					return new CareTempTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				},
                                'CareTime'=>function ($serviceManager) {
					return new CareTimeTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				},
                                'CareBeacon'=>function ($serviceManager) {
					return new CareBeaconTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				},
                                 'CareBeaconRule'=>function ($serviceManager) {
					return new CareBeaconRuleTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				},                                      
                                 'CareBeaconAssoc'=>function ($serviceManager) {
					return new CareBeaconAssocTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				},
                                 'CareBeaconData'=>function ($serviceManager) {
					return new CareBeaconDataTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
				}, 
			)
		);		 
	 }
	  /**
     * @param MvcEvent $e
     * @return null|\Zend\Http\PhpEnvironment\Response
     */
    public function postProcess(MvcEvent $e) {
        $routeMatch = $e->getRouteMatch();
        $formatter = $routeMatch->getParam('formatter', false);

        /** @var \Zend\Di\Di $di */
        $di = $e->getTarget()->getServiceLocator()->get('di');

        if ($formatter !== false) {
            if ($e->getResult() instanceof \Zend\View\Model\ViewModel) {
                if (is_array($e->getResult()->getVariables())) {
                    $vars = $e->getResult()->getVariables();
                } else {
                    $vars = null;
                }
            } else {
                $vars = $e->getResult();
            }

            /** @var PostProcessor\AbstractPostProcessor $postProcessor */
            $postProcessor = $di->get($formatter . '-pp', array(
                'response' => $e->getResponse(),
                'vars' => $vars,
            ));

            $postProcessor->process();

            return $postProcessor->getResponse();
        }

        return null;
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}