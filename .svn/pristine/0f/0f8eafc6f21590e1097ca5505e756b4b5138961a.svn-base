<?php
/*************************
    * PAGE: USE TO MANAGE THE STUDY USER FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Rachit Jain
    * CREATOR: 20/04/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class ServicesStudyTable extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_super_study_bucket_invitation';
    public $tablestud = 'hm_super_studies';
    public $tablepart = 'hm_selfassess_participation';
    public $tablesqo = 'hm_super_ass_study_questions_option';
    public $tableprivacy = 'hm_users_privacy_settings';
    public $tableeligable = 'hm_study_eligible_users';
    public $tableadminfilter = 'hm_super_study_main_que_filter';
    public $tablepassdomain = 'hm_pass_subdomain';
    public $onsitetrialtable = 'hm_super_onsite_study_trial';
    

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function insertbucket($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->table);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getStudy($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(),$type=0, $lowerLimit='', $maxLimit='') {

        try {
			$columns = array("study_id","type","eligible_for_main","is_accepted");
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud' => $this->table
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }
			$select->where->notIn("bucket", array("-2"));
            $select->join(array("stli"=>$this->tablestud), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=stli.study_id and stli.study_status=1 and stli.publish_status=1"), array("study_id","created_by","researcher_name","study_title","study_description","start_date","end_date","creation_date"), "LEFT");
            $select->join(array("pass"=>$this->tablepassdomain), new \Zend\Db\Sql\Predicate\Expression("stli.created_by=pass.id"), array("subdomain"), "LEFT");

            $select->join(array("part"=>$this->tablepart), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=part.study_id and stud.user_id=part.UserID and part.study_type=$type"), array("part_status"), "LEFT");
          
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchcontent) > 0) {
                
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
			if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}
			//echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAllStudiesPoints($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'pari_stu' => 'hm_selfassess_participation'
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }
            $select->join(array('stu' => 'hm_super_studies'), "stu.study_id = pari_stu.study_id", array(), 'LEFT');

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            if ($data && $data[0]['points'] != '') {
                $data = $data[0];
            } else {
                $data = array('points' => 0);
            }
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*****
     * This function is used to get the redeem points
     * **********/
    
     public function getUserAllRedeemPoints($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'points_history' => 'hm_users_redeem_history'
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }
            //$select->join(array('stu' => 'hm_super_studies'), "stu.study_id = pari_stu.study_id", array(), 'LEFT');

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            if ($data && $data[0]['Redeem_points'] != '') {
                $data = $data[0];
            } else {
                $data = array('Redeem_points' => 0);
            }
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */
    public function getSupeStudyOnly($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud' => $this->tablestud
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //  echo $select->getSqlString();exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function isStudy($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'studls' => $this->tablestud
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //   echo $select->getSqlString();exit;            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateBucket($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            //   echo $update->getSqlString();exit; 
            //echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            if ($result) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * @Action check filteration for main
     * *** */

    public function filterForMain($where = array(), $columns = array(), $lookingfor = false, $wherrFil = false, $count = NULL) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'elmain' => $this->tableeligable
            ));

            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()) . $wherrFil);
            //echo $query;exit;
            $statement = $this->adapter->query($query);
            if ($count == 1) {
                $user = $this->resultSetPrototype->initialize($statement->execute())->count();
            } else {
                $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            }
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * @Action check filteration for main set by admin
     * *** */

    public function filterForMainByAdmin($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'elmainbyad' => $this->tableadminfilter
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getStudytrials($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0, $notEqual = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud' => $this->tablestud
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchcontent) > 0) {
                
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //  echo $select->getSqlString();
            //  exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getStudytrialsbuck($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0, $notEqual = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'studbuc' => $this->table
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchcontent) > 0) {
                
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //  echo $select->getSqlString();
            //  exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getStudylist($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0, $lowerLimit='', $maxLimit='') {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud' => $this->table
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            $select->join(array("stli" => $this->tablestud), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=stli.study_id and stli.study_status=1 and stli.publish_status=1"), array("*"), "LEFT");

            $select->join(array("part" => $this->tablepart), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=part.study_id and stud.user_id=part.UserID and part.study_type=$type"), array("last_page", "part_status", "study_type", "part_startdate", "part_enddate"), "LEFT");

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchcontent) > 0) {
                
            }
            if ($lookingfor) {
                //   $select->order($lookingfor);
            }
            if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}
			//echo $select->getSqlString();exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getStudyInvitationSet($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud_invi' => 'hm_super_study_invitation_set'
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->join(array('stu' => 'hm_super_studies'), "stu.study_id = stud_invi.invitation_set_study", array('screening'), 'lEFT');

            //echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * **This function is used to in**** */

    public function DeleteStudyInvitaion($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from('hm_super_study_invitation_set');
            if (count($where) > 0) {
                $delete->where($where);
            }
            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * **This function is used to in**** */

    public function insertStudyInvitaion($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert('hm_super_study_bucket_invitation');
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /* public function getStudytrials($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0, $notEqual = false) {

      try {
      $sql = new Sql($this->getAdapter());
      $select = $sql->select()->from(array(
      'stud' => $this->table
      ));
      if (is_array($where) && count($where) > 0) {
      $select->where($where);
      }
      if ($notEqual) {
      $select->where->nest()->notEqualTo("stud.user_id", $notEqual)
      ->or->nest()->equalTo("stud.user_id", $notEqual)
      ->equalTo("bucket", "-2");
      }


      $select->join(array("stli" => $this->tablestud), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=stli.study_id and stli.study_status=1 and stli.publish_status=1"), array("*"), "LEFT");
      $select->join(array("part" => $this->tablepart), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=part.study_id and stud.user_id=part.UserID and part.study_type=$type"), array("last_page", "part_status", "study_type", "part_startdate", "part_enddate"), "LEFT");
      if (count($columns) > 0) {
      $select->columns($columns);
      }
      if (count($searchcontent) > 0) {

      }
      if ($lookingfor) {
      $select->order($lookingfor);
      }
      // echo $select->getSqlString();exit;

      $statement = $sql->prepareStatementForSqlObject($select);
      $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
      return $data;
      } catch (\Exception $e) {
      throw new \Exception($e->getPrevious()->getMessage());
      }
      } */


	public function getStudyResearcher($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $lowerLimit='', $maxLimit='') {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'part' => $this->tablepart
            ));
			if (is_array($where) && count($where) > 0) {         
                 $select->where($where);
            }

            $select->join(array("stud"=>$this->tablestud), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=part.study_id and stud.created_by != 1"), array('pass_researcher_id'), "INNER");
            
            $select->join(array("pass"=>$this->tablepassdomain), new \Zend\Db\Sql\Predicate\Expression("stud.created_by=pass.id"), array("subdomain"), "LEFT");
             
            $user=array('FirstName','LastName','UserName','Image','Email','Dob');
			$select->join(array('us' =>'hm_users'), "us.UserID = stud.pass_researcher_id", $user, 'INNER');
            $select->join(array('hm_con' =>'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'INNER');
			$select->join(array('states' =>'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'INNER');
			$select->join(array('country' =>'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'INNER');

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if ($lookingfor) {
				$select->order($lookingfor);	
            }
			if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}
			
			$select->group('stud.pass_researcher_id');
			//echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    public function getOnsiteStudyList($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $lowerLimit='', $maxLimit='') {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'trial' => $this->onsitetrialtable
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->group('category');
            }
			if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            $select->order("category asc");
            //echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
}
