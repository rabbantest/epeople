<?php
/* * ***********************
 * PAGE: USE TO MANAGE THE MODULE CONFIG FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rabban Ahmad
 * CREATOR: 12/11/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */
return array(
	'router'=>array(
		'routes'=>array(
			'home'=>array(
				'type'=>'Zend\Mvc\Router\Http\Literal',
				'options'=>array(
					'route' => '/',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'index',
                    ),
				)
			),
			 // using the path /application/:controller/:action
            'caregiver' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/caregiver',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Caregiver\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
			'cg-login' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/cg-login',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action'     => 'login',
                    ),
                ),
            ),
			/* For home or dashboard page for caregiver*/
			'cg_dash' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/cg-dashboard',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action'     => 'dashboard',
                    ),
                ),
            ),
			/* For dashboard page for pending caregiver request */
			'cg_dash_pending' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/cg-pending-request',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action'     => 'pendingrequest',
                    ),
                ),
            ),
			/* For dashboard page for pending caregiver request */
			'cg_notregis-pending' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/cg-notregis-pending',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action'     => 'requestfornotregistered',
                    ),
                ),
            ),
		)
	),
	'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
	'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
	'controllers' => array(
        'invokables' => array(
            'Caregiver\Controller\Index' => 'Caregiver\Controller\IndexController',
            'Caregiver\Controller\CaregiverAjax' => 'Caregiver\Controller\CaregiverAjaxController',
        ),
    ),
	'view_helpers' => array(
        'invokables' => array(
            'displayDate' => 'Caregiver\View\Helper\DisplayDateHelper',
        )
    ),
	'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'header/caregiverheader' => __DIR__ . '/../view/header/caregiverheader.phtml',
            'left-menu/caregiver-left-menu' => __DIR__ . '/../view/left-menus/caregiver-left-menu.phtml',
            'paginator/paginators' => __DIR__ . '/../view/caregiver/paginator/paginators.phtml',
            'layout' => __DIR__ . '/../view/layout/caregiverlayout.phtml',
            'footer/caregiverfooter' => __DIR__ . '/../view/footer/caregiverfooter.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
	// Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
?>