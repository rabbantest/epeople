<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman
 * CREATOR: 10/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class ServicesUsersConditionTable extends AbstractTableGateway{
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_users_conditions';
    public $AdminConditiontable = 'hm_super_conditions';
    public $AdminSymtomstable = 'hm_super_symptoms';
    public $AdminSubSymtable = 'hm_super_subsymptoms';
    public $subTable = 'hm_super_subconditions';
    public $UserSymtomsTable = 'hm_users_symtoms';
    public $AdminTreatTable = 'hm_super_treatments';
    public $AdminSubTreatTable = 'hm_super_subtreatments';
    public $UserTreatmentTable = 'hm_users_treatments';
    public $ConSympTreStatus='hm_conditions_status';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function SaveUserCondions($data = array()) {
        try {

            $insert = $this->insert($data);
            if ($insert) {
                $lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                return $lastId;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*
      @ : This function is used to update the user subcondion
      @ : Created: 6/1/15
     */

    public function updateUserSubcondition($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            //echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
        } catch (\Exception $e) {
            echo $e->getPrevious()->getMessage();
            die;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUserConditionsList($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                //$select->join(array('sb' => $this->subTable), "us.id = sb.condition_id", $subcondition, 'LEFT');
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            //$select->order('common_name ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /**
      @ This method is used to get the user condtions for view condtion page
      @ Created:2/1/15
     * */
    public function getUserAllConditions($where = array(), $columns = array(), $lowerLimit='', $maxLimit='') {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'con' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->join(array('adminCon' => $this->AdminConditiontable), "con.condition_id = adminCon.id", array('common_name', 'description'), 'INNER');
            $select->join(array('adminSubCon' =>$this->subTable), "con.sub_condition_id = adminSubCon.sub_id", array('sub_condition_name' => 'sub_common_name', 'sub_description'), 'LEFT');
            $select->join(array('conStatus' =>$this->ConSympTreStatus), "con.condition_status = conStatus.id", array('conStatus' => 'status'), 'LEFT');
			if (count($where) > 0) {
                $select->where($where);
            }

            //$select->order('common_name ASC');
            if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}
            $statement = $sql->prepareStatementForSqlObject($select);
            //$statement->prepare();
            //echo $statement->getSql();exit;
            $Ucondtions = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            /* echo "<pre>";
              print_r($Ucondtions);die; */
            return $Ucondtions;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	
	/**
      @ This method is used to get the user condtions for view condtion page
      @ Created:2/1/15
     * */
    public function getUserAllConditionsForEdit($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'con' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->join(array('adminCon' => $this->AdminConditiontable), "con.condition_id = adminCon.id", array('common_name',), 'INNER');
            $select->join(array('adminSubCon' =>$this->subTable), "con.sub_condition_id = adminSubCon.sub_id", array('sub_condition_name' => 'sub_common_name'), 'LEFT');
            $select->join(array('conStatus' =>$this->ConSympTreStatus), "con.condition_status = conStatus.id", array('conStatus' => 'status','status_id'=>'id'), 'LEFT');
			if (count($where) > 0) {
                $select->where($where);
            }

            //$select->order('common_name ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            //$statement->prepare();
            //echo $statement->getSql();exit;
            $Ucondtions = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            /* echo "<pre>";
              print_r($Ucondtions);die; */
            return $Ucondtions;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

	
	/**
      @ This method is used to get the user condtions/sympotms/treatment status
      @ Created:4/3/15
     * */
    public function getConditionSymptomsTreatmentStatus() {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'con' => 'hm_conditions_status'
            ));

            //$select->order('common_name ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            //$statement->prepare();
            //echo $statement->getSql();exit;
            $Ucondtions = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $Ucondtions;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /**
      @ This method is used to get the user condtions IDs
      @ Created:2/1/15
     * */
    public function getUserAllConditionsIDs($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'con' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            //$select->join(array('adminCon' => $this->AdminConditiontable), "con.condition_id = adminCon.id", array('common_name','description'), 'INNER');
            if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            $Ucondtions = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $Ucondtions;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /**
      @ This method is used to get the similar condition user
      @ Created:2/1/15
     * */
    public function getAllSimilarConditionUser($where = array(), $notEqual = array(), $columns = array(), $searchContent = array(),$nameSearch='', $age_from='', $age_to='', $lowerLimit='',$maxLimit='') {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'con' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $user = array('UserID','FirstName', 'LastName', 'UserName', 'Image', 'Email', 'Dob');

            $select->join(array('u' => 'hm_users'), "con.user_id = u.UserID", $user, 'INNER');
            $select->group('u.UserID');
            $select->join(array('hm_con' => 'hm_contacts_info'), "hm_con.user_id = u.UserID", array(), 'INNER');

            $select->join(array('states' => 'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'INNER');
            $select->join(array('country' => 'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'INNER');

            if (count($where) > 0) {
                $select->where($where);
            }

			if (!empty($age_from) && !empty($age_to)) {
				$select ->where->greaterThanOrEqualTo('Age', $age_from);
				$select ->where->lessThanOrEqualTo('Age', $age_to);		
            }

            if (count($notEqual)) {
                foreach ($notEqual as $k => $v) {
                    $select->where->NotequalTo('con.user_id', $v);
                }
            }
			if($nameSearch){
				$select->where->nest->like('u.FirstName', '%' . $nameSearch . '%')
                    ->or->like('u.MiddleName', '%' . $nameSearch . '%')
                    ->or->like('u.LastName', '%' . $nameSearch . '%');
			}
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
						$select->where($likeWhr);
                    }
                }
            }

			if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}
			
			//echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $statement = $sql->prepareStatementForSqlObject($select);
            $Ucondtions = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $Ucondtions;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /**
      @ This method is used to get the user condtions for view condtion page
      @ Created:2/1/15
     * */
    public function getUserAllSymtomsAccordingCondion($where = array(), $columns = array(), $lowerLimit='', $maxLimit='') {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'user_sym' => $this->UserSymtomsTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->join(array('adminSym' => $this->AdminSymtomstable), "user_sym.symtoms_id = adminSym.id", array('symtoms_name' => 'common_name', 'description'), 'INNER');
            $select->join(array('adminSubSym' => 'hm_super_subsymptoms'), "user_sym.sub_symtoms_id = adminSubSym.sub_id", array('sub_symtoms_name' => 'sub_common_name', 'sub_description'), 'LEFT');
            /* if (count($searchContent) > 0) {
              $likeWhr = new \Zend\Db\Sql\Where();
              foreach ($searchContent AS $fields=>$data) {
              if (!empty($data)) {
              $likeWhr->addPredicate(
              new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
              );
              $select->where($likeWhr);
              }
              }
              } */

            if (count($where) > 0) {
                $select->where($where);
            }
			
			if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}

            //$select->order('common_name ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            //$statement->prepare();
            //echo $statement->getSql();exit;
            $Ucondtions = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            /* echo "<pre>";
              print_r($Ucondtions);die; */
            return $Ucondtions;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /**
      @ This method is used to get the user condtions for view condtion page
      @ Created:2/1/15
     * */
    public function getUserAllTreatmentAccordingCondion($where = array(), $columns = array(), $lowerLimit='', $maxLimit='') {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'user_tre' => $this->UserTreatmentTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->join(array('adminTre' => $this->AdminTreatTable), "user_tre.treatment_id = adminTre.id", array('treatment_name' => 'common_name', 'description'), 'INNER');
            $select->join(array('adminSubTre' => $this->AdminSubTreatTable), "user_tre.sub_treatment_id = adminSubTre.sub_id", array('sub_treatment_name' => 'sub_common_name', 'sub_description'), 'LEFT');
            /* if (count($searchContent) > 0) {
              $likeWhr = new \Zend\Db\Sql\Where();
              foreach ($searchContent AS $fields=>$data) {
              if (!empty($data)) {
              $likeWhr->addPredicate(
              new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
              );
              $select->where($likeWhr);
              }
              }
              } */

            if (count($where) > 0) {
                $select->where($where);
            }

			if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}
            //$select->order('common_name ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            //$statement->prepare();
            //echo $statement->getSql();exit;
            $Ucondtions = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            /* echo "<pre>";
              print_r($Ucondtions);die; */
            return $Ucondtions;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	/*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    
    public function deleteUserCondtion($where = array()) {
        try {
				$this->delete($where); 
				return true;
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }

}
