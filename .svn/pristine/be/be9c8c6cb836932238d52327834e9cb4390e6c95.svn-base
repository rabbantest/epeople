<?php
namespace Caregiver;
/* * ***********************
 * PAGE: USE TO MANAGE THE MODULE CONFIG FOR APPLICATION.
 * #COPYRIGHT: Affle
 * @AUTHOR: Rabban Ahmad
 * CREATOR: 27/3/2015.
 * UPDATED: 27/3/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Caregiver\Model\CaregiverUsersTable;
use Caregiver\Model\TempCaregiverUsersTable;
use Caregiver\Model\CareBodyMeasurementTable;
use Caregiver\Model\CareBmiTable;
use Caregiver\Model\CareCalorieTable;
use Caregiver\Model\CareCholestrolTable;
use Caregiver\Model\CareDiastolicTable;
use Caregiver\Model\CareDistanceTable;
use Caregiver\Model\CareEthnicityTable;
use Caregiver\Model\CareGlucoseTable;
use Caregiver\Model\CarePulseTable;
use Caregiver\Model\CareSleepTable;
use Caregiver\Model\CareStepsTable;
use Caregiver\Model\CareSystolicTable;
use Caregiver\Model\CareTempTable;
use Caregiver\Model\CareTimeTable;
use Caregiver\Model\CareBeaconTable;
use Caregiver\Model\CareBeaconRuleTable;
use Caregiver\Model\CareBeaconAssocTable;
use Caregiver\Model\CareBeaconDataTable;
use Caregiver\Model\CareGeofenceTable;
use Caregiver\Model\Usersr;
use Caregiver\Model\UsersrTable;
use Caregiver\Model\CareSubscriptionTable;
use Zend\Session\Container;
use Individual\Controller\Plugin\CustomControllerPlugin;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        date_default_timezone_set('UTC');
        $eventManager = $e->getApplication()->getEventManager();
        $ServiceManager = $e->getApplication()->getServiceManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, function($e) {
            $result = $e->getResult();
            $result->setTerminal(TRUE);
        });
       
        //$eventManager->attach('dispatch', array($this, 'rabban'), 100);      
        $eventManager->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($event) {
            $controller = $event->getTarget();
            $controllerName = get_class($controller);
            $moduleNamespace = substr($controllerName, 0, strpos($controllerName, '\\'));
            $sessionObj = new Container('Caregiver');
            $siteURL = $_SERVER['HTTP_HOST'];
            $siteArray = explode('.', $siteURL);
            if (strtolower($siteArray[0]) == 'www') {
                $siteDomain = $siteArray[1];
            } else {
                $siteDomain = $siteArray[0];
            }
            $explodurl = explode("/", $_SERVER["REQUEST_URI"]);
            $basename = $explodurl[1];
            $ValidDomain = $this->GetValidateDomain($event->getApplication()->getServiceManager());
            //echo $ValidDomain;die;
            if (($sessionObj->isCaregiverlogin && ($siteDomain != $sessionObj->domain)) || ($basename != 'cg-getprofileinfo' && $basename != 'cg-login' && !$sessionObj->isCaregiverlogin && $moduleNamespace == 'Caregiver') || !$ValidDomain) {
                $sessionObj->getManager()->getStorage()->clear('Caregiver');
                $url = $event->getRouter()->assemble(array(), array('name' => 'login'));
                $response = $event->getResponse();
                if($ValidDomain){
                $response->getHeaders()->addHeaderLine('Location', '/cg-login');
                }else{
                  $response->getHeaders()->addHeaderLine('Location', 'http://quantextualhealth.com/');  
                }
                $response->setStatusCode(302);
                $response->sendHeaders();

                // When an MvcEvent Listener returns a Response object,
                // It automatically short-circuit the Application running 
                // -> true only for Route Event propagation see Zend\Mvc\Application::run
                // To avoid additional processing
                // we can attach a listener for Event Route with a high priority
                $stopCallBack = function($event) use ($response) {
                    $event->stopPropagation();
                    return $response;
                };
                //Attach the "break" as a listener with a high priority
                $e->getApplication()->getEventManager()->attach(MvcEvent::EVENT_ROUTE, $stopCallBack, -10000);
                return $response;
            }
            $configs = $event->getApplication()->getServiceManager()->get('config');
            if (isset($configs['module_layouts'][$moduleNamespace])) {
                $controller->layout($configs['module_layouts'][$moduleNamespace]);
            }
        }, 100);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function GetValidateDomain($Sevice_Locator) {
        $newClass = new \Individual\Controller\Plugin\CustomControllerPlugin();
        $isDomainValid = $newClass->ValidateDomainIdentity($Sevice_Locator);
        return $isDomainValid;
    }

    public function getServiceConfig() {
        return array('factories' => array(
                'CaregiverUsers' => function ($serviceManager) {
            return new CaregiverUsersTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'TempCareUsers' => function ($serviceManager) {
            return new TempCaregiverUsersTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'CareBodyMeasurement' => function ($serviceManager) {
            return new CareBodyMeasurementTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'CareBmi' => function ($serviceManager) {
            return new CareBmiTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'CareCalorie' => function ($serviceManager) {
            return new CareCalorieTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        }
                ,
                'CareCholestrol' => function ($serviceManager) {
            return new CareCholestrolTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        }
                ,
                'CareDiastolic' => function ($serviceManager) {
            return new CareDiastolicTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        }
                ,
                'CareDistance' => function ($serviceManager) {
            return new CareDistanceTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        }
                ,
                'CareEthnicity' => function ($serviceManager) {
            return new CareEthnicityTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        }
                ,
                'CareGlucose' => function ($serviceManager) {
            return new CareGlucoseTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        }
                ,
                'CarePulse' => function ($serviceManager) {
            return new CarePulseTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        }
                ,
                'CareSleep' => function ($serviceManager) {
            return new CareSleepTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        }
                ,
                'CareSteps' => function ($serviceManager) {
            return new CareStepsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        }
                ,
                'CareSystolic' => function ($serviceManager) {
            return new CareSystolicTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        }
                ,
                'CareTemp' => function ($serviceManager) {
            return new CareTempTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'CareTime' => function ($serviceManager) {
            return new CareTimeTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'CareBeacon' => function ($serviceManager) {
            return new CareBeaconTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'CareBeaconRule' => function ($serviceManager) {
            return new CareBeaconRuleTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'CareBeaconAssoc' => function ($serviceManager) {
            return new CareBeaconAssocTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'CareBeaconData' => function ($serviceManager) {
            return new CareBeaconDataTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'CareGeofence' => function ($serviceManager) {
            return new CareGeofenceTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'CareSubscriptionObj' => function ($serviceManager) {
            return new CareSubscriptionTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'Caregiver\Model\UsersrTable' => function($sm) {
            $tableGateway = $sm->get('UsersrTableGateway');
            $table = new Usersr($tableGateway);
            return $table;
        },
                'UsersrTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Usersr());
            return new TableGateway('hm_caregivers', $dbAdapter, null, $resultSetPrototype);
        },
            )
        );
    }

    /**
     * @param MvcEvent $e
     * @return null|\Zend\Http\PhpEnvironment\Response
     */
    public function postProcess(MvcEvent $e) {
        $routeMatch = $e->getRouteMatch();
        $formatter = $routeMatch->getParam('formatter', false);

        /** @var \Zend\Di\Di $di */
        $di = $e->getTarget()->getServiceLocator()->get('di');

        if ($formatter !== false) {
            if ($e->getResult() instanceof \Zend\View\Model\ViewModel) {
                if (is_array($e->getResult()->getVariables())) {
                    $vars = $e->getResult()->getVariables();
                } else {
                    $vars = null;
                }
            } else {
                $vars = $e->getResult();
            }

            /** @var PostProcessor\AbstractPostProcessor $postProcessor */
            $postProcessor = $di->get($formatter . '-pp', array(
                'response' => $e->getResponse(),
                'vars' => $vars,
            ));

            $postProcessor->process();

            return $postProcessor->getResponse();
        }
//        $plugin = new \Individual\Controller\Plugin\CustomControllerPlugin();
//        $plugin->ValidateDomainIdentity();die;
        return null;
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}
