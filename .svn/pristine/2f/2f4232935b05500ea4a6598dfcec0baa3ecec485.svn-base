<?php
/*************************
    * PAGE: USE TO MANAGE THE STUDY USER FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Rachit Jain
    * CREATOR: 20/04/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class ServicesStudyTable extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_super_study_bucket_invitation';
    public $tablestud = 'hm_super_studies';
    public $tablepart = 'hm_selfassess_participation';
    public $tablesqo = 'hm_super_ass_study_questions_option';
    public $tableprivacy = 'hm_users_privacy_settings';
    public $tableeligable = 'study_eligible_users';
    public $tableadminfilter = 'hm_super_study_main_que_filter';
    public $tablepassdomain = 'hm_pass_subdomain';
    

    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }


    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getStudy($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(),$type=0, $lowerLimit='', $maxLimit='') {

        try {
			$columns = array("study_id","type","eligible_for_main","is_accepted");
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud' => $this->table
            ));
			if (is_array($where) && count($where) > 0) {         
                 $select->where($where);
            }
            $current_date = date("Y-m-d");

            $select->join(array("stli"=>$this->tablestud), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=stli.study_id and stli.study_status=1 and stli.publish_status=1"), array("study_id","created_by","researcher_name","study_title","study_description","start_date","end_date","creation_date"), "LEFT");
            
            $select->join(array("pass"=>$this->tablepassdomain), new \Zend\Db\Sql\Predicate\Expression("stli.created_by=pass.id"), array("subdomain"), "LEFT");

            $select->join(array("part"=>$this->tablepart), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=part.study_id and stud.user_id=part.UserID and part.study_type=$type"), array("part_status"), "LEFT");
          
            if (count($columns) > 0) {
                $select->columns($columns);
            }
             if (count($searchcontent) > 0) {
//                if (isset($searchcontent['keyword']) && !empty($searchcontent['keyword'])) {
//                    $select->where->nest->like('title', '%' . $searchcontent['keyword'] . '%')
//                    ->or->like('category', '%' . $searchcontent['keyword'] . '%');
//                }
//                if (isset($searchcontent['cat']) && !empty($searchcontent['cat'])) {
//                    $select->where->nest->equalTo("category", $searchcontent['cat']);
//                }
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
			if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getSupeStudyOnly($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud' => $this->tablestud  
            ));
              if (is_array($where) && count($where) > 0) {         
                 $select->where($where);
            }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
         //  echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

      public function isStudy($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'studls' => $this->tablestud
            ));
              if (is_array($where) && count($where) > 0) {         
                 $select->where($where);
            }
        
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
       //   echo $select->getSqlString();exit;            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
  public function updateBucket($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
          //   echo $update->getSqlString();exit; 
            //echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute(); 
            if($result){
                return true;
            }  else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    } 
  
      /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
       * @Action check filteration for main
    *****/
    public function filterForMain($where = array(), $columns = array(), $lookingfor = false,  $wherrFil= false, $count= NULL) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'elmain' => $this->tableeligable  
            ));
           
              if (is_array($where) && count($where) > 0) {         
                $select->where($where);
            }

             $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()) . $wherrFil);
           //echo $query;exit;
            $statement = $this->adapter->query($query);
             if ($count == 1) {
                $user = $this->resultSetPrototype->initialize($statement->execute())->count();
            } else {
                $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            }
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
      /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
       * @Action check filteration for main set by admin
    *****/
    public function filterForMainByAdmin($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'elmainbyad' => $this->tableadminfilter  
            ));
              if (is_array($where) && count($where) > 0) {         
                 $select->where($where);
            }
           
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
       
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

          
    
    
}
