<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman
 * CREATOR: 10/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
//use Zend\Mvc\Controller\AbstractActionController;
use Individual\Controller\Plugin;

class IndividualPhysicalActivityTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_physical_acitivity';
    public $pat = 'hm_physical_activity_type';
    public $intensity = 'hm_intensity_type';
    public $activity = 'hm_moves_activity';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getPhysicalActivityRecord($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'pa' => $this->table
            ));

            if (count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'ExerciseRecorddate') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("ExerciseRecorddate <= 'DATE_SUB(CURDATE(),$value)'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
//            $statement->prepare();
//           echo $statement->getSql();exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getintensityTypeRecord($where = array(), $columns = array(), $lookingfor = false) {

        try {

            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'intens' => $this->intensity
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }

            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            //print_r($user);
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getPhysicalActivityTypeRecord($where = array(), $columns = array(), $lookingfor = false) {
        // echo 'suman';exit;
        try {

            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'pat' => $this->pat
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }

            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            //print_r($user);
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function insertPhysicalActivity($data = array()) {
        try {

            $insert = $this->insert($data);
            if ($insert) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*
      @ : This function is used to update the physical activity
      @ : Created: 6/1/15
     */

    public function updatePhysicalActivity($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }

            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
        } catch (\Exception $e) {
            echo $e->getPrevious()->getMessage();
            die;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function deletePhysicalActivity($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->table);

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getPhyAcOneYearMontlyData($where = array(), $lookingfor = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'pa' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'ExerciseRecorddate') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("ExerciseRecorddate <= 'DATE_SUB(CURDATE(),$value)'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }


            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('monthname(ExerciseRecorddate) As Month'),
                        new \Zend\Db\Sql\Expression('year(ExerciseRecorddate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(Duration) As AvgDuration'),
                        new \Zend\Db\Sql\Expression('avg(Distance) As AvgDistance'),
                        new \Zend\Db\Sql\Expression('avg(NumberOfSteps) As AvgSteps'),
                        new \Zend\Db\Sql\Expression('avg(CaloriesBurned) As AvgCB'),
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $select->group(
                    array(
                        new \Zend\Db\Sql\Expression('monthname(ExerciseRecorddate)'),
                        new \Zend\Db\Sql\Expression('year(ExerciseRecorddate)'))
            );

            $statement = $sql->prepareStatementForSqlObject($select);
//     $statement->prepare();
//            echo $statement->getSql();exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();

            return $data;
        } catch (\Exception $e) {
            
        }
    }

    /*
     * get record by year, month, week
     */

    public function getPhyAcRecordByYear($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cho' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'start_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("ExerciseRecorddate >= '$value'"));
                    } elseif ($key == 'end_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("ExerciseRecorddate <= '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(ExerciseRecorddate) As Day'),
                        new \Zend\Db\Sql\Expression('monthname(ExerciseRecorddate) As Month'),
                        new \Zend\Db\Sql\Expression('year(ExerciseRecorddate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(Duration) As AvgDuration'),
                        new \Zend\Db\Sql\Expression('avg(Distance) As AvgDistance'),
                        new \Zend\Db\Sql\Expression('avg(NumberOfSteps) As AvgSteps'),
                        new \Zend\Db\Sql\Expression('avg(CaloriesBurned) As AvgCB'),
                        new \Zend\Db\Sql\Expression('avg(NumberOfSteps) As TotalStep'),
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*
     * get record by year, month, week
     */

    public function getPhyAcRecordByYearSteps($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cho' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'start_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("ExerciseRecorddate >= '$value'"));
                    } elseif ($key == 'end_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("ExerciseRecorddate <= '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(ExerciseRecorddate) As Day'),
                        new \Zend\Db\Sql\Expression('monthname(ExerciseRecorddate) As Month'),
                        new \Zend\Db\Sql\Expression('year(ExerciseRecorddate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(Duration) As AvgDuration'),
                        new \Zend\Db\Sql\Expression('avg(Distance) As AvgDistance'),
                        new \Zend\Db\Sql\Expression('avg(NumberOfSteps) As AvgSteps'),
                        new \Zend\Db\Sql\Expression('avg(CaloriesBurned) As AvgCB'),
                        new \Zend\Db\Sql\Expression('SUM(NumberOfSteps) As TotalStep'),
                    )
            );
            //  echo  $select->getSqlString();exit;
            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function gettotalstepsData($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'pa' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('SUM(NumberOfSteps) As TotalStep'),
                    )
            );

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            
        }
    }

    public function getPhysicalRecords($where = array(), $columns = array(), $lookingfor = false) {        

        try {
         
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'phy' => $this->table
            ));

            if (count($where) > 0) {
                 // $select->where($where);
                  foreach ($where as $key => $value) {
                    if ($key == 'startDate') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("CONCAT(ExerciseRecorddate, ' ', ExerciseRecordtime) >= '$value'"));
                    } elseif ($key == 'endDate') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("CONCAT(ExerciseRecorddate, ' ', ExerciseRecordtime) <= '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
//echo $select->getSqlString();exit;
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*
     * @Author: shivendr Suman
     * @get moves physical activity
     */

    public function getActivityColor($where = array(), $columns = array(), $lookingfor = false) {

        try {

            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'phy' => $this->activity
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*
     * get record of one day
     */

    public function getPhyAcRecordOfDay($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cho' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'current_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("ExerciseRecorddate = '$value'"));
                    } elseif ($key == 'start_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("ExerciseRecordtime >= '$value'"));
                    } elseif ($key == 'end_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("ExerciseRecordtime < '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(ExerciseRecorddate) As Day'),
                        new \Zend\Db\Sql\Expression('monthname(ExerciseRecorddate) As Month'),
                        new \Zend\Db\Sql\Expression('year(ExerciseRecorddate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(Duration) As AvgDuration'),
                        new \Zend\Db\Sql\Expression('avg(Distance) As AvgDistance'),
                        new \Zend\Db\Sql\Expression('avg(NumberOfSteps) As AvgSteps'),
                        new \Zend\Db\Sql\Expression('avg(CaloriesBurned) As AvgCB'),
                        new \Zend\Db\Sql\Expression('SUM(NumberOfSteps) As TotalStep'),
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $sql->getSqlstringForSqlObject($select);exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
