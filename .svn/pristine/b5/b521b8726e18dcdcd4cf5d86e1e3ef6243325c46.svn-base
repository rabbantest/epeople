<?php

/* * ***********************
 * PAGE: USE TO MANAGE User follower relation
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rabban Ahmad
 * CREATOR: 22/1/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class ServicesNotificationTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_notifications';
    public $type_table = 'hm_notification_type';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * *** */

    public function SaveNotification($data = array(), $type) {
        try {
            $typeArr = array(
                'follow_request' => 1,
                'follow_accept' => 2
            );
            $not_type = $typeArr[$type];
            $data['type'] = $not_type;

            $insert = $this->insert($data);
            if ($insert) {
                $lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                return $lastId;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*
      @ : This function is used to udpate follower status accordingly
      @ : Created: 22/1/15
     */

    public function updateNotification($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            //echo $update->getSqlString($this->getAdapter()->getPlatform());exit;
            $result = $statement->execute();
            return true;
        } catch (\Exception $e) {
            echo $e->getPrevious()->getMessage();
            die;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getNotifications($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $lowerLimit = '', $maxLimit = '') {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'not' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            $userArr = array('FirstName', 'LastName', 'UserName', 'Image');
            $select->join(array('us' => 'hm_users'), "not.sender_id = us.UserID", $userArr, 'INNER');
            $notArr = array('type', 'message');
            $select->join(array('nt' => 'hm_notification_type'), "not.type = nt.type_id", $notArr, 'INNER');

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            if (!empty($maxLimit)) {
                $select->limit($maxLimit)->offset($lowerLimit);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*
      @ : This function is used to delete the symtoms
     */

    public function deleteNotification($where = array()) {
        try {
            $this->delete($where);
            return true;
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getNotificationType($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $lowerLimit = '', $maxLimit = '') {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'not_type' => $this->type_table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            if (!empty($maxLimit)) {
                $select->limit($maxLimit)->offset($lowerLimit);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    public function getNotificationsCount($where = array(),$maxID = null) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'not' => $this->table
            ));

            $userArr = array('count' => new \Zend\Db\Sql\Expression('COUNT(*)'),'maxId' => new \Zend\Db\Sql\Expression('Max(id)'));
            if (count($userArr) > 0) {
                $select->columns($userArr);
            }
            if($maxID){
                $select->where->greaterThan('id', $maxID);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


}
