<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE MODULE CONFIG FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: ANKIT SHUKLA(fb/gshukla67).
 * CREATOR: 12/11/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\Authentication\Adapter\DbTable as DbAuthAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Admin\Form\ChangePassword;
use Admin\Model\UsersTable;
use Admin\Model\MembersTable;
use Admin\Model\StateTable;
use Admin\Model\AdminCountryTable;
use Admin\Model\EthnicityTable;
use Admin\Model\ThresholdBmiTable;
use Admin\Model\BodyMeasurementTable;
use Admin\Model\ThresholdStepsTable;
use Admin\Model\ThresholdTimeTable;
use Admin\Model\ThresholdCalorieTable;
use Admin\Model\ThresholdDistanceTable;
use Admin\Model\ThresholdCholestrolTable;
use Admin\Model\ThresholdPulseTable;
use Admin\Model\ThresholdSystolicTable;
use Admin\Model\ThresholdDiastolicTable;
use Admin\Model\ThresholdTempTable;
use Admin\Model\ThresholdSleepTable;
use Admin\Model\ThresholdGlucoseTable;
use Admin\Model\CandiTable;
use Admin\Model\SymptTable;
use Admin\Model\TreatsTable;
use Admin\Model\QuestionTable;
use Admin\Model\VariablesTable;
use Admin\Model\AssessmentTable;
use Admin\Model\QuesAnalyzeTable;
use Admin\Model\HealthmetrixTable;
use Admin\Model\AssessmentFormulaTable;
use Admin\Model\AssessmentFormulaDisplayTable;
use Admin\Model\StudyTable;
use Admin\Model\BlogTable;
use Admin\Model\StudyEligibleTable;
use Admin\Model\PriceTable;
use Admin\Model\OnsiteStudyTrialTable;
use Admin\Model\PassiveTable;
use Admin\Model\ResearcherInviteTable;
use Admin\Model\SourceTable;
use Admin\Model\QuestionnaireTable;
use Admin\Model\AnswergroupTable;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ViewHelperProviderInterface {
    /*     * ******
     *
     * @param MvcEvent $e            
     * *** */

    public function onBootstrap($e) {
        // @var \Zend\ModuleManager\ModuleManager $moduleManager
        $moduleManager = $e->getApplication()
                ->getServiceManager()
                ->get('modulemanager');
        // @var \Zend\EventManager\SharedEventManager $sharedEvents
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        $form = new ChangePassword();
        $viewModel->form = $form;
        /*
          $sharedEvents->attach('Zend\Mvc\Controller\AbstractRestfulController', MvcEvent::EVENT_DISPATCH, array(
          $this,
          'postProcess'
          ), - 100);
          $sharedEvents->attach('Zend\Mvc\Application', MvcEvent::EVENT_DISPATCH_ERROR, array(
          $this,
          'errorProcess'
          ), 999);
          /*
          $eventManager        = $e->getApplication()->getEventManager();
          $moduleRouteListener = new ModuleRouteListener();
          $moduleRouteListener->attach($eventManager);
         */
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
                'options' => function($sm) {
            $helper = new View\Helper\Options;
            return $helper;
        },
                'Percentage' => function($sm) {
            $helper = new View\Helper\Percentage;
            return $helper;
        },
                'PercentageNonmember' => function($sm) {
            $helper = new View\Helper\PercentageNonmember;
            return $helper;
        },
                'Getdata' => function($sm) {
            $helper = new View\Helper\Getdata;
            return $helper;
        },
                'GetStudy' => function($sm) {
            $helper = new View\Helper\GetStudy;
            return $helper;
        },
                'Resize' => function($sm) {
            $helper = new View\Helper\Resize;
            return $helper;
        },
          'GroupQuestion' => function($sm) {
            $helper = new View\Helper\GroupQuestion;
            return $helper;
        },
            'PercentageGroup' => function($sm) {
            $helper = new View\Helper\PercentageGroup;
            return $helper;
        }
            )
        );
    }

    public function getServiceConfig() {
        return array(
        'factories' => array(
        'AdminAuth' => function ($sm) {
        $adapter = $sm->get('Zend\Db\Adapter\Adapter');
        $dbAuthAdapter = new DbAuthAdapter($adapter, 'hm_pass_subdomain', 'admin_user_name', 'admin_password');
        $auth = new AuthenticationService();
        $auth->setAdapter($dbAuthAdapter);
        return $auth;
        },
        'UserTable' => function ($serviceManager) {
        return new UsersTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'MembersTable' => function ($serviceManager) {
        return new MembersTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'AdminCountryTable' => function ($serviceManager) {
        return new AdminCountryTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'StateTable' => function ($serviceManager) {
        return new StateTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'EthnicityTable' => function ($serviceManager) {
        return new EthnicityTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'BmiTable' => function ($serviceManager) {
        return new ThresholdBmiTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'BodyMeasurementTable' => function ($serviceManager) {
        return new BodyMeasurementTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'StepsTable' => function ($serviceManager) {
        return new ThresholdStepsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'TimeTable' => function ($serviceManager) {
        return new ThresholdTimeTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'CalorieTable' => function ($serviceManager) {
        return new ThresholdCalorieTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'DistanceTable' => function ($serviceManager) {
        return new ThresholdDistanceTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'CholestrolTable' => function ($serviceManager) {
        return new ThresholdCholestrolTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'PulseTable' => function ($serviceManager) {
        return new ThresholdPulseTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'DiastolicTable' => function ($serviceManager) {
        return new ThresholdDiastolicTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'SystolicTable' => function ($serviceManager) {
        return new ThresholdSystolicTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'TemperatureTable' => function ($serviceManager) {
        return new ThresholdTempTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'SleepTable' => function ($serviceManager) {
        return new ThresholdSleepTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'GlucoseTable' => function ($serviceManager) {
        return new ThresholdGlucoseTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'ConditionTable' => function ($serviceManager) {
        return new CandiTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'SymptomsTable' => function ($serviceManager) {
        return new SymptTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'TreatmentsTable' => function ($serviceManager) {
        return new TreatsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'SelfAssTable' => function ($serviceManager) {
        return new AssessmentTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'VarTable' => function ($serviceManager) {
        return new VariablesTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'QuesTable' => function ($serviceManager) {
        return new QuestionTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'AnalyzeTable' => function ($serviceManager) {
        return new QuesAnalyzeTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'HealthTable' => function ($serviceManager) {
        return new HealthmetrixTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'AssessmentFormula' => function ($serviceManager) {
        return new AssessmentFormulaTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'AssessmentFormulaDisplay' => function ($serviceManager) {
        return new AssessmentFormulaDisplayTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'StuTable' => function ($serviceManager) {
        return new StudyTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'StudyEligibleTable' => function ($serviceManager) {
        return new StudyEligibleTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'BlogTable' => function ($serviceManager) {
        return new BlogTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'PriceTable' => function ($serviceManager) {
        return new PriceTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'OnsiteStudyTrial' => function ($serviceManager) {
        return new OnsiteStudyTrialTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'PassiveTable' => function ($serviceManager) {
        return new PassiveTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'ResearcherInviteTable' => function ($serviceManager) {
        return new ResearcherInviteTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'SourceTable' => function ($serviceManager) {
        return new SourceTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        }, 
        'QuestionnaireTable' => function ($serviceManager) {
        return new QuestionnaireTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'AnswergroupTable' => function ($serviceManager) {
        return new AnswergroupTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
        'Logger' => function ($sm) {
        $filename = 'log_' . date('F') . '.txt';
        $log = new Logger();
        if (!is_dir('./data/logs')) {
        mkdir('./data/logs');
        chmod('./data/logs', 0777);
        }
        $writer = new Stream('./data/logs/' . $filename);
        $log->addWriter($writer);
        return $log;
        }
        ),
        );
    }

}
