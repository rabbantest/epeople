<?php
/* * ***********************
 * PHTML: DISPLAY LIST OF STUDIES.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT CHUPHAL.
 * CREATOR: 3/3/2015.
 * UPDATED: --/--/----.
 *
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */
?>
<?php $this->flashMessenger()->clearMessages(); ?>

<style>
    .setQuestion .formWrapperContainer .row .input input[type="text"], .setQuestion .formWrapperContainer .row .input textarea {
        width:auto;
    }
</style>
<script>
    $(document).ready(function () {
        $('#AddStudyForm').validate({
            rules: {
                researcher_name: {
                    required: true,
                    minlength: 1,
                    maxlength: 100
                },
                study_title: {
                    required: true,
                    minlength: 1,
                    maxlength: 30,
                    remote: {
                        url: "<?= $this->basePath(); ?>/researcher/passstudies/checkStudyTitle",
                        type: "post",
                        data: {
                            StudyId: function () {
                                return $("#StudyId").val();
                            }
                        }
                    }
                },
                study_description: {
                    required: true,
                    minlength: 1,
                    maxlength: 500
                },
                start_date: {
                    required: true,
                },
                end_date: {
                    required: true,
                },
                panel_members: {
                    required: true,
                    digits: true
                },
                points: {
                    required: true,
                    digits: true
                },
                irb_form: {
                    required: true,
                    accept: 'doc|docx'
                },
//                dbriefing_form: {
//                    required: true,
//                    accept: 'doc|docx'
//                },
                consent_form: {
                    required: true,
                    accept: 'doc|docx'
                }
            },
            messages: {
                irb_form: {
                    required: 'Please select a file',
                    accept: 'Please select file with ext(doc|docx)'
                },
//                dbriefing_form: {
//                    required: 'Please select a file',
//                    accept: 'Please select file with ext(doc|docx)'
//                },
                consent_form: {
                    required: 'Please select a file',
                    accept: 'Please select file with ext(doc|docx)'
                },
                study_title: {
                    remote: 'Study name already exists.'
                }
            },
            errorPlacement: function (error, element) {
                $(element).parent().parent().append(error);
            }
        })
        $('#EditStudyForm').validate({
            rules: {
                researcher_name: {
                    required: true,
                    minlength: 1,
                    maxlength: 100
                },
                study_title: {
                    required: true,
                    minlength: 1,
                    maxlength: 30,
                    remote: {
                        url: "<?= $this->basePath(); ?>/researcher/passstudies/checkStudyTitle",
                        type: "post",
                        data: {
                            StudyId: function () {
                                return $("#StudyId").val();
                            }
                        }
                    }
                },
                study_description: {
                    required: true,
                    minlength: 1,
                    maxlength: 500
                },
                start_date: {
                    required: true,
                },
                end_date: {
                    required: true,
                },
                panel_members: {
                    required: true,
                    digits: true
                },
                points: {
                    required: true,
                    digits: true
                },
                irb_form: {
                    accept: 'doc|docx'
                },
//                dbriefing_form: {
//                    accept: 'doc|docx'
//                },
                consent_form: {
                    accept: 'doc|docx'
                }
            },
            messages: {
                irb_form: {
                    accept: 'Please select file with ext(doc|docx)'
                },
//                dbriefing_form: {
//                    accept: 'Please select file with ext(doc|docx)'
//                },
                consent_form: {
                    accept: 'Please select file with ext(doc|docx)'
                },
                study_title: {
                    remote: 'Study name already exists.'
                }
            },
            errorPlacement: function (error, element) {
                $(element).parent().parent().append(error);
            }
        })
<?php if (!empty($study)) { ?>
            $('#EditStudyForm').valid();
<?php } ?>
        $("#StartDate").datepicker({
            dateFormat: 'dd M yy',
            changeMonth: true,
            numberOfMonths: 2,
            onClose: function (selectedDate) {
                $("#EndDate").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#EndDate").datepicker({
            dateFormat: 'dd M yy',
            changeMonth: true,
            numberOfMonths: 2,
            onClose: function (selectedDate) {
                $("#StartDate").datepicker("option", "maxDate", selectedDate);

            }
        });

    })
</script>
<div id="content-wrapper" class="clearfix">
    <div class="pageWrapper">
         <div class="pghFixed">
            <div class="pgh_title">| Studies</div>
            <div class="pgh_mT clearfix">
                <h3><a href="<?= $this->url('pass_researcher_studies_list'); ?>">Studies List</a> &nbsp;>&nbsp; <span>
                        <?php if (!empty($study[0]['study_id'])) { ?>Edit<?php } else { ?>New<?php } ?>
                        Study</span></h3>
                <a href="<?= $this->url('pass_CreateStudies'); ?>" class="create-new">Create New</a>
            </div>
        </div>
        <?php foreach ($this->flashMessenger()->getCurrentMessages() AS $msg) { ?>
            <?php if (isset($msg["error"]) && $msg["error"] != ""): ?>
                <div class="errorMsg">
                    <span class="ui_icon error"></span>
                    <?= $msg["error"]; ?>
                </div>
            <?php endif; ?>
            <?php if (isset($msg["success"]) && $msg["success"] != ""): ?>
                <div class="successMsg">
                    <span class="ui_icon success"></span>
                    <?= $msg["success"]; ?>
                </div>
            <?php endif; ?>
        <?php } ?>
        <style>
            div.row:nth-child(even) .error{
                margin-left: 15px;
            }
        </style>
        <?php
        $form->prepare();
        echo $this->form()->openTag($form);
        ?>
        <div class="createStudy">
            <div class="formWrapperContainer csFrm">
                <div class="csf-double clearfix">
                    <div class="row">
                        <div class="input first">
                            <?= $this->formElement($form->get('researcher_name')); ?>
                            <?= $this->formElement($form->get('study_id')); ?>
                        </div>
                        <?= $this->formElementErrors($form->get('researcher_name'), array('class' => "error")); ?>
                    </div>
                    <div class="row">
                        <div class="input">
                            <?= $this->formElement($form->get('start_date')); ?>
                        </div>
                        <?= $this->formElementErrors($form->get('start_date'), array('class' => "error", 'style' => 'margin-left: 15px;')); ?>
                    </div>
                </div>
                <div class="csf-double clearfix">
                    <div class="row">
                        <div class="input first">
                            <?= $this->formElement($form->get('study_title')); ?>
                        </div>
                        <?= $this->formElementErrors($form->get('study_title'), array('class' => "error")); ?>
                    </div>
                    <div class="row">
                        <div class="input">
                            <?= $this->formElement($form->get('end_date')); ?>
                        </div>
                        <?= $this->formElementErrors($form->get('end_date'), array('class' => "error", 'style' => 'margin-left: 15px;')); ?>
                    </div>
                </div>
                <div class="csf-double clearfix">
                    <div class="row">
                        <div class="input first">
                            <?= $this->formElement($form->get('points')); ?>
                        </div>
                        <?= $this->formElementErrors($form->get('points'), array('class' => "error")); ?>
                    </div>
                    <div class="row">
                        <div class="input">
                            <?= $this->formElement($form->get('panel_members')); ?>
                        </div>
                        <?= $this->formElementErrors($form->get('panel_members'), array('class' => "error", 'style' => 'margin-left: 15px;')); ?>
                    </div>
                </div>
                <div class="csf-double clearfix">
                    <div class="row">
                        <div class="input first">
                            <?= $this->formElement($form->get('study_description')); ?>
                        </div>
                        <?= $this->formElementErrors($form->get('study_description'), array('class' => "error")); ?>
                    </div>
                    <div class="row clearfix">
                        <h4>Study Type:</h4>
                        <div class="checkbox_col radioLable">
                            <div class="checkbox_col radioButtonWrap ">
                                <span class="radio radioSType bdr_rds_circle  <?php
                                if (!empty($study)) {
                                    if ($study[0]['study_type'] == 1) {
                                        ?>selected<?php
                                          }
                                      } else {
                                          ?>selected<?php } ?>">
                                    <input <?php
                                    if (!empty($study)) {
                                        if ($study[0]['study_type'] == 1) {
                                            ?>checked="checked"<?php
                                            }
                                        } else {
                                            ?>checked="checked"<?php } ?>  class="StudyType" type="radio" value="1" id="active" name="study_type"></span>
                                <label for="active">Active</label>
                            </div>
                        </div>
                        <div class="checkbox_col radioLable">
                            <div class="checkbox_col radioButtonWrap ">
                                <span class="radio radioSType bdr_rds_circle <?php
                                if (!empty($study)) {
                                    if ($study[0]['study_type'] == 2) {
                                        ?>selected<?php
                                          }
                                      }
                                      ?>"><input class="StudyType" type="radio" <?php
                                      if (!empty($study)) {
                                          if ($study[0]['study_type'] == 2) {
                                              ?>checked="checked"<?php
                                            }
                                        }
                                        ?> value="2" id="actpass" name="study_type"></span>
                                <label for="actpass">Active & Passive</label>
                            </div>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function () {
                            $('.radioSType').click(function () {
                                if ($(this).find(".StudyType").val() == 2) {
                                    $('#PassiveSource2').slideDown('slow');
                                    $('#PassiveSource1').slideDown('slow');
                                    $('#PassiveSource3').slideDown('slow');
                                    $('#TimeInterval').addClass('required');
                                    if ($(".SType:checked", '#PassiveSource2').val() == 'Global') {
                                        $('#PassiveSource3').slideDown('slow');
                                        $('#PassiveSource3').find('select').addClass('required');
                                    } else {
                                        $('#PassiveSource').slideDown('slow');
                                        $('.Element').each(function () {
                                            if ($(this).find(".ElementInput").prop('checked') == true) {
                                                $(this).parent().find('.selectBox').find('select').addClass('required');
                                            } else {
                                                $(this).parent().find('.selectBox').find('select').removeClass('required');
                                            }
                                        });
                                        $('#PassiveSource').find('select').addClass('required');
                                    }

                                } else {
                                    $('#PassiveSource2').slideUp('slow');
                                    $('#PassiveSource1').slideUp('slow');
                                    $('#TimeInterval').removeClass('required');
                                    $('#PassiveSource3').slideUp('slow');
                                    $('#PassiveSource').slideUp('slow');
                                    $('#PassiveSource ul li').each(function () {
                                        $(this).find('select').removeClass('required');
                                    });
                                }
                            })

                            $('.radioType').click(function () {
                                if ($(this).find(".SType").val() == 'Element') {
                                    $('#PassiveSource').slideDown('slow');
                                    $('#PassiveSource3').slideUp('slow');
                                    $('#PassiveSource3').find('select').removeClass('required');
                                } else {
                                    $('#PassiveSource').slideUp('slow');
                                    $('#PassiveSource3').slideDown('slow');
                                    $('#PassiveSource3').find('select').addClass('required');
                                }
                            })


                            $('.sourceType option').mouseleave(function () {
                                if ($(this).parent().find('option:selected').length > 10) {
                                    countSel = 0;
                                    $(this).parent().find('option:selected').each(function () {
                                        countSel++;
                                        if (countSel > 10) {
                                            $(this).prop('selected', false);
                                        }
                                    })
                                    alert('User can select only ten source.');

                                }
                            });

                            $('.Element').click(function () {
                                if ($(this).find(".ElementInput").prop('checked') == true) {
                                    $(this).parent().find('.selectBox').show();
                                    $(this).parent().find('.selectBox').find('select').addClass('required');
                                } else {
                                    $(this).parent().find('.selectBox').hide();
                                    $(this).parent().find('.selectBox').find('select').removeClass('required');
                                }
                            })
                        })
                    </script>
                    <style>
                        .archivalDataList .error{
                            margin: 0px !important;
                            font-weight: normal;
                            font-size: 13px;
                        }
                    </style>

                </div>
                <?php
                $preData = isset($study[0]['source']) ? json_decode($study[0]['source'], true) : array();
                ?>

                <div class="formWrapperContainer arcDataList" id="PassiveSource1" <?php
                if (isset($study[0]['study_type'])) {
                    if ($study[0]['study_type'] != 2) {
                        ?>style="display:none;"<?php
                         }
                     } else {
                         ?>style="display:none;" <?php } ?>>
                    <h3  align="center">Time Duration/Interval</h3>
                    <div class="row clearfix" style="margin:0px!important;">
                        <ul class="archivalDataList archivalDataListNew">
                            <li class="clearfix">

                                <div class="row selectBox" style="width:50%;margin-left:25%;">

                                    <div class="select">
                                        <select style="width: 100%;" name="timeinterval" id="TimeInterval" class="select Source <?php if (isset($study[0]['study_type']) || $study[0]['study_type'] == 2) { ?> required <?php } ?>" title="Select Time Interval *">
                                            <option value="">Select Time Interval</option>
                                            <option <?php if (isset($preData['TimeInterval']) && $preData['TimeInterval'] == 1) { ?> selected <?php } ?>  value="1">Per Day</option>
                                            <option <?php if (isset($preData['TimeInterval']) && $preData['TimeInterval'] == 7) { ?> selected <?php } ?> value="7">Per Week</option>
                                            <option <?php if (isset($preData['TimeInterval']) && $preData['TimeInterval'] == 30) { ?> selected <?php } ?> value="30">Per Month</option>
                                        </select>

                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>


                <div class="formWrapperContainer arcDataList" id="PassiveSource2" <?php
                if (isset($study[0]['study_type'])) {
                    if ($study[0]['study_type'] != 2) {
                        ?>style="display:none;"<?php
                         }
                     } else {
                         ?>style="display:none; <?php } ?>>
                     <h3 align="center">Source Selection Type</h3>
                    <div class="row clearfix" style="margin-left:0%!important;">
                        <div class="checkbox_col radioLable" style="width:50%;">
                            <div class="checkbox_col radioButtonWrap">
                                <span class="radio radioType bdr_rds_circle <?php if (isset($preData['SOURCE']['Global']) || !isset($preData['SOURCE']['Element'])) { ?>selected<?php } ?>">
                                    <input <?php if (isset($preData['SOURCE']['Global']) || !isset($preData['SOURCE']['Element'])) { ?>checked="checked"<?php } ?> type="radio" value="Global" id="Global" class="SType" name="SType"></span>
                                <label for="Global">Global Source</label>
                            </div>
                        </div>
                        <div class="checkbox_col radioLable"  style="width:50%;float: right;">
                            <div class="checkbox_col radioButtonWrap">
                                <span class="radio radioType bdr_rds_circle <?php if (isset($preData['SOURCE']['Element'])) { ?>selected<?php } ?>" >
                                    <input <?php if (isset($preData['SOURCE']['Element'])) { ?>checked="checked"<?php } ?> type="radio" value="Element" id="Element" class="SType"  name="SType"></span>
                                <label for="Element">Element Source</label>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="formWrapperContainer arcDataList" id="PassiveSource3" <?php if (isset($study[0]['study_type']) && $study[0]['study_type'] != 2) { ?>style="display:none;"<?php } else { ?>style="display:none;"<?php } ?> <?php if (!isset($study[0]['study_type'])) { ?>style="display:none;"<?php } else if (isset($preData['SOURCE']['Element'])) { ?>style="display:none;"<?php } ?>>
                    <h3 align="center">Select Global Source</h3>
                    <div class="clearfix" style="margin-left: 10% !important;height: 240px;">
                        <div class="row">
                            <div style="color: #FF8400;">
                                <b>NOTE : Press CTRL for multiple Selection</b>
                            </div>

                            <select style=" height:180px;width: 80%;" name="sourceTypeGlobal[]" class="sourceType <?php if (isset($preData['SOURCE']['Global']) || !isset($preData['SOURCE']['Element'])) { ?> required <?php } ?>" title="Please select atleast one source" multiple="multiple">
                                <?php foreach ($Sources as $key => $index) { ?>
                                    <option <?php
                                    if (isset($preData['SOURCE']['Global'])) {
                                        if (in_array($key, $preData['SOURCE']['Global'])) {
                                            ?> selected="selected" <?php
                                            }
                                        }
                                        ?> value="<?= $key; ?>"><?= $index; ?></option>
                                    <?php } ?>
                            </select>

                        </div>
                    </div>
                </div>

                <div class="formWrapperContainer arcDataList" id="PassiveSource" <?php if (!isset($study[0]['study_type'])) { ?>style="display:none;"<?php } else if (isset($preData['SOURCE']['Global']) || !isset($preData['SOURCE']['Element'])) { ?>style="display:none;"<?php } ?>>
                    <h3  align="center">Select Source By Elements</h3>
                    <?php 
//echo '<pre>';
//print_r($SourcesElement);
//print_r($preData);die;?>
                    <div class="row clearfix" style="margin:0px!important;">
                        <ul class="archivalDataList archivalDataListNew">
                            <?php
                            $i = 1;
                            foreach ($SetArray as $set) {
                                ?>
                                <li class="clearfix">
                                    <div class="checkboxRWrapper row Element">
                                        <input type="checkbox" class="ElementInput" <?php if (isset($preData['SOURCE']['Element'][$set])) { ?> checked <?php } ?> value="<?= $set; ?>" id="<?= $set; ?>" name="element[<?= $i; ?>]">
                                        <label for="<?= $set; ?>"><?= $set; ?></label>
                                    </div> 
                                    <div class="row selectBox" <?php if (!isset($preData['SOURCE']['Element'][$set])) { ?>style="display:none;"<?php } ?>>

                                        <div class="select">
                                            <select id="" name="sourceType[<?= $i; ?>]" class="select Source <?php if (isset($preData['SOURCE']['Element'][$set])) { ?> required <?php } ?>" title="Select Source">
                                                <option value="">Select Source</option>
                                                <?php foreach ($SourcesElement[$set] as $key => $index) { ?>
                                                    <option 
                                                    <?php
                                                    if (isset($preData['SOURCE']['Element'][$set])) {
                                                        if ($preData['SOURCE']['Element'][$set] == $key) {
                                                            ?>
                                                                selected
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        value="<?= ($key == '') ? 'manual' : $key; ?>"><?= $index; ?></option>
                                                    <?php } ?>
                                            </select>

                                        </div>

                                    </div>
                                </li>
                                <?php
                                $i++;
                            }
                            ?>
                        </ul>
                    </div>
                </div>

            </div>




            <div class="formulaSetup">
                <div class="fsCondition formWrapperContainer">
                    <div class="fscTitle">Upload Documents</div>
                    <div class="browsDocList clearfix">
                        <div class="bdl_row clearfix" style="width: 30%;">
                            <div class="browsDoc">
                                <div style="width: auto;" onclick="javascript:document.getElementById('IRB').click();" class="bd_btn">IRB Form
                                    <?= $this->formElement($form->get('irb_form')); ?>
                                </div> 
                            </div>
                            <p class="help-block">Allowed files are doc, docx.</p>
                        </div>
                        <div class="bdl_row clearfix" style="width: 30%;">
                            <div class="browsDoc">
                                <div style="width: auto;" onclick="javascript:document.getElementById('Dbriefing').click();" class="bd_btn">	Debriefing Form
                                    <?= $this->formElement($form->get('dbriefing_form')); ?>
                                </div>
                            </div>
                            <p class="help-block">Allowed files are doc, docx.</p>
                        </div>
                        <div class="bdl_row clearfix" style="width: 30%;">
                            <div class="browsDoc">
                                <div style="width: auto;" onclick="javascript:document.getElementById('Consent').click();" class="bd_btn">Consent Form
                                    <?= $this->formElement($form->get('consent_form')); ?>
                                </div>
                            </div>
                            <p class="help-block">Allowed files are doc, docx.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $('.StuForm').change(function () {
                    $(this).parent().parent().parent().find('p').html($(this).val());
                    $(this).parent().parent().parent().find('p').css('color', '#008000');
                })
            })
        </script>
        <div class="boxBtn bb_btn_list clearfix">
            <ul style="float:right;">
                <li>
                    <?= $this->formSubmit($form->get("submit")); ?>
                </li>
            </ul>
        </div>
        <?php echo $this->form()->closeTag(); ?>
    </div>
</div>
