<?php
namespace Caregiver\Form;

use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Form;

class CareLogin extends Form{
	public function __construct ($name = null){
		parent::__construct('CareLogin');
		$this->SetAttribute('method','post');
		$this->SetAttribute('action','cg-login');
		$this->add(array(
            'name' => 'email',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'email',
                'placeholder' => 'Email Address *',
            ),
            'options' => array(
            ),
        ));
		$this->add(array(
            'name' => 'password',
            'type' => 'Zend\Form\Element\Password',
            'attributes' => array(
                'id' => 'password',
                'placeholder' => 'Password *',
            ),
            'options' => array(
            ),
        ));
		
		
		$this->add(array(
			'name' => 'Submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class'=>'submit_button',
                'value' => 'Submit',
                'id' => 'Submit'
            ),
        ));
			
	}
	
}

?>