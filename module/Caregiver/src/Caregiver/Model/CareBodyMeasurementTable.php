<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: SHIVENDRA SUMAN.
 * CREATOR: 10/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Caregiver\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class CareBodyMeasurementTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_care_threshold_bodymeasurement';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getBodyMeasurement($where = array(), $columns = array(), $lookingfor = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString();exit;
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertBodyMeasurement($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->table);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateBodyMeasurement($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            // echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ****
      @ Purpose: This function is used to return the up/down weight of user with compare to threshold metrix
      @ Created BY : Rabban Ahmad
      @ Created : 13/4/15
     * ***** */

    public function getUpDownThresholdHeight($where, $MinHeight, $MaxHeight, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'hei_rec' => 'hm_height_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('HRID DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $MinHeight && $MaxHeight) {
                if ($MinHeight < $res[0]['Height'] && $MaxHeight > $res[0]['Height']) {
                    $returnArray = array('Data' => 0);
                } elseif ($MinHeight > $res[0]['Height']) {
                    $displayData = $this->centToFeetInc($MinHeight - $res[0]['Height']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData,'label'=>'Height');
                } else {
                    $displayData = $this->centToFeetInc(($res[0]['Height'] - $MaxHeight));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData ,'label'=>'Height');
                }
            } else {
                $returnArray = array('Data' => '0');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ****
      @ Purpose: This function is used to return the up/down waist of user with compare to threshold metrix
      @ Created BY : Rabban Ahmad
      @ Created : 13/4/15
     * ***** */

    public function getUpDownThresholdWaist($where, $MinWaist, $MaxWaist, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'wai_rec' => 'hm_waist_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('WaistID DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $MinWaist && $MaxWaist) {
                if ($MinWaist < $res[0]['Waist'] && $MaxWaist > $res[0]['Waist']) {
                    $returnArray = array('Data' => 0);
                } elseif ($MinWaist > $res[0]['Waist']) {
                    $displayData = $this->centToInches($MinWaist - $res[0]['Waist']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData,'label'=>'Waist');
                } else {
                    $displayData = $this->centToInches(($res[0]['Waist'] - $MaxWaist));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData,'label'=>'Waist');
                }
            } else {
                $returnArray = array('Data' => '0');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ****
      @ Purpose: This function is used to return the up/down waist of user with compare to threshold metrix
      @ Created BY : Rabban Ahmad
      @ Created : 13/4/15
     * ***** */

    public function getUpDownThresholdWeight($where, $MinWeight, $MaxWeight, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'wei_rec' => 'hm_weight_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('WeiID DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $MinWeight && $MaxWeight) {
                if ($MinWeight < $res[0]['Weight'] && $MaxWeight > $res[0]['Weight']) {
                    $returnArray = array('Data' => 0);
                } elseif ($MinWeight > $res[0]['Weight']) {
                    $displayData = $this->gmToPound($MinWeight - $res[0]['Weight']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData,'label'=>'Weight');
                } else {
                    $displayData = $this->gmToPound(($res[0]['Weight'] - $MaxWeight));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData,'label'=>'Weight');
                }
            } else {
                $returnArray = array('Data' => '0');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function centToFeetInc($m) {
        $valInFeet = $m * 0.0328084;
        $valFeet = (int) $valInFeet;
        $valInches = round(($valInFeet - $valFeet) * 12);
        $data = $valFeet . "&prime;" . $valInches . "&Prime;";
        return $data;
    }

    public function gmToPound($grams) {
        $lb = sprintf("%.2f", $grams * 0.0022046);
        return $lb;
    }

    public function centToInches($cm) {
        $m = $cm;
        $valInches = round($m * 0.393701);
        $data = $valInches . '"';
        return $data;
    }

}
