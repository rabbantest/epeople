<?php

/* * *******
 * Purpose:This Model class is used to Manage Subscription Table
 * Created:7/04/15
 * By: Affle Appstudioz
 *   
 * ****************** */

namespace Caregiver\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

Class CareSubscriptionTable Extends AbstractTableGateway {

    public $table = 'hm_super_pricing_matrix';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getSubscriptionDetails($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'subs' => $this->table
            ));
            if (count($where) > 0) {
                $select->where($where);
            }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('noOfUsers ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            $subscription = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $subscription;
        } catch (Exception $ex) {
            throw new \Exception($ex->getPrevious()->getMessage());
        }
    }

    /*     * *******This function is used to check this user is subscribed or not and check for subscription status************* */

    public function getSubscriptionStatus($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'pms' => 'hm_pricing_matrix_subscription'
            ));

            if (count($where) > 0) {
                $where['pms.status'] = 1;
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('pms.id DESC');
            $select->limit(1);
            $date = date("Y-m-d");
            $user = array('NoOfUsers' => 'user');
            $select->join(array('spm' => 'hm_super_pricing_matrix'), "pms.price_id = spm.id", $user, 'INNER');
            $select->where(new \Zend\Db\Sql\Predicate\Expression("pms.start_date <= '$date'"));
            $select->where(new \Zend\Db\Sql\Predicate\Expression("pms.end_date >= '$date'"));
            $statement = $sql->prepareStatementForSqlObject($select);

            //$statement->prepare();
            //echo $statement->getSql();exit;
            //echo     $select->getSqlString();exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (Exception $ex) {
            throw new \Exception($ex->getPrevious()->getMessage());
        }
    }

    public function getLastSubscriptionStatus($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'pms' => 'hm_pricing_matrix_subscription'
            ));

            if (count($where) > 0) {
                $where['pms.status'] = 1;
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('pms.id DESC');
            $select->limit(1);
            $date = date("Y-m-d");
            $user = array('NoOfUsers' => 'user');
            $select->join(array('spm' => 'hm_super_pricing_matrix'), "pms.price_id = spm.id", $user, 'INNER');
            //$select->where(new \Zend\Db\Sql\Predicate\Expression("pms.start_date <= '$date'"));
            // $select->where(new \Zend\Db\Sql\Predicate\Expression("pms.end_date >= '$date'"));
            $statement = $sql->prepareStatementForSqlObject($select);

            //$statement->prepare();
            //echo $statement->getSql();exit;
            //echo     $select->getSqlString();exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (Exception $ex) {
            throw new \Exception($ex->getPrevious()->getMessage());
        }
    }

    /*     * *This function is used to get the Researcher used archival data for users * */

    public function getArchivalUserCount($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'resubs' => 'researcher_subscription_uses'
            ));
            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('resubs.id DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            if ($user)
                return $user[0]['users'];
            else
                return 0;
        } catch (Exception $ex) {
            throw new Exception($ex->getPrevious->getMessage());
        }
    }

    /*     * ****This function is used to save payment details ************* */

    public function insertPaymentDetails($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert('hm_pricing_matrix_subscription');
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ****This function is used to save payment details ************* */

    public function UpdatePaymentDetails($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table('hm_pricing_matrix_subscription');
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            // echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ****This function is used to get the Billing history****** */

    public function getBillingHistory($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_pricing_matrix_subscription'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
           // echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;
 

            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
