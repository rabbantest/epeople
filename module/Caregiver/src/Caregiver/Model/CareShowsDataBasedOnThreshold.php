<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 08/04/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Caregiver\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Delete;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class CareShowsDataBasedOnThreshold extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_associated_beacons';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function getBodyMeasurement($where = array(), $columns = array(), $lookingfor = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_care_threshold_bodymeasurement'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *********This function is used to get the Data shows for Dashboard************* */

    public function ShowDataBasedOnThreadhold($IndividualId = null, $gender = null, $age = null) {
        try {
            $i = 0;
            $returnDataArr = array();
            $session_obj = new Container('Caregiver');
            $UserID = $session_obj->userDetails['UserID'];
            $whereSystolic = array('caregiver_id' => $UserID, 'gender' => $gender);
            $columnsHdl = array('min_systolic', 'max_systolic');
            /*             * ****Code to get systolic Data****** */
            $SystolicData = $this->getThresholdSystolic($whereSystolic, $columnsHdl, $age);
            if ($SystolicData && $SystolicData[0]['min_systolic'] && $SystolicData[0]['max_systolic']) {
                $wheresystolic = array('UserID' => $IndividualId);
                $min_systolic = $SystolicData[0]['min_systolic'];
                $max_systolic = $SystolicData[0]['max_systolic'];
                $Showsystolic = $this->getUpDownSystolic($wheresystolic, $min_systolic, $max_systolic, array('systolic'));
                if ($Showsystolic['Data']) {
                    $i++;
                    $returnDataArr[] = $Showsystolic;
                } else {
                    $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Systolic', 'class' => '', 'bg_color' => '');
                }
            } else {
                $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Systolic', 'class' => '', 'bg_color' => '');
            }
            //}
            /*             * ****Code to get dystolic Data****** */
            // if ($i < 3) {
            $whereDystolic = array('caregiver_id' => $UserID, 'gender' => $gender);
            $columnsHdl = array('min_diastolic', 'max_diastolic');
            $DystolicData = $this->getThresholdDystolic($whereDystolic, $columnsHdl, $age);

            if ($DystolicData && isset($DystolicData[0]['min_diastolic']) && isset($DystolicData[0]['max_diastolic'])) {
                $wheresystolic = array('UserID' => $IndividualId);
                $min_diastolic = $DystolicData[0]['min_diastolic'];
                $max_diastolic = $DystolicData[0]['max_diastolic'];
                $Showdystolic = $this->getUpDownDystolic($wheresystolic, $min_diastolic, $max_diastolic, array('diastolic'));
                if ($Showdystolic['Data']) {
                    $i++;
                    $returnDataArr[] = $Showdystolic;
                } else {
                    $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Diastolic', 'class' => '', 'bg_color' => '');
                }
            } else {
                $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Diastolic', 'class' => '', 'bg_color' => '');
            }
            //$returnDataArr[] = array('Data' => $returnDataArr1[0]['Data'] . "/" . $returnDataArr2[0]['Data'], 'displayData' => $returnDataArr1[0]['displayData'] . "/" . $returnDataArr2[0]['displayData'], 'label' => 'Systolic/Diastolic', 'class' => $returnDataArr1[0]['class'] . "/" . $returnDataArr2[0]['class']);

            /*             * ****Code to get Plus Data****** */
            //if ($i < 3) {
            $whereDystolic = array('caregiver_id' => $UserID, 'gender' => $gender);
            $columnsHdl = array('min_pulse', 'max_pulse');
            $PluseData = $this->getThresholdPluse($whereDystolic, $columnsHdl, $age);

            if ($PluseData && $PluseData[0]['min_pulse'] && $PluseData[0]['max_pulse']) {
                $wherepluse = array('UserID' => $IndividualId);
                $min_pulse = $PluseData[0]['min_pulse'];
                $max_pulse = $PluseData[0]['max_pulse'];
                $Showdystolic = $this->getUpDownPluse($wherepluse, $min_pulse, $max_pulse, array('pulse'));
                if ($Showdystolic['Data']) {
                    $i++;
                    $returnDataArr[] = $Showdystolic;
                } else {
                    $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Pulse', 'class' => '', 'bg_color' => '');
                }
            } else {
                $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Pulse', 'class' => '', 'bg_color' => '');
            }
            // }
            /** Temprature ** */
            $whereHdl = array('caregiver_id' => $UserID);
            $columnsHdl = array('min_temp', 'max_temp');
            $getThresholdTempData = $this->getThresholdTemp($whereHdl, $columnsHdl);
            if ($getThresholdTempData && $getThresholdTempData[0]['min_temp'] && $getThresholdTempData[0]['max_temp']) {
                $whereHdl = array('UserID' => $IndividualId);
                $min_temp = $getThresholdTempData[0]['min_temp'];
                $max_temp = $getThresholdTempData[0]['max_temp'];
                $ShowLdl = $this->getUpDownTemp($whereHdl, $min_temp, $max_temp, array('temperature'));
                if ($ShowLdl['Data']) {
                    $i++;
                    $returnDataArr[] = $ShowLdl;
                } else {
                    $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Temp', 'class' => '', 'bg_color' => '');
                }
            } else {
                $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Temp', 'class' => '', 'bg_color' => '');
            }


            /*             * ****Code to get Weight Data****** */
            //if ($i < 3) {
            $ThresholdForWaitHeiWaist = $this->getBodyMeasurement(array("status" => 1, "caregiver_id" => $UserID), array('min_height', 'max_height', 'min_wast', 'max_wast', 'min_weight', 'max_weight'));
            if ($ThresholdForWaitHeiWaist && $ThresholdForWaitHeiWaist[0]['min_weight'] && $ThresholdForWaitHeiWaist[0]['max_weight']) {
                $whereHeight = array('UserID' => $IndividualId);
                $min_weight = $ThresholdForWaitHeiWaist[0]['min_weight'];
                $max_weight = $ThresholdForWaitHeiWaist[0]['max_weight'];
                $ShowHeight = $this->getUpDownThresholdWeight($whereHeight, $min_weight, $max_weight);
                if ($ShowHeight['Data']) {
                    $i++;
                    $returnDataArr[] = $ShowHeight;
                } else {
                    $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Weight', 'class' => '', 'bg_color' => '');
                }
            } else {
                $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Weight', 'class' => '', 'bg_color' => '');
            }
            //  }
            /*             * ****Code to get Fasting glucose Data****** */
//            if ($i < 3) {
            $whereHdl = array('caregiver_id' => $UserID);
            $columnsglucose = array('min_glucose', 'max_glucose');
            $GlucoseData = $this->getThresholdGlucose($whereHdl, $columnsglucose);
            if ($GlucoseData && $GlucoseData[0]['min_glucose'] && $GlucoseData[0]['max_glucose']) {
                $whereHeight = array('UserID' => $IndividualId);
                $min_glucose = $GlucoseData[0]['min_glucose'];
                $max_glucose = $GlucoseData[0]['max_glucose'];
                $ShowHeight = $this->getUpDownGlucose($whereHeight, $min_glucose, $max_glucose);
                if ($ShowHeight['Data']) {
                    $i++;
                    $returnDataArr[] = $ShowHeight;
                } else {
                    $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Glucose', 'class' => '', 'bg_color' => '');
                }
            } else {
                $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Glucose', 'class' => '', 'bg_color' => '');
            }
//            }


            /*             * ****Code to get systolic Data****** */
            // if ($i < 3) {
            // }

            /*             * ****Code to get Sleep Data****** */
//            if ($i < 3) {
            $whereHdl = array('caregiver_id' => $UserID);
            $columnsHdl = array('min_sleep', 'max_sleep');
            $HdlLdlTriglycData = $this->getThresholdsleep($whereHdl, $columnsHdl);
            if ($HdlLdlTriglycData && $HdlLdlTriglycData[0]['min_sleep'] && $HdlLdlTriglycData[0]['max_sleep']) {
                $whereHdl = array('UserID' => $IndividualId);
                $min_sleep = $HdlLdlTriglycData[0]['min_sleep'];
                $max_sleep = $HdlLdlTriglycData[0]['max_sleep'];
                $ShowLdl = $this->getUpDownSleep($whereHdl, $min_sleep, $max_sleep, array('total_sleep'));
                if ($ShowLdl['Data']) {
                    $i++;
                    $returnDataArr[] = $ShowLdl;
                } else {
                    $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Sleep', 'class' => '', 'bg_color' => '');
                }
            } else {
                $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Sleep', 'class' => '', 'bg_color' => '');
            }
//            }


            /*             * ****Code to get steps Data****** */
            //if ($i < 3) {
            $whereSteps = array('caregiver_id' => $UserID, 'gender' => $gender);
            $columnSteps = array('min_steps', 'max_steps');
            $StepsData = $this->getThresholdSteps($whereSteps, $columnSteps, $age);

            if ($StepsData && $StepsData[0]['min_steps'] && $StepsData[0]['max_steps']) {
                $wherestep = array('UserID' => $IndividualId);
                $noteqal = array('NumberOfSteps' => '');
                $min_step = $StepsData[0]['min_steps'] * 1000;
                $max_step = $StepsData[0]['max_steps'] * 1000;
                $StepsData = $this->getUpDownSteps($wherestep, $min_step, $max_step, $noteqal);
                if ($StepsData['Data']) {
                    $i++;
                    $returnDataArr[] = $StepsData;
                } else {
                    $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Steps', 'class' => '', 'bg_color' => '');
                }
            } else {
                $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Steps', 'class' => '', 'bg_color' => '');
            }
            // }


            /*             * ****Code to get HDL Data****** */
//            if ($i < 3) {
//                $whereHdl = array('caregiver_id' => $UserID);
//                $columnsHdl = array('min_hld', 'max_hld');
//                $HdlLdlTriglycData = $this->getThresholdcholestrol($whereHdl, $columnsHdl);
//                if ($HdlLdlTriglycData && $HdlLdlTriglycData[0]['min_hld'] && $HdlLdlTriglycData[0]['max_hld']) {
//                    $whereHdl = array('UserID' => $IndividualId);
//                    $min_hld = $HdlLdlTriglycData[0]['min_hld'];
//                    $max_hld = $HdlLdlTriglycData[0]['max_hld'];
//                    $ShowHdl = $this->getUpDownHdl($whereHdl, $min_hld, $max_hld, array('HDL'));
//                    if ($ShowHdl['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowHdl;
//                    }
//                }
//            }
            /*             * ****Code to get LDL Data****** */
//            if ($i < 3) {
//                $whereHdl = array('caregiver_id' => $UserID);
//                $columnsHdl = array('min_ldl', 'max_ldl', 'min_triglyc', 'max_triglyc');
//                $HdlLdlTriglycData = $this->getThresholdcholestrol($whereHdl, $columnsHdl);
//                if ($HdlLdlTriglycData && $HdlLdlTriglycData[0]['min_ldl'] && $HdlLdlTriglycData[0]['max_ldl']) {
//                    $whereHdl = array('UserID' => $IndividualId);
//                    $min_ldl = $HdlLdlTriglycData[0]['min_ldl'];
//                    $max_ldl = $HdlLdlTriglycData[0]['max_ldl'];
//                    $ShowLdl = $this->getUpDownLdl($whereHdl, $min_ldl, $max_ldl, array('LDL'));
//                    if ($ShowLdl['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowLdl;
//                    }
//                }
//            }
            /*             * ****Code to get Triglyc Data****** */
//            if ($i < 3) {
//                $whereHdl = array('caregiver_id' => $UserID);
//                $columnsHdl = array('min_triglyc', 'max_triglyc');
//                $HdlLdlTriglycData = $this->getThresholdcholestrol($whereHdl, $columnsHdl);
//                if ($HdlLdlTriglycData && $HdlLdlTriglycData[0]['min_triglyc'] && $HdlLdlTriglycData[0]['max_triglyc']) {
//                    $whereHdl = array('UserID' => $IndividualId);
//                    $min_triglyc = $HdlLdlTriglycData[0]['min_triglyc'];
//                    $max_triglyc = $HdlLdlTriglycData[0]['max_triglyc'];
//                    $ShowLdl = $this->getUpDownTriglycerides($whereHdl, $min_triglyc, $max_triglyc, array('Triglycerides'));
//                    if ($ShowLdl['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowLdl;
//                    }
//                }
//            }
            /*             * ****Code to get height Data****** */
//            if ($i < 3) {
//                $ThresholdForWaitHeiWaist = $this->getBodyMeasurement(array("status" => 1, "caregiver_id" => $UserID), array('min_height', 'max_height', 'min_wast', 'max_wast', 'min_weight', 'max_weight'));
//                if ($ThresholdForWaitHeiWaist && $ThresholdForWaitHeiWaist[0]['min_height'] && $ThresholdForWaitHeiWaist[0]['max_height']) {
//                    $whereHeight = array('UserID' => $IndividualId);
//                    $MinHeight = $ThresholdForWaitHeiWaist[0]['min_height'];
//                    $MaxHeight = $ThresholdForWaitHeiWaist[0]['max_height'];
//                    $ShowHeight = $this->getUpDownThresholdHeight($whereHeight, $MinHeight, $MaxHeight);
//                    if ($ShowHeight['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowHeight;
//                    }
//                }
//            }
            /*             * ****Code to get Waist Data****** */
//            if ($i < 3) {
//                $ThresholdForWaitHeiWaist = $this->getBodyMeasurement(array("status" => 1, "caregiver_id" => $UserID), array('min_height', 'max_height', 'min_wast', 'max_wast', 'min_weight', 'max_weight'));
//                if ($ThresholdForWaitHeiWaist && $ThresholdForWaitHeiWaist[0]['min_wast'] && $ThresholdForWaitHeiWaist[0]['max_wast']) {
//                    $whereHeight = array('UserID' => $IndividualId);
//                    $min_wast = $ThresholdForWaitHeiWaist[0]['min_wast'];
//                    $max_wast = $ThresholdForWaitHeiWaist[0]['max_wast'];
//                    $ShowWaist = $this->getUpDownThresholdWaist($whereHeight, $min_wast, $max_wast);
//                    if ($ShowWaist['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowWaist;
//                    }
//                }
//            }
            /*             * ****Code to get Weight Data****** */
            //if ($i < 3) {
//            $ThresholdForWaitHeiWaist = $this->getBodyMeasurement(array("status" => 1, "caregiver_id" => $UserID), array('min_height', 'max_height', 'min_wast', 'max_wast', 'min_weight', 'max_weight'));
//            if ($ThresholdForWaitHeiWaist && $ThresholdForWaitHeiWaist[0]['min_weight'] && $ThresholdForWaitHeiWaist[0]['max_weight']) {
//                $whereHeight = array('UserID' => $IndividualId);
//                $min_weight = $ThresholdForWaitHeiWaist[0]['min_weight'];
//                $max_weight = $ThresholdForWaitHeiWaist[0]['max_weight'];
//                $ShowHeight = $this->getUpDownThresholdWeight($whereHeight, $min_weight, $max_weight);
//                if ($ShowHeight['Data']) {
//                    $i++;
//                    $returnDataArr[] = $ShowHeight;
//                } else {
//                    $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Weight');
//                }
//            } else {
//                $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Weight');
//            }
            //  }


            return $returnDataArr;
        } catch (Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUpDownSleep($where, $min_sleep, $max_sleep, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_sleep_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('sleep_id DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $min_sleep && $max_sleep) {
                if ($min_sleep < $res[0]['total_sleep'] && $max_sleep > $res[0]['total_sleep']) {
                    $returnArray = array('Data' => 0, 'class' => '', 'bg_color' => '');
                } elseif ($min_sleep > $res[0]['total_sleep']) {
                    $displayData = ($min_sleep - $res[0]['total_sleep']);
                    $div_color = $this->getActivityStage(array('activity_name' => 'sleep'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' min', 'label' => 'Sleep', 'bg_color' => $div_color);
                } else {
                    $displayData = ($res[0]['total_sleep'] - $max_sleep);
                    $div_color = $this->getActivityStage(array('activity_name' => 'sleep'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' min', 'label' => 'Sleep', 'bg_color' => $div_color);
                }
            } else {
                $returnArray = array('Data' => '0', 'class' => '', 'bg_color' => '');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUpDownSteps($where, $min_step, $max_step, $notEqual = array(), $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'step' => 'hm_physical_acitivity'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($notEqual)) {
                foreach ($notEqual as $k => $v) {
                    $select->where->NotequalTo('step.NumberOfSteps', $v);
                }
            }
            $select->order('PhyActID DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $min_step && $max_step) {
                if ($min_step < $res[0]['NumberOfSteps'] && $max_step > $res[0]['NumberOfSteps']) {
                    $returnArray = array('Data' => 0, 'class' => '', 'bg_color' => '');
                } elseif ($min_step > $res[0]['NumberOfSteps']) {
                    $displayData = ($min_step - $res[0]['NumberOfSteps']);
                    $div_color = $this->getActivityStage(array('activity_name' => 'steps'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' steps', 'label' => 'Steps', 'bg_color' => $div_color);
                } else {
                    $displayData = ($res[0]['NumberOfSteps'] - $max_step);
                    $div_color = $this->getActivityStage(array('activity_name' => 'steps'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' steps', 'label' => 'Steps', 'bg_color' => $div_color);
                }
            } else {
                $returnArray = array('Data' => '0', 'class' => '', 'bg_color' => '');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getThresholdsleep($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_care_threshold_sleep'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUpDownTemp($where, $min_temp, $max_temp, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_temperature_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('temperature_id DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $min_temp && $max_temp) {
                if ($min_temp < $res[0]['temperature'] && $max_temp > $res[0]['temperature']) {
                    $returnArray = array('Data' => 0, 'class' => '', 'bg_color' => '');
                } elseif ($min_temp > $res[0]['temperature']) {
                    $displayData = ($min_temp - $res[0]['temperature']);
                    $div_color = $this->getActivityStage(array('activity_name' => 'temperature'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' &#8457', 'label' => 'Temp', 'bg_color' => $div_color);
                } else {
                    $displayData = ($res[0]['temperature'] - $max_temp);
                    $div_color = $this->getActivityStage(array('activity_name' => 'temperature'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' &#8457', 'label' => 'Temp', 'bg_color' => $div_color);
                }
            } else {
                $returnArray = array('Data' => '0', 'class' => '', 'bg_color' => '');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getThresholdTemp($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_care_threshold_temperature'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUpDownGlucose($where, $min_glucose, $max_glucose, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_blood_glucose_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('bgr_id DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $min_glucose && $max_glucose) {
                if ($min_glucose < $res[0]['measurement'] && $max_glucose > $res[0]['measurement']) {
                    $returnArray = array('Data' => 0, 'class' => '', 'bg_color' => '');
                } elseif ($min_glucose > $res[0]['measurement']) {
                    $displayData = ($min_glucose - $res[0]['measurement']);
                    $div_color = $this->getActivityStage(array('activity_name' => 'glucose'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' mg/dL', 'label' => 'Glucose', 'bg_color' => $div_color);
                } else {
                    $displayData = ($res[0]['measurement'] - $max_glucose);
                    $div_color = $this->getActivityStage(array('activity_name' => 'glucose'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' mg/dl', 'label' => 'Glucose', 'bg_color' => $div_color);
                }
            } else {
                $returnArray = array('Data' => '0', 'class' => '', 'bg_color' => '');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getThresholdGlucose($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_care_threshold_fastglucose'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUpDownPluse($where, $min_pulse, $max_pulse, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_blood_pressure_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('bpr_id DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $min_pulse && $max_pulse) {
                if ($min_pulse < $res[0]['pulse'] && $max_pulse > $res[0]['pulse']) {
                    $returnArray = array('Data' => 0, 'class' => '', 'bg_color' => '');
                } elseif ($min_pulse > $res[0]['pulse']) {
                    $displayData = ($min_pulse - $res[0]['pulse']);
                    $div_color = $this->getActivityStage(array('activity_name' => 'pulse'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' bpm', 'label' => 'Pulse', 'bg_color' => $div_color);
                } else {
                    $displayData = ($res[0]['pulse'] - $max_pulse);
                    $div_color = $this->getActivityStage(array('activity_name' => 'pulse'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' bpm', 'label' => 'Pulse', 'bg_color' => $div_color);
                }
            } else {
                $returnArray = array('Data' => '0', 'class' => '', 'bg_color' => '');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getThresholdPluse($where = array(), $columns = array(), $age = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_care_threshold_pulse'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->where(new \Zend\Db\Sql\Predicate\Expression("us.age_from <= '$age'"));
            $select->where(new \Zend\Db\Sql\Predicate\Expression("us.age_to >= '$age'"));
            $select->order('id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getThresholdSteps($where = array(), $columns = array(), $age = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'step' => 'hm_care_threshold_steps'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->where(new \Zend\Db\Sql\Predicate\Expression("step.age_from <= '$age'"));
            $select->where(new \Zend\Db\Sql\Predicate\Expression("step.age_to >= '$age'"));
            $select->order('id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUpDownDystolic($where, $min_diastolic, $max_diastolic, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_blood_pressure_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('bpr_id DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $min_diastolic && $max_diastolic) {
                if ($min_diastolic < $res[0]['diastolic'] && $max_diastolic > $res[0]['diastolic']) {
                    $returnArray = array('Data' => 0, 'class' => '', 'bg_color' => '');
                } elseif ($min_diastolic > $res[0]['diastolic']) {
                    $displayData = ($min_diastolic - $res[0]['diastolic']);
                    $div_color = $this->getActivityStage(array('activity_name' => 'diastolic'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' mm Hg', 'label' => 'Diastolic', 'bg_color' => $div_color);
                } else {
                    $displayData = ($res[0]['diastolic'] - $max_diastolic);
                    $div_color = $this->getActivityStage(array('activity_name' => 'diastolic'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' mm Hg', 'label' => 'Diastolic', 'bg_color' => $div_color);
                }
            } else {
                $returnArray = array('Data' => '0', 'class' => '', 'bg_color' => '');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getThresholdDystolic($where = array(), $columns = array(), $age = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_care_threshold_diastolic'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->where(new \Zend\Db\Sql\Predicate\Expression("us.age_from <= '$age'"));
            $select->where(new \Zend\Db\Sql\Predicate\Expression("us.age_to >= '$age'"));
            $select->order('id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUpDownSystolic($where, $min_systolic, $max_systolic, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_blood_pressure_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('bpr_id DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $min_systolic && $max_systolic) {
                if ($min_systolic < $res[0]['systolic'] && $max_systolic > $res[0]['systolic']) {
                    $returnArray = array('Data' => 0, 'class' => '', 'bg_color' => '');
                } elseif ($min_systolic > $res[0]['systolic']) {
                    $displayData = ($min_systolic - $res[0]['systolic']);
                    $div_color = $this->getActivityStage(array('activity_name' => 'systolic'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' mm Hg', 'label' => 'Systolic', 'bg_color' => $div_color);
                } else {
                    $displayData = ($res[0]['systolic'] - $max_systolic);
                    $div_color = $this->getActivityStage(array('activity_name' => 'systolic'), $displayData, array('stage_color'));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' mm Hg', 'label' => 'Systolic', 'bg_color' => $div_color);
                }
            } else {
                $returnArray = array('Data' => '0', 'class' => '', 'bg_color' => '');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getThresholdSystolic($where = array(), $columns = array(), $age = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_care_threshold_systolic'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->where(new \Zend\Db\Sql\Predicate\Expression("us.age_from <= '$age'"));
            $select->where(new \Zend\Db\Sql\Predicate\Expression("us.age_to >= '$age'"));
            $select->order('id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ****
      @ Purpose: This function is used to return the up/down waist of user with compare to threshold metrix
      @ Created BY : Rabban Ahmad
      @ Created : 13/4/15
     * ***** */

    public function getUpDownTriglycerides($where, $min_triglyc, $max_triglyc, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_cholesterol_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('CholID DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $min_triglyc && $max_triglyc) {
                if ($min_triglyc < $res[0]['Triglycerides'] && $max_triglyc > $res[0]['Triglycerides']) {
                    $returnArray = array('Data' => 0, 'class' => '', 'bg_color' => '');
                } elseif ($min_triglyc > $res[0]['Triglycerides']) {
                    $displayData = ($min_triglyc - $res[0]['Triglycerides']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' mg/dl', 'label' => 'Triglycerides');
                } else {
                    $displayData = ($res[0]['Triglycerides'] - $max_triglyc);
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' mg/dl', 'label' => 'Triglycerides');
                }
            } else {
                $returnArray = array('Data' => '0', 'class' => '', 'bg_color' => '');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ****
      @ Purpose: This function is used to return the up/down waist of user with compare to threshold metrix
      @ Created BY : Rabban Ahmad
      @ Created : 13/4/15
     * ***** */

    public function getUpDownLdl($where, $MinLDL, $Maxldl, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_cholesterol_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('CholID DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $MinLDL && $Maxldl) {
                if ($MinLDL < $res[0]['LDL'] && $Maxldl > $res[0]['LDL']) {
                    $returnArray = array('Data' => 0);
                } elseif ($MinLDL > $res[0]['LDL']) {
                    $displayData = ($MinLDL - $res[0]['LDL']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' mg/dl', 'label' => 'LDL');
                } else {
                    $displayData = ($res[0]['LDL'] - $Maxldl);
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' mg/dl', 'label' => 'LDL');
                }
            } else {
                $returnArray = array('Data' => '0');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ****
      @ Purpose: This function is used to return the up/down waist of user with compare to threshold metrix
      @ Created BY : Rabban Ahmad
      @ Created : 13/4/15
     * ***** */

    public function getUpDownHdl($where, $Minhld, $Maxhld, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_cholesterol_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('CholID DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $Minhld && $Maxhld) {
                if ($Minhld < $res[0]['HDL'] && $Maxhld > $res[0]['HDL']) {
                    $returnArray = array('Data' => 0, 'class' => '', 'bg_color' => '');
                } elseif ($Minhld > $res[0]['HDL']) {
                    $displayData = ($Minhld - $res[0]['HDL']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' mg/dl', 'label' => 'HDL');
                } else {
                    $displayData = ($res[0]['HDL'] - $Maxhld);
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' mg/dl', 'label' => 'HDL');
                }
            } else {
                $returnArray = array('Data' => '0', 'class' => '', 'bg_color' => '');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getThresholdcholestrol($where = array(), $columns = array(), $lookingfor = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_care_threshold_cholestrol'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }
            $select->order('id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUpDownThresholdHeight($where, $MinHeight, $MaxHeight, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'hei_rec' => 'hm_height_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('HRID DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $MinHeight && $MaxHeight) {
                if ($MinHeight < $res[0]['Height'] && $MaxHeight > $res[0]['Height']) {
                    $returnArray = array('Data' => 0, 'class' => '', 'bg_color' => '');
                } elseif ($MinHeight > $res[0]['Height']) {
                    $displayData = ($MinHeight - $res[0]['Height']);
                    $div_color = $this->getActivityStage(array('activity_name' => 'height'), $displayData, array('stage_color'));
                    $displayData = $this->centToFeetInc($MinHeight - $res[0]['Height']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData, 'label' => 'Height','bg_color'=>$div_color);
                } else {
                    $displayData = ($MaxHeight - $res[0]['Height']);
                    $div_color = $this->getActivityStage(array('activity_name' => 'height'), $displayData, array('stage_color'));
                    $displayData = $this->centToFeetInc(($res[0]['Height'] - $MaxHeight));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData, 'label' => 'Height','bg_color'=>$div_color);
                }
            } else {
                $returnArray = array('Data' => '0', 'class' => '', 'bg_color' => '');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ****
      @ Purpose: This function is used to return the up/down waist of user with compare to threshold metrix
      @ Created BY : Rabban Ahmad
      @ Created : 13/4/15
     * ***** */

    public function getUpDownThresholdWaist($where, $MinWaist, $MaxWaist, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'wai_rec' => 'hm_waist_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('WaistID DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $MinWaist && $MaxWaist) {
                if ($MinWaist < $res[0]['Waist'] && $MaxWaist > $res[0]['Waist']) {
                    $returnArray = array('Data' => 0);
                } elseif ($MinWaist > $res[0]['Waist']) {
                    $displayData = $this->centToInches($MinWaist - $res[0]['Waist']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData, 'label' => 'Waist');
                } else {
                    $displayData = $this->centToInches(($res[0]['Waist'] - $MaxWaist));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData, 'label' => 'Waist');
                }
            } else {
                $returnArray = array('Data' => '0');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ****
      @ Purpose: This function is used to return the up/down waist of user with compare to threshold metrix
      @ Created BY : Rabban Ahmad
      @ Created : 13/4/15
     * ***** */

    public function getUpDownThresholdWeight($where, $MinWeight, $MaxWeight, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'wei_rec' => 'hm_weight_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('WeiID DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $MinWeight && $MaxWeight) {
                if ($MinWeight < $res[0]['Weight'] && $MaxWeight > $res[0]['Weight']) {
                    $returnArray = array('Data' => 0);
                } elseif ($MinWeight > $res[0]['Weight']) {
                    $displayData = $this->gmToPound($MinWeight - $res[0]['Weight']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData, 'label' => 'Weight');
                } else {
                    $displayData = $this->gmToPound(($res[0]['Weight'] - $MaxWeight));
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData, 'label' => 'Weight');
                }
            } else {
                $returnArray = array('Data' => '0');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getActivityStage($where, $value, $columns) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'activity' => 'hm_activity_stages'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->where(new \Zend\Db\Sql\Predicate\Expression("activity.stage_min_val <= $value"));
            $select->where(new \Zend\Db\Sql\Predicate\Expression("activity.stage_max_val >= $value"));
            $select->order('activity_stage_id DESC');
            $select->limit(1);
            //echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            if ($res) {
                return $res[0]['stage_color'];
            } else {
                return "#FFFFFF";
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function centToFeetInc($m) {
        $valInFeet = $m * 0.0328084;
        $valFeet = (int) $valInFeet;
        $valInches = round(($valInFeet - $valFeet) * 12);
        $data = $valFeet . "&prime;" . $valInches . "&Prime;";
        return $data;
    }

    public function gmToPound($grams) {
        $lb = sprintf("%.1f", $grams * 0.0022046);
        return $lb . "    &pound";
    }

    public function centToInches($cm) {
        $m = $cm;
        $valInches = round($m * 0.393701);
        $data = $valInches . '"';
        return $data;
    }

}
