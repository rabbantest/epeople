<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE CARE GIVE GEOFENCE FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 15/04/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Caregiver\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Delete;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class CareGeofenceTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_user_geofence';
    public $tablegdata = 'hm_geofence_data';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getGpsData($where = array(), $columns = array(), $lookingfor = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'gps' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //echo $select->getSqlString();exit;
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertGpsData($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->table);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function deleteGpsData($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->table);

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*
      @ : This function is used to udpate follower status accordingly
      @ : Created: 22/1/15
     */

    public function updateGpsData($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            //echo $update->getSqlString($this->getAdapter()->getPlatform());exit;
            $result = $statement->execute();
            return true;
        } catch (\Exception $e) {

            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * User Geofence data
     * *** */

    public function getGpsUserData($where = array(), $columns = array(), $lookingfor = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'gpsudata' => $this->tablegdata
            ));

            if (count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'startDate') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("CONCAT(date, ' ', time) >= '$value'"));
                    } elseif ($key == 'endDate') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("CONCAT(date, ' ', time) <= '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->join(array("hug" => "hm_user_geofence"), "hug.id=gpsudata.geofence_id", array("name", "latitude", "longitude", "radius", "status", "location", "from_time", "to_time"), "LEFT");

            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //echo $select->getSqlString();exit;
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
