<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rabban Ahmad
 * CREATOR: 16/2/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Caregiver\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;

class CaregiverUsersTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_caregivers';
    public $weighttable = 'hm_weight_records';
    public $tablemd = 'hm_measurement_defaults';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     * This function is used to save contact iformation
     *
     * ******* */

    public function saveCareGiver($dataArry) {
        try {
            $this->insert($dataArry);
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateCareGiver($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            // echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            if ($result) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function deleteCareGiver($where = array()) {
        try {

            $insert = $this->delete($where);
            if ($insert) {
                $lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                return $lastId;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getIndividualList($where = array(), $columns = array(), $notEqual = false, $lookingfor = false, $limit = array(), $nameSearch = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_users'
            ));
            if (count($where) > 0) {
                $select->where($where);
            }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($nameSearch) {
                $select->where->nest->expression("CONCAT('', us.FirstName, us.MiddleName, us.LastName) LIKE ?", '%' . trim($nameSearch) . '%');
            }
            if (isset($where['hmu_privacy.want_takecare']))
                $select->join(array('hmu_privacy' => 'hm_users_privacy_settings'), "hmu_privacy.UserID = us.UserID", array(), 'LEFT');
            $select->join(array('hm_con' => 'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
            $select->join(array('states' => 'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
            $select->join(array('country' => 'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');

            if (count($notEqual)) {
                foreach ($notEqual as $k => $v) {
                    $select->where->NotequalTo('us.UserID', $v);
                }
            }
            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            $select->group('us.UserID');
          //echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
        //     die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getCareGiver($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'care' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $user = array('UserID', 'FirstName', 'LastName', 'UserName', 'Image', 'Email', 'Dob', 'Gender');
            $select->join(array('us' => 'hm_users'), "care.UserID = us.UserID", $user, 'INNER');
            $select->join(array('hm_con' => 'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
            $select->join(array('states' => 'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
            $select->join(array('country' => 'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');
            $select->join(array('weiTable' => $this->weighttable), "care.UserID=weiTable.UserID", array('Weight', 'Unit'), 'LEFT');

            if ($lookingfor) {
                
            }
            $select->group('us.UserID');
            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());die;
            $users = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $users;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getIndividualUser($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'care' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $user = array('UserID', 'FirstName', 'LastName', 'UserName', 'Image', 'Email', 'Dob');
            $select->join(array('us' => 'hm_users'), "care.UserID = us.UserID", $user, 'INNER');
            $select->join(array('hm_con' => 'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
            $select->join(array('states' => 'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
            $select->join(array('country' => 'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');

            if ($lookingfor) {
                
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
   
    
        /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function geteDefaultSource($where = array(), $columns = array(), $lookingfor = false, $searchContent = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'mds' => $this->tablemd
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchContent) > 0) {
                if (count($searchContent) > 0) {
                    $likeWhr = new \Zend\Db\Sql\Where();
                    foreach ($searchContent AS $fields => $data) {
                        if (!empty($data)) {
                            $likeWhr->addPredicate(
                                    new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                            );
                            $select->where($likeWhr);
                        }
                    }
                }
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            // echo $select->getSqlString();exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
