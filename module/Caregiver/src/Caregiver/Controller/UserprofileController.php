<?php

/*
  @ Used: This class is used to manage Imunization
  @ Created By: Rabban Ahmad
  @ Created On: 22/01/2015
  @ Updated On: 22/01/2015
  @ Zend Framework (http://framework.zend.com/)
 */

namespace Caregiver\Controller;

use Caregiver\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class UserprofileController extends BaseController {

    public function __construct() {
        $this->_sessionObj = new Container('Caregiver');
    }

    /*
     * @this is for individual basic  setting 
     * @ Auth: Rabban Ahmad
     * Date: 22-01-2015
     */

    public function indexAction() {
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        //$userId = $this->_sessionObj->userDetails['UserID'];
        $userId = $CusConPlug->EncryptDecrypt($this->params('id'), 'decrypt');
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $whereArray = array('UserID' => $userId);
        $userDetails = $IndUserTableObj->getUser($whereArray);

        $request = $this->getRequest();
        $dataArr = array();
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Profile Settings',
                    'userDetails' => $userDetails, 'user_id' => $this->params('id'),'CusConPlug'=>$CusConPlug
        ));
    }

    /*
     * @this is for individual contact  setting 
     * @ Auth: Rabban Ahmad
     * Date: 27-01-2015
     */

    public function contactinfoAction() {
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        //$userId = $this->_sessionObj->userDetails['UserID'];
        $userId = $CusConPlug->EncryptDecrypt($this->params('id'), 'decrypt');
        $IndContTableObj = $this->getServiceLocator()->get("ContactInfoTable");
        $CountryTableObj = $this->getServiceLocator()->get("CountryTable");
        $IndStateTableObj = $this->getServiceLocator()->get("IndStateTable");
        $CountyTableObj = $this->getServiceLocator()->get("CountyTable");
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $request = $this->getRequest();
        $dataArr = array();
        $countryArr = $CountryTableObj->getCountry();
        $whereArray = array('user_id' => $userId);
        $userContDetails = $IndContTableObj->getUserContactInfo($whereArray);
        if (isset($userContDetails[0]['country_id']) && !empty($userContDetails[0]['country_id'])) {
            $whereStateArr = array("country_id" => $userContDetails[0]['country_id']);
            $states = $IndStateTableObj->getStates($whereStateArr);
        } else {
            $states = array();
        }
        if (isset($userContDetails[0]['state_id']) && !empty($userContDetails[0]['state_id'])) {
            $whereCountyArr = array("state_id" => $userContDetails[0]['state_id']);
            $county = $CountyTableObj->getCounty($whereCountyArr);
        } else {
            $county = array();
        }

        //user current country name 
        if (isset($userContDetails[0]['country_id']) && !empty($userContDetails[0]['country_id'])) {
            $whereCountrySingleArr = array("country_id" => $userContDetails[0]['country_id']);
            $country_current = $CountryTableObj->getCountry($whereCountrySingleArr);
            $userCountry = isset($country_current[0]['country_name']) ? $country_current[0]['country_name'] : "";
        } else {
            $userCountry = '';
        }
        //user current state name 
        if (isset($userContDetails[0]['state_id']) && !empty($userContDetails[0]['state_id'])) {

            $whereStateSingleArr = array("state_id" => $userContDetails[0]['state_id']);
            $states_current = $IndStateTableObj->getStates($whereStateSingleArr);
            $UserState = $states_current[0]['state_name'];
        } else {
            $UserState = '';
        }
        //user current county name 
        if (isset($userContDetails[0]['county_id']) && !empty($userContDetails[0]['county_id'])) {
            $whereCountySingleArr = array("county_id" => $userContDetails[0]['county_id']);
            $county_current = $CountyTableObj->getCounty($whereCountySingleArr);
            $userCounty = $county_current[0]['county_name'];
        } else {
            $userCounty = '';
        }
        $userDetails = $IndUserTableObj->getUser($whereArray);
        //$UserName = ($userDetails[0]['FirstName'] ? ucfirst($userDetails[0]['FirstName']) . " " . ucfirst($userDetails[0]['LastName']) : '');

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Profile Settings',
                    'user_country' => $userCountry,
                    'user_state' => $UserState,
                    'user_county' => $userCounty,
                    'contactDetails' => $userContDetails,
                    'country' => $countryArr,
                    'states' => $states,
                    'county' => $county, 'user_id' => $this->params('id'),
                    'userDetails' => $userDetails,'CusConPlug'=>$CusConPlug
        ));
    }

    /*
     * @this is for individual profile  setting 
     * @ Auth: Rabban Ahmad
     * Date: 27-01-2015
     */

    public function profileinfoAction() {
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        //$userId = $this->_sessionObj->userDetails['UserID'];
        $userId = $CusConPlug->EncryptDecrypt($this->params('id'), 'decrypt');
        //echo $userId;die;
        $IndProfileTableObj = $this->getServiceLocator()->get("ProfileInfoTable");
        $IndContTableObj = $this->getServiceLocator()->get("ContactInfoTable");
        $UserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $request = $this->getRequest();

        $whereArray = array('UserID' => $userId);
        $userDetails = $UserTableObj->getUser($whereArray);


        $whereProfileArray = array('user_id' => $userId);
        $userProfileDetails = $IndProfileTableObj->getUserProfileInfo($whereProfileArray);
        $userContDetails = $IndContTableObj->getUserContactInfo($whereProfileArray);
        //echo '<pre>';print_r($userContDetails);die;
        $LanguageArr = $UserTableObj->getLanguage();
        $BloodTypeArr = $UserTableObj->getBloodType();
        $EthnicityArr = $UserTableObj->getEthnicity();
        $EducationcArr = $UserTableObj->getEducation();

        //select current language of user 
        if (isset($userProfileDetails[0]['language']) && !empty($userProfileDetails[0]['language'])) {
            $whereLangArray = array("laguage_id" => $userProfileDetails[0]['language']);
            $language_current = $UserTableObj->getLanguage($whereLangArray);
            $userLanguage = $language_current[0]['language'];
        } else {
            $userLanguage = '';
        }

        //select current Ethnicity of user
        if (isset($userContDetails[0]['ethnicity']) && !empty($userContDetails[0]['ethnicity'])) {

            $whereEthArray = array("ethnicity_id" => $userContDetails[0]['ethnicity']);
            $ethnicity_current = $UserTableObj->getEthnicity($whereEthArray);
            $userEthnicity = $ethnicity_current[0]['ethnicity_name'];
        } else {
            $userEthnicity = '';
        }

        //select current education of user 
        if (isset($userProfileDetails[0]['education_degree']) && !empty($userProfileDetails[0]['education_degree'])) {

            $whereEducationArray = array("education_id" => $userProfileDetails[0]['education_degree']);
            $education_current = $UserTableObj->getEducation($whereEducationArray);
            $userEducation = $education_current[0]['highest_degree_name'];
        } else {
            $userEducation = '';
        }
        $typeidArr = explode(',', $userDetails[0]['UserTypeId']);
        $isResercher = 0;
        $isCaregiver = 0;
        if (in_array(2, $typeidArr)) {
            $isCaregiver = 1;
        }if (in_array(3, $typeidArr)) {
            $isResercher = 1;
        }
        $userDetails = $UserTableObj->getUser($whereArray);
        
        //$UserName = ($userDetails[0]['FirstName'] ? ucfirst($userDetails[0]['FirstName']) . " " . ucfirst($userDetails[0]['LastName']) : '');
       // echo 'hello'.$userDetails[0]['Image'];die;
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Profile Settings',
                    'userId' => $userId,
                    'userImage' => (isset($userDetails[0]['Image']) ? $userDetails[0]['Image'] : ""),
                    'userLanguage' => $userLanguage,
                    'userEthnicity' => $userEthnicity,
                    'userEducation' => $userEducation,
                    'profileDetails' => $userProfileDetails,
                    'language' => $LanguageArr,
                    'education' => $EducationcArr,
                    'ethnicity' => $EthnicityArr,
                    'bloodtype' => $BloodTypeArr,
                    'isCaregiver' => $isCaregiver,
                    'isResearcher' => $isResercher, 'user_id' => $this->params('id'),
                     'userDetails' => $userDetails,'CusConPlug'=>$CusConPlug
        ));
    }

}
