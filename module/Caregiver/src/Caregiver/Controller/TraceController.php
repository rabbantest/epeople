<?php

/*
  @ Used: This class is used to TARCE USER BY CAREGIVER
  @ Created By: Shivendra Suman
  @ Created On: 07/04/2015
  @ Updated On: --/--/---
  @ Zend Framework (http://framework.zend.com/)
 */

namespace Caregiver\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class TraceController extends BaseController {

    protected $_sessionObj;

    public function __construct() {
        parent::__construct();
        $this->_sessionObj = new Container('Caregiver');
    }

    public function indexAction() {
        $caregiver_id = $this->_sessionObj->userDetails['UserID']; // caregiver id
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $CareBeaconTableObj = $this->getServiceLocator()->get("CareBeacon");
        $CareAssBecTabObj = $this->getServiceLocator()->get('CareBeaconAssoc');
        $CareBeDataTabObj = $this->getServiceLocator()->get('CareBeaconData');
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin  
        $time_zone = $this->_sessionObj->userDetails['time_zone'];


        $today2 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        $today_date_time = $today2->format('Y-m-d H:i:s');
        // encryptdata
        $userid = $CusConPlug->decryptdata($this->params('id'));
        $whereArray = array('UserID' => $userid);
        $coloumArray = array("UserID", "FirstName", "LastName", "Gender", "Dob", "Image", "short_description");
        $userDetails = $IndUserTableObj->getUser($whereArray, $coloumArray);
        // $associateStatus=0; // 0 means no permission to edit 

        $wherebecon = array(' becon.UserID' => $userid,);
        $beconList = $CareBeaconTableObj->getUserBeacon($wherebecon);
        $beconData = array();
        if (count($beconList) > 0) {
            $i = 0;
            //beacon data            
            //set date and time for data 
            $today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0, 0);
            $today->format('Y-m-d H:i:s');
            //Get UTC midnight time
            $today->setTimezone(new \DateTimeZone("UTC"));
            $start = $today->format('Y-m-d H:i:s');
            $end = $today->modify('+24 hours')->format('Y-m-d H:i:s'); //$start + 24;

            $coloumn = array();
            $lookingfor = 'bd.creation_date DESC';
            $beconData = $CareBeDataTabObj->getBeaconData(array("bd.UserID" => $userid, "startDate" => $start, "endDate" => $end), $coloumn, $lookingfor);
            //$beconData = $CareBeDataTabObj->getBeaconData(array("bd.UserID" => $userid, "date" => $today_date), $coloumn, $lookingfor);

            foreach ($beconList as $bvalue) {
                //   $beconData[] =  
                $AssId = $CareAssBecTabObj->getAssocBeacon(array("beacon_id" => $bvalue['id'], 'caregiver_id' => $caregiver_id));
                if (count($AssId) > 0) {
                    $beconList[$i]['associateStatus'] = 1;
                    $beconList[$i]['ass_beacon_id'] = $AssId[0]['beacon_id']; //assocate becone id 
                } else {
                    $beconList[$i]['associateStatus'] = 0;  //0 means no permission to edit 
                    $beconList[$i]['ass_beacon_id'] = ''; //assocate becone id
                }
                $i++;
            }
        }
        /*         * ********Code to update unread notification************** */
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
        $whereUpdate = array('receiver_id' => $caregiver_id, 'sender_id' => $userid, 'type' => array('7', '8'));
        $updateData = array('is_read' => 1);
        $NotificationTableObj->updateNotification($whereUpdate, $updateData);
        /*         * ********End Code to update unread notification************** */
        $sendArr = array(
            'pageTitle' => "View Beacons",
            'session' => $this->_sessionObj->isCaregiverlogin,
            'userDetails' => $userDetails,
            'CusConPlug' => $CusConPlug,
            'beconList' => $beconList,
            'beconData' => $beconData,
            'today_date' => $today_date_time,
            'time_zone' => $time_zone
        );
        return new ViewModel($sendArr);
    }

    /*     * ********
     * Action: LIST ALL RULE OF BEACON
     * @author: SHIVENDRA SUMAN
     * Created Date: 09-04-2015.
     * *** */

    public function setruleAction() {

        $becon_title = '';
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $CareBeaconTableObj = $this->getServiceLocator()->get("CareBeacon");
        $careBeRuTabObj = $this->getServiceLocator()->get("CareBeaconRule");
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin   

        $userid = $CusConPlug->decryptdata($this->params('id'));
        $beaconid = $CusConPlug->decryptdata($this->params('beaconid'));
        //user details
        $whereArray = array('UserID' => $userid);
        $coloumArray = array("UserID", "FirstName", "LastName", "Gender", "Dob", "Image", "short_description");
        $userDetails = $IndUserTableObj->getUser($whereArray, $coloumArray);

        //becone details  
        $coloumArrayBecon = array('becon_title');
        $wherebecon = array('UserID' => $userid, 'id' => $beaconid);
        $beconList = $CareBeaconTableObj->getUserBeacon($wherebecon, $coloumArrayBecon);
        if (count($beconList) > 0) {
            $becon_title = $beconList[0]['becon_title'];
        }
        $wherebeconRul = array('UserID' => $userid, 'beacon_id' => $beaconid);
        $ruleData = $careBeRuTabObj->getBeaconRule($wherebeconRul);
        $sendArr = array(
            'pageTitle' => "Track  Moves",
            'session' => $this->_sessionObj->isCaregiverlogin,
            'userDetails' => $userDetails,
            'CusConPlug' => $CusConPlug,
            'beaconid' => $this->params('beaconid'),
            'becon_title' => $becon_title,
            'user_id' => $this->params('id'),
            'ruleData' => $ruleData,
        );
        return new ViewModel($sendArr);
    }

    /*     * ********
     * Action: EDIT RULE OF BECONE SHOW DATA ON POUP
     * @author: SHIVENDRA SUMAN
     * Created Date: 14-04-2015.
     * *** */

    public function editruleAction() {
        $becon_title = '';
        $careBeRuTabObj = $this->getServiceLocator()->get("CareBeaconRule");
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin   

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $rule_id = $CusConPlug->decryptdata($postDataArr['rule_id']);
            $user_id = $postDataArr['user_id'];
            $beaconid = $postDataArr['beaconid'];
            $wherebeconRul = array('rule_id' => $rule_id);

            $ruleData = $careBeRuTabObj->getBeaconRule($wherebeconRul);

            if (count($ruleData) > 0) {
                $ruleData = $ruleData[0];
            } else {
                $ruleData = array();
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        return $viewModel->setVariables(array(
                    'pageTitle' => "Edit Rule",
                    'session' => $this->_sessionObj->isCaregiverlogin,
                    'beaconid' => $beaconid,
                    'user_id' => $user_id,
                    'rule_id' => $CusConPlug->encryptdata($rule_id),
                    'ruleData' => $ruleData,
        ));
    }

    /*     * ********
     * Action: LIST BECON DATA ON AJAX
     * @author: SHIVENDRA SUMAN
     * Created Date: 14-04-2015.
     * *** */

    public function listbeacondataAction() {
        $CareBeDataTabObj = $this->getServiceLocator()->get('CareBeaconData');
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin 
        $time_zone = $this->_sessionObj->userDetails['time_zone'];
        $today1 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        $today_date = $today1->format('Y-m-d');
        $today2 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        $today_date_time = $today2->format('Y-m-d H:i:s');
        $request = $this->getRequest();
        $beconData = array();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if (!empty($postDataArr['data_date'])) {
                $today = (new \DateTime($postDataArr['data_date'] . " " . date('H:i:s', strtotime("now")), new \DateTimeZone($time_zone)))->setTime(0, 0);
                $today->format('Y-m-d H:i:s');
                //Get UTC midnight time
                $today->setTimezone(new \DateTimeZone("UTC"));
                $start = $today->format('Y-m-d H:i:s');
                $end = $today->modify('+24 hours')->format('Y-m-d H:i:s'); //$start + 24;
            } else {
                $today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0, 0);
                $today->format('Y-m-d H:i:s');
                //Get UTC midnight time
                $today->setTimezone(new \DateTimeZone("UTC"));
                $start = $today->format('Y-m-d H:i:s');
                $end = $today->modify('+24 hours')->format('Y-m-d H:i:s'); //$start + 24;
            }

            $user_id = isset($postDataArr['user_id']) ? $CusConPlug->decryptdata($postDataArr['user_id']) : '';
            $beacon_id = isset($postDataArr['beacon_id']) ? $CusConPlug->decryptdata($postDataArr['beacon_id']) : '';
            //   $data_date = (isset($postDataArr['data_date']) && !empty($postDataArr['data_date'])) ? date("Y-m-d", strtotime($utcDateTime)) : $today_date;
            if (empty($beacon_id)) {
                $whereArray = array("bd.UserID" => $user_id, 'startDate' => $start, 'endDate' => $end);
            } else {
                $whereArray = array("bd.beacon_id" => $beacon_id, "bd.UserID" => $user_id, 'startDate' => $start, 'endDate' => $end);
            }
            $coloumn = array();
            $lookingfor = 'date DESC, time DESC';
            $beconData = $CareBeDataTabObj->getBeaconData($whereArray, $coloumn, $lookingfor);
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        return $viewModel->setVariables(array(
                    'pageTitle' => "View Beacons",
                    'session' => $this->_sessionObj->isCaregiverlogin,
                    'beconData' => $beconData,
                    'CusConPlug' => $CusConPlug,
                    'today_date' => $today_date_time,
                    'time_zone' => $time_zone
                        // 'userid' => $CusConPlug->encryptdata($user_id),
        ));
    }

    /*     * ********
     * Action: TRACK MOVE DATA 
     * @author: SHIVENDRA SUMAN
     * Created Date: 07-04-2015.
     * *** */

    public function trackmoveAction() {
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin   
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $IndPhyTableObj = $this->getServiceLocator()->get("IndividualPhysicalActivityTable");
        $caregiver_id = $this->_sessionObj->userDetails['UserID'];  // caregiveri id 
        $time_zone = $this->_sessionObj->userDetails['time_zone'];
        $userid = $CusConPlug->decryptdata($this->params('id')); //user id
        //
      
        $today2 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        $today_date_time = $today2->format('Y-m-d H:i:s');

        //user details
        $whereArray = array('UserID' => $userid);
        $coloumArray = array("UserID", "FirstName", "LastName", "Gender", "Dob", "Image", "short_description");
        $userDetails = $IndUserTableObj->getUser($whereArray, $coloumArray);
        //moves data 
        // set date midnight of utc 0 
        $today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0, 0);
        $today->format('Y-m-d H:i:s');
        //Get UTC midnight time
        $today->setTimezone(new \DateTimeZone("UTC"));
        $startDate = $today->format('Y-m-d H:i:s');
        $endDate = $today->modify('+24 hours')->format('Y-m-d H:i:s'); //$start + 24;

        $whereMovesArray = array('UserID' => $userid, 'startDate' => $startDate, 'endDate' => $endDate, 'source' => 'movesapp');
        $orderby = "ExerciseRecorddate DESC, ExerciseRecordtime DESC";
        $movesData = $IndPhyTableObj->getPhysicalRecords($whereMovesArray, array(), $orderby);
        $i = 0;
        $coloumn = array('color');
        foreach ($movesData as $mvalue) {
            $colordata = $IndPhyTableObj->getActivityColor(array('activity' => $mvalue['ActivityType']), $coloumn);
            $colorname = (isset($colordata[0]['color']) ? $colordata[0]['color'] : '');
            if (empty($colorname)) {
                $colorname = '#e1e1e1';
            }
            $movesData[$i]['color'] = $colorname;
            $i++;
        }

        $sendArr = array(
            'pageTitle' => "Track  Moves",
            'session' => $this->_sessionObj->isCaregiverlogin,
            'userDetails' => $userDetails,
            'CusConPlug' => $CusConPlug,
            'movesData' => $movesData,
            'today_date' => $today_date_time,
            'time_zone' => $time_zone
        );
        return new ViewModel($sendArr);
    }

    /*     * ********
     * Action: LIST DATA OF FOLLOW IN GPS USER DATA 
     * @author: SHIVENDRA SUMAN
     * Created Date: 14-04-2015.
     * *** */

    public function followongpsAction() {
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin   
        $CareGeofenceTableObj = $this->getServiceLocator()->get("CareGeofence");
        $caregiver_id = $this->_sessionObj->userDetails['UserID'];  // caregiveri id
        $userid = $CusConPlug->decryptdata($this->params('id'));
        $time_zone = $this->_sessionObj->userDetails['time_zone'];
        $today1 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        $today_date = $today1->format('Y-m-d');

        $today2 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        $today_date_time = $today2->format('Y-m-d H:i:s');
        //user details

        $whereArray = array('UserID' => $userid);
        $coloumArray = array("UserID", "FirstName", "LastName", "Gender", "Dob", "Image", "short_description");
        $userDetails = $IndUserTableObj->getUser($whereArray, $coloumArray);
        //geofence list
        $whereArrayGps = array("caregiver_id" => $caregiver_id, "UserID" => $userid, "status" => 1);
        $geofenceData = $CareGeofenceTableObj->getGpsData($whereArrayGps);
        //time according to time zone
        $today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0, 0);
        $today->format('Y-m-d H:i:s');
        //Get UTC midnight time
        $today->setTimezone(new \DateTimeZone("UTC"));
        $start = $today->format('Y-m-d H:i:s');
        $end = $today->modify('+24 hours')->format('Y-m-d H:i:s'); //$start + 24;

        $wgereGpsArr = array("hug.UserID" => $userid, "hug.caregiver_id" => $caregiver_id, 'startDate' => $start, 'endDate' => $end);
        $orderby = "date DESC, time DESC";
        // $wgereGpsArr = array("hug.UserID" => $userid, "hug.caregiver_id" => $caregiver_id, "date" => $today_date);        
        $geofenceUserData = $CareGeofenceTableObj->getGpsUserData($wgereGpsArr, array(), $orderby);
        /*         * ********Code to update unread notification************** */
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
        $whereUpdate = array('receiver_id' => $caregiver_id, 'sender_id' => $userid, 'type' => 6);
        $updateData = array('is_read' => 1);
        $NotificationTableObj->updateNotification($whereUpdate, $updateData);
        /*         * ********End Code to update unread notification************** */
        //echo '<pre>';print_r($this->_sessionObj->userDetails);die;
        $sendArr = array(
            'pageTitle' => "Geofence Details",
            'session' => $this->_sessionObj->isCaregiverlogin,
            'userDetails' => $userDetails,
            'CusConPlug' => $CusConPlug,
            'geofenceData' => $geofenceData,
            'geofenceUserData' => $geofenceUserData,
            'today_date' => $today_date_time,
            'time_zone' => $time_zone
        );
        return new ViewModel($sendArr);
    }

    /*     * ********
     * Action: FOLLOW ON GPS ADD 
     * @author: SHIVENDRA SUMAN
     * Created Date: 15-04-2015.
     * *** */

//05:39 pm 11:59 pm
    function followongpsaddAction() {
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $CareGeofenceTableObj = $this->getServiceLocator()->get("CareGeofence");
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin  
        $caregiver_id = $this->_sessionObj->userDetails['UserID'];  // caregiveri id 
        $time_zone = $this->_sessionObj->userDetails['time_zone'];
        $userid = $CusConPlug->decryptdata($this->params('id'));
        $request = $this->getRequest();
        //save geofance data 
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            
            // validate form
            if (empty($postDataArr['geofence_name'])) {
                $message = "Please enter geofence name.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                // return $this->redirect()->toRoute('cg_followongpsadd');
            }
            if (empty($postDataArr['location'])) {
                $message = "Please select location.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                //return $this->redirect()->toRoute('cg_followongpsadd');
            }
            if (empty($postDataArr['location_lat'])) {
                $message = "Please select location.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                //return $this->redirect()->toRoute('cg_followongpsadd');
            }
            if (empty($postDataArr['location_lang'])) {
                $message = "Please select location.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                // return $this->redirect()->toRoute('cg_followongpsadd');
            }

            if ($postDataArr['notify_rule'] == '') {
                $message = "Please select when notify.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                //  return $this->redirect()->toRoute('cg_followongpsadd');
            }
            if (empty($postDataArr['from_time'])) {
                $message = "Please select from time.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                //return $this->redirect()->toRoute('cg_followongpsadd');
            }
            if (empty($postDataArr['to_time'])) {
                $message = "Please select to time.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                //return $this->redirect()->toRoute('cg_followongpsadd');
            }
//            $today1 = (new \DateTime("now", new \DateTimeZone($time_zone)));
//            $today_date = $today1->format('Y-m-d');
//            $DateTimeRes = $CusConPlug->convetUTC($today_date . " " . $postDataArr['from_time'], $time_zone);
//            $from_time = $DateTimeRes[1];
//            $DateTime = $CusConPlug->convetUTC($today_date . " " . $postDataArr['to_time'], $time_zone);
//            $to_time = $DateTime[1];
//            echo date("H:i:s", strtotime($postDataArr['to_time']));
//            echo '<pre>'; print_r($postDataArr);die;
            $saveArray = array(
                'UserID' => $userid,
                'caregiver_id' => $caregiver_id,
                'name' => $postDataArr['geofence_name'],
                'latitude' => $postDataArr['location_lat'],
                'longitude' => $postDataArr['location_lang'],
                'radius' => $postDataArr['map_radius'] * 1.609344,
                'location' => $postDataArr['location'],
                'email_notification' => $postDataArr['email_notification'],
                'notify' => $postDataArr['notify_rule'],
                'from_time' => date("H:i:s", strtotime($postDataArr['from_time'])),
                'to_time' => date("H:i:s", strtotime($postDataArr['to_time'])),
                'creation_date' => round(microtime(true) * 1000)
            );
            //echo '<pre>'; print_r($saveArray);die;
            $ins = $CareGeofenceTableObj->insertGpsData($saveArray);
            if ($ins) {
                $message = "Geofence sucessfully added.";
                $this->flashMessenger()->addMessage(array('success' => $message));
                /*                 * *******Start code to save notification Created: Rabban************ */
                $this->NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
                $type = 'geofence_request';
                $notDataArr = array("sender_id" => $caregiver_id, "user_type" => 1, "receiver_id" => $userid, "created_date" => round(microtime(true) * 1000));
                $saveNoti = $this->NotificationTableObj->SaveNotification($notDataArr, $type);
                //echo $saveNoti;die;
                /*                 * *******End code to save notification************ */
                /*                 * *****************send mail start**************** */
                $userData = $IndUserTableObj->getIndividualUser(array('UserID' => $userid));
                $userFname = ((isset($userData[0]['FirstName']) ? $userData[0]['FirstName'] : ''));
                $userEmail = ((isset($userData[0]['Email']) ? $userData[0]['Email'] : ''));
                $Name = $this->_sessionObj->userDetails['FirstName'] . $this->_sessionObj->userDetails['LastName'];
                $url = "https://" . $_SERVER['HTTP_HOST'] . "/individual-request-geofence";
                $messages = "<img src='http://individual.quantextualhealth.com/img/mail_header_image.jpeg' alt='Company Logo'><br> <br>";
                $messages .= "<br><br>";
                $messages .= "Dear " . $userFname . ",<br> <br>";
                $messages .= "$Name  is requesting to establish a geofence for you.  To accept this request, simply visit the Notifications section of your account by clicking <a href='$url' target='blank'> here </a> or within your Q app.
";
                $messages .="<br><br>Here’s to your health,<br>";
                $messages .="<br>The Quantextual Health Team";
                $subject = "Geofence Request";
                $CusConPlug->senInvitationMail($userEmail, $subject, $messages);
                /*                 * *****************send mail end****************** */
                return $this->redirect()->toUrl('/cg-followongps/' . $postDataArr['user_id']);
            } else {
                $message = "Some thing wrong. Please try again later.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toUrl('/cg-followongps-add/' . $postDataArr['user_id']);
            }
        }
        //user details
        $whereArray = array('UserID' => $userid);
        $coloumArray = array("UserID", "FirstName", "LastName", "Gender", "Dob", "Image", "short_description");
        $userDetails = $IndUserTableObj->getUser($whereArray, $coloumArray);


        $sendArr = array(
            'pageTitle' => "Track  Moves",
            'session' => $this->_sessionObj->isCaregiverlogin,
            'userid' => $this->params('id'),
            'userDetails' => $userDetails,
            'CusConPlug' => $CusConPlug,
        );
        return new ViewModel($sendArr);
    }

    /*     * ********
     * Action: FOLLOW ON GPS EDIT 
     * @author: SHIVENDRA SUMAN
     * Created Date: 14-04-2015.
     * *** */

    public function followongpseditAction() {
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $CareGeofenceTableObj = $this->getServiceLocator()->get("CareGeofence");
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin   
        $caregiver_id = $this->_sessionObj->userDetails['UserID'];  // caregiveri id 
        $time_zone = $this->_sessionObj->userDetails['time_zone'];
        $userid = $CusConPlug->decryptdata($this->params('id'));
        $geofence_id = $CusConPlug->decryptdata($this->params('gpsid'));
        //user details

        $whereArray = array('UserID' => $userid);
        $coloumArray = array("UserID", "FirstName", "LastName", "Gender", "Dob", "Image");
        $userDetails = $IndUserTableObj->getUser($whereArray, $coloumArray);

        $whereArrayGps = array("caregiver_id" => $caregiver_id, "UserID" => $userid, "id" => $geofence_id);
        $geofenceData = $CareGeofenceTableObj->getGpsData($whereArrayGps);
        if (count($geofenceData) > 0) {
            $geofenceData = $geofenceData[0];
        } else {
            $geofenceData = array();
        }

        $request = $this->getRequest();
        //save geofance data 
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
//            echo '<pre>';
//            print_r($postDataArr);
            // validate form
            if (empty($postDataArr['geofence_id'])) {

                $message = "Some thing wrong. Please try again later.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                // return $this->redirect()->toRoute('cg_followongpsadd');
            }

            if (empty($postDataArr['geofence_name'])) {
                $message = "Please enter geofence name.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                // return $this->redirect()->toRoute('cg_followongpsadd');
            }
            if (empty($postDataArr['location'])) {
                $message = "Please select location.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                //return $this->redirect()->toRoute('cg_followongpsadd');
            }
            if (empty($postDataArr['location_lat'])) {
                $message = "Please select location.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                //return $this->redirect()->toRoute('cg_followongpsadd');
            }
            if (empty($postDataArr['location_lang'])) {
                $message = "Please select location.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                // return $this->redirect()->toRoute('cg_followongpsadd');
            }


            if ($postDataArr['notify_rule'] == '') {
                $message = "Please select when notify.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                //  return $this->redirect()->toRoute('cg_followongpsadd');
            }
            if (empty($postDataArr['from_time'])) {
                $message = "Please select from time.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                //return $this->redirect()->toRoute('cg_followongpsadd');
            }
            if (empty($postDataArr['to_time'])) {
                $message = "Please select to time.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                //return $this->redirect()->toRoute('cg_followongpsadd');
            }

//            $today1 = (new \DateTime("now", new \DateTimeZone($time_zone)));
//
//            $today_date = $today1->format('Y-m-d');
//
//            $DateTimeRes = $CusConPlug->convetUTC($today_date . " " . $postDataArr['from_time'], $time_zone);
//            $from_time = $DateTimeRes[1];
//            $DateTime = $CusConPlug->convetUTC($today_date . " " . $postDataArr['to_time'], $time_zone);
//            $to_time = $DateTime[1];
            $email_notification = 0;
            if ($postDataArr['email_notification'] == 1) {
                $email_notification = 1;
            }

            $saveArray = array(
                'UserID' => $userid,
                'caregiver_id' => $caregiver_id,
                'name' => $postDataArr['geofence_name'],
                'latitude' => $postDataArr['location_lat'],
                'longitude' => $postDataArr['location_lang'],
                'radius' => $postDataArr['map_radius'] * 1.609344,
                'location' => $postDataArr['location'],
                'email_notification' => $email_notification,
                'notify' => $postDataArr['notify_rule'],
//                'from_time' => date("H:i:s", strtotime($from_time)),
//                'to_time' => date("H:i:s", strtotime($to_time)),
                'from_time' => date("H:i:s", strtotime($postDataArr['from_time'])),
                'to_time' => date("H:i:s", strtotime($postDataArr['to_time'])),
                'creation_date' => round(microtime(true) * 1000)
            );
            $wheregpsCon = array("id" => $CusConPlug->decryptdata($postDataArr['geofence_id']));
            $ins = $CareGeofenceTableObj->updateGpsData($wheregpsCon, $saveArray);
            if ($ins) {
                $message = "Geofence sucessfully updated.";
                $this->flashMessenger()->addMessage(array('success' => $message));
                return $this->redirect()->toUrl('/cg-followongps/' . $postDataArr['user_id']);
            } else {
                $message = "Some thing wrong. Please try again later.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toUrl('/cg-followongps-edit/' . $postDataArr['user_id'] . '/' . $postDataArr['geofence_id']);
            }
        }
      
        $sendArr = array(
            'pageTitle' => "Edit  Geofence",
            'session' => $this->_sessionObj->isCaregiverlogin,
            'userDetails' => $userDetails,
            'CusConPlug' => $CusConPlug,
            'geofence_id' => $this->params('gpsid'),
            'userid' => $this->params('id'),
            'geofenceData' => $geofenceData,
            'time_zone' => $time_zone
        );
        return new ViewModel($sendArr);
    }

    /*     * ********
     * Action: LIST DATA OF FOLLOW IN GPS USER DATA 
     * @author: SHIVENDRA SUMAN
     * Created Date: 14-04-2015.
     * *** */

    public function followongpslistAction() {
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin   
        $CareGeofenceTableObj = $this->getServiceLocator()->get("CareGeofence");
        $caregiver_id = $this->_sessionObj->userDetails['UserID'];  // caregiveri id 
        // $data_date = date("Y-m-d");
        $time_zone = $this->_sessionObj->userDetails['time_zone'];

        $today2 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        $today_date_time = $today2->format('Y-m-d H:i:s');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();

            if (!empty($postDataArr['data_date'])) {
                $today = (new \DateTime($postDataArr['data_date'] . " " . date('H:i:s', strtotime("now")), new \DateTimeZone($time_zone)))->setTime(0, 0);
                $today->format('Y-m-d H:i:s');
                //Get UTC midnight time
                $today->setTimezone(new \DateTimeZone("UTC"));
                $start = $today->format('Y-m-d H:i:s');
                $end = $today->modify('+24 hours')->format('Y-m-d H:i:s'); //$start + 24;
            } else {
                $today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0, 0);
                $today->format('Y-m-d H:i:s');
                //Get UTC midnight time
                $today->setTimezone(new \DateTimeZone("UTC"));
                $start = $today->format('Y-m-d H:i:s');
                $end = $today->modify('+24 hours')->format('Y-m-d H:i:s'); //$start + 24;
            }

            $user_id = isset($postDataArr['user_id']) ? $CusConPlug->decryptdata($postDataArr['user_id']) : '';
            $geofence_id = isset($postDataArr['geofence_id']) ? $CusConPlug->decryptdata($postDataArr['geofence_id']) : '';
            //  $data_date = (isset($postDataArr['data_date']) && !empty($postDataArr['data_date'])) ? date("Y-m-d", strtotime($utcDateTime)) : $today_date;
            $wgereGpsArr = array('geofence_id' => $geofence_id, "gpsudata.UserID" => $user_id, 'startDate' => $start, 'endDate' => $end);
            if (!$geofence_id) {
                $wgereGpsArr = array("hug.UserID" => $user_id, "hug.caregiver_id" => $caregiver_id, 'startDate' => $start, 'endDate' => $end);
            }
            $orderby = 'date DESC, time DESC';
            $geofenceUserData = $CareGeofenceTableObj->getGpsUserData($wgereGpsArr, array(), $orderby);
        }
//        echo '<pre>';
//        print_R($geofenceUserData);die;
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        return $viewModel->setVariables(array(
                    'pageTitle' => "View Beacons",
                    'session' => $this->_sessionObj->isCaregiverlogin,
                    'geofenceUserData' => $geofenceUserData,
                    'CusConPlug' => $CusConPlug,
                    'today_date' => $today_date_time,
                    'time_zone' => $time_zone
        ));
    }

}
