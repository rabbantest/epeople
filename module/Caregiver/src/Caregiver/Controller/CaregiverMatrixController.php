<?php

/*
  @ Used: This class is used to manage Threeshold Matrix In caregiver
  @ Created By: Shivendra Suman
  @ Created On: 02/04/2015
  @ Updated On: --/--/---
  @ Zend Framework (http://framework.zend.com/)
 */

namespace Caregiver\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class CaregiverMatrixController extends BaseController {

    protected $_sessionObj;

    public function __construct() {
        parent::__construct();
        $this->_sessionObj = new Container('Caregiver');
    }

    public function indexAction() {
        $userId = $this->_sessionObj->userDetails['UserID'];
        
        $request = $this->getRequest();

        $BodyMeasurementTable = $this->getServiceLocator()->get("CareBodyMeasurement");

        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if (empty($postDataArr['min_ht_feet']) && empty($postDataArr['min_ht_inches']) && empty($postDataArr['max_ht_feet']) && empty($postDataArr['max_ht_inches']) && empty($postDataArr['min_wast']) && empty($postDataArr['max_wast']) && empty($postDataArr['min_weight']) && empty($postDataArr['max_weight'])
            ) {
                $message = "Please fill the HEIGHT, WAIST and WEIGHT fields.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            } else {
                $minHeigthFeetTpCM = ($postDataArr['min_ht_feet'] * 30.48) + ($postDataArr['min_ht_inches'] * 2.54);
                $minHeigthInchTpCM = $postDataArr['min_ht_inches'] * 2.54;
                $maxHeigthFeetTpCM = ($postDataArr['max_ht_feet'] * 30.48) + ($postDataArr['max_ht_inches'] * 2.54);
                $maxHeigthInchTpCM = $postDataArr['max_ht_inches'] * 2.54;
                $minWeightInGram = $postDataArr['min_weight'] * 453.592;
                $maxWeightInGram = $postDataArr['max_weight'] * 453.592;
                if (!count($BodyMeasurementTable->getBodyMeasurement(array("status" => 1, "caregiver_id" => $userId), array("id")))) {
               //  echo $userId."insert";exit;
                    $data = array(
                        'caregiver_id' => $userId,
                        "min_height" => $minHeigthFeetTpCM,
                        "min_inch_height" => $minHeigthInchTpCM,
                        "max_height" => $maxHeigthFeetTpCM,
                        "max_inch_height" => $maxHeigthInchTpCM,
                        "min_wast" => $postDataArr['min_wast'],
                        "max_wast" => $postDataArr['max_wast'],
                        "min_weight" => $minWeightInGram,
                        "max_weight" => $maxWeightInGram,
                        "status" => 1,
                        "creation_date" => time(),
                    );
                    $insertData = $BodyMeasurementTable->insertBodyMeasurement($data);
                } else {
                   // echo $userId."update";exit;
                    $data = array(
                        'caregiver_id' => $userId,
                        "min_height" => $minHeigthFeetTpCM,
                        "min_inch_height" => $minHeigthInchTpCM,
                        "max_height" => $maxHeigthFeetTpCM,
                        "max_inch_height" => $maxHeigthInchTpCM,
                        "min_wast" => $postDataArr['min_wast'],
                        "max_wast" => $postDataArr['max_wast'],
                        "min_weight" => $minWeightInGram,
                        "max_weight" => $maxWeightInGram,
                        "updation_date" => time(),
                    );
//                    echo "<pre>";
//                    print_r($data);
//                    exit;
                    $whereArr = array("caregiver_id" => $userId, "status" => 1);
                    $insertData = $BodyMeasurementTable->updateBodyMeasurement($whereArr, $data);
                }
            }
        }

        $columns = array("id", "min_height", "min_inch_height", "max_height", "max_inch_height", "min_wast", "max_wast", "min_weight", "max_weight");
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $bodyMeasurementArr = $BodyMeasurementTable->getBodyMeasurement($whereArr, $columns);

        $EthnicityTable = $this->getServiceLocator()->get("EthnicityTable");
        $columns = array("ethnicity_id", "ethnicity_name");
        $whereArr = array();
        $ethnicityArr = $EthnicityTable->getEthnicity($whereArr, $columns);
//        echo "<pre>";
//        print_r($bodyMeasurementArr);
//        exit;
        $sendArr = array(
            'ethnicity' => $ethnicityArr,
            'session' => $this->_sessionObj->isCaregiverlogin,
            'bodyMeasurement' => $bodyMeasurementArr,
        );
        return new ViewModel($sendArr);
    }

    public function stepsAction() {

        $userId = $this->_sessionObj->userDetails['UserID'];
        $request = $this->getRequest();


        $sendArr = array(
            'session' => $this->_sessionObj->isCaregiverlogin,
        );
        return new ViewModel($sendArr);
    }

    public function phyactivityAction() {

        $userId = $this->_sessionObj->userDetails['UserID'];
        $request = $this->getRequest();


        $sendArr = array(
            'session' => $this->_sessionObj->isCaregiverlogin,
        );
        return new ViewModel($sendArr);
    }

    public function cholestrolAction() {

        $userId = $this->_sessionObj->userDetails['UserID'];
        $sendArr = array();
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $request = $this->getRequest();


        $CholestrolTable = $this->getServiceLocator()->get("CareCholestrol");
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if (!(empty($postDataArr['min_hld']) && empty($postDataArr['max_hld']) && empty($postDataArr['min_ldl']) && empty($postDataArr['max_ldl']) && empty($postDataArr['min_triglyc']) && empty($postDataArr['max_triglyc']))) {

                if ($postDataArr['min_hld'] < $postDataArr['max_hld']) {
                    if ($postDataArr['min_ldl'] < $postDataArr['max_ldl']) {
                        if ($postDataArr['min_triglyc'] < $postDataArr['max_triglyc']) {
                            if (count($CholestrolTable->getThresholdcholestrol($whereArr, array("id")))) {
                                $data = array(
                                    'min_hld' => $postDataArr['min_hld'],
                                    'max_hld' => $postDataArr['max_hld'],
                                    'min_ldl' => $postDataArr['min_ldl'],
                                    'max_ldl' => $postDataArr['max_ldl'],
                                    'min_triglyc' => $postDataArr['min_triglyc'],
                                    'max_triglyc' => $postDataArr['max_triglyc'],
                                    'status' => 1,
                                    'updation_date' => time(),
                                );
                                $insertData = $CholestrolTable->updateThresholdcholestrol(array("caregiver_id" => $userId, "status" => 1), $data);
                                $message = "The cholesterol successfully updated .";
                                $this->flashMessenger()->addMessage(array('success' => $message));
                            } else {
                                $data = array(
                                    "caregiver_id" => $userId,
                                    'min_hld' => $postDataArr['min_hld'],
                                    'max_hld' => $postDataArr['max_hld'],
                                    'min_ldl' => $postDataArr['min_ldl'],
                                    'max_ldl' => $postDataArr['max_ldl'],
                                    'min_triglyc' => $postDataArr['min_triglyc'],
                                    'max_triglyc' => $postDataArr['max_triglyc'],
                                    'status' => 1,
                                    'creation_date' => time(),
                                );
                                $insertData = $CholestrolTable->insertThresholdcholestrol($data);
                                $message = "The cholesterol successfully added .";
                                $this->flashMessenger()->addMessage(array('success' => $message));
                            }
                        } else {
                            $sendArr = array(
                                'viewData' => isset($postDataArr)?$postDataArr:'',
                                'session' => $this->_sessionObj->isCaregiverlogin,
                            );
                            $message = "Maximum triglyc must be greater then Minimum triglyc.";
                            $this->flashMessenger()->addMessage(array('error' => $message));
                        }
                    } else {
                        $sendArr = array(
                            'viewData' => isset($postDataArr)?$postDataArr:'',
                            'session' => $this->_sessionObj->isCaregiverlogin,
                        );
                        $message = "Maximum ldl must be greater then Minimum ldl.";
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                } else {
                    $sendArr = array(
                        'viewData' => isset($postDataArr)?$postDataArr:'',
                        'session' => $this->_sessionObj->isCaregiverlogin,
                    );
                    $message = "Maximum hld must be greater then Minimum hld.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            } else {
                $sendArr = array(
                    'viewData' => isset($postDataArr)?$postDataArr:'',
                    'session' => $this->_sessionObj->isCaregiverlogin,
                );
                $message = "All fields are mandatory! please enter the value.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }

        $columns = array("id", "min_hld", "max_hld", "min_ldl", "max_ldl", "min_triglyc", "max_triglyc");
        $cholestrolArr = $CholestrolTable->getThresholdcholestrol($whereArr, $columns);
        $returnData=array();
        if($cholestrolArr)
            $returnData=  isset ($cholestrolArr[0])?$cholestrolArr[0]:'';

        if (empty($sendArr)) {
            $sendArr = array(
                'viewData' => $returnData,
                'session' => $this->_sessionObj->isCaregiverlogin,
            );
        }
        return new ViewModel($sendArr);
    }

    public function bldpressureAction() {

        $userId = $this->_sessionObj->userDetails['UserID'];
        $request = $this->getRequest();


        $sendArr = array(
            'session' => $this->_sessionObj->isCaregiverlogin,
        );
        return new ViewModel($sendArr);
    }

    public function fastglucoseAction() {

        $userId = $this->_sessionObj->userDetails['UserID'];
        $sendArr = array();
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $request = $this->getRequest();

        $GlucoseTable = $this->getServiceLocator()->get("CareGlucose");
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if ($postDataArr['min_glucose'] != "" && $postDataArr['max_glucose'] != "") {
                if ($postDataArr['min_glucose'] < $postDataArr['max_glucose']) {
                    if (count($GlucoseTable->getThresholdGlucose($whereArr, array("id")))) {
                        $data = array(
                            'min_glucose' => $postDataArr['min_glucose'],
                            'max_glucose' => $postDataArr['max_glucose'],
                            'status' => 1,
                            'updation_date' => time(),
                        );
                        // echo '<pre>'; print_r($data); die;
                        $insertData = $GlucoseTable->updateThresholdGlucose(array("caregiver_id" => $userId, "status" => 1), $data);
                        $message = "The glucose successfully updated .";
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    } else {
                        $data = array(
                            "caregiver_id" => $userId,
                            'min_glucose' => $postDataArr['min_glucose'],
                            'max_glucose' => $postDataArr['max_glucose'],
                            'status' => 1,
                            'creation_date' => time(),
                        );
                        // echo '<pre>'; print_r($data); die;
                        $insertData = $GlucoseTable->insertThresholdGlucose($data);
                        $message = "The glucose successfully added .";
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    }
                } else {
                    $sendArr = array(
                        'viewData' => isset($postDataArr)?$postDataArr:'',
                        'session' => $this->_sessionObj->isCaregiverlogin,
                    );
                    $message = "Maximum glucose must be greater then Minimum glucose.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            } else {
                $sendArr = array(
                    'viewData' => isset($postDataArr)?$postDataArr:'',
                    'session' => $this->_sessionObj->isCaregiverlogin,
                );
                $message = "All fields are mandatory! please enter the value.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }

        $columns = array("id", "caregiver_id", "min_glucose", "max_glucose");
        $glucoseArr = $GlucoseTable->getThresholdGlucose($whereArr, $columns);

        if (empty($sendArr)) {
            $sendArr = array(
                'viewData' => isset($glucoseArr[0])?$glucoseArr[0]:'',
                'session' => $this->_sessionObj->isCaregiverlogin,
            );
        }
        return new ViewModel($sendArr);
    }

    public function tempAction() {

        $userId = $this->_sessionObj->userDetails['UserID'];
        $sendArr = array();
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $request = $this->getRequest();

        $TemperatureTable = $this->getServiceLocator()->get("CareTemp");
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if ($postDataArr['min_temp'] != "" && $postDataArr['max_temp'] != "") {
                if ($postDataArr['min_temp'] < $postDataArr['max_temp']) {
                    if (count($TemperatureTable->getThresholdTemperature($whereArr, array("id")))) {
                        $data = array(
                            'min_temp' => $postDataArr['min_temp'],
                            'max_temp' => $postDataArr['max_temp'],
                            'status' => 1,
                            'updation_date' => time(),
                        );
                        // echo '<pre>'; print_r($data); die;
                        $insertData = $TemperatureTable->updateThresholdTemperature(array("caregiver_id" => $userId, "status" => 1), $data);
                        $message = "The Temperature successfully updated .";
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    } else {
                        $data = array(
                            "caregiver_id" => $userId,
                            'min_temp' => $postDataArr['min_temp'],
                            'max_temp' => $postDataArr['max_temp'],
                            'status' => 1,
                            'creation_date' => time(),
                        );
                        // echo '<pre>'; print_r($data); die;
                        $insertData = $TemperatureTable->insertThresholdTemperature($data);
                        $message = "The Temperature successfully added .";
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    }
                } else {
                    $sendArr = array(
                        'viewData' => isset($postDataArr)?$postDataArr:'',
                        'session' => $this->_sessionObj->isCaregiverlogin,
                    );
                    $message = "Maximum Temperature must be greater then Minimum Temperature.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            } else {
                $sendArr = array(
                    'viewData' => isset($postDataArr)?$postDataArr:'',
                    'session' => $this->_sessionObj->isCaregiverlogin,
                );
                $message = "All fields are mandatory! please enter the value.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }

        $columns = array("id", "caregiver_id", "min_temp", "max_temp");
        $temperatureArr = $TemperatureTable->getThresholdTemperature($whereArr, $columns);

        if (empty($sendArr)) {
            $sendArr = array(
                'viewData' => isset($temperatureArr[0])?$temperatureArr[0]:'',
                'session' => $this->_sessionObj->isCaregiverlogin,
            );
        }
        return new ViewModel($sendArr);
    }

    public function sleepAction() {

        $userId = $this->_sessionObj->userDetails['UserID'];

        $sendArr = array();
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $request = $this->getRequest();

        $SleepTable = $this->getServiceLocator()->get("CareSleep");
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if ($postDataArr['min_sleep'] != "" && $postDataArr['max_sleep'] != "") {
                if ($postDataArr['min_sleep'] < $postDataArr['max_sleep']) {
                    if (count($SleepTable->getThresholdSleep($whereArr, array("id")))) {
                        $data = array(
                            'min_sleep' => $postDataArr['min_sleep'],
                            'max_sleep' => $postDataArr['max_sleep'],
                            'status' => 1,
                            'updation_date' => time(),
                        );
                        // echo '<pre>'; print_r($data); die;
                        $insertData = $SleepTable->updateThresholdSleep(array("caregiver_id" => $userId, "status" => 1), $data);
                        $message = "The sleep successfully updated .";
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    } else {
                        $data = array(
                            "caregiver_id" => $userId,
                            'min_sleep' => $postDataArr['min_sleep'],
                            'max_sleep' => $postDataArr['max_sleep'],
                            'status' => 1,
                            'creation_date' => time(),
                        );
                        // echo '<pre>'; print_r($data); die;
                        $insertData = $SleepTable->insertThresholdSleep($data);
                        $message = "The sleep successfully added .";
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    }
                } else {
                    $sendArr = array(
                        'viewData' => isset($postDataArr)?$postDataArr:'',
                        'session' => $this->_sessionObj->isCaregiverlogin,
                    );
                    $message = "Maximum sleep must be greater then Minimum sleep.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            } else {
                $sendArr = array(
                    'viewData' => isset($postDataArr)?$postDataArr:'',
                    'session' => $this->_sessionObj->isCaregiverlogin,
                );
                $message = "All fields are mandatory! please enter the value.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }

        $columns = array("id", "caregiver_id", "min_sleep", "max_sleep");
        $sleepArr = $SleepTable->getThresholdSleep($whereArr, $columns);

        if (empty($sendArr)) {
            $sendArr = array(
                'viewData' => isset($sleepArr[0])?$sleepArr[0]:'',
                'session' => $this->_sessionObj->isCaregiverlogin,
            );
        }
        return new ViewModel($sendArr);
    }

    protected function bmicalculationAction() {
        $postData = $_POST;
        $userId = $this->_sessionObj->userDetails['UserID'];
        // $postData = $this->getResponse();
        $BmiTable = $this->getServiceLocator()->get("CareBmi");
        if ($postData['ethnicity'] != "" && $postData['gender'] != "") {
            // CHECKING EXISTING BMI.
            $whereArr = array(
                "caregiver_id" => $userId,
                "ethnicity_id" => $postData['ethnicity'],
                "gender" => $postData['gender'],
                "status" => 1
            );
            if (!count($BmiTable->getThresholdBmi($whereArr, array("id")))) {
                $data = array(
                    "caregiver_id" => $userId,
                    "ethnicity_id" => $postData['ethnicity'],
                    "gender" => $postData['gender'],
                    "min_bmi" => $postData['min_bmi'],
                    "max_bmi" => $postData['max_bmi'],
                    "status" => 1,
                    "creation_date" => time(),
                );
                $insertData = $BmiTable->insertThresholdBmi($data);
            } else {
                // echo "You have already entered the bmi for this ethnicity and gender! try again.";
                die;
            }
        }

        $columns = array("id", "ethnicity_id", "gender", "min_bmi", "max_bmi");
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $bmiSrr = $BmiTable->getThresholdBmi($whereArr, $columns, "true");


        $sendArr = array(
            'bmis' => $bmiSrr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function distancemanageAction() {
        $postData = $_POST;
        $userId = $this->_sessionObj->userDetails['UserID'];
        $DistanceTable = $this->getServiceLocator()->get("CareDistance");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("distance", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "caregiver_id" => $userId,
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_distance" => $postData['min_distance'],
                    "max_distance" => $postData['max_distance'],
                    "status" => 1,
                    "creation_date" => time(),
                );
                $insertData = $DistanceTable->insertThresholdDistance($data);
            } else {
                //echo "Age range overlapping, please select proper agers.";
                die;
            }
        }
        $columns = array("id", "gender", "age_from", "age_to", "min_distance", "max_distance");
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $distanceArr = $DistanceTable->getThresholdDistance($whereArr, $columns);

        $sendArr = array(
            'distance' => $distanceArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function caloriemanageAction() {
        $postData = $_POST;
        $userId = $this->_sessionObj->userDetails['UserID'];
        $CalorieTable = $this->getServiceLocator()->get("CareCalorie");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("calo", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "caregiver_id" => $userId,
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_calorie" => $postData['min_calorie'],
                    "max_calorie" => $postData['max_calorie'],
                    "status" => 1,
                    "creation_date" => time(),
                );
                $insertData = $CalorieTable->insertThresholdCalorie($data);
            } else {
                // echo "Age range overlapping, please select proper ages.";
                die;
            }
        }

        $columns = array("id", "gender", "age_from", "age_to", "min_calorie", "max_calorie");
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $calorieArr = $CalorieTable->getThresholdCalorie($whereArr, $columns);

        $sendArr = array(
            'calorie' => $calorieArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function timemanageAction() {
        $postData = $_POST;
        $userId = $this->_sessionObj->userDetails['UserID'];
        $TimeTable = $this->getServiceLocator()->get("CareTime");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("time", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "caregiver_id" => $userId,
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_time" => $postData['min_time'],
                    "max_time" => $postData['max_time'],
                    "status" => 1,
                    "creation_date" => time(),
                );
                $insertData = $TimeTable->insertThresholdTime($data);
            } else {
                // echo "Age range overlapping, please select proper ages.";
                die;
            }
        }

        $columns = array("id", "gender", "age_from", "age_to", "min_time", "max_time");
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $timeArr = $TimeTable->getThresholdTime($whereArr, $columns);

        $sendArr = array(
            'time' => $timeArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function systolicmanageAction() {
        $postData = $_POST;
        $userId = $this->_sessionObj->userDetails['UserID'];
        $SystolicTable = $this->getServiceLocator()->get("CareSystolic");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("syst", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "caregiver_id" => $userId,
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_systolic" => $postData['min_systolic'],
                    "max_systolic" => $postData['max_systolic'],
                    "status" => 1,
                    "creation_date" => time(),
                );
                $insertData = $SystolicTable->insertThresholdSystolic($data);
            } else {
                // echo "Age range overlapping, please select proper ages.";
                die;
            }
        }

        $columns = array("id", "gender", "age_from", "age_to", "min_systolic", "max_systolic");
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $systolicArr = $SystolicTable->getThresholdSystolic($whereArr, $columns);

        $sendArr = array(
            'systolic' => $systolicArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function diastolicmanageAction() {
        $postData = $_POST;
        $userId = $this->_sessionObj->userDetails['UserID'];
        $DiastolicTable = $this->getServiceLocator()->get("CareDiastolic");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("dias", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "caregiver_id" => $userId,
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_diastolic" => $postData['min_diastolic'],
                    "max_diastolic" => $postData['max_diastolic'],
                    "status" => 1,
                    "creation_date" => time(),
                );
                $insertData = $DiastolicTable->insertThresholdDiastolic($data);
            } else {
                // echo "Age range overlapping, please select proper ages.";
                die;
            }
        }

        $columns = array("id", "gender", "age_from", "age_to", "min_diastolic", "max_diastolic");
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $diastolicArr = $DiastolicTable->getThresholdDiastolic($whereArr, $columns);

        $sendArr = array(
            'diastolic' => $diastolicArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function pulsemanageAction() {
        $postData = $_POST;
        $userId = $this->_sessionObj->userDetails['UserID'];
        $PulseTable = $this->getServiceLocator()->get("CarePulse");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("pulse", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "caregiver_id" => $userId,
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_pulse" => $postData['min_pulse'],
                    "max_pulse" => $postData['max_pulse'],
                    "status" => 1,
                    "creation_date" => time(),
                );
                $insertData = $PulseTable->insertThresholdPulse($data);
            } else {
                // echo "Age range overlapping, please select proper ages.";
                die;
            }
        }

        $columns = array("id", "gender", "age_from", "age_to", "min_pulse", "max_pulse");
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $pulseArr = $PulseTable->getThresholdPulse($whereArr, $columns);

        $sendArr = array(
            'pulse' => $pulseArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function managerecordsAction() {
        $postData = $_POST;
        $userId = $this->_sessionObj->userDetails['UserID'];
        if ($postData['id'] != "") {
            $whereArr = array("id" => $postData['id'], "caregiver_id" => $userId);
            if ($postData['type'] == "bmi") {
                $BmiTable = $this->getServiceLocator()->get("CareBmi");
                $BmiTable->deleteThresholdBmi($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "step") {
                $StepsTable = $this->getServiceLocator()->get("CareSteps");
                $StepsTable->deleteThresholdSteps($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "time") {
                $TimeTable = $this->getServiceLocator()->get("CareTime");
                $TimeTable->deleteThresholdTime($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "calorie") {
                $CalorieTable = $this->getServiceLocator()->get("CareCalorie");
                $CalorieTable->deleteThresholdCalorie($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "distance") {
                $DistanceTable = $this->getServiceLocator()->get("CareDistance");
                $DistanceTable->deleteThresholdDistance($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "pulse") {
                $PulseTable = $this->getServiceLocator()->get("CarePulse");
                $PulseTable->deleteThresholdPulse($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "systo") {
                $SystolicTable = $this->getServiceLocator()->get("CareSystolic");
                $SystolicTable->deleteThresholdSystolic($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "diasto") {
                $DiastolicTable = $this->getServiceLocator()->get("CareDiastolic");
                $DiastolicTable->deleteThresholdDiastolic($whereArr);
                echo 1;
                die;
            }
        }
    }

    protected function stepsmanageAction() {
        $postData = $_POST;
        $userId = $this->_sessionObj->userDetails['UserID'];
        $StepsTable = $this->getServiceLocator()->get("CareSteps");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("steps", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "caregiver_id" => $userId,
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_steps" => $postData['min_steps'] / 1000,
                    "max_steps" => $postData['max_steps'] / 1000,
                    "status" => 1,
                    "creation_date" => time(),
                );
                $insertData = $StepsTable->insertThresholdSteps($data);
            } else {
                // echo "Age range overlapping, please select proper agers.";
                die;
            }
        }

        $columns = array("id", "gender", "age_from", "age_to", "min_steps", "max_steps");
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        $stepsSrr = $StepsTable->getThresholdSteps($whereArr, $columns);

        $sendArr = array(
            'steps' => $stepsSrr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    private function checkAgeValidation($mode, $age_from, $age_to, $gender) {
        $result = 1;
        $userId = $this->_sessionObj->userDetails['UserID'];
        $whereArr = array("status" => 1, "caregiver_id" => $userId);
        if ($mode == "steps") {
            $StepsTable = $this->getServiceLocator()->get("CareSteps");
            $existArr = $StepsTable->getThresholdSteps($whereArr, array("age_from", "age_to", "gender"));
        }
        if ($mode == "distance") {
            $DistanceTable = $this->getServiceLocator()->get("CareDistance");
            $existArr = $DistanceTable->getThresholdDistance($whereArr, array("age_from", "age_to", "gender"));
        }
        if ($mode == "calo") {
            $CalorieTable = $this->getServiceLocator()->get("CareCalorie");
            $existArr = $CalorieTable->getThresholdCalorie($whereArr, array("age_from", "age_to", "gender"));
        }
        if ($mode == "time") {
            $TimeTable = $this->getServiceLocator()->get("CareTime");
            $existArr = $TimeTable->getThresholdTime($whereArr, array("age_from", "age_to", "gender"));
        }
        if ($mode == "syst") {
            $SystolicTable = $this->getServiceLocator()->get("CareSystolic");
            $existArr = $SystolicTable->getThresholdSystolic($whereArr, array("age_from", "age_to", "gender"));
        }
        if ($mode == "dias") {
            $DiastolicTable = $this->getServiceLocator()->get("CareDiastolic");
            $existArr = $DiastolicTable->getThresholdDiastolic($whereArr, array("age_from", "age_to", "gender"));
        }

        if ($mode == "pulse") {
            $PulseTable = $this->getServiceLocator()->get("CarePulse");
            $existArr = $PulseTable->getThresholdPulse($whereArr, array("age_from", "age_to", "gender"));
        }

        foreach ($existArr AS $valueArr) {
            $start = $valueArr['age_from'];
            $end = $valueArr['age_to'];
            $genderExist = $valueArr['gender'];
            for ($start; $end >= $start; $start++) {
                if ($start == $age_from && $gender == $genderExist) {
                    $result = 0;
                    break;
                }
                if ($start == $age_to && $gender == $genderExist) {
                    $result = 0;
                    break;
                }
            }
            if (!$result)
                break;
        }
        return $result;
    }

}
