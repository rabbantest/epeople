<?php

namespace Caregiver\Controller;

/*
  @ Used: This class is used to manage Imunization
  @ Created By: Rabban Ahmad
  @ Created On: 22/01/2015
  @ Updated On: 22/01/2015
  @ Zend Framework (http://framework.zend.com/)
 */

use Caregiver\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class ProfileController extends BaseController {

    public function __construct() {
        $this->_sessionObj = new Container('Caregiver');
    }

    /*
     * @this is for individual basic  setting 
     * @ Auth: Rabban Ahmad
     * Date: 22-01-2015
     */

    public function indexAction() {

        $userId = $this->_sessionObj->userDetails['UserID'];
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $whereArray = array('UserID' => $userId);
        $userDetails = $IndUserTableObj->getUser($whereArray);

        $request = $this->getRequest();
        $dataArr = array();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $dataArr['FirstName'] = $postDataArr['firstname'];
            $dataArr['MiddleName'] = $postDataArr['middlename'];
            $dataArr['LastName'] = $postDataArr['lastname'];
            $dataArr['NickName'] = $postDataArr['nickname'];
            $dataArr['short_description'] = $postDataArr['description'];
            $dob = date('Y-m-d', strtotime($postDataArr['dob']));
            $dataArr['Dob'] = $dob;
            $dataArr['Gender'] = $postDataArr['gender'];

            $updateBasic = $IndUserTableObj->updateUser(array('UserID' => $userId), $dataArr);
            if ($updateBasic) {
                $this->_sessionObj->userDetails['FirstName'] = $postDataArr['firstname'];
                $this->_sessionObj->userDetails['LastName'] = $postDataArr['lastname'];
                $message = "Profile basic information updated successfully.";
                $this->flashMessenger()->addMessage(array('success' => $message));
                return $this->redirect()->toRoute('cg_basic_profile');
            } else {
                $message = "some error occured please try again later.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('cg_basic_profile');
            }
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Profile Settings',
                    'basicDetails' => $userDetails,
        ));
    }

    /*
     * @this is for individual contact  setting 
     * @ Auth: Rabban Ahmad
     * Date: 27-01-2015
     */

    public function contactinfoAction() {

        $userId = $this->_sessionObj->userDetails['UserID'];
        $IndContTableObj = $this->getServiceLocator()->get("ContactInfoTable");
        $CountryTableObj = $this->getServiceLocator()->get("CountryTable");
        $IndStateTableObj = $this->getServiceLocator()->get("IndStateTable");
        $CountyTableObj = $this->getServiceLocator()->get("CountyTable");
        $request = $this->getRequest();
        $dataArr = array();
        $countryArr = $CountryTableObj->getCountry();


        $whereArray = array('user_id' => $userId);
        $userContDetails = $IndContTableObj->getUserContactInfo($whereArray, array(), 1);
//        echo '<pre>';
//        print_r($userContDetails);die;
        if (isset($userContDetails[0]['country_id']) && !empty($userContDetails[0]['country_id'])) {
            $whereStateArr = array("country_id" => $userContDetails[0]['country_id']);
            $states = $IndStateTableObj->getStates($whereStateArr);
        } else {
            $states = array();
        }
        if (isset($userContDetails[0]['state_id']) && !empty($userContDetails[0]['state_id'])) {
            $whereCountyArr = array("state_id" => $userContDetails[0]['state_id']);
            $county = $CountyTableObj->getCounty($whereCountyArr);
        } else {
            $county = array();
        }

        //user current country name 
        if (isset($userContDetails[0]['country_id']) && !empty($userContDetails[0]['country_id'])) {
            $whereCountrySingleArr = array("country_id" => $userContDetails[0]['country_id']);
            $country_current = $CountryTableObj->getCountry($whereCountrySingleArr);
            $userCountry = isset($country_current[0]['country_name']) ? $country_current[0]['country_name'] : "";
        } else {
            $userCountry = '';
        }
        //user current state name 
        if (isset($userContDetails[0]['state_id']) && !empty($userContDetails[0]['state_id'])) {

            $whereStateSingleArr = array("state_id" => $userContDetails[0]['state_id']);
            $states_current = $IndStateTableObj->getStates($whereStateSingleArr);
            $UserState = $states_current[0]['state_name'];
        } else {
            $UserState = '';
        }

        //user current county name 
        if (isset($userContDetails[0]['county_id']) && !empty($userContDetails[0]['county_id'])) {
            $whereCountySingleArr = array("county_id" => $userContDetails[0]['county_id']);
            $county_current = $CountyTableObj->getCounty($whereCountySingleArr);
            $userCounty = $county_current[0]['county_name'];
        } else {
            $userCounty = '';
        }
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $dataArr['user_id'] = $userId;
            $dataArr['street_address1'] = $postDataArr['address1'];
            $dataArr['street_address2'] = $postDataArr['address2'];
            $dataArr['city'] = $postDataArr['city'];
            $dataArr['state_id'] = $postDataArr['state'];
            $dataArr['country_id'] = $postDataArr['country'];
            $dataArr['phone_no'] = $postDataArr['phone_number'];
            $dataArr['county_id'] = $postDataArr['county'];
            $dataArr['postal_code'] = $postDataArr['postal_code'];
            $dataArr['ethnicity'] = $postDataArr['ethnicity'];
            $dataArr['origin_id'] = $postDataArr['origin'];
            $contactdata = $IndContTableObj->getUserContactInfo(array("user_id" => $userId));
            if (count($contactdata) > 0) {
                $contactUpdate = $IndContTableObj->updateContact(array('user_id' => $userId), $dataArr);
            } else {
                $IndContTableObj->save($dataArr);
                $contactUpdate = true;
            }
            if ($contactUpdate) {
                $message = "Profile contact information updated successfully.";
                $this->flashMessenger()->addMessage(array('success' => $message));
                return $this->redirect()->toRoute('cg_contact_info');
            } else {
                $message = "some error occured please try again later.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('cg_contact_info');
            }
        }


        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Profile Settings',
                    'user_country' => $userCountry,
                    'user_state' => $UserState,
                    'user_county' => $userCounty,
                    'contactDetails' => $userContDetails[0],
                    'country' => $countryArr,
                    'states' => $states,
                    'county' => $county
        ));
    }

    /*
     * @this is for individual profile  setting 
     * @ Auth: Rabban Ahmad
     * Date: 27-01-2015
     */

    public function profileinfoAction() {

        $userId = $this->_sessionObj->userDetails['UserID'];
        $DomainID = $this->_sessionObj->userDetails['DomainID'];
        $IndProfileTableObj = $this->getServiceLocator()->get("ProfileInfoTable");
        $UserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $request = $this->getRequest();

        $whereArray = array('UserID' => $userId);
        $userDetails = $UserTableObj->getUser($whereArray);


        $whereProfileArray = array('user_id' => $userId);
        $userProfileDetails = $IndProfileTableObj->getUserProfileInfo($whereProfileArray);

        $LanguageArr = $UserTableObj->getLanguage();
        $BloodTypeArr = $UserTableObj->getBloodType();
        $EthnicityArr = $UserTableObj->getEthnicity();
        $EducationcArr = $UserTableObj->getEducation();

        //select current language of user 
        if (isset($userProfileDetails[0]['language']) && !empty($userProfileDetails[0]['language'])) {
            $whereLangArray = array("laguage_id" => $userProfileDetails[0]['language']);
            $language_current = $UserTableObj->getLanguage($whereLangArray);
            $userLanguage = $language_current[0]['language'];
        } else {
            $userLanguage = '';
        }

        //select current Ethnicity of user
        if (isset($userProfileDetails[0]['ethnicity']) && !empty($userProfileDetails[0]['ethnicity'])) {

            $whereEthArray = array("ethnicity_id" => $userProfileDetails[0]['ethnicity']);
            $ethnicity_current = $UserTableObj->getEthnicity($whereEthArray);
            $userEthnicity = $ethnicity_current[0]['ethnicity_name'];
        } else {
            $userEthnicity = '';
        }

        //select current education of user 
        if (isset($userProfileDetails[0]['education_degree']) && !empty($userProfileDetails[0]['education_degree'])) {

            $whereEducationArray = array("education_id" => $userProfileDetails[0]['education_degree']);
            $education_current = $UserTableObj->getEducation($whereEducationArray);
            $userEducation = $education_current[0]['highest_degree_name'];
        } else {
            $userEducation = '';
        }
        $typeidArr = json_decode($userDetails[0]['UserTypeId'], true);
        $typeIds = $typeidArr[$DomainID];
        $isResercher = 0;
        $isCaregiver = 0;
        if (in_array(2, $typeIds)) {
            $isCaregiver = 1;
        }if (in_array(3, $typeIds)) {
            $isResercher = 1;
        }

        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $dataArr['user_id'] = $userId;
            if (in_array(3, $typeIds)) {
                $dataArr['institute_name'] = $postDataArr['institute'];
                $dataArr['department'] = $postDataArr['depratment'];
                $dataArr['dept_phone'] = $postDataArr['dept_phone'];
                $dataArr['work_phone'] = $postDataArr['work_phone'];
                $dataArr['dept_head_name'] = $postDataArr['dept_head_name'];
            }
            if (in_array(2, $typeIds)) {
                $dataArr['practice_name'] = $postDataArr['practice'];
            }
            $dataArr['education_degree'] = $postDataArr['education'];
            //$dataArr['ethnicity'] = $postDataArr['ethnicity'];
            $dataArr['income'] = $postDataArr['income'];
            $dataArr['language'] = $postDataArr['language'];
            //$dataArr['blood_group']=$postDataArr['blood'];
            if (!isset($postDataArr['marital_status'])) {
                $marital_status = "m";
            } else {
                $marital_status = $postDataArr['marital_status'];
            }
            $dataArr['maritial_status'] = $marital_status;
            $dataArr['CreationDate'] = date("Y-m-d");
            $dataArr['blood_group'] = $postDataArr['blood_type'];

            /* start code for checking mime type */
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime_type = finfo_file($finfo, $_FILES['profile_pic']['tmp_name']);
            if (isset($_FILES['profile_pic']['tmp_name']) && !empty($_FILES['profile_pic']['tmp_name'])) {
                $filetype = array("image/gif", "image/jpg", "image/jpeg", "image/png", "image/GIF", "image/JPG", "image/JPEG", "image/PNG");
                if (!in_array($mime_type, $filetype)) {
                    $message = "please select valid image.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                    return $this->redirect()->toRoute('cg_profile_info');
                }
            }
            finfo_close($finfo);
            /* end code for checking mime type */
            $profiledata = $IndProfileTableObj->getUserProfileInfo(array("user_id" => $userId));
            if (count($profiledata) > 0) {
                $updateProfile = $IndProfileTableObj->updateUser(array('user_id' => $userId), $dataArr);
            } else {
                $IndProfileTableObj->save($dataArr);
                $updateProfile = true;
            }
            $updateImage = true;
            $udpatedData = array();
            if ($_FILES["profile_pic"]["size"] > 100) {
                $udpatedData['Image'] = '';
                if ($_FILES['profile_pic']['tmp_name']) {
                    $fileName = $_FILES['profile_pic']['name'];
                    $tmp_name = $_FILES['profile_pic']['tmp_name'];
                    $dir = $_SERVER['DOCUMENT_ROOT'] . "/upload_image/";
                    mkdir($dir . $userId, 777);
                    chmod($dir . $userId, 0777);
                    $newfilename = md5(rand() * time());
                    $file_name = strtolower(basename($fileName));
                    $ext = substr($file_name, strrpos($file_name, '.') + 1);
                    $ext = "." . $ext;
                    $returnName = $newfilename . $ext;
                    $newname = $dir . $userId . "/" . $newfilename . $ext;

                    move_uploaded_file($tmp_name, $newname);
                    $udpatedData['Image'] = $returnName;
                }

                $updateImage = $UserTableObj->updateUser(array('UserID' => $userId), $udpatedData);
                if ($updateImage) {
                    $this->_sessionObj->userDetails['Image'] = $returnName;
                }
            }
            if ($updateProfile || $updateImage) {
                $message = "Profile  information updated successfully.";
                $this->flashMessenger()->addMessage(array('success' => $message));
                return $this->redirect()->toRoute('cg_profile_info');
            } else {
                $message = "some error occured please try again later.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('cg_profile_info');
            }
        }
        $IncomeArr = $UserTableObj->getIncomeData();
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Profile Settings',
                    'userId' => $userId,
                    'userImage' => (isset($userDetails[0]['Image']) ? $userDetails[0]['Image'] : ""),
                    'userLanguage' => $userLanguage,
                    'userEthnicity' => $userEthnicity,
                    'userEducation' => $userEducation,
                    'profileDetails' => $userProfileDetails,
                    'language' => $LanguageArr,
                    'education' => $EducationcArr,
                    'IncomeArr' => $IncomeArr,
                    'bloodtype' => $BloodTypeArr,
                    'isCaregiver' => $isCaregiver,
                    'isResearcher' => $isResercher
        ));
    }

}
