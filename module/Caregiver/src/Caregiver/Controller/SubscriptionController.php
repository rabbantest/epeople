<?php

namespace Caregiver\Controller;

/* * ***********************
 * PAGE: USE TO MANAGE SUBSCRIPTION IN CAREGIVER.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: RABBAN AHMAD.
 * CREATOR: 29/04/2015
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\Plugin\Redirect;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class SubscriptionController extends AbstractActionController {

    //protected $_sessionObj;

    public function __construct() {
        $this->_sessionObj = new Container('Caregiver');
    }

    public function indexAction() {
        $CareSubscriptionObj = $this->getServiceLocator()->get("CareSubscriptionObj");
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $where = array('subs.type' => 1, 'subs.status' => 1);
        $columns = array('noOfUsers' => 'user', 'price', 'id');
        $subscriptionRes = $CareSubscriptionObj->getSubscriptionDetails($where, $columns);
        $Host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'];
        $paymentParams = array('rm' => 2, 'cmd' => '_xclick', 'business' => 'membership@quantextualhealth.com', 'return' => $Host . '/cg-subs-billing'
            , 'cancel_return' => $Host . '/cg-subscription', 'notify' => $Host . '/cg-subs-notify', 'item_name' => 'Healthmetrix Payment', 'currency_code' => 'USD', 'custom' => $UserID);
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Caregiver', 'subscriptionRes' => $subscriptionRes, 'paymentParams' => $paymentParams
        ));
    }

    public function billingAction() {
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $CareSubscriptionObj = $this->getServiceLocator()->get("CareSubscriptionObj");
        $txn = $_POST;
        $success = '';
        if ($txn) {
            $txn_id = $txn['txn_id'];
            $payment_status = $txn['payment_status'];
            $custom = $txn['custom'];
            $CareSubscriptionObj = $this->getServiceLocator()->get("CareSubscriptionObj");
            $where = array('hash_code' => $custom);
            $UpdateData = array('transaction_id' => $txn_id, 'payment_status' => $payment_status, 'status' => 1);
            $CareSubscriptionObj->UpdatePaymentDetails($where, $UpdateData);
            $success = 1;
        }
        $whereHistory = array('user_id' => $UserID,'payment_by'=>1);
        $result = $CareSubscriptionObj->getBillingHistory($whereHistory);
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Caregiver', 'success' => $success, 'history' => $result
        ));
    }

    public function notifyAction() {
        $txn = (object) $_POST;
        $plugin = $this->CustomControllerPlugin();
        //$plugin->senInvitationMail('rabban.ahmad@gmail.com', 'notifydata', json_encode($txn));
        exit;
    }

    public function crrstatusAction() {
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $CareGiverTableObj = $this->getServiceLocator()->get("IndividualCaregiverTable");
        $CareSubscriptionObj = $this->getServiceLocator()->get("CareSubscriptionObj");
        $where = array('pms.user_id' => $UserID, 'spm.type' => 1);
        $results = $CareSubscriptionObj->getLastSubscriptionStatus($where, array('amount', 'end_date'));
        $whereC = array('care.caregiver_id' => $UserID, 'care.status' => 1);
        $amount = '';
        $NextBillingDate = '';
        $NoOfUsers = '';
        if (isset($results[0]['amount'])) {
            $amount = $results[0]['amount'];
        }
        if (isset($results[0]['end_date'])) {
            $NextBillingDate = $results[0]['end_date'];
        }
        if (isset($results[0]['NoOfUsers'])) {
            $NoOfUsers = $results[0]['NoOfUsers'];
        }

        $res = $CareGiverTableObj->getCareGiverCount($whereC);
        $returnArray = array('userCount' => $res, 'amount' => $amount, 'NextBillingDate' => $NextBillingDate,
            'userPlan' => $NoOfUsers);
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Caregiver', 'result' => $returnArray
        ));
    }

}
