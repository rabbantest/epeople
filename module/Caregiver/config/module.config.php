<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE MODULE CONFIG FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rabban Ahmad
 * CREATOR: 12/11/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */
return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'index',
                    ),
                )
            ),
            // using the path /application/:controller/:action
            'caregiver' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/caregiver',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Caregiver\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
            'cg-login' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-login',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'login',
                    ),
                ),
            ),
            /* For home or dashboard page for caregiver */
            'cg_dash' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-dashboard',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'dashboard',
                    ),
                ),
            ),
            /* For getting caregiver profile req info */
            'cg_getpro' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-getprofileinfo',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'getprofileinfo',
                    ),
                ),
            ),
            /* For dashboard page for pending caregiver request */
            'cg_dash_pending' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-pending-request',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'pendingrequest',
                    ),
                ),
            ),
            /* For dashboard page for pending caregiver request */
            'cg_individual-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-individual-list',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'listindividual',
                    ),
                ),
            ),
            /* For dashboard page for pending caregiver profile */
            'cg_basic_profile' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-basic-info',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Profile',
                        'action' => 'index',
                    ),
                ),
            ),
            /* For dashboard page for pending caregiver profile */
            'cg_contact_info' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-contact-info',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Profile',
                        'action' => 'contactinfo',
                    ),
                ),
            ),
            /* For dashboard page for pending caregiver profile */
            'cg_profile_info' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-profile-info',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Profile',
                        'action' => 'profileinfo',
                    ),
                ),
            ),
            /* For dashboard page for pending caregiver profile */
            'cg_user_dashboard' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/cg-user-dashboard[/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Userdashboard',
                        'action' => 'index',
                    ),
                ),
            ),
            /* For dashboard page for basic Individual profile */
            'cg_userprofile_info' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/cg-user-profile[/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Userprofile',
                        'action' => 'index',
                    ),
                ),
            ),
            /* For dashboard page for contact Individual profile */
            'cg_usercontact_info' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/cg-usercontact-profile[/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Userprofile',
                        'action' => 'contactinfo',
                    ),
                ),
            ),
            /* For dashboard page for Individual profile */
            'cg_indprofile_info' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/cg-userprofile[/:id]',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Userprofile',
                        'action' => 'profileinfo',
                    ),
                ),
            ),
            /* For caregiver bodymeasurment threeshold matrix */
            'cg_bodymeas' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-bodymeasurment',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\CaregiverMatrix',
                        'action' => 'index',
                    ),
                ),
            ),
            /* For caregiver step and commut threeshold matrix */
            'cg_steps&comm' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-steps&comm',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\CaregiverMatrix',
                        'action' => 'steps',
                    ),
                ),
            ),
            'cg_physicalactivity' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-physical+activity',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\CaregiverMatrix',
                        'action' => 'phyactivity',
                    ),
                ),
            ),
            'cg_temp' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-cgtempass',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'cgtempass',
                    ),
                ),
            ),
            'cg_congrates' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-cgcongrates',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'cgcongrates',
                    ),
                ),
            ),
            'cg_basic' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-cgbasicinfo',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'cgbasicinfo',
                    ),
                ),
            ),
            'cg_contactinfo' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-cgcontactinfo',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'cgcontactinfo',
                    ),
                ),
            ),
            'cg_cgprofileinfo' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-cgprofileinfo',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'cgprofileinfo',
                    ),
                ),
            ),
            'cg_cgprofileinfo' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-advancesearch',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Index',
                        'action' => 'advancesearch',
                    ),
                ),
            ),
            'cg_Cholestrol' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-cholestrol',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\CaregiverMatrix',
                        'action' => 'cholestrol',
                    ),
                ),
            ),
            'cg_BldPressure' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-blood+pressure',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\CaregiverMatrix',
                        'action' => 'bldpressure',
                    ),
                ),
            ),
            'cg_FastGlucose' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-fasting+glucose',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\CaregiverMatrix',
                        'action' => 'fastglucose',
                    ),
                ),
            ),
            'cg_Temperature' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-temperature',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\CaregiverMatrix',
                        'action' => 'temp',
                    ),
                ),
            ),
            'cg_Sleep' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-sleep',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\CaregiverMatrix',
                        'action' => 'sleep',
                    ),
                ),
            ),
            'cg_listbecon' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/cg-list-becones[/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Trace',
                        'action' => 'index',
                    ),
                ),
            ),
            'cg_trackmove' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/cg-track-moves[/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Trace',
                        'action' => 'trackmove',
                    ),
                ),
            ),
            'cg_setbeconerule' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/cg-beacon-rule[/:id][/:beaconid]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Trace',
                        'action' => 'setrule',
                    ),
                ),
            ),
            'cg_followgps' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/cg-followongps[/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Trace',
                        'action' => 'followongps',
                    ),
                ),
            ),
            'cg_followongpsadd' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/cg-followongps-add[/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Trace',
                        'action' => 'followongpsadd',
                    ),
                ),
            ),
            'cg_followongpsedit' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/cg-followongps-edit[/:id][/:gpsid]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Trace',
                        'action' => 'followongpsedit',
                    ),
                ),
            ),
            /***********Routing start for Subscrioptin here Created : 29/04/15************* */
            'cg_subscrip' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-subscription',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Subscription',
                        'action' => 'index',
                    ),
                ),
            ),
            'cg_subsc_billing' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-subs-billing',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Subscription',
                        'action' => 'billing',
                    ),
                ),
            ),
            'cg_subsc_crrstatus' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-subs-crrstatus',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Subscription',
                        'action' => 'crrstatus',
                    ),
                ),
            ),
            'cg_subsc_notify' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/cg-subs-notify',
                    'defaults' => array(
                        'controller' => 'Caregiver\Controller\Subscription',
                        'action' => 'notify',
                    ),
                ),
            ),
          /***********Subscription routing here************* */
        )
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Caregiver\Controller\Index' => 'Caregiver\Controller\IndexController',
            'Caregiver\Controller\CaregiverAjax' => 'Caregiver\Controller\CaregiverAjaxController',
            'Caregiver\Controller\Profile' => 'Caregiver\Controller\ProfileController',
            'Caregiver\Controller\CaregiverMatrix' => 'Caregiver\Controller\CaregiverMatrixController',
            'Caregiver\Controller\Userdashboard' => 'Caregiver\Controller\UserdashboardController',
            'Caregiver\Controller\Trace' => 'Caregiver\Controller\TraceController',
            'Caregiver\Controller\Userprofile' => 'Caregiver\Controller\UserprofileController',
            'Caregiver\Controller\Subscription' => 'Caregiver\Controller\SubscriptionController',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'displayDate' => 'Caregiver\View\Helper\DisplayDateHelper',
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'header/caregiverheader' => __DIR__ . '/../view/header/caregiverheader.phtml',
            'left-menu/caregiver-left-menu' => __DIR__ . '/../view/left-menus/caregiver-left-menu.phtml',
            'paginator/paginators' => __DIR__ . '/../view/caregiver/paginator/paginators.phtml',
            'matrix-top-nav/matrix-top-navs' => __DIR__ . '/../view/caregiver/caregiver-matrix/matrix-top-nav.phtml',
            'trace_nav/trace_nav' => __DIR__ . '/../view/caregiver/trace/trace_nav.phtml',
            'layout' => __DIR__ . '/../view/layout/caregiverlayout.phtml',
            'footer/caregiverfooter' => __DIR__ . '/../view/footer/caregiverfooter.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
?>