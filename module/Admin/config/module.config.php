<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE MODULE CONFIG FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rabban
 * CREATOR: 13/11/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 * 
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */


return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'Video' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/video',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'video',
                    ),
                ),
            ),
            'beacon videos' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/beacon_videos',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'beaconvideos',
                    ),
                ),
            ),
            'caregiver_Video' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/caregiver_video',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'caregivervideo',
                    ),
                ),
            ),
            'researcher_video' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher_video',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'researchervideo',
                    ),
                ),
            ),
             'app_video' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/app_video',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'appvideo',
                    ),
                ),
            ),
            'app_video1' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/app_video1',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'appvideo1',
                    ),
                ),
            ),
            'app_video2' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/app_video2',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'appvideo2',
                    ),
                ),
            ),
            'app_video3' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/app_video3',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'appvideo3',
                    ),
                ),
            ),
           
            'dashboard' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-dashboard',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Index',
                        'action' => 'dashboard',
                    ),
                ),
            ),
            'upload' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-upload',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Index',
                        'action' => 'upload',
                    ),
                ),
            ),
            'super user' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-users',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\User',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt Activity_stage' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-activitystage',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Matrix',
                        'action' => 'activitystage',
                    ),
                ),
            ),
             'Defalt Measurement Body' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-bodymeasurement',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Matrix',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt Steps' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-steps&comm',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Matrix',
                        'action' => 'steps',
                    ),
                ),
            ),
            'Defalt Physical Activity' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-physical+activity',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Matrix',
                        'action' => 'phyactivity',
                    ),
                ),
            ),
            'Defalt Cholestrol' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-cholestrol',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Matrix',
                        'action' => 'cholestrol',
                    ),
                ),
            ),
            'Defalt BldPressure' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-blood+pressure',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Matrix',
                        'action' => 'bldpressure',
                    ),
                ),
            ),
            'Defalt FastGlucose' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-fasting+glucose',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Matrix',
                        'action' => 'fastglucose',
                    ),
                ),
            ),
            'Defalt Temperature' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-temperature',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Matrix',
                        'action' => 'temp',
                    ),
                ),
            ),
            'Defalt Sleep' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-sleep',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Matrix',
                        'action' => 'sleep',
                    ),
                ),
            ),
            'Defalt Conditions' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-conditions',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Condition',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt Symptoms' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-symptoms',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Condition',
                        'action' => 'symptoms',
                    ),
                ),
            ),
            'Defalt Treatments' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-treatments',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Condition',
                        'action' => 'treatments',
                    ),
                ),
            ),
            'Defalt Assesments' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-self',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Selfs',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt AssQuestionaire' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-self-questionnaire',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Selfs',
                        'action' => 'questionnaire',
                    ),
                ),
            ),
            'Defalt AssQuestion' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-self-question',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Selfs',
                        'action' => 'question',
                    ),
                ),
            ),
            /***Routing for Answer gorup*****/
            'Defalt AnswerGroup' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-answergroups',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Answergroup',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt AnswerGroup Answer' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-answergroups-answer',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Answergroup',
                        'action' => 'setanswer',
                    ),
                ),
            ),
            /***Routing for Answer gorup*****/
            'Defalt AssDel' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-self-del',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Selfs',
                        'action' => 'deleteAssesment',
                    ),
                ),
            ),
            'Defalt Details' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-self-details',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Selfs',
                        'action' => 'questionlist',
                    ),
                ),
            ),
            'Defalt AssFormula' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-self-assesmentformula',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Selfs',
                        'action' => 'assesmentformulas',
                    ),
                ),
            ),
            'Defalt Analyze' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-self-analyze',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Selfs',
                        'action' => 'quesanalyze',
                    ),
                ),
            ),
            'Defalt AnalyzeNonmember' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-self-analyze-nonmember',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Selfs',
                        'action' => 'quesanalyzeNonmember',
                    ),
                ),
            ),
            'Defalt ComplexSetting' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-self-complex-setting',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Selfs',
                        'action' => 'complexformulasetting',
                    ),
                ),
            ),
            'Defalt ResponceSetting' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-self-responce-setting',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Selfs',
                        'action' => 'questionresponcesetting',
                    ),
                ),
            ),
            'Defalt PassDomain' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-passDomain',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Pass',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt PassDomainAdd' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-passDomain-add',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Pass',
                        'action' => 'add',
                    ),
                ),
            ),
            'Defalt PassDomainDetail' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-passDomain-detail',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Pass',
                        'action' => 'detail',
                    ),
                ),
            ),
            'Defalt Studies' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt StudyRequest' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-request-study',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studyrequest',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt CreateStudies' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-create',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'studies',
                    ),
                ),
            ),
            'Defalt StudyPassiveVar' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-passive-var',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'passiveStudyVar',
                    ),
                ),
            ),
            'Defalt StudyQues' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-ques',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'question',
                    ),
                ),
            ),
            'Defalt StudyDel' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-del',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'deleteStudy',
                    ),
                ),
            ),
            'Defalt StudyQuestionnaire' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-questionnaire',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'questionnaire',
                    ),
                ),
            ),
            'Defalt StudyQuesScr' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-screening-ques',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'questionscreening',
                    ),
                ),
            ),
            'Defalt StudyVarSet' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-varrable-set',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'studiesvarset',
                    ),
                ),
            ),
            'Defalt StudyVarSetVirtual' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-update_trials',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'enableadminstudytrials',
                    ),
                ),
            ),
            'Defalt StudyVarSetAdd' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-varrable-set-add',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'addAttribute',
                    ),
                ),
            ),
            'Defalt StudyInvitationSet' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-invitation-set',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'studiesInvitationsSet',
                    ),
                ),
            ),
            'Defalt StudyEligibleUsers' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-eligible-users',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'studyEligibleSetUsers',
                    ),
                ),
            ),
            'Defalt StudyEligibleUsers Priview' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-eligible-users_preview',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'studyEligibleSetUsersPreview',
                    ),
                ),
            ),
            'Defalt StudyMainQueFilter' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-mainque-filter',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'mainQuestionFilter',
                    ),
                ),
            ),
            'Defalt StudyCodebook' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-codebook',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'codebook',
                    ),
                ),
            ),
            'Defalt StudySrcResult' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-scrresult',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'studyResult',
                    ),
                ),
            ),
            'Defalt StudyMainResult' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-studies-mainresult',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studies',
                        'action' => 'studyMainResult',
                    ),
                ),
            ),
            'Defalt BlogList' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-blogs',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Blog',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt BlogAdd' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-add-blogs',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Blog',
                        'action' => 'addblog',
                    ),
                ),
            ),
            'Defalt CronInvite' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/study-email-cron',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'emailInvitation',
                    ),
                ),
            ),
            'Defalt Cron' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/study-invitation-cron',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt CronActive' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/study-active-cron',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'studyActiveData',
                    ),
                ),
            ),
            'Defalt CronPassive' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/study-passive-cron',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'studyPassiveData',
                    ),
                ),
            ),
            'Defalt CronEmail' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/study-emailxls-cron',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'studyEmailData',
                    ),
                ),
            ),
            'Defalt CronTrail' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/study-trialxlx-cron',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'studyTrialData',
                    ),
                ),
            ),
            'Defalt Pricing' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-matrix-caregiver',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Price',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt PricingRes' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-matrix-researcher',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Price',
                        'action' => 'researcher',
                    ),
                ),
            ),
            'Defalt PricingInvoice' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-matrix-cg-invoice',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Price',
                        'action' => 'invoice',
                    ),
                ),
            ),
            'Defalt PricingInvoiceResearcher' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-matrix-researcher-invoice',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Price',
                        'action' => 'researcherInvoice',
                    ),
                ),
            ),
            'Defalt Studytrial' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-trial',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studytrial',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt BlogDetails' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/admin-blog-details[/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    //'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Blog',
                        'action' => 'viewblog',
                    ),
                ),
            ),
            /*             * *****Routing for Reward section********* */
            'Defalt RewardDetails' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-reward',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Pass',
                        'action' => 'reward',
                    ),
                ),
            ),
            'Defalt Registercc' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-reward-registercc',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Pass',
                        'action' => 'registercc',
                    ),
                ),
            ),
            'Defalt Deletecc' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-deletecc',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Pass',
                        'action' => 'deleteCard',
                    ),
                ),
            ),
            /*             * ***End reward routing**** */

            /*             * *****Routing for researcher invite section********* */
            'Defalt ResearcherInvitation' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-researcherinvite',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Researchinvitation',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt ManageResearcherInvitation' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-manageresearchinvite',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Researchinvitation',
                        'action' => 'manageinvitations',
                    ),
                ),
            ),
            /*             * ***End researcher invite routing**** */
            'Defalt BlogEdit' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/admin-edit-blogs[/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    //'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Blog',
                        'action' => 'editblog',
                    ),
                ),
            ),
            'Defalt Subscription' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-subscription',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Subscription',
                        'action' => 'index',
                    ),
                ),
            ),
            'Defalt StudyInvite' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-instant-invitation',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Studyinvite',
                        'action' => 'index',
                    ),
                ),
            ), 'Defalt Invitation' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-invitation',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Invitation',
                        'action' => 'index',
                    ),
                ),
            ), 'Defalt Source' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-source-allocation',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Source',
                        'action' => 'index',
                    ),
                ),
            ), 'Defalt Questionnaire' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-questionnaire',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Questionnaire',
                        'action' => 'index',
                    ),
                ),
            ), 'Defalt QuestionnaireDel' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-questionnaire-delete',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Questionnaire',
                        'action' => 'deleteQuestionnaire',
                    ),
                ),
            ), 
            'Defalt QuestionnaireDel' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-questionnaire-delete',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Questionnaire',
                        'action' => 'deleteQuestionnaire',
                    ),
                ),
            ), 'Defalt QuestionnaireQueD' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-answergroup-delete',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Answergroup',
                        'action' => 'deleteQuestionnaire',
                    ),
                ),
            ),
            'Defalt QuestionnaireQue' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin-questionnaire-question',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Questionnaire',
                        'action' => 'question',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'admin' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/admin',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Index' => 'Admin\Controller\IndexController',
            'Admin\Controller\User' => 'Admin\Controller\UserController',
            'Admin\Controller\Matrix' => 'Admin\Controller\MatrixController',
            'Admin\Controller\Condition' => 'Admin\Controller\ConditionController',
            'Admin\Controller\Selfs' => 'Admin\Controller\AssesmentController',
            'Admin\Controller\Studies' => 'Admin\Controller\StudiesController',
            'Admin\Controller\Blog' => 'Admin\Controller\BlogController',
            'Admin\Controller\Crons' => 'Admin\Controller\CronsController',
            'Admin\Controller\Pass' => 'Admin\Controller\PassController',
            'Admin\Controller\Price' => 'Admin\Controller\PriceController',
            'Admin\Controller\Studytrial' => 'Admin\Controller\StudytrialController',
            'Admin\Controller\Subscription' => 'Admin\Controller\SubscriptionController',
            'Admin\Controller\Invitation' => 'Admin\Controller\InvitationController',
            'Admin\Controller\Researchinvitation' => 'Admin\Controller\ResearchinvitationController',
            'Admin\Controller\Studyinvite' => 'Admin\Controller\StudyinviteController',
            'Admin\Controller\Source' => 'Admin\Controller\SourceController',
            'Admin\Controller\Questionnaire' => 'Admin\Controller\QuestionnaireController',
            'Admin\Controller\Studyrequest' => 'Admin\Controller\StudyrequestController',
            'Admin\Controller\Answergroup' => 'Admin\Controller\AnswergroupController',
            
            
            
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'PluginController' => 'Admin\Controller\Plugin\PluginController',
            'ResizeController' => 'Admin\Controller\Plugin\ResizeController',
        )
    ),
    // 'view_helpers' => array(
    //     'invokables'=> array(
    //         'test_helper' => 'Admin\View\Helper\Options',
    //     )
    // ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'headers/header' => __DIR__ . '/../view/header/header.phtml',
            'left-menus/left-menu' => __DIR__ . '/../view/left-menu/left-menu.phtml',
            'matrix/top_navigation' => __DIR__ . '/../view/admin/matrix/top_navigation.phtml',
            'Condition/top_navigation' => __DIR__ . '/../view/admin/condition/top_navigation.phtml',
            'paginav/paginators' => __DIR__ . '/../view/admin/paginav/paginators.phtml',
            'layouts/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'footers/footer' => __DIR__ . '/../view/footer/footer.phtml',
            'errors/404' => __DIR__ . '/../view/error/404.phtml',
            'errors/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
