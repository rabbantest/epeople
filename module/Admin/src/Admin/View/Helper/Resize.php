<?php

namespace Admin\View\Helper;

use Admin\Model\VariablesTable;
use Zend\View\Helper\AbstractHelper;

class Resize extends AbstractHelper {

    public function __invoke($pathImg, $image, $newWidth, $newHeight) {

        $base_path = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'];
        $originalFile = $pathImg . $image;
        $check = str_replace($base_path, '', $originalFile);
        if (!file_exists('public/' . $check)) {
            return "";
        }
        $info = getimagesize($originalFile);
        $mime = $info['mime'];
        $image_create_func = '';
        $image_save_func = '';
        $new_image_ext = '';

        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw Exception('Unknown image type.');
        }

        $file = 'public/cacheImg/' . $image . $newWidth . $newHeight . '.' . $new_image_ext;
        if (file_exists($file)) {
            return $base_path . '/cacheImg/' . $image . $newWidth . $newHeight . '.' . $new_image_ext;
        }
        $img = $image_create_func($originalFile);
        list($width, $height) = getimagesize($originalFile);
        $tmp = imagecreatetruecolor($newWidth, $newHeight);
        $white = imagecolorallocate($tmp, 255, 255, 255);
        imagefill($tmp, 0, 0, $white);
        imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);


        $file = 'public/cacheImg/' . $image . $newWidth . $newHeight . '.' . $new_image_ext;
        $image_save_func($tmp, "$file");
        return $base_path . '/cacheImg/' . $image . $newWidth . $newHeight . '.' . $new_image_ext;
    }

}
