<?php

namespace Admin\View\Helper;

use Admin\Model\VariablesTable;
use Zend\View\Helper\AbstractHelper;

class Percentage extends AbstractHelper {

    public function __invoke($quesOj, $optionsArr = array(), $question_id = NULL, $bucket_id = NULL, $study_id=NULL) {
        $answeArr = array();
        if (count($optionsArr) > 0) {
            $columns = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'), 'option_id');
            //$whereArr = array("ans_status" => 1, "quest_id" => $question_id/* , 'option_id' => $option['opt_value'] */);
            $whereArr['ans_status'] = 1;
            $whereArr['quest_id'] = $question_id;
            if($study_id != NULL){                
                 $whereArr['study_id'] = $study_id;
            }
            if ($bucket_id != NULL) {
                $whereArr['bucket'] = $bucket_id;
            }
            $totalArr = $quesOj->getAdminQuestionsAnalyze($whereArr, $columns, 0, array(), 0, 1);
            $totalCount = array_sum(array_column($totalArr, 'num'));
            $totalArr = array_combine(array_column($totalArr, 'option_id'), array_column($totalArr, 'num'));

            foreach ($optionsArr AS $option) {

                $answeArr[] = array(
                    'opt_id' => $option['opt_id'],
                    "opt_option" => $option['opt_option'],
                    "total_count" => $totalCount,
                    "option_count" => isset($totalArr[$option['opt_value']]) ? $totalArr[$option['opt_value']] : 0
                );
            }
        }

        return $answeArr;
    }

}
