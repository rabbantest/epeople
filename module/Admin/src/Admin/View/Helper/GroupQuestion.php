<?php

namespace Admin\View\Helper;

use Admin\Model\VariablesTable;
use Zend\View\Helper\AbstractHelper;

class GroupQuestion extends AbstractHelper {

    public function __invoke($quesOj, $is_group_questionnaire = NULL) {
        $optionArr = array();

        if ($is_group_questionnaire) {
            $columns = array("ques_id", "questionnaire_id", "ques_question");
            $where = array('questionnaire_id' => $is_group_questionnaire, 'ques_status' => 1);
            $optionArr = $quesOj->getAdminQuestionarQues($where, $columns);
            if (count($optionArr) > 0) {
                $i=0;
                foreach ($optionArr as $ovalue) {
                    $whereQueOptArr = array("question_id" => $ovalue['ques_id'], "opt_status" => 1);
                    $coloumnQueOpt = array("opt_id", "opt_option", "opt_value");
                    $optionArr[$i]['question_opt'] = $quesOj->getAdminQuestionairOpt($whereQueOptArr, $coloumnQueOpt);
                    $i++;
                }
            }
        }
        return $optionArr;
    }

}
