<?php

/* * ***********************
 * PAGE: USE TO MANAGE SUPER ADMIN CONDITION PANNEL.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: ANKIT SHUKLA(fb/gshukla67).
 * CREATOR: 26/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Delete;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Db\Adapter\Driver\DriverInterface;

class QuestionTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_super_ass_study_questions';
    public $optTable = 'hm_super_ass_study_questions_option';
    public $varTable = 'hm_super_ass_study_questions_variable';
    public $ansTable = 'hm_selfassess_ques_answer';
    public $mainFilterTable = 'hm_super_study_main_que_filter';
    public $queTable = 'hm_super_questionnaire_question';
    public $tableqo = 'hm_super_questionnaire_option';


    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }
    
    
    
    function deleteEmailInvitations($where = array(), $notIn = array()) {
        try {
            
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from('hm_super_study_invitation_set');

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    function deleteVarSet($where = array(), $notIn = array()) {
        try {
            
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from('hm_super_study_varrable_set');

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    function deleteMainFilter($where = array(), $notIn = array()) {
        try {
            
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->mainFilterTable);

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    function deleteVariable($where = array(), $notIn = array()) {
        try {
            
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->varTable);

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    function deleteQuestion($where = array(), $notIn = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->table);

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAdminQuestions($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $group = NULL, $limit = array()) {

        $orderBy = array("ques_order ASC", "ques_id asc");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asq' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $options = array("var_name");
                $select->join(array('vr' => $this->varTable), "asq.ques_variable = vr.var_id", $options, 'LEFT');
            }


            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                if (!empty($searchContent)) {
                    foreach ($searchContent AS $fields => $data) {
                        if (!empty($data)) {
                            $likeWhr->addPredicate(
                                    new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                            );
                            $select->where($likeWhr);
                        }
                    }
                }
                $orderBy = "ques_variable ASC";
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            if ($group) {
                $select->group('ques_variable');
            }

            $select->order($orderBy);


            $statement = $sql->prepareStatementForSqlObject($select);
//             echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//            die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getStudyQuestion($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $group = NULL, $limit = array()) {


        $orderBy = array("ques_order ASC", "ques_id asc");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asq' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $options = array();
                $select->join(array('vr' => $this->varTable), "asq.ques_variable = vr.var_id", $options, 'LEFT');
                $options = array();
                $select->join(array('option' => $this->optTable), "asq.ques_id = option.question_id", $options, 'LEFT');
            }


            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                if (!empty($searchContent)) {
                    foreach ($searchContent AS $fields => $data) {
                        if (!empty($data)) {
                            $likeWhr->addPredicate(
                                    new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                            );
                            $select->where($likeWhr);
                        }
                    }
                }
                $orderBy = "ques_variable ASC";
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            if ($group) {
                $select->group('ques_id');
            }

            $select->order($orderBy);


            $statement = $sql->prepareStatementForSqlObject($select);
//            echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//            die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            //echo '<pre>';print_r($user); die;
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAdminQuestionsOption($where = array(), $columns = array(), $searchContent = array()) {

        // $orderBy = "cat_name ASC";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'op' => $this->optTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            // $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function insertAdminQuestions($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->table);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
        } catch (\Exception $e) {
            echo $e;die;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateAdminQuestions($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function insertAdminQuestionsOption($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->optTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateAdminQuestionsOption($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->optTable);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function deleteAdminQuestionsOption($where = array(), $notIn = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->optTable);

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('opt_id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function deleteAdminQuestions($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->table);

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function deleteAdminVar($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->varTable);

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     * Action:- Update assesment order in database on ajax call.
     * @author: Hemant.
     * @param array $where            
     * @param array $columns            
     * @throws \Exception
     * @return true/false
     * *** */

    public function orderAdminQuestion($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            // echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            if ($result) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function checkAvilable($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asf' => $this->table
            ));

            $select->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));

            if (count($where) > 0) {
                $select->where($where);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $status = $this->resultSetPrototype->initialize($statement->execute())->toArray();
//            echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//            die;
            return ($status[0]['num'] == 0) ? true : false;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getAdminQuestionarQues($where = array(), $columns = array(), $lookingfor = false, $limit = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'tqq' => $this->queTable
            ));
              if (is_array($where) && count($where) > 0) {         
                 $select->where($where);
            }
           if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
             if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
          // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
     /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getAdminQuestionairOpt($where = array(), $columns = array(), $lookingfor = false, $groupby = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'tqo' => $this->tableqo
            ));
              if (is_array($where) && count($where) > 0) {         
                 $select->where($where);
            }
           if (count($columns) > 0) {
                $select->columns($columns);
            }
            if($groupby){
                $select->group($groupby);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
          // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
