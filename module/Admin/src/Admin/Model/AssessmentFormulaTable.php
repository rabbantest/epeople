<?php
/*************************
    * PAGE: USE TO MANAGE SUPER ADMIN CONDITION PANNEL.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: HEMANT SINGH CHUPHAL.
    * CREATOR: 26/2/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/

namespace Admin\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Delete;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

use Zend\Db\Adapter\Driver\DriverInterface;


class AssessmentFormulaTable extends AbstractTableGateway
{

    /*********
        *
        * @var String
    *****/
    public $table = 'hm_selfassess_formulas';
    public $metrixTable="hm_super_health_metrix";
    public $formulaDisTable="hm_selfassess_formula_display";
    public $questionTable="hm_super_ass_study_questions";
    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }
    
    public function checkAvilable($where=array()){
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asf' => $this->table
            ));
            
            $select->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));
           
            if (count($where) > 0) {
                $select->where($where);
            }
         
            $statement = $sql->prepareStatementForSqlObject($select);
            $status = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            
            return ($status[0]['num']==0)?true:false;
            
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getAdminAssessmentFormula($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array()) {

        $orderBy = "formula_id asc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asf' => $this->table
            ));
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $options = array("col_name","type",'health_id');
                $select->join(array('mtxAll' => $this->metrixTable), "asf.formula_health = mtxAll.health_id", $options, 'LEFT');
                
                $options = array("display_id","columns","feedback");
                $select->join(array('fdt' => $this->formulaDisTable), "asf.formula_id = fdt.display_formula", $options, 'LEFT');
                
                $options = array("ques_question");
                $select->join(array('que' => $this->questionTable), "asf.response_question = que.ques_id", $options, 'LEFT');
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    
    
    
    public function insertAdminAssessmentFormula($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->table);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function updateAdminAssessment ($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute(); 
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function deleteAdminAssessmentFormula ($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->table);

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
}