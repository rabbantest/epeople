<?php

namespace Admin\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Delete;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Db\Adapter\Driver\DriverInterface;

class QuestionnaireTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_super_questionnaire';
    public $varTable = 'hm_super_questionnaire_variable';
    public $queTable = 'hm_super_questionnaire_question';
    public $optTable = 'hm_super_questionnaire_option';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    function deleteQuestionnaireVar($where = array(), $notIn = array()) {
        try {

            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->varTable);

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    function deleteQuestionnaireQuestion($where = array(), $notIn = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->queTable);

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    function deleteQuestionnaire($where = array(), $notIn = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->table);

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminQuestionsAndOptions($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $group = NULL, $limit = array()) {

        $orderBy = array("ques_id asc");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'que' => $this->queTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $options = array("*");
                $select->join(array('opt' => $this->optTable), "que.ques_id = opt.question_id", $options, 'LEFT');
            }


            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                if (!empty($searchContent)) {
                    foreach ($searchContent AS $fields => $data) {
                        if (!empty($data)) {
                            $likeWhr->addPredicate(
                                    new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                            );
                            $select->where($likeWhr);
                        }
                    }
                }
                $orderBy = "ques_variable ASC";
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            if ($group) {
                $select->group('ques_variable');
            }

            $select->order($orderBy);


            $statement = $sql->prepareStatementForSqlObject($select);
//            $statement->prepare();
//            echo $statement->getSql();
//            exit;
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function checkAvilableQuestionnaire($where = array(), $notIn = NULL) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'questionnaire' => $this->table
            ));

            $select->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (!empty($notIn)) {
                $select->where->NotequalTo('questionnaire.id', $notIn);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $status = $this->resultSetPrototype->initialize($statement->execute())->toArray();

            return ($status[0]['num'] == 0) ? true : false;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAdminQuestionnaire($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array(), $notIn = NULL) {

        $orderBy = "id DESC";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'questionnaire' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (!empty($notIn)) {
                $select->where->NotequalTo('questionnaire.id', $notIn);
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function insertAdminQuestionnaire($data = array()) {
        try {

            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->table);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function updateAdminQuestionnaire($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminVariables($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array()) {

        $orderBy = array("var_name ASC");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'var' => $this->varTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);
//            echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//            die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertAdminVariables($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->varTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateAdminVariables($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->varTable);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function checkAvilable($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'que' => $this->queTable
            ));

            $select->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));

            if (count($where) > 0) {
                $select->where($where);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $status = $this->resultSetPrototype->initialize($statement->execute())->toArray();

            return ($status[0]['num'] == 0) ? true : false;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateAdminQuestions($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->queTable);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertAdminQuestions($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->queTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertAdminQuestionsOption($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->optTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function deleteAdminQuestions($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->queTable);

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateAdminQuestionsOption($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->optTable);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function deleteAdminQuestionsOption($where = array(), $notIn = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->optTable);

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('opt_id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminQuestions($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $group = NULL, $limit = array()) {

        $orderBy = array("ques_id asc");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'que' => $this->queTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $options = array("var_name");
                $select->join(array('vr' => $this->varTable), "que.ques_variable = vr.var_id", $options, 'LEFT');
            }


            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                if (!empty($searchContent)) {
                    foreach ($searchContent AS $fields => $data) {
                        if (!empty($data)) {
                            $likeWhr->addPredicate(
                                    new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                            );
                            $select->where($likeWhr);
                        }
                    }
                }
                $orderBy = "ques_variable ASC";
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            if ($group) {
                $select->group('ques_variable');
            }

            $select->order($orderBy);


            $statement = $sql->prepareStatementForSqlObject($select);
//            $statement->prepare();
//            echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//            die;
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminQuestionsIDSOption($where = array(), $columns = array(), $searchContent = array(), $group = null) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'opt' => $this->optTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }
            if ($group) {
                $select->group('opt.question_id');
            }
//             if ($groupforoption) {
//                $select->group('opt.question_id');
//            }
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminQuestionsOption($where = array(), $columns = array(), $searchContent = array(), $group = null) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'opt' => $this->optTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }
            if ($group) {
                $select->group('opt.opt_option');
            }
            $select->order('opt.opt_value ASC');
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function deleteAdminVar($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->varTable);

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
