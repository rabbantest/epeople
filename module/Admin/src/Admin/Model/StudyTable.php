<?php

/* * ***********************
 * PAGE: USE TO MANAGE SUPER ADMIN CONDITION PANNEL.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 3/3/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Delete;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Db\Adapter\Driver\DriverInterface;

class StudyTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_super_studies';
    public $varSetTable = 'hm_super_study_varrable_set';
    public $invitationSetTable = 'hm_super_study_invitation_set';
    public $inviBucketTable = 'hm_super_study_bucket_invitation';
    public $settingTable = 'hm_super_study_bucket_cron_setting';
    public $partTable = 'hm_selfassess_participation';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    function deleteStudy($where = array(), $notIn = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->table);

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getSource($where = array(), $columns = array(), $likeArray = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'Source' => 'hm_validic_app_source'
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (count($likeArray) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($likeArray AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }
            $select->order('source ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertAnsStudy($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert('hm_selfassess_ques_answer');
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAnsStudy($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'answe' => 'hm_selfassess_ques_answer'
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudyBucketInvi($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'inviBucket' => $this->inviBucketTable
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudyParticipation($where = array(), $columns = array(),$notIn=0) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'part' => $this->partTable
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            
            
            /// $select->group(array('part.UserID'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    public function getAdminStudyBucketInvitation($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array(), $group = 0) {
        $orderBy = "id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'invi' => 'hm_super_study_bucket_invitation'
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->join(array('part' => 'hm_selfassess_participation'), "invi.study_id = part.study_id AND invi.user_id = part.UserID", array(), 'LEFT');
            }
            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }
            if ($group) {
                $select->group(array('invi.is_accepted'));
            }

            if (count($where) > 0) {
                $select->where($where);
            }
            $select->order($orderBy);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudyBucketCronSetting($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'bucket' => $this->settingTable
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudyInvitationSet($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array()) {

        $orderBy = "invitation_set_id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'inv' => $this->invitationSetTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $options = array('study_title', 'start_date','end_date');
                $select->join(array('Stu' => 'hm_super_studies'), new \Zend\Db\Sql\Expression("Stu.study_id = inv.invitation_set_study"), $options, 'LEFT');
            
                $options = array('subdomain');
                $select->join(array('Dom' => 'hm_pass_subdomain'), new \Zend\Db\Sql\Expression("inv.domain_id = Dom.id"), $options, 'LEFT');
            
            }

            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudy($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array(), $date = NULL,$notIn=NULL) {

        $orderBy = "study_id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stu' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                /* $options = array('total' => new \Zend\Db\Sql\Expression('SUM(number)/'));
                  $select->join(array('set' => $this->settingTable), new \Zend\Db\Sql\Expression("stu.study_id = set.study_id AND set.sent=1"), $options, 'LEFT');
                  $select->group(array('stu.study_id'));

                  $options = array('Participated' => new \Zend\Db\Sql\Expression('count(part.study_id)'));
                  $select->join(array('part' => $this->partTable), "stu.study_id = part.study_id", $options, 'LEFT'); */
            }
            
             if (!empty($notIn)) {
                $select->where->notIn('requested_by', array('0'));
            }

            if (!empty($date)) {
                $select->where(new \Zend\Db\Sql\Predicate\Expression("start_date >= '$date'"));
            }

            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);
//           echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//die;
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudyVarSet($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array()) {

        $orderBy = "set_id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'varSet' => $this->varSetTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }

            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function insertAdminStudy($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->table);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertAdminStudyVarSet($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->varSetTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateAdminStudyVarSet($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->varSetTable);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    public function updateAdminStudyStudyInvitationSet($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->invitationSetTable);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertAdminStudyInvitationSet($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->invitationSetTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function deleteAdminStudyInvitation($where = array(), $notIn = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->invitationSetTable);

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('opt_id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateAdminStudy($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function checkAvilableStudy($where = array(), $notIn = NULL) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stu' => $this->table
            ));

            $select->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (!empty($notIn)) {
                $select->where->NotequalTo('stu.study_id', $notIn);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $status = $this->resultSetPrototype->initialize($statement->execute())->toArray();

            return ($status[0]['num'] == 0) ? true : false;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
