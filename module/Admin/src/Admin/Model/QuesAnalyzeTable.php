<?php

/* * ***********************
 * PAGE: USE TO MANAGE SUPER ADMIN SELF ASSESSMENT PANNEL.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: ANKIT SHUKLA(fb/gshukla67).
 * CREATOR: 27/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Delete;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Db\Adapter\Driver\DriverInterface;

class QuesAnalyzeTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_selfassess_ques_answer';
    public $tablenonmem = 'hm_nonmember-assess_ques_answer';
    public $optionTable = 'hm_super_assquestions_option';
    public $optionPart = 'hm_selfassess_participation';
    public $goptionTable = 'hm_super_questionnaire_option';
    public $gqatable = 'hm_user_study_group_ans';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */
    
    
    public function getAdminQuestionsAnalyzeNonMember($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(),$group=0,$groupOpt=0) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asqnon' => $this->tablenonmem
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $opt_value = array('percent' => new \Zend\Db\Sql\Expression('AVG(opt_value)'));
                $select->join(array('opt' => $this->optionTable), "asqnon.option_id = opt.opt_id", $opt_value, 'LEFT');
            }
            if (count($searchContent) > 0) {
                
            }

            if (count($where) > 0) {
                $select->where($where);
            }
            
            if($group){
                $select->group('asqnon.nonmember_id');
                $select->order('asqnon.id desc');
            }
            
            if($groupOpt){
                $select->group('asqnon.option_id');
            }
            
            

            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function deleteAdminQuesAnalyze($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->table);

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
    
    public function deleteParticipation($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->optionPart);

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAdminQuestionsAnalyze($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(),$group=0,$groupOpt=0) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asq' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $opt_value = array('percent' => new \Zend\Db\Sql\Expression('AVG(opt_value)'));
                $select->join(array('opt' => $this->optionTable), "asq.option_id = opt.opt_id", $opt_value, 'LEFT');
            }
            if (count($searchContent) > 0) {
                
            }

            if (count($where) > 0) {
                $select->where($where);
            }
            
            if($group){
                $select->group('asq.user_id');
                $select->order('asq.id desc');
            }
            
            if($groupOpt){
                $select->group('asq.option_id');
            }
            
            

            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
 /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAdminGroupQuestionsAnalyze($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(),$group=0,$groupOpt=0) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asq' => $this->gqatable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $opt_value = array('percent' => new \Zend\Db\Sql\Expression('AVG(opt_value)'));
                $select->join(array('opt' => $this->goptionTable), "asq.option_id = opt.opt_id", $opt_value, 'LEFT');
            }
            if (count($searchContent) > 0) {
                
            }

            if (count($where) > 0) {
                $select->where($where);
            }
            
            if($group){
                $select->group('asq.user_id');
                $select->order('asq.id desc');
            }
            
            if($groupOpt){
                $select->group('asq.option_value');
            }
            
            

            $statement = $sql->prepareStatementForSqlObject($select);
          //   echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;
             
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
}
