<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE CONDITION PART OF ADMIN.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 2/3/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;


class PriceController extends AbstractActionController {

    protected $_priceMatrixObj;

    public function init() {
        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');
        $this->_sessionObj = new Container('super_admin');
        if ($this->_sessionObj->admin['id'] != 1) {
            return $this->redirect()->toRoute('admin');
        }
    }

    /*     * *****
     * Action:- Price metrix add edit listing for caregiver.
     * @author: Hemant.
     * Created Date: 5-05-2015.
     * *** */

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $form = new \Admin\Form\Price();
        $request = $this->getRequest();
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $Pass = array();
        $this->_priceMatrixObj = $this->getServiceLocator()->get("PriceTable");
        //check Pass edit to prefill edit form

        if ($sid) {
            $ColArr = array('id', 'type', 'user', 'variable', 'price');
            $Price = $this->_priceMatrixObj->getPriceMatrix(array('id' => $sid), $ColArr, '0');
            foreach ($Price[0] as $key => $index) {
                $form->get($key)->setValue($index);
            }
        }

        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $PriceValidator = new \Admin\Form\PriceValidator();
            $form->setInputFilter($PriceValidator->getInputFilter());
            $form->setValidationGroup('type', 'user', 'price');
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $postDataArr;
                unset($data['submit']);
                $ColCountArr = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'));
                $WhereCountArr = array('type' => 1, 'user' => $postDataArr['user']);

                $PriceCount = $this->_priceMatrixObj->getPriceMatrix($WhereCountArr, $ColCountArr, '0', array(), array(), $postDataArr['id']);
                if ($PriceCount[0]['num'] == 0) {
                    $this->_saveData($postDataArr['id'], $data); //call function to save data
                } else {
                    $message = 'Price matrix already configure for this settings.';
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
                $ColArr = array('id', 'type', 'user', 'variable', 'price');
                foreach ($ColArr as $key) {
                    $form->get($key)->setValue('');
                }
            } else {
                $message = 'Please Provide valid data.';
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }

        $PriceMarix = $this->_priceMatrixObj->getPriceMatrix(array('type' => 1), array('id', 'user', 'price', 'status'), '0');

        $sendArr = array(
            'form' => $form,
            'PriceMarix' => $PriceMarix,
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- Price metrix add edit listing for researcher.
     * @author: Hemant.
     * Created Date: 6-05-2015.
     * *** */

    function researcherAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $form = new \Admin\Form\Price();
        $request = $this->getRequest();
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $Pass = array();
        $this->_priceMatrixObj = $this->getServiceLocator()->get("PriceTable");
        //check Pass edit to prefill edit form
        $form->setAttribute('action', 'admin-matrix-researcher');
        if ($sid) {
            $ColArr = array('id', 'type', 'user', 'variable', 'price');
            $Price = $this->_priceMatrixObj->getPriceMatrix(array('id' => $sid), $ColArr, '0');
            foreach ($Price[0] as $key => $index) {
                $form->get($key)->setValue($index);
            }
        }

        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $PriceValidator = new \Admin\Form\PriceValidator();
            $form->setInputFilter($PriceValidator->getInputFilter());
            $form->setValidationGroup('type', 'user', 'price');
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $postDataArr;
                unset($data['submit']);
                $ColCountArr = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'));
                $WhereCountArr = array('type' => 2, 'user' => $postDataArr['user']);

                $PriceCount = $this->_priceMatrixObj->getPriceMatrix($WhereCountArr, $ColCountArr, '0', array(), array(), $postDataArr['id']);
                if ($PriceCount[0]['num'] == 0) {
                    $this->_saveData($postDataArr['id'], $data); //call function to save data
                } else {
                    $message = 'Price matrix already configure for this settings.';
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
                $ColArr = array('id', 'type', 'user', 'price');
                foreach ($ColArr as $key) {
                    $form->get($key)->setValue('');
                }
            } else {
                $message = 'Please Provide valid data.';
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }

        $PriceMarix = $this->_priceMatrixObj->getPriceMatrix(array('type' => 2), array('id', 'user', 'price', 'status', 'variable'), '0');

        $sendArr = array(
            'form' => $form,
            'PriceMarix' => $PriceMarix,
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- Protected function to save data of price matrix.
     * @author: Hemant.
     * Created Date: 05-05-2015.
     * *** */

    function _saveData($sid = NULl, $data = NULL) {
        if ($sid) {
            $data['updation_date'] = time();
            if ($this->_priceMatrixObj->updatePriceMatrix(array('id' => $sid), $data)) {
                $message = 'Sucessfully updated pricing matrix.';
                $this->flashMessenger()->addMessage(array('success' => $message));
            } else {
                $message = 'Error occurred while saving please try again.';
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        } else {
            $data['creation_date'] = time();
            $sid = $this->_priceMatrixObj->insertPriceMatrix($data);
            if ($sid) {
                $message = 'Sucessfully created new pricing matrix.';
                $this->flashMessenger()->addMessage(array('success' => $message));
            } else {
                $message = 'Error occurred while saving please try again.';
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
    }

    /*     * *****
     * Action:- Change status on ajax call for pricing matrix admin for (researcher and caregiver).
     * @author: Hemant.
     * Created Date: 6-05-2015.
     * *** */

    function changeStatusAction() {
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $this->_priceMatrixObj = $this->getServiceLocator()->get("PriceTable");
        if (($request->isXmlHttpRequest())) {
            $status = ($postDataArr['status'] == 0) ? 1 : 0;
            if ($this->_priceMatrixObj->updatePriceMatrix(array('id' => $postDataArr['id']), array('status' => $status))) {
                echo json_encode(array('true', $postDataArr['id'], $status));
            } else {
                echo json_encode(array('false'));
            }
        }
        die;
    }

    /*     * *****
     * Action:- Pricing matrix Invoice for researcher.
     * @author: Hemant.
     * Created Date: 6-05-2015.
     * *** */

    function researcherInvoiceAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $this->_priceMatrixObj = $this->getServiceLocator()->get("PriceTable");
        $columns = array('start_date', 'end_date', 'transaction_id', 'amount', 'billing_date', 'payment_status');
        $where = array('type' => 2);
        $limit = array(0, 10);
        $page = 1;
        if (base64_decode($this->params()->fromQuery('page')) > 0) {
            $page = base64_decode($this->params()->fromQuery('page'));
            $limit = array($limit[1] * $page, $limit[1]);
            $page++;
        }
        $Subscription = $this->_priceMatrixObj->getSubscription($where, $columns, "1", array(), $limit); //CODE FOR SCROLL PAGINATION
        $request = $this->getRequest();
//        echo '<pre>';
//        print_r($Subscription);die;
        $sendArr = new ViewModel();
        ($request->isXmlHttpRequest()) ? $sendArr->setTerminal(true) : '';
        return $sendArr->setVariables(array('Subscription' => $Subscription, 'page' => $page, 'session' => $this->_sessionObj->admin));
    }

    /*     * *****
     * Action:- Pricing matrix Invoice for caregiver.
     * @author: Hemant.
     * Created Date: 6-05-2015.
     * *** */

    function invoiceAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $this->_priceMatrixObj = $this->getServiceLocator()->get("PriceTable");
        $columns = array('start_date', 'end_date', 'transaction_id', 'amount', 'billing_date', 'payment_status');
        $where = array('type' => 1);
        $limit = array(0, 10);
        $page = 1;
        if (base64_decode($this->params()->fromQuery('page')) > 0) {
            $page = base64_decode($this->params()->fromQuery('page'));
            $limit = array($limit[1] * $page, $limit[1]);
            $page++;
        }
        $Subscription = $this->_priceMatrixObj->getSubscription($where, $columns, "1", array(), $limit); //CODE FOR SCROLL PAGINATION
        $request = $this->getRequest();
//        echo '<pre>';
//        print_r($Subscription);die;
        $sendArr = new ViewModel();
        ($request->isXmlHttpRequest()) ? $sendArr->setTerminal(true) : '';
        return $sendArr->setVariables(array('Subscription' => $Subscription, 'page' => $page, 'session' => $this->_sessionObj->admin));
    }

}
