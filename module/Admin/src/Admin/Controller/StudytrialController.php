<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE CONDITION PART OF ADMIN.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 8/5/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class StudytrialController extends AbstractActionController {

    protected $_studyTrialObj;

    public function init() {
        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');
        $this->_sessionObj = new Container('super_admin');
        if ($this->_sessionObj->admin['id'] != 1) {
            return $this->redirect()->toRoute('admin');
        }
    }

    /*     * *****
     * Action:-Study Trial onsite listing page.
     * @author: Hemant.
     * Created Date: 08-05-2015.
     * *** */

    function indexAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $this->_studyTrialObj = $this->getServiceLocator()->get("OnsiteStudyTrial");
        $columns = array('id', 'category', 'description', 'start_date', 'end_date', 'address');
        $cat=base64_decode($this->params()->fromQuery('cat'));
        $i=base64_decode($this->params()->fromQuery('i'));
        $where = array();
        $limit = array(0, 10);
        $page = 1;
        if (base64_decode($this->params()->fromQuery('page')) > 0) {
            $page = base64_decode($this->params()->fromQuery('page'));
            $limit = array($limit[1] * $page, $limit[1]);
            $page++;
        }
        $StudyTrials = $this->_studyTrialObj->get($where, $columns, "1", array(), $limit); //CODE FOR SCROLL PAGINATION
        $request = $this->getRequest();
        $sendArr = new ViewModel();
        ($request->isXmlHttpRequest()) ? $sendArr->setTerminal(true) : '';
        return $sendArr->setVariables(array('i'=>$i,'cat'=>$cat,'StudyTrials' => $StudyTrials, 'page' => $page, 'session' => $this->_sessionObj->admin));
    }

    /*     * *****
     * Action:- function to upload excel for Onsite Study Trial .
     * @author: Hemant Chuphal.
     * Created Date: 08-05-2015.
     * *** */

    function uploadXlsAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest() && $request->isPost() && !$_FILES['excel_file']['error']) {
            $postDataArr = $request->getPost()->toArray();
            if (!empty($_FILES['excel_file']['name']) && !($_FILES['excel_file']['error'])) {
                $this->_upload($_FILES);
            }
        }
    }

    /*     * *****
     * Action:- function to import xls file.
     * @author: Hemant Chuphal.
     * Created Date: 08-05-2015.
     * *** */

    function _Upload($file = NULL) {
        $phpExcel = new \PHPExcel();
        $path = BASE_PATH . "/" . md5(microtime()) . '.xls';
        $ext = pathinfo($file['excel_file']['name'], PATHINFO_EXTENSION);
        if (in_array($ext, array('xls'))) {
            if (move_uploaded_file($file["excel_file"]["tmp_name"], $path)) {
                $sheetData = $phpExcel->load($path)->getActiveSheet()->toArray(null, true, true, true);
                $headerCol = array('category', 'description', 'start_date', 'end_date', 'address');
                unlink($path);
                if (empty(array_diff($sheetData[1], $headerCol))) {
                    $Onsitetrial = $this->getServiceLocator()->get("OnsiteStudyTrial");
                    unset($sheetData[1]);
                    $data=array();
                    
                    if(!empty($sheetData)){
                        foreach($sheetData as $Sheet){
                            $Sheet=  array_combine($headerCol,$Sheet);
                            $Sheet['start_date']=date('Y-m-d',strtotime($Sheet['start_date']));
                            $Sheet['end_date']=date('Y-m-d',strtotime($Sheet['end_date']));
                            $Sheet['creation_date']=time();
                            $Onsitetrial->insert($Sheet);
                        }
                    }
                    
                }

            }
        }die;
    }
    
     /*     * *****
     * Action:- function to handle ajax call to delete Study & Trial
     * @author: Hemant Chuphal.
     * Created Date: 11-05-2015.
     * *** */
    function deleteAction(){
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $this->_studyTrialObj = $this->getServiceLocator()->get("OnsiteStudyTrial");
        if (($request->isXmlHttpRequest())) {
            if ($this->_studyTrialObj->delete(array('id' => $postDataArr['id']))) {
                echo json_encode(array('true', $postDataArr['id']));
            } else {
                echo json_encode(array('false'));
            }
        }
        die;
    }

}
