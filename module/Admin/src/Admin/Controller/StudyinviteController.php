<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE CONDITION PART OF ADMIN.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 2/3/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class StudyinviteController extends AbstractActionController {

    protected $_studiesObj;
    protected $_queAnsObj;
    var $objPHPExcel = array();

    public function init() {
//        $this->_layoutObj = $this->layout();
//        $this->_layoutObj->setTemplate('layouts/layout');
        $this->_sessionObj = new Container('super_admin');
    }

    /*     * *****
     * Action:-Function for cron.
     * @author: Hemant Chuphal.
     * Created Date: 14-5-2015.
     * *** */

    public function indexAction() {
        ini_set('max_execution_time', 7200);
        $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
        $this->_queAnsObj = $this->getServiceLocator()->get("StudyEligibleTable");
        $curDate = date('Y-m-d');
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $bucket = base64_decode($this->params()->fromQuery('bucket'));
        $type = $this->params()->fromQuery('type');
        $number = $this->params()->fromQuery('number');

        $data = array('study_id' => $sid, 'type' => $type, 'bucket' => $bucket, 'number' => $number);
        
        if ($this->_queAnsObj->insertAdminStudyBucketCronSetting($data)) {

            $StudyData = $this->_studiesObj->getAdminStudy(array('publish_status' => 1, 'study_id' => $sid), array('study_id', 'start_date', 'end_date', 'screening', 'study_title', 'created_by'), '0', array(), array(0, 1), $curDate);
            $Study = array_column($StudyData, 'study_id');
            $StudyPass = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'created_by'));
            $StudyType = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'screening'));
            $StudyName = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'study_title'));
            $StudyStartDate = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'start_date'));
            $StudyEndDate = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'end_date'));
            $user = array();
            $ids = array();

            if ($Study) {
                $StudyVarSet = $this->_studiesObj->getAdminStudyVarSet(array('set_study' => $Study), array('set_study', 'json', 'total_combination'));
                if (!empty($StudyVarSet)) {
                    foreach ($StudyVarSet as $SetData) {
                        $JsonArray = isset($SetData['json']) ? json_decode($SetData['json'], true) : array();
                        $conditionsArray = !empty($JsonArray) ? $this->_getCondition($JsonArray, $SetData['total_combination']) : array();
                        if (!empty($conditionsArray['Variables'])) {
                            foreach ($conditionsArray['Variables'] as $key => $Variables) {

                                $Cron = $this->_queAnsObj->getAdminStudyBucketCronSetting(array(
                                    'study_id' => $SetData['set_study'],
                                    'bucket' => $key,
                                    'sent' => 0
                                        ), array(
                                    'number', 'id'
                                        ), '0', array(), array(0, 2));

                                if (!empty($Cron)) {
                                    $limit = 0;
                                    foreach ($Cron as $number) {

                                        $existingUser = $this->_queAnsObj->getAdminStudyBucketInvitation(array('study_id' => $SetData['set_study'], 'bucket' => $key), array('user_id'));
                                        $existingUsers = array_combine(array_column($existingUser, 'user_id'), array_column($existingUser, 'user_id'));

                                        $user[$SetData['set_study']][$key][] = $this->_getEligibleUsersList($Variables, isset($conditionsArray['Common']) ? $conditionsArray['Common'] : array(), array($limit, $number['number']), $existingUsers, $StudyPass[$SetData['set_study']]);
                                        $limit = $limit + $number['number'];
                                        $ids[] = $number['id'];
                                    }
                                }
                            }
                        } else {
                            $Cron = $this->_queAnsObj->getAdminStudyBucketCronSetting(array(
                                'study_id' => $SetData['set_study'],
                                'bucket' => 0,
                                'sent' => 0
                                    ), array(
                                'number', 'id'
                                    ), '0', array(), array(0, 2));

                            if (!empty($Cron)) {
                                $limit = 0;
                                foreach ($Cron as $number) {

                                    $existingUser = $this->_queAnsObj->getAdminStudyBucketInvitation(array('study_id' => $SetData['set_study'], 'bucket' => 0), array('user_id'));
                                    $existingUsers = array_combine(array_column($existingUser, 'user_id'), array_column($existingUser, 'user_id'));

                                    $user[$SetData['set_study']][0][] = $this->_getEligibleUsersList(NULL, isset($conditionsArray['Common']) ? $conditionsArray['Common'] : array(), array($limit, $number['number']), $existingUsers, $StudyPass[$SetData['set_study']]);
                                    $limit = $limit + $number['number'];
                                    $ids[] = $number['id'];
                                }
                            }
                        }
                    }
                }
            }
            if (!empty($user)) {
                $data = array();

                foreach ($user as $key => $indexMain) {
                    $data['study_id'] = $key;
                    foreach ($indexMain as $bucket => $index) {
                        $data['bucket'] = $bucket;
                        $data['type'] = ($StudyType[$key]) ? 0 : 1;
                        $data['eligible_for_main'] = ($StudyType[$key]) ? 0 : 1;
                        $this->_saveAndSendInvitation($data, $this->_queAnsObj, $index, $StudyName[$data['study_id']], $StudyStartDate[$data['study_id']], $StudyEndDate[$data['study_id']]);
                    }
                }
            }
            if (!empty($ids)) {
                $this->_queAnsObj->updateAdminStudyBucketCronSetting(array('id' => $ids), array('sent' => 1));
            }
        }
        return $this->redirect()->toUrl("/admin-studies-eligible-users?sid=" . base64_encode($sid));
    }

    /*     * *****
     * Action:- Send Email AND Save Invitation
     * @author: Hemant Chuphal.
     * Created Date: 13-04-2015.
     * *** */

    function _saveAndSendInvitation($data = NULL, $Obj = NULL, $index = NULL, $studyName, $startDate, $endDate) {
        $plugin = $this->PluginController();

        foreach ($index as $bucket => $users) {
            if (!empty($users)) {
                foreach ($users as $id => $email) {
                    $data['user_id'] = $id;
                    $data['creation_date'] = time();

                    $Obj->insertAdminStudyBucketInvitation($data);
                }
                $Url = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . '/individual-study';
                $message = "Dear User,<br>";
                $message .= "&nbsp;&nbsp;&nbsp;&nbsp;You are invited to attempt Study name ($studyName).<br>";
                $message .= "&nbsp;&nbsp;&nbsp;&nbsp;Start Date -$startDate To End Date-$endDate.<br>";
                $message .= "&nbsp;&nbsp;&nbsp;&nbsp;Study Url: $Url.<br>";
                $subject = SITE_NAME." : Study Invitation";
                $plugin->sendMailAdmin($users, $subject, $message);
                sleep(.5);
            }
            sleep(1);
        }
    }

    /*     * *****
     * Action:- Get Eligible Users for study
     * @author: Hemant Chuphal.
     * Created Date: 13-04-2015.
     * *** */

    function _getEligibleUsersList($VarData, $CommonData, $limit, $existingUsers, $pass) {
        $this->_queAnsObj = $this->getServiceLocator()->get("StudyEligibleTable");
        $where = array();
        $cols = array('UserID', 'Email');
        $VarResultData = array();

        if (!empty($VarData)) {
            $i = 0;
            if (is_array($VarData)) {
                foreach ($VarData as $Var) {
                    $Var = json_decode($Var, TRUE);
                    $where[$i] = array('ques_variable' => $Var[0], 'option_id' => $Var[1]);
                    $i++;
                }
            } else {
                $Var = json_decode($VarData, TRUE);
                $where[$i] = array('ques_variable' => $Var[0], 'option_id' => $Var[1]);
                $i++;
            }

            $VarResultData = $this->_queAnsObj->getAdminVarStudyEligibleUser($where, $cols, '1', $CommonData, $limit, '0', '1', $existingUsers, $pass);
        } else {
            $VarResultData = $this->_queAnsObj->getAdminVarStudyEligibleUser(NULL, $cols, '1', $CommonData, $limit, '0', '1', $existingUsers, $pass);
        }
        sleep(.5);
        return array_combine(array_column($VarResultData, 'UserID'), array_column($VarResultData, 'Email'));
    }

    /*     * *****
     * Action:- Get condition set for study
     * @author: Hemant Chuphal.
     * Created Date: 13-04-2015.
     * *** */

    function _getCondition($JsonArray, $totalComb = NULL) {
        $result = array();
        if ($totalComb < 16) {
            foreach ($JsonArray as $key => $Data) {
                if (isset($Data[2])) {
                    $var_id = $Data[2]['var_id'];
                    $resultData = array();
                    array_walk($Data[2]['option_id'], function($value) use (& $var_id, & $resultData) {
                        $resultData[] = json_encode(array($var_id, $value));
                    });
                    $result['Variables'][$var_id] = $resultData;
                    unset($JsonArray[$key]);
                }
            }
        } else {
            $resultData = array();
            foreach ($JsonArray as $key => $Data) {
                if (isset($Data[2])) {
                    $var_id = $Data[2]['var_id'];

                    array_walk($Data[2]['option_id'], function($value) use (& $var_id, & $resultData) {
                        $resultData = array_merge($resultData, array(json_encode(array($var_id, $value))));
                    });


                    unset($JsonArray[$key]);
                }
            }
            $result['Variables'][$var_id] = $resultData;
        }


        $result['Variables'] = !empty($result['Variables']) ? array_values($result['Variables']) : array();
        $result['Variables'] = ($totalComb < 16) ? $this->_combinations($result['Variables']) : $result['Variables'];
        $result['Common'] = $JsonArray;

        return $result;
    }

    /*     * *****
     * Action:- get combination based on varrable for study
     * @author: Hemant Chuphal.
     * Created Date: 10-04-2015.
     * *** */

    function _combinations($arrays, $i = 0) {
        if (!isset($arrays[$i])) {
            return array();
        }
        if ($i == count($arrays) - 1) {
            return $arrays[$i];
        }
        $tmp = $this->_combinations($arrays, $i + 1);

        $result = array();
        foreach ($arrays[$i] as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t) ?
                        array_merge(array($v), $t) :
                        array($v, $t);
            }
        }

        return $result;
    }

}
