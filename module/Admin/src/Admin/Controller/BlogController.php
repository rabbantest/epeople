<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE BLOG PART OF ADMIN.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 25/03/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Admin\Model\BlogTable;
use Admin\Form\BlogForm;
use Admin\Form\BlogValidator;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class BlogController extends AbstractActionController {

    protected $_layoutObj;
    protected $_sessionObj;
    protected $_blogObj;
    protected $_catObj;
    protected $_authObj;

    public function init() {

        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');

        $this->_sessionObj = new Container('super_admin');
        if ($this->_sessionObj->admin['id'] != 1) {
            return $this->redirect()->toRoute('admin');
        }
    }

    /*     * *****
     * Action:- Use to manage blog section here.
     * @author: Shivendra Suman.
     * Created Date: 25/03/2015.
     *  
     * *** */

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        // $user_id = $this->_sessionObj->admin['id'];   
        $this->_blogObj = $this->getServiceLocator()->get("BlogTable");
        $PluginController = $this->PluginController();  // custome plugin
        $sort = $this->params()->fromQuery("sort");

        $whereBlog = array();
        $coloumblog = array();
        $lookingfor = "creation_date DESC";

        //sort by 
        if (!empty($sort)) {
            if ($sort == 'monthd') {
                $lookingfor = "creation_date DESC";
            }
            if ($sort == 'montha') {
                $lookingfor = "creation_date ASC";
            }
            if ($sort == 'cata') {
                $lookingfor = "cat.cat_name ASC";
            }
            if ($sort == 'catd') {
                $lookingfor = "cat.cat_name DESC";
            }
            if ($sort == 'autha') {
                $lookingfor = "auth.author_name ASC";
            }
            if ($sort == 'authd') {
                $lookingfor = "auth.author_name DESC";
            }
        }
        $blogData = $this->_blogObj->getBlog($whereBlog, $coloumblog, $lookingfor);

        $searchcontent = array("sort" => $sort);
        $itemsPerPage = 10;
        //pagination data 
        $paginator = $this->paginataAction($blogData, $itemsPerPage);

        $sendArr = array(
            'pageTitle' => "Blog List",
            'blogData' => $paginator,
            'sort' => $sort,
            'PluginController' => $PluginController,
            'routeParams' => $searchcontent,
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- Use to add blog section here.
     * @author: Shivendra Suman.
     * Created Date: 25/03/2015.
     *  
     * *** */

    public function addblogAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $this->_blogObj = $this->getServiceLocator()->get("BlogTable");
        $form = new BlogForm();
        $request = $this->getRequest();
        $err = 0;
        $blog_images = '';
        if ($request->isPost()) {

            $formValidator = new BlogValidator();
            $form->setInputFilter($formValidator->getInputFilter());
            $form->setData($request->getPost());
            $fileis = $this->getRequest()->getFiles()->toArray();
            // upload image
            if ($fileis["blog_image"]["size"] > 0) {
                $extvalidate = new \Zend\Validator\File\Extension(array('extension' => array('jpg', 'png', 'gif', 'jpeg', 'JPG', 'PNG', 'GIF', 'JPEG')));
                $adapter = new \Zend\File\Transfer\Adapter\Http($fileis['blog_image']['name']);
                $adapter->addValidator($extvalidate);
                //$adapter->addValidator($size);
                if ($adapter->isValid()) {
                    $fileName = $fileis['blog_image']['name'];
                    $tmp_name = $fileis['blog_image']['tmp_name'];
                    $dir = BASE_PATH . "/uploads/blogs";
                    if (!is_dir($dir)) {
                        @mkdir($dir, 777);
                        @chmod($dir, 0777);
                    }
                    $newfilename = md5(rand() * time());
                    $file_name = strtolower(basename($fileName));
                    $ext = substr($file_name, strrpos($file_name, '.') + 1);
                    $ext = "." . $ext;
                    $returnName = $newfilename . $ext;
                    $newname = $dir . "/" . $newfilename . $ext;
                    if (move_uploaded_file($tmp_name, $newname)) {
                        $blog_images = $returnName;
                    } else {
                        $err = 1;
                        $message = "Unable to upload image please try again !";
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                } else {
                    $err = 1;
                    $message = "Please upload a valid image (jpg, png, gif, jpeg) !";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            }

            if ($form->isValid() && $err == 0) {
                $postDataArr = $this->getRequest()->getPost()->toArray();
                $fileis = $this->getRequest()->getFiles()->toArray();
                $title = $postDataArr['title'];
                $long_description = $postDataArr['long_description'];
                $short_description = $postDataArr['short_description'];
                $category_id = $postDataArr['category_id'];
                $category = $postDataArr['category'];
                $author_id = $postDataArr['author_id'];
                $author = $postDataArr['author'];
                $status = $postDataArr['status'];
                $blog_image = $blog_images;
                // $published_status = $postDataArr['published_status'];
                // chek is categor if not then create 
                $CatArr = $this->_blogObj->getCat(array("cat_id" => $category_id, "cat_name" => $category), array('cat_id', 'cat_name'));
                if (count($CatArr) <= 0) {
                    $catIns = $this->_blogObj->insertCat(array("cat_name" => $category, "creation_date" => time()));
                    if ($catIns) {
                        $category_id = $catIns; //created cat id
                    } else {

                        $message = "category not created please tray again !";
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                }

                $AuthArr = $this->_blogObj->getAuthor(array("author_id" => $author_id, "author_name" => $author), array('author_id', 'author_name'));
                if (count($AuthArr) <= 0) {
                    $authIns = $this->_blogObj->insertAuthor(array("author_name" => $author, "creation_date" => time()));
                    if ($authIns) {
                        $author_id = $authIns; //created autor id 
                    } else {
                        $message = "author not created please tray again !";
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                }

                $saveBlog = array(
                    'title' => $title,
                    'cat_id' => $category_id,
                    'author_id' => $author_id,
                    'short_description' => addslashes($short_description),
                    'long_description' => addslashes($long_description),
                    'status' => $status,
                    'blog_image' => $blog_image,
                    // 'published_status' => $published_status,
                    'published_date' => time(),
                    'creation_date' => time()
                );
                $savBlog = $this->_blogObj->insertBlog($saveBlog);
                if ($savBlog) {
                    $message = "Blog sucessfully created";
                    $this->flashMessenger()->addMessage(array('success' => $message));
                    return $this->redirect()->toRoute('Defalt BlogList');
                } else {
                    $message = "Some error occured, please tray again !";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            } else {
                $message = "Some error occured, please tray again !";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }

        $sendArr = array(
            'pageTitle' => "Add Blog",
            'session' => $this->_sessionObj->admin,
            'form' => $form
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- Use to add blog section here.
     * @author: Shivendra Suman.
     * Created Date: 25/03/2015.
     *  
     * *** */

    public function editblogAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $this->_blogObj = $this->getServiceLocator()->get("BlogTable");
        $PluginController = $this->PluginController();  // custome plugin
        $Id = $this->params('id');
        $blogId = $PluginController->decryptdata($Id); // insurance id decode
        $form = new BlogForm();
        $err = 0;
        $blog_images = '';
        //display data
        if (!empty($blogId)) {
            $whereBlog = array('blog_id' => $blogId);
            $blogs = $this->_blogObj->getBlog($whereBlog);
            if (count($blogs) > 0) {
                //   echo "suman";exit;

                $blogsetarray = array();
                foreach ($blogs as $bvalue) {
                    $blogsetarray = array(
                        'blog_id' => $bvalue['blog_id'],
                        'title' => $bvalue['title'],
                        'category_id' => $bvalue['cat_id'],
                        'category' => $bvalue['cat_name'],
                        'author_id' => $bvalue['author_id'],
                        'author' => $bvalue['author_name'],
                        'short_description' => stripcslashes($bvalue['short_description']),
                        'long_description' => stripcslashes($bvalue['long_description']),
                        'status' => $bvalue['status'],
                        'blog_image' => $bvalue['blog_image'],
                    );
                }
                $form->setData($blogsetarray);
            } else {
                //send error if blog id not found
                $this->getResponse()->setStatusCode(404);
                return;
            }
        } else {
            //send error if blog id not found
            $this->getResponse()->setStatusCode(404);
            return;
        }

        //update data 
        //update 
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formValidator = new BlogValidator();
            $form->setInputFilter($formValidator->getInputFilter());
            $form->setData($request->getPost());
            $fileis = $this->getRequest()->getFiles()->toArray();

            // upload image
            if ($fileis["blog_image"]["size"] > 0) {
                $extvalidate = new \Zend\Validator\File\Extension(array('extension' => array('jpg', 'png', 'gif', 'jpeg', 'JPG', 'PNG', 'GIF', 'JPEG')));
                $adapter = new \Zend\File\Transfer\Adapter\Http($fileis['blog_image']['name']);
                $adapter->addValidator($extvalidate);
                //$adapter->addValidator($size);
                if ($adapter->isValid()) {
                    $fileName = $fileis['blog_image']['name'];
                    $tmp_name = $fileis['blog_image']['tmp_name'];
                    $dir = BASE_PATH . "/uploads/blogs";
                    if (!is_dir($dir)) {
                        @mkdir($dir, 777);
                        @chmod($dir, 0777);
                    }
                    $newfilename = md5(rand() * time());
                    $file_name = strtolower(basename($fileName));
                    $ext = substr($file_name, strrpos($file_name, '.') + 1);
                    $ext = "." . $ext;
                    $returnName = $newfilename . $ext;
                    $newname = $dir . "/" . $newfilename . $ext;
                    if (move_uploaded_file($tmp_name, $newname)) {
                        $blog_images = $returnName;
                    } else {

                        $err = 1;
                        $message = "Unable to upload image please try again !";
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                } else {
                    $err = 1;
                    $message = "Please upload a valid image (jpg, png, gif, jpeg) !";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            }

            if ($form->isValid()) {
                $postDataArr = $this->getRequest()->getPost()->toArray();

                $title = $postDataArr['title'];
                $blog_id = $postDataArr['blog_id'];
                $long_description = $postDataArr['long_description'];
                $short_description = $postDataArr['short_description'];
                $category_id = $postDataArr['category_id'];
                $category = $postDataArr['category'];
                $author_id = $postDataArr['author_id'];
                $author = $postDataArr['author'];
                $status = $postDataArr['status'];
                $blog_image = $blog_images;
                // $published_status = $postDataArr['published_status'];
                // chek is categor if not then create 
                $CatArr = $this->_blogObj->getCat(array("cat_id" => $category_id, "cat_name" => $category), array('cat_id', 'cat_name'));
                if (count($CatArr) <= 0) {
                    $catIns = $this->_blogObj->insertCat(array("cat_name" => $category, "creation_date" => time()));
                    if ($catIns) {
                        $category_id = $catIns; //created cat id
                    } else {

                        $message = "category not created please tray again !";
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                }

                $AuthArr = $this->_blogObj->getAuthor(array("author_id" => $author_id, "author_name" => $author), array('author_id', 'author_name'));
                if (count($AuthArr) <= 0) {
                    $authIns = $this->_blogObj->insertAuthor(array("author_name" => $author, "creation_date" => time()));
                    if ($authIns) {
                        $author_id = $authIns; //created autor id 
                    } else {
                        $message = "author not created please tray again !";
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                }
                if (!empty($blog_image)) {
                    $updateBlog = array(
                        'title' => $title,
                        'cat_id' => $category_id,
                        'author_id' => $author_id,
                        'short_description' => addslashes($short_description),
                        'long_description' => addslashes($long_description),
                        'status' => $status,
                        'blog_image' => $blog_image,
                        'updation_date' => time()
                    );
                } else {
                    $updateBlog = array(
                        'title' => $title,
                        'cat_id' => $category_id,
                        'author_id' => $author_id,
                        'short_description' => addslashes($short_description),
                        'long_description' => addslashes($long_description),
                        'status' => $status,
                        'updation_date' => time()
                    );
                }
                $whereB = array("blog_id" => $blog_id);
                //   $savBlog = $this->_blogObj->insertBlog($saveBlog);
                $updateB = $this->_blogObj->updateBlog($whereB, $updateBlog);
                if ($updateB) {
                    $message = "Blog sucessfully updated";
                    $this->flashMessenger()->addMessage(array('success' => $message));
                    return $this->redirect()->toRoute('Defalt BlogList');
                } else {
                    $message = "Some error occured, please tray again !";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                    $redirectUrl = "/admin-edit-blogs/$PluginController->encryptdata($blog_id)";
                    return $this->redirect()->toUrl($redirectUrl);
                }
            } else {
                $message = "Some error occured, please tray again !";
                $this->flashMessenger()->addMessage(array('error' => $message));
//                        $redirectUrl = "/admin-edit-blogs/$PluginController->encryptdata($blog_id)";
//                        return  $this->redirect()->toUrl($redirectUrl);
            }
        }

        $sendArr = array(
            'pageTitle' => "Edit Blog",
            'session' => $this->_sessionObj->admin,
            'form' => $form,
            'blogId' => $Id
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- Use to manage Blog  details   here.
     * @author: Shivendra Suman.
     * Created Date: 30/03/2015.
     *  
     * *** */

    public function viewblogAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        // $user_id = $this->_sessionObj->admin['id'];   

        $this->_blogObj = $this->getServiceLocator()->get("BlogTable");
        $PluginController = $this->PluginController();  // custome plugin
        $Id = $this->params('id');
        //$Id = base64_decode($this->params()->fromQuery('id'));
        //$blogId=base64_decode($Id);
        $blogId = base64_decode($Id); // insurance id decode
        if (!empty($blogId)) {
            $whereBlog = array('blog_id' => $blogId);
            $blogs = $this->_blogObj->getBlog($whereBlog);
           
            if (count($blogs) > 0) {
//            $where = array('cat_id'=>$blogs[0]['cat_id']);
//            $columns=array('cat_id','cat_name');
//            $blog_cat =   $this->_blogObj->getCat($where, $columns);
//            $blogs[0]['cat_name'] = (isset($blog_cat[0]['cat_name'])? $blog_cat[0]['cat_name'] : '' );    
//          
//             $whereAuth = array('author_id'=>$blogs[0]['author_id']);
//            $columnsAuth=array('author_id','author_name');
//            
//            $blog_auth =   $this->_blogObj->getAuthor($whereAuth, $columnsAuth);
//            $blogs[0]['author_name'] = (isset($blog_auth[0]['author_name'])? $blog_auth[0]['author_name'] : '' );  

                $blogData = $blogs[0];
            } else {
                //send error if insurance id not found
                $this->getResponse()->setStatusCode(404);
                return;
            }
        } else {
            //send error if insurance id not found
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $sendArr = array(
            'pageTitle' => "Blog List",
            'blogData' => $blogData,
            // 'sort' => $sort,
            'PluginController' => $PluginController,
            'blogId' => $Id,
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- Use to manage author  list here.
     * @author: Shivendra Suman.
     * Created Date: 25/03/2015.
     *  
     * *** */

    protected function authorlistAction() {
        $postData = $_GET;
        $this->_authObj = $this->getServiceLocator()->get("BlogTable");
        $whereArr = array();
        $columns = array("author_id", "author_name");
        $searchArr = array("author_name" => strtolower($postData['author_name']));
        $nameArr = $this->_authObj->getAuthor($whereArr, $columns, "0", $searchArr);
        $sendArr = array(
            'name' => $nameArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    /*     * *****
     * Action:- Use to manage author  list here.
     * @author: Shivendra Suman.
     * Created Date: 25/03/2015.
     *  
     * *** */

    protected function catlistAction() {
        $postData = $_GET;
        $this->_catObj = $this->getServiceLocator()->get("BlogTable");
        $whereArr = array();
        $columns = array("cat_id", "cat_name");
        $searchArr = array("cat_name" => strtolower($postData['cat_name']));
        $nameArr = $this->_catObj->getCat($whereArr, $columns, "0", $searchArr);
        $sendArr = array(
            'name' => $nameArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    /*     * *****
     * Action:- Use to manage pagination section here.
     * @author: Shivendra Suman.
     * Created Date: 25/03/2015.
     *  
     * *** */

    public function paginataAction($dataArray, $itemsPerPage) {
        if (is_array($dataArray)) {
            $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($dataArray));
            $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
            $paginator->setItemCountPerPage($itemsPerPage);
            return $paginator;
        } else {
            return false;
        }
    }

}
