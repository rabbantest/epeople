<?php
/*************************
    * PAGE: USE TO MANAGE THE CONDITION PART OF ADMIN.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: ANKIT SHUKLA(fb/gshukla67).
    * CREATOR: 26/12/2014.
    * UPDATED: 30/12/2014.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class ConditionController extends AbstractActionController
{
    
    protected $_layoutObj;
    protected $_sessionObj;

    public function init(){

        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');

        $this->_sessionObj = new Container('super_admin');
        if ($this->_sessionObj->admin['id'] != 1) {
            return $this->redirect()->toRoute('admin');
    }
    }

    /*     * *****
        * Action:- Use to manage Conditions section here.
        * @author: Ankit Shukla(fb/gshukla67).
        * Created Date: 26-12-2014.
    *****/
    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $request = $this->getRequest();

        if($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if ($postDataArr['candi_search'] != "") {
                $searchContent = array("common_name"=>$postDataArr['candi_search']);
            }
        } else {
            $searchContent = array();
            $postDataArr = array();
        }

        $IndCondiTableObj = $this->getServiceLocator()->get("ConditionTable");
        $columns = array("id", "common_name");
        $whereArr = array("status"=>1);
        $conditionsArr = $IndCondiTableObj->getAdminConditions($whereArr, $columns, "0", $searchContent);


        $sendArr = array(
            'postData'=>$postDataArr, 
            'conditions'=>$conditionsArr, 
            'session'=>$this->_sessionObj->admin, 
        );
        return new ViewModel($sendArr);
    }

    /*******
        * Action:- Use to manage Symptoms section here.
        * @author: Ankit Shukla(fb/gshukla67).
        * Created Date: 26-12-2014.
    *****/
    public function symptomsAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $request = $this->getRequest(); 

        if($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if ($postDataArr['sympt_search'] != "") {
                $searchContent = array("common_name"=>$postDataArr['sympt_search']);
            }
        } else {
            $searchContent = array();
            $postDataArr = array();
        }

        $SymptomsTable = $this->getServiceLocator()->get("SymptomsTable");
        $columns = array("id", "common_name");
        $whereArr = array("status"=>1);
        $symptomsArr = $SymptomsTable->getAdminSymptoms($whereArr, $columns, "0", $searchContent);
        $columns = array("othr_id", "othr_common_name");
        $whereArr = array("othr_status"=>1);
        $otherSymptomsArr = $SymptomsTable->getAdminOtherSymptoms($whereArr, $columns, "0", $searchContent);

        $sendArr = array(
            'postData'=>$postDataArr, 
            'symptoms'=>$symptomsArr, 
            'otherSymptoms'=>$otherSymptomsArr, 
            'session'=>$this->_sessionObj->admin, 
        );
        return new ViewModel($sendArr);
    }

    /*******
        * Action:- Use to manage Treatments section here.
        * @author: Ankit Shukla(fb/gshukla67).
        * Created Date: 26-12-2014.
    *****/
    public function treatmentsAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $request = $this->getRequest(); 

        if($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if ($postDataArr['sympt_search'] != "") {
                $searchContent = array("common_name"=>$postDataArr['sympt_search']);
            }
        } else {
            $searchContent = array();
            $postDataArr = array();
        }

        $TreatmentsTable = $this->getServiceLocator()->get("TreatmentsTable");
        $columns = array("id", "common_name");
        $whereArr = array("status"=>1);
        $treatmentsArr = $TreatmentsTable->getAdminTreatments($whereArr, $columns, "0", $searchContent);
        $columns = array("othr_id", "othr_common_name");
        $whereArr = array("othr_status"=>1);
        $otherTreatmentsArr = $TreatmentsTable->getAdminOtherTreatments($whereArr, $columns, "0", $searchContent);

        $sendArr = array(
            'postData'=>$postDataArr, 
            'treatments'=>$treatmentsArr, 
            'otherTreatments'=>$otherTreatmentsArr, 
            'session'=>$this->_sessionObj->admin, 
        );
        return new ViewModel($sendArr);
    }

    protected function conditiondescriptionAction() {
        $postData = $_GET;
        $ConditionTable = $this->getServiceLocator()->get("ConditionTable");
        $columns = array("id", "media_name", "common_name", "code_minrange", "code_maxrange", "description");
        $whereArr = array(
            "id"=>$postData['id'],
            "status"=>1
        );
        $descriptionArr = $ConditionTable->getAdminConditions($whereArr, $columns, "1");

        $sendArr = array(
            'description'=>$descriptionArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function subconditiondescriptionAction() {
        $postData = $_GET;
        $ConditionTable = $this->getServiceLocator()->get("ConditionTable");
        $columns = array("sub_id", "sub_media_name", "sub_common_name", "unique_code", "sub_description");
        $whereArr = array(
            "sub_id"=>$postData['id'],
            "sub_status"=>1
        );
        $descriptionArr = $ConditionTable->getAdminSubConditions($whereArr, $columns, 0);
        // echo '<pre>';print_r($descriptionArr); die;

        $sendArr = array(
            'condition'=>$postData['type'],
            'description'=>$descriptionArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }
    
    protected function symptomdescriptionAction() {
        $postData = $_GET;
        $SymptomsTable = $this->getServiceLocator()->get("SymptomsTable");
        $columns = array("id", "media_name", "common_name", "description");
        $whereArr = array(
            "id"=>$postData['id'],
            "status"=>1
        );
        $descriptionArr = $SymptomsTable->getAdminSymptoms($whereArr, $columns, "1");

        $sendArr = array(
            'description'=>$descriptionArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function subsymptomdescriptionAction() {
        $postData = $_GET;
        $SymptomsTable = $this->getServiceLocator()->get("SymptomsTable");
        $columns = array("sub_id", "sub_media_name", "sub_common_name", "sub_description");
        $whereArr = array(
            "sub_id"=>$postData['id'],
            "sub_status"=>1
        );
        $descriptionArr = $SymptomsTable->getAdminSubSymptoms($whereArr, $columns, 0);

        $sendArr = array(
            'symptom'=>$postData['type'],
            'description'=>$descriptionArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function symptomdeleteAction () {
        $postData = $_POST;
        $SymptomsTable = $this->getServiceLocator()->get("SymptomsTable");
        if ($postData['type'] == "mn") {
            // print_r($postData); die;
            $whereArr = array(
                "id"=>$postData['id']
            );
            $SymptomsTable->deleteAdminSymptoms($whereArr);
            echo "This record has been deleted successfully.";
        }
        if ($postData['type'] == "sb") {
            // print_r($postData); die;
            $whereArr = array(
                "sub_id"=>$postData['id']
            );
            $SymptomsTable->deleteAdminSubSymptoms($whereArr);
        }
        die;
    }

    protected function addsymptomsAction () {
        $id = 0;
        $postData = $_POST;
        $SymptomsTable = $this->getServiceLocator()->get("SymptomsTable");
        if ($postData['type'] == "mn") {
            $data = array(
                "media_name"=>$postData['media'],
                "common_name"=>$postData['common'],
                "description"=>$postData['description'],
                "status"=>1,
                "creation_date"=>time(),
            );
            $id = $SymptomsTable->insertAdminSymptoms($data);
        }
        if ($postData['type'] == "sb") {
            $data = array(
                "symptoms_id"=>$postData['id'],
                "sub_media_name"=>$postData['media'],
                "sub_common_name"=>$postData['common'],
                "sub_description"=>$postData['description'],
                "sub_status"=>1,
                "creation_date"=>time(),
            );
            $id = $SymptomsTable->insertAdminSubSymptoms($data);
        }
        if ($id)
            echo 1;
        else
            echo 0;
        die;
    }

    protected function addtreatmentsAction () {
        $id = 0;
        $postData = $_POST;
        $TreatmentsTable = $this->getServiceLocator()->get("TreatmentsTable");
        if ($postData['type'] == "mn") {
            $data = array(
                "media_name"=>$postData['media'],
                "common_name"=>$postData['common'],
                "description"=>$postData['description'],
                "status"=>1,
                "creation_date"=>time(),
            );
            $id = $TreatmentsTable->insertAdminTreatment($data);
        }
        if ($postData['type'] == "sb") {
            $data = array(
                "treatments_id"=>$postData['id'],
                "sub_media_name"=>$postData['media'],
                "sub_common_name"=>$postData['common'],
                "sub_description"=>$postData['description'],
                "sub_status"=>1,
                "creation_date"=>time(),
            );
            $id = $TreatmentsTable->insertAdminSubTreatment($data);
        }
        if ($id)
            echo 1;
        else
            echo 0;
        die;
    }

    protected function treatmentdescriptionAction() {
        $postData = $_GET;
        $TreatmentsTable = $this->getServiceLocator()->get("TreatmentsTable");
        $columns = array("id", "media_name", "common_name", "description");
        $whereArr = array(
            "id"=>$postData['id'],
            "status"=>1
        );
        $descriptionArr = $TreatmentsTable->getAdminTreatments($whereArr, $columns, "1");

        $sendArr = array(
            'description'=>$descriptionArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function subtreatmentdescriptionAction() {
        $postData = $_GET;
        $TreatmentsTable = $this->getServiceLocator()->get("TreatmentsTable");
        $columns = array("sub_id", "sub_media_name", "sub_common_name", "sub_description");
        $whereArr = array(
            "sub_id"=>$postData['id'],
            "sub_status"=>1
        );
        $descriptionArr = $TreatmentsTable->getAdminSubTreatments($whereArr, $columns, 0);

        $sendArr = array(
            'symptom'=>$postData['type'],
            'description'=>$descriptionArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function treatmentdeleteAction () {
        $postData = $_POST;
        $TreatmentsTable = $this->getServiceLocator()->get("TreatmentsTable");
        if ($postData['type'] == "mn") {
            // print_r($postData); die;
            $whereArr = array(
                "id"=>$postData['id']
            );
            $TreatmentsTable->deleteAdminTreatments($whereArr);
            echo "This record has been deleted successfully.";
        }
        if ($postData['type'] == "sb") {
            // print_r($postData); die;
            $whereArr = array(
                "sub_id"=>$postData['id']
            );
            $TreatmentsTable->deleteAdminSubTreatments($whereArr);
        }
        die;
    }
}
