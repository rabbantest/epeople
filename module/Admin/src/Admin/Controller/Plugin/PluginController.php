<?php

namespace Admin\Controller\Plugin;

/* * ***********************
 * PHTML: USE TO MANAGE THE THIRD PARTY FUNCTION LIKE MAIL, DATE FORMATE etc ON ADMIN HEALTH MATRIX.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: ANKIT SHUKLA(fb/gshukla67).
 * CREATOR: 23/12/2014.
 * UPDATED: 23/12/2014.
 *
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PluginController extends AbstractPlugin {

    var $emailfromtext;
    var $subject;
    var $emailto;
    var $message;
    var $emailtotext;
    var $port;
    var $emailFrom;
    var $host;
    var $password;
    var $from;
    var $ssl;
    var $EmailConfig;
    protected $messageObj;
    protected $serviceLocator;

    public function __construct() {
        $this->port = 587;
        $this->emailFrom = "AKIAJBM7REUBMKQMFPRQ";
        $this->host = "email-smtp.us-east-1.amazonaws.com";
        $this->emailSentFrom = "no-reply@quantextualhealth.com";
        $this->password = "AoCmpywXjBxSLdxi8u14llGoF4uODEJdJKpQQgxPV80v";
        $this->from = "Admin";
        $this->ssl = "tls";
        $this->EmailConfig = array(
            'host' => $this->host,
            'port' => $this->port,
            'connection_class' => 'login',
            'connection_config' => array(
                'ssl' => $this->ssl,
                'username' => $this->emailFrom,
                'password' => $this->password,
            ),
        );
        
    }

    function sendMailAdmin($email, $subject, $message,$companyName='Quantextual Health') {
        // Set html with message.
        $this->messageObj = new Message();
        
        $message=str_replace('{message}', $message, EMAIL_TEMPLATE);
        
        $html = new MimePart($message);
        $html->type = "text/html";

        // Add html with message.
        $body = new MimeMessage();

        $body->addPart($html);
        
        if (is_array($email)) {
            $this->messageObj->addTo('no-reply@quantextualhealth.com');
            foreach ($email as $Emails) {
                $this->messageObj->addBcc($Emails);
                
            }
            
            //$this->messageObj->addCc($email);
            $this->messageObj->addFrom($this->emailSentFrom, $companyName)
                    ->setSubject($subject)
                    ->setBody($body);
        } else {
            // Add rest message part like to, from, subject etc..
            $this->messageObj->addTo($email)
                    ->addFrom($this->emailSentFrom, $companyName)
                    ->setSubject($subject)
                    ->setBody($body);
        }
        
        // Set smtp transport mail setup.
        $transport = new SmtpTransport();
        $options = new SmtpOptions($this->EmailConfig);
        $transport->setOptions($options);
        
        // SMTP MAIL  START HERE.
        try {
            $transport->send($this->messageObj);
            $mailMes = 1;
            return true;
        } catch (Exception $e) {
            pr($e->getMessage());
            $mailMes = 0;
            return false;
        }
    }

    function encryptdata($text, $salt = "healthmetrix") {
        return base64_encode($text);
        //return  base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($salt), $text, MCRYPT_MODE_CBC, md5(md5($salt))));
        // return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }

    function decryptdata($text, $salt = "healthmetrix") {
        return base64_decode($text);
        //return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($salt), base64_decode($text), MCRYPT_MODE_CBC, md5(md5($salt))), "\0");
        //return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }

}
