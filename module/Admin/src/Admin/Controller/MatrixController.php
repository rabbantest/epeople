<?php

/* * ***********************
 * PAGE: CONTROLLER USE TO MANAGE THE USER FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: ANKIT SHUKLA(fb/gshukla67).
 * CREATOR: 11/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class MatrixController extends AbstractActionController {

    protected $_layoutObj;
    protected $_sessionObj;

    public function init() {
        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');
        $this->_sessionObj = new Container('super_admin');
    }

    /*     * *****
     * Action:- Use to list of user and searching on it.
     * @author: Ankit Shukla(fb/gshukla67).
     * Created Date: 11-12-2014.
     * *** */

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $request = $this->getRequest();
        $BodyMeasurementTable = $this->getServiceLocator()->get("BodyMeasurementTable");
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if (empty($postDataArr['min_ht_feet']) && empty($postDataArr['min_ht_inches']) && empty($postDataArr['max_ht_feet']) && empty($postDataArr['max_ht_inches']) && empty($postDataArr['min_wast']) && empty($postDataArr['max_wast']) && empty($postDataArr['min_weight']) && empty($postDataArr['max_weight'])
            ) {
                $message = "Please fill the HEIGHT, WAIST and WEIGHT fields.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            } else {
                $minHeigthFeetTpCM = ($postDataArr['min_ht_feet'] * 30.48) + ($postDataArr['min_ht_inches'] * 2.54);
                $minHeigthInchTpCM = $postDataArr['min_ht_inches'] * 2.54;
                $maxHeigthFeetTpCM = ($postDataArr['max_ht_feet'] * 30.48) + ($postDataArr['max_ht_inches'] * 2.54);
                $maxHeigthInchTpCM = $postDataArr['max_ht_inches'] * 2.54;
                $minWeightInGram = $postDataArr['min_weight'] * 453.592;
                $maxWeightInGram = $postDataArr['max_weight'] * 453.592;
                if (!count($BodyMeasurementTable->getBodyMeasurement(array("status" => 1, "created_by" => $user_id), array("id")))) {
                    $data = array(
                        "min_height" => $minHeigthFeetTpCM,
                        "min_inch_height" => $minHeigthInchTpCM,
                        "max_height" => $maxHeigthFeetTpCM,
                        "max_inch_height" => $maxHeigthInchTpCM,
                        "min_wast" => $postDataArr['min_wast'],
                        "max_wast" => $postDataArr['max_wast'],
                        "min_weight" => $minWeightInGram,
                        "max_weight" => $maxWeightInGram,
                        "status" => 1,
                        "creation_date" => time(),
                        "created_by" => $user_id,
                    );
                    $insertData = $BodyMeasurementTable->insertBodyMeasurement($data);
                } else {
                    $data = array(
                        "min_height" => $minHeigthFeetTpCM,
                        "min_inch_height" => $minHeigthInchTpCM,
                        "max_height" => $maxHeigthFeetTpCM,
                        "max_inch_height" => $maxHeigthInchTpCM,
                        "min_wast" => $postDataArr['min_wast'],
                        "max_wast" => $postDataArr['max_wast'],
                        "min_weight" => $minWeightInGram,
                        "max_weight" => $maxWeightInGram,
                        "updation_date" => time(),
                        "created_by" => $user_id,
                    );
                    $whereArr = array("created_by" => $user_id, "status" => 1);
                    $insertData = $BodyMeasurementTable->updateBodyMeasurement($whereArr, $data);
                }
            }
        }
        $columns = array("id", "min_height", "min_inch_height", "max_height", "max_inch_height", "min_wast", "max_wast", "min_weight", "max_weight");
        $whereArr = array("status" => 1, "created_by" => $user_id);
        $bodyMeasurementArr = $BodyMeasurementTable->getBodyMeasurement($whereArr, $columns);
        // echo '<pre>'; print_r($bodyMeasurementArr); die;
        // GETTING ETHNICITY DATA.
        $EthnicityTable = $this->getServiceLocator()->get("EthnicityTable");
        $columns = array("ethnicity_id", "ethnicity_name");
        $whereArr = array();
        $ethnicityArr = $EthnicityTable->getEthnicity($whereArr, $columns);
        $sendArr = array(
            'ethnicity' => $ethnicityArr,
            'session' => $this->_sessionObj->admin,
            'bodyMeasurement' => $bodyMeasurementArr,
        );
        return new ViewModel($sendArr);
    }

    public function stepsAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $request = $this->getRequest();
        $sendArr = array(
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    public function phyactivityAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $request = $this->getRequest();

        $sendArr = array(
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    public function cholestrolAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $sendArr = array();
        $whereArr = array("status" => 1, "created_by" => $user_id);
        $request = $this->getRequest();

        $CholestrolTable = $this->getServiceLocator()->get("CholestrolTable");
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if (!(empty($postDataArr['min_hld']) && empty($postDataArr['max_hld']) && empty($postDataArr['min_ldl']) && empty($postDataArr['max_ldl']) && empty($postDataArr['min_triglyc']) && empty($postDataArr['max_triglyc']))) {
                if ($postDataArr['min_hld'] < $postDataArr['max_hld']) {
                    if ($postDataArr['min_ldl'] < $postDataArr['max_ldl']) {
                        if ($postDataArr['min_triglyc'] < $postDataArr['max_triglyc']) {
                            if (count($CholestrolTable->getThresholdcholestrol($whereArr, array("id")))) {
                                $data = array(
                                    'min_hld' => $postDataArr['min_hld'],
                                    'max_hld' => $postDataArr['max_hld'],
                                    'min_ldl' => $postDataArr['min_ldl'],
                                    'max_ldl' => $postDataArr['max_ldl'],
                                    'min_triglyc' => $postDataArr['min_triglyc'],
                                    'max_triglyc' => $postDataArr['max_triglyc'],
                                    'status' => 1,
                                    'updation_date' => time(),
                                    "created_by" => $user_id,
                                );
                                $insertData = $CholestrolTable->updateThresholdcholestrol(array("status" => 1, "created_by" => $user_id), $data);
                                $message = "The cholesterol successfully updated by admin.";
                                $this->flashMessenger()->addMessage(array('success' => $message));
                            } else {
                                $data = array(
                                    'min_hld' => $postDataArr['min_hld'],
                                    'max_hld' => $postDataArr['max_hld'],
                                    'min_ldl' => $postDataArr['min_ldl'],
                                    'max_ldl' => $postDataArr['max_ldl'],
                                    'min_triglyc' => $postDataArr['min_triglyc'],
                                    'max_triglyc' => $postDataArr['max_triglyc'],
                                    'status' => 1,
                                    'creation_date' => time(),
                                    "created_by" => $user_id,
                                );
                                $insertData = $CholestrolTable->insertThresholdcholestrol($data);
                                $message = "The cholesterol successfully added by admin.";
                                $this->flashMessenger()->addMessage(array('success' => $message));
                            }
                        } else {
                            $sendArr = array(
                                'viewData' => $postDataArr,
                                'session' => $this->_sessionObj->admin,
                            );
                            $message = "Maximum triglyc must be greater then Minimum triglyc.";
                            $this->flashMessenger()->addMessage(array('error' => $message));
                        }
                    } else {
                        $sendArr = array(
                            'viewData' => $postDataArr,
                            'session' => $this->_sessionObj->admin,
                        );
                        $message = "Maximum ldl must be greater then Minimum ldl.";
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                } else {
                    $sendArr = array(
                        'viewData' => $postDataArr,
                        'session' => $this->_sessionObj->admin,
                    );
                    $message = "Maximum hld must be greater then Minimum hld.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            } else {
                $sendArr = array(
                    'viewData' => $postDataArr,
                    'session' => $this->_sessionObj->admin,
                );
                $message = "All fields are mandatory! please enter the value.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
        $columns = array("id", "min_hld", "max_hld", "min_ldl", "max_ldl", "min_triglyc", "max_triglyc");
        $cholestrolArr = $CholestrolTable->getThresholdcholestrol($whereArr, $columns);
        if (empty($sendArr)) {
            $sendArr = array(
                'viewData' => isset($cholestrolArr[0]) ? $cholestrolArr[0] : array(),
                'session' => $this->_sessionObj->admin,
            );
        }
        return new ViewModel($sendArr);
    }

    public function bldpressureAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $request = $this->getRequest();

        $sendArr = array(
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    public function fastglucoseAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $sendArr = array();
        $whereArr = array("status" => 1, "created_by" => $user_id);
        $request = $this->getRequest();
        $GlucoseTable = $this->getServiceLocator()->get("GlucoseTable");
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if ($postDataArr['min_glucose'] != "" && $postDataArr['max_glucose'] != "") {
                if ($postDataArr['min_glucose'] < $postDataArr['max_glucose']) {
                    if (count($GlucoseTable->getThresholdGlucose($whereArr, array("id")))) {
                        $data = array(
                            'min_glucose' => $postDataArr['min_glucose'],
                            'max_glucose' => $postDataArr['max_glucose'],
                            'status' => 1,
                            'updation_date' => time(),
                            "created_by" => $user_id,
                        );
                        // echo '<pre>'; print_r($data); die;
                        $insertData = $GlucoseTable->updateThresholdGlucose(array("created_by" => $user_id, "status" => 1), $data);
                        $message = "The glucose successfully updated by admin.";
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    } else {
                        $data = array(
                            'min_glucose' => $postDataArr['min_glucose'],
                            'max_glucose' => $postDataArr['max_glucose'],
                            'status' => 1,
                            'creation_date' => time(),
                            "created_by" => $user_id,
                        );
                        // echo '<pre>'; print_r($data); die;
                        $insertData = $GlucoseTable->insertThresholdGlucose($data);
                        $message = "The glucose successfully added by admin.";
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    }
                } else {
                    $sendArr = array(
                        'viewData' => $postDataArr,
                        'session' => $this->_sessionObj->admin,
                    );
                    $message = "Maximum glucose must be greater then Minimum glucose.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            } else {
                $sendArr = array(
                    'viewData' => $postDataArr,
                    'session' => $this->_sessionObj->admin,
                );
                $message = "All fields are mandatory! please enter the value.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
        $columns = array("id", "min_glucose", "max_glucose");
        $glucoseArr = $GlucoseTable->getThresholdGlucose($whereArr, $columns);
        if(!$glucoseArr){
            $glucoseArr=0;
        }else{
            $glucoseArr=$glucoseArr[0];
        }
        
        if (empty($sendArr)) {
            $sendArr = array(
                'viewData' => isset($glucoseArr) ? $glucoseArr : array(),
                'session' => $this->_sessionObj->admin,
            );
        }
        return new ViewModel($sendArr);
    }

    public function tempAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $sendArr = array();
        $whereArr = array("status" => 1, "created_by" => $user_id);
        $request = $this->getRequest();
        $TemperatureTable = $this->getServiceLocator()->get("TemperatureTable");
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if ($postDataArr['min_temp'] != "" && $postDataArr['max_temp'] != "") {
                if ($postDataArr['min_temp'] < $postDataArr['max_temp']) {
                    if (count($TemperatureTable->getThresholdTemperature($whereArr, array("id")))) {
                        $data = array(
                            'min_temp' => $postDataArr['min_temp'],
                            'max_temp' => $postDataArr['max_temp'],
                            'status' => 1,
                            'updation_date' => time(),
                            "created_by" => $user_id,
                        );
                        // echo '<pre>'; print_r($data); die;
                        $insertData = $TemperatureTable->updateThresholdTemperature(array("created_by" => $user_id, "status" => 1), $data);
                        $message = "The Temperature successfully updated by admin.";
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    } else {
                        $data = array(
                            'min_temp' => $postDataArr['min_temp'],
                            'max_temp' => $postDataArr['max_temp'],
                            'status' => 1,
                            'creation_date' => time(),
                            "created_by" => $user_id,
                        );
                        // echo '<pre>'; print_r($data); die;
                        $insertData = $TemperatureTable->insertThresholdTemperature($data);
                        $message = "The Temperature successfully added by admin.";
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    }
                } else {
                    $sendArr = array(
                        'viewData' => $postDataArr,
                        'session' => $this->_sessionObj->admin,
                    );
                    $message = "Maximum Temperature must be greater then Minimum Temperature.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            } else {
                $sendArr = array(
                    'viewData' => $postDataArr,
                    'session' => $this->_sessionObj->admin,
                );
                $message = "All fields are mandatory! please enter the value.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
        $columns = array("id", "min_temp", "max_temp");
        $temperatureArr = $TemperatureTable->getThresholdTemperature($whereArr, $columns);
        if (empty($sendArr)) {
            $sendArr = array(
                'viewData' => isset($temperatureArr[0]) ? $temperatureArr[0] : array(),
                'session' => $this->_sessionObj->admin,
            );
        }
        return new ViewModel($sendArr);
    }

    public function sleepAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $sendArr = array();
        $whereArr = array("status" => 1, "created_by" => $user_id);
        $request = $this->getRequest();
        $SleepTable = $this->getServiceLocator()->get("SleepTable");
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if ($postDataArr['min_sleep'] != "" && $postDataArr['max_sleep'] != "") {
                if ($postDataArr['min_sleep'] < $postDataArr['max_sleep']) {
                    if (count($SleepTable->getThresholdSleep($whereArr, array("id")))) {
                        $data = array(
                            'min_sleep' => $postDataArr['min_sleep'],
                            'max_sleep' => $postDataArr['max_sleep'],
                            'status' => 1,
                            'updation_date' => time(),
                            "created_by" => $user_id,
                        );
                        // echo '<pre>'; print_r($data); die;
                        $insertData = $SleepTable->updateThresholdSleep(array("created_by" => $user_id, "status" => 1), $data);
                        $message = "The sleep successfully updated by admin.";
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    } else {
                        $data = array(
                            'min_sleep' => $postDataArr['min_sleep'],
                            'max_sleep' => $postDataArr['max_sleep'],
                            'status' => 1,
                            'creation_date' => time(),
                            "created_by" => $user_id,
                        );
                        // echo '<pre>'; print_r($data); die;
                        $insertData = $SleepTable->insertThresholdSleep($data);
                        $message = "The sleep successfully added by admin.";
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    }
                } else {
                    $sendArr = array(
                        'viewData' => $postDataArr,
                        'session' => $this->_sessionObj->admin,
                    );
                    $message = "Maximum sleep must be greater then Minimum sleep.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            } else {
                $sendArr = array(
                    'viewData' => $postDataArr,
                    'session' => $this->_sessionObj->admin,
                );
                $message = "All fields are mandatory! please enter the value.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
        $columns = array("id", "min_sleep", "max_sleep");
        $sleepArr = $SleepTable->getThresholdSleep($whereArr, $columns);
        if (empty($sendArr)) {
            $sendArr = array(
                'viewData' => isset($sleepArr[0]) ? $sleepArr[0] : array(),
                'session' => $this->_sessionObj->admin,
            );
        }
        return new ViewModel($sendArr);
    }

    protected function bmicalculationAction() {
        $this->init();
        $postData = $_POST;
        // $postData = $this->getResponse();
        $BmiTable = $this->getServiceLocator()->get("BmiTable");
        if ($postData['ethnicity'] != "" && $postData['gender'] != "") {
            // CHECKING EXISTING BMI.
            $whereArr = array(
                "ethnicity_id" => $postData['ethnicity'],
                "gender" => $postData['gender'],
                "status" => 1,
                "created_by" => $this->_sessionObj->admin['id']
            );
            if (!count($BmiTable->getThresholdBmi($whereArr, array("id")))) {
                $data = array(
                    "ethnicity_id" => $postData['ethnicity'],
                    "gender" => $postData['gender'],
                    "min_bmi" => $postData['min_bmi'],
                    "max_bmi" => $postData['max_bmi'],
                    "status" => 1,
                    "creation_date" => time(),
                    "created_by" => $this->_sessionObj->admin['id']
                );
                $insertData = $BmiTable->insertThresholdBmi($data);
            } else {
                // echo "You have already entered the bmi for this ethnicity and gender! try again.";
                die;
            }
        }
        $columns = array("id", "ethnicity_id", "gender", "min_bmi", "max_bmi");
        $whereArr = array("status" => 1, 'created_by' => $this->_sessionObj->admin['id']);
        $bmiSrr = $BmiTable->getThresholdBmi($whereArr, $columns, "true");

        $sendArr = array(
            'bmis' => $bmiSrr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function stepsmanageAction() {
        $this->init();
        $postData = $_POST;
        // $postData = $this->getResponse();
        $StepsTable = $this->getServiceLocator()->get("StepsTable");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("steps", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_steps" => $postData['min_steps'] / 1000,
                    "max_steps" => $postData['max_steps'] / 1000,
                    "status" => 1,
                    "creation_date" => time(),
                    'created_by' => $this->_sessionObj->admin['id'],
                );
                $insertData = $StepsTable->insertThresholdSteps($data);
            } else {
                // echo "Age range overlapping, please select proper agers.";
                die;
            }
        }
        $columns = array("id", "gender", "age_from", "age_to", "min_steps", "max_steps");
        $whereArr = array("status" => 1, 'created_by' => $this->_sessionObj->admin['id']);
        $stepsSrr = $StepsTable->getThresholdSteps($whereArr, $columns);
        $sendArr = array(
            'steps' => $stepsSrr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function distancemanageAction() {
        $this->init();
        $postData = $_POST;
        // $postData = $this->getResponse();
        $DistanceTable = $this->getServiceLocator()->get("DistanceTable");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("distance", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_distance" => $postData['min_distance'],
                    "max_distance" => $postData['max_distance'],
                    "status" => 1,
                    "creation_date" => time(),
                    'created_by' => $this->_sessionObj->admin['id'],
                );
                $insertData = $DistanceTable->insertThresholdDistance($data);
            } else {
                //echo "Age range overlapping, please select proper agers.";
                die;
            }
        }
        $columns = array("id", "gender", "age_from", "age_to", "min_distance", "max_distance");
        $whereArr = array("status" => 1, 'created_by' => $this->_sessionObj->admin['id']);
        $distanceArr = $DistanceTable->getThresholdDistance($whereArr, $columns);
        $sendArr = array(
            'distance' => $distanceArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function caloriemanageAction() {
        $this->init();
        $postData = $_POST;
        // $postData = $this->getResponse();
        $CalorieTable = $this->getServiceLocator()->get("CalorieTable");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("calo", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_calorie" => $postData['min_calorie'],
                    "max_calorie" => $postData['max_calorie'],
                    "status" => 1,
                    "creation_date" => time(),
                    'created_by' => $this->_sessionObj->admin['id'],
                );
                $insertData = $CalorieTable->insertThresholdCalorie($data);
            } else {
                // echo "Age range overlapping, please select proper ages.";
                die;
            }
        }
        $columns = array("id", "gender", "age_from", "age_to", "min_calorie", "max_calorie");
        $whereArr = array("status" => 1, 'created_by' => $this->_sessionObj->admin['id']);
        $calorieArr = $CalorieTable->getThresholdCalorie($whereArr, $columns);
        $sendArr = array(
            'calorie' => $calorieArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function timemanageAction() {
        $this->init();
        $postData = $_POST;
        // $postData = $this->getResponse();
        $TimeTable = $this->getServiceLocator()->get("TimeTable");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("time", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_time" => $postData['min_time'],
                    "max_time" => $postData['max_time'],
                    "status" => 1,
                    "creation_date" => time(),
                    'created_by' => $this->_sessionObj->admin['id'],
                );
                $insertData = $TimeTable->insertThresholdTime($data);
            } else {
                // echo "Age range overlapping, please select proper ages.";
                die;
            }
        }
        $columns = array("id", "gender", "age_from", "age_to", "min_time", "max_time");
        $whereArr = array("status" => 1, 'created_by' => $this->_sessionObj->admin['id']);
        $timeArr = $TimeTable->getThresholdTime($whereArr, $columns);
        $sendArr = array(
            'time' => $timeArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function systolicmanageAction() {
        $this->init();
        $postData = $_POST;
        // $postData = $this->getResponse();
        $SystolicTable = $this->getServiceLocator()->get("SystolicTable");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("syst", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_systolic" => $postData['min_systolic'],
                    "max_systolic" => $postData['max_systolic'],
                    "status" => 1,
                    "creation_date" => time(),
                    'created_by' => $this->_sessionObj->admin['id'],
                );
                $insertData = $SystolicTable->insertThresholdSystolic($data);
            } else {
                // echo "Age range overlapping, please select proper ages.";
                die;
            }
        }
        $columns = array("id", "gender", "age_from", "age_to", "min_systolic", "max_systolic");
        $whereArr = array("status" => 1, 'created_by' => $this->_sessionObj->admin['id']);
        $systolicArr = $SystolicTable->getThresholdSystolic($whereArr, $columns);
        $sendArr = array(
            'systolic' => $systolicArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function diastolicmanageAction() {
        $this->init();
        $postData = $_POST;
        // $postData = $this->getResponse();
        $DiastolicTable = $this->getServiceLocator()->get("DiastolicTable");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("dias", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_diastolic" => $postData['min_diastolic'],
                    "max_diastolic" => $postData['max_diastolic'],
                    "status" => 1,
                    "creation_date" => time(),
                    'created_by' => $this->_sessionObj->admin['id'],
                );
                $insertData = $DiastolicTable->insertThresholdDiastolic($data);
            } else {
                // echo "Age range overlapping, please select proper ages.";
                die;
            }
        }
        $columns = array("id", "gender", "age_from", "age_to", "min_diastolic", "max_diastolic");
        $whereArr = array("status" => 1, 'created_by' => $this->_sessionObj->admin['id']);
        $diastolicArr = $DiastolicTable->getThresholdDiastolic($whereArr, $columns);
        $sendArr = array(
            'diastolic' => $diastolicArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function pulsemanageAction() {
        $this->init();
        $postData = $_POST;
        // $postData = $this->getResponse();
        $PulseTable = $this->getServiceLocator()->get("PulseTable");
        if ($postData['gender'] != "" && $postData['age_from'] != "" && $postData['age_to'] != "") {
            // CHECKING EXISTING BMI.
            if ($this->checkAgeValidation("pulse", $postData['age_from'], $postData['age_to'], $postData['gender'])) {
                $data = array(
                    "gender" => $postData['gender'],
                    "age_from" => $postData['age_from'],
                    "age_to" => $postData['age_to'],
                    "min_pulse" => $postData['min_pulse'],
                    "max_pulse" => $postData['max_pulse'],
                    "status" => 1,
                    "creation_date" => time(),
                    'created_by' => $this->_sessionObj->admin['id'],
                );
                $insertData = $PulseTable->insertThresholdPulse($data);
            } else {
                // echo "Age range overlapping, please select proper ages.";
                die;
            }
        }
        $columns = array("id", "gender", "age_from", "age_to", "min_pulse", "max_pulse");
        $whereArr = array("status" => 1, 'created_by' => $this->_sessionObj->admin['id']);
        $pulseArr = $PulseTable->getThresholdPulse($whereArr, $columns);
        $sendArr = array(
            'pulse' => $pulseArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function managerecordsAction() {
        $postData = $_POST;
        if ($postData['id'] != "") {
            $whereArr = array("id" => $postData['id']);
            if ($postData['type'] == "bmi") {
                $BmiTable = $this->getServiceLocator()->get("BmiTable");
                $BmiTable->deleteThresholdBmi($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "step") {
                $StepsTable = $this->getServiceLocator()->get("StepsTable");
                $StepsTable->deleteThresholdSteps($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "time") {
                $TimeTable = $this->getServiceLocator()->get("TimeTable");
                $TimeTable->deleteThresholdTime($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "calorie") {
                $CalorieTable = $this->getServiceLocator()->get("CalorieTable");
                $CalorieTable->deleteThresholdCalorie($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "distance") {
                $DistanceTable = $this->getServiceLocator()->get("DistanceTable");
                $DistanceTable->deleteThresholdDistance($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "pulse") {
                $PulseTable = $this->getServiceLocator()->get("PulseTable");
                $PulseTable->deleteThresholdPulse($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "systo") {
                $SystolicTable = $this->getServiceLocator()->get("SystolicTable");
                $SystolicTable->deleteThresholdSystolic($whereArr);
                echo 1;
                die;
            }
            if ($postData['type'] == "diasto") {
                $DiastolicTable = $this->getServiceLocator()->get("DiastolicTable");
                $DiastolicTable->deleteThresholdDiastolic($whereArr);
                echo 1;
                die;
            }
        }
    }

    private function checkAgeValidation($mode, $age_from, $age_to, $gender) {
        $result = 1;
        $whereArr = array("status" => 1, 'created_by' => $this->_sessionObj->admin['id']);
        if ($mode == "steps") {
            $StepsTable = $this->getServiceLocator()->get("StepsTable");
            $existArr = $StepsTable->getThresholdSteps($whereArr, array("age_from", "age_to", "gender"));
        }
        if ($mode == "distance") {
            $DistanceTable = $this->getServiceLocator()->get("DistanceTable");
            $existArr = $DistanceTable->getThresholdDistance($whereArr, array("age_from", "age_to", "gender"));
        }
        if ($mode == "calo") {
            $CalorieTable = $this->getServiceLocator()->get("CalorieTable");
            $existArr = $CalorieTable->getThresholdCalorie($whereArr, array("age_from", "age_to", "gender"));
        }
        if ($mode == "time") {
            $TimeTable = $this->getServiceLocator()->get("TimeTable");
            $existArr = $TimeTable->getThresholdTime($whereArr, array("age_from", "age_to", "gender"));
        }
        if ($mode == "syst") {
            $SystolicTable = $this->getServiceLocator()->get("SystolicTable");
            $existArr = $SystolicTable->getThresholdSystolic($whereArr, array("age_from", "age_to", "gender"));
        }
        if ($mode == "dias") {
            $DiastolicTable = $this->getServiceLocator()->get("DiastolicTable");
            $existArr = $DiastolicTable->getThresholdDiastolic($whereArr, array("age_from", "age_to", "gender"));
        }
        if ($mode == "pulse") {
            $PulseTable = $this->getServiceLocator()->get("PulseTable");
            $existArr = $PulseTable->getThresholdPulse($whereArr, array("age_from", "age_to", "gender"));
        }
        foreach ($existArr AS $valueArr) {
            $start = $valueArr['age_from'];
            $end = $valueArr['age_to'];
            $genderExist = $valueArr['gender'];
            for ($start; $end >= $start; $start++) {
                if ($start == $age_from && $gender == $genderExist) {
                    $result = 0;
                    break;
                }
                if ($start == $age_to && $gender == $genderExist) {
                    $result = 0;
                    break;
                }
            }
            if (!$result)
                break;
        }
        return $result;
    }

    public function activitystageAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $IndActStgTableObj = $this->getServiceLocator()->get("IndividualActivityStageTable");
        $where = $columns = array();
        $lookingfor = "creation_date DESC";
        $activitystagedata = $IndActStgTableObj->getActivityStage($where, $columns, $lookingfor);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $activitySavArr['activity_name'] = $postDataArr['activity_name'];
            $activitySavArr['stage_name'] = $postDataArr['stage_name'];
            $activitySavArr['stage_min_val'] = $postDataArr['stage_min_val'];
            $activitySavArr['stage_max_val'] = $postDataArr['stage_max_val'];
            $activitySavArr['stage_color'] = "#".$postDataArr['stage_color'];
            $activitySavArr['creation_date'] = round(microtime(true) * 1000);
            $insdata = $IndActStgTableObj->insertActivityStage($activitySavArr);
            if ($insdata) {
                $activitystagedata = $IndActStgTableObj->getActivityStage($where, $columns, $lookingfor);
                $message = " successfully added.";
                $this->flashMessenger()->addMessage(array('success' => $message));
            } else {
                $message = "some error occured, please try again later.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
        
        $sendArr = array(
            'session' => $this->_sessionObj->admin,'activitystagedata' => $activitystagedata
        );
        return new ViewModel($sendArr);
    }

}
