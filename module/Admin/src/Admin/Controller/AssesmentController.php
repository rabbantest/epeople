<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE CONDITION PART OF ADMIN.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: ANKIT SHUKLA(fb/gshukla67).
 * CREATOR: 31/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use Admin\Form\CreateSelf;
use Admin\Form\CreateSelfValidator;

class AssesmentController extends AbstractActionController {

    protected $_selfObj;
    protected $_varObj;
    protected $_quesObj;
    protected $_layoutObj;
    protected $_analyzeObj;
    protected $_sessionObj;
    protected $_healthMatxTbl;
    protected $_selfFormulaObj;
    protected $_anlObj;

    public function init() {
        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');
        $this->_sessionObj = new Container('super_admin');
        if ($this->_sessionObj->admin['id'] != 1) {
            return $this->redirect()->toRoute('admin');
        }
        //to check assisment publish or not
        $UnPublish = array('question', 'questionnaire', 'quesorder', 'assesmentformulas', 'complexformulasetting', 'questionresponcesetting');
        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        $sid = $this->params()->fromQuery('sid');
        if ($sid) {
            $columnsCheckAss = array("is_publish");
            $whereCheckAssArr = array("ass_id" => base64_decode($this->params()->fromQuery('sid')));
            $assessmentCheck = $this->_selfObj->getAdminAssessment($whereCheckAssArr, $columnsCheckAss, "0");
            if (in_array($this->params('action'), $UnPublish)) {
                if ($assessmentCheck[0]['is_publish'] == 1) {
                    return $this->redirect()->toRoute('Defalt Assesments');
                }
            } else {
                if ($assessmentCheck[0]['is_publish'] == 0) {
                    return $this->redirect()->toRoute('Defalt Assesments');
                }
            }
        }
    }
    
    function deleteAssesmentAction() {
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $this->_quesObj = $this->getServiceLocator()->get("QuesTable");
        $this->_varObj = $this->getServiceLocator()->get("VarTable");
        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        $Assessment = $this->_selfObj->getAdminAssessment(array('ass_id' => $sid));
        if (isset($Assessment[0]['assessment_image'])) {
            if (!empty($Assessment[0]['assessment_image'])) {
                unlink(BASE_PATH . "/Assessment/" . $Assessment[0]['assessment_image']);
            }
        }
        
        
        $this->_selfObj->deleteAssessment(array('ass_id' => $sid));
        $Question = $this->_quesObj->getAdminQuestions(array('assessment_id' => $sid), array('ques_variable'));
        $this->_quesObj->deleteQuestion(array('assessment_id' => $sid));
        foreach ($Question as $Questions) {
            $Quevar = $this->_quesObj->getAdminQuestions(array('ques_variable' => $Questions['ques_variable']), array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));
            if ($Quevar[0]['num'] == 0) {
               $this->_quesObj->deleteVariable(array('var_id' => $Questions['ques_variable']));
            }
        }
        
        $this->_healthMatxTbl = $this->getServiceLocator()->get("HealthTable");
        $this->_healthMatxTbl->deleteMetrixData(array('assessment_id' => $sid));
       
        return $this->redirect()->toUrl('admin-self');
    }

    function questionnaireAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $error = 0;
        $postDataArr = array();
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $request = $this->getRequest();

        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $questionnaire_id = $postDataArr['questionnaire_id'];
            $QuestionnaireObj = $this->getServiceLocator()->get("QuestionnaireTable");

            $QuestionnaireQuestion = $QuestionnaireObj->getAdminQuestions(array('questionnaire_id' => $questionnaire_id), array('*'), 1);
            $this->_varObj = $this->getServiceLocator()->get("VarTable");
            if (!empty($QuestionnaireQuestion)) {
                foreach ($QuestionnaireQuestion as $Question) {
                    $variableArr = $this->_varObj->getAdminVariables(array("var_status" => 1, "var_name" => strtolower(trim($Question['var_name']))), array("var_id", "var_name"), "0");
                    if (count($variableArr) > 0) {
                        $Question['ques_variable'] = $variableArr[0]['var_id'];
                    } else {
                        $data = array('var_name' => trim($Question['var_name']), 'var_status' => 1);
                        $data['creation_date'] = time();
                        $Question['ques_variable'] = $this->_varObj->insertAdminVariables($data);
                    }
                    $this->_quesObj = $this->getServiceLocator()->get("QuesTable");

                    $status = $this->_quesObj->checkAvilable(array('assessment_id' => $sid, 'ques_variable' => isset($variableArr[0]['var_id']) ? $variableArr[0]['var_id'] : NULL));

                    if ($status) {

                        $Question['assessment_id'] = $sid;
                        unset($Question['questionnaire_id']);
                        $ques_id = $Question['ques_id'];
                        unset($Question['ques_id']);
                        unset($Question['var_name']);


                        $question_id = $this->_quesObj->insertAdminQuestions($Question);

                        if (!empty($question_id)) {
                            $Option = $QuestionnaireObj->getAdminQuestionsAndOptions(array('ques_id' => $ques_id), array('ques_id'), 1);

                            foreach ($Option as $Opt) {
                                unset($Opt['ques_id']);
                                $Opt['question_id'] = $question_id;
                                unset($Opt['opt_id']);
                                $this->_quesObj->insertAdminQuestionsOption($Opt);
                            }
                        }
                    }
                }
            }
        }

        return $this->redirect()->toUrl("/admin-self-question?sid=" . base64_encode($sid));
    }

    /*     * *****
     * Action:- Set question status mendetory.
     * @author: Hemant.
     * Created Date: 17-03-2015.
     * *** */

    function assessmentDataAction() {
        $postData = $_GET;

        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        $Data = $this->_selfObj->getAdminAssessment(array('ass_id' => base64_decode($_GET['sid'])), array('*'), '1');
        echo json_encode($Data[0]);
        die;
    }

    /*     * *****
     * Action:- Use to manage Assesments section here.
     * @author: HEMANT CHUPHAL.
     * Created Date: 18-2-2014.
     * *** */

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $error = 0;
        $postDataArr = array();
        $user_id = $this->_sessionObj->admin['id'];
        $request = $this->getRequest();
        $form = new CreateSelf('self');
        $request = $this->getRequest();
        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        if ($request->isPost()) {
            // $user = new ();
            $formValidator = new CreateSelfValidator(); {
                $form->setInputFilter($formValidator->getInputFilter());
                $form->setData($request->getPost());
            }
            if ($form->isValid()) {
                // $user->exchangeArray($form->getData()); 
                $postDataArr = $request->getPost()->toArray();

                $data = array(
                    "ass_name" => $postDataArr['ass_name'],
                    "ass_recurring" => ($postDataArr['ass_recurring'] == "on" ? 1 : 0),
                    "ass_update" => (isset($postDataArr['ass_update']) && $postDataArr['ass_update'] == "on" ? 1 : 0),
                    "ass_targets" => $postDataArr['ass_target'],
                    "assessment_desc" => $postDataArr['assessment_desc'],
                    "assessment_que_desc" => $postDataArr['assessment_que_desc'],
                    'copyright_text' => $postDataArr['copyright_text'],
                    "ass_status" => 1,
                );
                $error = 0;
                if (!empty($postDataArr['ass_id'])) {
                    $data['ass_id'] = $postDataArr['ass_id'];
                    $data['updation_date'] = time();
                } else {
                    $data['creation_date'] = time();
                }
                $AsessmenDatat = $this->_selfObj->getAdminAssessment(array('ass_id' => $postDataArr['ass_id']), array('assessment_image'));
                if (!empty($_FILES['assessment_image']["name"])) {
                    $allowed = array('jpg', 'jpeg', 'png', 'gif');
                    $path = BASE_PATH . "/Assessment/";
                    $ext = pathinfo($_FILES['assessment_image']['name'], PATHINFO_EXTENSION);
                    $file_name = md5(microtime());
                    $file_path = $path . $file_name . '.' . $ext;
                    if (in_array($ext, $allowed)) {
                        if (move_uploaded_file($_FILES['assessment_image']["tmp_name"], $file_path)) {
                            $data['assessment_image'] = $file_name . '.' . $ext;
                            if (!empty($AsessmenDatat[0]['assessment_image'])) {
                                @unlink($path . $AsessmenDatat[0]['assessment_image']);
                            }
                        } else {
                            $error = 1;
                        }
                    } else {
                        $error = 1;
                    }
                }
                if (!$error) {
                    if ($postDataArr['ass_cat'] != "") {
                        $whereArr = array("cat_status" => 1);
                        $columns = array("cat_id");
                        $searchArr = array("cat_name" => $postDataArr['ass_cat']);
                        $categoryArr = $this->_selfObj->getAdminCategory($whereArr, $columns, $searchArr);
                        if (count($categoryArr) > 0) {
                            $data['ass_category'] = $categoryArr[0]['cat_id'];
                        } else {
                            $catData = array(
                                "cat_name" => $postDataArr['ass_cat'],
                                "cat_status" => 1,
                                "creation_date" => time(),
                            );
                            $data['ass_category'] = $this->_selfObj->insertAdminCategory($catData);
                        }
                    } else {
                        $error = 1;
                        $message = "On Both and Non-Members audiences, category must be required.";
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                }
                if (!$error) {
                    if (!empty($data['ass_id'])) {
                        if ($this->_selfObj->updateAdminAssessment(array('ass_id' => $data['ass_id']), $data)) {
                            $insert_id = $data['ass_id'];
                        }
                    } else {
                        $insert_id = $this->_selfObj->insertAdminAssessment($data);
                    }
                    if ($insert_id) {
                        return $this->redirect()->toUrl("/admin-self-question?sid=" . base64_encode($insert_id));
                    } else {
                        $error = 1;
                        $message = "Due to Some technical issue! Assessment not created.";
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                }
            } else {
                $error = 1;
            }
        }
        $columns = array("ass_id", "ass_name", "is_publish", "ass_status");
        $whereArr = array("ass_status" => 1);
        $limit = array(0, 12);
        $page = 1;
        if (base64_decode($this->params()->fromQuery('page')) > 0) {
            $page = base64_decode($this->params()->fromQuery('page'));
            $limit = array($limit[1] * $page, $limit[1]);
            $page++;
        }
        $assessmentArr = $this->_selfObj->getAdminAssessment($whereArr, $columns, "0", array(), $limit);
        $sendArr = array(
            'form' => $form,
            'error' => $error,
            'postData' => $postDataArr,
            'assessment' => $assessmentArr,
            'page' => $page,
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    public function checkTitleAction() {
        $SelfAssTable = $this->getServiceLocator()->get("SelfAssTable");
        $postData = $_POST;
        $whereCountArr = array('ass_name' => $postData['ass_name'], 'ass_status' => 1);
        if (!empty($postData['ass_id'])) {
            $assCountArr = $SelfAssTable->checkAvilable($whereCountArr, $postData['ass_id']);
        } else {
            $assCountArr = $SelfAssTable->checkAvilable($whereCountArr);
        }
        echo ($assCountArr) ? 'true' : 'false';
        die;
    }

    /*     * *****
     * Action:- Use to manage Assesments section here.
     * @author: Ankit Shukla(fb/gshukla67).
     * Created Date: 12-01-2014.
     * *** */

    public function questionAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $postDataArr = array();
        $request = $this->getRequest();
        $user_id = $this->_sessionObj->admin['id'];
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $this->_varObj = $this->getServiceLocator()->get("VarTable");
        $this->_quesObj = $this->getServiceLocator()->get("QuesTable");

        $QuestionnaireObj = $this->getServiceLocator()->get("QuestionnaireTable");

        $Questionnaire = $QuestionnaireObj->getAdminQuestionnaire(array('status' => 1, 'created_by' => $user_id), array('id', 'name'));
        
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
  
            if (empty($postDataArr['ques_var_value']) || !empty($postDataArr['ques_var_id'])) {
                if (trim($postDataArr['ques_variable']) != "") {
                    $variableArr = $this->_varObj->getAdminVariables(array("var_status" => 1), array("var_id", "var_name"), "0", array("var_name" => strtolower(trim($postDataArr['ques_variable']))));
                    $variableMainArr = $QuestionnaireObj->getAdminVariables(array("var_status" => 1, "var_name" => $postDataArr['ques_variable']), array("var_id", "var_name"), "0");

//$check$this->_varObj->checkAvilable(array('assessment_id' => $sid, 'ques_variable' => $variableArr[0]['var_id']));
                    if (!empty($postDataArr['ques_var_id'])) {
                        $variableArr = array();
                    }
                    $variable_id = NULL;
                    if (count($postDataArr['ques_option']) >= 2) {
                        if (count($variableArr) > 0) {
                            $variable_id = $variableArr[0]['var_id'];
                        } else if (count($variableMainArr) > 0) {
                            $data = array(
                                'var_name' => trim($variableMainArr[0]['var_name']),
                                'var_status' => 1
                            );
                            if (isset($postDataArr['ques_var_id'])) {
                                if (!empty($postDataArr['ques_var_id'])) {
                                    $data['updation_date'] = time();
                                    if ($this->_varObj->updateAdminVariables(array('var_id' => $postDataArr['ques_var_id']), $data)) {
                                        $variable_id = $postDataArr['ques_var_id'];
                                    }
                                }
                            } else {
                                $data['creation_date'] = time();
                                $variable_id = $this->_varObj->insertAdminVariables($data);
                            }
                        } else {
                            $data = array(
                                'var_name' => trim($postDataArr['ques_variable']),
                                'var_status' => 1
                            );
                            if (isset($postDataArr['ques_var_id'])) {
                                if (!empty($postDataArr['ques_var_id'])) {
                                    $data['updation_date'] = time();
                                    if ($this->_varObj->updateAdminVariables(array('var_id' => $postDataArr['ques_var_id']), $data)) {
                                        $variable_id = $postDataArr['ques_var_id'];
                                    }
                                }
                            } else {
                                $data['creation_date'] = time();
                                $variable_id = $this->_varObj->insertAdminVariables($data);
                            }
                        }

                        $status = $this->_quesObj->checkAvilable(array('assessment_id' => $sid, 'ques_variable' => $variable_id));
                        if ($variable_id && $status || !empty($postDataArr['ques_var_id'])) {
                            if (trim($postDataArr['ques_question']) != "") {
                                if (count($postDataArr['ques_option']) >= 2) {
                                    if (
                                            trim($postDataArr['ques_option'][0]) != "" &&
                                            trim($postDataArr['ques_value'][0]) != "" &&
                                            trim($postDataArr['ques_option'][1]) != "" &&
                                            trim($postDataArr['ques_value'][1]) != ""
                                    ) {
                                        $data = array(
                                            'assessment_id' => $sid,
                                            'ques_variable' => $variable_id,
                                            'ques_var_type' => $postDataArr['ques_var_type'],
                                            'ques_question' => trim($postDataArr['ques_question']),
                                            'ques_mendatory' => ($postDataArr['ques_mendatory'] == "on" ? 1 : 0),
                                            'ques_order' => 0,
                                            'ques_status' => 1
                                        );
                                        if (!empty($postDataArr['ques_var_value'])) {
                                            $data['updation_date'] = time();
                                            if ($this->_quesObj->updateAdminQuestions(array('ques_id' => $postDataArr['ques_var_value']), $data)) {
                                                $question_id = $postDataArr['ques_var_value'];
                                            }
                                        } else {
                                            $data['creation_date'] = time();
                                            $question_id = $this->_quesObj->insertAdminQuestions($data);
                                        }
                                        $ques_option_id = array();
                                        if ($question_id) {
                                            $data = array(
                                                'question_id' => $question_id,
                                                'opt_status' => 1
                                            );
                                            foreach ($postDataArr['ques_option'] AS $key => $value) {
                                                $data['opt_option'] = trim($value);
                                                $data['opt_value'] = trim($postDataArr['ques_value'][$key]);
                                                if ($data['opt_option'] != "" && $data['opt_value'] != "") {
                                                    // updateAdminQuestionsOption
                                                    if (!empty($postDataArr['ques_option_id'][$key])) {
                                                        $data['updation_date'] = time();
                                                        $option_id = $this->_quesObj->updateAdminQuestionsOption(array('opt_id' => $postDataArr['ques_option_id'][$key]), $data);
                                                        $ques_option_id[] = $postDataArr['ques_option_id'][$key];
                                                    } else {
                                                        $data['creation_date'] = time();
                                                        $option_id = $this->_quesObj->insertAdminQuestionsOption($data);
                                                        $ques_option_id[] = $option_id;
                                                    }
                                                }
                                            }
                                            if (!empty($ques_option_id)) {
                                                $option_id = $this->_quesObj->deleteAdminQuestionsOption(array('question_id' => $question_id), $ques_option_id);
                                            }
                                        }
                                        $message = 'Question has been successfully created with *' . $postDataArr['ques_variable'] . '* variable.';
                                        $postDataArr = array();
                                        $this->flashMessenger()->addMessage(array('success' => $message));
                                    } else {
                                        $message = 'Please enter the options of question in proper format.';
                                        $this->flashMessenger()->addMessage(array('error' => $message));
                                    }
                                } else {
                                    $message = 'Options of the question must be greater than or equal to two(2).';
                                    $this->flashMessenger()->addMessage(array('error' => $message));
                                }
                            } else {
                                $message = 'Please enter your question! its must be maintained.';
                                $this->flashMessenger()->addMessage(array('error' => $message));
                            }
                        } else {
                            $message = 'Please enter the valid question variable! its must be maintained.';
                            $this->flashMessenger()->addMessage(array('error' => $message));
                        }
                    } else {
                        $message = 'Please provide at least two options.';
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                } else {
                    $message = 'Please enter question variable! its must be maintained.';
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            } else {
                $reinsert = 1;
                $columns = array("ques_id", "assessment_id", "ques_variable", "ques_question", "ques_mendatory");
                $whereArr = array(
                    "ques_id" => $postDataArr['ques_var_value'],
                    "ques_status" => 1,
                );
                $resultArr = $this->_quesObj->getAdminQuestions($whereArr, $columns, "0");

                $variableArr = $this->_varObj->getAdminVariables(array("var_status" => 1, "var_name" => $postDataArr['ques_variable']), array("var_id", "var_name"), "0");

                if (empty($variableArr)) {
                    $data['var_name'] = $postDataArr['ques_variable'];
                    $data['creation_date'] = time();
                    $variableArr[0]['ques_variable'] = $this->_varObj->insertAdminVariables($data);
                }

                $data = array(
                    'assessment_id' => $sid,
                    'ques_variable' => $resultArr[0]['ques_variable'],
                    'ques_var_type' => 1,
                    'ques_question' => $resultArr[0]['ques_question'],
                    'ques_mendatory' => $resultArr[0]['ques_mendatory'],
                    'ques_order' => 0,
                    'ques_status' => 1,
                    'creation_date' => time(),
                );
                $status = $this->_quesObj->checkAvilable(array('assessment_id' => $sid, 'ques_variable' => $resultArr[0]['ques_variable']));
                if (!$status) {
                    $message = '*' . $postDataArr['ques_variable'] . '* already exist in this assessment! Please try other variable.';
                    $this->flashMessenger()->addMessage(array('error' => $message));
                    return $this->redirect()->toUrl("/admin-self-question?sid=" . base64_encode($sid));
                } else {
                    $question_id = $this->_quesObj->insertAdminQuestions($data);
                    if ($question_id) {
                        $columns = array("opt_id", "opt_option", "opt_value");
                        $whereArr = array(
                            "question_id" => $resultArr[0]['ques_id'],
                            "opt_status" => 1,
                        );
                        $optionsArr = $this->_quesObj->getAdminQuestionsOption($whereArr, $columns);
                        $data = array(
                            'question_id' => $question_id,
                            'opt_status' => 1,
                            'creation_date' => time(),
                        );
                        foreach ($optionsArr AS $key => $val) {
                            $data['opt_option'] = $val['opt_option'];
                            $data['opt_value'] = $val['opt_value'];
                            $option_id = $this->_quesObj->insertAdminQuestionsOption($data);
                        }
                    }
                    $message = 'Question has been successfully created with *' . $postDataArr['ques_variable'] . '* variable.';
                    $postDataArr = array();
                    $this->flashMessenger()->addMessage(array('success' => $message));
                    // return $this->redirect()->toUrl("/admin-self-question?sid=".base64_encode($sid)."&tmp=".base64_encode('exist'));
                }
            }
        }
        if ($this->params()->fromQuery('var')) {
            $variable_id = base64_decode($this->params()->fromQuery('var'));
            $varName = base64_decode($this->params()->fromQuery('varName'));
            $columns = array("ques_id", "assessment_id", "ques_variable", "ques_question", "ques_mendatory");
            $whereArr = array(
                "ques_variable" => $variable_id,
                "ques_status" => 1,
                'vr.var_name'=>$varName
            );
            $resultArr = $this->_quesObj->getAdminQuestions($whereArr, $columns, "1");
            
            if (empty($resultArr)) {
                $columns = array("ques_id", "ques_variable", "ques_question", "ques_mendatory");
                $resultArr = $QuestionnaireObj->getAdminQuestions($whereArr, $columns, "1");
                if (!empty($resultArr)) {
                    $resultArr[0]['assessment_id'] = NULL;
                }
            }

            if (count($resultArr) > 0) {
                $columns = array("opt_id", "opt_option", "opt_value");
                $whereArr = array(
                    "question_id" => $resultArr[0]['ques_id'],
                    "opt_status" => 1,
                );
                $optionsArr = $this->_quesObj->getAdminQuestionsOption($whereArr, $columns);
                if (empty($optionsArr)) {
                    $optionsArr = $QuestionnaireObj->getAdminQuestionsOption($whereArr, $columns);
                }
                foreach ($optionsArr AS $key => $val) {
                    $option[$key] = $val['opt_option'];
                    $optValue[$key] = $val['opt_value'];
                }
                $resultArr[0]['ques_option'] = $option;
                $resultArr[0]['ques_value'] = $optValue;
                $postDataArr = $resultArr[0];
                $status = $this->_quesObj->checkAvilable(array('assessment_id' => $sid, 'ques_variable' => $postDataArr['ques_variable']));
                // echo $sid.'<pre>'; print_r($postDataArr['ques_variable']); die;
                if ((!isset($reinsert)) && !$status) {
                    $variableMainArr = $QuestionnaireObj->getAdminVariables(array("var_status" => 1, 'var_id' => trim($postDataArr['ques_variable'])), array("var_id", "var_name"), "0", array());
                    $statusMain = $this->_quesObj->checkAvilable(array('assessment_id' => $sid, 'ques_variable' => isset($variableMainArr[0]['var_name'])?$variableMainArr[0]['var_name']:NULL));

                    if (!$statusMain) {
                        $message = '*' . $postDataArr['var_name'] . '* already exist in this questionnaire! Please try other variable.';
                        $this->flashMessenger()->addMessage(array('error' => $message));
                        $postDataArr = array();
                    } else {
                        $message = '*' . $postDataArr['var_name'] . '* already exist in this questionnaire! Please try other variable.';
                        $this->flashMessenger()->addMessage(array('error' => $message));
                        $postDataArr = array();
                    }
                } else {

                    $variableMainArr = $QuestionnaireObj->getAdminVariables(array("var_status" => 1, 'var_id' => trim($postDataArr['ques_variable'])), array("var_id", "var_name"), "0", array());
                    $statusMain = $this->_quesObj->checkAvilable(array('assessment_id' => $sid, 'ques_variable' => isset($variableMainArr[0]['var_name'])?$variableMainArr[0]['var_name']:NULL));

                    if ((!isset($reinsert)) && !$statusMain) {
                        $message = '*' . $postDataArr['var_name'] . '* already exist in this assessment! Please try other variable.';
                        $this->flashMessenger()->addMessage(array('error' => $message));
                        $postDataArr = array();
                        //return $this->redirect()->toUrl("/admin-self-question?sid=".base64_encode($sid));
                    }
                }

            }
        }
        // GET THIS ASSESSMENT.
        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        $columns = array("ass_id", "ass_name", "ass_status");
        $whereArr = array("ass_id" => $sid);
        $assessmentArr = $this->_selfObj->getAdminAssessment($whereArr, $columns, "0");
        $message = '';
        if ($this->params()->fromQuery('type') == 'delete') {
            $varId = base64_decode($this->params()->fromQuery('varId'));
            $questionId = base64_decode($this->params()->fromQuery('questionId'));
            if (!empty($varId) && !empty($questionId)) {
                if ($this->_quesObj->deleteAdminQuestions(array('ques_id' => $questionId))) {
                    $message .= ' Question has been successfully deleted from assessment.';
                    $this->flashMessenger()->addMessage(array('success' => $message));
                }
                if ($this->_quesObj->checkAvilable(array('ques_variable' => $varId))) {
                    if ($this->_quesObj->deleteAdminVar(array('var_id' => $varId))) {
                        $message = 'Varrable &';
                    }
                }
            }
        }
        if ($this->params()->fromQuery('type') == 'edit') {
            $varId = base64_decode($this->params()->fromQuery('varId'));
            $questionId = base64_decode($this->params()->fromQuery('questionId'));
            $columns = array("ques_id", "assessment_id", "ques_variable", "ques_question", "ques_mendatory");
            $whereArr = array(
                "ques_id" => $questionId,
                "ques_status" => 1,
            );
            $resultArr = $this->_quesObj->getAdminQuestions($whereArr, $columns, "1");
            $whereCountArr = array("ques_status" => 1, 'ques_variable' => $resultArr[0]['ques_variable']);
            $columnsCount = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'));
            $varCountCheckArr = $this->_quesObj->getAdminQuestions($whereCountArr, $columnsCount, '0', array(''), '1');
            if (count($varCountCheckArr) && $varCountCheckArr[0]['num'] <= 1) {
                if (count($resultArr) > 0) {
                    $columns = array("opt_id", "opt_option", "opt_value");
                    $whereArr = array(
                        "question_id" => $resultArr[0]['ques_id'],
                        "opt_status" => 1,
                    );
                    $optionsArr = $this->_quesObj->getAdminQuestionsOption($whereArr, $columns);
                    foreach ($optionsArr AS $key => $val) {
                        $option[$key] = $val['opt_option'];
                        $optValue[$key] = $val['opt_value'];
                        $optId[$key] = $val['opt_id'];
                    }
                    $resultArr[0]['ques_option'] = $option;
                    $resultArr[0]['ques_value'] = $optValue;
                    $resultArr[0]['ques_option_id'] = $optId;
                    $postDataArr = $resultArr[0];
                    $postDataArr['type'] = 'edit';
                }
            } else {
                $message = 'Not editable must be used in some other assessment or study.';
                $this->flashMessenger()->addMessage(array('error' => $message));
                $postDataArr['type'] = '';
            }
        } else {
            $postDataArr['type'] = '';
        }
//        echo '<pre>';
//        print_r($postDataArr);
//        die;
        $columns = array("ques_id", "assessment_id", "ques_question", "creation_date", 'ques_variable', 'ques_mendatory');
        $whereArr = array("ques_status" => 1, "assessment_id" => $sid);
        $questionArr = $this->_quesObj->getAdminQuestions($whereArr, $columns, "0");
        $queVars = array();
        if (!empty($questionArr)) {
            foreach ($questionArr as $QuestionArr) {
                $queVars[] = $QuestionArr['ques_variable'];
            }
        } else {
            $queVars = array('0');
        }
        $columnsCount = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'), 'ques_variable');
        $whereCountArr = array("ques_status" => 1, 'ques_variable' => $queVars);
        $varCountArr = $this->_quesObj->getAdminQuestions($whereCountArr, $columnsCount, '0', array(''), '1');
        $varStatus = array();
        if (!empty($varCountArr)) {
            foreach ($varCountArr as $varCount) {
                $varStatus[$varCount['ques_variable']] = $varCount['num'];
            }
        }
//        echo '<pre>';
//        print_r($varStatus);
//        die;
        // GET THIS ASSESSMENT QUESTION.
//         echo '<pre>'; print_r($postDataArr); echo '<pre>'; print_r($assessmentArr); die;
        $sendArr = array(
            'question' => $questionArr,
            'postData' => $postDataArr,
            'varStatus' => $varStatus,
            'Questionnaire' => $Questionnaire,
            'sid' => $sid,
            'assessment' => $assessmentArr[0],
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- Use to manage Assesments section here.
     * @author: Ankit Shukla(fb/gshukla67).
     * Created Date: 07-01-2014.
     * *** */

    protected function questionlistAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $postDataArr = array();
        $request = $this->getRequest();
        $user_id = $this->_sessionObj->admin['id'];
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $this->_quesObj = $this->getServiceLocator()->get("QuesTable");
        $this->_anlObj = $this->getServiceLocator()->get("AnalyzeTable");
        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        $selfPart = $this->getServiceLocator()->get("AnalyzeTable");
        // GET AND MANAGE THIS ASSESSMENT.
        if ($this->params()->fromQuery('type') == "amend") {
            if ($this->_anlObj->deleteAdminQuesAnalyze(array('assessment_id' => $sid))) {
                $selfPart->deleteParticipation(array('ass_id' => $sid));
                $data = array(
                    "is_publish" => 0,
                    "publish_date" => 0,
                    "updation_date" => time(),
                );
                $where = array(
                    "ass_id" => $sid
                );
                if ($this->_selfObj->updateAdminAssessment($where, $data)) {
                    return $this->redirect()->toUrl('admin-self-question?sid=' . base64_encode($sid));
                }
            }
        }
        if ($this->params()->fromQuery('status') != "") {
            $status = base64_decode($this->params()->fromQuery('status'));
            $data = array(
                "is_publish" => ($status ? 0 : 1),
                "publish_date" => ($status ? 0 : time()),
                "updation_date" => time(),
            );
            $where = array(
                "ass_id" => $sid
            );
            if ($this->_selfObj->updateAdminAssessment($where, $data)) {
                // echo $status; die;
                $msgStatus = ($status ? "deactivated!" : "activated!");
                $message = "The selected assessment successfully " . $msgStatus;
                $this->flashMessenger()->addMessage(array('success' => $message));
                return $this->redirect()->toRoute('Defalt Assesments');
            }
        }
        $columns = array("ass_id", "ass_name", "is_publish", "ass_status", 'ass_category', "ass_targets", "ass_recurring", "ass_update");
        $whereArr = array("ass_id" => $sid);
        $assessmentArr = $this->_selfObj->getAdminAssessment($whereArr, $columns, "0");
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $data = $assessmentArr[0];
            unset($data['ass_id']);
            $data['ass_name'] = $postDataArr['ass_name'];
            $data["is_publish"] = 0;
            $data["creation_date"] = time();
            $columnsCount = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'));
            $whereCountArr = array('ass_name' => $postDataArr['ass_name']);
            $assCountArr = $this->_selfObj->getAdminAssessment($whereCountArr, $columnsCount, '0');
            if ($assCountArr[0]['num'] == 0) {
                $lastAssId = $this->_selfObj->insertAdminAssessment($data);
                $questionDataArr = $this->_quesObj->getAdminQuestions(array('assessment_id' => $sid));
                if ($lastAssId) {
                    if (!empty($questionDataArr)) {
                        foreach ($questionDataArr as $QuestionData) {
                            $data = $QuestionData;
                            unset($data["ques_id"]);
                            $data["assessment_id"] = $lastAssId;
                            $data["creation_date"] = time();
                            $questionId = $this->_quesObj->insertAdminQuestions($data);
                            $questionOption = $this->_quesObj->getAdminQuestionsOption(array('question_id' => $QuestionData['ques_id']));
                            if (!empty($questionOption)) {
                                foreach ($questionOption as $QuestionOption) {
                                    $data = $QuestionOption;
                                    unset($data['opt_id']);
                                    $data["creation_date"] = time();
                                    $data['question_id'] = $questionId;
                                    $this->_quesObj->insertAdminQuestionsOption($data);
                                }
                            }
                        }
                    }
                }
                return $this->redirect()->toUrl('admin-self-question?sid=' . base64_encode($lastAssId));
            } else {
                $message = "Assessment name (" . $postDataArr['ass_name'] . ") not available";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
        // GET THIS ASSESSMENT QUESTION.
        $columns = array("ques_id", "assessment_id", "ques_question", "creation_date");
        $whereArr = array("ques_status" => 1, "assessment_id" => $sid);
        $questionArr = $this->_quesObj->getAdminQuestions($whereArr, $columns, "1");
        // echo $status.'<pre>'; print_r($assessmentArr); die;
        $sendArr = array(
            'question' => $questionArr,
            'quesOj' => $this->_quesObj,
            'form' => $form,
            'assessment' => $assessmentArr[0],
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- Use to manage Assesments section here.
     * @author: Ankit Shukla(fb/gshukla67).
     * Created Date: 24-01-2014.
     * *** */

    public function quesanalyzeAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $postDataArr = array();
        $request = $this->getRequest();
        $user_id = $this->_sessionObj->admin['id'];
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $this->_quesObj = $this->getServiceLocator()->get("QuesTable");
        $this->_analyzeObj = $this->getServiceLocator()->get("AnalyzeTable");
        // GET THIS ASSESSMENT.
        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        $columns = array("ass_id", "ass_name", "ass_status");
        $whereArr = array("ass_id" => $sid);
        $assessmentArr = $this->_selfObj->getAdminAssessment($whereArr, $columns, "0");
        // GET THIS ASSESSMENT QUESTION.
        $columns = array("ques_id", "assessment_id", "ques_question", "creation_date");
        $whereArr = array("ques_status" => 1, "assessment_id" => $sid);
        $questionArr = $this->_quesObj->getAdminQuestions($whereArr, $columns, "1");

        $sendArr = array(
            'question' => $questionArr,
            'quesOj' => $this->_quesObj,
            'analyze' => $this->_analyzeObj,
            'sid' => $sid,
            'assessment' => $assessmentArr[0],
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    public function quesanalyzeNonmemberAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $postDataArr = array();
        $request = $this->getRequest();
        $user_id = $this->_sessionObj->admin['id'];
        $sid = base64_decode($this->params()->fromQuery('sid'));

        $this->_quesObj = $this->getServiceLocator()->get("QuesTable");
        $this->_analyzeObj = $this->getServiceLocator()->get("AnalyzeTable");
        // GET THIS ASSESSMENT.
        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        $columns = array("ass_id", "ass_name", "ass_status");
        $whereArr = array("ass_id" => $sid);
        $assessmentArr = $this->_selfObj->getAdminAssessment($whereArr, $columns, "0");
        // GET THIS ASSESSMENT QUESTION.
        $columnsQue = array("ques_id", "assessment_id", "ques_question", "creation_date");
        $whereQueArr = array("ques_status" => 1, "assessment_id" => $sid);
        $questionArr = $this->_quesObj->getAdminQuestions($whereQueArr, $columnsQue, "1");

        $sendArr = array(
            'question' => $questionArr,
            'quesOj' => $this->_quesObj,
            'analyze' => $this->_analyzeObj,
            'sid' => $sid,
            'assessment' => $assessmentArr[0],
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- FUNCTION TO CHECK aSSESSMENT NAME EXISTENCE FOR JQUERY REMOTE VALIDATION.
     * @author: Hemant Chuphal.
     * Created Date: 22-2-2015.
     * *** */

    public function checkSelfAssAction() {
        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        $columnsCount = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'));
        $whereCountArr = array('ass_name' => $_POST['ass_name']);
        $assCountArr = $this->_selfObj->getAdminAssessment($whereCountArr, $columnsCount, '0');
        echo ($assCountArr[0]['num'] == 0) ? 'true' : 'false';
        die;
    }

    /*     * *****
     * Action:- FUNCTION TO CHECK aSSESSMENT NAME EXISTENCE FOR JQUERY REMOTE VALIDATION.
     * @author: Hemant Chuphal.
     * Created Date: 18-2-2015.
     * *** */

    protected function createselfAction() {
        $postData = $_GET;
        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        if (isset($postData['ass_name'])) {
            $type = "ass";
            $columns = array("ass_id", "ass_name", "ass_status");
            $whereArr = array("ass_name" => strtolower($postData['ass_name']));
            $searchArr = array();
            if (isset($postData['ass_id']) && !empty($postData['ass_id'])) {
                $nameArr = $this->_selfObj->getAdminAssessment($whereArr, $columns, "0", $searchArr, '', $postData['ass_id']);
            } else {
                $nameArr = $this->_selfObj->getAdminAssessment($whereArr, $columns, "0", $searchArr);
            }
        }
        if (isset($postData['ass_cat'])) {
            $type = "cat";
            $whereArr = array("cat_status" => 1);
            $columns = array("cat_id", "cat_name", "cat_status");
            $searchArr = array("cat_name" => strtolower($postData['ass_cat']));
            $nameArr = $this->_selfObj->getAdminCategory($whereArr, $columns, $searchArr);
        }
        $sendArr = array(
            'type' => $type,
            'name' => $nameArr,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    protected function checkvariablesAction() {
        $postData = $_GET;
        $this->_varObj = $this->getServiceLocator()->get("VarTable");
        $columns = array("var_id", "var_name");
        $whereArr = array("var_status" => 1);
        $searchArr = array("var_name" => strtolower($postData['q_var']));
        $nameArr = $this->_varObj->getAdminVariables($whereArr, $columns, "0", $searchArr);

        $QuestionnaireObj = $this->getServiceLocator()->get("QuestionnaireTable");
        $columnsMain = array("var_id", "var_name");
        $whereArrMain = array("var_status" => 1);
        $searchArrMain = array("var_name" => strtolower($postData['q_var']));
        $nameArrMain = $QuestionnaireObj->getAdminVariables($whereArrMain, $columnsMain, "0", $searchArrMain);

        $sendArr = array(
            'name' => !empty($nameArr) ? $nameArr : array(),
            'nameMain' => !empty($nameArrMain) ? $nameArrMain : array(),
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    /*     * *****
     * Action:- Set question status mendetory.
     * @author: Hemant.
     * Created Date: 17-03-2015.
     * *** */

    function mendatoryStatusAction() {
        $postData = $_GET;
        $QuesTable = $this->getServiceLocator()->get("QuesTable");
        $this->_selfFormulaObj = $this->getServiceLocator()->get('AssessmentFormula');
        if ($_GET['status'] == 0) {
            $this->_selfFormulaObj->deleteAdminAssessmentFormula(array('formula_assessment' => base64_decode($_GET['sid'])));
        }


        if ($QuesTable->updateAdminQuestions(array('ques_id' => base64_decode($_GET['qid'])), array('ques_mendatory' => $_GET['status']))) {
            echo 'true';
        } else {
            echo 'false';
        }
        die;
    }

    /*     * *****
     * Action:- to set order in seft-assessment questions.
     * @author: Hemant.
     * Created Date: 17-02-2015.
     * *** */

    function quesorderAction() {
        $postData = $_GET;
        $QuesTable = $this->getServiceLocator()->get("QuesTable");
        $whereOldPosArr = array("ques_order" => $postData['pos'], "assessment_id" => $postData['assessment_id']);
        $columnsPos = array("ques_id");
        $Result = $QuesTable->getAdminQuestions($whereOldPosArr, $columnsPos);
        //Get Action
        $newPosition = ($postData['type'] == 'UP') ? $newPosition = $postData['pos'] + 1 : $newPosition = $postData['pos'] - 1;
        if (!empty($Result)) {
            $columnsOldPos = array("ques_order" => $newPosition);
            $whereOldPosArr = array("ques_id" => $Result[0]['ques_id'], "assessment_id" => $postData['assessment_id']);
            $rowAffectOldPos = $QuesTable->orderAdminQuestion($whereOldPosArr, $columnsOldPos);
        }
        $columns = array("ques_order" => $postData['pos']);
        $whereArr = array("ques_id" => $postData['ques_id'], "assessment_id" => $postData['assessment_id']);
        $rowAffect = $QuesTable->orderAdminQuestion($whereArr, $columns);
        $sendArr = array(
            'status' => $rowAffect,
        );
        echo json_encode($sendArr);
        die;
    }

    /*     * *****
     * Action:- HEALTH METRIX XLS UPLOAD.
     * @author: Hemant.
     * Created Date: 17-02-2015.
     * *** */

    function _Upload($file = NULL, $sid = NULL) {
        $phpExcel = new \PHPExcel();
        $file_name = md5(microtime());
        $path = BASE_PATH . "/" . $file_name . '.xls';
        //FILE UPLOAD CODE
        if (!empty($file['metrix_file']['name']) && !($file['metrix_file']['error'])) {
            $allowed = array('xls');
            $ext = pathinfo($file['metrix_file']['name'], PATHINFO_EXTENSION);
            if (in_array($ext, $allowed)) {
                if (move_uploaded_file($file["metrix_file"]["tmp_name"], $path)) {
                    //MOVE DATA FROM EXCEL TO DB
                    $type = str_replace('.xls', '', $file['metrix_file']['name']);
                    $objPHPExcel = $phpExcel->load($path);
                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    unlink($path);
                    $dataColsArray = array();
                    if (!empty($sheetData)) {
                        foreach ($sheetData as $key => $data) {
                            if ($key == 1) {
                                foreach ($data as $key => $cols) {
                                    $dataColsArray[$key]['col_name'] = $cols;
                                    $dataColsArray[$key]['assessment_id'] = $sid;
                                    $dataColsArray[$key]['type'] = $type;
                                    $dataColsArray[$key]['creation_date'] = time();
                                    for ($i = 2; $i <= count($sheetData); $i++) {
                                        $dataColsArray[$key]['value'][$i - 1]['col_value'] = $sheetData[$i][$key];
                                        $dataColsArray[$key]['value'][$i - 1]['creation_date'] = time();
                                    }
                                }
                            }
                        }
                    }
                    if ($this->_healthMatxTbl->saveMultipleRecord($dataColsArray)) {
                        //MOVE DATA FROM EXCEL TO DB
                        $message = 'Matrix file has been uploaded successfully.';
                        $this->flashMessenger()->addMessage(array('success' => $message));
                    } else {
                        $message = 'Please provide correct matrix file.';
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                } else {
                    $message = 'Matrix file uploading failed please try again.';
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            } else {
                $message = 'Please upload (.xls) file.';
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
        //FILE UPLOAD CODE
    }

    /*     * *****
     * Action:- Function to set up formula for assisment bases.
     * @author: Hemant.
     * Created Date: 19-02-2015.
     * *** */

    function assesmentformulasAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $postDataArr = array();
        $request = $this->getRequest();
        $user_id = $this->_sessionObj->admin['id'];
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        $this->_QueTbl = $this->getServiceLocator()->get("QuesTable");
        $this->_healthMatxTbl = $this->getServiceLocator()->get("HealthTable");
        $this->_selfFormulaObj = $this->getServiceLocator()->get('AssessmentFormula');
        $message = '';
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if (!empty($_FILES)) {
                $this->_Upload($_FILES, $sid);
            } else {
                if (!empty($postDataArr['col_name'])) {
                    for ($i = 0; $i <= count($postDataArr['col_name']); $i++) {
                        if (!empty($postDataArr['col_name'][$i]) && $postDataArr['col_name'][$i] != 0) {
                            $formulaDataArr['formula_assessment'] = $sid;
                            $formulaDataArr['formula_health'] = $postDataArr['col_name'][$i];
                            $formulaDataArr['formula_type'] = $postDataArr['formula_type'][$i];
                            if ($formulaDataArr['formula_type'] == 'complex') {
                                $formulaDataArr['formula'] = implode(' ', $postDataArr['var_oper'][$i]);
                                $formulaDataArr['response_question'] = NULL;
                            } else {
                                $formulaDataArr['formula'] = NULL;
                                $formulaDataArr['response_question'] = $postDataArr['que_resp'][$i];
                            }
                            if (isset($postDataArr['formula_id'][$i])) {
                                if (!empty($postDataArr['formula_id'][$i])) {
                                    $whereSaveArr = array('formula_id' => $postDataArr['formula_id'][$i]);
                                    $formulaDataArr['updation_date'] = time();
                                    $this->_selfFormulaObj->updateAdminAssessment($whereSaveArr, $formulaDataArr);
                                }
                            } else {
                                $formulaDataArr['creation_date'] = time();
                                $checkWhere = array('formula_assessment' => $sid, 'formula_health' => $formulaDataArr['formula_health'], 'formula_type' => $formulaDataArr['formula_type']);
                                if ($this->_selfFormulaObj->checkAvilable($checkWhere)) {
                                    $this->_selfFormulaObj->insertAdminAssessmentFormula($formulaDataArr);
                                }
                            }
                            $message = 'Formula has been saved successfully.';
                        }
                    }
                    $this->flashMessenger()->addMessage(array('success' => $message));
                } else {
                    $message = 'Unable to save formula please upload excel sheet and try again.';
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            }
        }
        $publish = $this->params()->fromQuery('publish');
        if (!empty($publish)) {
            $formula_id = $this->params()->fromQuery('formula_id');
            if (formula_id) {
                $whereAss = array('ass_id' => $sid);
                $columnAss = array('is_publish' => '1', 'publish_date' => time());
                if ($this->_selfObj->updateAdminAssessment($whereAss, $columnAss)) {
                    return $this->redirect()->toRoute('Defalt Assesments');
                }
            }
        }
        if ($this->params()->fromQuery('formula_id')) {
            $formula_id = base64_decode($this->params()->fromQuery('formula_id'));
            if (!empty($formula_id)) {
                if ($this->_selfFormulaObj->deleteAdminAssessmentFormula(array('formula_id' => $formula_id))) {
                    $message = 'Formula set has been removed successfully.';
                    $this->flashMessenger()->addMessage(array('success' => $message));
                }
            }
        }
        if ($this->params()->fromQuery('type') == 'remove') {
            $file = base64_decode($this->params()->fromQuery('file'));
            $WhereDeleteExcel = array('assessment_id' => $sid, 'type' => $file);
            if ($this->_healthMatxTbl->deleteMetrixData($WhereDeleteExcel)) {
                $message = 'Materix file has been removed successfully.';
                $this->flashMessenger()->addMessage(array('success' => $message));
            }
        }
        $whereFormulaArr = array('formula_assessment' => $sid);
        $whereColumnArr = array('formula_id', 'formula_assessment', 'formula_health', 'formula', 'formula_type', 'response_question');
        $formulaData = $this->_selfFormulaObj->getAdminAssessmentFormula($whereFormulaArr, $whereColumnArr);
        $columns = array("ass_id", "ass_name", "is_publish", "ass_status");
        $whereArr = array("ass_id" => $sid);
        $assessmentArr = $this->_selfObj->getAdminAssessment($whereArr, $columns, "0");
        $whereVarArr = array("assessment_id" => $sid, 'ques_mendatory' => '1');
        $columnsVarArr = array("ques_variable", "ques_question", "ques_id");
        $assessmentVarArr = $this->_QueTbl->getAdminQuestions($whereVarArr, $columnsVarArr, "1", "0");
        $whereHealthArr = array("assessment_id" => $sid, 'datatype' => 'int');
        $columnsHealthArr = array("col_name", "health_id", 'type');
        $healthMetrixArr = $this->_healthMatxTbl->getAdminHealthMetrix($whereHealthArr, $columnsHealthArr, "0", "0");
        $whereHealthUpload = array("assessment_id" => $sid);
        $columnsHealthUpload = array("name" => new \Zend\Db\Sql\Expression('DISTINCT type'));
        $healthMetrixUpload = $this->_healthMatxTbl->getAdminHealthMetrix($whereHealthUpload, $columnsHealthUpload, "0", "0");
        $sendArr = array(
            'assessment' => $assessmentArr[0],
            'assessmentVar' => $assessmentVarArr,
            'healthMetrixArr' => $healthMetrixArr,
            'healthMetrixUpload' => $healthMetrixUpload,
            'formulaDataArr' => $formulaData,
            'sid' => base64_encode($sid),
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- FUNCTION FOR COMPLEX FORMULA SETTING.
     * @author: Hemant.
     * Created Date: 26-02-2015.
     * *** */

    public function complexformulasettingAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $postDataArr = array();
        $request = $this->getRequest();
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $this->_selfFormulaObj = $this->getServiceLocator()->get('AssessmentFormula');
        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        $this->_healthMatxTbl = $this->getServiceLocator()->get("HealthTable");
        $this->_selfFormulaDisplayObj = $this->getServiceLocator()->get("AssessmentFormulaDisplay");
        $columns = array("ass_id", "ass_name", "is_publish", "ass_status");
        $whereArr = array("ass_id" => $sid);
        $assessmentArr = $this->_selfObj->getAdminAssessment($whereArr, $columns, "0");
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if (!empty($postDataArr['display_formula'])) {
                $i = 1;
                foreach ($postDataArr['display_formula'] as $PostDataArr) {
                    if (!empty($postDataArr['column'][$i]) || !empty($postDataArr['display_id'])) {
                        $data = array(
                            'display_formula' => $PostDataArr,
                            'columns' => implode(',', $postDataArr['column'][$i])
                        );
                        if (!empty($postDataArr['display_id'][$i])) {
                            $data['updation_date'] = time();
                            $where = array('display_id' => $postDataArr['display_id'][$i]);
                            $this->_selfFormulaDisplayObj->updateAdminFormulaDisplay($where, $data);
                        } else {
                            $data['creation_date'] = time();
                            $this->_selfFormulaDisplayObj->insertAdminFormulaDislay($data);
                        }
                        $i++;
                    }
                }
                $message = 'Setting has been saved successfully.';
                $this->flashMessenger()->addMessage(array('success' => $message));
            }
            if ($postDataArr['submit'] == 'Save & Publish') {
                $whereAss = array('ass_id' => $sid);
                $columnAss = array('is_publish' => '1', 'publish_date' => time());
                if ($this->_selfObj->updateAdminAssessment($whereAss, $columnAss)) {
                    return $this->redirect()->toRoute('Defalt Assesments');
                }
            }
        }
        $whereFormulaArr = array('formula_assessment' => $sid, 'formula_type' => 'complex');
        $whereColumnArr = array('formula_id', 'formula_assessment', 'formula_health');
        $formulaData = $this->_selfFormulaObj->getAdminAssessmentFormula($whereFormulaArr, $whereColumnArr, "1");
        $whereFormulaArr = array('formula_assessment' => $sid, 'formula_type' => 'response');
        $columnsCount = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'));
        $formulaResp = $this->_selfFormulaObj->getAdminAssessmentFormula($whereFormulaArr, $columnsCount, "0");
        $respCount = $formulaResp['0']['num'];
        if (empty($formulaData) && $respCount > 0) {
            return $this->redirect()->toUrl('admin-self-responce-setting?sid=' . base64_encode($sid));
        } elseif (empty($formulaData) && $respCount == 0) {
            return $this->redirect()->toUrl('admin-self-assesmentformula?sid=' . base64_encode($sid));
        }
        $FinalFormulaArr = array();
        if (!empty($formulaData)) {
            foreach ($formulaData as $FormulaData) {
                $FinalFormulaArr[$FormulaData['formula_id']]['name'] = $FormulaData['type'] . '-' . $FormulaData['col_name'];
                $FinalFormulaArr[$FormulaData['formula_id']]['data'] = array(
                    'display_id' => $FormulaData['display_id'],
                    'columns' => $FormulaData['columns'],
                    'feedback' => $FormulaData['feedback']
                );
                $whereMetArr = array('type' => $FormulaData['type'], 'assessment_id' => $FormulaData['formula_assessment']);
                $columnMetArr = array('col_name', 'health_id');
                $FinalFormulaArr[$FormulaData['formula_id']]['cols'] = $this->_healthMatxTbl->getAdminHealthMetrix($whereMetArr, $columnMetArr);
            }
        }
        $sendArr = array(
            'assessment' => $assessmentArr[0],
            'sid' => base64_encode($sid),
            'respCount' => $respCount,
            'FinalFormulaArr' => $FinalFormulaArr
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- FUNCTION FOR QUESTION RESPONCE SETTING.
     * @author: Hemant.
     * Created Date: 25-02-2015.
     * *** */

    public function questionresponcesettingAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $postDataArr = array();
        $request = $this->getRequest();
        $this->_selfFormulaObj = $this->getServiceLocator()->get('AssessmentFormula');
        $this->_selfObj = $this->getServiceLocator()->get("SelfAssTable");
        $this->_healthMatxTbl = $this->getServiceLocator()->get("HealthTable");
        $this->_selfFormulaDisplayObj = $this->getServiceLocator()->get("AssessmentFormulaDisplay");
        $columns = array("ass_id", "ass_name", "is_publish", "ass_status");
        $whereArr = array("ass_id" => $sid);
        $assessmentArr = $this->_selfObj->getAdminAssessment($whereArr, $columns, "0");
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if (!empty($postDataArr['display_formula'])) {
                $i = 1;
                foreach ($postDataArr['display_formula'] as $PostDataArr) {
                    if (!empty($postDataArr['columns'][$i]) || !empty($postDataArr['display_id'])) {
                        $data = array(
                            'display_formula' => $PostDataArr,
                            'columns' => $postDataArr['columns'][$i],
                            'feedback' => $postDataArr['feedback'][$i]
                        );
                        if (!empty($postDataArr['display_id'][$i])) {
                            $data['updation_date'] = time();
                            $where = array('display_id' => $postDataArr['display_id'][$i]);
                            $this->_selfFormulaDisplayObj->updateAdminFormulaDisplay($where, $data);
                        } else {
                            $data['creation_date'] = time();
                            $this->_selfFormulaDisplayObj->insertAdminFormulaDislay($data);
                        }
                        $i++;
                    }
                }
                $message = 'Setting has been saved successfully.';
                $this->flashMessenger()->addMessage(array('success' => $message));
            }
            if ($postDataArr['submit'] == 'Save & Publish') {
                $whereAss = array('ass_id' => $sid);
                $columnAss = array('is_publish' => '1', 'publish_date' => time());
                if ($this->_selfObj->updateAdminAssessment($whereAss, $columnAss)) {
                    return $this->redirect()->toRoute('Defalt Assesments');
                }
            }
        }
        $whereFormulaArr = array('formula_assessment' => $sid, 'formula_type' => 'response');
        $whereColumnArr = array('formula_id', 'formula_assessment', 'formula_health');
        $formulaData = $this->_selfFormulaObj->getAdminAssessmentFormula($whereFormulaArr, $whereColumnArr, "1");
        $whereFormulaArr = array('formula_assessment' => $sid, 'formula_type' => 'complex');
        $columnsCount = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'));
        $formulaComp = $this->_selfFormulaObj->getAdminAssessmentFormula($whereFormulaArr, $columnsCount, "0");
        $compCount = $formulaComp['0']['num'];
        if (empty($formulaData) && $compCount > 0) {
            return $this->redirect()->toUrl('admin-self-complex-setting?sid=' . base64_encode($sid));
        } elseif (empty($formulaData) && $compCount == 0) {
            return $this->redirect()->toUrl('admin-self-assesmentformula?sid=' . base64_encode($sid));
        }
        $FinalFormulaArr = array();
        if (!empty($formulaData)) {
            foreach ($formulaData as $FormulaData) {
                $FinalFormulaArr[$FormulaData['formula_id']]['question'] = $FormulaData['ques_question'];
                $FinalFormulaArr[$FormulaData['formula_id']]['data'] = array(
                    'display_id' => $FormulaData['display_id'],
                    'columns' => $FormulaData['columns'],
                    'feedback' => $FormulaData['feedback']
                );
                $whereMetArr = array('type' => $FormulaData['type'], 'assessment_id' => $FormulaData['formula_assessment']);
                $columnMetArr = array('col_name', 'health_id');
                $FinalFormulaArr[$FormulaData['formula_id']]['cols'] = $this->_healthMatxTbl->getAdminHealthMetrix($whereMetArr, $columnMetArr);
            }
        }
        $sendArr = array(
            'assessment' => $assessmentArr[0],
            'sid' => base64_encode($sid),
            'compCount' => $compCount,
            'FinalFormulaArr' => $FinalFormulaArr
        );
        return new ViewModel($sendArr);
    }

}
