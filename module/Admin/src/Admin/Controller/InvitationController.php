<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE Invitation PART OF ADMIN.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 14/05/2015.
 * UPDATED: 15/05/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class InvitationController extends AbstractActionController {

    protected $_passDomObj;

    public function init() {
        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');
        $this->_sessionObj = new Container('super_admin');
    }

    /*     * *****
     * Action:- Pass admin Invitation index page .
     * @author: Hemant.
     * Created Date: 14-05-2015.
     * *** */

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        ini_set('max_execution_time', 7200);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $plugin = $this->PluginController();
            if (!empty($postDataArr['email']) && !empty($postDataArr['editor1'])) {
                foreach ($postDataArr['email'] as $email) {
                    $plugin->sendMailAdmin($email, INVITESUBJECT, $postDataArr['editor1']);
                }
                $message = 'Invitation email has been send successfully.';
                $this->flashMessenger()->addMessage(array('success' => $message));
            } else {
                $message = 'Please enter email address and invitation message.';
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
        $sendArr = new ViewModel();
        return $sendArr->setVariables(array(
                    'session' => $this->_sessionObj->admin
        ));
    }

    /*     * *****
     * Action:- Function to check ajax call that email address already exists or not.
     * @author: Hemant.
     * Created Date: 15-05-2015.
     * *** */

    function checkEmailAction() {
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        if (($request->isXmlHttpRequest())) {
            $MembersTable = $this->getServiceLocator()->get("MembersTable");
            $user_id = $postDataArr['id'];
            $exp = '"' . $user_id . '":\\\[("[[:digit:]]*",)*"1"';
            $whereArr = array(new \Zend\Db\Sql\Predicate\Expression(" UserTypeId REGEXP '$exp'"));
            $whereArr['Email'] = $postDataArr['email'];
            $membersCount = $MembersTable->getUserMembers($whereArr, array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));

            if ($membersCount[0]['num'] == 0) {
                echo '1';
            } else {
                echo '0';
            }die;
        }
    }

    function uploadXlsAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest() && $request->isPost() && !$_FILES['excel_file']['error']) {
            $postDataArr = $request->getPost()->toArray();
            $existing = explode(',', $postDataArr['existing']);
            //print_r($existing);die;
            if (!empty($_FILES['excel_file']['name']) && !($_FILES['excel_file']['error'])) {
                $this->_upload($_FILES, $existing);
            }
        }die;
    }

    function _Upload($file = NULL, $existing) {
        $phpExcel = new \PHPExcel();
        $this->init();
        $path = BASE_PATH . "/" . md5(microtime()) . '.xls';
        $ext = pathinfo($file['excel_file']['name'], PATHINFO_EXTENSION);
        if (in_array($ext, array('xls'))) {
            if (move_uploaded_file($file["excel_file"]["tmp_name"], $path)) {
                $sheetData = $phpExcel->load($path)->getActiveSheet()->toArray(null, true, true, true);
                unlink($path);
                $MembersTable = $this->getServiceLocator()->get("MembersTable");
                $UserArray = array();

                $user_id = $this->_sessionObj->admin['id'];
                $callback = function ($email) use (& $MembersTable, & $user_id, & $UserArray, & $existing) {

                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        if (!in_array($email, $existing)) {

                            $exp = '"' . $user_id . '":\\\[("[[:digit:]]*",)*"1"';
                            $whereArr = array(new \Zend\Db\Sql\Predicate\Expression(" UserTypeId REGEXP '$exp'"));
                            $whereArr['Email'] = $email;
                            $membersCount = $MembersTable->getUserMembers($whereArr, array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));

                            if ($membersCount[0]['num'] == 0) {
                                $UserArray[] = $email;
                            }
                        }
                    }
                };
                array_walk_recursive($sheetData, $callback);
            }
        }
        echo json_encode($UserArray);
    }

}
