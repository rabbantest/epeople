<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE CONDITION PART OF ADMIN.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 2/3/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class CronsController extends AbstractActionController {

    protected $_studiesObj;
    protected $_queAnsObj;
    var $objPHPExcel = array();

    function videoAction() {
        
    }

    function caregivervideoAction() {
        // echo date("Y-m-d H:i:s");   die;
    }

    function researchervideoAction() {
        // echo date("Y-m-d H:i:s");   die;
    }
     function beaconvideosAction() {
        // echo date("Y-m-d H:i:s");   die;
    }
    function appvideoAction() {
        // echo date("Y-m-d H:i:s");   die;
    }
    function appvideo1Action() {
        // echo date("Y-m-d H:i:s");   die;
    }
    function appvideo2Action() {
        // echo date("Y-m-d H:i:s");   die;
    }
    function appvideo3Action() {
        // echo date("Y-m-d H:i:s");   die;
    }

    function emailInvitationAction() {
        $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
        $limit = array(0, 80);
        ini_set('max_execution_time', 7200);


        for ($page = 0; $page < 4; $page++) {

            if ($page > 0) {
                $limit = array($limit[1] * $page, $limit[1]);
            }
            $result = $this->_studiesObj->getAdminStudyInvitationSet(array('sent_status' => '0'), array('invitation_email', 'invitation_set_id', 'sent_status', 'domain_id'), "1", array(), $limit);

            if (!empty($result)) {

                foreach ($result as $Data) {
                    $plugin = $this->PluginController();
                    $Domain = $this->getServiceLocator()->get("UserTable");
                    $subdomain = $Domain->getDomain(array('id' => $Data['domain_id']), array('subdomain'));
                    $studyName = $Data['study_title'];
                    $startDate = $Data['start_date'];
                    $endDate = $Data['end_date'];

                    //$Host = str_replace('individual', $subdomain[0]['subdomain'], $_SERVER['HTTP_HOST']);
                    $Host = $_SERVER['HTTP_HOST'];
                    if ($Data['domain_id'] != 1) {
                        $Host = str_replace('my', $subdomain['subdomain'], $_SERVER['HTTP_HOST']);
                    }
                    $Url = $_SERVER['REQUEST_SCHEME'] ."s"."://" . $Host . '/register/' . base64_encode($Data['invitation_set_id']) . '/' . base64_encode($Data['invitation_email']);
                    $message = "Dear User,<br>";
                    $message .= "&nbsp;&nbsp;&nbsp;&nbsp;You are invited to attempt Study name ($studyName).<br>";
                    $message .= "&nbsp;&nbsp;&nbsp;&nbsp;Start Date -$startDate To End Date-$endDate.<br>";
                    $message .= "&nbsp;&nbsp;&nbsp;&nbsp;Study Url: <a href='$Url'>Click Here</a>.<br>";
                    $subject = SITE_NAME . " : Study Invitation";
                    $plugin->sendMailAdmin($Data['invitation_email'], $subject, $message);
                    $this->_studiesObj->updateAdminStudyStudyInvitationSet(array('invitation_set_id' => $Data['invitation_set_id']), array('sent_status' => 1));
                }
                sleep(.1);
            }
        }

       // echo 'done';
        die;
    }

    function _getCodeBook($Study) {
        $QuesTableObj = $this->getServiceLocator()->get("QuesTable");
        $cols = array(
                "Variable Name" => new \Zend\Db\Sql\Predicate\Expression("var_name"),
                "Group Questionaire" => new \Zend\Db\Sql\Predicate\Expression("is_group_questionnaire"),
                'Question' => new \Zend\Db\Sql\Predicate\Expression("ques_question"),
                'Options Value' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(DISTINCT(opt_value) SEPARATOR \'#$#$#\')'),
                'Options Label' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(DISTINCT(opt_option) SEPARATOR \'#$#$#\')')
            );
        $QuestionnaireObj = $this->getServiceLocator()->get("QuestionnaireTable");
        $QueData = $QuesTableObj->getStudyQuestion(array('studies_id' => $Study['study_id']), $cols, 1, array(), 1);

        $objPHPExcel = new \PHPExcel();

        $jCode = 1;
        unset($Study['passive_last_date']);
        unset($Study['source']);
        $LableArray = array('study_description' => 'Study Description', 'researcher_name' => 'Researcher Name', 'study_type' => 'Study Type', 'study_title' => 'Study Name', 'start_date' => 'Study Duration', 'end_date' => 'Study Duration','study_trials'=>'Study Trials');
        if (!empty($Study)) {
            foreach ($Study as $Key => $Index) {
                if (!in_array($Key, array('study_id', 'study_type', 'start_date', 'end_date'))) {
                    $iCode = 0;
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($iCode, $jCode)->getFont()->setBold(true);

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $LableArray[$Key]);
                    $iCode++;
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $Index);
                    $jCode++;
                }
            }
            $iCode = 0;
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($iCode, $jCode)->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $LableArray['start_date']);
            $iCode++;
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $Study['start_date'] . ' TO ' . $Study['end_date']);
            $jCode++;

            $iCode = 0;
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($iCode, $jCode)->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $LableArray['study_type']);
            $iCode++;
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
            $type = ($Study['study_type'] == 1) ? 'Active' : 'Active & Passive';
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $type);
            $jCode++;
            $jCode++;
            $jCode++;
        }

       if (!empty($QueData)) {
                foreach ($QueData as $Que) {
                    $iCode = 0;
                    $j = 1;
                    $groupQ = $Que['Group Questionaire'];
                    $Qcoun = 0;
                    $groupQuestionCount=1;
                    foreach ($Que as $QKey => $QIndex) {
                        if ($groupQ != 0) {
                            if (in_array($QKey, array('Group Questionaire', 'Question'))) {

                                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($iCode, $jCode)->getFont()->setBold(true);
                                $WHereQuestion = array('id' => $groupQ);
                                if ($QKey != 'Question') {
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $QKey);
                                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                                    $q_Name = $QuestionnaireObj->getAdminQuestionnaire($WHereQuestion, array('name'));
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode + 1, $q_Name[0]['name']);
                                } else {
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, 'Questions');
                                    $WHereQ = array('questionnaire_id' => $groupQ);
                                    $fetchItem = array('ques_question');
                                    $QuestionnaireOptions = $QuestionnaireObj->getAdminQuestions($WHereQ, $fetchItem);
                                    $groupQuestionCount = count($QuestionnaireOptions);
                                    $k = 1;
                                    $whereArrOptions = array("Q_id" => $groupQ);
                                    $columnss = array(
                                        'Options Value' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(DISTINCT(opt_value) SEPARATOR \'#$#$#\')'),
                                        'Options Label' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(DISTINCT(opt_option) SEPARATOR \'#$#$#\')')
                                    );
                                    $QuestionnaireOptionss = $QuestionnaireObj->getAdminQuestionsOption($whereArrOptions, $columnss, array(), "1");
                                    /*                                     * End* */
                                    $Qcoun = count($QuestionnaireOptionss);
                                    foreach ($QuestionnaireOptions as $question) {
                                        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode + $k + $Qcoun, $question['ques_question']);
                                        $k++;
                                    }
                                }
                                $iCode++;
                            }
                            if (in_array($QKey, array('Options Value', 'Options Label'))) {

                                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($iCode, $jCode)->getFont()->setBold(true);

                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $QKey);
                                $whereArrOption = array("Q_id" => $groupQ);
                                $columns = array(
                                    'Options Value' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(DISTINCT(opt_value) SEPARATOR \'#$#$#\')'),
                                    'Options Label' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(DISTINCT(opt_option) SEPARATOR \'#$#$#\')')
                                );
                                $QuestionnaireOptions = $QuestionnaireObj->getAdminQuestionsOption($whereArrOption, $columns, array(), "1");
                                $j = 1;
                                $a = array('a', 'b');
                                //  foreach($a as $as){
                                //$p=0;
                                foreach ($QuestionnaireOptions as $dataArr) {

                                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode + $j, $dataArr[$QKey]);
                                    $j++;
                                }


                                // }
                                $iCode++;
                                //$p++;
                            }
                        } else {
                            if (in_array($QKey, array('Variable Name', 'Question'))) {

                                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($iCode, $jCode)->getFont()->setBold(true);

                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $QKey);

                                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode + 1, $QIndex);
                                $iCode++;
                            }

                            if (in_array($QKey, array('Options Value', 'Options Label'))) {

                                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($iCode, $jCode)->getFont()->setBold(true);

                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $QKey);

                                $DataArr = explode('#$#$#', $QIndex);
                                $j = 1;
                                foreach ($DataArr as $dataArrRR) {

                                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode + $j, $dataArrRR);
                                    $j++;
                                }

                                $iCode++;
                            }
                        }
                    }

                    $jCode = $jCode + $j;
                    $jCode = $jCode + $Qcoun + $groupQuestionCount;
                }
            }

        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objPHPExcel->getActiveSheet()->setTitle($Study['study_title']);
        if ($Study['study_type'] == 1) {
            $dirActive = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Active/";
            if (!file_exists($dirActive . $Study['study_id'])) {
                mkdir($dirActive . $Study['study_id'], 777);
                chmod($dirActive . $Study['study_id'], 0777);
            }
            $objWriter->save($dirActive . $Study['study_id'] . '/' . $Study['study_title'] . '-Code-Book.xlsx');
        } else if ($Study['study_type'] == 2) {
            $dirActive = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Active/";
            if (!file_exists($dirActive . $Study['study_id'])) {
                mkdir($dirActive . $Study['study_id'], 777);
                chmod($dirActive . $Study['study_id'], 0777);
            }
            $objWriter->save($dirActive . $Study['study_id'] . '/' . $Study['study_title'] . '-Code-Book.xlsx');
            $dir = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Passive/";
            if (!file_exists($dir . $Study['study_id'])) {
                mkdir($dir . $Study['study_id'], 777);
                chmod($dir . $Study['study_id'], 0777);
            }
            $objWriter->save($dir . $Study['study_id'] . '/' . $Study['study_title'] . '-Code-Book.xlsx');
        }

        $dirActive = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Email/";
        if (!file_exists($dirActive . $Study['study_id'])) {
            mkdir($dirActive . $Study['study_id'], 777);
            chmod($dirActive . $Study['study_id'], 0777);
        }
        $objWriter->save($dirActive . $Study['study_id'] . '/' . $Study['study_title'] . '-Code-Book.xlsx');

        if ($Study['study_trials'] == 1) {
            $dirActive = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Trial/";
            if (!file_exists($dirActive . $Study['study_id'])) {
                mkdir($dirActive . $Study['study_id'], 777);
                chmod($dirActive . $Study['study_id'], 0777);
            }
            $objWriter->save($dirActive . $Study['study_id'] . '/' . $Study['study_title'] . '-Code-Book.xlsx');
        }
        unset($objPHPExcel);
        unset($objWriter);
    }

    /*     * *****
     * Action:-Function for cron to get study Passive data.
     * @author: Hemant Chuphal.
     * Created Date: 02-06-2015.
     * *** */

    public function studyPassiveDataAction() {
        $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
        $PassiveTableObj = $this->getServiceLocator()->get("PassiveTable");
        $curDate = date('Y-m-d');
        ini_set('max_execution_time', 7200);
        $StudyData = $this->_studiesObj->getAdminStudy(array('study_type' => 2, 'passive_status' => 0, new \Zend\Db\Sql\Predicate\Expression("end_date < '$curDate'")), array('study_id', 'study_title', 'study_description', 'researcher_name', 'start_date', 'end_date', 'study_type', 'source', 'passive_last_date', 'study_trials'), '0', array(), array(0, 1));

        if (!empty($StudyData)) {
            foreach ($StudyData as $Study) {

                $dirActive = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Active/";
                $filename = $dirActive . $Study['study_id'] . '/' . $Study['study_title'] . '-Code-Book.xlsx';
                $dirPassive = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Passive/";
                $filenamePassive = $dirActive . $Study['study_id'] . '/' . $Study['study_title'] . '-Code-Book.xlsx';

                if (!file_exists($filename) || !file_exists($filename)) {
                    $this->_getCodeBook($Study);
                }

                $JsonDataArray = json_decode($Study['source'], true);
                $increment = $JsonDataArray['TimeInterval'];
                $sourceArray = $JsonDataArray['SOURCE'];

                $day = 60 * 60 * 24;
                $end = strtotime($Study['end_date']);
                $start = !empty($Study['passive_last_date']) ? strtotime($Study['passive_last_date']) : strtotime($Study['start_date']); //Check last updated date and calculate start date
                $diff = round(($end - $start) / $day);
                $i = 0;
                echo date('H:i:s') . " Start.\r\n";
                while ($i <= $diff && $i <= $increment * 2) {
                    if (isset($sourceArray['Global'])) {
                        foreach ($sourceArray['Global'] as $src) {
                         // echo 'if';  echo '<pre>';print_r($src);die;
                            $j = 1;
                            $page = 0;
                            $count = 0;
                            $set = 1;
                            $lastDay = $i - 1;
                            $nextDay = $i + $increment;
                            $nextDay = ($nextDay > $diff) ? $diff : $nextDay;

                            $Dates['start_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $lastDay . 'days')));
                            $Dates['cur_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $i . 'days')));
                            $Dates['end_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $nextDay . 'days')));
                            $Dates['source'] = $src;
                            $this->_savePassiveExcel($Study, $page, $j, $PassiveTableObj, $count, $set, $Dates);
                        }
                    } else if (isset($sourceArray['Element'])) {
                       //echo 'else'; echo '<pre>';print_r($sourceArray['Element']);die;
                        $j = 1;
                        $page = 0;
                        $count = 0;
                        $set = 1;
                        $lastDay = $i - 1;
                        $nextDay = $i + $increment;
                        $nextDay = ($nextDay > $diff) ? $diff : $nextDay;

                        $Dates['start_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $lastDay . 'days')));
                        $Dates['cur_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $i . 'days')));
                        $Dates['end_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $nextDay . 'days')));
                        $Dates['source'] = $sourceArray['Element'];
                        $this->_savePassiveExcel($Study, $page, $j, $PassiveTableObj, $count, $set, $Dates);
                        // CODE FOR ELEMENT BY SOURCE ALLOCATION
                    } else {
                        exit;
                    }
                    $i = $i + $increment;
                }
                $i = $i - 1;
                $finalDate = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $i . 'days')));
                if (strtotime($Study['end_date']) <= strtotime($finalDate)) {
//                    Update Date And status and create ZIP
                    $filter = new \Zend\Filter\Compress(array(
                        'adapter' => 'Zip',
                        'options' => array(
                            'archive' => $_SERVER['DOCUMENT_ROOT'] . '/StudyDump/Passive/' . $Study['study_id'] . "/" . $Study['study_title'] . '.zip'
                    )));
                    $compress = $filter->filter($_SERVER['DOCUMENT_ROOT'] . '/StudyDump/Passive/' . $Study['study_id']);
                    $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
                    $this->_studiesObj->updateAdminStudy(array('study_id' => $Study['study_id']), array('passive_status' => 1));
                } else {
                    //Update Date
                    $this->_studiesObj->updateAdminStudy(array('study_id' => $Study['study_id']), array('passive_status' => 0, 'passive_last_date' => $finalDate));
                }

                //echo date('H:i:s') . " End\n";
            }
        }
        exit;
//        echo '<pre>';
//        print_r($StudyData);
//        die;
    }

    function _savePassiveExcel($study = NULL, $page = NULL, $j, $PassiveTableObj, $count, $set, $Dates) {
        if ($count == 0) {
            $this->objPHPExcel[$set] = new \PHPExcel();
        }

        $SetArray = array(
            'Height' => array('MIN HEIGHT IN CENTIMETER', 'MAX HEIGHT IN CENTIMETER'),
            'Weight' => array('MIN WEIGHT IN POUNDS', 'MAX WEIGHT IN POUNDS'),
            'PhysicalData' => array('TOTAL STEPS', 'TOTAL DURATION', 'TOTAL DISTANCE', 'TOTAL CALORIES BURNED'),
            'Cholesterol' => array('MIN LDL', 'MAX LDL', 'MIN HDL', 'MAX HDL', 'MIN TRIGLYCERIDES', 'MAX TRIGLYCERIDES'),
            'Blood_Pressure' => array('MIN SYSTOLIC', 'MAX SYSTOLIC', 'MIN DIASTOLIC', 'MAX DIASTOLIC', 'MIN PULSE', 'MAX PULSE'),
            'Glucose' => array('MIN GLUCOSE', 'MAX GLUCOSE'),
            'SleepRecord' => array('TOTAL SLEEP', 'TOTAL AWAKE', 'TOTAL DEEP', 'TOTAL LIGHT', 'TOTAL RIM'),
            'Temperature' => array('MIN TEMPERATURE', 'MAX TEMPERATURE')
        );

        if (is_array($Dates['source'])) {
            foreach ($SetArray as $Srckey => $Srcindex) {
                if (!isset($Dates['source'][$Srckey])) {
                    unset($SetArray[$Srckey]);
                }
            }
        }

        $limit = array(0, 200);
        $cols = array('varName' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(var_name)'),
            'optionVal' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(opt_value)')
        );
        if ($page > 0) {
            $limit = array($limit[1] * $page, $limit[1]);
        }
        $page++;

        $dataa = $PassiveTableObj->getAdminViewData(array('study_id' => $study['study_id']), $cols, '0', array(), $limit, '1', $Dates, '1');


        if (!empty($dataa)) {

            $this->objPHPExcel[$set]->setActiveSheetIndex(0);
            foreach ($dataa as $Data) {

                $i = 0;
                if ($j == 1) {
                    $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, 'Email');
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i + 1, $j, 'UserID');
                }

                $j = $j + 1;
                $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);

                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $Data['Email']);
                $i++;
                $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $Data['UserID']);

                $newArray = array_combine(explode(',', $Data['varName']), explode(',', $Data['optionVal']));
                if (!empty($newArray)) {

                    foreach ($newArray as $var => $opt) {
                        $i++;
                        if ($j == 2) {
                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $var);
                        }
                        $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                        $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $opt);
                    }
                }
                unset($Data['varName']);
                unset($Data['optionVal']);
                unset($Data['Email']);
                unset($Data['UserID']);
                foreach ($SetArray as $key => $index) {
                    if (!empty($Data[$key])) {
                        $dataFieldsVal = explode('-', $Data[$key]);
                        $pos = 0;
                        foreach ($index as $field) {
                            $i++;
                            if ($j == 2) {
                                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $field . ' (' . $Dates['source'] . ')');
                            }
                            $val = !empty($dataFieldsVal[$pos]) ? $dataFieldsVal[$pos] : '';
                            $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $val);
                            $pos++;
                        }
                    } else {
                        foreach ($index as $field) {
                            $i++;
                            if ($j == 2) {
                                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $field . ' (' . $Dates['source'] . ')');
                            }
                            $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, '');
                        }
                    }
                    unset($Data[$key]);
                }
                foreach ($Data as $key => $index) {
                    $i++;
                    if ($j == 2) {
                        $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $key);
                    }
                    $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $index);
                }

                $count++;
            }

            if (($count % 10000 == 0 && $count != 0)) {
                echo $count . '<br>';
                sleep(1);

                $objWriter[$set] = new \PHPExcel_Writer_Excel2007($this->objPHPExcel[$set]);
                $dir = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Passive/";
                if (!file_exists($dir . $study['study_id'])) {
                    mkdir($dir . $study['study_id'], 777);
                    chmod($dir . $study['study_id'], 0777);
                }

                $this->objPHPExcel[$set]->getActiveSheet()->setTitle($study['study_title']);
                if (is_array($Dates['source'])) {
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-date-' . $Dates['cur_date'] . '-set-' . $set . '.xlsx');
                } else {
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-date-' . $Dates['cur_date'] . '-source-' . $Dates['source'] . '-set-' . $set . '.xlsx');
                }
                unset($objWriter[$set]);
                unset($this->objPHPExcel[$set]);
                $j = 1;
                $set++;
                $this->objPHPExcel[$set] = new \PHPExcel();
            }
            sleep(1);

            $this->_savePassiveExcel($study, $page, $j, $PassiveTableObj, $count, $set, $Dates);
        } else {

            $dir = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Passive/";
            if (($count % 10000 != 0)) {
                echo $count . '<br>';
                sleep(1);

                $objWriter[$set] = new \PHPExcel_Writer_Excel2007($this->objPHPExcel[$set]);

                if (!file_exists($dir . $study['study_id'])) {
                    mkdir($dir . $study['study_id'], 777);
                    chmod($dir . $study['study_id'], 0777);
                }

                $this->objPHPExcel[$set]->getActiveSheet()->setTitle($study['study_title']);
                if (is_array($Dates['source'])) {
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-date-' . $Dates['cur_date'] . '-set-' . $set . '.xlsx');
                } else {
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-date-' . $Dates['cur_date'] . '-source-' . $Dates['source'] . '-set-' . $set . '.xlsx');
                }

                unset($objWriter[$set]);
                unset($this->objPHPExcel[$set]);
            }
        }
    }

    /*     * *****
     * Action:-Function for cron to get study data.
     * @author: Hemant Chuphal.
     * Created Date: 14-5-2015.
     * *** */

    public function studyActiveDataAction() {
        $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
        $StudyEligibleTableObj = $this->getServiceLocator()->get("StudyEligibleTable");
        $curDate = date('Y-m-d');
        ini_set('max_execution_time', 7200);
        $StudyData = $this->_studiesObj->getAdminStudy(array('activexls_status' => 0, new \Zend\Db\Sql\Predicate\Expression("end_date < '$curDate'")), array('study_id', 'study_title', 'study_description', 'researcher_name', 'start_date', 'end_date', 'study_type', 'source', 'passive_last_date', 'study_trials'), '0', array(), array(0, 1));

        if (!empty($StudyData)) {
            foreach ($StudyData as $Study) {

                $dirActive = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Active/";
                $filename = $dirActive . $Study['study_id'] . '/' . $Study['study_title'] . '-Code-Book.xlsx';

                if (!file_exists($filename)) {
                    $this->_getCodeBook($Study);
                }

                $j = 1;
                //echo date('H:i:s') . " Start.\r\n";
                $page = 0;
                $count = 0;
                $set = 1;
                $this->_saveExcel($Study, $page, $j, $StudyEligibleTableObj, $count, $set);
                //echo date('H:i:s') . " End\n";
            }
        }
        exit;
    }

    function _saveExcel($study = NULL, $page = NULL, $j, $StudyEligibleTableObj, $count, $set) {
        if ($count == 0) {
            $this->objPHPExcel[$set] = new \PHPExcel();
        }


        $limit = array(0, 500);
        $cols = array('varName' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(var_name)'),
            'optionVal' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(opt_value)')
        );
        if ($page > 0) {
            $limit = array($limit[1] * $page, $limit[1]);
        }
        $page++;
        $data = $StudyEligibleTableObj->getAdminViewData(array('study_id' => $study['study_id']), $cols, '0', array(), $limit, '1', '1');
//        echo '<pre>';
//        print_r($data);die;
        if (!empty($data)) {

            $this->objPHPExcel[$set]->setActiveSheetIndex(0);
            foreach ($data as $Data) {

                $i = 0;
                if ($j == 1) {
                    $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, 'Email');
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i + 1, $j, 'UserID');
                }

                $j = $j + 1;
                $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);

                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $Data['Email']);

                $i++;
                $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $Data['UserID']);

                $newArray = array_combine(explode(',', $Data['varName']), explode(',', $Data['optionVal']));
                if (!empty($newArray)) {

                    foreach ($newArray as $var => $opt) {
                        $i++;
                        if ($j == 2) {
                            $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);

                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $var);
                        }
                        $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);

                        $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $opt);
                    }
                }
                $count++;
            }

            if (($count % 10000 == 0 && $count != 0)) {
                echo $count . '<br>';
                sleep(1);

                $objWriter[$set] = new \PHPExcel_Writer_Excel2007($this->objPHPExcel[$set]);
                $dir = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Active/";
                if (!file_exists($dir . $study['study_id'])) {
                    mkdir($dir . $study['study_id'], 777);
                    chmod($dir . $study['study_id'], 0777);
                }
                $this->objPHPExcel[$set]->getActiveSheet()->setTitle($study['study_title']);
                $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-set-' . $set . '.xlsx');
                unset($objWriter[$set]);
                unset($this->objPHPExcel[$set]);
                $j = 1;
                $set++;
                $this->objPHPExcel[$set] = new \PHPExcel();
            }
            sleep(1);
            $this->_saveExcel($study, $page, $j, $StudyEligibleTableObj, $count, $set);
        } else {
            $dir = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Active/";
            if (($count % 10000 != 0)) {
                echo $count . '<br>';
                sleep(1);

                $objWriter[$set] = new \PHPExcel_Writer_Excel2007($this->objPHPExcel[$set]);

                if (!file_exists($dir . $study['study_id'])) {
                    mkdir($dir . $study['study_id'], 777);
                    chmod($dir . $study['study_id'], 0777);
                }
                $this->objPHPExcel[$set]->getActiveSheet()->setTitle($study['study_title']);
                $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-set-' . $set . '.xlsx');
                unset($objWriter[$set]);
                unset($this->objPHPExcel[$set]);
            }

            $filter = new \Zend\Filter\Compress(array(
                'adapter' => 'Zip',
                'options' => array(
                    'archive' => $_SERVER['DOCUMENT_ROOT'] . '/StudyDump/Active/' . $study['study_id'] . "/" . $study['study_title'] . '.zip'
            )));
            $compress = $filter->filter($dir . $study['study_id']);

            $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
            $this->_studiesObj->updateAdminStudy(array('study_id' => $study['study_id']), array('activexls_status' => 1));
        }
    }

    /*     * *****
     * Action:-Function for cron.
     * @author: Hemant Chuphal.
     * Created Date: 14-5-2015.
     * *** */

    public function indexAction() {
        ini_set('max_execution_time', 7200);
        $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
        $this->_queAnsObj = $this->getServiceLocator()->get("StudyEligibleTable");
        $curDate = date('Y-m-d');
        $StudyData = $this->_studiesObj->getAdminStudy(array('publish_status' => 1), array('study_id', 'start_date', 'end_date', 'screening', 'study_title', 'created_by'), '0', array(), array(0, 1), $curDate);
        $Study = array_column($StudyData, 'study_id');
        $StudyPass = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'created_by'));
        $StudyType = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'screening'));
        $StudyName = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'study_title'));
        $StudyStartDate = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'start_date'));
        $StudyEndDate = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'end_date'));
        $user = array();
        $ids = array();

        if ($Study) {
            $StudyVarSet = $this->_studiesObj->getAdminStudyVarSet(array('set_study' => $Study), array('set_study', 'json', 'total_combination'));
            if (!empty($StudyVarSet)) {
                foreach ($StudyVarSet as $SetData) {
                    $JsonArray = isset($SetData['json']) ? json_decode($SetData['json'], true) : array();
                    $conditionsArray = !empty($JsonArray) ? $this->_getCondition($JsonArray, $SetData['total_combination']) : array();
                    if (!empty($conditionsArray['Variables'])) {
                        foreach ($conditionsArray['Variables'] as $key => $Variables) {

                            $Cron = $this->_queAnsObj->getAdminStudyBucketCronSetting(array(
                                'study_id' => $SetData['set_study'],
                                'bucket' => $key,
                                'sent' => 0
                                    ), array(
                                'number', 'id'
                                    ), '0', array(), array(0, 2));

                            if (!empty($Cron)) {
                                $limit = 0;
                                foreach ($Cron as $number) {

                                    $existingUser = $this->_queAnsObj->getAdminStudyBucketInvitation(array('study_id' => $SetData['set_study'], 'bucket' => $key), array('user_id'));
                                    $existingUsers = array_combine(array_column($existingUser, 'user_id'), array_column($existingUser, 'user_id'));

                                    $user[$SetData['set_study']][$key][] = $this->_getEligibleUsersList($Variables, isset($conditionsArray['Common']) ? $conditionsArray['Common'] : array(), array($limit, $number['number']), $existingUsers, $StudyPass[$SetData['set_study']]);
                                    $limit = $limit + $number['number'];
                                    $ids[] = $number['id'];
                                }
                            }
                        }
                    } else {
                        $Cron = $this->_queAnsObj->getAdminStudyBucketCronSetting(array(
                            'study_id' => $SetData['set_study'],
                            'bucket' => 0,
                            'sent' => 0
                                ), array(
                            'number', 'id'
                                ), '0', array(), array(0, 2));

                        if (!empty($Cron)) {
                            $limit = 0;
                            foreach ($Cron as $number) {

                                $existingUser = $this->_queAnsObj->getAdminStudyBucketInvitation(array('study_id' => $SetData['set_study'], 'bucket' => 0), array('user_id'));
                                $existingUsers = array_combine(array_column($existingUser, 'user_id'), array_column($existingUser, 'user_id'));

                                $user[$SetData['set_study']][0][] = $this->_getEligibleUsersList(NULL, isset($conditionsArray['Common']) ? $conditionsArray['Common'] : array(), array($limit, $number['number']), $existingUsers, $StudyPass[$SetData['set_study']]);
                                $limit = $limit + $number['number'];
                                $ids[] = $number['id'];
                            }
                        }
                    }
                }
            }
        }
        if (!empty($user)) {
            $data = array();

            foreach ($user as $key => $indexMain) {
                $data['study_id'] = $key;
                foreach ($indexMain as $bucket => $index) {
                    $data['bucket'] = $bucket;
                    $data['type'] = ($StudyType[$key]) ? 0 : 1;
                    $data['eligible_for_main'] = ($StudyType[$key]) ? 0 : 1;
                    $this->_saveAndSendInvitation($data, $this->_queAnsObj, $index, $StudyName[$data['study_id']], $StudyStartDate[$data['study_id']], $StudyEndDate[$data['study_id']], $StudyPass[$data['study_id']]);
                }
            }
        }
        if (!empty($ids)) {
            $this->_queAnsObj->updateAdminStudyBucketCronSetting(array('id' => $ids), array('sent' => 1));
        }
        exit('done');
    }

    /*     * *****
     * Action:- Send Email AND Save Invitation
     * @author: Hemant Chuphal.
     * Created Date: 13-04-2015.
     * *** */

    function _saveAndSendInvitation($data = NULL, $Obj = NULL, $index = NULL, $studyName, $startDate, $endDate, $StudyPass) {
        $plugin = $this->PluginController();
        $Domain = $this->getServiceLocator()->get("UserTable");
        $subdomain = $Domain->getDomain(array('id' => $StudyPass), array('subdomain'));
        foreach ($index as $bucket => $users) {
            if (!empty($users)) {
                foreach ($users as $id => $email) {
                    $data['user_id'] = $id;
                    $data['creation_date'] = time();

                    $Obj->insertAdminStudyBucketInvitation($data);
                }

                $Host = str_replace('my', $subdomain[0]['subdomain'], $_SERVER['HTTP_HOST']);
                $Url = $_SERVER['REQUEST_SCHEME'] . "://" . $Host . '/individual-study';
                $message = "Dear User,<br>";
                $message .= "You have been invited to participate in the following study: Study Name ($studyName).  To accept this request and participate in the study, please visit the <b>My Research / Studies</b> section of your account by clicking <a href='$Url'>here</a> or within your Q app.  This study is available from: Start Date -$startDate To End Date-$endDate..<br>";
                $message .="<br><br>Here’s to your health,";
                $message .="<br>The Quantextual Health Team";
                $subject = SITE_NAME . " : Study Invitation";
                $plugin->sendMailAdmin($users, $subject, $message);
                sleep(.5);
            }
            sleep(1);
        }
    }

    /*     * *****
     * Action:- Get Eligible Users for study
     * @author: Hemant Chuphal.
     * Created Date: 13-04-2015.
     * *** */

    function _getEligibleUsersList($VarData, $CommonData, $limit, $existingUsers, $pass) {
        $this->_queAnsObj = $this->getServiceLocator()->get("StudyEligibleTable");
        $where = array();
        $cols = array('UserID', 'Email');
        $VarResultData = array();

        if (!empty($VarData)) {
            $i = 0;
            if (is_array($VarData)) {
                foreach ($VarData as $Var) {
                    $Var = json_decode($Var, TRUE);
                    $where[$i] = array('ques_variable' => $Var[0], 'option_id' => $Var[1]);
                    $i++;
                }
            } else {
                $Var = json_decode($VarData, TRUE);
                $where[$i] = array('ques_variable' => $Var[0], 'option_id' => $Var[1]);
                $i++;
            }

            $VarResultData = $this->_queAnsObj->getAdminVarStudyEligibleUser($where, $cols, '1', $CommonData, $limit, '0', '1', $existingUsers, $pass);
        } else {
            $VarResultData = $this->_queAnsObj->getAdminVarStudyEligibleUser(NULL, $cols, '1', $CommonData, $limit, '0', '1', $existingUsers, $pass);
        }
        sleep(.5);
        return array_combine(array_column($VarResultData, 'UserID'), array_column($VarResultData, 'Email'));
    }

    /*     * *****
     * Action:- Get condition set for study
     * @author: Hemant Chuphal.
     * Created Date: 13-04-2015.
     * *** */

    function _getCondition($JsonArray, $totalComb = NULL) {
        $result = array();
        if ($totalComb < 16) {
            foreach ($JsonArray as $key => $Data) {
                if (isset($Data[2])) {
                    $var_id = $Data[2]['var_id'];
                    $resultData = array();
                    array_walk($Data[2]['option_id'], function($value) use (& $var_id, & $resultData) {
                        $resultData[] = json_encode(array($var_id, $value));
                    });
                    $result['Variables'][$var_id] = $resultData;
                    unset($JsonArray[$key]);
                }
            }
        } else {
            $resultData = array();
            foreach ($JsonArray as $key => $Data) {
                if (isset($Data[2])) {
                    $var_id = $Data[2]['var_id'];

                    array_walk($Data[2]['option_id'], function($value) use (& $var_id, & $resultData) {
                        $resultData = array_merge($resultData, array(json_encode(array($var_id, $value))));
                    });


                    unset($JsonArray[$key]);
                }
            }
            $result['Variables'][$var_id] = $resultData;
        }


        $result['Variables'] = !empty($result['Variables']) ? array_values($result['Variables']) : array();
        $result['Variables'] = ($totalComb < 16) ? $this->_combinations($result['Variables']) : $result['Variables'];
        $result['Common'] = $JsonArray;

        return $result;
    }

    /*     * *****
     * Action:- get combination based on varrable for study
     * @author: Hemant Chuphal.
     * Created Date: 10-04-2015.
     * *** */

    function _combinations($arrays, $i = 0) {
        if (!isset($arrays[$i])) {
            return array();
        }
        if ($i == count($arrays) - 1) {
            return $arrays[$i];
        }
        $tmp = $this->_combinations($arrays, $i + 1);

        $result = array();
        foreach ($arrays[$i] as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t) ?
                        array_merge(array($v), $t) :
                        array($v, $t);
            }
        }

        return $result;
    }

    // Email 

    public function studyEmailDataAction() {
        $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
        $PassiveTableObj = $this->getServiceLocator()->get("PassiveTable");
        $curDate = date('Y-m-d');
        ini_set('max_execution_time', 7200);
        $StudyData = $this->_studiesObj->getAdminStudy(array('emailxls_status' => 0, new \Zend\Db\Sql\Predicate\Expression("end_date < '$curDate'")), array('study_id', 'study_title', 'study_description', 'researcher_name', 'start_date', 'end_date', 'study_type', 'source', 'passive_last_date', 'study_trials'), '0', array(), array(0, 1));

        if (!empty($StudyData)) {
            foreach ($StudyData as $Study) {

                $dirActive = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Email/";
                $filename = $dirActive . $Studie['study_id'] . '/' . $Studie['study_title'] . '-Code-Book.xlsx';

                if (!file_exists($filename) || !file_exists($filename)) {
                    $this->_getCodeBook($Study);
                }

                $JsonDataArray = json_decode($Study['source'], true);
                $increment = $JsonDataArray['TimeInterval'];
                $sourceArray = $JsonDataArray['SOURCE'];

                $day = 60 * 60 * 24;
                $end = strtotime($Study['end_date']);
                $start = !empty($Study['passive_last_date']) ? strtotime($Study['passive_last_date']) : strtotime($Study['start_date']); //Check last updated date and calculate start date
                $diff = round(($end - $start) / $day);
                $i = 0;
                echo date('H:i:s') . " Start.\r\n";
                while ($i <= $diff && $i <= $increment * 2) {
                    if (isset($sourceArray['Global'])) {
                        foreach ($sourceArray['Global'] as $src) {
//                            echo '<pre>';
//                            print_r($sourceArray['Global']);
//                            die;
                            $j = 1;
                            $page = 0;
                            $count = 0;
                            $set = 1;
                            $lastDay = $i - 1;
                            $nextDay = $i + $increment;
                            $nextDay = ($nextDay > $diff) ? $diff : $nextDay;

                            $Dates['start_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $lastDay . 'days')));
                            $Dates['cur_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $i . 'days')));
                            $Dates['end_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $nextDay . 'days')));
                            $Dates['source'] = $src;
                            $this->_saveEmailExcel($Study, $page, $j, $PassiveTableObj, $count, $set, $Dates);
                        }
                    } else if (isset($sourceArray['Element'])) {

                        $j = 1;
                        $page = 0;
                        $count = 0;
                        $set = 1;
                        $lastDay = $i - 1;
                        $nextDay = $i + $increment;
                        $nextDay = ($nextDay > $diff) ? $diff : $nextDay;

                        $Dates['start_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $lastDay . 'days')));
                        $Dates['cur_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $i . 'days')));
                        $Dates['end_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $nextDay . 'days')));
                        $Dates['source'] = $sourceArray['Element'];

                        $this->_saveEmailExcel($Study, $page, $j, $PassiveTableObj, $count, $set, $Dates);
                        // CODE FOR ELEMENT BY SOURCE ALLOCATION
                    } else {
                        die('done');
                    }
                    $i = $i + $increment;
                }
                $i = $i - 1;
                $finalDate = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $i . 'days')));
                if (strtotime($Study['end_date']) <= strtotime($finalDate)) {
//                    Update Date And status and create ZIP
                    $filter = new \Zend\Filter\Compress(array(
                        'adapter' => 'Zip',
                        'options' => array(
                            'archive' => $_SERVER['DOCUMENT_ROOT'] . '/StudyDump/Email/' . $Study['study_id'] . "/" . $Study['study_title'] . '.zip'
                    )));
                    $compress = $filter->filter($_SERVER['DOCUMENT_ROOT'] . '/StudyDump/Email/' . $Study['study_id']);
                    $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
                    $this->_studiesObj->updateAdminStudy(array('study_id' => $Study['study_id']), array('emailxls_status' => 1));
                } else {
                    //Update Date
                    $this->_studiesObj->updateAdminStudy(array('study_id' => $Study['study_id']), array('emailxls_status' => 0, 'passive_last_date' => $finalDate));
                }

                echo date('H:i:s') . " End\n";
            }
        }
        die('done');
//        echo '<pre>';
//        print_r($StudyData);
//        die;
    }

    function _saveEmailExcel($study = NULL, $page = NULL, $j, $PassiveTableObj, $count, $set, $Dates) {
        if ($count == 0) {
            $this->objPHPExcel[$set] = new \PHPExcel();
        }

        $SetArray = array(
            'Height' => array('MIN HEIGHT IN CENTIMETER', 'MAX HEIGHT IN CENTIMETER'),
            'Weight' => array('MIN WEIGHT IN POUNDS', 'MAX WEIGHT IN POUNDS'),
            'PhysicalData' => array('TOTAL STEPS', 'TOTAL DURATION', 'TOTAL DISTANCE', 'TOTAL CALORIES BURNED'),
            'Cholesterol' => array('MIN LDL', 'MAX LDL', 'MIN HDL', 'MAX HDL', 'MIN TRIGLYCERIDES', 'MAX TRIGLYCERIDES'),
            'Blood_Pressure' => array('MIN SYSTOLIC', 'MAX SYSTOLIC', 'MIN DIASTOLIC', 'MAX DIASTOLIC', 'MIN PULSE', 'MAX PULSE'),
            'Glucose' => array('MIN GLUCOSE', 'MAX GLUCOSE'),
            'SleepRecord' => array('TOTAL SLEEP', 'TOTAL AWAKE', 'TOTAL DEEP', 'TOTAL LIGHT', 'TOTAL RIM'),
            'Temperature' => array('MIN TEMPERATURE', 'MAX TEMPERATURE')
        );

        if (is_array($Dates['source'])) {
            foreach ($SetArray as $Srckey => $Srcindex) {
                if (!isset($Dates['source'][$Srckey])) {
                    unset($SetArray[$Srckey]);
                }
            }
        }

        $limit = array(0, 200);
        $cols = array('varName' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(var_name)'),
            'optionVal' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(opt_value)')
        );
        if ($page > 0) {
            $limit = array($limit[1] * $page, $limit[1]);
        }
        $page++;

        $data = $PassiveTableObj->getAdminViewData(array('study_id' => $study['study_id'], 'bucket' => -1), $cols, '0', array(), $limit, '1', $Dates, '0');
//        echo '<pre>';
//        echo $study['study_id'];
//        print_r($data);die;

        if (!empty($data)) {

            $this->objPHPExcel[$set]->setActiveSheetIndex(0);
            foreach ($data as $Data) {

                $i = 0;
                if ($j == 1) {
                    $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, 'Email');
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i + 1, $j, 'UserID');
                }

                $j = $j + 1;
                $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);

                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $Data['Email']);
                $i++;
                $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $Data['UserID']);

                $newArray = array_combine(explode(',', $Data['varName']), explode(',', $Data['optionVal']));
                if (!empty($newArray)) {

                    foreach ($newArray as $var => $opt) {
                        $i++;
                        if ($j == 2) {
                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $var);
                        }
                        $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                        $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $opt);
                    }
                }
                unset($Data['varName']);
                unset($Data['optionVal']);
                unset($Data['Email']);
                unset($Data['UserID']);
                foreach ($SetArray as $key => $index) {
                    if (!empty($Data[$key])) {
                        $dataFieldsVal = explode('-', $Data[$key]);
                        $pos = 0;
                        foreach ($index as $field) {
                            $i++;
                            if ($j == 2) {
                                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $field . ' (' . $Dates['source'][$key] . ')');
                            }
                            $val = !empty($dataFieldsVal[$pos]) ? $dataFieldsVal[$pos] : '';
                            $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $val);
                            $pos++;
                        }
                    } else {
                        foreach ($index as $field) {
                            $i++;
                            if ($j == 2) {
                                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $field . ' (' . $Dates['source'][$key] . ')');
                            }
                            $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, '');
                        }
                    }
                    unset($Data[$key]);
                }
                foreach ($Data as $key => $index) {
                    $i++;
                    if ($j == 2) {
                        $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $key);
                    }
                    $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $index);
                }

                $count++;
            }

            if (($count % 10000 == 0 && $count != 0)) {
                echo $count . '<br>';
                sleep(1);

                $objWriter[$set] = new \PHPExcel_Writer_Excel2007($this->objPHPExcel[$set]);
                $dir = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Email/";
                if (!file_exists($dir . $study['study_id'])) {
                    mkdir($dir . $study['study_id'], 777);
                    chmod($dir . $study['study_id'], 0777);
                }

                $this->objPHPExcel[$set]->getActiveSheet()->setTitle($study['study_title']);
                if (is_array($Dates['source'])) {
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-date-' . $Dates['cur_date'] . '-set-' . $set . '.xlsx');
                } else {
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-date-' . $Dates['cur_date'] . '-source-' . $Dates['source'] . '-set-' . $set . '.xlsx');
                }
                unset($objWriter[$set]);
                unset($this->objPHPExcel[$set]);
                $j = 1;
                $set++;
                $this->objPHPExcel[$set] = new \PHPExcel();
            }
            sleep(1);

            $this->_saveEmailExcel($study, $page, $j, $PassiveTableObj, $count, $set, $Dates);
        } else {

            $dir = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Email/";
            if (($count % 10000 != 0)) {
                echo $count . '<br>';
                sleep(1);

                $objWriter[$set] = new \PHPExcel_Writer_Excel2007($this->objPHPExcel[$set]);

                if (!file_exists($dir . $study['study_id'])) {
                    mkdir($dir . $study['study_id'], 777);
                    chmod($dir . $study['study_id'], 0777);
                }

                $this->objPHPExcel[$set]->getActiveSheet()->setTitle($study['study_title']);
                if (is_array($Dates['source'])) {
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-date-' . $Dates['cur_date'] . '-set-' . $set . '.xlsx');
                } else {
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-date-' . $Dates['cur_date'] . '-source-' . $Dates['source'] . '-set-' . $set . '.xlsx');
                }

                unset($objWriter[$set]);
                unset($this->objPHPExcel[$set]);
            }
        }
    }

    // Trial 



    public function studyTrialDataAction() {
        $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
        $PassiveTableObj = $this->getServiceLocator()->get("PassiveTable");
        $curDate = date('Y-m-d');
        ini_set('max_execution_time', 7200);
        $StudyData = $this->_studiesObj->getAdminStudy(array('study_trials' => 1, 'trialxls_status' => 0, new \Zend\Db\Sql\Predicate\Expression("end_date < '$curDate'")), array('study_id', 'study_title', 'study_description', 'researcher_name', 'start_date', 'end_date', 'study_type', 'source', 'passive_last_date', 'study_trials'), '0', array(), array(0, 1));



        if (!empty($StudyData)) {
            foreach ($StudyData as $Study) {

                $dirActive = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Trial/";
                $filename = $dirActive . $Studie['study_id'] . '/' . $Studie['study_title'] . '-Code-Book.xlsx';

                if (!file_exists($filename) || !file_exists($filename)) {
                    $this->_getCodeBook($Study);
                }

                $JsonDataArray = json_decode($Study['source'], true);
                $increment = $JsonDataArray['TimeInterval'];
                $sourceArray = $JsonDataArray['SOURCE'];

                $day = 60 * 60 * 24;
                $end = strtotime($Study['end_date']);
                $start = !empty($Study['passive_last_date']) ? strtotime($Study['passive_last_date']) : strtotime($Study['start_date']); //Check last updated date and calculate start date
                $diff = round(($end - $start) / $day);
                $i = 0;
                echo date('H:i:s') . " Start.\r\n";
                while ($i <= $diff && $i <= $increment * 2) {
                    if (isset($sourceArray['Global'])) {
                        foreach ($sourceArray['Global'] as $src) {
//                            echo '<pre>';
//                            print_r($sourceArray['Global']);
//                            die;
                            $j = 1;
                            $page = 0;
                            $count = 0;
                            $set = 1;
                            $lastDay = $i - 1;
                            $nextDay = $i + $increment;
                            $nextDay = ($nextDay > $diff) ? $diff : $nextDay;

                            $Dates['start_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $lastDay . 'days')));
                            $Dates['cur_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $i . 'days')));
                            $Dates['end_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $nextDay . 'days')));
                            $Dates['source'] = $src;
                            $this->_saveTrialExcel($Study, $page, $j, $PassiveTableObj, $count, $set, $Dates);
                        }
                    } else if (isset($sourceArray['Element'])) {

                        $j = 1;
                        $page = 0;
                        $count = 0;
                        $set = 1;
                        $lastDay = $i - 1;
                        $nextDay = $i + $increment;
                        $nextDay = ($nextDay > $diff) ? $diff : $nextDay;

                        $Dates['start_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $lastDay . 'days')));
                        $Dates['cur_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $i . 'days')));
                        $Dates['end_date'] = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $nextDay . 'days')));
                        $Dates['source'] = $sourceArray['Element'];

                        $this->_saveTrialExcel($Study, $page, $j, $PassiveTableObj, $count, $set, $Dates);
                        // CODE FOR ELEMENT BY SOURCE ALLOCATION
                    } else {
                        die('done');
                    }
                    $i = $i + $increment;
                }
                $i = $i - 1;
                $finalDate = date('Y-m-d', strtotime((date('Y-m-d', $start) . ' +' . $i . 'days')));
                if (strtotime($Study['end_date']) <= strtotime($finalDate)) {
//                    Update Date And status and create ZIP
                    $filter = new \Zend\Filter\Compress(array(
                        'adapter' => 'Zip',
                        'options' => array(
                            'archive' => $_SERVER['DOCUMENT_ROOT'] . '/StudyDump/Trial/' . $Study['study_id'] . "/" . $Study['study_title'] . '.zip'
                    )));
                    $compress = $filter->filter($_SERVER['DOCUMENT_ROOT'] . '/StudyDump/Trial/' . $Study['study_id']);
                    $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
                    $this->_studiesObj->updateAdminStudy(array('study_id' => $Study['study_id']), array('trialxls_status' => 1));
                } else {
                    //Update Date
                    $this->_studiesObj->updateAdminStudy(array('study_id' => $Study['study_id']), array('trialxls_status' => 0, 'passive_last_date' => $finalDate));
                }

                echo date('H:i:s') . " End\n";
            }
        }
        die('done');
//        echo '<pre>';
//        print_r($StudyData);
//        die;
    }

    function _saveTrialExcel($study = NULL, $page = NULL, $j, $PassiveTableObj, $count, $set, $Dates) {
        if ($count == 0) {
            $this->objPHPExcel[$set] = new \PHPExcel();
        }

        $SetArray = array(
            'Height' => array('MIN HEIGHT IN CENTIMETER', 'MAX HEIGHT IN CENTIMETER'),
            'Weight' => array('MIN WEIGHT IN POUNDS', 'MAX WEIGHT IN POUNDS'),
            'PhysicalData' => array('TOTAL STEPS', 'TOTAL DURATION', 'TOTAL DISTANCE', 'TOTAL CALORIES BURNED'),
            'Cholesterol' => array('MIN LDL', 'MAX LDL', 'MIN HDL', 'MAX HDL', 'MIN TRIGLYCERIDES', 'MAX TRIGLYCERIDES'),
            'Blood_Pressure' => array('MIN SYSTOLIC', 'MAX SYSTOLIC', 'MIN DIASTOLIC', 'MAX DIASTOLIC', 'MIN PULSE', 'MAX PULSE'),
            'Glucose' => array('MIN GLUCOSE', 'MAX GLUCOSE'),
            'SleepRecord' => array('TOTAL SLEEP', 'TOTAL AWAKE', 'TOTAL DEEP', 'TOTAL LIGHT', 'TOTAL RIM'),
            'Temperature' => array('MIN TEMPERATURE', 'MAX TEMPERATURE')
        );

        if (is_array($Dates['source'])) {
            foreach ($SetArray as $Srckey => $Srcindex) {
                if (!isset($Dates['source'][$Srckey])) {
                    unset($SetArray[$Srckey]);
                }
            }
        }

        $limit = array(0, 200);
        $cols = array('varName' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(var_name)'),
            'optionVal' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(opt_value)')
        );
        if ($page > 0) {
            $limit = array($limit[1] * $page, $limit[1]);
        }
        $page++;

        $data = $PassiveTableObj->getAdminViewData(array('study_id' => $study['study_id'], 'bucket' => -2), $cols, '0', array(), $limit, '1', $Dates, '0');


        if (!empty($data)) {

            $this->objPHPExcel[$set]->setActiveSheetIndex(0);
            foreach ($data as $Data) {

                $i = 0;
                if ($j == 1) {
                    $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, 'Email');
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i + 1, $j, 'UserID');
                }

                $j = $j + 1;
                $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);

                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $Data['Email']);
                $i++;
                $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $Data['UserID']);

                $newArray = array_combine(explode(',', $Data['varName']), explode(',', $Data['optionVal']));
                if (!empty($newArray)) {

                    foreach ($newArray as $var => $opt) {
                        $i++;
                        if ($j == 2) {
                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $var);
                        }
                        $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                        $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $opt);
                    }
                }
                unset($Data['varName']);
                unset($Data['optionVal']);
                unset($Data['Email']);
                unset($Data['UserID']);
                foreach ($SetArray as $key => $index) {
                    if (!empty($Data[$key])) {
                        $dataFieldsVal = explode('-', $Data[$key]);
                        $pos = 0;
                        foreach ($index as $field) {
                            $i++;
                            if ($j == 2) {
                                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $field . ' (' . $Dates['source'][$key] . ')');
                            }
                            $val = !empty($dataFieldsVal[$pos]) ? $dataFieldsVal[$pos] : '';
                            $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $val);
                            $pos++;
                        }
                    } else {
                        foreach ($index as $field) {
                            $i++;
                            if ($j == 2) {
                                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $field . ' (' . $Dates['source'][$key] . ')');
                            }
                            $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, '');
                        }
                    }
                    unset($Data[$key]);
                }
                foreach ($Data as $key => $index) {
                    $i++;
                    if ($j == 2) {
                        $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $key);
                    }
                    $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $index);
                }

                $count++;
            }

            if (($count % 10000 == 0 && $count != 0)) {
                echo $count . '<br>';
                sleep(1);

                $objWriter[$set] = new \PHPExcel_Writer_Excel2007($this->objPHPExcel[$set]);
                $dir = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Trial/";
                if (!file_exists($dir . $study['study_id'])) {
                    mkdir($dir . $study['study_id'], 777);
                    chmod($dir . $study['study_id'], 0777);
                }

                $this->objPHPExcel[$set]->getActiveSheet()->setTitle($study['study_title']);
                if (is_array($Dates['source'])) {
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-date-' . $Dates['cur_date'] . '-set-' . $set . '.xlsx');
                } else {
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-date-' . $Dates['cur_date'] . '-source-' . $Dates['source'] . '-set-' . $set . '.xlsx');
                }
                unset($objWriter[$set]);
                unset($this->objPHPExcel[$set]);
                $j = 1;
                $set++;
                $this->objPHPExcel[$set] = new \PHPExcel();
            }
            sleep(1);

            $this->_saveTrialExcel($study, $page, $j, $PassiveTableObj, $count, $set, $Dates);
        } else {

            $dir = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Trial/";
            if (($count % 10000 != 0)) {
                echo $count . '<br>';
                sleep(1);

                $objWriter[$set] = new \PHPExcel_Writer_Excel2007($this->objPHPExcel[$set]);

                if (!file_exists($dir . $study['study_id'])) {
                    mkdir($dir . $study['study_id'], 777);
                    chmod($dir . $study['study_id'], 0777);
                }

                $this->objPHPExcel[$set]->getActiveSheet()->setTitle($study['study_title']);
                if (is_array($Dates['source'])) {
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-date-' . $Dates['cur_date'] . '-set-' . $set . '.xlsx');
                } else {
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . '-date-' . $Dates['cur_date'] . '-source-' . $Dates['source'] . '-set-' . $set . '.xlsx');
                }

                unset($objWriter[$set]);
                unset($this->objPHPExcel[$set]);
            }
        }
    }

}
