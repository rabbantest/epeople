<?php
namespace Admin\Form;
use Zend\Form\Element;
use Zend\Form\Form;
//use CKEditorModule\Form\Element;
class BlogForm extends Form
{
    //public function init(){
    public function __construct($name=null) {
        parent::__construct('blog');
        $this->setAttribute("method", "post");
        $this->setAttribute("action", "/admin-add-blogs");
        
        $this->add(array(
            'name' => 'title',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'title',
                'placeholder' => 'Tiltle *',
                 'required' => 'required', 
            ),
            'options' => array(),
        ));
        
         $this->add(array(
            'name' => 'blog_id',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'blog_id'
            ),
            'options' => array(),
        ));
         
         
        
        $this->add(array(
            'name' => 'category',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'category',
                'placeholder' => 'Category *',
                 'required' => 'required', 
                 'autocomplete' => 'off', 
                'onKeyUp' => 'suggestNameOfCat(this);', 
            ),
            'options' => array(),
        ));
        
         $this->add(array(
            'name' => 'category_id',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'category_id'
            ),
            'options' => array(),
        ));
        
        
        $this->add(array(
            'name' => 'author',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'author',
                'placeholder' => 'Author *',
                 'required' => 'required', 
                'autocomplete' => 'off', 
                'onKeyUp' => 'suggestNameOfAuthor(this);', 
            ),
            'options' => array(),
        ));
        
           $this->add(array(
            'name' => 'author_id',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'author_id'
            ),
            'options' => array(),
        ));
        $this->add(array(
            'name'=> 'short_description',
            'type' => 'Zend\Form\Element\Textarea',
           'attributes' => array(
                'id' => 'short_description',
                'placeholder' => 'Short Description *',
                'required' => 'required', 
            ),
            'options' => array(),
        ));
       
   $this->add(array(
            'name'=> 'long_description',
            'type' => 'Zend\Form\Element\Textarea',
           'attributes' => array(
                'id' => 'long_description',
               'class' => 'classy-editor',
                'placeholder' => 'Long Description  *',
                'required' => 'required', 
            ),
            'options' => array(),
        ));
     
//        $this->add(array(
//  'type' => 'CKEditorModule\Form\Element\CKEditor',
//  'name' => 'long_description',
//  'options' => array(
//    'label' => 'Editor content',
//    'ckeditor' => array(
//        // add anny config you would normaly add via CKEDITOR.editorConfig
//        'language' => 'nl',
//        'uiColor' => '#AADC6E',
//    )
//  ),
//));
        
       $this->add(array( 
            'name' => 'blog_image', 
            'type' => 'file', 
            'attributes' => array( 
                'id' => 'blog_image',            
            ), 
            'options' => array(), 
        )); 
 
        
        $this->add(array( 
            'name' => 'status', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                 'id' => 'status',
                'required' => 'required', 
                'class' => 'select'
            ), 
            'options' => array( 
               // 'label' => 'Status *', 
                'value_options' => array(
                    '1' => 'Active', 
                    '0' => 'Inactive', 
                ),
            ), 
        )); 
        
//        $this->add(array(
//            'name'=> 'published_status',
//            'type' => 'Zend\Form\Element\Select',
//            'attributes' =>  array(
//                'id' => 'published_status',
//                 'required' => 'required', 
//                  'class' => 'select'
//            ),
//            'options' => array(
//             //   'label' => 'Publish Status *', 
//                'value_options' => array(
//                    '1' => 'Publish', 
//                    '0' => 'Draft', 
//                ),
//            ),
//        ));
        
        $this->add(array(
            'name'=> 'submit',
            'type' => 'Zend\Form\Element\Submit',
           'attributes' => array(
               'type' => 'submit',
                'class'=>'submit_button',
                'value' => 'Submit',
                //'onClick' => 'gppOpen("gpp_terms_condition");',
                'id' => 'submit'              
            ),
           
        ));

         $this->add(array(
            'name'=> 'cancle',
            'type' => 'Zend\Form\Element\Button',
           'attributes' => array(
               'type' => 'button',
                'class'=>'submit_button',
                'value' => 'Cancle',
                //'onClick' => 'gppOpen("gpp_terms_condition");',
                'id' => 'Cancle'              
            ),
           
        ));
        
        
    }
}