<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Condition extends Form {

    public function __construct($name = null) {
        parent::__construct('condition');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', 'javascript:void(0)');
        $this->setAttribute('id', 'AddConditionForm');

        $this->add(array(
            'name' => 'condition_id',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'ConditionId',
                'class' => 'selectM_condition required',
                'multiple' => 'multiple'
            ),
            'options' => array(
            )
        ));

        $this->add(array('name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class' => 'submit_button bb_btn',
                'value' => 'Save',
                'id' => 'submit'
            ),
        ));
    }

}
