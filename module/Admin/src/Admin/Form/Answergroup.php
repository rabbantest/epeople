<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Answergroup extends Form {

    public function __construct($name = null) {
        parent::__construct('answergroup');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', 'admin-answergroups');
        $this->setAttribute('id', 'AddAnswergroupForm');

        $this->add(array(
            'name' => 'name',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'AnswergroupName',
                'placeholder' => 'Answer group name',
            ),
            'options' => array(
            ),
        ));
        
          $this->add(array(
            'name' => 'q_instruction',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'q_instruction',
                'placeholder' => 'Questionnaire instructions',
            ),
            'options' => array(
            ),
        ));

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'AnswergroupId'
            )
        ));


        $this->add(array(
            'name' => 'status',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'Status',
                'class' => 'select required',
                'title' => 'Select Status'
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Draft',
                    '1' => 'Publish'
                )
            )
        ));

        $this->add(array('name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class' => 'submit_button',
                'value' => 'Submit',
                'id' => 'c_submit'
            ),
        ));

        // $this->add(
        //     array( 
        //         'name' => 'csrf', 
        //         'type' => 'Zend\Form\Element\Csrf', 
        //     )
        // ); 
    }

}
