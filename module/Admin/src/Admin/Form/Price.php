<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Price extends Form {

    public function __construct($name = null) {
        parent::__construct('price');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', 'admin-matrix-caregiver');
        $this->setAttribute('id', 'AddPriceForm');


        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'PriceId'
            )
        ));

        $this->add(array(
            'name' => 'type',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'Type'
            )
        ));

        $this->add(array(
            'name' => 'user',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'User',
                'placeholder' => 'Number of Users',
            ),
            'options' => array(
            ),
        ));
        
        $this->add(array(
            'name' => 'variable',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'Variable',
                'placeholder' => 'Number of Variables',
            ),
            'options' => array(
            ),
        ));

        $this->add(array(
            'name' => 'price',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'Price',
                'placeholder' => 'Cost of Subscription',
            ),
            'options' => array(
            ),
        ));

        $this->add(array(
            'name' => 'status',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'status'
            ),
            'options' => array(),
        ));

        $this->add(array('name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class' => 'submit_button',
                'value' => 'Save',
                'id' => 'submit'
            ),
        ));

    }

}
