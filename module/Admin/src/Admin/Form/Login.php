<?php

namespace Admin\Form;

use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Form;

class Login extends Form {

    public function __construct($name = null) {
        parent::__construct('login');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/');

        $this->add(array(
            'name' => 'email', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'id' => 'email', 
                'placeholder' => 'User Name', 
            ), 
            'options' => array( 
            ), 
        )); 

        $this->add(array( 
            'name' => 'password', 
            'type' => 'Zend\Form\Element\Password', 
            'attributes' => array( 
                'id' => 'password', 
                'placeholder' => 'Password', 
            ), 
            'options' => array( 
            ), 
        )); 

        $this->add(array('name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class'=>'submit_button',
                'value' => 'Login',
                'onClick' => 'gppOpen("gpp_terms_condition");',
                'id' => 'submit'
            ),
        ));

        // $this->add(
        //     array( 
        //         'name' => 'csrf', 
        //         'type' => 'Zend\Form\Element\Csrf', 
        //     )
        // ); 
    }
}