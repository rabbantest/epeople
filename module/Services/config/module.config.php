<?php

/*************************
    * PAGE: USE TO MANAGE THE MODULE CONFIG FOR APPLICATION.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Rachit Jain
    * CREATOR: 02/01/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/

return array(
	'errors' => array(
		'post_processor' => 'json-pp',
		'show_exceptions' => array(
			'message' => true,
			'trace'   => true
		)
	),
	'di' => array(
		'instance' => array(
			'alias' => array(
				'json-pp'  => 'Services\PostProcessor\Json',
				'image-pp' => 'Services\PostProcessor\Image',
				'xml-pp'   => 'Services\PostProcessor\Xml',
				'phps-pp'  => 'Services\PostProcessor\Phps',
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'index' => 'Services\Controller\IndexController',
			'user' => 'Services\Controller\UserController',
			'body' => 'Services\Controller\BodyController',
			'dashboard'=>'Services\Controller\DashboardController',
			'device'=>'Services\Controller\DeviceController',
			'mood'=>'Services\Controller\MoodController',
			'people'=>'Services\Controller\PeopleController',
			'document'=>'Services\Controller\DocumentController',
			'econtact'=>'Services\Controller\EmergencycontactController',
			'insurance'=>'Services\Controller\InsuranceController',
			'appointment'=>'Services\Controller\AppointmentController',
			'imunization'=>'Services\Controller\ImunizationController',
			'checkup'=>'Services\Controller\CheckupController',
			'blog'=>'Services\Controller\BlogController',
			'location'=>'Services\Controller\LocationController',
			'history'=>'Services\Controller\HistoryController',
			'symptom'=>'Services\Controller\SymptomController',
			'treatment'=>'Services\Controller\TreatmentController',
			'fitness'=>'Services\Controller\FitnessController',
			'assessment'=>'Services\Controller\AssessmentController',
			'profile'=>'Services\Controller\ProfileController',
			'notifications'=>'Services\Controller\NotificationController',
			'study'=>'Services\Controller\StudyController',
			'studytrial'=>'Services\Controller\StudytrialController',
			'reward'=>'Services\Controller\RewardController',
		)
	),
	'router' => array(
		'routes' => array(
			'restful' => array(
				'type'    => 'Zend\Mvc\Router\Http\Segment',
				'options' => array(
					'route'       => '/:controller[.:formatter][/:id]',
					'constraints' => array(
						'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'formatter'  => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'         => '[a-zA-Z0-9_-]*'
					),
				),
			),
		),
	),
	'controller_plugins' => array(
		'invokables' => array(
			  'CustomControllerPlugin1' => 'Services\Controller\Plugin\CustomControllerPlugin1'
		)
	),
);
