<?php
/*************************
    * PAGE: USE TO MANAGE THE MODULE CONFIG FOR APPLICATION.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Rachit Jain
    * CREATOR: 04/01/2015.
	* UPDATED: 09/01/2015.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/

namespace Services;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;

/*
 * import all  model  
 */
use Services\Model\ServicesSecurityTable;
use Services\Model\ServicesUsersTable;
use Services\Model\ServicesContactInfoTable;
use Services\Model\ServicesProfileInfoTable;
use Services\Model\ServicesCountryTable;
use Services\Model\ServicesCountyTable;
use Services\Model\ServicesStateTable;
use Services\Model\ServicesHeightRecordTable;
use Services\Model\ServicesWeightRecordTable;
use Services\Model\ServicesWaistRecordTable;
use Services\Model\ServicesPhysicalActivityTable;
use Services\Model\ServicesBmiRecordTable;
use Services\Model\ServicesCholesterolRecordTable;
use Services\Model\ServicesBloodPressureRecordTable;
use Services\Model\ServicesBloodGlucoseRecordTable;
use Services\Model\ServicesTemperatureRecordTable;
use Services\Model\ServicesSleepRecordTable;
use Services\Model\ServicesWeightTargetTable;
use Services\Model\ServicesFoodDrinkTable;
use Services\Model\ServicesConditionTable;
use Services\Model\ServicesSymtomsTable;
use Services\Model\ServicesTreatmentTable;
use Services\Model\ServicesUsersConditionTable;
use Services\Model\ServicesUsersSymtomsTable;
use Services\Model\ServicesUsersTreatmentsTable;
use Services\Model\ServicesUsersOtherSymtomsTable;
use Services\Model\ServicesUsersOtherTreatmentsTable;
use Services\Model\ServicesOtherTreatmentsTable;
use Services\Model\ServicesOtherSymtomsTable;
use Services\Model\ServicesMoodRecordTable;
use Services\Model\ServicesUsersImunizationTable;
use Services\Model\ServicesUserCheckupTable;
use Services\Model\ServicesBiometricsRecordTable;
use Services\Model\ServicesFollowerUsersTable;
use Services\Model\ServicesActivityStageTable;
use Services\Model\ServicesPrivacySettingsTable;
use Services\Model\ServicesDocumentTable;
use Services\Model\ServicesInsuranceTable;
use Services\Model\ServicesEmergencycontactTable;
use Services\Model\ServicesAppointmentTable;
use Services\Model\ServicesCaregiverTable;
use Services\Model\ServicesLocationTable;
use Services\Model\ServicesBlogTable;
use Services\Model\ServicesBeconTable;
use Services\Model\ServicesAssessmentTable;
use Services\Model\ServicesAssesFormulaTable;
use Services\Model\ServicesAssociatedBeaconTable;
use Services\Model\ServicesBeaconRulesTable;
use Services\Model\ServicesBeaconDataTable;
use Services\Model\ServicesGeofenceDataTable;
use Services\Model\ServicesStudyQuestion;
use Services\Model\ServicesStudyTable;
use Services\Model\ServicesNotificationTable;
use Services\Model\ServicesRewardTable;
use Services\Model\ServicePurposeTypeTable;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        date_default_timezone_set('UTC');
		/*$eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, function($e) {
             $result = $e->getResult();
             $result->setTerminal(TRUE);
          });*/
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    /**
     * This method returns an array of factories that are all merged together by the ModuleManager before passing to the ServiceManager
     * We also tell the ServiceManager that an UsersTableGateway is created by getting a Zend\Db\Adapter\Adapter (also from the ServiceManager) and using it to create a TableGateway object.
     */
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'ServicesSecurityTable' => function ($serviceManager) {
                    return new ServicesSecurityTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesUsersTable' => function ($serviceManager) {
                    return new ServicesUsersTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesHeightRecordTable' => function ($serviceManager) {
                    return new ServicesHeightRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesWeightRecordTable' => function ($serviceManager) {
                    return new ServicesWeightRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesWaistRecordTable' => function ($serviceManager) {
                    return new ServicesWaistRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesPhysicalActivityTable' => function ($serviceManager) {
                    return new ServicesPhysicalActivityTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesContactInfoTable' => function ($serviceManager) {
                    return new ServicesContactInfoTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesProfileInfoTable' => function ($serviceManager) {
                    return new ServicesProfileInfoTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesCountryTable' => function ($serviceManager) {
                    return new ServicesCountryTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesStateTable' => function ($serviceManager) {
                    return new ServicesStateTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesCountyTable' => function ($serviceManager) {
                    return new ServicesCountyTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesBmiRecordTable' => function ($serviceManager) {
                    return new ServicesBmiRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesCholesterolRecordTable' => function ($serviceManager) {
                    return new ServicesCholesterolRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesBloodPressureRecordTable' => function ($serviceManager) {
                    return new ServicesBloodPressureRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesBloodGlucoseRecordTable' => function ($serviceManager) {
                    return new ServicesBloodGlucoseRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesTemperatureRecordTable' => function ($serviceManager) {
                    return new ServicesTemperatureRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesSleepRecordTable' => function ($serviceManager) {
                    return new ServicesSleepRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
				'ServicesWeightTargetTable' => function ($serviceManager) {
                    return new ServicesWeightTargetTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
				'ServicesFoodDrinkTable' => function ($serviceManager) {
                    return new ServicesFoodDrinkTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesConditionTable' => function ($serviceManager) {
                    return new ServicesConditionTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
				'ServicesSymtomsTable' => function ($serviceManager) {
                    return new ServicesSymtomsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
				'ServicesTreatmentTable' => function ($serviceManager) {
                    return new ServicesTreatmentTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
				'ServicesUsersConditionTable' => function ($serviceManager) {
                    return new ServicesUsersConditionTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
				'ServicesUsersSymtomsTable' => function ($serviceManager) {
                    return new ServicesUsersSymtomsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
				'ServicesUsersTreatmentsTable' => function ($serviceManager) {
                    return new ServicesUsersTreatmentsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesUsersOtherSymtomsTable' => function ($serviceManager) {
                    return new ServicesUsersOtherSymtomsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
				'ServicesUsersOtherTreatmentsTable' => function ($serviceManager) {
                    return new ServicesUsersOtherTreatmentsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
				'ServicesOtherTreatmentsTable' => function ($serviceManager) {
                    return new ServicesOtherTreatmentsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
				'ServicesOtherSymtomsTable' => function ($serviceManager) {
                    return new ServicesOtherSymtomsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesMoodRecordTable' => function ($serviceManager) {
                    return new ServicesMoodRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
				'ServicesUsersImunizationTable' => function ($serviceManager) {
                    return new ServicesUsersImunizationTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesUserCheckupTable' => function ($serviceManager) {
                    return new ServicesUserCheckupTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesBiometricsRecordTable' => function ($serviceManager) {
                    return new ServicesBiometricsRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesFollowerUsersTable' => function ($serviceManager) {
                    return new ServicesFollowerUsersTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesActivityStageTable' => function ($serviceManager) {
                    return new ServicesActivityStageTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesPrivacySettingsTable' => function ($serviceManager) {
                    return new ServicesPrivacySettingsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesDocumentTable' => function ($serviceManager) {
                    return new ServicesDocumentTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesInsuranceTable' => function ($serviceManager) {
                    return new ServicesInsuranceTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesEmergencycontactTable' => function ($serviceManager) {
                    return new ServicesEmergencycontactTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesAppointmentTable' => function ($serviceManager) {
                    return new ServicesAppointmentTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesCaregiverTable' => function ($serviceManager) {
                    return new ServicesCaregiverTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesLocationTable' => function ($serviceManager) {
                    return new ServicesLocationTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesBeconTable' => function ($serviceManager) {
                    return new ServicesBeconTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesBlogTable' => function ($serviceManager) {
                    return new ServicesBlogTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesAssessmentTable' => function ($serviceManager) {
                    return new ServicesAssessmentTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                 'ServicesAssesFormulaTable' => function ($serviceManager) {
                    return new ServicesAssesFormulaTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                 'ServicesAssociatedBeaconTable' => function ($serviceManager) {
                    return new ServicesAssociatedBeaconTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                 'ServicesBeaconRulesTable' => function ($serviceManager) {
                    return new ServicesBeaconRulesTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesBeaconDataTable' => function ($serviceManager) {
                    return new ServicesBeaconDataTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesGeofenceDataTable' => function ($serviceManager) {
                    return new ServicesGeofenceDataTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesStudyQuestion' => function ($serviceManager) {
                    return new ServicesStudyQuestion($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesStudyTable' => function ($serviceManager) {
                    return new ServicesStudyTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesNotificationTable' => function ($serviceManager) {
                    return new ServicesNotificationTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesRewardTable' => function ($serviceManager) {
                    return new ServicesRewardTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicesRewardTable' => function ($serviceManager) {
                    return new ServicesRewardTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ServicePurposeTypeTable' => function ($serviceManager) {
                    return new ServicePurposeTypeTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
            ),
        );
    }
    
    /**
     * @param MvcEvent $e
     * @return null|\Zend\Http\PhpEnvironment\Response
     */
    public function postProcess(MvcEvent $e) {
        $routeMatch = $e->getRouteMatch();
        $formatter = $routeMatch->getParam('formatter', false);

        /** @var \Zend\Di\Di $di */
        $di = $e->getTarget()->getServiceLocator()->get('di');

        if ($formatter !== false) {
            if ($e->getResult() instanceof \Zend\View\Model\ViewModel) {
                if (is_array($e->getResult()->getVariables())) {
                    $vars = $e->getResult()->getVariables();
                } else {
                    $vars = null;
                }
            } else {
                $vars = $e->getResult();
            }

            /** @var PostProcessor\AbstractPostProcessor $postProcessor */
            $postProcessor = $di->get($formatter . '-pp', array(
                'response' => $e->getResponse(),
                'vars' => $vars,
            ));

            $postProcessor->process();

            return $postProcessor->getResponse();
        }

        return null;
    }
}
