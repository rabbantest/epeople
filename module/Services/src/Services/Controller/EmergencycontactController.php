<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE DOCUMENT OF Emergency Contact  FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain.
 * CREATOR: 05/02/2015.
 * UPDATED: 05/02/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\EventManager\EventManagerInterface;
use Zend\View\Model\JsonModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Mail;
use \Exception;

class EmergencycontactController extends AbstractRestfulController {

	public function __construct(){

    }

    public function getList(){

    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id) {

	}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);
		//echo '<pre>';print_r($requestArr);exit;

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'getecontacts':
				$responseArr = $this->getecontacts($service_key);
				break;
			case 'addecontact':
				$responseArr = $this->addecontact($service_key);
				break;
			case 'editecontact':
				$responseArr = $this->editecontact($service_key);
				break;
			case 'deleteecontact':
				$responseArr = $this->deleteecontact($service_key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}

	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {

	}

	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}


	/*****
     * Action: Get E-Contacts
     * @author: Rachit Jain
     * Created Date: 09-02-2015.
     *****/
    public function getecontacts($service_key) {
		$sucess = TRUE;
		$IndEMCTableObj = $this->getServiceLocator()->get("ServicesEmergencycontactTable");

		try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($userId)){
				throw new Exception("Please enter user id.");
			}

			$contactType = $IndEMCTableObj->getContactType();
			$contactData = array();
			$whereArray = array('UserID' =>$userId, 'status'=>0);
			
			$totalCount = count($IndEMCTableObj->getEMCRecord($whereArray, array(), '', array(), "", ""));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$contactData = $IndEMCTableObj->getEMCRecord($whereArray, array(), '', array(), $lowerLimit, $maxLimit);

			$i = 0;
			foreach ($contactData as $cvalue) {
				$content_type_id = $cvalue['contact_type'];
				$whereCTArray = array('appointment_type_id' => $content_type_id);
				$contentName = $IndEMCTableObj->getContactType($whereCTArray);
				$contactData[$i]['contact_type_name'] = $contentName[0]['appointment_type'];
				$i++;
			}
		}
        catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"contactData"=>$contactData,"contactType"=>$contactType,"total"=>$totalpage);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


    /*****
     * Action: Add E-Contact
     * @author: Rachit Jain
     * Created Date: 09-02-2015.
     *****/
    public function addecontact($service_key) {
		$sucess = TRUE;
		$IndEMCTableObj = $this->getServiceLocator()->get("ServicesEmergencycontactTable");

		try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : '';
			$contact_name = isset($data['contact_name']) ? $data['contact_name'] : '';
			$contact = isset($data['contact']) ? $data['contact'] : '';
			$contact_type = isset($data['contact_type']) ? $data['contact_type'] : '';
			$notes = isset($data['notes']) ? $data['notes'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			$dataArray = array();

			if(empty($userId)){
				throw new Exception("Please enter user id.");
			}
			if(empty($contact_name)){
				throw new Exception("Please enter contact name.");
			}
			if(empty($contact)){
				throw new Exception("Please enter contact.");
			}
			if(empty($contact_type)){
				throw new Exception("Please enter contact type.");
			}

			$dataArray = array(
						"UserID"=> $userId,
						"name" => $contact_name,
						"contact" => $contact,
						"contact_type" => $contact_type,
						"notes" => $notes,
						"creation_date" => round(microtime(true) * 1000)
						);

			$insdata =  $IndEMCTableObj->insertEMC($dataArray);
			if(!$insdata){
				throw new Exception("Some error occured please try again later.");
			}
		}
        catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Contact added successfully.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}

    }


    /*****
     * Action: Edit E-Contact
     * @author: Rachit Jain
     * Created Date: 09-02-2015.
     *****/
    public function editecontact($service_key) {
		$sucess = TRUE;
		$IndEMCTableObj = $this->getServiceLocator()->get("ServicesEmergencycontactTable");

		try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : '';
			$contactID = isset($data['contactid']) ? $data['contactid'] : '';
			$contact_name = isset($data['contact_name']) ? $data['contact_name'] : '';
			$contact = isset($data['contact']) ? $data['contact'] : '';
			$contact_type = isset($data['contact_type']) ? $data['contact_type'] : '';
			$notes = isset($data['notes']) ? $data['notes'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			$dataArray = array();

			if(empty($userId)){
				throw new Exception("Please enter user id.");
			}
			if(empty($contactID)){
				throw new Exception("Please enter contact id.");
			}
			if(empty($contact_name)){
				throw new Exception("Please enter contact name.");
			}
			if(empty($contact)){
				throw new Exception("Please enter contact.");
			}
			if(empty($contact_type)){
				throw new Exception("Please enter contact type.");
			}

			$whereAr = array("contacts_id" => $contactID, "UserID"=> $userId );
			$dataArray = array(
						"UserID"=> $userId,
						"name" => $contact_name,
						"contact" => $contact,
						"contact_type" => $contact_type,
						"notes" => $notes,
						"updation_date" => round(microtime(true) * 1000)
						);

			$updatedata =  $IndEMCTableObj->updateEMC($whereAr,$dataArray);
			if(!$updatedata){
				throw new Exception("Some error occured please try again later.");
			}
		}
        catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Contact updated successfully.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


    /*****
     * Action: Delete E-Contact
     * @author: Rachit Jain
     * Created Date: 09-02-2015.
     *****/
    public function deleteecontact($service_key) {
        $sucess = TRUE;
        $IndEMCTableObj = $this->getServiceLocator()->get("ServicesEmergencycontactTable");

        try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : '';
			$contactID = isset($data['contactid']) ? $data['contactid'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($userId)){
				throw new Exception("Please enter user id.");
			}
			if(empty($contactID)){
				throw new Exception("Please enter contact id.");
			}
			$whereArr = array("UserID" => $userId,'contacts_id'=>$contactID);
			$columns = array('status' => 1);
			$res = $IndEMCTableObj->updateEMC($whereArr, $columns);
			if(!$res){
				throw new Exception("Some error occured.");
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Contact deleted successfully.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }
}
