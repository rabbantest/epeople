<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE DOCUMENT OF INDIVIDUAL  FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain.
 * CREATOR: 05/02/2015.
 * UPDATED: 05/02/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use \Exception;

class StudyController extends AbstractRestfulController {

    public function __construct() {
	
    }


	public function getList(){
		
    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id){

	}

	/**
	 * Create a new resource
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'getstudyscreening':
				$responseArr = $this->getstudycurrent($service_key);
				break;
			case 'getstudymain':
				$responseArr = $this->getstudyarchive($service_key);
				break;
			case 'getstudycurrent':
				$responseArr = $this->getstudycurrent($service_key);
				break;
			case 'getstudyarchive':
				$responseArr = $this->getstudyarchive($service_key);
				break;
			case 'participatestudy':
				$responseArr = $this->participatestudy($service_key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}

	/**
	 * Update an existing resource
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {
	
	}


	/**
	 * Delete an existing resource
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}

	/*
     * @Author:Rachit Jain 
     * @Action: This action used to get screening studies
     */
    public function getstudyscreening($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$IndStudObj = $this->getServiceLocator()->get("ServicesStudyTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$domainID = isset($data['domain_id']) ? $data['domain_id'] : '1'; //current domain id
			$condition = isset($data['condition']) ? $data['condition'] : '';
			$subcondition = isset($data['subcondition']) ? $data['subcondition'] : '';
			$location = isset($data['location']) ? $data['location'] : '';
			$start_date = isset($data['start_date']) ? $data['start_date'] : '';
			$end_date = isset($data['end_date']) ? $data['end_date'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID)){
				throw new Exception("Please enter user id.");
			}

			//Check Panel Member 
			$wherePanelArr = array("UserID"=>$UserID);
			$checkPanel = $IndUserTableObj->getUser($wherePanelArr);
			if($checkPanel[0]['PanelMember'] == 0){
				throw new Exception("You are not a panel member.");
			}

			//get study list   
			$searchcontent = array();
			$searchcontent = array('condation' => $condition, 'sub_condation' => $subcondition,'location' => $location, 'start_date' => $start_date, 'end_date' => $end_date);

			$lookingfor = "start_date DESC";
			$whereArr = array("user_id" => $UserID, "screening" => 1, "type" => 0, "created_by" => $domainID);
			$columnArr = array();
			
			$totalCount = count($IndStudObj->getStudy($whereArr, $columnArr, $lookingfor, $searchcontent,0,"",""));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$studyList = $IndStudObj->getStudy($whereArr, $columnArr, $lookingfor, $searchcontent, 0, $lowerLimit, $maxLimit);
			
			foreach($studyList as $key=>$val){
				/////////////////////  Get Status Text Start///////////////////////
				if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
					$labelText = 'Coming Soon..';
				}elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null) && $val['is_accepted']==1){
						$labelText = 'Finished';
					}
					else{
						$labelText = 'Expired';
					}
				}
				else{
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null)){
						$labelText = 'Completed';
					}
					else{
						$labelText = 'Pending';
					}
				}
				$studyList[$key]['labelText'] = $labelText;
				/////////////////////  Get Status Text End///////////////////////
				
				/////////////////////  Get Button Text Start///////////////////////
				if($val['is_accepted']==1){
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null)){
						$buttonText = 'Finished';
					}
					else{
						if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
							$buttonText = 'Coming soon..';
						}
						elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
							$buttonText = 'Expired';
						}
						else{
							$buttonText = 'Participate';
						}
					}
				}else{
					if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
						$buttonText = 'Coming soon..';
					}
					elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
						$buttonText = 'Expired';
					}
					else{
						$buttonText = 'Accept';
					}
				}
				$studyList[$key]['buttonText'] = $buttonText;
				/////////////////////  Get Button Text End///////////////////////
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}
		
		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"studyArr"=>$studyList,"total"=>$totalpage);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}

    }
    

    /*
     * @Author:Rachit Jain 
     * @Action: This action used to get main studies
     */
    public function getstudymain($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$IndStudObj = $this->getServiceLocator()->get("ServicesStudyTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$domainID = isset($data['domain_id']) ? $data['domain_id'] : '1'; //current domain id
			$condition = isset($data['condition']) ? $data['condition'] : '';
			$subcondition = isset($data['subcondition']) ? $data['subcondition'] : '';
			$location = isset($data['location']) ? $data['location'] : '';
			$start_date = isset($data['start_date']) ? $data['start_date'] : '';
			$end_date = isset($data['end_date']) ? $data['end_date'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID)){
				throw new Exception("Please enter user id.");
			}

			//Check Panel Member 
			$wherePanelArr = array("UserID"=>$UserID);
			$checkPanel = $IndUserTableObj->getUser($wherePanelArr);
			if($checkPanel[0]['PanelMember'] == 0){
				throw new Exception("You are not a panel member.");
			}

			$searchcontent = array();
			$searchcontent = array('condation' => $condition,'sub_condation' => $subcondition, 'location' => $location,'start_date' => $start_date, 'end_date' => $end_date);

			$lookingfor = "start_date DESC";
			$whereArr = array("user_id" => $UserID, "eligible_for_main" => 1, "created_by" => $domainID);
			$columnArr = array();

			$totalCount = count($IndStudObj->getStudy($whereArr, $columnArr, $lookingfor, $searchcontent,1,"",""));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$studyList = $IndStudObj->getStudy($whereArr, $columnArr, $lookingfor, $searchcontent,1, $lowerLimit, $maxLimit);
			
			foreach($studyList as $key=>$val){
				/////////////////////  Get Status Text Start///////////////////////
				if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
					$labelText = 'Coming Soon..';
				}elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null) && $val['is_accepted']==1){
						$labelText = 'Finished';
					}
					else{
						$labelText = 'Expired';
					}
				}
				else{
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null)){
						$labelText = 'Completed';
					}
					else{
						$labelText = 'Pending';
					}
				}
				$studyList[$key]['labelText'] = $labelText;
				/////////////////////  Get Status Text End///////////////////////
				
				/////////////////////  Get Button Text Start///////////////////////
				if($val['is_accepted']==1){
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null)){
						$buttonText = 'Finished';
					}
					else{
						if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
							$buttonText = 'Coming soon..';
						}
						elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
							$buttonText = 'Expired';
						}
						else{
							$buttonText = 'Participate';
						}
					}
				}else{
					if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
						$buttonText = 'Coming soon..';
					}
					elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
						$buttonText = 'Expired';
					}
					else{
						$buttonText = 'Accept';
					}
				}
				$studyList[$key]['buttonText'] = $buttonText;
				/////////////////////  Get Button Text End///////////////////////
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"studyArr"=>$studyList,"total"=>$totalpage);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


    /*
     * @Author:Rachit Jain 
     * @Action: This action used for first time partishipation
     */
    public function participatestudy($service_key) {
        $sucess = TRUE;
        $CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $IndStudObj = $this->getServiceLocator()->get("ServicesStudyTable");

        try{
			$data = $this->params;
			$userId = isset($data['user_id']) ? $data['user_id'] : '';
			$study_id = isset($data['study_id']) ? $data['study_id'] : '';
			$study_id = $CusConPlug->encryptdata($study_id);
			$study_type = isset($data['study_type']) ? $data['study_type'] : '';
			$study_type = $CusConPlug->encryptdata($study_type);
			$type = 'mobile';
			$type = $CusConPlug->encryptdata($type);
			$subdomain = isset($data['subdomain']) ? $data['subdomain'] : 'PeoplePoweredHealth';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($userId) || empty($study_id) || empty($study_type) || empty($subdomain)){
				throw new Exception("Required parameter missing.");
			}

			$serverScheme = $_SERVER['REQUEST_SCHEME'];
			$serverHost = $_SERVER['HTTP_HOST'];

			//Code to replace subdomain
			$serverArray = explode('.', $serverHost);
			if (strtolower($serverArray[0]) == 'www') {
				array_splice($serverArray,1,1,array($subdomain));
			} else {
				array_splice($serverArray,0,1,array($subdomain));
			}

			$HOST = implode('.',$serverArray);

			$Url = $serverScheme."s".'://'.$HOST."/individual/index/login?study_id=".$study_id."&study_type=".$study_type."&type=".$type;
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"url"=>$Url);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*
     * @Author:Rachit Jain 
     * @Action: This action used to get screening studies
     */
    public function getstudycurrent($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$IndStudObj = $this->getServiceLocator()->get("ServicesStudyTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$domainID = isset($data['domain_id']) ? $data['domain_id'] : '1'; //current domain id
			$condition = isset($data['condition']) ? $data['condition'] : '';
			$subcondition = isset($data['subcondition']) ? $data['subcondition'] : '';
			$location = isset($data['location']) ? $data['location'] : '';
			$start_date = isset($data['start_date']) ? $data['start_date'] : '';
			$end_date = isset($data['end_date']) ? $data['end_date'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID)){
				throw new Exception("Please enter user id.");
			}

			//Check Panel Member 
			$wherePanelArr = array("UserID"=>$UserID);
			$checkPanel = $IndUserTableObj->getUser($wherePanelArr);
			if($checkPanel[0]['PanelMember'] == 0){
				throw new Exception("You are not a panel member.");
			}

			//get study list   
			$searchcontent = array();
			if (!empty($condition) || !empty($subcondition) || !empty($location) || !empty($start_date) || !empty($end_date)) {
				$searchcontent = array('condation' => $condition, 'sub_condation' => $subcondition,'location' => $location, 'start_date' => $start_date, 'end_date' => $end_date);
			}

			$lookingfor = "start_date DESC";
			//$whereArr = array("user_id" => $UserID, "screening" => 1, "type" => 0, "created_by" => $domainID);
			$current_date = date('Y-m-d');
			$whereArr = array("user_id" => $UserID, "created_by" => $domainID, 'end_date' => $current_date);
			$columnArr = array();
			
			$totalCount = count($IndStudObj->getStudy1($whereArr, $columnArr, $lookingfor, $searchcontent,0,"",""));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$studyList = $IndStudObj->getStudy1($whereArr, $columnArr, $lookingfor, $searchcontent, 0, $lowerLimit, $maxLimit);
			
			$study=array();
			foreach($studyList as $stu){
				if($stu['part_status'] == null || $stu['part_status'] > 0){
					$study[]=$stu;
				}
			}
			
			foreach($study as $key=>$val){
				/////////////////////  Get Status Text Start///////////////////////
				if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
					$labelText = 'Coming Soon..';
				}elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null) && $val['is_accepted']==1){
						$labelText = 'Finished';
					}
					else{
						$labelText = 'Expired';
					}
				}
				else{
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null)){
						$labelText = 'Completed';
					}
					else{
						$labelText = 'Pending';
					}
				}
				$study[$key]['labelText'] = $labelText;
				/////////////////////  Get Status Text End///////////////////////
				
				/////////////////////  Get Button Text Start///////////////////////
				if($val['is_accepted']==1){
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null)){
						$buttonText = 'Finished';
					}
					else{
						if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
							$buttonText = 'Coming soon..';
						}
						elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
							$buttonText = 'Expired';
						}
						else{
							$buttonText = 'Participate';
						}
					}
				}else{
					if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
						$buttonText = 'Coming soon..';
					}
					elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
						$buttonText = 'Expired';
					}
					else{
						$buttonText = 'Accept';
					}
				}
				$study[$key]['buttonText'] = $buttonText;
				/////////////////////  Get Button Text End///////////////////////
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}
		
		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"studyArr"=>$study,"total"=>$totalpage);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}

    }
    

    /*
     * @Author:Rachit Jain 
     * @Action: This action used to get main studies
     */
    public function getstudyarchive($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$IndStudObj = $this->getServiceLocator()->get("ServicesStudyTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$domainID = isset($data['domain_id']) ? $data['domain_id'] : '1'; //current domain id
			$condition = isset($data['condition']) ? $data['condition'] : '';
			$subcondition = isset($data['subcondition']) ? $data['subcondition'] : '';
			$location = isset($data['location']) ? $data['location'] : '';
			$start_date = isset($data['start_date']) ? $data['start_date'] : '';
			$end_date = isset($data['end_date']) ? $data['end_date'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID)){
				throw new Exception("Please enter user id.");
			}

			//Check Panel Member 
			$wherePanelArr = array("UserID"=>$UserID);
			$checkPanel = $IndUserTableObj->getUser($wherePanelArr);
			if($checkPanel[0]['PanelMember'] == 0){
				throw new Exception("You are not a panel member.");
			}

			$searchcontent = array();
			if (!empty($condition) || !empty($subcondition) || !empty($location) || !empty($start_date) || !empty($end_date)) {
				$searchcontent = array('condation' => $condition,'sub_condation' => $subcondition, 'location' => $location,'start_date' => $start_date, 'end_date' => $end_date);
			}

			$lookingfor = "start_date DESC";
			//$whereArr = array("user_id" => $UserID, "eligible_for_main" => 1, "created_by" => $domainID);
			$current_date = date('Y-m-d');
			$whereArr = array("user_id" => $UserID, "created_by" => $domainID, 'start_date' => $current_date);
			$orwhere = " (end_date <= $current_date OR part.part_status=0)";
			$columnArr = array();

			$totalCount = count($IndStudObj->getStudyArchive($whereArr, $columnArr, $lookingfor, $searchcontent, $orwhere,"",""));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$studyList = $IndStudObj->getStudyArchive($whereArr, $columnArr, $lookingfor, $searchcontent, $orwhere, $lowerLimit, $maxLimit);
			
			foreach($studyList as $key=>$val){
				/////////////////////  Get Status Text Start///////////////////////
				if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
					$labelText = 'Coming Soon..';
				}elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null) && $val['is_accepted']==1){
						$labelText = 'Finished';
					}
					else{
						$labelText = 'Expired';
					}
				}
				else{
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null)){
						$labelText = 'Completed';
					}
					else{
						$labelText = 'Pending';
					}
				}
				$studyList[$key]['labelText'] = $labelText;
				/////////////////////  Get Status Text End///////////////////////
				
				/////////////////////  Get Button Text Start///////////////////////
				if($val['is_accepted']==1){
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null)){
						$buttonText = 'Finished';
					}
					else{
						if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
							$buttonText = 'Coming soon..';
						}
						elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
							$buttonText = 'Expired';
						}
						else{
							$buttonText = 'Participate';
						}
					}
				}else{
					if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
						$buttonText = 'Coming soon..';
					}
					elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
						$buttonText = 'Expired';
					}
					else{
						$buttonText = 'Accept';
					}
				}
				$studyList[$key]['buttonText'] = $buttonText;
				/////////////////////  Get Button Text End///////////////////////
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"studyArr"=>$studyList,"total"=>$totalpage);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


}
