<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE DOCUMENT OF INDIVIDUAL  FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain.
 * CREATOR: 015/09/2015.
 * UPDATED: 015/09/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use \Exception;

class RewardController extends AbstractRestfulController {

    public function __construct() {
	
    }


	public function getList(){
		
    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id){

	}

	/**
	 * Create a new resource
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'getrewardhistory':
				$responseArr = $this->getrewardhistory($service_key);
				break;
			case 'redeempoints':
				$responseArr = $this->redeempoints($service_key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}

	/**
	 * Update an existing resource
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {
	
	}


	/**
	 * Delete an existing resource
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}

	/*
     * @Author:Rachit Jain 
     * @Action: This action used to get screening studies
     */
    public function getrewardhistory($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$StudyTableObj = $this->getServiceLocator()->get("ServicesStudyTable");

		try{
			$data = $this->params;
			$UserId = isset($data['user_id']) ? $data['user_id'] : '';
			$DomainID = isset($data['domain_id']) ? $data['domain_id'] : '1';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			/*$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}*/
			
			if(empty($UserId)){
				throw new Exception("Please enter user id.");
			}

			$where = array('UserID' => $UserId, 'pari_stu.study_type' => 1, 'part_status' => 0, 'ass_id' => 0);
			$whereRedeem = array('user_id' => $UserId, 'pass_id' => $DomainID);
			$studyPoints = $StudyTableObj->getAllStudiesPoints($where, array('points' => new \Zend\Db\Sql\Expression('SUM(points)')));
			$RedeemcurrentPoints = $StudyTableObj->getUserAllRedeemPoints($whereRedeem, array('Redeem_points' => new \Zend\Db\Sql\Expression('SUM(points)')));
			$currentPoints = $studyPoints['points']-$RedeemcurrentPoints['Redeem_points'];
			
			$columns = array('amount', 'points', 'creation_date', 'order_id');
			$where = array('user_id' => $UserId,'pass_id'=>$DomainID);
			
			$totalCount = count($IndUserTableObj->getRedeemHistoy($where, $columns, "0", array(),"",""));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}
			
			$Data = $IndUserTableObj->getRedeemHistoy($where, $columns, "0", array(), $lowerLimit, $maxLimit);
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}
		
		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"Data"=>$Data,"total"=>$totalpage,"EarnstudyPoints"=>(int)$studyPoints['points'],"current_points"=>$currentPoints);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}

    }


	public function redeempoints($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$StudyTableObj = $this->getServiceLocator()->get("ServicesStudyTable");
		$ServicesRewardTableObj = $this->getServiceLocator()->get("ServicesRewardTable");

		try{
			$data = $this->params;
			$userId = isset($data['user_id']) ? $data['user_id'] : '';
			$DomainID = isset($data['domain_id']) ? $data['domain_id'] : '1';
			$points = isset($data['points']) ? $data['points'] : '';

			/*$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}*/

			if(empty($userId) || empty($DomainID) || empty($points)){
				throw new Exception("Required parameter missing.");
			}

			$whereUserArr = array("UserID"=>$userId);
			$userArr = $IndUserTableObj->getUser($whereUserArr);
			$name = $userArr[0]['FirstName'] . " " . $userArr[0]['LastName'];
			$email = isset($userArr[0]['Email']) ? $userArr[0]['Email'] : '';

			$DomainDetail = $IndUserTableObj->getDomainName(array('id' => $DomainID), array('tango_customer'));

			$where = array('UserID' => $userId, 'pari_stu.study_type' => 1, 'part_status' => 0, 'ass_id' => 0);
			$whereRedeem = array('user_id' => $userId, 'pass_id' => $DomainID);
			$studyPoints = $StudyTableObj->getAllStudiesPoints($where, array('points' => new \Zend\Db\Sql\Expression('SUM(points)')));
			$RedeemcurrentPoints = $StudyTableObj->getUserAllRedeemPoints($whereRedeem, array('Redeem_points' => new \Zend\Db\Sql\Expression('SUM(points)')));
			$currentPoints = $studyPoints['points'] - $RedeemcurrentPoints['Redeem_points'];

			if ($currentPoints >= $points) {
				$url = TANGOCARD_URL . "orders";
				//32925564 cc_token
				$Auth = "Basic " . base64_encode(TANGOCARD_PLATEFORM . ":" . TANGOCARD_KEY);
				$fields = array(
					'customer' => $DomainDetail['tango_customer'],
					'account_identifier' => $DomainDetail['tango_customer'],
					'campaign' => SITE_NAME . " Study Reward",
					'recipient' => (Object) array('name' => $name, 'email' => $email),
					'sku' => 'TNGO-E-V-STD',
					'amount' => (int) $points,
					'reward_from' => SITE_NAME,
					'reward_subject' => SITE_NAME . ' Study redeem points',
					'reward_message' => 'Study redeem points',
					'send_reward' => true,
				);
				//print_r($fields);exit;
				$fields_string = json_encode($fields);

				//open connection
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($fields_string),
					'Authorization: ' . $Auth
				));
				$result = curl_exec($ch);
				$encodeRes = json_decode($result, true);
				curl_close($ch);

				if ($encodeRes['success'] == true) {
					$amount = $encodeRes['order']['amount'] / POINTAMOUNT;
					$data = array('user_id' => $userId, 'pass_id' => $DomainID, 'amount' => $amount, 'points' => $encodeRes['order']['amount']
						, 'order_id' => $encodeRes['order']['order_id'], 'creation_date' => date('Y-m-d H:i:s'));
					$IndUserTableObj->insertRedeemHistoy($data);
				} else {
					throw new Exception($encodeRes['error_message']);
				}
			} else {
				throw new Exception("Not enough Study reward point to redeem");
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}
		
		if($sucess === TRUE){
			return array("errstr"=>"Reward point has been redeemed sucessfully","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}

    }
}
