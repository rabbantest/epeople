<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE How I feel  FOR Individual.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain.
 * CREATOR: 29/01/2015.
 * UPDATED:  .
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use \Exception;

class MoodController extends AbstractRestfulController {

    public function __construct() {
		
    }

    public function getList(){
		
    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id) {

	}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);
		//echo '<pre>';print_r($requestArr);exit;

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'addmood':
				$responseArr = $this->addmood($service_key);
				break;
			case 'mood':
				$responseArr = $this->mood($service_key);        
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}

	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {
	
	}

	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}


	/**********
		* Action: Used to add mood info.
		* @author: Rachit Jain
		* Created Date: 29-01-2015.
	*****/
	public function addmood($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndMoodTableObj = $this->getServiceLocator()->get("ServicesMoodRecordTable");
		$creationdate = round(microtime(true) * 1000);
		
		try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : "";
			$GMVD = isset($data['general_mood_value']) ? $data['general_mood_value'] : "";
			$PMVD = isset($data['physical_mood_value']) ? $data['physical_mood_value'] : "";
			$MMVD = isset($data['mental_mood_value']) ? $data['mental_mood_value'] : "";

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($GMVD)){
					throw new Exception("Please check general mood.");
			}
			if(empty($PMVD)){
					throw new Exception("Please check physical mood.");
			}
			if(empty($MMVD)){
					throw new Exception("Please check mental mood.");
			}
			$dataArray = array(
				'UserID' => $userId,               
				'general_mood_value' => $GMVD,
				'physical_mood_value' => $PMVD,
				'mental_mood_value' => $MMVD,
				'mood_record_date' => date('Y-m-d'),
				'mood_record_time' => date('H:i:s'),
				'creation_date' => $creationdate
			);
			$insmood = $IndMoodTableObj->insertmoodRecord($dataArray);
			if(!$insmood){
					throw new Exception("Please try again later some error occured.");
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE)
        { 
            return array("errstr"=>"Mood submitted successfully.","success"=>1);
        }
        else
        {
            return array("errstr"=>$error,"success"=>0);
        }
    }



    public function mood($service_key) {
        $sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndMoodTableObj = $this->getServiceLocator()->get("ServicesMoodRecordTable");
		$creationdate = round(microtime(true) * 1000);

		try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : "";
			$filter_select = isset($data['filter_select']) ? $data['filter_select'] : "oneweek";
			$time_zone = isset($data['time_zone']) ? $data['time_zone'] : 'ASIA/KOLKATA';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			$general_val_avg_week = $physical_val_avg_week = $mental_val_avg_week= '';
			$general_today_val = $physical_today_val = $mental_today_val= $AvgPMV = $AvgPMV = 
			$AvgMMV = $submit_mood = $general_mood_val_avg = $physical_mood_val_avg = $mental_mood_val_avg = '';
			$moodlist = $whereArr= $columns= $dataArray = $todaymooddata = array();
			/* Get today last value
			 * 
			 */
			$wheredataArrayStart = array('UserID' => $userId,
									'mood_record_date' => date("Y-m-d"));
			$lookingfor = "creation_date DESC";
			$todaymooddata = $IndMoodTableObj->getmoodRecord($wheredataArrayStart, $columns, $lookingfor);
			//print_r($todaymooddata);exit;

			$isavalabledata =0;
			if(count($todaymooddata) > 0){
				$general_today_valus =  $todaymooddata[0]['general_mood_value'];
				$general_today_val = number_format((float) $general_today_valus, 2, '.', '');
				$physical_today_vals = $todaymooddata[0]['physical_mood_value'];
				$physical_today_val = number_format((float) $physical_today_vals, 2, '.', '');
				$mental_today_vals = $todaymooddata[0]['mental_mood_value'];
				$mental_today_val= number_format((float) $mental_today_vals, 2, '.', '');
				$mood_record_date = $todaymooddata[0]['mood_record_date'];
				$mood_record_time = $todaymooddata[0]['mood_record_time'];
				$moodDate = $mood_record_date.' '.$mood_record_time;

				if(!empty($general_today_valus) || !empty( $physical_today_vals) || !empty( $physical_today_vals) ){
					$isavalabledata =1;
				}
			}
			$general_unit = '';
            $general_intensity = '';
			$general_color1 = '#5200A3';
			$general_color2 = '#7533B5';
			
			$physical_unit = '';
            $physical_intensity = '';
			$physical_color1 = '#0066CC';
			$physical_color2 = '#4D94DB';
			
			$mental_unit = '';
            $mental_intensity = '';
			$mental_color1 = '#CC6600';
			$mental_color2 = '#D68533';
			
			if(!empty($moodDate)){
				$Cdate = $moodDate;
			}
			else{
				$Cdate = date('Y-m-d H:i:s');
			}

			/*
			 * Monthly graph data
			 */
			if ($filter_select == 'oneyear' || $filter_select == 'sixmonth' || $filter_select == 'threemonth') {

				$time = new \DateTime('now');
				if ($filter_select == 'oneyear') {
					$duration = 12;
					$formate = 'Y-m';
					$formate2 = 'M';
					$month_array = $CusConPlug->month_year_lists($duration,$formate);
					$month_array_list = $CusConPlug->month_year_lists($duration,$formate2);
				}
				if ($filter_select == 'sixmonth') {
					$duration = 5;
					$formate = 'Y-m';
					$formate2 = 'M';
					$month_array = $CusConPlug->month_year_lists($duration,$formate);
					$month_array_list = $CusConPlug->month_year_lists($duration,$formate2);
				}
				if ($filter_select == 'threemonth') {
					$duration = 2;
					$formate = 'Y-m';
					$formate2 = 'M';
					$month_array = $CusConPlug->month_year_lists($duration,$formate);
					$month_array_list = $CusConPlug->month_year_lists($duration,$formate2);
				}
				// arrange month list a/c to graph
				$graph_month_list = $CusConPlug->graphmontlist($month_array_list); 
				
				// get one by one month data
				$i = 0;
				foreach ($month_array as $mvalue) {
					$start_date = date("$mvalue-01");
					$end_date = date("$mvalue-t");

					$wheredataArrayDay = array(
						'UserID' => $userId,
						'start_date' => $start_date,
						'end_date' => $end_date
					);
					$mooddata[$i] = $IndMoodTableObj->getmoodRecordByYear($wheredataArrayDay);
					$i++;
				}

				// get data from loop
				$counth_data_mood = 1;
				foreach ($mooddata as $movalue) {
					$cout_data_mood = count($mooddata);

					if ($counth_data_mood == $cout_data_mood) {
						$delimeter = '';
					} else {
						$delimeter = ",";
					}

					$general_mood_val_avg .= (!empty($movalue[0]['AvgGMV']) ?  number_format((float) $movalue[0]['AvgGMV'], 2, '.', '') : '0.0'). $delimeter;
					$physical_mood_val_avg .= (!empty($movalue[0]['AvgPMV']) ? number_format((float) $movalue[0]['AvgPMV'], 2, '.', '') : '0.0') .$delimeter;
					$mental_mood_val_avg .= (!empty($movalue[0]['AvgMMV']) ? number_format((float) $movalue[0]['AvgMMV'], 2, '.', '') : '0.0'). $delimeter;

					$counth_data_mood++;
				}

				// assign data to variables
				$xaxisData = $graph_month_list;
				$avg_values_general = $general_mood_val_avg;
				$avg_values_physical = $physical_mood_val_avg;
				$avg_values_mental = $mental_mood_val_avg;           
            }
            
            /*
             * Weekly graph data
             */
			if ($filter_select == 'oneweek' || $filter_select == 'twoweek' || $filter_select == 'fourweek') {

				$time = new \DateTime('now');
				if ($filter_select == 'oneweek') {
					$duration_value_start = '-1 week';
					$duration_value_end = '1 week';
				}
				if ($filter_select == 'twoweek') {
					$duration_value_start = '-2 week';
					$duration_value_end = '2 week';
				}
				if ($filter_select == 'fourweek') {
					$duration_value_start = '-4 week';
					$duration_value_end = '4 week';
				}
				//calculate date to get data
				$start_dates = $time->modify($duration_value_start)->format('Y-m-d');
				$end_dates = $time->modify($duration_value_end)->format('Y-m-d');
				$week_array = $CusConPlug->date_difference($start_dates, $end_dates);
				$week_array_list = $CusConPlug->datelist($start_dates, $end_dates);
				if ($filter_select == 'oneweek') {
					$week_array_list = $CusConPlug->daylist($start_dates, $end_dates);
				}
				// set array of week day alternate day
				if ($filter_select == 'twoweek') {
					$week_array = $CusConPlug->date_difference_alternate($start_dates, $end_dates);
					$week_array_list = $CusConPlug->daylist_alternate($start_dates, $end_dates);
				}
				// set array of week day alternate day
				if($filter_select == 'fourweek'){
					$week_array = $CusConPlug->date_difference_alternate($start_dates, $end_dates);
					$week_array_list = $CusConPlug->datelist_alternate($start_dates, $end_dates);
				}            
				$graph_day_list = $CusConPlug->graphmontlist($week_array_list);
				
				// get one by one week daata 
				$i = 0;
				$count_loop_data = 1;
				foreach ($week_array as $wvalue) {
					$count_total_week = count($week_array);

					$start_date_week = date('Y-m-d', strtotime($wvalue));
					$end_date_week = date('Y-m-d', strtotime($wvalue));
					if ($count_total_week > $count_loop_data && ($filter_select == 'fourweek' ||  $filter_select == 'twoweek')) {
						$date = new \DateTime($start_date_week);
						$end_date_week = $date->modify('+1 day')->format('Y-m-d');
					}
					$whereWeekArrayDay = array(
						'UserID' => $userId,
						'start_date' => $start_date_week,
						'end_date' => $end_date_week
					);
					$week_mooddata[$i] = $IndMoodTableObj->getmoodRecordByYear($whereWeekArrayDay);

					$i++;
					$count_loop_data++;
				}
				
				// get one by one week data
				$counth_data_mo_week = 1;
				foreach ($week_mooddata as $moweekvalue) {
					$cout_data_mo_week = count($week_mooddata);

					if ($counth_data_mo_week == $cout_data_mo_week) {
						$delimeter = '';
					} else {
						$delimeter = ",";
					}

					$general_val_avg_week .= (!empty($moweekvalue[0]['AvgGMV']) ? number_format((float) $moweekvalue[0]['AvgGMV'], 2, '.', '') : '0.0').$delimeter;
					$physical_val_avg_week .= (!empty($moweekvalue[0]['AvgPMV']) ? number_format((float) $moweekvalue[0]['AvgPMV'], 2, '.', '') : '0.0').$delimeter;
					$mental_val_avg_week .= (!empty($moweekvalue[0]['AvgMMV']) ? number_format((float) $moweekvalue[0]['AvgMMV'], 2, '.', '') : '0.0').$delimeter;

					$counth_data_mo_week++;
				}

				// assign data to variables
				$xaxisData = $graph_day_list;
				$avg_values_general = $general_val_avg_week;
				$avg_values_physical = $physical_val_avg_week;
				$avg_values_mental = $mental_val_avg_week;
			}

			/*
			 * Hourly graph data 
			 */
			if ($filter_select == 'oneday') {
				//calculate time to get data
				//Get midnight Time of given timezone
				$today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0,0);
				$today->format('Y-m-d H:i:s');

				//Get UTC midnight time
				$today->setTimezone(new \DateTimeZone("UTC"));
				$start = $today->format('Y-m-d H:i:s');
				$week_array = $CusConPlug->time_difference($start);
				
				//$current_date = date('Y-m-d');
				//$start_times = '00:00:00';
				//$end_times = '23:59:59';
				//$week_array = $CusConPlug->time_difference($start_times, $end_times);
				//print_r($week_array);exit;
				// Get one by one hour data 
				$i = 0;
				$count_loop_data = 1;
				foreach ($week_array as $wvalue) {
					$date = date('Y-m-d', strtotime($wvalue));
					$start_time = date('H:i:s', strtotime($wvalue));
					$end_time = date('H:i:s', strtotime($wvalue . ' +1 hour'));
					$whereWeekArrayDay = array(
						'UserID' => $userId,
						'current_date' => $date,
						'start_time' => $start_time,
						'end_time' => $end_time
					);

					$week_mooddata[$i] = $IndMoodTableObj->getmoodRecordOfDay($whereWeekArrayDay);
					$i++;
					$count_loop_data++;
				}

				// get data from loop
				$counth_data_mo_week = 1;
				foreach ($week_mooddata as $moweekvalue) {
					$cout_data_mo_week = count($week_mooddata);

					if ($counth_data_mo_week == $cout_data_mo_week) {
						$delimeter = '';
					} else {
						$delimeter = ",";
					}

					$general_val_avg_week .= (!empty($moweekvalue[0]['AvgGMV']) ? number_format((float) $moweekvalue[0]['AvgGMV'], 2, '.', '') : '0.0').$delimeter;
					$physical_val_avg_week .= (!empty($moweekvalue[0]['AvgPMV']) ? number_format((float) $moweekvalue[0]['AvgPMV'], 2, '.', '') : '0.0').$delimeter;
					$mental_val_avg_week .= (!empty($moweekvalue[0]['AvgMMV']) ? number_format((float) $moweekvalue[0]['AvgMMV'], 2, '.', '') : '0.0').$delimeter;

					$counth_data_mo_week++;
				}

				// assign data to variables
				$avg_values_general = $general_val_avg_week;
				$avg_values_physical = $physical_val_avg_week;
				$avg_values_mental = $mental_val_avg_week;
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE)
        {
            return array("errstr"=>"Success.","success"=>1,"isavalabledata"=>$isavalabledata,"general_today_val"=>$general_today_val,"physical_today_val"=>$physical_today_val,"mental_today_val"=>$mental_today_val,"yaxisData_general"=>$avg_values_general,"yaxisData_physical"=>$avg_values_physical,"yaxisData_mental"=>$avg_values_mental,"filter"=>$filter_select,"general_unit"=>$general_unit,"general_intensity"=>$general_intensity,"general_color1"=>$general_color1,"general_color2"=>$general_color2,"physical_unit"=>$physical_unit,"physical_intensity"=>$physical_intensity,"physical_color1"=>$physical_color1,"physical_color2"=>$physical_color2,"mental_unit"=>$mental_unit,"mental_intensity"=>$mental_intensity,"mental_color1"=>$mental_color1,"mental_color2"=>$mental_color2,"date"=>$Cdate);
        }
        else
        {
            return array("errstr"=>$error,"success"=>0);
        }
    }

}
