<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE SELF ASSESSMENT OF INDIVIDUAL  FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain.
 * CREATOR: 26/02/2015.
 * UPDATED: 00/00/0000.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\EventManager\EventManagerInterface;
use Zend\View\Model\JsonModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Mail;
use \Exception;

class AssessmentController extends AbstractRestfulController {

    public function __construct() {
		
    }

	
	public function getList(){
		
    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id){

	}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);

		//echo '<pre>';print_r($requestArr);exit;

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'getassessments':
				$responseArr = $this->getassessments($service_key);
				break;
			case 'participateassessment':
				$responseArr = $this->participateassessment($service_key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}


	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {

	}


	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}


	/*
     * @Author:Rachit Jain
     * @Action: This action used to get assessments list
     */
    public function getassessments($service_key) {
        $sucess = TRUE;
        $CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $IndAssesTableObj = $this->getServiceLocator()->get("ServicesAssessmentTable");

        try{
			$data = $this->params;
			$userId = isset($data['user_id']) ? $data['user_id'] : '';
			$catId = isset($data['cat']) ? $data['cat'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			//$searchcontent = array();
			$whereCat = array("cat_status"=>1);
			$coloumncat=array("cat_id","cat_name");
			$lookingfor="cat_name ASC";
			$catData = $IndAssesTableObj->getcat($whereCat,$coloumncat,$lookingfor);
			$filter_select = '';

			if(!empty($catId)){
				$filter_select = $catId;
			}

			//if(!empty($filter_select)){
			//	$searchcontent= array("cat"=>$catId);
			//}

			if(!empty($filter_select)){
				$where = array('is_publish' => 1, 'ass_status' => 1,'ass_category'=>$filter_select);
			}else{
				 $where = array('is_publish' => 1, 'ass_status' => 1);
			}
			$notEqual = array('ass_targets' => 3); // assessment target 3 show it is for non-member
			$assessmentData = $IndAssesTableObj->getassessment($where, $columns = array(), $lookingfor = false, $notEqual);
			if (count($assessmentData) > 0) {
				$i = 0;
				foreach ($assessmentData as $value) {
					$wherePart = array("UserID" => $userId, "ass_id" => $value['ass_id']);
					$partishipData = $IndAssesTableObj->getPartish($wherePart);
					if (count($partishipData) > 0) {
						$assessmentData[$i]['part_status'] = $partishipData[0]['part_status'];
					} else {
						$assessmentData[$i]['part_status'] = 1;
					}
					if($assessmentData[$i]['part_status'] == 0){
						$assessmentData[$i]['part_status_text'] = 'Completed';
					}
					else{
						$assessmentData[$i]['part_status_text'] = 'Pending';
					}
					$assessmentData[$i]['publish_date'] = date("d M Y", $value['publish_date']);
					//Get Full image path
					if($assessmentData[$i]['assessment_image'] != ""){
						$server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
						$image = $server_url."/Assessment/".$assessmentData[$i]['assessment_image'];
						$assessmentData[$i]['assessment_image'] = $image;
					}
					$i++;
				}
			}
			else{
				throw new Exception("No assessment found.");
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"assessmentData"=>$assessmentData,"catData"=>$catData);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*
     * @Author:Rachit Jain 
     * @Action: This action used for first time partishipation
     */
    public function participateassessment($service_key) {
        $sucess = TRUE;
        $CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $IndAssesTableObj = $this->getServiceLocator()->get("ServicesAssessmentTable");

        try{
			$data = $this->params;
			//$userId = isset($data['user_id']) ? $data['user_id'] : '72';
			//$userId = $CusConPlug->encryptdata($userId);
			$assesment_id = isset($data['assessment_id']) ? $data['assessment_id'] : '';
			$assesment_id = $CusConPlug->encryptdata($assesment_id);
			$type = 'mobile';
			$type = $CusConPlug->encryptdata($type);

			/*$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}*/

			$Url = "http://individual.quantextualhealth.com/individual/index/login?assessment_id=".$assesment_id."&type=".$type;
			//$Url = html_entity_decode($Url);

		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"url"=>$Url);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }

}
