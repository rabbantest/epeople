<?php
/* * ***********************
 * PAGE: USE TO MANAGE THE DOCUMENT OF INDIVIDUAL  FOR APPONTMENT.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain.
 * CREATOR: 10/02/2015.
 * UPDATED: 10/02/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use \Exception;

class AppointmentController extends AbstractRestfulController {

    public function __construct() {
		
    }

    public function getList(){

    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id) {

	}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);
		//echo '<pre>';print_r($requestArr);exit;

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}
		$responseArr = array();
		switch ($method) {
			case 'getappointments':
				$responseArr = $this->getappointments($service_key);
				break;
			case 'getmonthappointments':
				$responseArr = $this->getmonthappointments($service_key);
				break;
			case 'addappointment':
				$responseArr = $this->addappointment($service_key);
				break;
			case 'editappointment':
				$responseArr = $this->editappointment($service_key);
				break;
			case 'deleteappointment':
				$responseArr = $this->deleteappointment($service_key);
				break;
			case 'providerlist':
				$responseArr = $this->providerlist($service_key);        
				break;
			case 'addemergencycontact'  :
				 $responseArr = $this->addemergencycontact($service_key); 
				break;
			case 'providerdata':
				$responseArr = $this->providerdata($service_key);        
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}

	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {

	}

	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}


	/*****
     * Action: Get Appointments
     * @author: Rachit Jain
     * Created Date: 25-02-2015.
     *****/
    public function getappointments($service_key) {
		$sucess = TRUE;
        $CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $IndAppoiTableObj = $this->getServiceLocator()->get("ServicesAppointmentTable");
        $IndEMCTableObj = $this->getServiceLocator()->get("ServicesEmergencycontactTable");

        try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : '';
			$appointment_date = isset($data['appointment_date']) ? date("Y-m-d", strtotime($data['appointment_date'])) : '';
			$time_zone = isset($data['time_zone']) ? $data['time_zone'] : 'ASIA/KOLKATA';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($userId)) {
				throw new Exception("Please enter user id.");
			}
			if(empty($appointment_date)) {
				throw new Exception("Please enter appointment date.");
			}
			
			//Get midnight Time of given timezone
			$today = (new \DateTime($appointment_date, new \DateTimeZone($time_zone)))->setTime(0,0);
			$today->format('Y-m-d H:i:s');

			//Get UTC midnight time
			$today->setTimezone(new \DateTimeZone("UTC"));
			$start = $today->format('Y-m-d H:i:s');
			$end = date('Y-m-d H:i:s', strtotime($start . ' +23 hour'));
			
			$whereappArray = array('UserID' => $userId, 'start' => $start, 'end' => $end,'is_deleted' => 0);

			//$whereappArray = array('UserID' => $userId, 'appointment_date' => $appointment_date,'is_deleted' => 0);

			$columnsapp = array();
			$lookingforapp ="creation_date DESC";
			$appoinmentData = array();
			$appoinmentData = $IndAppoiTableObj->getAppoiRecord($whereappArray, $columnsapp, $lookingforapp);
			//echo '<pre>';print_r($appoinmentData);exit;
			$i=0;
			foreach ($appoinmentData as $apvalue) {
				$provider_id = $apvalue['provider'];
				$type_id = $apvalue['type'];
				$status_id = $apvalue['status'];
				$appoinmentData[$i]['purpose']=  $apvalue['purpose_type'];

				//provider name
				if(!empty($provider_id)){
					$whereProArray = array('contacts_id' => $provider_id);
					$columns = array('name');
					$providerData =  $IndEMCTableObj->getEMCRecord($whereProArray, $columns);
					$appoinmentData[$i]['provider_name'] = $providerData[0]['name'];
				}else{
					$appoinmentData[$i]['provider_name']= '';     
				}
				//appointment type
				if(!empty($type_id)){
					$wherTypeAr = array('appointment_type_id' =>$type_id );
					$contactType = $IndEMCTableObj->getContactType($wherTypeAr);
					$appoinmentData[$i]['contact_type_name']= $contactType[0]['appointment_type'];
				}else{
					$appoinmentData[$i]['contact_type_name']= '';    
				}
				//appointment status
				if(!empty($status_id)){ 
					$wherStatusAr = array('status_id' => $status_id);
					$statusType = $IndAppoiTableObj->getAppStatus($wherStatusAr);
					$appoinmentData[$i]['appointment_status_name']=  $statusType[0]['status'];
				}else{
					$appoinmentData[$i]['appointment_status_name']='';   
				}

				//$appoinmentData[$i]['appointment_date'] = date('d M Y', strtotime($apvalue['appointment_date']));
				//$appoinmentData[$i]['appointment_time'] = date('H:i:s', strtotime($apvalue['appointment_time']));
				$i++;
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"appoinmentData"=>$appoinmentData);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: Get Appointments
     * @author: Rachit Jain
     * Created Date: 25-02-2015.
     *****/
    public function getmonthappointments($service_key) {
		$sucess = TRUE;
        $CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $IndAppoiTableObj = $this->getServiceLocator()->get("ServicesAppointmentTable");

        try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : "";
			$month = isset($data['month']) ? $data['month'] : "";
			$year = isset($data['year']) ? $data['year'] : "";

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($userId)) {
				throw new Exception("Please enter user id.");
			}
			if(empty($month)) {
				throw new Exception("Please enter month.");
			}
			if(empty($year)) {
				throw new Exception("Please enter year.");
			}

			$whereappArray = array('UserID'=>$userId,'month'=>$month,'year'=>$year,'is_deleted' => 0);

			$columnsapp = array("appointment_id", "appointment_date");
			$lookingforapp ="creation_date DESC";
			$appoinmentData = array();
			$appoinmentData = $IndAppoiTableObj->getAppoiRecord($whereappArray, $columnsapp, $lookingforapp);
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"appoinmentData"=>$appoinmentData);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }

/*****
     * Action: Add Emergency Contact
     * @author: Shivendra Suman
     * Created Date: 30-10-2015.
     *****/
	public function addemergencycontact($service_key) {
		$sucess = TRUE;
        $IndEMCTableObj = $this->getServiceLocator()->get("ServicesEmergencycontactTable");

		try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : "";
			$contact_name = (isset($data['contact_name']) ? $data['contact_name'] : "");
			$contact_type= (isset($data['contact_type']) ? $data['contact_type'] : "");
			$contact = (isset($data['contact']) ? $data['contact'] : "");     
			$notes = (isset($data['notes']) ? $data['notes'] : "");  
			//valide service and user 
			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($userId)) {
				throw new Exception("Please enter user id.");
			}
			if(empty($contact_name)) {
				throw new Exception("Please enter contact name.");
			}
			if(empty($contact_type)) {
				throw new Exception("Please select contact type.");
			}
			if(empty($contact)) {
				throw new Exception("Please enter contact.");
			}
			$creation_date = round(microtime(true) * 1000);
			$SaveArray = array(
			 'UserID' => $userId,
			 'name' => $contact_name,
             'contact' =>  $contact,
			 'contact_type' =>  $contact_type,			
			 'notes' => addslashes($notes),			
			 'creation_date' => $creation_date
			);
			$insData = $IndEMCTableObj->insertEMC($SaveArray);
			if(!$insData){
				throw new Exception("Some error occured , please try again later.");
			}
		}
        catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Emergency contact added.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: Add Appointment
     * @author: Rachit Jain
     * Created Date: 25-02-2015.
     *****/
	public function addappointment($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndAppoiTableObj = $this->getServiceLocator()->get("ServicesAppointmentTable");

		try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : "";
			$purpose = isset($data['purpose']) ? $data['purpose'] : "";
			//$purpose_type_id = isset($data['purpose_type_id']) ? $data['purpose_type_id'] : "";
			$status = isset($data['status']) ? $data['status'] : "";
			$provider = isset($data['provider']) ? $data['provider'] : "";
			$appointment_date = isset($data['appointment_date']) ? date("Y-m-d", strtotime($data['appointment_date'])) : "";
			$appointment_time = isset($data['appointment_time']) ? date("H:i:s", strtotime($data['appointment_time'])) : "";
			$type = isset($data['type']) ? $data['type'] : "";
			$specialty = isset($data['specialty']) ? $data['specialty'] : "";
			$description = isset($data['description']) ? $data['description'] : "";

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($userId)) {
				throw new Exception("Please enter user id.");
			}
			if(empty($purpose)) {
				throw new Exception("Please enter purpose.");
			}
			/*if(empty($purpose_type_id)) {
				throw new Exception("Please enter purpose type id.");
			}*/
			if(empty($status)) {
				throw new Exception("Please select status.");
			}
			if(empty($appointment_date) || empty($appointment_time)) {
				throw new Exception("Please enter datetime.");
			}
			$creation_date = round(microtime(true) * 1000);
			$SaveArray = array(
			 'UserID' => $userId,
			 'provider' => $provider,
			 'appointment_date' =>  $appointment_date,
			 'appointment_time' =>  $appointment_time,
			 'purpose' => $purpose,
			 'type' => $type,
			 'specialty' => $specialty,
			 'status' => $status,
			 'description' => $description,
			 'creation_date' => $creation_date
			);
			$insData = $IndAppoiTableObj->insertAppoi($SaveArray);
			if(!$insData){
				throw new Exception("Some error occured , please try again later.");
			}
		}
        catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Appointment successfully added.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: Edit Appointment
     * @author: Rachit Jain
     * Created Date: 25-02-2015.
     *****/
	public function editappointment($service_key) {
        $sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndAppoiTableObj = $this->getServiceLocator()->get("ServicesAppointmentTable");

		try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : "";
			$appointmentID = isset($data['appointmentid']) ? $data['appointmentid'] : "";
			$purpose = isset($data['purpose']) ? $data['purpose'] : "";
			$status = isset($data['status']) ? $data['status'] : "";
			$provider = isset($data['provider']) ? $data['provider'] : "";
			$appointment_date = isset($data['appointment_date']) ? date("Y-m-d", strtotime($data['appointment_date'])) : "";
			$appointment_time = isset($data['appointment_time']) ? date("H:i:s", strtotime($data['appointment_time'])) : "";
			$type = isset($data['type']) ? $data['type'] : "";
			$specialty = isset($data['specialty']) ? $data['specialty'] : "";
			$description = isset($data['description']) ? $data['description'] : "";

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($userId)) {
				throw new Exception("Please enter user id.");
			}
			if(empty($appointmentID)) {
				throw new Exception("Please enter appointment id.");
			}
			if(empty($purpose)) {
				throw new Exception("Please enter purpose.");
			}
			if(empty($status)) {
				throw new Exception("Please select status.");
			}
			if(empty($appointment_date) || empty($appointment_time)) {
				throw new Exception("Please enter datetime.");
			}
			$creation_date = round(microtime(true) * 1000);
			$SaveArray = array(
             'UserID' => $userId,
             'provider' => $provider,
             'appointment_date' => $appointment_date,
             'appointment_time' => $appointment_time,
             'purpose' => $purpose,
             'type' => $type,
             'status' => $status,
             'specialty' => $specialty,
             'description' => $description,
             'updation_date' => $creation_date
			);
			$whereUpdate = array('appointment_id' => $appointmentID);
			$insData = $IndAppoiTableObj->updateAppoi($whereUpdate,$SaveArray);
			if(!$insData){
				throw new Exception("Some error occured , please try again later.");
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

        if($sucess === TRUE){
			return array("errstr"=>"Appointment successfully updated.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: Delete Appointment
     * @author: Rachit Jain
     * Created Date: 25-02-2015.
     *****/
	public function deleteappointment($service_key) {
		$sucess = TRUE;
		$IndAppoiTableObj = $this->getServiceLocator()->get("ServicesAppointmentTable");
		
		try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : "";
			$appointment_id = isset($data['appointmentid']) ? $data['appointmentid'] : "";

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			$whereArr = array("UserID" => $userId,'appointment_id'=>$appointment_id);
			$columns = array('is_deleted' => 1);
			$res = $IndAppoiTableObj->updateAppoi($whereArr, $columns);
			if(!$res){
				throw new Exception("Some error occured.");         
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
	}


	/*****
     * Action: List provider name
     * @author: Rachit Jain
     * Created Date: 25-02-2015.
     *****/
    public function providerlist($service_key) {
		$sucess = TRUE;
		$IndAppoiTableObj = $this->getServiceLocator()->get("ServicesAppointmentTable");
		$IndEMCTableObj = $this->getServiceLocator()->get("ServicesEmergencycontactTable");
        $servicPTObj = $this->getServiceLocator()->get("ServicePurposeTypeTable");
            
		try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : "";

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			$whereArray = array('UserID'=>$userId, 'contact_type' => '2', 'status' => 0);
			$columns = array('name','contacts_id');
			$lookingfor = 'updation_date DESC';
			$providerData =  $IndEMCTableObj->getEMCRecord($whereArray, $columns, $lookingfor);
			$contactType = $IndEMCTableObj->getContactType();//contact type
			$statusType = $IndAppoiTableObj->getAppStatus(); //appointment status
			$coloumnpurpt = array("purpose_type_id", "purpose_type");
			$purposeType =  $servicPTObj->getPurposeType(array(), $coloumnpurpt); //purpose type
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

        if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"providerData"=>$providerData,"contactType"=>$contactType,"statusType"=>$statusType, "purposeType" => $purposeType);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: List provider name
     * @author: Rachit Jain
     * Created Date: 25-02-2015.
     *****/
    public function providerdata($service_key) {
		$sucess = TRUE;
		$IndAppoiTableObj = $this->getServiceLocator()->get("ServicesAppointmentTable");
		$IndEMCTableObj = $this->getServiceLocator()->get("ServicesEmergencycontactTable");
        $servicPTObj = $this->getServiceLocator()->get("ServicePurposeTypeTable");
            
		try{
			$data = $this->params;
			$userId = isset($data['user_id']) ? $data['user_id'] : "";
			$contactType = isset($data['contact_type_id']) ? $data['contact_type_id'] : "";

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			$whereArray = array('UserID'=>$userId, 'contact_type' => $contactType, 'status' => 0);
			$columns = array('name','contacts_id');
			$lookingfor = 'updation_date DESC';
			$providerData =  $IndEMCTableObj->getEMCRecord($whereArray, $columns, $lookingfor);
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

        if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"providerData"=>$providerData);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }
}
