<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE History CONTROLLER FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 30/12/2014.
 * UPDATED: 30/12/2014.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use \Exception;

class HistoryController extends AbstractRestfulController {

    public function __construct() {
		
    }


    public function getList(){
		$request = $this->request->getQuery();
		$method = $request->method;
		$key = $request->key;
		$search = $request->search;
		$id = $request->cid;
		$user_id = $request->user_id;
		$service_key = $request->service_key;
		
		$params['user_id'] = $user_id;
		$params['type'] = 'GET';
		$this->params = $params;
		
		switch ($method) {
			case 'gethistorylist':
				$responseArr = $this->gethistorylist($key,$search);
				break;
			case 'getsubhistorylist':
				$responseArr = $this->getsubhistorylist($key,$id,$search);
				break;
			case 'getuserconditions':
				$responseArr = $this->getuserconditions($service_key);
				break;
			case 'getstatuslist':
				$responseArr = $this->getstatuslist($key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id) {

	}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'getuserconditions':
				$responseArr = $this->getuserconditions($service_key);
				break;
			case 'addcondition':
				$responseArr = $this->addcondition($service_key);
				break;
			case 'editcondition':
				$responseArr = $this->editcondition($service_key);
				break;
			case 'conditiondescription':
				$responseArr = $this->conditiondescription($service_key);
				break;
			case 'updateconditonstatus':
				$responseArr = $this->updateconditonstatus($service_key);
				break;
			case 'checkuserconditions':
				$responseArr = $this->checkuserconditions($service_key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}

	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {
	
	}

	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}


	/*****
     * Action: Used to get user conditions
     * @author: Rachit Jain
     * Created Date: 16-03-2015.
     *****/
    public function getuserconditions($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custome plugin
		$IndCondiTableObj = $this->getServiceLocator()->get("ServicesConditionTable");
		$IndUserConditionTableObj = $this->getServiceLocator()->get("ServicesUsersConditionTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;
			//FROM GET METHOD
			$type = isset($data['type']) ? $data['type'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID)){
				throw new Exception("Please enter user id.");
			}

			$whereArr = array("user_id" => $UserID,'con.status'=>1);
			
			$totalCount = count($IndUserConditionTableObj->getUserAllConditions($whereArr, array(), "", ""));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$conditionslist = $IndUserConditionTableObj->getUserAllConditions($whereArr, array(), $lowerLimit, $maxLimit);
			
			if($type == 'GET'){
				$i=0;
				foreach($conditionslist as $k=>$v){
					$userconditions[$i]['id'] = $v['condition_id'];
					$userconditions[$i]['condition_id'] = $v['condition_id'];
					$userconditions[$i]['common_name'] = $v['common_name'];
					$i++;
				}
			}
			else{
				$i=0;
				foreach($conditionslist as $k=>$v){
					$conditionslist[$i]['id'] = $v['condition_id'];
					$conditionslist[$i]['sub_condition_name'] = isset($v['sub_condition_name']) ? $v['sub_condition_name'] : '';
					$conditionslist[$i]['sub_description'] = isset($v['sub_description']) ? $v['sub_description'] : '';
					$i++;
				}
				$userconditions = $conditionslist;
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"user_Conditions"=>$userconditions,"total"=>$totalpage);
		}
        else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: Used to add conditions and subconditions
     * @author: Rachit Jain
     * Created Date: 18-03-2015.
     *****/
    /*public function addcondition() {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $IndCondiTableObj = $this->getServiceLocator()->get("ServicesConditionTable");
        $IndUserConditionTableObj = $this->getServiceLocator()->get("ServicesUsersConditionTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$condition_list = isset($data['condition_list']) ? $data['condition_list'] : array();
			$condtion_status = isset($data['condtion_status']) ? $data['condtion_status'] : array();
			$subcondtion = isset($data['subcondtion']) ? $data['subcondtion'] : array();

			if(!count($condition_list)){
				throw new Exception("Please select condition first.");
			}

			$existCountCondition = $IndUserConditionTableObj->getUserConditionsList(array("user_id" => $UserID,'us.status'=>1));
			if(count($existCountCondition)>9){
				throw new Exception("You can not add more than 9 conditions.");
			}

			foreach ($condition_list as $k=>$val) {
				$conditionStatusID = $condtion_status[$k];
				$SubconditionId = $subcondtion[$k];
				$conditonArr = array('user_id'=>$UserID,'condition_id'=>$val,
				'sub_condition_id'=>$SubconditionId,'condition_status'=>$conditionStatusID,'creation_date'=>round(microtime(true) * 1000),
				'updation_date'=>round(microtime(true) * 1000));
				$IndUserConditionTableObj->SaveUserCondions($conditonArr);
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Data added successfully.","success"=>1);
		}
        else{
			return array("errstr"=>$error,"success"=>0);
		}
    }
	*/
	public function addcondition($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $IndUserConditionTableObj = $this->getServiceLocator()->get("ServicesUsersConditionTable");
        $FollowerUserObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$condition_list = isset($data['condition_list']) ? $data['condition_list'] : '';
			$condition_status = isset($data['condition_status']) ? $data['condition_status'] : '';
			$subcondition = isset($data['subcondition']) ? $data['subcondition'] : '';
			//$connected_users = isset($data['connected_users']) ? $data['connected_users'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID)){
				throw new Exception("Please enter user id.");
			}
			if(empty($condition_list)){
				throw new Exception("Please select condition first.");
			}
			if(empty($condition_status)){
				throw new Exception("Please select condition status.");
			}

			$existCondition = $IndUserConditionTableObj->getUserConditionsList(array("user_id" => $UserID,"condition_id" => $condition_list,'us.status'=>1));
			if(count($existCondition)){
				throw new Exception("You have already added this condition.");
			}

			$existCountCondition = $IndUserConditionTableObj->getUserConditionsList(array("user_id" => $UserID,'us.status'=>1));
			if(count($existCountCondition)>9){
				throw new Exception("You can not add more than 9 conditions.");
			}

			$conditionArr = array('user_id'=>$UserID,'condition_id'=>$condition_list,
			'sub_condition_id'=>$subcondition,'condition_status'=>$condition_status,'creation_date'=>round(microtime(true) * 1000),
			'updation_date'=>round(microtime(true) * 1000));
			$IndUserConditionTableObj->SaveUserCondions($conditionArr);

			$connectedUser = array();
			//Get Connected Peoples
			$whereFollower['follow.user_id'] = $UserID;
			$whereFollower['follow.status'] = 1;
			$whereFollower['follow.is_send_notification'] = 1;
			$connectedUser = $FollowerUserObj->getUserFollowerDetails($whereFollower);

			/*
			if(!empty($connected_users)){
				$connectedUser = explode(',', $connected_users);
			}
			*/

			//UserInfo
			$where = array("UserID"=>$UserID);
			$columns = array('FirstName','LastName','Email');
			$userData = $IndUserTableObj->getUser($where,$columns);
			$UserName = $userData[0]['FirstName'].' '.$userData[0]['LastName'];

			//Save Notification
			$type = 'condition_add';
			foreach($connectedUser as $k=>$v){
				$notDataArr = array("sender_id"=>$UserID,"receiver_id"=>$v['follower_id'],"user_type"=>1,"created_date"=>round(microtime(true) * 1000));
				$saveNoti = $NotificationTableObj->SaveNotification($notDataArr, $type);

				//Get User Devices
				$deviceTokenData = array();
				$whereArr = array('UserID'=>$v['follower_id'], 'status'=>1);
				$deviceTokenData = $IndUserTableObj->getDeviceToken($whereArr);

				$androidArray = array();
				$iphoneArray = array();
				foreach($deviceTokenData as $key=>$val){
					if(($val['DeviceType'] == 'android') && !empty($val['RegistrationID'])){
						array_push($androidArray, $val['RegistrationID']);
					}
					elseif(($val['DeviceType'] == 'ios') && !empty($val['DeviceToken'])){
						array_push($iphoneArray, $val['DeviceToken']);
					}
				}

				//Send Notification To User
				$message = $UserName.' added a condition';
				$push_data =array();
				$push_data['user_id'] = $UserID;
				$push_data['type'] = $type;
				$push_data['alert'] = $message;
				$push_data['badge'] = 1; 
				$push_data['sound'] = 'default';
				$payload['aps'] = $push_data;

				if(count($iphoneArray) > 0){
					$CusConPlug->sendPush($iphoneArray, $payload);
				}
				if(count($androidArray) > 0){
					$CusConPlug->sendPushAndroid($androidArray, $payload);
				}
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Data added successfully.","success"=>1);
		}
        else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: Used to edit conditions and subconditions
     * @author: Rachit Jain
     * Created Date: 18-03-2015.
     *****/
    public function editcondition($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $IndUserConditionTableObj = $this->getServiceLocator()->get("ServicesUsersConditionTable");
		$FollowerUserObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$conditionID = isset($data['condition_id']) ? $data['condition_id'] : '';
			$conditionStatusID = isset($data['condition_status']) ? $data['condition_status'] : '';
			$SubconditionId = isset($data['subcondition_id']) ? $data['subcondition_id'] : '';
			//$connected_users = isset($data['connected_users']) ? $data['connected_users'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID) || empty($conditionID) || empty($conditionStatusID)){
				throw new Exception("Required parameter missing.");
			}

			$where = array('user_id'=>$UserID,'status'=>1,'condition_id'=>$conditionID);
			$data = array('sub_condition_id'=>$SubconditionId,'condition_status'=>$conditionStatusID);
			$IndUserConditionTableObj->updateUserSubcondition($where,$data);

			$connectedUser = array();
			//Get Connected Peoples
			$whereFollower['follow.user_id'] = $UserID;
			$whereFollower['follow.status'] = 1;
			$whereFollower['follow.is_send_notification'] = 1;
			$connectedUser = $FollowerUserObj->getUserFollowerDetails($whereFollower);

			/*
			if(!empty($connected_users)){
				$connectedUser = explode(',', $connected_users);
			}
			*/

			//UserInfo
			$where = array("UserID"=>$UserID);
			$columns = array('FirstName','LastName','Email');
			$userData = $IndUserTableObj->getUser($where,$columns);
			$UserName = $userData[0]['FirstName'].' '.$userData[0]['LastName'];

			//Save Notification
			$type = 'condition_edit';
			foreach($connectedUser as $k=>$v){
				$notDataArr = array("sender_id"=>$UserID,"receiver_id"=>$v['follower_id'],"user_type"=>1,"created_date"=>round(microtime(true) * 1000));
				$saveNoti = $NotificationTableObj->SaveNotification($notDataArr, $type);

				//Get User Devices
				$deviceTokenData = array();
				$whereArr = array('UserID'=>$v['follower_id'], 'status'=>1);
				$deviceTokenData = $IndUserTableObj->getDeviceToken($whereArr);

				$androidArray = array();
				$iphoneArray = array();
				foreach($deviceTokenData as $key=>$val){
					if(($val['DeviceType'] == 'android') && !empty($val['RegistrationID'])){
						array_push($androidArray, $val['RegistrationID']);
					}
					elseif(($val['DeviceType'] == 'ios') && !empty($val['DeviceToken'])){
						array_push($iphoneArray, $val['DeviceToken']);
					}
				}

				//Send Notification To User
				$message = $UserName.' edit his condition';
				$push_data =array();
				$push_data['user_id'] = $UserID;
				$push_data['type'] = $type;
				$push_data['alert'] = $message;
				$push_data['badge'] = 1; 
				$push_data['sound'] = 'default';
				$payload['aps'] = $push_data;

				if(count($iphoneArray) > 0){
					$CusConPlug->sendPush($iphoneArray, $payload);
				}
				if(count($androidArray) > 0){
					$CusConPlug->sendPushAndroid($androidArray, $payload);
				}
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Data updated successfully.","success"=>1);
		}
        else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: This action is used to update the condition status
     * @author: Rachit Jain
     * Created Date: 23-03-2014.
     *****/
    public function updateconditonstatus($service_key) {
        $sucess = TRUE;
        $IndUserConditionsTableObj = $this->getServiceLocator()->get("ServicesUsersConditionTable");
        
        try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$conditionID = isset($data['condition_id']) ? $data['condition_id'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID) || empty($conditionID)){
				throw new Exception("Required parameter missing.");
			}

			$whereArr = array("user_id" => $UserID, 'condition_id' => $conditionID);
			$IndUserConditionsTableObj->updateUserSubcondition($whereArr, array('status' => 0));
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1);
		}
        else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: Used for condition description
     * @author: Rachit Jain
     * Created Date: 18-03-2015.
     *****/
	 public function conditiondescription($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserConditionTableObj = $this->getServiceLocator()->get("ServicesUsersConditionTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$condition_id = isset($data['condition_id']) ? $data['condition_id'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID) || empty($condition_id)){
				throw new Exception("Required parameter missing.");
			}

			$whereArr = array("user_id" => $UserID,'con.status'=>1,'con.condition_id'=>$condition_id);
			$conditionslist = $IndUserConditionTableObj->getUserAllConditions($whereArr);
			$finalRes = array();
			$result = array();
			if(count($conditionslist)){
				foreach($conditionslist as $val){
					$result['condition_id'] = $val['condition_id'];
					$result['condition_name'] = $val['common_name'];
					$result['condition_description'] = $val['description'];
					$result['sub_condition_name'] = $val['sub_condition_name'];
					$result['sub_condition_description'] = $val['sub_description'];
					$result['status'] = $val['conStatus'];
					$finalRes[] = $result;
				}
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"user_Conditions"=>$finalRes[0]);
		}
        else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


    /*****
     * Action: Used to check user already add condition
     * @author: Rachit Jain
     * Created Date: 18-03-2015.
     *****/
    public function checkuserconditions($service_key) {
        $sucess = TRUE;
        $IndUserConditionsTableObj = $this->getServiceLocator()->get("ServicesUsersConditionTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$condition_id = isset($data['condition_id']) ? $data['condition_id'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID) || empty($condition_id)){
				throw new Exception("Required parameter missing.");
			}

			$whereArr = array("user_id" => $UserID, 'condition_id' => $condition_id, 'status' => 1);
			$res = $IndUserConditionsTableObj->getUserConditionsList($whereArr);
			if(count($res)){
				throw new Exception("This condition already added by you.");
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1);
		}
        else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


    /*****
     * Action: This action is used to get list of conditions, symptoms, treatments
     * @author: Rachit Jain
     * Created Date: 05-02-2015.
     *****/
    public function gethistorylist($key, $search){
		$sucess = TRUE;
		$IndCondiTableObj = $this->getServiceLocator()->get("ServicesConditionTable");
        $IndSymtomsTableObj = $this->getServiceLocator()->get("ServicesSymtomsTable");
        $IndTreatmentTableObj = $this->getServiceLocator()->get("ServicesTreatmentTable");
        $IndUserConditionTableObj = $this->getServiceLocator()->get("ServicesUsersConditionTable");
        
        try{
			$finalArr = $conditionsArr = $symtomsArr = $treatmentArr = $statusArr = array();

			$searchContent = array();
			$columns = array("id", "common_name");
			$whereArr = array("status"=>1);
			$searchContent['common_name'] = $search;

			if($key == 'condition'){
				//List conditions with subconditions
				$conditionsArr = $IndCondiTableObj->getConditionsList($whereArr, $columns, "0", $searchContent);

				$finalArr = $conditionsArr;
				if(empty($finalArr)){
					throw new Exception("No result found");
				}
			}

			if($key == 'symptom'){
				//List symptoms with subsymptoms
				$symtomsArr = $IndSymtomsTableObj->getSymptoms($whereArr, $columns, "0", $searchContent);

				$finalArr = $symtomsArr;
				if(empty($finalArr)){
					throw new Exception("No result found");
				}
			}

			if($key == 'treatment'){
				//List treatments with subtreatments
				$treatmentArr = $IndTreatmentTableObj->getTreatments($whereArr, $columns, "0", $searchContent);

				$finalArr = $treatmentArr;
				if(empty($finalArr)){
					throw new Exception("No result found");
				}
			}

			//Get condition status list.
			$wherecond = array("type"=>1);
			$conditionstatusArr = $IndUserConditionTableObj->getConditionSymptomsTreatmentStatus($wherecond);
			
			//Get symptom status list.
			$wheresymp = array("type"=>2);
			$symptomstatusArr = $IndUserConditionTableObj->getConditionSymptomsTreatmentStatus($wheresymp);
			
			//Get treatment status list.
			$wheretreat = array("type"=>3);
			$treatmentstatusArr = $IndUserConditionTableObj->getConditionSymptomsTreatmentStatus($wheretreat);
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess == TRUE){
			return array("errstr"=>"Success.","success"=>1,"resultArr"=>$finalArr,"conditionstatusArr"=>$conditionstatusArr,"symptomstatusArr"=>$symptomstatusArr,"treatmentstatusArr"=>$treatmentstatusArr);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
	}


	/*****
     * Action: Use to get sub conditions .
     * @author: Rachit Jain
     * Created Date: 18-03-2015.
     *****/
    public function getsubhistorylist($key, $id, $search) {
        $sucess = TRUE;
        $IndCondiTableObj = $this->getServiceLocator()->get("ServicesConditionTable");
        $IndSymtomsTableObj = $this->getServiceLocator()->get("ServicesSymtomsTable");
        $IndTreatmentTableObj = $this->getServiceLocator()->get("ServicesTreatmentTable");

        try{
			$finalArr = $subconditionsArr = $subsymtomsArr = $subtreatmentArr = array();

			$searchContent = array();
			$subcolumns = array("sub_id", "sub_common_name");
			$searchContent['sub_common_name'] = $search;

			if($key == 'subcondition'){
				$subwhereArr = array("sub_status" => 1, 'condition_id' => $id);
				$subconditionsArr = $IndCondiTableObj->getAdminSubConditions($subwhereArr, $subcolumns,"0",$searchContent);
				$finalArr = $subconditionsArr;
				if(empty($finalArr)){
					throw new Exception("No result found");
				}
			}
			
			if($key == 'subsymptom'){
				$subwhereArr = array("sub_status" => 1, 'symptoms_id' => $id);
				$subsymtomsArr = $IndSymtomsTableObj->getAdminSubSymptoms($subwhereArr, $subcolumns,"0",$searchContent);
				$finalArr = $subsymtomsArr;
				if(empty($finalArr)){
					throw new Exception("No result found");
				}
			}
			
			if($key == 'subtreatment'){
				$subwhereArr = array("sub_status" => 1, 'treatments_id' => $id);
				$subtreatmentArr = $IndTreatmentTableObj->getAdminSubTreatments($subwhereArr, $subcolumns,"0",$searchContent);
				$finalArr = $subtreatmentArr;
				if(empty($finalArr)){
					throw new Exception("No result found");
				}
			}

		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"resultArr"=>$finalArr);
		}
        else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: This action is used to get status list of conditions, symptoms, treatments
     * @author: Rachit Jain
     * Created Date: 05-06-2015.
     *****/
    public function getstatuslist($key){
		$sucess = TRUE;
        $IndUserConditionTableObj = $this->getServiceLocator()->get("ServicesUsersConditionTable");
        
        try{
			if($key == 'condition'){
				$wherecond = array("type"=>1);
				$resultArr = $IndUserConditionTableObj->getConditionSymptomsTreatmentStatus($wherecond);
			}

			if($key == 'symptom'){
				$wheresymp = array("type"=>2);
				$resultArr = $IndUserConditionTableObj->getConditionSymptomsTreatmentStatus($wheresymp);
			}

			if($key == 'treatment'){
				$wheretreat = array("type"=>3);
				$resultArr = $IndUserConditionTableObj->getConditionSymptomsTreatmentStatus($wheretreat);
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess == TRUE){
			return array("errstr"=>"Success.","success"=>1,"resultArr"=>$resultArr);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
	}
}
