<?php

/*
  @ Used: This class is used to manage Imunization
  @ Created By: Rachit Jain
  @ Created On: 10/1/15
  @ Updated On: 10/1/15
  @ Zend Framework (http://framework.zend.com/)
 */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\EventManager\EventManagerInterface;
use Zend\View\Model\JsonModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Mail;
use \Exception;

class LocationController extends AbstractRestfulController {

    public function __construct() {
        
    }

    public function init() {
        
    }

    public function getList() {
        
    }

    /**
     * Return single resource
     * @param mixed $id
     * @return mixed
     */
    public function get($id) {
        
    }

    /**
     * Create a new resource
     *
     * @param mixed $data
     * @return mixed
     */
    public function create($data) {
        //fetch array from json data
        $request = $this->request->getContent();
        $requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);
        //echo '<pre>';print_r($requestArr);exit;

        $method = $requestArr['method'];
        $service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
        if (isset($requestArr['params'])) {
            $params = $requestArr['params'];
            $this->params = $params;
        }

        switch ($method) {
            case 'acceptedgeofence':
                $responseArr = $this->acceptedgeofence($service_key);
                break;
            case 'requestedgeofence':
                $responseArr = $this->requestedgeofence($service_key);
                break;
            case 'updategeostatus':
                $responseArr = $this->updategeostatus($service_key);
                break;
            case 'updategeofencestatus':
                $responseArr = $this->updategeofencestatus($service_key);
                break;
            case 'deletegeofence':
                $responseArr = $this->deletegeofence($service_key);
                break;
            case 'addgeofencedata':
                $responseArr = $this->addgeofencedata($service_key);
                break;
            case 'getgeofencedata':
                $responseArr = $this->getgeofencedata($service_key);
                break;
            case 'getgeofencename':
                $responseArr = $this->getgeofencename($service_key);
                break;
            case 'activatebeacon':
                $responseArr = $this->activatebeacon($service_key);
                break;
            case 'updatebeacon':
                $responseArr = $this->updatebeacon($service_key);
                break;
            case 'beaconslist':
                $responseArr = $this->beaconslist($service_key);
                break;
            case 'updatebeaconstatus':
                $responseArr = $this->updatebeaconstatus($service_key);
                break;
            case 'deletebeacon':
                $responseArr = $this->deletebeacon($service_key);
                break;
            case 'assigncaregivertobeacon':
                $responseArr = $this->assigncaregivertobeacon($service_key);
                break;
            case 'getbeaconrules':
                $responseArr = $this->getbeaconrules($service_key);
                break;
            case 'addbeacondata':
                $responseArr = $this->addbeacondata($service_key);
                break;
            case 'getbeacondata':
                $responseArr = $this->getbeacondata($service_key);
                break;

            default:
                break;
        }

        echo json_encode($responseArr);
        exit;
    }

    /**
     * Update an existing resource
     *
     * @param mixed $id
     * @param mixed $data
     * @return mixed
     */
    public function update($id, $data) {
        
    }

    /**
     * Delete an existing resource
     *
     * @param  mixed $id
     * @return mixed
     */
    public function delete($id) {
        
    }

    /*
      @ : THis method is used to get accepted geofences
      @ : Created 10/03/15
     */

    public function acceptedgeofence($service_key) {
        $sucess = TRUE;
        $LocationObj = $this->getServiceLocator()->get("ServicesLocationTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $page = isset($data['page']) ? $data['page'] : '1';
            $maxLimit = 10;
            $lowerLimit = ($page - 1) * $maxLimit;

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID)) {
                throw new Exception("Required parameter missing.");
            }

            $whereArr = array('geo.UserID' => $UserID, 'geo.status' => 1);

            $totalCount = count($LocationObj->getGeofenceRequest($whereArr, array(), '', array(), '', ''));
            $totalpage = $page = '';
            $page = (int) ($totalCount / $maxLimit);
            if (($page * $maxLimit) < $totalCount) {
                $totalpage = $page + 1;
            } else {
                $totalpage = $page;
            }

            $GeofenceUsers = $LocationObj->getGeofenceRequest($whereArr, array(), '', array(), $lowerLimit, $maxLimit);

            $i = 0;
            foreach ($GeofenceUsers as $key1 => $val1) {
                //Get full image url
                $image = "";
                if ($val1['Image'] != "") {
                    $server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
                    $image = $server_url . "/upload_image/" . $val1['UserID'] . "/" . $val1['Image'];
                }
                $GeofenceUsers[$i]['Image'] = $image;
                $i++;
            }

            //Count Requests
            $wherArr1 = array('geo.UserID' => $UserID, 'geo.status' => 0);
            $GeofenceReq = $LocationObj->getGeofenceRequest($wherArr1);
            $requestCount = count($GeofenceReq);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "accept_geofence" => $GeofenceUsers, "requestCount" => $requestCount, "total" => $totalpage);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to get requested geofences
      @ : Created 10/03/15
     */

    public function requestedgeofence($service_key) {
        $sucess = TRUE;
        $LocationObj = $this->getServiceLocator()->get("ServicesLocationTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $page = isset($data['page']) ? $data['page'] : '1';
            $maxLimit = 10;
            $lowerLimit = ($page - 1) * $maxLimit;

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID)) {
                throw new Exception("Required parameter missing.");
            }

            $whereArr = array('geo.UserID' => $UserID, 'geo.status' => 0);

            $totalCount = count($LocationObj->getGeofenceRequest($whereArr, array(), '', array(), '', ''));
            $totalpage = $page = '';
            $page = (int) ($totalCount / $maxLimit);
            if (($page * $maxLimit) < $totalCount) {
                $totalpage = $page + 1;
            } else {
                $totalpage = $page;
            }

            $GeofenceReq = $LocationObj->getGeofenceRequest($whereArr, array(), '', array(), $lowerLimit, $maxLimit);

            $i = 0;
            foreach ($GeofenceReq as $key1 => $val1) {
                //Get full image url
                $image = "";
                if ($val1['Image'] != "") {
                    $server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
                    $image = $server_url . "/upload_image/" . $val1['UserID'] . "/" . $val1['Image'];
                }
                $GeofenceReq[$i]['Image'] = $image;
                $i++;
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "geofence_request" => $GeofenceReq, "total" => $totalpage);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to accept/reject the geofence reqeust
      @ : Created 15-04-2015
     */

    public function updategeostatus($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
        $CareGiverTableObj = $this->getServiceLocator()->get("ServicesCaregiverTable");
        $LocationObj = $this->getServiceLocator()->get("ServicesLocationTable");
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $geofence_id = isset($data['geofenceid']) ? $data['geofenceid'] : '';
            $status = isset($data['status']) ? $data['status'] : '';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID) || empty($geofence_id)) {
                throw new Exception("Required parameter missing");
            }

            $whereArr = array('id' => $geofence_id);
            $dataArr = array('status' => $status);

            if ($status == 1) {
                $LocationObj->updateGeofence($whereArr, $dataArr);

                //UserInfo
                $where = array("UserID" => $UserID);
                $columns = array('FirstName', 'LastName', 'Email');
                $userData = $IndUserTableObj->getUser($where, $columns);
                $UserName = $userData[0]['FirstName'] . ' ' . $userData[0]['LastName'];

                //CaregiverInfo
                $whereArr1 = array("geo.id" => $geofence_id);
                $caregiverData = $LocationObj->getGeofenceRequest($whereArr1);
                $CaregiverID = $caregiverData[0]['UserID'];
                $CaregiverName = $caregiverData[0]['FirstName'];
                $CaregiverEmail = $caregiverData[0]['Email'];

                //Save Notification
                $type = 'geofence_accept';
                $notDataArr = array("sender_id" => $UserID, "receiver_id" => $CaregiverID, "user_type" => 1, "created_date" => round(microtime(true) * 1000));
                $saveNoti = $NotificationTableObj->SaveNotification($notDataArr, $type);

                //Send Mail
                $message = "Dear $CaregiverName,<br>";
                $message .= "<br>$UserName has accepted your Geofence request.  You can easily manage 
these settings in the Locations section of your account on your web platform or mobile Q app.";
                $message .="<br><br>Here’s to your health,<br>The Quantextual Health Team";
                $subject = "Geofence Request Accepted";
                $plugin->senInvitationMail($CaregiverEmail, $subject, $message);
            } else {
                $LocationObj->deleteGeofecneRequest($whereArr);
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to udate geofence_status play/pause
      @ : Created 17/04/15
     */

    public function updategeofencestatus($service_key) {
        $sucess = TRUE;
        $LocationObj = $this->getServiceLocator()->get("ServicesLocationTable");

        try {
            $data = $this->params;
            $UserID = isset($data['user_id']) ? $data['user_id'] : '';
            $geofence_id = isset($data['geofence_id']) ? $data['geofence_id'] : '';
            $geofence_status = isset($data['geofence_status']) ? $data['geofence_status'] : '';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID) || empty($geofence_id)) {
                throw new Exception("Required parameter missing");
            }

            $whereArr = array("id" => $geofence_id);
            $dataArr = array('geofence_status' => $geofence_status);
            $LocationObj->updateGeofence($whereArr, $dataArr);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to delete geofence
      @ : Created 17/04/15
     */

    public function deletegeofence($service_key) {
        $sucess = TRUE;
        $LocationObj = $this->getServiceLocator()->get("ServicesLocationTable");

        try {
            $data = $this->params;
            $UserID = isset($data['user_id']) ? $data['user_id'] : '';
            $geofence_id = isset($data['geofence_id']) ? $data['geofence_id'] : '';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID) || empty($geofence_id)) {
                throw new Exception("Required parameter missing");
            }

            $whereArr = array("id" => $geofence_id);
            $LocationObj->deleteGeofecneRequest($whereArr);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to add geofence data
      @ : Created 17/04/15
     */

    public function addgeofencedata($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $GeofenceDataTableObj = $this->getServiceLocator()->get("ServicesGeofenceDataTable");
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");

        try {
            $data = $this->params;
            $UserID = isset($data['user_id']) ? $data['user_id'] : '';
            $time_zone = isset($data['time_zone']) ? $data['time_zone'] : 'ASIA/KOLKATA';

            $dataArr = $data['data'];

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID)) {
                throw new Exception("Required parameter missing.");
            }

            $where = array("UserID" => $UserID);
            $columns = array('FirstName', 'LastName', 'Email');
            $userData = $IndUserTableObj->getUser($where, $columns);
            $userName = $userData[0]['FirstName'] . ' ' . $userData[0]['LastName'];

            foreach ($dataArr as $key => $value) {
                $GeofenceID = isset($value['geofence_id']) ? $value['geofence_id'] : '';
                $notify = isset($value['notify']) ? $value['notify'] : '';
                $date = isset($value['date']) ? date("Y-m-d", strtotime($value['date'])) : date("Y-m-d");
                $time = isset($value['date']) ? date("H:i:s", strtotime($value['date'])) : date("H:i:s");
                $radius = isset($value['radius']) ? $value['radius'] : '';
                $creation_date = round(microtime(true) * 1000);
                
                //To send mail on local time
                date_default_timezone_set('UTC');
				$localdatetime = new \DateTime($value['date']);
				$localdatetime->setTimeZone(new \DateTimeZone($time_zone));
				$localdatetime->format("Y-m-d H:i:s");

                $localdate = $localdatetime->format("Y-m-d");
				$localtime = $localdatetime->format("H:i:s");
                
                $checkwhereArr = array('geofence_id' => $GeofenceID, 'geo.UserID' => $UserID, 'geo.notify' => $notify, 'date' => $date, 'time' => $time, 'geo.radius' => $radius);
                $UserGeofenceData = $GeofenceDataTableObj->getGeofenceData($checkwhereArr);

				if(!count($UserGeofenceData)){
					$dataArr = array('geofence_id' => $GeofenceID, 'UserID' => $UserID, 'notify' => $notify, 'date' => $date, 'time' => $time, 'radius' => $radius, 'creation_date' => $creation_date);
					$saveGeofenceDataId = $GeofenceDataTableObj->SaveGeofenceData($dataArr);

					$result = array();
					$whereGeoArr = array('gt.id' => $saveGeofenceDataId);
					$result = $GeofenceDataTableObj->getGeofenceDataRelatedCaregivers($whereGeoArr, array(), $localtime);
					//echo '<pre>';print_r($result);exit;

					foreach ($result as $k => $v) {
						$caregiverFirstName = $v['FirstName'];
						$caregiverLastName = $v['LastName'];
						$caregiverEmail = $v['Email'];
						$caregiverName = $caregiverFirstName;
						$geofence_name = $v['name'];
						if ($notify == 1) {
							$text = 'entered';
						} else {
							$text = 'exit from';
						}

						//Send Mail
						$message = "Dear $caregiverName,<br>";
						$message .= "<br>$userName has $text geofence $geofence_name";
						$message .="<br><br>Here’s to your health,<br>The Quantextual Health Team";
						$subject = "Geofence Data Notification";
						$plugin->senInvitationMail($caregiverEmail, $subject, $message);
					}
				}
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to get geofence data
      @ : Created 22/04/15
      @ : Rachit Jain
     */

    public function getgeofencedata($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $GeofenceDataTableObj = $this->getServiceLocator()->get("ServicesGeofenceDataTable");

        try {
            $data = $this->params;

            $UserID = isset($data['user_id']) ? $data['user_id'] : '';
            $date = isset($data['date']) ? date('Y-m-d', strtotime($data['date'])) : '';
            $time_zone = isset($data['time_zone']) ? $data['time_zone'] : 'ASIA/KOLKATA';
            $page = isset($data['page']) ? $data['page'] : '1';
            $maxLimit = 10;
            $lowerLimit = ($page - 1) * $maxLimit;

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($date) || empty($UserID)) {
                throw new Exception("Required parameter missing.");
            }

            //Get midnight Time of given timezone
            $today = (new \DateTime($date, new \DateTimeZone($time_zone)))->setTime(0, 0);
            $today->format('Y-m-d H:i:s');

            //Get UTC midnight time
            $today->setTimezone(new \DateTimeZone("UTC"));
            $start = $today->format('Y-m-d H:i:s');
            $end = date('Y-m-d H:i:s', strtotime($start . ' +23 hour'));

            $whereArr = array('start' => $start, 'end' => $end, 'geo.UserID' => $UserID);

            //$whereArr = array('geo.date'=>$date,'geo.UserID'=>$UserID);

            $totalCount = count($GeofenceDataTableObj->getGeofenceData($whereArr, array(), '', array(), '', ''));
            $totalpage = $page = '';
            $page = (int) ($totalCount / $maxLimit);
            if (($page * $maxLimit) < $totalCount) {
                $totalpage = $page + 1;
            } else {
                $totalpage = $page;
            }

            $UserGeofenceData = array();
            $UserGeofenceData = $GeofenceDataTableObj->getGeofenceData($whereArr, array(), '', array(), $lowerLimit, $maxLimit);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "usergeofencedata" => $UserGeofenceData, "total" => $totalpage);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to get geofence name
      @ : Created 20/04/15
     */

    public function getgeofencename($service_key) {
        $sucess = TRUE;
        $LocationObj = $this->getServiceLocator()->get("ServicesLocationTable");

        try {
            $data = $this->params;
            $UserID = isset($data['user_id']) ? $data['user_id'] : '';
            $geofence_id = isset($data['geofence_id']) ? $data['geofence_id'] : '';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID) || empty($geofence_id)) {
                throw new Exception("Required parameter missing.");
            }

            $whereArr = array('geo.UserID' => $UserID, 'geo.id' => $geofence_id);
            $columnArr = array('id', 'name', 'location');
            $GeofenceUsers = $LocationObj->getGeofenceRequest($whereArr, $columnArr);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "accept_geofence" => $GeofenceUsers);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to add becon
      @ : Created 06/04/15
      @ : Rachit Jain
     */

    public function activatebeacon($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $BeaconTableObj = $this->getServiceLocator()->get("ServicesBeconTable");
        //Organization Server API Key
        $ORGANIZATION_API_KEY = "d7984ad1144376fb9716f5e5a4c49e02";

        try {
            $data = $this->params;
            $user_id = isset($data['user_id']) ? $data['user_id'] : '';
            $beacon_name = isset($data['beacon_name']) ? $data['beacon_name'] : '';
            $factory_id = isset($data['factory_id']) ? $data['factory_id'] : '';
            $beacon_desc = isset($data['beacon_desc']) ? $data['beacon_desc'] : '';
            $latitude = isset($data['latitude']) ? $data['latitude'] : '28.6266410';
            $longitude = isset($data['longitude']) ? $data['longitude'] : '77.3848030';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $user_id, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($user_id) || empty($beacon_name) || empty($factory_id) || empty($beacon_desc)) {
                throw new Exception("Required parameter missing.");
            }

            $whereArr = array('bec.becon_title' => $beacon_name);
            $userBeacons = $BeaconTableObj->getBecons($whereArr);
            if (count($userBeacons)) {
                throw new Exception("This name is already used. Please select other name");
            }

            /*             * *********Activate Gimbal Start*********** */
            $url = "https://manager.gimbal.com/api/beacons";
            $fields = array('factory_id' => $factory_id,
                'name' => $beacon_name,
                'latitude' => $latitude,
                'longitude' => $longitude
				/* 	
					'latitude' => 32.7150,
					'longitude' => 117.1625
					'config_id' => 100,
					'visibility' => "private"
				*/                    
			);

            $regist = $plugin->curlPostBeacon($url, $fields);
            $regist = json_decode($regist);
            //echo '<pre>'; print_r($regist);exit;
            /*             * *********Activate Gimbal End************* */

            $beacon_id = isset($regist->id) ? $regist->id : '';
            $error_response_code = isset($regist->error->code) ? $regist->error->code : '';
            $error_response_message = isset($regist->error->message) ? $regist->error->message : '';

            $result = array();
            if ($beacon_id) {
                /*                 * *********Associate Gimbal with Place Start*********** */
                $url1 = "https://manager.gimbal.com/api/v2/places";
                $beaconArr = array('id' => $beacon_id, 'name' => $beacon_name);
                $fields1 = array('name' => $beacon_name,
                    'beacons' => array('0' => $beaconArr),
                );

                $regist1 = $plugin->curlPostBeacon($url1, $fields1);
                $regist1 = json_decode($regist1);
                //echo '<pre>'; print_r($regist1);exit;
                $place_id = isset($regist1->id) ? $regist1->id : '';
                /*                 * *********Associate Gimbal with Place End*********** */

                $result = $regist;

                $dataArr = array('UserID' => $user_id, 'becon_id' => $beacon_id, 'factory_id' => $factory_id, 'becon_title' => $beacon_name, 'becon_description' => $beacon_desc, 'place_id' => $place_id, 'creation_date' => round(microtime(true) * 1000), 'updation_date' => round(microtime(true) * 1000));
                $BeaconTableObj->SaveBecon($dataArr);
            } else {
                throw new Exception($error_response_message);
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "result" => $result);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis aciton is used to get Becons list
      @ : Created : 07/04/15
      @ : Rachit Jain
     */

    public function beaconslist($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $BeaconTableObj = $this->getServiceLocator()->get("ServicesBeconTable");
        $CareGiverTableObj = $this->getServiceLocator()->get("ServicesCaregiverTable");
        $AsscociatedBeaconTableObj = $this->getServiceLocator()->get("ServicesAssociatedBeaconTable");

        try {
            $data = $this->params;
            $UserID = isset($data['user_id']) ? $data['user_id'] : '';
            $page = isset($data['page']) ? $data['page'] : '1';
            $maxLimit = 10;
            $lowerLimit = ($page - 1) * $maxLimit;


            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID)) {
                throw new Exception("Required parameter missing.");
            }

            $wherArr = array('bec.UserID' => $UserID, 'bec.status' => 1);

            $totalCount = count($BeaconTableObj->getBecons($wherArr, array(), '', array(), '', ''));
            $totalpage = $page = '';
            $page = (int) ($totalCount / $maxLimit);
            if (($page * $maxLimit) < $totalCount) {
                $totalpage = $page + 1;
            } else {
                $totalpage = $page;
            }

            $userBeacons = array();
            $userBeacons = $BeaconTableObj->getBecons($wherArr, array(), '', array(), $lowerLimit, $maxLimit);
            //print_r($userBeacons);exit;

            $i = 0;
            foreach ($userBeacons as $key => $val) {
                $where1 = array('UserID' => $UserID, 'beacon_id' => $val['id']);
                $caregiverBeacon = $AsscociatedBeaconTableObj->getCaregiverBecons($where1);
                $userBeacons[$i]['total_caregiver'] = count($caregiverBeacon);
                $i++;
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "userbeacons" => $userBeacons, "total" => $totalpage);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to update becon
      @ : Created 13/04/15
      @ : Rachit Jain
     */

    //NOT IN USE IN PROJECT
    public function updatebeacon($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $BeaconTableObj = $this->getServiceLocator()->get("ServicesBeconTable");
        //Organization Server API Key
        $ORGANIZATION_API_KEY = "d7984ad1144376fb9716f5e5a4c49e02";

        try {
            $data = $this->params;
            $user_id = isset($data['user_id']) ? $data['user_id'] : '';
            $beacon_name = isset($data['beacon_name']) ? $data['beacon_name'] : '';
            $factory_id = isset($data['factory_id']) ? $data['factory_id'] : '';
            $beacon_desc = isset($data['beacon_desc']) ? $data['beacon_desc'] : '';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $user_id, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($user_id) || empty($beacon_name) || empty($factory_id) || empty($beacon_desc)) {
                throw new Exception("Required parameter missing.");
            }

            $url = "https://manager.gimbal.com/api/beacons/" . $factory_id;
            $fields = array('name' => $beacon_name/* ,
                      'latitude' => 32.7150,
                      'longitude' => 117.1625,
                      'config_id' => 100,
                      'visibility' => "private" */);

            $regist = $plugin->curlPostBeacon($url, $fields);
            $regist = json_decode($regist);
            //echo '<pre>'; print_r($regist);exit;

            $beacon_id = isset($regist->id) ? $regist->id : '';
            $error_response_code = isset($regist->error->code) ? $regist->error->code : '';
            $error_response_message = isset($regist->error->message) ? $regist->error->message : '';

            $result = array();
            if ($beacon_id) {
                $result = $regist;

                $dataArr = array('UserID' => $user_id, 'becon_id' => $beacon_id, 'factory_id' => $factory_id, 'becon_title' => $beacon_name, 'becon_description' => $beacon_desc, 'creation_date' => round(microtime(true) * 1000), 'updation_date' => round(microtime(true) * 1000));
                $BeaconTableObj->SaveBecon($dataArr);
            } else {
                throw new Exception($error_response_message);
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "result" => $result);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to udate becon status play/pause
      @ : Created 06/04/15
      @ : Rachit Jain
     */

    public function updatebeaconstatus($service_key) {
        $sucess = TRUE;
        $BeaconTableObj = $this->getServiceLocator()->get("ServicesBeconTable");

        try {
            $data = $this->params;
            $UserID = isset($data['user_id']) ? $data['user_id'] : '';
            $BeaconID = isset($data['beacon_id']) ? $data['beacon_id'] : '';
            $becon_status = isset($data['beacon_status']) ? $data['beacon_status'] : '';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID) || empty($BeaconID)) {
                throw new Exception("Required parameter missing.");
            }

            $whereArr = array('UserID' => $UserID, 'id' => $BeaconID);
            $dataArr = array('becon_status' => $becon_status);
            $BeaconTableObj->updateBecon($whereArr, $dataArr);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to delete becon
      @ : Created 08/04/15
      @ : Rachit Jain
     */

    public function deletebeacon($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $BeaconTableObj = $this->getServiceLocator()->get("ServicesBeconTable");

        try {
            $data = $this->params;
            $UserID = isset($data['user_id']) ? $data['user_id'] : '';
            $BeaconID = isset($data['beacon_id']) ? $data['beacon_id'] : '';
            $FactoryId = isset($data['factory_id']) ? $data['factory_id'] : '';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID) || empty($BeaconID)) {
                throw new Exception("Required parameter missing.");
            }

            //Deactivate Beacon From Gimbal
            $url = "https://manager.gimbal.com/api/beacons/" . $FactoryId;
            $res = $plugin->curlDeleteBeacon($url);

            if (!$res) {
                $whereArr = array('bec.id' => $BeaconID);
                $userBeacons = $BeaconTableObj->getBecons($whereArr, array('place_id'));
                $place_id = $userBeacons[0]['place_id'];
                //Delete Place From Gimbal
                $url1 = "https://manager.gimbal.com/api/v2/places/" . $place_id;
                $res1 = $plugin->curlDeleteBeacon($url1);

                $whereArr = array('UserID' => $UserID, 'id' => $BeaconID);
                $BeaconTableObj->deleteBecon($whereArr);
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to assign beacon to caregiver
      @ : Created 08/04/15
      @ : Rachit Jain
     */

    public function assigncaregivertobeacon($service_key) {
        $sucess = TRUE;
        $BeaconTableObj = $this->getServiceLocator()->get("ServicesAssociatedBeaconTable");
        $CareGiverTableObj = $this->getServiceLocator()->get("ServicesCaregiverTable");
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");

        try {
            $data = $this->params;
            $UserID = isset($data['user_id']) ? $data['user_id'] : '';
            $CaregiverID = isset($data['caregiver_id']) ? $data['caregiver_id'] : '';
            $beaconId = isset($data['beacon_id']) ? $data['beacon_id'] : '';
            $DeassociatedID = isset($data['deassociated_id']) ? $data['deassociated_id'] : '';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID) || empty($beaconId)) {
                throw new Exception("Required parameter missing.");
            }

            $whereArr1 = array('UserID' => $UserID, 'beacon_id' => $beaconId);
            $delete = $BeaconTableObj->deleteCaregiverBecon($whereArr1);

            if (!empty($CaregiverID)) {
                $caregiverarr = explode(',', $CaregiverID);
                foreach ($caregiverarr as $key => $val) {
                    if ($val != '') {
                        //check caregiver
                        $CareGiveRes = array();
                        $whereArr = array('care.UserID' => $UserID, 'care.caregiver_id' => $val, 'care.status' => 1);
                        $CareGiveRes = $CareGiverTableObj->getCareGiver($whereArr);
                        if (!count($CareGiveRes)) {
                            throw new Exception("User Id " . $val . "is not your caregiver");
                        }

                        $dataArr = array('UserID' => $UserID, 'beacon_id' => $beaconId, 'caregiver_id' => $val, 'creation_date' => round(microtime(true) * 1000), 'updation_date' => round(microtime(true) * 1000));
                        $saveBeaconId = $BeaconTableObj->SaveCaregiverBecon($dataArr);

                        //Save Notification
                        $type = 'beacon_associate';
                        $notDataArr = array("sender_id" => $UserID, "receiver_id" => $val, "user_type" => 1, "created_date" => round(microtime(true) * 1000));
                        $saveNoti = $NotificationTableObj->SaveNotification($notDataArr, $type);
                    }
                }
            }

            if (!empty($DeassociatedID)) {
                $deassociatearr = explode(',', $DeassociatedID);
                foreach ($deassociatearr as $key1 => $val1) {
                    if ($val1 != '') {
                        //Save Notification
                        $type = 'beacon_deassociate';
                        $notDataArr = array("sender_id" => $UserID, "receiver_id" => $val1, "user_type" => 1, "created_date" => round(microtime(true) * 1000));
                        $saveNoti = $NotificationTableObj->SaveNotification($notDataArr, $type);
                    }
                }
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to get beacon rules
      @ : Created 10/04/15
      @ : Rachit Jain
     */

    public function getbeaconrules($service_key) {
        $sucess = TRUE;
        $BeaconRuleTableObj = $this->getServiceLocator()->get("ServicesBeaconRulesTable");

        try {
            $data = $this->params;
            $UserID = isset($data['user_id']) ? $data['user_id'] : '';
            $BeaconID = isset($data['beacon_id']) ? $data['beacon_id'] : '';
            $page = isset($data['page']) ? $data['page'] : '1';
            $maxLimit = 10;
            $lowerLimit = ($page - 1) * $maxLimit;

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID) || empty($BeaconID)) {
                throw new Exception("Required parameter missing.");
            }

            $whereArr = array('bec.UserID' => $UserID, 'bec.beacon_id' => $BeaconID);

            $totalCount = count($BeaconRuleTableObj->getBeaconRule($whereArr, array(), '', array(), '', ''));
            $totalpage = $page = '';
            $page = (int) ($totalCount / $maxLimit);
            if (($page * $maxLimit) < $totalCount) {
                $totalpage = $page + 1;
            } else {
                $totalpage = $page;
            }

            $userBeaconRules = array();
            $userBeaconRules = $BeaconRuleTableObj->getBeaconRule($whereArr, array(), '', array(), $lowerLimit, $maxLimit);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "userbeaconrules" => $userBeaconRules, "total" => $totalpage);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to add becon data
      @ : Created 10/04/15
      @ : Rachit Jain
     */

    public function addbeacondata($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
        $BeaconTableObj = $this->getServiceLocator()->get("ServicesBeconTable");
        $BeaconDataTableObj = $this->getServiceLocator()->get("ServicesBeaconDataTable");

        try {
            $data = $this->params;
            $UserID = isset($data['user_id']) ? $data['user_id'] : '';
            $time_zone = isset($data['time_zone']) ? $data['time_zone'] : 'ASIA/KOLKATA';
            $dataArr = $data['data'];

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID)) {
                throw new Exception("Required parameter missing.");
            }

            $where = array("UserID" => $UserID);
            $columns = array('FirstName', 'LastName', 'Email');
            $userData = $IndUserTableObj->getUser($where, $columns);
            $userName = $userData[0]['FirstName'] . ' ' . $userData[0]['LastName'];

            //To Check which beacon_id is getting blank from front end
            /* $emptyValues = array();
              foreach($dataArr as $key1=>$value1){
              if(!empty($value1['beacon_id'])){
              $emptyValues[] = $value1['beacon_id'];
              }
              }
              print_r($emptyValues);exit;
             */

            foreach ($dataArr as $key => $value) {
                //$FactoryID = isset($value['beacon_id']) ? $value['beacon_id'] : '';
                $PlaceID = isset($value['place_id']) ? $value['place_id'] : '';
                $Rule = isset($value['rule']) ? $value['rule'] : '';
                $date = isset($value['date']) ? date("Y-m-d", strtotime($value['date'])) : date("Y-m-d");
                $time = isset($value['date']) ? date("H:i:s", strtotime($value['date'])) : date("H:i:s");
                //$rss = isset($value['rss']) ? $value['rss'] : '';
                //$temp = isset($value['temp']) ? $value['temp'] : '';
                //$battery = isset($value['battery']) ? $value['battery'] : '';
                //$radius = isset($value['radius']) ? $value['radius'] : '';
                $creation_date = round(microtime(true) * 1000);
                
                //To send mail on local time
                date_default_timezone_set('UTC');
				$localdatetime = new \DateTime($value['date']);
				$localdatetime->setTimeZone(new \DateTimeZone($time_zone));
				$localdatetime->format("Y-m-d H:i:s");

                $localdate = $localdatetime->format("Y-m-d");
				$localtime = $localdatetime->format("H:i:s");

                //$whereArr = array('bec.factory_id'=>$FactoryID);
                $whereArr = array('bec.place_id' => $PlaceID);
                $userBeacons = $BeaconTableObj->getBecons($whereArr);

                if (count($userBeacons)) {
                    $BeaconID = $userBeacons[0]['id'];
                    $BeaconName = $userBeacons[0]['becon_title'];

                    $whereBecArr = array('beacon_id' => $BeaconID, 'UserID' => $UserID, 'rule' => $Rule, 'date' => $date, 'time' => $time);
                    $checkBeaconData = $BeaconDataTableObj->checkBeaconData($whereBecArr);
                    //print_r($checkBeaconData);exit;

					if(!count($checkBeaconData)){
						$dataArr = array('beacon_id' => $BeaconID, 'UserID' => $UserID, 'rule' => $Rule, 'date' => $date, 'time' => $time, 'creation_date' => $creation_date);
						$saveBeaconDataId = $BeaconDataTableObj->SaveBeaconData($dataArr);

						$result = array();
						$whereBecon = array('bec.beacon_id' => $BeaconID);
						$result = $BeaconDataTableObj->getBeaconDataRelatedCaregivers($whereBecon, array(), $localtime);
						//echo '<pre>';print_r($result);exit;

						if ($Rule == 1) {
							$text = 'entered the';
						} else {
							$text = 'exit from';
						}

						foreach ($result as $k => $v) {
							$caregiverFirstName = $v['FirstName'];
							$caregiverLastName = $v['LastName'];
							$caregiverEmail = $v['Email'];
							$caregiverName = $caregiverFirstName;

							//Send Mail
							$message = "Dear $caregiverName,<br>";
							$message .= "<br>$userName has $text $BeaconName";
							$message .="<br><br>Thank you<br>";
							$message .="<br><br>Regards,<br>Quantextual Health Team";
							$subject = "Beacon Data Notification";
							$plugin->senInvitationMail($caregiverEmail, $subject, $message);
						}
					}
                }
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ : THis method is used to get becon data
      @ : Created 17/04/15
      @ : Rachit Jain
     */

    public function getbeacondata($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $BeaconTableObj = $this->getServiceLocator()->get("ServicesBeaconDataTable");

        try {
            $data = $this->params;

            //$BeaconID = isset($data['beacon_id']) ? $data['beacon_id'] : '';
            $UserID = isset($data['user_id']) ? $data['user_id'] : '';
            $date = isset($data['date']) ? date('Y-m-d', strtotime($data['date'])) : '';
            $time_zone = isset($data['time_zone']) ? $data['time_zone'] : 'ASIA/KOLKATA';
            $page = isset($data['page']) ? $data['page'] : '1';
            $maxLimit = 10;
            $lowerLimit = ($page - 1) * $maxLimit;

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($date) || empty($UserID)) {
                throw new Exception("Required parameter missing.");
            }

            //Get midnight Time of given timezone
            $today = (new \DateTime($date, new \DateTimeZone($time_zone)))->setTime(0, 0);
            $today->format('Y-m-d H:i:s');

            //Get UTC midnight time
            $today->setTimezone(new \DateTimeZone("UTC"));
            $start = $today->format('Y-m-d H:i:s');
            $end = date('Y-m-d H:i:s', strtotime($start . ' +23 hour'));

            $whereArr = array('start' => $start, 'end' => $end, 'bec.UserID' => $UserID);

            //$whereArr = array('bec.date'=>$date,'bec.UserID'=>$UserID);

            $totalCount = count($BeaconTableObj->getBeaconData($whereArr, array(), '', array(), '', ''));
            $totalpage = $page = '';
            $page = (int) ($totalCount / $maxLimit);
            if (($page * $maxLimit) < $totalCount) {
                $totalpage = $page + 1;
            } else {
                $totalpage = $page;
            }

            $UserBeaconData = array();
            $UserBeaconData = $BeaconTableObj->getBeaconData($whereArr, array(), '', array(), $lowerLimit, $maxLimit);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "userbeacondata" => $UserBeaconData, "total" => $totalpage);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

}

?>
