<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE Device FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain
 * CREATOR: 23/03/2015.
 * UPDATED:  .
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use \Exception;

class FitnessController extends AbstractRestfulController {

    public function __construct() {
	
    }


	public function getList(){
		
    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id){

	}

	/**
	 * Create a new resource
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);

		//echo '<pre>';print_r($requestArr);exit;
		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'getenddate':
				$responseArr = $this->getenddate($service_key);
				break;
			case 'addgooglefitdata':
				$responseArr = $this->addgooglefitdata($service_key);
				break;
			case 'addhealthkitdata':
				$responseArr = $this->addhealthkitdata($service_key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}

	/**
	 * Update an existing resource
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {
	
	}


	/**
	 * Delete an existing resource
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}


	public function getenddate($service_key){
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");

		try{
			$data = $this->params;
			
			$UserId = isset($data['user_id']) ? $data['user_id'] : '';
			$devicetype = isset($data['device_type']) ? $data['device_type'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			$whereUserAr = array("UserID" => $UserId);
			$coloumnarray = array("UserID", "DeviceType", "DeviceToken", "google_end_date", "healthkit_end_date");
			$user_details = $IndUserTableObj->getUser($whereUserAr, $coloumnarray);
			
			//$end_date =  date("Y-m-d H:i:s");

			if($devicetype == 'android'){
				$end_date = (empty($user_details[0]['google_end_date']) ? '' : date("Y-m-d H:i:s",strtotime($user_details[0]['google_end_date'])));
				//$end_date = (!empty($user_details[0]['google_end_date']) ? date("Y-m-d H:i:s") : date("Y-m-d H:i:s",strtotime($user_details[0]['google_end_date'])));
			}
			else{
				$end_date = (empty($user_details[0]['healthkit_end_date']) ? date("Y-m-d H:i:s") : date("Y-m-d H:i:s",strtotime($user_details[0]['healthkit_end_date'])));
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"end_date"=>$end_date);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
	}


    public function addgooglefitdata($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$IndHeightTableObj = $this->getServiceLocator()->get("ServicesHeightRecordTable");
		$IndWeightTableObj = $this->getServiceLocator()->get("ServicesWeightRecordTable");
		$IndWaistTableObj = $this->getServiceLocator()->get("ServicesWaistRecordTable");
		$IndBmiTableObj = $this->getServiceLocator()->get("ServicesBmiRecordTable");
		$IndBpTableObj = $this->getServiceLocator()->get('ServicesBloodPressureRecordTable');
		$TempTableObj = $this->getServiceLocator()->get('ServicesTemperatureRecordTable');
		$SleepTableObj = $this->getServiceLocator()->get('ServicesSleepRecordTable');
		$IndPhyTableObj = $this->getServiceLocator()->get("ServicesPhysicalActivityTable");
		$IndChoTableObj = $this->getServiceLocator()->get("ServicesCholesterolRecordTable");
		$IndBgTableObj = $this->getServiceLocator()->get('ServicesBloodGlucoseRecordTable');
		$IndFoodDrinkTblObj = $this->getServiceLocator()->get("ServicesFoodDrinkTable");
		$IndBiometricTblObj = $this->getServiceLocator()->get("ServicesBiometricsRecordTable");

		try{
			$data = $this->params;

			$UserId = isset($data['user_id']) ? $data['user_id'] : '';
			$date = isset($data['end_date']) ? date("c", strtotime($data['end_date'])) : date("c");
			$source = 'googlefit';
			$dataArr  = isset($data['dataArr']) ? $data['dataArr'] : array();

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserId) || empty($date)){
				throw new Exception("Required parameter missing.");
			}
			
			$coloumUpdate = array('google_end_date' => $date);
			$whereUserAr = array("UserID" => $UserId);
			$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);

			if(count($dataArr)){
				$i=0;
				foreach($dataArr as $key=>$value){
					if(count($value['data_point'])){
						$i = $i + count($value['data_point']);
					}
				}

				if($i == 0){
					throw new Exception("No data Found");
				}

				$steps = $distance = $weight = $calories = '';

				foreach($dataArr as $key=>$value){
					if($value['name'] == 'com.google.step_count.delta'){
						foreach($value['data_point'] as $k=>$v){
							$steps = isset($v['fields']['steps']) ? $v['fields']['steps'] : '';
							$end_time = !empty($v['end_time']) ? date("c", strtotime($v['end_time'])) :date("c");
							//echo $end_time;exit;
							$googlefitSaveArr = array();

							//Google Fit Data
							$googlefitSaveArr['UserID'] = $UserId;
							$google_end_date = date("Y-m-d", strtotime($end_time));
							$google_end_time = date("H:i:s", strtotime($end_time));
							$googlefitSaveArr['ExerciseRecorddate'] = $google_end_date;
							$googlefitSaveArr['ExerciseRecordtime'] = $google_end_time;
							$googlefitSaveArr['NumberOfSteps'] = $steps;
							$googlefitSaveArr['source'] = $source;
							$googlefitSaveArr['CreationDate'] = round(microtime(true) * 1000);

							$insdata = $IndPhyTableObj->insertPhysicalActivity($googlefitSaveArr);

							/*if($insdata){
								$coloumUpdate = array('google_end_date' => $end_time);
								$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
							}*/
						}
					}

					//Distance Data
					if($value['name'] == 'com.google.distance.delta'){
						foreach($value['data_point'] as $k=>$v){
							$distance = isset($v['fields']['distance']) ? $v['fields']['distance'] : '';
							$end_time = !empty($v['end_time']) ? date("c", strtotime($v['end_time'])) : date("c");

							$googlefitSaveArr = array();

							//Google Fit Data
							$googlefitSaveArr['UserID'] = $UserId;
							$google_end_date = date("Y-m-d", strtotime($end_time));
							$google_end_time = date("H:i:s", strtotime($end_time));
							$googlefitSaveArr['ExerciseRecorddate'] = $google_end_date;
							$googlefitSaveArr['ExerciseRecordtime'] = $google_end_time;
							$googlefitSaveArr['Distance'] = $distance;
							$googlefitSaveArr['source'] = $source;
							$googlefitSaveArr['CreationDate'] = round(microtime(true) * 1000);

							$insdata = $IndPhyTableObj->insertPhysicalActivity($googlefitSaveArr);

							/*if($insdata){
								$coloumUpdate = array('google_end_date' => $end_time);
								$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
							}*/
						}
					}

					//Weight Data
					if($value['name'] == 'com.google.weight'){
						foreach($value['data_point'] as $k=>$v){
							$weight = isset($v['fields']['weight']) ? $v['fields']['weight'] : '';
							$end_time = !empty($v['end_time']) ? date("c", strtotime($v['end_time'])) : date("c");

							$googlefitSaveArr = array();

							//Google Fit Data
							$googlefitSaveArr['UserID'] = $UserId;
							$google_end_date = date("Y-m-d", strtotime($end_time));
							$google_end_time = date("H:i:s", strtotime($end_time));
							$googlefitSaveArr['WeightRecordDate'] = $google_end_date;
							$googlefitSaveArr['WeightRecordTime'] = $google_end_time;
							$googlefitSaveArr['Weight'] = $weight * 1000;
							$googlefitSaveArr['source'] = $source;
							$googlefitSaveArr['CreationDate'] = round(microtime(true) * 1000);

							$WeightData = $IndWeightTableObj->insertWeightRecord($googlefitSaveArr);

							/*if($WeightData){
								$coloumUpdate = array('google_end_date' => $end_time);
								$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
							}*/
						}
					}
					
					//Height Data
					if($value['name'] == 'com.google.height'){
						foreach($value['data_point'] as $k=>$v){
							$height = isset($v['fields']['height']) ? $v['fields']['height'] : '';
							$end_time = !empty($v['end_time']) ? date("c", strtotime($v['end_time'])) : date("c");

							$googlefitSaveArr = array();

							//Google Fit Data
							$googlefitSaveArr['UserID'] = $UserId;
							$google_end_date = date("Y-m-d", strtotime($end_time));
							$google_end_time = date("H:i:s", strtotime($end_time));
							$googlefitSaveArr['HeightRecordDate'] = $google_end_date;
							$googlefitSaveArr['HeightRecordTime'] = $google_end_time;
							$googlefitSaveArr['Height'] = $height * 100;
							$googlefitSaveArr['source'] = $source;
							$googlefitSaveArr['CreationDate'] = round(microtime(true) * 1000);

							$HeightData = $IndHeightTableObj->insertHeightRecord($googlefitSaveArr);

							/*if($HeightData){
								$coloumUpdate = array('google_end_date' => $end_time);
								$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
							}*/
						}
					}

					//Calories Expended Data
					if($value['name'] == 'com.google.calories.expended'){
						foreach($value['data_point'] as $k=>$v){
							$calories = isset($v['fields']['calories']) ? $v['fields']['calories'] : '';
							$end_time = !empty($v['end_time']) ? date("c", strtotime($v['end_time'])) : date("c");

							$googlefitSaveArr = array();

							//Google Fit Data
							$googlefitSaveArr['UserID'] = $UserId;
							$google_end_date = date("Y-m-d", strtotime($end_time));
							$google_end_time = date("H:i:s", strtotime($end_time));
							$googlefitSaveArr['ExerciseRecorddate'] = $google_end_date;
							$googlefitSaveArr['ExerciseRecordtime'] = $google_end_time;
							$googlefitSaveArr['CaloriesBurned'] = $calories * 1000;
							$googlefitSaveArr['source'] = $source;
							$googlefitSaveArr['CreationDate'] = round(microtime(true) * 1000);

							$insdata = $IndPhyTableObj->insertPhysicalActivity($googlefitSaveArr);

							/*if($insdata){
								$coloumUpdate = array('google_end_date' => $end_time);
								$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
							}*/
						}
					}
					
					//Pulse Data
					if($value['name'] == 'com.google.heart_rate.bpm'){
						foreach($value['data_point'] as $k=>$v){
							$bpm = isset($v['fields']['bpm']) ? $v['fields']['bpm'] : '';
							$end_time = !empty($v['end_time']) ? date("c", strtotime($v['end_time'])) : date("c");

							$googlefitSaveArr = array();

							//Google Fit Data
							$googlefitSaveArr['UserID'] = $UserId;
							$google_end_date = date("Y-m-d", strtotime($end_time));
							$google_end_time = date("H:i:s", strtotime($end_time));
							$googlefitSaveArr['bp_record_date'] = $google_end_date;
							$googlefitSaveArr['bp_record_time'] = $google_end_time;
							$googlefitSaveArr['pulse'] = $bpm;
							$googlefitSaveArr['source'] = $source;
							$googlefitSaveArr['creation_date'] = round(microtime(true) * 1000);

							$insdata = $IndBpTableObj->insertBpRecord($googlefitSaveArr);

							/*if($insdata){
								$coloumUpdate = array('google_end_date' => $end_time);
								$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
							}*/
						}
					}
				}

			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


    public function addhealthkitdata($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$IndHeightTableObj = $this->getServiceLocator()->get("ServicesHeightRecordTable");
		$IndWeightTableObj = $this->getServiceLocator()->get("ServicesWeightRecordTable");
		$IndWaistTableObj = $this->getServiceLocator()->get("ServicesWaistRecordTable");
		$IndBmiTableObj = $this->getServiceLocator()->get("ServicesBmiRecordTable");
		$IndBpTableObj = $this->getServiceLocator()->get('ServicesBloodPressureRecordTable');
		$TempTableObj = $this->getServiceLocator()->get('ServicesTemperatureRecordTable');
		$SleepTableObj = $this->getServiceLocator()->get('ServicesSleepRecordTable');
		$IndPhyTableObj = $this->getServiceLocator()->get("ServicesPhysicalActivityTable");
		$IndChoTableObj = $this->getServiceLocator()->get("ServicesCholesterolRecordTable");
		$IndBgTableObj = $this->getServiceLocator()->get('ServicesBloodGlucoseRecordTable');
		$IndFoodDrinkTblObj = $this->getServiceLocator()->get("ServicesFoodDrinkTable");
		$IndBiometricTblObj = $this->getServiceLocator()->get("ServicesBiometricsRecordTable");

		try{
			$data = $this->params;

			$UserId = isset($data['user_id']) ? $data['user_id'] : '';
			$source = 'healthkit';
			$dataArr  = $data['dataArr'];

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			$height = $weight = $bmi = $steps = $distance = $calorie = $systolic = $diastolic = $pulse = $temperature = $end_date = '';
			$whereUserAr = array("UserID" => $UserId);

			foreach($dataArr as $key=>$value){

				// Height Data
				if($value['type'] == 'height'){
					$height = isset($value['value']) ? $value['value'] : ''; //in meter
					$heightincm = $height*100;
					$end_date = !empty($value['date']) ? date("c", strtotime($value['date'])) : date("c");

					$healthkitSaveArr = array();

					$healthkitSaveArr['UserID'] = $UserId;
					$healthkit_end_date = date("Y-m-d", strtotime($end_date));
					$healthkit_end_time = date("H:i:s", strtotime($end_date));
					$healthkitSaveArr['HeightRecordDate'] = $healthkit_end_date;
					$healthkitSaveArr['HeightRecordTime'] = $healthkit_end_time;
					$healthkitSaveArr['Height'] = $heightincm;
					$healthkitSaveArr['Unit'] = 'cm';
					$healthkitSaveArr['source'] = $source;
					$healthkitSaveArr['CreationDate'] = round(microtime(true) * 1000);
					
					$heightArr = array('UserID'=>$UserId, 'HeightRecordDate'=>$healthkit_end_date, 'HeightRecordTime'=>$healthkit_end_time, 'Height'=>$heightincm, 'hr.source'=>$source);
					$checkHeight = $IndHeightTableObj->getHeightRecord($heightArr);

					if(!count($checkHeight)){
						$HeightData = $IndHeightTableObj->insertHeightRecord($healthkitSaveArr);

						if($HeightData){
							$coloumUpdate = array('healthkit_end_date' => $end_date);
							$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
						}
					}
				}
				
				// Weight Data
				if($value['type'] == 'weight'){
					$weight = isset($value['value']) ? $value['value'] : ''; //in grams
					$end_date = !empty($value['date']) ? date("c", strtotime($value['date'])) : date("c");

					$healthkitSaveArr = array();

					$healthkitSaveArr['UserID'] = $UserId;
					$healthkit_end_date = date("Y-m-d", strtotime($end_date));
					$healthkit_end_time = date("H:i:s", strtotime($end_date));
					$healthkitSaveArr['WeightRecordDate'] = $healthkit_end_date;
					$healthkitSaveArr['WeightRecordTime'] = $healthkit_end_time;
					$healthkitSaveArr['Weight'] = $weight;
					$healthkitSaveArr['source'] = $source;
					$healthkitSaveArr['CreationDate'] = round(microtime(true) * 1000);
					
					$weightArr = array('UserID'=>$UserId, 'WeightRecordDate'=>$healthkit_end_date, 'WeightRecordTime'=>$healthkit_end_time, 'Weight'=>$weight, 'wr.source'=>$source);
					$checkWeight = $IndWeightTableObj->getWeightRecord($weightArr);
					
					if(!count($checkWeight)){
						$WeightData = $IndWeightTableObj->insertWeightRecord($healthkitSaveArr);

						if($WeightData){
							$coloumUpdate = array('healthkit_end_date' => $end_date);
							$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
						}
					}
				}

				// BMI Data
				if($value['type'] == 'bmi'){
					$bmi = isset($value['value']) ? $value['value'] : ''; //in kg/m2
					$end_date = !empty($value['date']) ? date("c", strtotime($value['date'])) : date("c");

					$healthkitSaveArr = array();

					$healthkitSaveArr['UserID'] = $UserId;
					$healthkit_end_date = date("Y-m-d", strtotime($end_date));
					$healthkit_end_time = date("H:i:s", strtotime($end_date));
					$healthkitSaveArr['bmi_record_date'] = $healthkit_end_date;
					$healthkitSaveArr['bmi_record_time'] = $healthkit_end_time;
					$healthkitSaveArr['bmi'] = $bmi;
					$healthkitSaveArr['source'] = $source;
					$healthkitSaveArr['creation_date'] = round(microtime(true) * 1000);

					$bmiArr = array('UserID'=>$UserId, 'bmi_record_date'=>$healthkit_end_date, 'bmi_record_time'=>$healthkit_end_time, 'bmi'=>$bmi, 'bmi.source'=>$source);
					$checkbmi = $IndBmiTableObj->getBmiRecord($bmiArr);
					
					if(!count($checkbmi)){
						$insbmiData = $IndBmiTableObj->insertBmiRecord($healthkitSaveArr);

						if($insbmiData){
							$coloumUpdate = array('healthkit_end_date' => $end_date);
							$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
						}
					}
				}

				// Step Data
				if($value['type'] == 'step'){
					$steps = isset($value['value']) ? $value['value'] : '';
					$end_date = !empty($value['date']) ? date("c", strtotime($value['date'])) : date("c");

					$healthkitSaveArr = array();

					$healthkitSaveArr['UserID'] = $UserId;
					$healthkit_end_date = date("Y-m-d", strtotime($end_date));
					$healthkit_end_time = date("H:i:s", strtotime($end_date));
					$healthkitSaveArr['ExerciseRecorddate'] = $healthkit_end_date;
					$healthkitSaveArr['ExerciseRecordtime'] = $healthkit_end_time;
					$healthkitSaveArr['NumberOfSteps'] = $steps;
					$healthkitSaveArr['source'] = $source;
					$healthkitSaveArr['CreationDate'] = round(microtime(true) * 1000);

					$stepsArr = array('UserID'=>$UserId, 'ExerciseRecorddate'=>$healthkit_end_date, 'ExerciseRecordtime'=>$healthkit_end_time, 'NumberOfSteps'=>$steps, 'pa.source'=>$source);
					$checkSteps = $IndPhyTableObj->getPhysicalActivityRecord($stepsArr);
					
					if(!count($checkSteps)){
						$insdata = $IndPhyTableObj->insertPhysicalActivity($healthkitSaveArr);

						if($insdata){
							$coloumUpdate = array('healthkit_end_date' => $end_date);
							$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
						}
					}
				}

				// Distance Data
				if($value['type'] == 'distance'){
					$distance = isset($value['value']) ? $value['value'] : ''; //in meter
					$end_date = !empty($value['date']) ? date("c", strtotime($value['date'])) : date("c");

					$healthkitSaveArr = array();

					$healthkitSaveArr['UserID'] = $UserId;
					$healthkit_end_date = date("Y-m-d", strtotime($end_date));
					$healthkit_end_time = date("H:i:s", strtotime($end_date));
					$healthkitSaveArr['ExerciseRecorddate'] = $healthkit_end_date;
					$healthkitSaveArr['ExerciseRecordtime'] = $healthkit_end_time;
					$healthkitSaveArr['Distance'] = $distance;
					$healthkitSaveArr['source'] = $source;
					$healthkitSaveArr['CreationDate'] = round(microtime(true) * 1000);

					$ditanceArr = array('UserID'=>$UserId, 'ExerciseRecorddate'=>$healthkit_end_date, 'ExerciseRecordtime'=>$healthkit_end_time, 'Distance'=>$distance, 'pa.source'=>$source);
					$checkDistance = $IndPhyTableObj->getPhysicalActivityRecord($ditanceArr);
					
					if(!count($checkDistance)){
						$insdata = $IndPhyTableObj->insertPhysicalActivity($healthkitSaveArr);

						if($insdata){
							$coloumUpdate = array('healthkit_end_date' => $end_date);
							$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
						}
					}
				}
				
				// Calories Data
				if($value['type'] == 'calories'){
					$calorie = isset($value['value']) ? $value['value'] : ''; //in calorie
					$end_date = !empty($value['date']) ? date("c", strtotime($value['date'])) : date("c");

					$healthkitSaveArr = array();

					$healthkitSaveArr['UserID'] = $UserId;
					$healthkit_end_date = date("Y-m-d", strtotime($end_date));
					$healthkit_end_time = date("H:i:s", strtotime($end_date));
					$healthkitSaveArr['ExerciseRecorddate'] = $healthkit_end_date;
					$healthkitSaveArr['ExerciseRecordtime'] = $healthkit_end_time;
					$healthkitSaveArr['CaloriesBurned'] = $calorie;
					$healthkitSaveArr['source'] = $source;
					$healthkitSaveArr['CreationDate'] = round(microtime(true) * 1000);

					$caloriesArr = array('UserID'=>$UserId, 'ExerciseRecorddate'=>$healthkit_end_date, 'ExerciseRecordtime'=>$healthkit_end_time, 'CaloriesBurned'=>$calorie, 'pa.source'=>$source);
					$checkCalories = $IndPhyTableObj->getPhysicalActivityRecord($caloriesArr);
					
					if(!count($checkCalories)){
						$insdata = $IndPhyTableObj->insertPhysicalActivity($healthkitSaveArr);

						if($insdata){
							$coloumUpdate = array('healthkit_end_date' => $end_date);
							$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
						}
					}
				}
				
				// Bloodpressure Data
				if($value['type'] == 'bloodpressure'){
					//in mmHg
					$systolic = isset($value['systolicvalue']) ? $value['systolicvalue'] : '';
					$diastolic = isset($value['diastolicvalue']) ? $value['diastolicvalue'] : '';
					$end_date = !empty($value['date']) ? date("c", strtotime($value['date'])) : date("c");

					$healthkitSaveArr = array();

					$healthkitSaveArr['UserID'] = $UserId;
					$healthkit_end_date = date("Y-m-d", strtotime($end_date));
					$healthkit_end_time = date("H:i:s", strtotime($end_date));
					$healthkitSaveArr['bp_record_date'] = $healthkit_end_date;
					$healthkitSaveArr['bp_record_time'] = $healthkit_end_time;
					$healthkitSaveArr['systolic'] = $systolic;
					$healthkitSaveArr['diastolic'] = $diastolic;
					$healthkitSaveArr['source'] = $source;
					$healthkitSaveArr['creation_date'] = round(microtime(true) * 1000);

					$pressureArr = array('UserID'=>$UserId, 'bp_record_date'=>$healthkit_end_date, 'bp_record_time'=>$healthkit_end_time, 'systolic'=>$systolic, 'diastolic'=>$diastolic, 'bp.source'=>$source);
					$checkPressure = $IndBpTableObj->getBpRecord($pressureArr);
					
					if(!count($checkPressure)){
						$bpinsData = $IndBpTableObj->insertBpRecord($healthkitSaveArr);

						if($bpinsData){
							$coloumUpdate = array('healthkit_end_date' => $end_date);
							$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
						}
					}
				}
				
				// Pulse Data
				if($value['type'] == 'pulse'){
					//in bpm
					$pulse = isset($value['value']) ? $value['value'] : '';
					$end_date = !empty($value['date']) ? date("c", strtotime($value['date'])) : date("c");

					$healthkitSaveArr = array();

					$healthkitSaveArr['UserID'] = $UserId;
					$healthkit_end_date = date("Y-m-d", strtotime($end_date));
					$healthkit_end_time = date("H:i:s", strtotime($end_date));
					$healthkitSaveArr['bp_record_date'] = $healthkit_end_date;
					$healthkitSaveArr['bp_record_time'] = $healthkit_end_time;
					$healthkitSaveArr['pulse'] = $pulse;
					$healthkitSaveArr['source'] = $source;
					$healthkitSaveArr['creation_date'] = round(microtime(true) * 1000);

					$pulseArr = array('UserID'=>$UserId, 'bp_record_date'=>$healthkit_end_date, 'bp_record_time'=>$healthkit_end_time, 'pulse'=>$pulse, 'bp.source'=>$source);
					$checkPulse = $IndBpTableObj->getBpRecord($pulseArr);
					
					if(!count($checkPulse)){
						$bpinsData = $IndBpTableObj->insertBpRecord($healthkitSaveArr);

						if($bpinsData){
							$coloumUpdate = array('healthkit_end_date' => $end_date);
							$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
						}
					}
				}
				
				// Temperature Data
				if($value['type'] == 'temperature'){
					$temperature = isset($value['value']) ? $value['value'] : ''; //in Celcius
					//Convert it into Fahrenheit
					$temperature = $CusConPlug->CelTOFah($temperature);
					$end_date = !empty($value['date']) ? date("c", strtotime($value['date'])) : date("c");

					$healthkitSaveArr = array();

					$healthkitSaveArr['UserID'] = $UserId;
					$healthkit_end_date = date("Y-m-d", strtotime($end_date));
					$healthkit_end_time = date("H:i:s", strtotime($end_date));
					$healthkitSaveArr['temperature_record_date'] = $healthkit_end_date;
					$healthkitSaveArr['temperature_record_time'] = $healthkit_end_time;
					$healthkitSaveArr['temperature'] = $temperature;
					$healthkitSaveArr['temperature_unit'] = 'C';
					$healthkitSaveArr['source'] = $source;
					$healthkitSaveArr['creation_date'] = round(microtime(true) * 1000);

					$tempArr = array('UserID'=>$UserId, 'temperature_record_date'=>$healthkit_end_date, 'temperature_record_time'=>$healthkit_end_time, 'temperature'=>$temperature, 'temp.source'=>$source);
					$checkTemp = $TempTableObj->gettempRecord($tempArr);
					
					if(!count($checkTemp)){
						$tempinsData = $TempTableObj->inserttempRecord($healthkitSaveArr);

						if($tempinsData){
							$coloumUpdate = array('healthkit_end_date' => $end_date);
							$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
						}
					}
				}

				// Sleep Data
				if($value['type'] == 'sleep'){
					$start_date = !empty($value['startdate']) ? date("c", strtotime($value['startdate'])) : date("c");
					$end_date = !empty($value['enddate']) ? date("c", strtotime($value['enddate'])) : date("c");

					$from_time = strtotime($start_date);
					$to_time = strtotime($end_date);

					$total_sleep = round(abs($to_time - $from_time) / 60); //in minutes

					$healthkitSaveArr = array();

					$healthkitSaveArr['UserID'] = $UserId;
					$healthkit_end_date = date("Y-m-d", strtotime($end_date));
					$healthkit_end_time = date("H:i:s", strtotime($end_date));
					$healthkitSaveArr['sleep_record_date'] = $healthkit_end_date;
					$healthkitSaveArr['sleep_record_time'] = $healthkit_end_time;
					$healthkitSaveArr['total_sleep'] = $total_sleep;
					$healthkitSaveArr['source'] = $source;
					$healthkitSaveArr['creation_date'] = round(microtime(true) * 1000);

					$sleepArr = array('UserID'=>$UserId, 'sleep_record_date'=>$healthkit_end_date, 'sleep_record_time'=>$healthkit_end_time, 'total_sleep'=>$total_sleep, 'sle.source'=>$source);
					$checkSleep = $SleepTableObj->getsleepRecord($sleepArr);
					
					if(!count($checkSleep)){
						$sleepinsData = $SleepTableObj->insertsleepRecord($healthkitSaveArr);

						if($sleepinsData){
							$coloumUpdate = array('healthkit_end_date' => $end_date);
							$IndUserTableObj->updateUser($whereUserAr, $coloumUpdate);
						}
					}
				}

			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }

}
