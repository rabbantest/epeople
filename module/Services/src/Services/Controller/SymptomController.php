<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE SYMPTOM CONTROLLER FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain.
 * CREATOR: 30/12/2014.
 * UPDATED: 30/12/2014.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use \Exception;

class SymptomController extends AbstractRestfulController {

    public function __construct() {
		
    }

    public function getList(){

    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id) {

	}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);
		//echo '<pre>';print_r($requestArr);exit;

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'getusersymptoms':
				$responseArr = $this->getusersymptoms($service_key);
				break;
			case 'addsymptoms':
				$responseArr = $this->addsymptoms($service_key);
				break;
			case 'editsymptoms':
				$responseArr = $this->editsymptoms($service_key);
				break;
			case 'viewsymptoms':
				$responseArr = $this->viewsymptoms($service_key);
				break;
			case 'updatesymptomsstatus':
				$responseArr = $this->updatesymptomsstatus($service_key);
				break;
			case 'editothersymptoms':
				$responseArr = $this->editothersymptoms($service_key);
				break;
			case 'deleteothersymptoms':
				$responseArr = $this->deleteothersymptoms($service_key);
				break;
			case 'getrelatedsymptoms':
				$responseArr = $this->getrelatedsymptoms($service_key);
				break;
			case 'checkusersymptoms':
				$responseArr = $this->checkusersymptoms($service_key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}

	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {

	}

	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}
    
    
    /*****
     * Action: This action is used to get user symptoms
     * @author: Rachit Jain
     * Created Date: 20-03-2015.
     *****/
    public function getusersymptoms($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserSymtomsTableObj = $this->getServiceLocator()->get("ServicesUsersSymtomsTable");
		$IndCondiTableObj = $this->getServiceLocator()->get("ServicesConditionTable");
		$IndUserOtherSympTableObj = $this->getServiceLocator()->get("ServicesUsersOtherSymtomsTable");
		
		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			/*$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}*/

			if (empty($UserID)) {
                throw new Exception("Please enter user id.");
            }

			//for user symptoms
			$whereSymArr = array("user_id" => $UserID,'symp.status' => 1);
			
			//for user other symptoms
			$otherwhere = array('user_id'=>$UserID,'ot_sym.status'=>1);
			
			$totalCount = count($IndUserSymtomsTableObj->getUserSymptoms($whereSymArr,array(),'',array(), '', '')) + count($IndUserOtherSympTableObj->getUserOtherSymtoms($otherwhere));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$user_Symptoms=array();
			$user_Symptoms = $IndUserSymtomsTableObj->getUserSymptoms($whereSymArr,array(),'',array(), $lowerLimit, $maxLimit);
			$finalRes = array();
			if(count($user_Symptoms)){
				foreach($user_Symptoms as $val){
					$result['symptoms_name'] = $val['common_name'];
					$result['symtoms_id'] = $val['symtoms_id'];
					$result['sub_symptoms_name'] = isset($val['sub_symptoms_name']) ? $val['sub_symptoms_name'] : '';
					$result['sub_symtoms_id'] = isset($val['sub_symtoms_id']) ? $val['sub_symtoms_id'] : '';
					$result['description'] = isset($val['description']) ? $val['description'] : '';
					$result['media_name'] = isset($val['media_name']) ? $val['media_name'] : '';
					$result['sub_media_name'] = isset($val['sub_media_name']) ? $val['sub_media_name'] : '';
					$result['SympStatus'] = isset($val['SympStatus']) ? $val['SympStatus'] : '';
					$result['SympStatus_id'] = $val['symptoms_status'];
					$result['condition_name']=array();
					if($val['condition_ids']){
						$res = array();
						$res = $IndCondiTableObj->getConditionsList(array('status'=>1,'id'=>explode(',',$val['condition_ids'])), array('id','common_name'), "0", array());
						if($res){
							$result['condition_name'] = $res;
						}
					}
					$result['symptom_type'] = "real";
					$result['id'] = $val['id'];
					$finalRes[] = $result;
				}
			}

			//Get user other symptom
			$finalRes1 = array();
			$getUserOtherSymp = $IndUserOtherSympTableObj->getUserOtherSymtoms($otherwhere);
			if(count($getUserOtherSymp)){
				$result1['symptoms_name'] = $getUserOtherSymp[0]['other_symtoms'];
				$result1['symtoms_id'] = "";
				$result1['sub_symptoms_name'] = "";
				$result1['sub_symtoms_id'] = "";
				$result1['description'] = "";
				$result1['SympStatus'] = $getUserOtherSymp[0]['condition_status'];
				$result1['SympStatus_id'] = $getUserOtherSymp[0]['status_id'];
				$result1['condition_name']= array();
				$result1['symptom_type'] = "other";
				$result1['id'] = $getUserOtherSymp[0]['id'];
				$finalRes1[] = $result1;
			}
			$final = array();
			$final = array_merge($finalRes, $finalRes1);
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"user_Symptoms"=>$final,"total"=>$totalpage);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: This action is used to add symptoms and subsymptoms
     * @author: Rachit Jain
     * Created Date: 20-03-2015.
     *****/
    /*public function addsymptoms() {
		$sucess = TRUE;
        $IndCondiTableObj = $this->getServiceLocator()->get("ServicesConditionTable");
        $IndUserSymtomsTableObj = $this->getServiceLocator()->get("ServicesUsersSymtomsTable");
		$IndUserOtherSympTableObj = $this->getServiceLocator()->get("ServicesUsersOtherSymtomsTable");
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$symptomsId_list = isset($data['symptomsId_list']) ? $data['symptomsId_list'] : array();
			$symptoms_status = isset($data['symptoms_status']) ? $data['symptoms_status'] : array();
			$subsymptoms = isset($data['subsymptoms']) ? $data['subsymptoms'] : array();
			$user_condition = isset($data['user_condition']) ? $data['user_condition'] : array();
			$user_other_symptoms = isset($data['user_other_symptoms']) ? $data['user_other_symptoms'] : array();

			if (empty($UserID)) {
				throw new Exception("Please enter user id.");
			}
			if(!count($symptomsId_list)){
				throw new Exception("Please enter valid symptoms.");
			}
			$whereArr = array("user_id" => $UserID, 'symp.status' => 1);
			$existCountSymptoms = $IndUserSymtomsTableObj->getUserSymptoms($whereArr);
			if(count($existCountSymptoms) > 1){
				throw new Exception("You can't add more than 9 symptoms.");
			}

			$otherwhere = array('user_id'=>$UserID,'ot_sym.status'=>1);
			$getUserOtherSymp = $IndUserOtherSympTableObj->getUserOtherSymtoms($otherwhere);

			if($user_other_symptoms && !$getUserOtherSymp){
				$dataArr = array('user_id'=>$UserID,'other_symtoms'=>$user_other_symptoms,'creation_date'=>round(microtime(true) * 1000),					'updation_date'=>round(microtime(true) * 1000));
				$IndUserOtherSympTableObj->SaveUserOtherSymtoms($dataArr);
			}

			foreach ($symptomsId_list as $k=>$val) {
				$symptomsStatusID = 0;
				$SubSymptomsId = 0;
				$symptomsStatusID = $symptoms_status[$k];
				$SubSymptomsId = $subsymptoms[$k];
				$ConditionsIds='';
				if(count($user_condition[$k])){
					$ConditionsIds = implode(',',$user_condition[$k]);
				}
				$symtomsArr = array('user_id'=>$UserID,'symtoms_id'=>$val,'condition_ids'=>$ConditionsIds,
				'sub_symtoms_id'=>$SubSymptomsId,'symptoms_status'=>$symptomsStatusID,'creation_date'=>round(microtime(true) * 1000),
				'updation_date'=>round(microtime(true) * 1000));
				$IndUserSymtomsTableObj->SaveUserSymtoms($symtomsArr);
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Data added successfully.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }
	*/

    public function addsymptoms($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $IndUserSymtomsTableObj = $this->getServiceLocator()->get("ServicesUsersSymtomsTable");
		$IndUserOtherSympTableObj = $this->getServiceLocator()->get("ServicesUsersOtherSymtomsTable");
		$FollowerUserObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$symptoms_list = isset($data['symptoms_list']) ? $data['symptoms_list'] : '';
			$symptoms_status = isset($data['symptoms_status']) ? $data['symptoms_status'] : '';
			$subsymptoms = isset($data['subsymptoms']) ? $data['subsymptoms'] : '';
			$user_condition = isset($data['user_condition']) ? $data['user_condition'] : '';
			$user_other_symptoms = isset($data['user_other_symptoms']) ? $data['user_other_symptoms'] : '';
			$other_symptoms_status = isset($data['other_symptoms_status']) ? $data['other_symptoms_status'] : '';
			//$connected_users = isset($data['connected_users']) ? $data['connected_users'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if (empty($UserID)) {
				throw new Exception("Please enter user id.");
			}
			if(empty($symptoms_list) && empty($user_other_symptoms)){
				throw new Exception("Please enter valid symptoms.");
			}
			if (empty($symptoms_status) && empty($other_symptoms_status)) {
                throw new Exception("Please enter symptom status.");
            }

			$existSymptom = $IndUserSymtomsTableObj->getUserSymptoms(array("user_id" => $UserID,"symtoms_id" => $symptoms_list,'symp.status'=>1));
			if(count($existSymptom)){
				throw new Exception("You have already added this symptom.");
			}

			$whereArr = array("user_id" => $UserID, 'symp.status' => 1);
			$existCountSymptoms = $IndUserSymtomsTableObj->getUserSymptoms($whereArr);
			if(count($existCountSymptoms) > 9){
				throw new Exception("You can't add more than 9 symptoms.");
			}

			$otherwhere = array('user_id'=>$UserID,'ot_sym.status'=>1);
			$getUserOtherSymp = $IndUserOtherSympTableObj->getUserOtherSymtoms($otherwhere);
			if($user_other_symptoms && $getUserOtherSymp){
				throw new Exception("You have already added other symptom.");
			}
			if($user_other_symptoms && !$getUserOtherSymp){
				if (empty($other_symptoms_status)) {
					throw new Exception("Please enter other symptom status.");
				}
				$dataArr = array('user_id'=>$UserID,'other_symtoms'=>$user_other_symptoms,'status_id'=>$other_symptoms_status,'creation_date'=>round(microtime(true) * 1000),					'updation_date'=>round(microtime(true) * 1000));
				$IndUserOtherSympTableObj->SaveUserOtherSymtoms($dataArr);
			}

			if($symptoms_list){
				$symtomsArr = array('user_id'=>$UserID,'symtoms_id'=>$symptoms_list,'condition_ids'=>$user_condition,
				'sub_symtoms_id'=>$subsymptoms,'symptoms_status'=>$symptoms_status,'creation_date'=>round(microtime(true) * 1000),
				'updation_date'=>round(microtime(true) * 1000));
				$IndUserSymtomsTableObj->SaveUserSymtoms($symtomsArr);
			}

			$connectedUser = array();
			//Get Connected Peoples
			$whereFollower['follow.user_id'] = $UserID;
			$whereFollower['follow.status'] = 1;
			$whereFollower['follow.is_send_notification'] = 1;
			$connectedUser = $FollowerUserObj->getUserFollowerDetails($whereFollower);

			/*
			if(!empty($connected_users)){
				$connectedUser = explode(',', $connected_users);
			}
			*/

			//UserInfo
			$where = array("UserID"=>$UserID);
			$columns = array('FirstName','LastName','Email');
			$userData = $IndUserTableObj->getUser($where,$columns);
			$UserName = $userData[0]['FirstName'].' '.$userData[0]['LastName'];

			//Save Notification
			$type = 'symptom_add';
			foreach($connectedUser as $k=>$v){
				$notDataArr = array("sender_id"=>$UserID,"receiver_id"=>$v['follower_id'],"user_type"=>1,"created_date"=>round(microtime(true) * 1000));
				$saveNoti = $NotificationTableObj->SaveNotification($notDataArr, $type);
				
				//Get User Devices
				$deviceTokenData = array();
				$whereArr = array('UserID'=>$v['follower_id'], 'status'=>1);
				$deviceTokenData = $IndUserTableObj->getDeviceToken($whereArr);

				$androidArray = array();
				$iphoneArray = array();
				foreach($deviceTokenData as $key=>$val){
					if(($val['DeviceType'] == 'android') && !empty($val['RegistrationID'])){
						array_push($androidArray, $val['RegistrationID']);
					}
					elseif(($val['DeviceType'] == 'ios') && !empty($val['DeviceToken'])){
						array_push($iphoneArray, $val['DeviceToken']);
					}
				}

				//Send Notification To User
				$message = $UserName.' added a symptom';
				$push_data =array();
				$push_data['user_id'] = $UserID;
				$push_data['type'] = $type;
				$push_data['alert'] = $message;
				$push_data['badge'] = 1; 
				$push_data['sound'] = 'default';
				$payload['aps'] = $push_data;

				if(count($iphoneArray) > 0){
					$CusConPlug->sendPush($iphoneArray, $payload);
				}
				if(count($androidArray) > 0){
					$CusConPlug->sendPushAndroid($androidArray, $payload);
				}
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Data added successfully.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: This action is used to edit symptoms and subsymptoms
     * @author: Rachit Jain
     * Created Date: 20-03-2015.
     *****/
    public function editsymptoms($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserSymtomsTableObj = $this->getServiceLocator()->get("ServicesUsersSymtomsTable");
		$FollowerUserObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$symptomsID = isset($data['symptom_id']) ? $data['symptom_id'] : '';
			$symptoms_status = isset($data['symptoms_status']) ? $data['symptoms_status'] : '';
			$subsymptoms = isset($data['subsymptoms']) ? $data['subsymptoms'] : '';
			$user_condition = isset($data['user_condition']) ? $data['user_condition'] : '';
			//$connected_users = isset($data['connected_users']) ? $data['connected_users'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID) || empty($symptomsID)){
				throw new Exception("Required parameter missing.");
			}

			$dataArr = array();
			$dataArr['sub_symtoms_id']=$subsymptoms;
			$dataArr['symptoms_status']=$symptoms_status;
			$dataArr['condition_ids']=$user_condition;
			if(empty($subsymptoms)){
				$dataArr['sub_symtoms_id']=0;
			}
			if(empty($symptoms_status)){
				$dataArr['symptoms_status']=0;
			}
			if(empty($user_condition)){
				$dataArr['condition_ids']='';
			}

			$where = array('user_id'=>$UserID,'status'=>1,'symtoms_id'=>$symptomsID);
			$IndUserSymtomsTableObj->updateUserSymtoms($where,$dataArr);

			$connectedUser = array();
			//Get Connected Peoples
			$whereFollower['follow.user_id'] = $UserID;
			$whereFollower['follow.status'] = 1;
			$whereFollower['follow.is_send_notification'] = 1;
			$connectedUser = $FollowerUserObj->getUserFollowerDetails($whereFollower);

			/*
			if(!empty($connected_users)){
				$connectedUser = explode(',', $connected_users);
			}
			*/

			//UserInfo
			$where = array("UserID"=>$UserID);
			$columns = array('FirstName','LastName','Email');
			$userData = $IndUserTableObj->getUser($where,$columns);
			$UserName = $userData[0]['FirstName'].' '.$userData[0]['LastName'];

			//Save Notification
			$type = 'symptom_edit';
			foreach($connectedUser as $k=>$v){
				$notDataArr = array("sender_id"=>$UserID,"receiver_id"=>$v['follower_id'],"user_type"=>1,"created_date"=>round(microtime(true) * 1000));
				$saveNoti = $NotificationTableObj->SaveNotification($notDataArr, $type);

				//Get User Devices
				$deviceTokenData = array();
				$whereArr = array('UserID'=>$v['follower_id'], 'status'=>1);
				$deviceTokenData = $IndUserTableObj->getDeviceToken($whereArr);

				$androidArray = array();
				$iphoneArray = array();
				foreach($deviceTokenData as $key=>$val){
					if(($val['DeviceType'] == 'android') && !empty($val['RegistrationID'])){
						array_push($androidArray, $val['RegistrationID']);
					}
					elseif(($val['DeviceType'] == 'ios') && !empty($val['DeviceToken'])){
						array_push($iphoneArray, $val['DeviceToken']);
					}
				}

				//Send Notification To User
				$message = $UserName.' edit his symptom';
				$push_data =array();
				$push_data['user_id'] = $UserID;
				$push_data['type'] = $type;
				$push_data['alert'] = $message;
				$push_data['badge'] = 1; 
				$push_data['sound'] = 'default';
				$payload['aps'] = $push_data;

				if(count($iphoneArray) > 0){
					$CusConPlug->sendPush($iphoneArray, $payload);
				}
				if(count($androidArray) > 0){
					$CusConPlug->sendPushAndroid($androidArray, $payload);
				}
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Data updated successfully.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: This action is used for symptoms description
     * @author: Rachit Jain
     * Created Date: 19-03-2015.
     *****/
	public function viewsymptoms($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserSymtomsTableObj = $this->getServiceLocator()->get("ServicesUsersSymtomsTable");
		$IndCondiTableObj = $this->getServiceLocator()->get("ServicesConditionTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$symptomID = isset($data['symptom_id']) ? $data['symptom_id'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID) || empty($symptomID)){
				throw new Exception("Required parameter missing.");
			}

			$user_Symptoms=array();
			$whereSymArr = array("user_id" => $UserID,'symp.status' => 1,'symp.symtoms_id'=>$symptomID);
			$user_Symptoms = $IndUserSymtomsTableObj->getUserSymptoms($whereSymArr);
			$finalRes = array();
			if(count($user_Symptoms)){
				foreach($user_Symptoms as $val){
					$result['symptoms_name'] = $val['common_name'];
					$result['symtoms_id'] = $val['symtoms_id'];
					$result['sub_symptoms_name'] = $val['sub_symptoms_name'];
					$result['description'] = $val['description'];
					$result['SympStatus'] = $val['SympStatus'];
					$result['condition_name']='';
					if($val['condition_ids']){
						$res = $IndCondiTableObj->getConditionsList(array('status'=>1,'id'=>explode(',',$val['condition_ids'])), array('id','common_name'), "0", array());
						if($res){
							$result['condition_name'] = $res;
						}
					}
					$finalRes[] = $result;
				}
			}

			$symptom_desc = array();
			if(!empty($finalRes[0])){
				$symptom_desc = $finalRes[0];
			}
			
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}
		
		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"symptom_description"=>$symptom_desc);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	 /*****
     * Action: This action is used to update the user symptoms status
     * @author: Rachit Jain
     * Created Date: 18-03-2015.
     *****/
    public function updatesymptomsstatus($service_key) {
        $sucess = TRUE;
        $IndUserSymtomsTableObj = $this->getServiceLocator()->get("ServicesUsersSymtomsTable");
        $IndUserTreatmentTableObj = $this->getServiceLocator()->get("ServicesUsersTreatmentsTable");
        
        try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$symptomID = isset($data['symptom_id']) ? $data['symptom_id'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID) || empty($symptomID)){
				throw new Exception("Required parameter missing.");
			}

			$whereArr = array("user_id" => $UserID, 'symtoms_id' => $symptomID);
			$IndUserSymtomsTableObj->updateUserSymtoms($whereArr, array('status' => 0));
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: This action is used to update the user other symptom
     * @author: Rachit Jain
     * Created Date: 07-04-2015.
     *****/
    public function editothersymptoms($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserOtherSympTableObj = $this->getServiceLocator()->get("ServicesUsersOtherSymtomsTable");
		$FollowerUserObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$ID = isset($data['id']) ? $data['id'] : '';
			$user_other_symptoms = isset($data['user_other_symptoms']) ? $data['user_other_symptoms'] : '';
			$status = isset($data['status']) ? $data['status'] : '';
			//$connected_users = isset($data['connected_users']) ? $data['connected_users'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID) || empty($ID) || empty($user_other_symptoms) || empty($status)
			){
				throw new Exception("Required parameter missing.");
			}

			$dataArr = array('status_id'=>$status,'other_symtoms'=>$user_other_symptoms,'updation_date'=>round(microtime(true) * 1000));
			$where = array('id'=>$ID);
			$IndUserOtherSympTableObj->updateUserOtherSymtoms($where,$dataArr);

			$connectedUser = array();
			//Get Connected Peoples
			$whereFollower['follow.user_id'] = $UserID;
			$whereFollower['follow.status'] = 1;
			$whereFollower['follow.is_send_notification'] = 1;
			$connectedUser = $FollowerUserObj->getUserFollowerDetails($whereFollower);

			/*
			if(!empty($connected_users)){
				$connectedUser = explode(',', $connected_users);
			}
			*/

			//UserInfo
			$where = array("UserID"=>$UserID);
			$columns = array('FirstName','LastName','Email');
			$userData = $IndUserTableObj->getUser($where,$columns);
			$UserName = $userData[0]['FirstName'].' '.$userData[0]['LastName'];

			//Save Notification
			$type = 'symptom_edit';
			foreach($connectedUser as $k=>$v){
				$notDataArr = array("sender_id"=>$UserID,"receiver_id"=>$v['follower_id'],"user_type"=>1,"created_date"=>round(microtime(true) * 1000));
				$saveNoti = $NotificationTableObj->SaveNotification($notDataArr, $type);
				
				//Get User Devices
				$deviceTokenData = array();
				$whereArr = array('UserID'=>$v['follower_id'], 'status'=>1);
				$deviceTokenData = $IndUserTableObj->getDeviceToken($whereArr);

				$androidArray = array();
				$iphoneArray = array();
				foreach($deviceTokenData as $key=>$val){
					if(($val['DeviceType'] == 'android') && !empty($val['RegistrationID'])){
						array_push($androidArray, $val['RegistrationID']);
					}
					elseif(($val['DeviceType'] == 'ios') && !empty($val['DeviceToken'])){
						array_push($iphoneArray, $val['DeviceToken']);
					}
				}

				//Send Notification To User
				$message = $UserName.' edit his symptom';
				$push_data =array();
				$push_data['user_id'] = $UserID;
				$push_data['type'] = $type;
				$push_data['alert'] = $message;
				$push_data['badge'] = 1; 
				$push_data['sound'] = 'default';
				$payload['aps'] = $push_data;

				if(count($iphoneArray) > 0){
					$CusConPlug->sendPush($iphoneArray, $payload);
				}
				if(count($androidArray) > 0){
					$CusConPlug->sendPushAndroid($androidArray, $payload);
				}
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}
		
		if($sucess === TRUE){
			return array("errstr"=>"Data updated successfully.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: This action is used to delete other symptom
     * @author: Rachit Jain
     * Created Date: 14-04-2015.
     *****/
    public function deleteothersymptoms($service_key) {
		$sucess = TRUE;
		$IndUserOtherSympTableObj = $this->getServiceLocator()->get("ServicesUsersOtherSymtomsTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$ID = isset($data['id']) ? $data['id'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID) || empty($ID)){
				throw new Exception("Required parameter missing.");
			}

			$where = array('id'=>$ID);
			$IndUserOtherSympTableObj->deleteUserSymtoms($where);
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}
		
		if($sucess === TRUE){
			return array("errstr"=>"Data deleted successfully.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: get related symptoms
     * @author: Rachit Jain
     * Created Date: 19-03-2015.
     *****/
    public function getrelatedsymptoms($service_key) {
        $sucess = TRUE;
		$IndSymtomsTableObj = $this->getServiceLocator()->get("ServicesSymtomsTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$txt_name = isset($data['name']) ? $data['name'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			$whereArr = array("status" => 1);
			$columns = array("id", "common_name");
			$symtomsArr = $IndSymtomsTableObj->getSymptoms($whereArr, $columns, "0", array('common_name'=>$txt_name));
			if (empty($symtomsArr)) {
				throw new Exception("No result found.");
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}
		
		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"symptom_list"=>$symtomsArr);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*****
     * Action: Used to check user already added symptoms
     * @author: Rachit Jain
     * Created Date: 19-03-2015.
     *****/
    public function checkusersymptoms($service_key) {
		$sucess = TRUE;
		$IndUserSymtomsTableObj = $this->getServiceLocator()->get("ServicesUsersSymtomsTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$symptomId = isset($data['symptom_id']) ? $data['symptom_id'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID) || empty($symptomId)){
				throw new Exception("Required parameter missing.");
			}

			$whereArr = array("user_id" => $UserID, 'symtoms_id' => $symptomId, 'symp.status' => 1);
			$res = $IndUserSymtomsTableObj->getUserSymptoms($whereArr);
			if (count($res)) {
				throw new Exception("This symptom already added by you.");
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1);
		}
        else{
			return array("errstr"=>$error,"success"=>0);
		}
    }
}
