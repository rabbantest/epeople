<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE PEOPLE SECTION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain.
 * CREATOR: 29/01/2015.
 * UPDATED:  .
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\EventManager\EventManagerInterface;
use Zend\View\Model\JsonModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Mail;
use \Exception;

class PeopleController extends AbstractRestfulController {

    public function __construct() {
        
    }

    public function getList() {
        $request = $this->request->getQuery();
        $method = $request->method;
        $device_type = $request->device_type;
        $device_token = $request->device_token;

        $params['device_type'] = $device_type;
        $params['device_token'] = $device_token;
        $this->params = $params;

        switch ($method) {
            case 'sendpush':
                $responseArr = $this->sendpush();
                break;
            default:
                break;
        }

        echo json_encode($responseArr);
        exit;
    }

    /**
     * Return single resource
     * @param mixed $id
     * @return mixed
     */
    public function get($id) {
        
    }

    /**
     * Create a new resource
     *
     * @param mixed $data
     * @return mixed
     */
    public function create($data) {
        //fetch array from json data
        $request = $this->request->getContent();
        $requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);

        //print_r($requestArr);exit;

        $method = $requestArr['method'];
        $service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
        if (isset($requestArr['params'])) {
            $params = $requestArr['params'];
            $this->params = $params;
        }

        switch ($method) {
            case 'connected':
                $responseArr = $this->connected($service_key);
                break;
            case 'requested':
                $responseArr = $this->requested($service_key);
                break;
            case 'suggested':
                $responseArr = $this->suggested($service_key);
                break;
            case 'searchuser':
                $responseArr = $this->searchuser($service_key);
                break;
            case 'sendfollowrequest':
                $responseArr = $this->sendfollowrequest($service_key);
                break;
            case 'deletefollowrequest':
                $responseArr = $this->deletefollowrequest($service_key);
                break;
            case 'followeraccept':
                $responseArr = $this->followeraccept($service_key);
                break;
            case 'followerreject':
                $responseArr = $this->followerreject($service_key);
                break;
            case 'followernotifications':
                $responseArr = $this->followernotifications($service_key);
                break;
            case 'getuserconditionslist':
                $responseArr = $this->getuserconditionslist($service_key);
                break;
            default:
                break;
        }

        echo json_encode($responseArr);
        exit;
    }

    /**
     * Update an existing resource
     *
     * @param mixed $id
     * @param mixed $data
     * @return mixed
     */
    public function update($id, $data) {
        
    }

    /**
     * Delete an existing resource
     *
     * @param  mixed $id
     * @return mixed
     */
    public function delete($id) {
        
    }

    /*
      @ Used:This function is used to manage the connected people
      @ Created : 03-02-2015
      @ By : Rachit Jain
     */

    public function connected($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $FollowerUserObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");
        $IndCondiTableObj = $this->getServiceLocator()->get("ServicesConditionTable");
        $CountryTableObj = $this->getServiceLocator()->get("ServicesCountryTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $OtherUserID = isset($data['otheruserid']) ? $data['otheruserid'] : '';
            $page = isset($data['page']) ? $data['page'] : '1';
            $maxLimit = 10;
            $lowerLimit = ($page - 1) * $maxLimit;

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID) || empty($OtherUserID)) {
                throw new Exception("Required parameter missing.");
            }

            $connectedUser = array();
            $whereFollower = array();
            $searchContents = array();

            if (!empty($data['user_name'])) {
                $searchContents['us.FirstName'] = $data['user_name'];
                //$searchContents['u.MidleName'] = $postDataArr['user_name'];
                $searchContents['us.LastName'] = $data['user_name'];
            }
            if (!empty($data['location'])) {
                $whereFollower['hm_con.country_id'] = $data['location'];
                //$whereSentFollower['hm_con.country_id'] = $data['location'];
            }

            $whereFollower['follow.user_id'] = $OtherUserID;
            //$whereFollower['follow.user_id'] = $UserID;
            $whereFollower['follow.status'] = 1;
            $totalCount = count($FollowerUserObj->getUserFollowerDetails($whereFollower, array(), array(), $searchContents, '', ''));
            $totalpage = $page = '';
            $page = (int) ($totalCount / $maxLimit);
            if (($page * $maxLimit) < $totalCount) {
                $totalpage = $page + 1;
            } else {
                $totalpage = $page;
            }

            $connectedUser = $FollowerUserObj->getUserFollowerDetails($whereFollower, array(), array(), $searchContents, $lowerLimit, $maxLimit);

            $i = 0;
            foreach ($connectedUser as $key1 => $val1) {
                //Get full image url
                $image = "";
                if ($val1['Image'] != "") {
                    $server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
                    //Get Original Image
                    //$image = $server_url."/upload_image/".$val1['UserID']."/".$val1['Image'];
                    //Get Image From CacheImg Folder
                    $imagepath = $server_url . "/upload_image/" . $val1['UserID'] . "/";
                    $imagename = $val1['Image'];
                    $newWidth = 150;
                    $newHeight = 150;
                    $image = $plugin->resizeimage($imagepath, $imagename, $newWidth, $newHeight);
                }
                $connectedUser[$i]['Image'] = $image;
                $i++;
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "connectedUser" => $connectedUser, "total" => $totalpage);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ Used:This function is used to manage the user requests
      @ Created : 03-02-2015
      @ By : Rachit Jain
     */

    public function requested($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $FollowerUserObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");
        $IndCondiTableObj = $this->getServiceLocator()->get("ServicesConditionTable");
        $CountryTableObj = $this->getServiceLocator()->get("ServicesCountryTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $page = isset($data['page']) ? $data['page'] : '1';
            $maxLimit = 10;
            $lowerLimit = ($page - 1) * $maxLimit;

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID)) {
                throw new Exception("Please enter user id.");
            }

            $whereFollower = array();
            $whereSentFollower = array();
            $searchContents = array();
            $gotRequest = array();
            $sentRequest = array();

            if (!empty($data['user_name'])) {
                $searchContents['us.FirstName'] = $data['user_name'];
                //$searchContents['u.MidleName'] = $postDataArr['user_name'];
                $searchContents['us.LastName'] = $data['user_name'];
            }
            if (!empty($data['location'])) {
                $whereFollower['hm_con.country_id'] = $data['location'];
                $whereSentFollower['hm_con.country_id'] = $data['location'];
            }

            $whereFollower['follow.user_id'] = $UserID;
            $whereFollower['follow.status'] = 0;

            $whereSentFollower['follow.user_id'] = $UserID;
            $whereSentFollower['follow.status'] = 0;
            $whereSentFollower['parent_id'] = 0;

            $gotRequestCount = count($FollowerUserObj->getUserFollowerDetails($whereFollower, array(), array('parent_id' => 0), $searchContents, '', ''));
            $sentRequestCount = count($FollowerUserObj->getUserFollowerDetails($whereSentFollower, array(), array(), $searchContents, '', ''));

            $totalCount = $gotRequestCount + $sentRequestCount;
            $totalpage = $page = '';
            $page = (int) ($totalCount / $maxLimit);
            if (($page * $maxLimit) < $totalCount) {
                $totalpage = $page + 1;
            } else {
                $totalpage = $page;
            }

            $gotRequest = $FollowerUserObj->getUserFollowerDetails($whereFollower, array(), array('parent_id' => 0), $searchContents, $lowerLimit, $maxLimit);
            $sentRequest = $FollowerUserObj->getUserFollowerDetails($whereSentFollower, array(), array(), $searchContents, $lowerLimit, $maxLimit);
            $final_Request = array_merge($gotRequest, $sentRequest);

            $i = 0;
            foreach ($final_Request as $key1 => $val1) {
                //Get full image url
                $image = "";
                if ($val1['Image'] != "") {
                    $server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
                    //Get Original Image
                    //$image = $server_url."/upload_image/".$val1['UserID']."/".$val1['Image'];
                    //Get Image From CacheImg Folder
                    $imagepath = $server_url . "/upload_image/" . $val1['UserID'] . "/";
                    $imagename = $val1['Image'];
                    $newWidth = 150;
                    $newHeight = 150;
                    $image = $plugin->resizeimage($imagepath, $imagename, $newWidth, $newHeight);
                }

                //Get Relation With Other Users
                $hash = "";
                $where = array('user_id' => $UserID, 'follower_id' => $val1['follower_id'], 'status' => 0);
                $checkGotSentReqeust = $FollowerUserObj->getUserFollower($where);
                if (count($checkGotSentReqeust)) {
                    if ($checkGotSentReqeust[0]['parent_id'] == 0) {
                        $relation = 'resend';
                    } else {
                        $relation = 'accept_reject';
                    }
                    $hash = $checkGotSentReqeust[0]['hash'];
                } else {
                    $relation = 'follow';
                }

                $final_Request[$i]['Image'] = $image;
                $final_Request[$i]['hash'] = $hash;
                $final_Request[$i]['relation'] = $relation;
                $i++;
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "userRequests" => $final_Request, "total" => $totalpage);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ Used:This function is used to manage the suggested people
      @ Created : 03-02-2015
      @ By : Rachit Jain
     */

    public function suggested($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $FollowerUserObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");
        $IndCondiTableObj = $this->getServiceLocator()->get("ServicesConditionTable");
        $CountryTableObj = $this->getServiceLocator()->get("ServicesCountryTable");
        $IndUserConditionTableObj = $this->getServiceLocator()->get("ServicesUsersConditionTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $DomainID = 1;
            $page = isset($data['page']) ? $data['page'] : '1';
            $maxLimit = 10;
            $lowerLimit = ($page - 1) * $maxLimit;

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID)) {
                throw new Exception("Please enter user id.");
            }

            $conditionslist = array();
            $whereArr = array();
            $whereInids = array();
            $conditionIds = array();
            $searchContents = array();
            $ageRange = array();
            $similar_condtion = array();
            $nameSearch = '';
            $age_from = '';
            $age_to = '';

            $exp = '"' . $DomainID . '":\\\[("[[:digit:]]*",)*"1"';
            $whereInids = array(new \Zend\Db\Sql\Predicate\Expression(" UserTypeId REGEXP '$exp'"));

            $whereArr = array("user_id" => $UserID, 'con.status' => 1);
            $conditionslist = $IndUserConditionTableObj->getUserAllConditionsIDs($whereArr, array('condition_id'));

            if (!empty($data['user_name']) || !empty($data['select_condition']) || !empty($data['select_subcondition']) || !empty($data['location']) || (!empty($data['age_from']) && !empty($data['age_to']))) {
                if (!empty($data['user_name'])) {
                    $nameSearch = $data['user_name'];
                    $nameSearch = str_replace(" ", "", $nameSearch);
                }
                if (!empty($data['select_condition'])) {
                    $whereInids['condition_id'] = $data['select_condition'];
                }
                if (!empty($data['select_subcondition'])) {
                    $whereInids['con.sub_condition_id'] = $data['select_subcondition'];
                }
                if (!empty($data['location'])) {
                    $whereInids['hm_con.country_id'] = $data['location'];
                }
                if (!empty($data['age_from']) && !empty($data['age_to'])) {
                    $fromDate = strtotime("-" . $data['age_from'] . " year", time());
                    $ageFrom = date("Y-m-d", $fromDate);
                    $toDate = strtotime("-" . $data['age_to'] . " year", time());
                    $ageTo = date("Y-m-d", $toDate);
                    $ageRange = array('ageFrom' => $ageFrom, 'ageTo' => $ageTo);
                }
            } else {
                if (count($conditionslist)) {
                    if (empty($data['select_condition'])) {
                        foreach ($conditionslist as $k => $v) {
                            $conditionIds[] = $v['condition_id'];
                        }
                    }
                    $whereInids['condition_id'] = $conditionIds;
                    $whereInids['con.status'] = 1;
                }
            }

            //Get Followers
            $whereFollower = array('follower_id' => $UserID, 'status' => 1);
            $getConnecedId = $FollowerUserObj->getUserFollower($whereFollower, array('user_id'));
            $connecUIds = array();
            if (count($getConnecedId)) {
                foreach ($getConnecedId as $key => $val) {
                    $connecUIds[] = $val['user_id'];
                }
            }
            $whereInids['u.Verified'] = 1;
            $whereInids['privacy.demographic_info'] = 1;
            $notEq = array($UserID);
            $NotInSuggestedUsers = array_merge($notEq, $connecUIds);

            $totalCount = count($IndUserConditionTableObj->getAllSimilarConditionUser($whereInids, $NotInSuggestedUsers, array('user_id'), $searchContents, $nameSearch, $ageRange, '', ''));
            $totalpage = $page = '';
            $page = (int) ($totalCount / $maxLimit);
            if (($page * $maxLimit) < $totalCount) {
                $totalpage = $page + 1;
            } else {
                $totalpage = $page;
            }

            $similar_condtion = $IndUserConditionTableObj->getAllSimilarConditionUser($whereInids, $NotInSuggestedUsers, array('user_id'), $searchContents, $nameSearch, $ageRange, $lowerLimit, $maxLimit);
            //echo '<pre>';print_r($similar_condtion);exit;

            $i = 0;
            foreach ($similar_condtion as $key1 => $val1) {
                //Get full image url
                $image = "";
                if ($val1['Image'] != "") {
                    $server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
                    //Get Original Image
                    //$image = $server_url."/upload_image/".$val1['UserID']."/".$val1['Image'];
                    //Get Image From CacheImg Folder
                    $imagepath = $server_url . "/upload_image/" . $val1['UserID'] . "/";
                    $imagename = $val1['Image'];
                    $newWidth = 150;
                    $newHeight = 150;
                    $image = $plugin->resizeimage($imagepath, $imagename, $newWidth, $newHeight);
                }

                //Get Relation With Other Users
                $hash = "";
                $where = array('user_id' => $UserID, 'follower_id' => $val1['user_id'], 'status' => 0);
                $checkGotSentReqeust = $FollowerUserObj->getUserFollower($where);
                if (count($checkGotSentReqeust)) {
                    if ($checkGotSentReqeust[0]['parent_id'] == 0) {
                        $relation = 'resend';
                    } else {
                        $relation = 'accept_reject';
                    }
                    $hash = $checkGotSentReqeust[0]['hash'];
                } else {
                    $relation = 'follow';
                }
                $similar_condtion[$i]['Age'] = $plugin->GetAge($val1['Dob']);
                $similar_condtion[$i]['Image'] = $image;
                $similar_condtion[$i]['hash'] = $hash;
                $similar_condtion[$i]['relation'] = $relation;
                $i++;
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "similar_condtion_users" => $similar_condtion, "total" => $totalpage);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*
      @ Used:This function is used to manage the suggested people
      @ Created : 03-02-2015
      @ By : Rachit Jain
     */

    public function searchuser($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $FollowerUserObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");
        $IndCondiTableObj = $this->getServiceLocator()->get("ServicesConditionTable");
        $CountryTableObj = $this->getServiceLocator()->get("ServicesCountryTable");
        $IndUserConditionTableObj = $this->getServiceLocator()->get("ServicesUsersConditionTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $DomainID = 1;
            $page = isset($data['page']) ? $data['page'] : '1';
            $maxLimit = 10;
            $lowerLimit = ($page - 1) * $maxLimit;

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID)) {
                throw new Exception("Please enter user id.");
            }

            $conditionslist = array();
            $whereArr = array();
            $whereInids = array();
            $conditionIds = array();
            $searchContents = array();
            $ageRange = array();
            $similar_condtion = array();
            $nameSearch = '';
            $age_from = '';
            $age_to = '';

            $exp = '"' . $DomainID . '":\\\[("[[:digit:]]*",)*"1"';
            $whereInids = array(new \Zend\Db\Sql\Predicate\Expression(" UserTypeId REGEXP '$exp'"));

            if (!empty($data['user_name'])) {
                $nameSearch = $data['user_name'];
                $nameSearch = str_replace(" ", "", $nameSearch);
            }
            if (!empty($data['select_condition'])) {
                $whereInids['condition_id'] = $data['select_condition'];
            }
            if (!empty($data['select_subcondition'])) {
                $whereInids['con.sub_condition_id'] = $data['select_subcondition'];
            }
            if (!empty($data['location'])) {
                $whereInids['hm_con.country_id'] = $data['location'];
            }
            if (!empty($data['age_from']) && !empty($data['age_to'])) {
                $fromDate = strtotime("-" . $data['age_from'] . " year", time());
                $ageFrom = date("Y-m-d", $fromDate);
                $toDate = strtotime("-" . $data['age_to'] . " year", time());
                $ageTo = date("Y-m-d", $toDate);
                $ageRange = array('ageFrom' => $ageFrom, 'ageTo' => $ageTo);
            }

            //Get Followers
            $whereFollower = array('follower_id' => $UserID, 'status' => 1);
            $getConnecedId = $FollowerUserObj->getUserFollower($whereFollower, array('user_id'));
            $connecUIds = array();
            if (count($getConnecedId)) {
                foreach ($getConnecedId as $key => $val) {
                    $connecUIds[] = $val['user_id'];
                }
            }
            $whereInids['u.Verified'] = 1;
            $whereInids['privacy.demographic_info'] = 1;
            $notEq = array($UserID);
            $NotInSuggestedUsers = array();
            $NotInSuggestedUsers = array_merge($notEq, $connecUIds);

            $columnArr = array('UserID', 'FirstName', 'LastName', 'UserName', 'Image', 'Email', 'Dob');
            $totalCount = count($IndUserConditionTableObj->getUserSearch($whereInids, $NotInSuggestedUsers, $columnArr, $searchContents, $nameSearch, $ageRange, '', ''));
            $totalpage = $page = '';
            $page = (int) ($totalCount / $maxLimit);
            if (($page * $maxLimit) < $totalCount) {
                $totalpage = $page + 1;
            } else {
                $totalpage = $page;
            }

            $similar_condtion = $IndUserConditionTableObj->getUserSearch($whereInids, $NotInSuggestedUsers, $columnArr, $searchContents, $nameSearch, $ageRange, $lowerLimit, $maxLimit);
            //echo '<pre>';print_r($similar_condtion);exit;

            $i = 0;
            foreach ($similar_condtion as $key1 => $val1) {
                //Get full image url
                $image = "";
                if ($val1['Image'] != "") {
                    $server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
                    //Get Original Image
                    //$image = $server_url."/upload_image/".$val1['UserID']."/".$val1['Image'];
                    //Get Image From CacheImg Folder
                    $imagepath = $server_url . "/upload_image/" . $val1['UserID'] . "/";
                    $imagename = $val1['Image'];
                    $newWidth = 150;
                    $newHeight = 150;
                    $image = $plugin->resizeimage($imagepath, $imagename, $newWidth, $newHeight);
                }

                //Get Relation With Other Users
                $hash = "";
                $where = array('user_id' => $UserID, 'follower_id' => $val1['UserID'], 'status' => 0);
                $checkGotSentReqeust = $FollowerUserObj->getUserFollower($where);
                if (count($checkGotSentReqeust)) {
                    if ($checkGotSentReqeust[0]['parent_id'] == 0) {
                        $relation = 'resend';
                    } else {
                        $relation = 'accept_reject';
                    }
                    $hash = $checkGotSentReqeust[0]['hash'];
                } else {
                    $relation = 'follow';
                }
                $similar_condtion[$i]['Age'] = $plugin->GetAge($val1['Dob']);
                $similar_condtion[$i]['Image'] = $image;
                $similar_condtion[$i]['hash'] = $hash;
                $similar_condtion[$i]['relation'] = $relation;
                $i++;
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "similar_condtion_users" => $similar_condtion, "total" => $totalpage);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*     * ***
     * Action: This action is used to send follow request
     * @author: Rachit Jain
     * Created Date: 11-02-2015.
     * *** */

    public function sendfollowrequest($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $UserFollowerTabObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $FollowerId = isset($data['followerid']) ? $data['followerid'] : '';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID) || empty($FollowerId)) {
                throw new Exception("Required parameter missing.");
            }

            /* Start Code for check already follower */
            $whereCondition1 = array('user_id' => $UserID, 'follower_id' => $FollowerId, 'status' => 1);
            $followerUserRes1 = $UserFollowerTabObj->getUserFollower($whereCondition1);
            if ($followerUserRes1) {
                throw new Exception("You request has been accepted successfully.");
            }
            /* End Code for check already follower */

            /* Start Code for check already sent request */
            $whereCondition = array('user_id' => $UserID, 'follower_id' => $FollowerId, 'status' => 0);
            $followerUserRes = $UserFollowerTabObj->getUserFollower($whereCondition, array('hash'), 1);
            /* End Code for check already sent request */

            if (!$followerUserRes) {
                $hash = md5(time() . rand(1, 100));
                $userfolloData = array("user_id" => $UserID, 'follower_id' => $FollowerId, 'hash' => $hash, 'creation_date' => date("Y-m-d"), 'updation_date' => date("Y-m-d"));
                $lastInId = $UserFollowerTabObj->SaveFollower($userfolloData);
                $folloData = array("follower_id" => $UserID, 'user_id' => $FollowerId, 'hash' => $hash, 'parent_id' => $lastInId, 'creation_date' => date("Y-m-d"), 'updation_date' => date("Y-m-d"));
                $UserFollowerTabObj->SaveFollower($folloData);
            }

            //Get Updated followers request array
            $updatedfollowerUserRes = $UserFollowerTabObj->getUserFollower($whereCondition, array('hash'), 1);

            $username = $updatedfollowerUserRes[0]['user_first_name'] . $updatedfollowerUserRes[0]['user_last_name'];
            $useremail = $updatedfollowerUserRes[0]['user_email'];
            $followername = $updatedfollowerUserRes[0]['follower_first_name'];
            $follweremail = $updatedfollowerUserRes[0]['follower_email'];

            //Save Notification
            $type = 'follow_request';
            $notDataArr = array("sender_id" => $UserID, "receiver_id" => $FollowerId, "user_type" => 1, "created_date" => round(microtime(true) * 1000));
            $saveNoti = $NotificationTableObj->SaveNotification($notDataArr, $type);

            //Get User Devices
            $deviceTokenData = array();
            $whereArr = array('UserID' => $FollowerId, 'status' => 1);
            $deviceTokenData = $IndUserTableObj->getDeviceToken($whereArr);

            $androidArray = array();
            $iphoneArray = array();
            foreach ($deviceTokenData as $key => $val) {
                if (($val['DeviceType'] == 'android') && !empty($val['RegistrationID'])) {
                    array_push($androidArray, $val['RegistrationID']);
                } elseif (($val['DeviceType'] == 'ios') && !empty($val['DeviceToken'])) {
                    array_push($iphoneArray, $val['DeviceToken']);
                }
            }

            //Send Notification To User
            $message = $username . ' sent you follow request';
            $push_data = array();
            $push_data['user_id'] = $UserID;
            $push_data['type'] = $type;
            $push_data['alert'] = $message;
            $push_data['badge'] = 1;
            $push_data['sound'] = 'default';
            $payload['aps'] = $push_data;

            if (count($iphoneArray) > 0) {
                $plugin->sendPush($iphoneArray, $payload);
            }
            if (count($androidArray) > 0) {
                $plugin->sendPushAndroid($androidArray, $payload);
            }

            //Send Mail
            $message = "Dear $followername,<br>";
            $message .= "<br>$username would like to connect with you. To accept this request, simply
						visit the My Connections / People section of your account by clicking here (https://individual.quantextualhealth.com) or within your Q app.";
            //$message .= "<br>To respond follower request, login in your account.";
            $message .="<br><br>Here’s to your health,<br>The Quantextual Health Team";
            $subject = "Connection Request";
            $plugin->senInvitationMail($follweremail, $subject, $message);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Request sent successfully.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*     * ***
     * Action: This action is used to delete follow request
     * @author: Rachit Jain
     * Created Date: 11-02-2015.
     * *** */

    public function deletefollowrequest($service_key) {
        $sucess = TRUE;
        $UserFollowerTabObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $hash = isset($data['hash']) ? $data['hash'] : '';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            $whereArr = array("hash" => $hash);
            $UserFollowerTabObj->deleteFollowerRequest($whereArr);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Request deleted successfully.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*     * ***
     * Action: This action is used to accept follow request
     * @author: Rachit Jain
     * Created Date: 11-02-2015.
     * *** */

    public function followeraccept($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $UserFollowerTabObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $hash = isset($data['hash']) ? $data['hash'] : '';
            $response = isset($data['response']) ? $data['response'] : '';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID) || empty($hash)) {
                throw new Exception("Required parameter missing.");
            }

            $whereArr = array("hash" => $hash);

            if ($response == "accept") {
                $status = 1;
                $UserFollowerTabObj->updateFollower($whereArr, array('status' => $status));

                $whereCondition = array('follower_id' => $UserID, 'hash' => $hash);
                $followerUserRes = $UserFollowerTabObj->getUserFollower($whereCondition, array(), 1);
                //echo '<pre>';print_r($followerUserRes);exit;
                $userid = $followerUserRes[0]['user_id'];
                $username = $followerUserRes[0]['user_first_name'];
                $useremail = $followerUserRes[0]['user_email'];
                $followerid = $followerUserRes[0]['follower_id'];
                $followername = $followerUserRes[0]['follower_first_name'] . ' ' . $followerUserRes[0]['follower_last_name'];
                $follweremail = $followerUserRes[0]['follower_email'];

                //Save Notification
                $type = 'follow_accept';
                $notDataArr = array("sender_id" => $followerid, "receiver_id" => $userid, "user_type" => 1, "created_date" => round(microtime(true) * 1000));
                $saveNoti = $NotificationTableObj->SaveNotification($notDataArr, $type);

                //Get User Devices
                $deviceTokenData = array();
                $whereArr = array('UserID' => $userid, 'status' => 1);
                $deviceTokenData = $IndUserTableObj->getDeviceToken($whereArr);

                $androidArray = array();
                $iphoneArray = array();
                foreach ($deviceTokenData as $key => $val) {
                    if (($val['DeviceType'] == 'android') && !empty($val['RegistrationID'])) {
                        array_push($androidArray, $val['RegistrationID']);
                    } elseif (($val['DeviceType'] == 'ios') && !empty($val['DeviceToken'])) {
                        array_push($iphoneArray, $val['DeviceToken']);
                    }
                }

                //Send Notification To User
                $message = $followername . ' accepted your follow reuest';
                $push_data = array();
                $push_data['user_id'] = $followerid;
                $push_data['type'] = $type;
                $push_data['alert'] = $message;
                $push_data['badge'] = 1;
                $push_data['sound'] = 'default';
                $payload['aps'] = $push_data;

                if (count($iphoneArray) > 0) {
                    $plugin->sendPush($iphoneArray, $payload);
                }
                if (count($androidArray) > 0) {
                    $plugin->sendPushAndroid($androidArray, $payload);
                }

                //Send Mail
                $message = "<img src='http://individual.quantextualhealth.com/img/mail_header_image.jpeg' alt='Company Logo'><br> <br>";
                $message .= "<br><br>";
                $message .= "Dear $username,<br>";
                $message .= "<br>$followername has accepted your Connection request.  You can easily manage all your connections in the My Connections section of your account on your web platform or mobile Q app.";
                $message .="<br><br>Here’s to your health,<br>The Quantextual Health Team";
                $subject = "Connection Request Accepted";
                $plugin->senInvitationMail($useremail, $subject, $message);
            } else {
                $UserFollowerTabObj->deleteFollowerRequest($whereArr);
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Request accepted successfully.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*     * ***
     * Action: This action is used to reject follow request
     * @author: Rachit Jain
     * Created Date: 11-02-2015.
     * *** */

    public function followerreject($service_key) {
        $sucess = TRUE;
        $UserFollowerTabObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $hash = isset($data['hash']) ? $data['hash'] : '';


            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            $whereArr = array("hash" => $hash);
            //$UserFollowerTabObj->updateFollower($whereArr, array('status' => 2));
            $UserFollowerTabObj->deleteFollowerRequest($whereArr);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Request rejected successfully.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*     * ***
     * Action: This action is used to on/off follower notifications
     * @author: Rachit Jain
     * Created Date: 11-06-2015.
     * *** */

    public function followernotifications($service_key) {
        $sucess = TRUE;
        $UserFollowerTabObj = $this->getServiceLocator()->get("ServicesFollowerUsersTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $FollowerId = isset($data['followerid']) ? $data['followerid'] : '';
            $is_send_notification = isset($data['is_send_notification']) ? $data['is_send_notification'] : '';

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            if (empty($UserID) || empty($FollowerId) || (empty($is_send_notification) && strlen($is_send_notification) == 0)) {
                throw new Exception("Required parameter missing.");
            }

            $dataArr = array('is_send_notification' => $is_send_notification);
            $whereArr = array("user_id" => $UserID, "follower_id" => $FollowerId);
            $UserFollowerTabObj->updateFollower($whereArr, $dataArr);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Setting updated successfully.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    /*     * ***
     * Action: This action is used to get list of user conditions, symptoms, treatments
     * @author: Rachit Jain
     * Created Date: 05-02-2015.
     * *** */

    public function getuserconditionslist($service_key) {
        $sucess = TRUE;
        $IndCondiTableObj = $this->getServiceLocator()->get("ServicesConditionTable");
        $IndSymtomsTableObj = $this->getServiceLocator()->get("ServicesSymtomsTable");
        $IndTreatmentTableObj = $this->getServiceLocator()->get("ServicesTreatmentTable");
        $IndUserConditionTableObj = $this->getServiceLocator()->get("ServicesUsersConditionTable");
        $IndUserSymtomsTableObj = $this->getServiceLocator()->get("ServicesUsersSymtomsTable");
        $IndUserTreatmentTableObj = $this->getServiceLocator()->get("ServicesUsersTreatmentsTable");

        try {
            $data = $this->params;
            $UserID = isset($data['userid']) ? $data['userid'] : '';
            $OtherUserID = isset($data['otheruserid']) ? $data['otheruserid'] : '';
            $page = isset($data['page']) ? $data['page'] : '1';
            $maxLimit = 20;
            $lowerLimit = ($page - 1) * $maxLimit;

            $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
            $whereArrKey = array("user_id" => $UserID, "service_key" => $service_key);
            $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
            if (COUNT($userSecurityKey) <= 0) {
                throw new Exception("Authentication Failed");
            }

            //User Conditions List With Subconditions
            $whereConArr = array("user_id" => $OtherUserID, 'con.status' => 1);
            //$whereConArr = array("user_id" => $UserID,'con.status'=>1);
            $conditionslist = $IndUserConditionTableObj->getUserAllConditions($whereConArr, array(), $lowerLimit, $maxLimit);

            $finalConRes = array();
            $finalSubConRes = array();
            $resultCon = array();
            $sub_resultCon = array();
            $finalConArr = array();

            if (count($conditionslist)) {
                foreach ($conditionslist as $val) {
                    $resultCon['condition_id'] = $val['condition_id'];
                    $resultCon['condition_name'] = $val['common_name'];
                    $resultCon['description'] = $val['description'];
                    if (!empty($val['sub_condition_id'])) {
                        $sub_resultCon['condition_id'] = $val['sub_condition_id'];
                        $sub_resultCon['condition_name'] = $val['sub_condition_name'];
                        $sub_resultCon['description'] = $val['sub_description'];
                        $finalSubConRes[] = $sub_resultCon;
                    }
                    $finalConRes[] = $resultCon;
                }
            }
            $finalConArr = array_merge($finalConRes, $finalSubConRes);

            //Get User Symptoms List
            $whereSymArr = array("user_id" => $OtherUserID, 'symp.status' => 1);
            //$whereSymArr = array("user_id" => $UserID,'symp.status' => 1);
            $user_Symptoms = $IndUserSymtomsTableObj->getUserSymptoms($whereSymArr, array(), '', array(), $lowerLimit, $maxLimit);

            $finalSymRes = array();
            $finalSubSymRes = array();
            $resultSym = array();
            $sub_resultSym = array();
            $finalSymArr = array();

            if (count($user_Symptoms)) {
                foreach ($user_Symptoms as $val) {
                    $resultSym['symptom_id'] = $val['symtoms_id'];
                    $resultSym['symptom_name'] = $val['common_name'];
                    $resultSym['description'] = $val['description'];
                    if (!empty($val['sub_symtoms_id'])) {
                        $sub_resultSym['symptom_id'] = $val['sub_symtoms_id'];
                        $sub_resultSym['symptom_name'] = $val['sub_symptoms_name'];
                        $sub_resultSym['description'] = $val['sub_description'];
                        $finalSubSymRes[] = $sub_resultSym;
                    }
                    $finalSymRes[] = $resultSym;
                }
            }
            $finalSymArr = array_merge($finalSymRes, $finalSubSymRes);


            //Get User Treatments List
            $whereTreArr = array("user_id" => $OtherUserID, 'treat.status' => 1);
            //$whereTreArr = array("user_id" => $UserID,'treat.status' => 1);
            $user_Treatments = $IndUserTreatmentTableObj->getUserTreatment($whereTreArr, array(), '', array(), $lowerLimit, $maxLimit);

            $finalTreRes = array();
            $finalSubTreRes = array();
            $resultTre = array();
            $sub_resultTre = array();
            $finalTreArr = array();

            if (count($user_Treatments)) {
                foreach ($user_Treatments as $val) {
                    $resultTre['treatment_id'] = $val['treatment_id'];
                    $resultTre['treatment_name'] = $val['common_name'];
                    $resultTre['description'] = $val['description'];
                    if (!empty($val['sub_treatment_id'])) {
                        $sub_resultTre['treatment_id'] = $val['sub_treatment_id'];
                        $sub_resultTre['treatment_name'] = $val['sub_treatment_name'];
                        $sub_resultTre['description'] = $val['sub_description'];
                        $finalSubTreRes[] = $sub_resultTre;
                    }
                    $finalTreRes[] = $resultTre;
                }
            }
            $finalTreArr = array_merge($finalTreRes, $finalSubTreRes);
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess == TRUE) {
            return array("errstr" => "Success.", "success" => 1, "user_conditions" => $finalConArr, "user_symptoms" => $finalSymArr, "user_treatments" => $finalTreArr);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    public function sendpush() {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");

        try {
            $data = $this->params;
            //print_r($data);exit;
            $DeviceType = isset($data['device_type']) ? $data['device_type'] : '';
            $DeviceToken = isset($data['device_token']) ? $data['device_token'] : '';
            $type = 'Push Test';

            if (empty($DeviceType) || empty($DeviceToken)) {
                throw new Exception("Required paramter missing.");
            }

            //Send Notification To User
            $message = 'Push Notification Testing';
            $push_data = array();
            $push_data['user_id'] = '1';
            $push_data['type'] = $type;
            $push_data['alert'] = $message;
            $push_data['badge'] = 1;
            $push_data['sound'] = 'default';
            $payload['aps'] = $push_data;

            if ($DeviceType == 'ios') {
                $iphoneArray = array();
                array_push($iphoneArray, $DeviceToken);
                //print_r($iphoneArray);exit;
                if (count($iphoneArray) > 0) {
                    $plugin->sendPush($iphoneArray, $payload);
                }
            } else {
                $androidArray = array();
                array_push($androidArray, $DeviceToken);
                //print_r($androidArray);exit;
                if (count($androidArray) > 0) {
                    $plugin->sendPushAndroid($androidArray, $payload);
                }
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

}

?>
