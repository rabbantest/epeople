<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE Device FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain.
 * CREATOR: 05/01/2015.
 * UPDATED:  .
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use \Exception;

class DeviceController extends AbstractRestfulController {

    public function __construct() {
        
    }

    public function getList() {
        $request = $this->request->getQuery();
        $method = $request->method;

        switch ($method) {
            case 'synchronizedevic':
                $responseArr = $this->synchronizedevic();
                break;
            case 'synchumanapi':
                $responseArr = $this->synchumanapi();
                break;
            default:
                break;
        }

        echo json_encode($responseArr);
        exit;
    }

    /**
     * Return single resource
     * @param mixed $id
     * @return mixed
     */
    public function get($id) {
        
    }

    /**
     * Create a new resource
     * @param mixed $data
     * @return mixed
     */
    public function create($data) {
        //fetch array from json data
        $request = $this->request->getContent();
        $requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);
        //echo '<pre>';print_r($requestArr);exit;

        $method = $requestArr['method'];
        if (isset($requestArr['params'])) {
            $params = $requestArr['params'];
            $this->params = $params;
        }

        switch ($method) {
            case 'index':
                $responseArr = $this->index();
                break;
            case 'connectdevice':
                $responseArr = $this->connectdevice();
                break;
            case 'synchronizedevic':
                $responseArr = $this->synchronizedevic();
                break;
            default:
                break;
        }

        echo json_encode($responseArr);
        exit;
    }

    /**
     * Update an existing resource
     * @param mixed $id
     * @param mixed $data
     * @return mixed
     */
    public function update($id, $data) {
        
    }

    /**
     * Delete an existing resource
     * @param  mixed $id
     * @return mixed
     */
    public function delete($id) {
        
    }

    public function index() {
        $sucess = TRUE;
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
        $CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();

        try {
            $data = $this->params;
            $userId = isset($data['userid']) ? $data['userid'] : '';
            $UserName = isset($data['UserName']) ? $data['UserName'] : '';
            /* $service_key = isset($data['service_key']) ? $data['service_key'] : "";

              $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
              $whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
              $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
              if (COUNT($userSecurityKey) <= 0){
              throw new Exception("Authentication Failed");
              } */

            $VALIDIC_ACCESS_TOKEN = $VALIDIC_USER_ID = NULL;

            /*
              @ get user validic id and access token
             */
            $whereArray = array("UserID" => $userId);
            $coloumnarray = array("validic_id", "validic_access_token", "CreationDate");
            $user_details = $IndUserTableObj->getUser($whereArray, $coloumnarray);

            foreach ($user_details as $uservalue) {
                $VALIDIC_ACCESS_TOKEN = $uservalue['validic_access_token'];
                $VALIDIC_USER_ID = $uservalue['validic_id'];
                $CREATION_DATE = $uservalue['CreationDate'];
            }

            if (empty($VALIDIC_USER_ID) && empty($VALIDIC_ACCESS_TOKEN)) {
                $validic_result = 0;
            } else {
                $validic_result = 1;
                //Sandbox
                //$ORGANIZATION_ID = "526ec0ca6dedda7b1c000025";
                //$ORGANIZATION_ACCESS_TOKEN = "35f76b711c1b31c8c3365412fbfe584b74bc4a21f6d5e3737b83cebf4d6b6c7f";
                //Production
                $ORGANIZATION_ID = "55c0cb3f6362310018360000";
                $ORGANIZATION_ACCESS_TOKEN = "5520b18f020d85b3ded445f8c6c821165e80ce273c8da3e550f95dd67e981d43";

                $validic_url = "https://api.validic.com/v1/organizations/";
                $return_url = $server_url . "/individual-device";
                $final = array();
                $res = array('result' => 'emtpy result');


                $res = json_decode(file_get_contents("$validic_url$ORGANIZATION_ID/apps.json?authentication_token=$VALIDIC_ACCESS_TOKEN&access_token=$ORGANIZATION_ACCESS_TOKEN&redirect_uri=$return_url"));
                $appsDataArr = $res->apps;
                $res = array();
                foreach ($appsDataArr as $value) {
                    $res['name'] = $value->name;
                    $res['sync_url'] = $value->sync_url;
                    $res['unsync_url'] = $value->unsync_url;
                    $res['refresh_url'] = $value->refresh_url;
                    $res['subname'] = $value->subname;
                    $res['logo_url'] = $value->logo_url;
                    $final[] = $res;
                }
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "UserName" => $UserName, "validic_url" => "https://app.validic.com", "validic_result" => $validic_result, "validic_device" => $final, "validic_access_tocken" => $VALIDIC_ACCESS_TOKEN, "validic_id" => $VALIDIC_USER_ID, "CREATION_DATE" => $CREATION_DATE);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    public function connectdevice() {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");

        //Sandbox
        //$ORGANIZATION_ID = "526ec0ca6dedda7b1c000025";
        //$ORGANIZATION_ACCESS_TOKEN = "35f76b711c1b31c8c3365412fbfe584b74bc4a21f6d5e3737b83cebf4d6b6c7f";
        //Production
        $ORGANIZATION_ID = "55c0cb3f6362310018360000";
        $ORGANIZATION_ACCESS_TOKEN = "5520b18f020d85b3ded445f8c6c821165e80ce273c8da3e550f95dd67e981d43";

        try {
            $data = $this->params;
            $user_name = isset($data['user_name']) ? $data['user_name'] : '';
            $user_name = str_replace(".", "", $user_name);
            $user_name = str_replace(" ", "", $user_name);
            /* $service_key = isset($data['service_key']) ? $data['service_key'] : "";

              $SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
              $whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
              $userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
              if (COUNT($userSecurityKey) <= 0){
              throw new Exception("Authentication Failed");
              } */

            if (empty($user_name)) {
                throw new Exception("Please enter username.");
            }

            //Check if user already have validic id and token
            $where = array('UserName' => $user_name);
            $userdetails = $IndUserTableObj->getUser($where);
            if (!empty($userdetails[0]['validic_id']) && !empty($userdetails[0]['validic_access_token'])) {
                throw new Exception("User already registered on Validic.");
            }

            //Register user on Validic
            $validic_user_id = $validicaccess_token = '';
            $url = "https://api.validic.com/v1/organizations/$ORGANIZATION_ID/users.json";
            $fields = array('uid' => $user_name . time(),
                'access_token' => $ORGANIZATION_ACCESS_TOKEN);

            $validic_regist = $plugin->curlPostValidic($url, $fields);
            $validic_regist = json_decode($validic_regist);
            $valid_code_response = $validic_regist->code;
            //echo '<pre>'; print_r($validic_regist);exit;

            if ($valid_code_response == 201) {
                $validic_user_id = $validic_regist->user->_id;
                $validicaccess_token = $validic_regist->user->access_token;
                $whereArray = array('UserName' => $user_name);
                $dataArray = array(
                    'validic_id' => $validic_user_id,
                    'validic_access_token' => $validicaccess_token
                );

                $IndUserTableObj->updateUser($whereArray, $dataArray);
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if ($sucess === TRUE) {
            return array("errstr" => "Success.", "success" => 1, "validic_user_id" => $validic_user_id, "validicaccess_token" => $validicaccess_token, "organisation_id" => $ORGANIZATION_ID, "organisation_token" => $ORGANIZATION_ACCESS_TOKEN);
        } else {
            return array("errstr" => $error, "success" => 0);
        }
    }

    public function synchronizedevic() {
        $sucess = TRUE;
        $CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $INDCusConPlug = $this->CustomControllerPlugin();
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
        $IndHeightTableObj = $this->getServiceLocator()->get("ServicesHeightRecordTable");
        $IndWeightTableObj = $this->getServiceLocator()->get("ServicesWeightRecordTable");
        $IndWaistTableObj = $this->getServiceLocator()->get("ServicesWaistRecordTable");
        $IndBmiTableObj = $this->getServiceLocator()->get("ServicesBmiRecordTable");
        $IndBpTableObj = $this->getServiceLocator()->get('ServicesBloodPressureRecordTable');
        $TempTableObj = $this->getServiceLocator()->get('ServicesTemperatureRecordTable');
        $SleepTableObj = $this->getServiceLocator()->get('ServicesSleepRecordTable');
        $IndPhyTableObj = $this->getServiceLocator()->get("ServicesPhysicalActivityTable");
        $IndChoTableObj = $this->getServiceLocator()->get("ServicesCholesterolRecordTable");
        $IndBgTableObj = $this->getServiceLocator()->get('ServicesBloodGlucoseRecordTable');
        $IndFoodDrinkTblObj = $this->getServiceLocator()->get("ServicesFoodDrinkTable");
        $IndBiometricTblObj = $this->getServiceLocator()->get("ServicesBiometricsRecordTable");
        $validicUrl = "https://api.validic.com/v1/";

        $validicID = $validic_access_token = $valdic_activity_date = $validic_id_push = $UserId = $start_date = $end_date = $page = '';
        $userData = $user_details = array();
        if (isset($this->params)) {
            $data = $this->params;
            $validicID = $data['validic_id'];
            if (empty($validicID)) {
                throw new Exception("You are not registered on validic.");
            }
        }

        try {
            if (empty($validicID)) {
                $userData = $IndUserTableObj->getvalidicconnectedUser();
            } else {
                $whereArrValidic = array("validic_id" => $validicID);
                $userData = $IndUserTableObj->getUser($whereArrValidic);
            }

            foreach ($userData as $key => $val) {
                $validic_id_push = $val['validic_id'];
                $whereArray = array("validic_id" => $validic_id_push);
                $coloumnarray = array("UserID", "validic_id", "validic_access_token", "validic_updated_date", "CreationDate", "Dob", "Gender", "FirstName");
                $user_details = $IndUserTableObj->getUser($whereArray, $coloumnarray);
                $UserId = $user_details[0]['UserID'];
                $FirstName = $user_details[0]['FirstName'];
                $validic_access_token = $user_details[0]['validic_access_token'];

                $user_details[0]['validic_updated_date'] = ($CusConPlug->isJSON($user_details[0]['validic_updated_date']) ? json_decode($user_details[0]['validic_updated_date'], true) : "");

                $limit = 100; // get 100 data in each hit from validic server
                //update user table for last activity date
                $whereUserAr = array("UserID" => $UserId);

                if (!empty($validic_access_token)) {
                    $insdata = $insroutdata = $inssleepdat = $insweightdat = $insbgdat = $inscholodat = $insbiometricsdat = $bpinsdata = $cholostrolinsdata = $tempinsdata = '';
                    /*
                     * Profile Data recive from validic server 
                     */
                    $profile = "profile.json?authentication_token=$validic_access_token";
                    $hitUrl = $validicUrl . $profile;
                    $profileJsonData = $profileData = array();
                    $profileJsonData = $CusConPlug->curlGet($hitUrl);
                    $profileData = json_decode($profileJsonData);

                    /* @author Rachit Jain
                     * Fitness Data recive from validic server fitness
                     * date: 15-03-2015
                     */
                    $user_details[0]['validic_updated_date']['fitness'] = (isset($user_details[0]['validic_updated_date']['fitness']) && !empty($user_details[0]['validic_updated_date']['fitness'])) ? $user_details[0]['validic_updated_date']['fitness'] : date('Y-m-d', strtotime('-7 day'));

                    $fitness = "fitness.json?authentication_token=$validic_access_token&start_date=" . $user_details[0]['validic_updated_date']['fitness'];
                    $hitfiturl = $validicUrl . $fitness;
                    // ECHO $hitfiturl;EXIT;
                    $fitnessJsonData = $fitnessData = array();
                    $fitnessJsonData = $CusConPlug->curlGet($hitfiturl);
                    $fitnessData = json_decode($fitnessJsonData);

                    $fitness_status = $fitness_message = $fitness_result = $fitness_start_date = $fitness_end_date = $fitness_offset = $fitness_limit = $fitness_next = '';
                    if (count($fitnessData) > 0) {
                        $fitness_status = $fitnessData->summary->status;
                        $fitness_message = $fitnessData->summary->message;
                        $fitness_result = $fitnessData->summary->results;
                        $fitness_start_date = $fitnessData->summary->start_date;
                        $fitness_end_date = $fitnessData->summary->end_date;
                        $fitness_offset = $fitnessData->summary->offset;
                        $fitness_limit = $fitnessData->summary->limit;
                        $fitness_next = $fitnessData->summary->next;

                        $totalhit = ($fitness_result / $limit); // calculate pagination number for validic 
                        $finalfitness = $fitnessSaveArr = array();
                        if ($totalhit > 0) {
                            for ($i = 0; $i < $totalhit; $i++) {
                                $page = ($i + 1);
                                $fitness1 = "fitness.json?authentication_token=$validic_access_token&limit=$limit&page=$page&start_date=" . $user_details[0]['validic_updated_date']['fitness'];
                                $hitfiturl1 = $validicUrl . $fitness1;
                                $fitnessJsonData1 = $r = array();
                                $fitnessJsonData1 = $CusConPlug->curlGet($hitfiturl1);
                                $r = json_decode($fitnessJsonData1);
                                $finalfitness = array_merge($finalfitness, $r->fitness);
                            }
                        }

                        if ($fitness_status == "200" && $fitness_result > 0) {
                            $count_start = 1;
                            $a = 1;
                            foreach ($finalfitness as $fitnessvalue) {
                                // $fitnessSaveArr['CountTotal'] =  $count_start;
                                $fitnessSaveArr['UserID'] = $UserId;
                                $validicid = $fitnessvalue->_id;
                                $fitness_time_stamp = $fitnessvalue->timestamp;
                                $fitness_utc_offset = $fitnessvalue->utc_offset;
                                $fitnessSaveArr['ActivityType'] = $fitness_type = (isset($fitnessvalue->type) ? $fitnessvalue->type : '' );
                                $fitnessSaveArr['Intensity'] = $fitness_intensity = (isset($fitnessvalue->intensity) ? $fitnessvalue->intensity : '' );

                                $fitness_start_time = $fitnessvalue->start_time;
                                $fitnessSaveArr['ExerciseRecorddate'] = date("Y-m-d", strtotime($fitness_start_time));
                                $fitnessSaveArr['ExerciseRecordtime'] = date("H:i:s", strtotime($fitness_start_time));

                                $fitnessSaveArr['Distance'] = $fitness_distance = (isset($fitnessvalue->distance) ? $fitnessvalue->distance : '' );
                                $fitnessSaveArr['Duration'] = $fitness_duration = (isset($fitnessvalue->duration) ? round($CusConPlug->seondtominute($fitnessvalue->duration)) : '' );
                                $fitnessSaveArr['CaloriesBurned'] = $fitness_calories = (isset($fitnessvalue->calories) ? $fitnessvalue->calories : '' );
                                $fitnessSaveArr['source'] = $fitness_source = (isset($fitnessvalue->source) ? $fitnessvalue->source : '' );
                                $fitness_source_name = (isset($fitnessvalue->source_name) ? $fitnessvalue->source_name : '' );
                                $fitness_last_update = (isset($fitnessvalue->last_updated) ? $fitnessvalue->last_updated : '' );
                                $fitnessSaveArr['CreationDate'] = round(microtime(true) * 1000);

                                //Check if there is already an entry
                                $paArr = array("UserID" => $UserId, "ExerciseRecorddate" => date("Y-m-d", strtotime($fitness_start_time)), "ExerciseRecordtime" => date("H:i:s", strtotime($fitness_start_time)), "source" => $fitness_source);
                                $physicalActivityRecord = $IndPhyTableObj->checkPhysicalActivityRecordForValidic($paArr);
                                if (!count($physicalActivityRecord)) {
                                    $insdata = $IndPhyTableObj->insertPhysicalActivity($fitnessSaveArr);
                                }

                                if ($insdata) {
                                    if (strtotime($user_details[0]['validic_updated_date']['fitness']) < strtotime($fitness_last_update)) {
                                        $user_details[0]['validic_updated_date']['fitness'] = $fitness_last_update;
                                        $dateFitness = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateFitness);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-fitness-".$a."<br>";
                                    }
                                }
                                $a++;
                            }
                        }
                    }


                    /* @author Rachit Jain
                     * Fitness Data recive from validic server of routine  
                     * date: 16-03-2015
                     */
                    $user_details[0]['validic_updated_date']['routine'] = (isset($user_details[0]['validic_updated_date']['routine']) && !empty($user_details[0]['validic_updated_date']['routine'])) ? $user_details[0]['validic_updated_date']['routine'] : date('Y-m-d', strtotime('-7 day'));

                    $routine = "routine.json?authentication_token=$validic_access_token&start_date=" . $user_details[0]['validic_updated_date']['routine'];
                    $hitrouturl = $validicUrl . $routine;
                    //echo $hitrouturl."<br>";
                    $routineJsonData = $routineData = array();
                    $routineJsonData = $CusConPlug->curlGet($hitrouturl);
                    $routineData = json_decode($routineJsonData);

                    $routine_status = $routine_message = $routine_result = $routine_start_date = $routine_end_date = $routine_offset = $routine_limit = $routine_next = '';
                    if (count($routineData) > 0) {
                        $routine_status = $routineData->summary->status;
                        $routine_message = $routineData->summary->message;
                        $routine_result = $routineData->summary->results;
                        $routine_start_date = $routineData->summary->start_date;
                        $routine_end_date = $routineData->summary->end_date;
                        $routine_offset = $routineData->summary->offset;
                        $routine_limit = $routineData->summary->limit;
                        $routine_next = $routineData->summary->next;

                        $totalhitroutine = ($routine_result / $limit); // calculate pagination number for validic 

                        $finalroutine = $routineSaveArr = array();
                        if ($totalhitroutine > 0) {
                            for ($i = 0; $i < $totalhitroutine; $i++) {
                                $page = ($i + 1);
                                $routine1 = "routine.json?authentication_token=$validic_access_token&limit=$limit&page=$page&start_date=" . $user_details[0]['validic_updated_date']['routine'];
                                $hitrouturl1 = $validicUrl . $routine1;
                                $routineJsonData1 = $r1 = array();
                                $routineJsonData1 = $CusConPlug->curlGet($hitrouturl1);
                                $r1 = json_decode($routineJsonData1);
                                $finalroutine = array_merge($finalroutine, $r1->routine);
                            }
                        }

                        if ($routine_status == "200" && $routine_result > 0) {
                            $b = 1;
                            foreach ($finalroutine as $routinevalue) {
                                $routine_utc_offset = $routinevalue->utc_offset;
                                $routineSaveArr['UserID'] = $UserId;
                                $routine_timestamp = $routinevalue->timestamp;
                                $routineSaveArr['source'] = $routine_source = (isset($routinevalue->source) ? $routinevalue->source : '' );
                                if ((strpos($routine_utc_offset, '+') !== false) && ($routine_source == 'fitbit')) {
                                    $routineSaveArr['ExerciseRecorddate'] = $updatedroutinedate = date("Y-m-d", strtotime($routine_timestamp . '+1 day'));
                                } else {
                                    $routineSaveArr['ExerciseRecorddate'] = $updatedroutinedate = date("Y-m-d", strtotime($routine_timestamp));
                                }
                                //$routineSaveArr['ExerciseRecorddate'] = date("Y-m-d", strtotime($routine_timestamp));
                                //$routineSaveArr['ExerciseRecordtime'] = date("H:i:s", strtotime($routine_timestamp));

                                $routineSaveArr['NumberOfSteps'] = $routine_steps = (isset($routinevalue->steps) ? $routinevalue->steps : '' );
                                $routineSaveArr['CaloriesBurned'] = $routine_calories_burned = (isset($routinevalue->calories_burned) ? $routinevalue->calories_burned : '' );
                                $routineSaveArr['Distance'] = $routine_distance = (isset($routinevalue->distance) ? $routinevalue->distance : '' );
                                $routine_floors = $routinevalue->floors;
                                $routine_elevation = $routinevalue->elevation;
                                $routineSaveArr['source'] = $routine_source = (isset($routinevalue->source) ? $routinevalue->source : '' );
                                $routine_source_name = $routinevalue->source_name;
                                $routine_last_update = $routinevalue->last_updated;
                                $routineSaveArr['CreationDate'] = round(microtime(true) * 1000);
                                $routineSaveArr['ExerciseRecordtime'] = date("H:i:s", strtotime($routine_last_update));

                                //Delete data for same date
                                $whereRoutineArr = array('UserID' => $UserId, 'ExerciseRecorddate' => $updatedroutinedate, "source" => $routine_source);
                                $notEqual = 1;
                                //print_r($whereRoutineArr);
                                $IndPhyTableObj->deletePhysicalActivity($whereRoutineArr, $notEqual);

                                $insroutdata = $IndPhyTableObj->insertPhysicalActivity($routineSaveArr);

                                if ($insroutdata) {
                                    if (strtotime($user_details[0]['validic_updated_date']['routine']) < strtotime($routine_last_update)) {
                                        //$user_details[0]['validic_updated_date']['routine'] = $routine_last_update;
                                        $user_details[0]['validic_updated_date']['routine'] = date("Y-m-d", strtotime($routine_last_update . '-1 day'));
                                        $dateroutine = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateroutine);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-routine-".$b."<br>";
                                    }
                                }
                                $b++;
                            }
                        }
                    }


                    /* @author Rachit Jain
                     * Fitness Data recive from validic server of nutrition  
                     * date: 16-03-2015
                     */
                    $user_details[0]['validic_updated_date']['nutrition'] = (isset($user_details[0]['validic_updated_date']['nutrition']) && !empty($user_details[0]['validic_updated_date']['nutrition'])) ? $user_details[0]['validic_updated_date']['nutrition'] : date('Y-m-d', strtotime('-7 day'));

                    $nutrition = "nutrition.json?authentication_token=$validic_access_token&start_date=" . $user_details[0]['validic_updated_date']['nutrition'];
                    $hitrouturl = $validicUrl . $nutrition;
                    $nutritionJsonData = $nutritionData = array();
                    $nutritionJsonData = $CusConPlug->curlGet($hitrouturl);
                    $nutritionData = json_decode($nutritionJsonData);

                    $nutrition_status = $nutrition_message = $nutrition_result = $nutrition_start_date = $nutrition_end_date = $nutrition_offset = $nutrition_limit = $nutrition_next = '';
                    if (count($nutritionData) > 0) {
                        $nutrition_status = $nutritionData->summary->status;
                        $nutrition_message = $nutritionData->summary->message;
                        $nutrition_result = $nutritionData->summary->results;
                        $nutrition_start_date = $nutritionData->summary->start_date;
                        $nutrition_end_date = $nutritionData->summary->end_date;
                        $nutrition_offset = $nutritionData->summary->offset;
                        $nutrition_limit = $nutritionData->summary->limit;
                        $nutrition_next = $nutritionData->summary->next;

                        $totalhitnutrition = ($nutrition_result / $limit); // calculate pagination number for validic 

                        $finalnutrition = $nutritionSaveArr = array();
                        if ($totalhitnutrition > 0) {
                            for ($i = 0; $i < $totalhitnutrition; $i++) {
                                $page = ($i + 1);
                                $nutrition1 = "nutrition.json?authentication_token=$validic_access_token&limit=$limit&page=$page&start_date=" . $user_details[0]['validic_updated_date']['nutrition'];
                                $hitnetuurl1 = $validicUrl . $nutrition1;
                                $nutritionJsonData1 = $r1 = array();
                                $nutritionJsonData1 = $CusConPlug->curlGet($hitnetuurl1);
                                $r1 = json_decode($nutritionJsonData1);
                                $finalnutrition = array_merge($finalnutrition, $r1->nutrition);
                            }
                        }

                        if ($nutrition_status == "200" && $nutrition_result > 0) {
                            $c = 1;
                            foreach ($finalnutrition as $nutritionvalue) {
                                $nutritionSaveArr['UserID'] = $UserId;
                                $nutrition_timestamp = $nutritionvalue->timestamp;
                                $nutritionSaveArr['FoodDrinkRecordDate'] = date("Y-m-d", strtotime($nutrition_timestamp));
                                $nutritionSaveArr['FoodDrinkRecordTime'] = date("H:i:s", strtotime($nutrition_timestamp));
                                $nutrition_utc_offset = $nutritionvalue->utc_offset;
                                $nutritionSaveArr['Calories'] = $nutrition_calories = (isset($nutritionvalue->calories) ? $nutritionvalue->calories : '' );

                                $nutrition_fat = $nutritionvalue->fat;
                                $nutrition_fiber = $nutritionvalue->fiber;
                                $nutrition_protein = $nutritionvalue->protein;
                                $nutrition_sodium = $nutritionvalue->sodium;
                                $nutrition_water = $nutritionvalue->water;
                                $nutritionSaveArr['MealType'] = $nutrition_meal = $nutritionvalue->meal;

                                $nutritionSaveArr['source'] = $nutrition_source = (isset($nutritionvalue->source) ? $nutritionvalue->source : '' );
                                $nutrition_source_name = $nutritionvalue->source_name;
                                $nutrition_last_update = $nutritionvalue->last_updated;

                                $nutritionSaveArr['CreationDate'] = round(microtime(true) * 1000);

                                //Check if there is already an entry
                                $fdArr = array("UserID" => $UserId, "FoodDrinkRecordDate" => date("Y-m-d", strtotime($nutrition_timestamp)), "FoodDrinkRecordTime" => date("H:i:s", strtotime($nutrition_timestamp)), "source" => $nutrition_source);
                                $foodDrinkRecord = $IndFoodDrinkTblObj->checkWeightTargetForValidic($fdArr);
                                if (!count($foodDrinkRecord)) {
                                    $insroutdata = $IndFoodDrinkTblObj->insertFoodDrinkRecord($nutritionSaveArr);
                                }

                                if ($insroutdata) {
                                    if (strtotime($user_details[0]['validic_updated_date']['nutrition']) < strtotime($nutrition_last_update)) {
                                        $user_details[0]['validic_updated_date']['nutrition'] = $nutrition_last_update;
                                        $datenutrition = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $datenutrition);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-nutration-".$c."<br>";
                                    }
                                }
                                $c++;
                            }
                        }
                    }


                    /* @author Rachit Jain
                     * Fitness Data recive from validic server of sleep  
                     * date: 19-03-2015
                     */
                    $user_details[0]['validic_updated_date']['sleep'] = (isset($user_details[0]['validic_updated_date']['sleep']) && !empty($user_details[0]['validic_updated_date']['sleep'])) ? $user_details[0]['validic_updated_date']['sleep'] : date('Y-m-d', strtotime('-7 day'));

                    $sleep = "sleep.json?authentication_token=$validic_access_token&start_date=" . $user_details[0]['validic_updated_date']['sleep'];
                    $hitrouturl = $validicUrl . $sleep;
                    $sleepJsonData = $sleepData = array();
                    $sleepJsonData = $CusConPlug->curlGet($hitrouturl);
                    $sleepData = json_decode($sleepJsonData);

                    $sleep_status = $sleep_message = $sleep_result = $sleep_start_date = $sleep_end_date = $sleep_offset = $sleep_limit = $sleep_next = '';
                    if (count($sleepData) > 0) {
                        $sleep_status = $sleepData->summary->status;
                        $sleep_message = $sleepData->summary->message;
                        $sleep_result = $sleepData->summary->results;
                        $sleep_start_date = $sleepData->summary->start_date;
                        $sleep_end_date = $sleepData->summary->end_date;
                        $sleep_offset = $sleepData->summary->offset;
                        $sleep_limit = $sleepData->summary->limit;
                        $sleep_next = $sleepData->summary->next;

                        $totalhitsleep = ($sleep_result / $limit); // calculate pagination number for validic 

                        $finalsleep = $sleepSaveArr = array();
                        if ($totalhitsleep > 0) {
                            for ($i = 0; $i < $totalhitsleep; $i++) {
                                $page = ($i + 1);
                                $sleep1 = "sleep.json?authentication_token=$validic_access_token&limit=$limit&page=$page&start_date=" . $user_details[0]['validic_updated_date']['sleep'];
                                $hitnetuurl1 = $validicUrl . $sleep1;
                                $sleepJsonData1 = $r1 = array();
                                $sleepJsonData1 = $CusConPlug->curlGet($hitnetuurl1);
                                $r1 = json_decode($sleepJsonData1);
                                $finalsleep = array_merge($finalsleep, $r1->sleep);
                            }
                        }

                        if ($sleep_status == "200" && $sleep_result > 0) {
                            $d = 1;
                            foreach ($finalsleep as $sleepvalue) {
                                $sleepSaveArr['UserID'] = $UserId;
                                $sleep_timestamp = $sleepvalue->timestamp;
                                $sleepSaveArr['sleep_record_date'] = date("Y-m-d", strtotime($sleep_timestamp));
                                $sleepSaveArr['sleep_record_time'] = date("H:i:s", strtotime($sleep_timestamp));
                                $sleep_utc_offset = $sleepvalue->utc_offset;
                                $sleepSaveArr['awake'] = $sleep_awake = isset($sleepvalue->awake) ? ($sleepvalue->awake) / 60 : '';
                                $sleepSaveArr['deep'] = $sleep_deep = isset($sleepvalue->deep) ? ($sleepvalue->deep) / 60 : '';
                                $sleepSaveArr['light'] = $sleep_light = isset($sleepvalue->light) ? ($sleepvalue->light) / 60 : '';
                                $sleepSaveArr['rem'] = $sleep_rem = isset($sleepvalue->rem) ? ($sleepvalue->rem) / 60 : '';
                                $sleep_time_woken = $sleepSaveArr['times_woken'] = isset($sleepvalue->times_woken) ? ($sleepvalue->times_woken) / 60 : '';
                                $sleepSaveArr['total_sleep'] = $sleep_total_sleep = isset($sleepvalue->total_sleep) ? ($sleepvalue->total_sleep) / 60 : '';
                                $sleepSaveArr['source'] = $sleep_source = isset($sleepvalue->source) ? $sleepvalue->source : '';
                                $sleep_source_name = $sleepvalue->source_name;
                                $sleep_last_update = $sleepvalue->last_updated;
                                $sleepSaveArr['creation_date'] = round(microtime(true) * 1000);

                                //Check if there is already an entry
                                $sleepArr = array("UserID" => $UserId, "sleep_record_date" => date("Y-m-d", strtotime($sleep_timestamp)), "sleep_record_time" => date("H:i:s", strtotime($sleep_timestamp)), "source" => $sleep_source);
                                $sleepRecord = $SleepTableObj->checksleepRecordForValidic($sleepArr);
                                if (!count($sleepRecord)) {
                                    $inssleepdat = $SleepTableObj->insertsleepRecord($sleepSaveArr);
                                }

                                if ($inssleepdat) {
                                    if (strtotime($user_details[0]['validic_updated_date']['sleep']) < strtotime($sleep_last_update)) {
                                        $user_details[0]['validic_updated_date']['sleep'] = $sleep_last_update;
                                        $datesleep = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $datesleep);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        //  echo "success-sleep-".$d."<br>";
                                    }
                                }
                                $d++;
                            }
                        }
                    }


                    /* @author Rachit Jain
                     * Fitness Data recive from validic server of weight  
                     * date: 19-03-2015
                     */
                    $user_details[0]['validic_updated_date']['weight'] = (isset($user_details[0]['validic_updated_date']['weight']) && !empty($user_details[0]['validic_updated_date']['weight'])) ? $user_details[0]['validic_updated_date']['weight'] : date('Y-m-d', strtotime('-7 day'));

                    $weight = "weight.json?authentication_token=$validic_access_token&start_date=" . $user_details[0]['validic_updated_date']['weight'];
                    $hitrouturl = $validicUrl . $weight;
                    $weightJsonData = $weightData = array();
                    $weightJsonData = $CusConPlug->curlGet($hitrouturl);
                    $weightData = json_decode($weightJsonData);

                    $weight_status = $weight_message = $weight_result = $weight_start_date = $weight_end_date = $weight_offset = $weight_limit = $weight_next = '';
                    if (count($weightData) > 0) {
                        $weight_status = $weightData->summary->status;
                        $weight_message = $weightData->summary->message;
                        $weight_result = $weightData->summary->results;
                        $weight_start_date = $weightData->summary->start_date;
                        $weight_end_date = $weightData->summary->end_date;
                        $weight_offset = $weightData->summary->offset;
                        $weight_limit = $weightData->summary->limit;
                        $weight_next = $weightData->summary->next;

                        $totalhitweight = ($weight_result / $limit); // calculate pagination number for validic 
                        $finalweight = $weightSaveArr = array();
                        if ($totalhitweight > 0) {
                            for ($i = 0; $i < $totalhitweight; $i++) {
                                $page = ($i + 1);
                                $weight1 = "weight.json?authentication_token=$validic_access_token&limit=$limit&page=$page&start_date=" . $user_details[0]['validic_updated_date']['weight'];
                                $hitnetuurl1 = $validicUrl . $weight1;
                                $weightJsonData1 = $r1 = array();
                                $weightJsonData1 = $CusConPlug->curlGet($hitnetuurl1);
                                $r1 = json_decode($weightJsonData1);
                                $finalweight = array_merge($finalweight, $r1->weight);
                            }
                        }

                        if ($weight_status == "200" && $weight_result > 0) {
                            $e = 1;
                            foreach ($finalweight as $weightvalue) {
                                $weightSaveArr['UserID'] = $UserId;
                                $weight_timestamp = $weightvalue->timestamp;
                                $weightSaveArr['WeightRecordDate'] = date("Y-m-d", strtotime($weight_timestamp));
                                $weightSaveArr['WeightRecordTime'] = date("H:i:s", strtotime($weight_timestamp));
                                $weight_utc_offset = $weightvalue->utc_offset;
                                $weightSaveArr['weight'] = $weight_weight = ($weightvalue->weight) * 1000;
                                $weightSaveArr['source'] = $weight_source = $weightvalue->source;
                                $weight_source_name = $weightvalue->source_name;
                                $weight_last_update = $weightvalue->last_updated;
                                $weightSaveArr['CreationDate'] = round(microtime(true) * 1000);

                                //Check if there is already an entry
                                $weightArr = array("UserID" => $UserId, "WeightRecordDate" => date("Y-m-d", strtotime($weight_timestamp)), "WeightRecordTime" => date("H:i:s", strtotime($weight_timestamp)), "source" => $weight_source);
                                $weightRecord = $IndWeightTableObj->checkWeightRecordForValidic($weightArr);
                                if (!count($weightRecord)) {
                                    $insweightdat = $IndWeightTableObj->insertWeightRecord($weightSaveArr);
                                }

                                if ($insweightdat) {
                                    if (strtotime($user_details[0]['validic_updated_date']['weight']) < strtotime($weight_last_update)) {
                                        $user_details[0]['validic_updated_date']['weight'] = $weight_last_update;
                                        $dateWeight = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateWeight);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        //   echo "success-weight-".$e."<br>";
                                    }
                                }
                                $e++;
                            }
                        }
                    }


                    /* @author Rachit Jain
                     * Fitness Data recive from validic server of diabetes  
                     * date: 19-03-2015
                     */
                    $user_details[0]['validic_updated_date']['diabetes'] = (isset($user_details[0]['validic_updated_date']['diabetes']) && !empty($user_details[0]['validic_updated_date']['diabetes'])) ? $user_details[0]['validic_updated_date']['diabetes'] : date('Y-m-d', strtotime('-7 day'));

                    $diabetes = "diabetes.json?authentication_token=$validic_access_token&start_date=" . $user_details[0]['validic_updated_date']['diabetes'];
                    $hitrouturl = $validicUrl . $diabetes;
                    $diabetesJsonData = $diabetesData = array();
                    $diabetesJsonData = $CusConPlug->curlGet($hitrouturl);
                    $diabetesData = json_decode($diabetesJsonData);

                    $diabetes_status = $diabetes_message = $diabetes_result = $diabetes_start_date = $diabetes_end_date = $diabetes_offset = $diabetes_limit = $diabetes_next = '';
                    if (count($diabetesData) > 0) {
                        $diabetes_status = $diabetesData->summary->status;
                        $diabetes_message = $diabetesData->summary->message;
                        $diabetes_result = $diabetesData->summary->results;
                        $diabetes_start_date = $diabetesData->summary->start_date;
                        $diabetes_end_date = $diabetesData->summary->end_date;
                        $diabetes_offset = $diabetesData->summary->offset;
                        $diabetes_limit = $diabetesData->summary->limit;
                        $diabetes_next = $diabetesData->summary->next;

                        $totalhitdiabetes = ($diabetes_result / $limit); // calculate pagination number for validic 

                        $finaldiabetes = array(); // final array of validic data diabetes
                        $bloodGlucoseSaveArr = array();
                        $cholesterolSaveArr = array();
                        $diabetesSaveArr = array();
                        if ($totalhitdiabetes > 0) {
                            for ($i = 0; $i < $totalhitdiabetes; $i++) {
                                $page = ($i + 1);
                                $diabetes1 = "diabetes.json?authentication_token=$validic_access_token&limit=$limit&page=$page&start_date=" . $user_details[0]['validic_updated_date']['diabetes'];
                                $hitnetuurl1 = $validicUrl . $diabetes1;
                                $diabetesJsonData1 = $r1 = array();
                                $diabetesJsonData1 = $CusConPlug->curlGet($hitnetuurl1);
                                $r1 = json_decode($diabetesJsonData1);
                                $finaldiabetes = array_merge($finaldiabetes, $r1->diabetes);
                            }
                        }

                        if ($diabetes_status == "200" && $diabetes_result > 0) {
                            $f = 1;
                            foreach ($finaldiabetes as $diabetesvalue) {
                                $diabetesSaveArr['UserID'] = $UserId;
                                $diabetes_timestamp = $diabetesvalue->timestamp;
                                $cholesterolSaveArr['CholesterolRecordDate'] = date("Y-m-d", strtotime($diabetes_timestamp));
                                $cholesterolSaveArr['CholesterolRecordTime'] = date("H:i:s", strtotime($diabetes_timestamp));

                                $bloodGlucoseSaveArr['blood_record_date'] = date("Y-m-d", strtotime($diabetes_timestamp));
                                $bloodGlucoseSaveArr['blood_record_time'] = date("H:i:s", strtotime($diabetes_timestamp));

                                $diabetes_utc_offset = $diabetesvalue->utc_offset;
                                $diabetes_c_peptide = $diabetesvalue->c_peptide;
                                $diabetes_fasting_plasma_glucose_test = $diabetesvalue->fasting_plasma_glucose_test;
                                $diabetes_hba1c = $diabetesvalue->hba1c;
                                $diabetes_insulin = $diabetesvalue->insulin;
                                $diabetes_oral_glucose_tolerance_test = $diabetesvalue->oral_glucose_tolerance_test;
                                $diabetes_random_plasma_glucose_test = $diabetesvalue->random_plasma_glucose_test;

                                $cholesterolSaveArr['Triglycerides'] = $diabetes_triglyceride = (!empty($diabetesvalue->triglyceride) ? $diabetesvalue->triglyceride : 0);
                                $cholesterolSaveArr['source'] = $diabetes_source = $diabetesvalue->source;

                                $bloodGlucoseSaveArr['measurement'] = $diabetes_blood_glucose = (!empty($diabetesvalue->blood_glucose) ? $diabetesvalue->blood_glucose : 0);
                                $bloodGlucoseSaveArr['source'] = $diabetesvalue->source;

                                $diabetes_source_name = $diabetesvalue->source_name;
                                $diabetes_last_update = $diabetesvalue->last_updated;

                                $cholesterolSaveArr['CreationDate'] = round(microtime(true) * 1000);
                                $bloodGlucoseSaveArr['creation_date'] = round(microtime(true) * 1000);

                                //Check if there is already an entry
                                $cholesterolArr = array("UserID" => $UserId, "CholesterolRecordDate" => date("Y-m-d", strtotime($diabetes_timestamp)), "CholesterolRecordTime" => date("H:i:s", strtotime($diabetes_timestamp)), "source" => $diabetes_source);
                                $cholesterolRecord = $IndChoTableObj->checkCholestrolRecordForValidic($cholesterolArr);
                                if (!count($cholesterolRecord)) {
                                    $inscholodat = $IndChoTableObj->insertCholestrolRecord($cholesterolSaveArr);
                                }

                                //Check if there is already an entry
                                $glucoseArr = array("UserID" => $UserId, "blood_record_date" => date("Y-m-d", strtotime($diabetes_timestamp)), "blood_record_time" => date("H:i:s", strtotime($diabetes_timestamp)), "source" => $diabetes_source);
                                $glucoseRecord = $IndBgTableObj->checkBgRecordForValidic($glucoseArr);
                                if (!count($glucoseRecord)) {
                                    $insbgdat = $IndBgTableObj->insertBgRecord($bloodGlucoseSaveArr);
                                }

                                if ($insbgdat || $inscholodat) {
                                    if (strtotime($user_details[0]['validic_updated_date']['diabetes']) < strtotime($diabetes_last_update)) {
                                        $user_details[0]['validic_updated_date']['diabetes'] = $diabetes_last_update;
                                        $datediabetes = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $datediabetes);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-diabetes-".$f."<br>";
                                    }
                                }
                                $f++;
                            }
                        }
                    }


                    /* @author Shivendra Suman
                     * Fitness Data recive from validic server of diabetes  
                     * date: 19-01-2014
                     */
                    $user_details[0]['validic_updated_date']['biometrics'] = (isset($user_details[0]['validic_updated_date']['biometrics']) && !empty($user_details[0]['validic_updated_date']['biometrics'])) ? $user_details[0]['validic_updated_date']['biometrics'] : date('Y-m-d', strtotime('-7 day'));

                    $biometrics = "biometrics.json?authentication_token=$validic_access_token&start_date=" . $user_details[0]['validic_updated_date']['biometrics'];
                    $hitrouturl = $validicUrl . $biometrics;
                    $biometricsJsonData = $biometricsData = array();
                    $biometricsJsonData = $CusConPlug->curlGet($hitrouturl);
                    $biometricsData = json_decode($biometricsJsonData);

                    $biometrics_status = $biometrics_message = $biometrics_result = $biometrics_start_date = $biometrics_end_date = $biometrics_offset = $biometrics_limit = $biometrics_next = '';
                    if (count($biometricsData) > 0) {
                        $biometrics_status = $biometricsData->summary->status;
                        $biometrics_message = $biometricsData->summary->message;
                        $biometrics_result = $biometricsData->summary->results;
                        $biometrics_start_date = $biometricsData->summary->start_date;
                        $biometrics_end_date = $biometricsData->summary->end_date;
                        $biometrics_offset = $biometricsData->summary->offset;
                        $biometrics_limit = $biometricsData->summary->limit;
                        $biometrics_next = $biometricsData->summary->next;

                        $totalhitbiometrics = ($biometrics_result / $limit); // calculate pagination number for validic 

                        $finalbiometrics = array(); // final array of validic data biometrics
                        $biometricsSaveArr = array();
                        $cholesterolSaveArr = array();
                        $bpSaveArr = array();
                        $temperatureSaveArr = array();
                        if ($totalhitbiometrics > 0) {
                            for ($i = 0; $i < $totalhitbiometrics; $i++) {
                                $page = ($i + 1);
                                $biometrics1 = "biometrics.json?authentication_token=$validic_access_token&limit=$limit&page=$page&start_date=" . $user_details[0]['validic_updated_date']['biometrics'];
                                $hitnetuurl1 = $validicUrl . $biometrics1;
                                $biometricsJsonData1 = $r1 = array();
                                $biometricsJsonData1 = $CusConPlug->curlGet($hitnetuurl1);
                                $r1 = json_decode($biometricsJsonData1);
                                $finalbiometrics = array_merge($finalbiometrics, $r1->biometrics);
                            }
                        }

                        if ($biometrics_status == "200" && $biometrics_result > 0) {
                            $g = 1;
                            foreach ($finalbiometrics as $biometricsvalue) {
                                $bpSaveArr['UserID'] = $temperatureSaveArr['UserID'] = $cholesterolSaveArr['UserID'] = $biometricsSaveArr['UserID'] = $UserId;
                                $biometrics_timestamp = $biometricsvalue->timestamp;
                                $bpSaveArr['bp_record_date'] = $temperatureSaveArr['temperature_record_date'] = $cholesterolSaveArr['CholesterolRecordDate'] = $biometricsSaveArr['biometric_record_date'] = date("Y-m-d", strtotime($biometrics_timestamp));
                                $bpSaveArr['bp_record_time'] = $temperatureSaveArr['temperature_record_time'] = $cholesterolSaveArr['CholesterolRecordTime'] = $biometricsSaveArr['biometric_record_time'] = date("H:i:s", strtotime($biometrics_timestamp));
                                $biometrics_utc_offset = $biometricsvalue->utc_offset;

                                $biometricsSaveArr['blood_calcium'] = $biometrics_blood_calcium = $CusConPlug->emptyreturn($biometricsvalue->blood_calcium);
                                $biometricsSaveArr['blood_chromium'] = $CusConPlug->emptyreturn($biometricsvalue->blood_chromium);
                                $biometricsSaveArr['blood_folic_acid'] = $CusConPlug->emptyreturn($biometricsvalue->blood_folic_acid);
                                $biometricsSaveArr['blood_magnesium'] = $CusConPlug->emptyreturn($biometricsvalue->blood_magnesium);
                                $biometricsSaveArr['blood_potassium'] = $CusConPlug->emptyreturn($biometricsvalue->blood_potassium);
                                $biometricsSaveArr['blood_sodium'] = $CusConPlug->emptyreturn($biometricsvalue->blood_sodium);
                                $biometricsSaveArr['blood_vitamin_b12'] = $CusConPlug->emptyreturn($biometricsvalue->blood_vitamin_b12);
                                $biometricsSaveArr['blood_zinc'] = $CusConPlug->emptyreturn($biometricsvalue->blood_zinc);
                                $biometricsSaveArr['creatine_kinase'] = $CusConPlug->emptyreturn($biometricsvalue->creatine_kinase);
                                $biometricsSaveArr['crp'] = $CusConPlug->emptyreturn($biometricsvalue->crp);
                                $diastolic = $CusConPlug->emptyreturn($biometricsvalue->diastolic);
                                $bpSaveArr['diastolic'] = $diastolic;
                                $biometricsSaveArr['ferritin'] = $CusConPlug->emptyreturn($biometricsvalue->ferritin);
                                $cholesterolSaveArr['HDL'] = $biometricsSaveArr['hdl'] = $CusConPlug->emptyreturn($biometricsvalue->hdl);
                                $biometricsSaveArr['hscrp'] = $CusConPlug->emptyreturn($biometricsvalue->hscrp);
                                $biometricsSaveArr['il6'] = $CusConPlug->emptyreturn($biometricsvalue->il6);
                                $cholesterolSaveArr['LDL'] = $biometricsSaveArr['ldl'] = $CusConPlug->emptyreturn($biometricsvalue->ldl);
                                $pulse = $CusConPlug->emptyreturn($biometricsvalue->resting_heartrate);
                                $biometricsSaveArr['resting_heartrate'] = $pulse;
                                $bpSaveArr['pulse'] = $CusConPlug->emptyreturn($biometricsvalue->resting_heartrate);
                                $systolic = $CusConPlug->emptyreturn($biometricsvalue->systolic);
                                $bpSaveArr['systolic'] = $systolic;
                                $biometricsSaveArr['testosterone'] = $CusConPlug->emptyreturn($biometricsvalue->testosterone);
                                $cholesterolSaveArr['TotalCholesterol'] = $biometricsSaveArr['total_cholesterol'] = $CusConPlug->emptyreturn($biometricsvalue->total_cholesterol);
                                $biometricsSaveArr['tsh'] = $CusConPlug->emptyreturn($biometricsvalue->tsh);
                                $biometricsSaveArr['uric_acid'] = $CusConPlug->emptyreturn($biometricsvalue->uric_acid);
                                $biometricsSaveArr['vitamin_d'] = $CusConPlug->emptyreturn($biometricsvalue->vitamin_d);
                                $biometricsSaveArr['white_cell_count'] = $CusConPlug->emptyreturn($biometricsvalue->white_cell_count);
                                $biometricsSaveArr['spo2'] = $CusConPlug->emptyreturn($biometricsvalue->spo2);
                                $temperature = $CusConPlug->emptyreturn($biometricsvalue->temperature);
                                $temperatureSaveArr['temperature'] = $biometricsSaveArr['temperature'] = $temperature;

                                $temperatureSaveArr['source'] = $cholesterolSaveArr['source'] = $bpSaveArr['source'] = $biometricsSaveArr['source'] = $biometrics_source = $biometricsvalue->source;
                                $biometricsSaveArr['source_name'] = $biometric_source_name = $biometricsvalue->source_name;
                                $biometricsSaveArr['last_updated'] = $biometrics_last_update = $biometricsvalue->last_updated;
                                $temperatureSaveArr['creation_date'] = $cholesterolSaveArr['CreationDate'] = $bpSaveArr['creation_date'] = $biometricsSaveArr['creation_date'] = round(microtime(true) * 1000);

                                //Check if there is already an entry
                                $bioArr = array("UserID" => $UserId, "biometric_record_date" => date("Y-m-d", strtotime($biometrics_timestamp)), "biometric_record_time" => date("H:i:s", strtotime($biometrics_timestamp)));
                                $biometricRecord = $IndBiometricTblObj->checkBiRecordForValidic($bioArr);
                                if (!count($biometricRecord)) {
                                    $insbiometricsdat = $IndBiometricTblObj->insertBiRecord($biometricsSaveArr);
                                }

                                /*                                 * ****** Code to get perivous value********** */
                                $IndividualBpTableObj = $this->getServiceLocator()->get('IndividualBloodPressureRecordTable');
                                $wherePre = array('UserID' => $UserId);
                                $PreviousRes = $IndividualBpTableObj->getPreviousPressureRecord($wherePre);
                                if ($PreviousRes) {
                                    $PreSystolic = $PreviousRes['systolic'];
                                    $PreDiostolic = $PreviousRes['diastolic'];
                                    $PrePulse = $PreviousRes['pulse'];
                                } else {
                                    $PreSystolic = 'N/A';
                                    $PreDiostolic = 'N/A';
                                    $PrePulse = 'N/A';
                                }
                                /*                                 * ****** End code ********** */

                                //Check if there is already an entry
//                                $date = date_create($PreviousRes['bp_record_date']);
//                                $Lastdate = date_create($bpSaveArr['bp_record_date']);
//                                $privousDateTime = date_format($date, 'd M, Y') . ", " . date('g:i a', strtotime($PreviousRes['bp_record_time']));
//                                $lastDateTime = date_format($Lastdate, 'd M, Y') . ", " . date('g:i a', strtotime($bpSaveArr['bp_record_time']));

                                $preDate = date("Y-m-d H:i:s", round($PreviousRes['creation_date'] / 1000));
                                $CurDate = date("Y-m-d H:i:s", microtime(true));

                                $PrevCreatedRecDate = $INDCusConPlug->convetLocalTime($preDate);
                                $showDate = $PrevCreatedRecDate[0];
                                $showtime = $PrevCreatedRecDate[1];

                                $CurrCreatedRecDate = $INDCusConPlug->convetLocalTime($CurDate);
                                $CurrshowDate = $CurrCreatedRecDate[0];
                                $Currshowtime = $CurrCreatedRecDate[1];
                                $date = date_create($showDate);
                                $Cudate = date_create($CurrshowDate);
                                //$Lastdate = date_create($bp_record_date);
                                $privousDateTime = date_format($date, 'd M, Y') . ", " . date('g:i a', strtotime($showtime));
                                $lastDateTime = date_format($Cudate, 'd M, Y') . ", " . date('g:i a', strtotime($Currshowtime));

                                //Check if there is already an entry
                                $bpArr = array("UserID" => $UserId, "bp_record_date" => date("Y-m-d", strtotime($biometrics_timestamp)), "bp_record_time" => date("H:i:s", strtotime($biometrics_timestamp)), "source" => $biometrics_source);
                                $bpRecord = $IndBpTableObj->checkBpRecordForValidic($bpArr);
                                if (!count($bpRecord)) {
                                    $bpinsdata = $IndBpTableObj->insertBpRecord($bpSaveArr);
                                }

                                $IndividualTempTableObj = $this->getServiceLocator()->get('IndividualTemperatureRecordTable');
                                $PreviousResult = $IndividualTempTableObj->getPreviousTempRecord($wherePre);
                                if ($PreviousResult) {
                                    $Pretemperature = $PreviousResult['temperature'];
                                    $Pretemperature = $Pretemperature . "&#8457;";
                                } else {
                                    $Pretemperature = 'N/A';
                                }

                                if ($temperatureSaveArr['temperature'] > 0) {
                                    $temperature = $temperatureSaveArr['temperature'];
                                    //Check if there is already an entry
                                    $tempArr = array("UserID" => $UserId, "temperature_record_date" => date("Y-m-d", strtotime($biometrics_timestamp)), "temperature_record_time" => date("H:i:s", strtotime($biometrics_timestamp)), "source" => $biometrics_source);
                                    $tempRecord = $TempTableObj->checktempRecordForValidic($tempArr);
                                    if (!count($tempRecord)) {
                                        $tempinsdata = $TempTableObj->inserttempRecord($temperatureSaveArr);
                                    }
                                } else {
                                    $temperature = $Pretemperature;
                                }


                                //Check if there is already an entry
                                $choArr = array("UserID" => $UserId, "CholesterolRecordDate" => date("Y-m-d", strtotime($biometrics_timestamp)), "CholesterolRecordTime" => date("H:i:s", strtotime($biometrics_timestamp)), "source" => $biometrics_source);
                                $choRecord = $IndChoTableObj->checkCholestrolRecordForValidic($choArr);
                                if (!count($choRecord)) {
                                    if (!empty($cholesterolSaveArr['HDL']) || !empty($cholesterolSaveArr['LDL']) || !empty($cholesterolSaveArr['TotalCholesterol'])) {
                                        $cholostrolinsdata = $IndChoTableObj->insertCholestrolRecord($cholesterolSaveArr);
                                    }
                                }

                                if (!count($choRecord) || !count($tempRecord) || !count($bpRecord)) {

                                    /*                                     * * Code start to get caregive of this users ** */

                                    $CareGiverTableObj = $this->getServiceLocator()->get("IndividualCaregiverTable");
                                    $whereArr = array('care.UserID' => $UserId, 'care.status' => 1);
                                    $CareGiverUserResIds = $CareGiverTableObj->getCaregiverThresholdSysDysPluse($whereArr, array('caregiver_id'), $lookingfor = '', $CusConPlug->GetAge($user_details[0]['Dob']), $user_details[0]['Gender']);
                                    if ($CareGiverUserResIds) {
                                        //$CareShowsDataBasedOnThresholdObj = $this->getServiceLocator()->get("CareShowsDataBasedOnThreshold");
                                        $whereThres = array('UserID' => $UserId);
                                        $sent = "";
                                        $subject = "Bodymeasurement values has been changed";
                                        foreach ($CareGiverUserResIds as $key => $val) {
                                            if ($val['Email']) {
                                                $email = $val['Email'];
                                                $body = 'Hi ' . $val['FirstName'] . ',' . "<br >";
                                                $body.="<br>" . $FirstName . "'s" . " blood pressure,pulse and temperature measurements have changed as follows:" . "<br >";
                                                $body.="<br> Previous measurement date :" . $privousDateTime . " <br >";
                                                $body.="<br> New measurement date :" . $lastDateTime . " <br >";
                                                $body.="<br>" . "<table style='width:50%'>
                                            <tr>
                                              <td><b>Measure</b></td>
                                              <td><b>Previous</b></td>		
                                              <td><b>New</b></td>
                                            </tr>
                                            <tr>
                                              <td>Systolic</td>
                                              <td>$PreSystolic mm Hg</td>		
                                              <td>$systolic mm Hg</td>
                                            </tr>
                                            <tr>
                                              <td>Diastolic</td>
                                              <td>$PreDiostolic mm Hg</td>		
                                              <td>$diastolic mm Hg</td>
                                            </tr>
                                             <tr>
                                              <td>Pulse</td>
                                              <td>$PrePulse bpm</td>		
                                              <td>$pulse bpm</td>
                                            </tr>
                                            <tr>
                                                <td>Temperature</td>
                                                <td>$Pretemperature </td>		
                                                <td>$temperature</td>
                                          </tr>
                                          </table>";
                                                // $body .="<br><br>Thank you<br>";
                                                $body .="<br>Health-ePeople";
                                                $CusConPlug->senInvitationMail($email, $subject, $body);
                                            }
                                        }
                                    }

                                    /*                                     * * Code end to get caregive of this users ** */
                                }
                                /* if ($temperatureSaveArr['temperature'] > 0) {
                                  $tempinsdata = $TempTableObj->inserttempRecord($temperatureSaveArr);
                                  } */

                                if ($insbiometricsdat || $bpinsdata || $cholostrolinsdata || $tempinsdata) {
                                    if (strtotime($user_details[0]['validic_updated_date']['biometrics']) < strtotime($biometrics_last_update)) {
                                        $user_details[0]['validic_updated_date']['biometrics'] = $biometrics_last_update;
                                        $datebiometrics = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $datebiometrics);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-biometric-".$g."<br>";
                                    }
                                }
                                $g++;
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if (!empty($validicID)) {
            if ($sucess === TRUE) {
                return array("errstr" => "Success.", "success" => 1);
            } else {
                return array("errstr" => $error, "success" => 0);
            }
        }
    }

    public function synchumanapi() {

        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
        $HumanBaseUrl = 'https://api.humanapi.co/v1/human/';
        $sucess = TRUE;
        $CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        // $UserId = 1;
        $activity_type_id = 1;
        $mdb = $CusConPlug->mongoConnect();
        $phyCollection = $mdb->hm_physical_acitivity;
        $BGCollection = $mdb->hm_blood_glucose_records;
        $BPCollection = $mdb->hm_blood_pressure_records;
        $BOCollection = $mdb->hm_blood_oxygen_records;
        $BMICollection = $mdb->hm_bmi_records;
        $BodyFatCollection = $mdb->hm_body_fat_records;
        $GeneticTraitsCollection = $mdb->hm_genetic_traints_records;
        $GenotypesCollection = $mdb->hm_genotypes_records;
        $HeartRateCollection = $mdb->hm_heart_rates_records;
        $LocationsCollection = $mdb->hm_locations_records;
        $MealsCollection = $mdb->hm_meals_records;
        $SleepsCollection = $mdb->hm_sleep_records;
        $HeightCollection = $mdb->hm_height_records;
        $WeightCollection = $mdb->hm_weight_records;
        $data = $this->request->getQuery();
        // $access_token = "demo";
        $createdType = 'individual';
        $validicID = $validic_access_token = $valdic_activity_date = $validic_id_push = $UserId = $start_date = $end_date = $page = '';
        $userData = $user_details = array();
        if (isset($data->validic_id)) {

            $validicID = $data->validic_id;
            if (empty($validicID)) {
                throw new Exception("You are not registered on humanApi.");
            }
        }

        try {
            if (empty($validicID)) {
                $userData = $IndUserTableObj->getvalidicconnectedUser();
            } else {
                $whereArrValidic = array("validic_id" => $validicID);
                $userData = $IndUserTableObj->getUser($whereArrValidic);
            }

            foreach ($userData as $key => $val) {
                $validic_id_push = $val['validic_id'];
                $whereArray = array("validic_id" => $validic_id_push);
                $coloumnarray = array("UserID", "validic_id", "validic_access_token", "validic_updated_date", "CreationDate", "Dob", "Gender", "FirstName");
                $user_details = $IndUserTableObj->getUser($whereArray, $coloumnarray);
                $UserId = $user_details[0]['UserID'];
                $FirstName = $user_details[0]['FirstName'];
                $validic_access_token = $user_details[0]['validic_access_token'];
                $access_token = $validic_access_token;
                $user_details[0]['validic_updated_date'] = ($CusConPlug->isJSON($user_details[0]['validic_updated_date']) ? json_decode($user_details[0]['validic_updated_date'], true) : "");

                $limit = 100; // get 100 data in each hit from validic server
                //update user table for last activity date
                $whereUserAr = array("UserID" => $UserId);


                /*                 * *******************ACTIVTY DATA START ********************************* */
                // Get a list of activities the user has
                //GET https://api.humanapi.co/v1/human/activities
                //Returns a single activity
                //GET https://api.humanapi.co/v1/human/activities/{id}
                if (!empty($validic_access_token)) {
                    $activity_url = $HumanBaseUrl . "activities?access_token=$access_token";
                    $activityJsonData = array();
                    $activityData = array();
                    $error = "some error occured";
                    //echo $activity_url;exit;
                    $activityJsonData = $CusConPlug->httpGet($activity_url);

                    $activityData = json_decode($activityJsonData);
                    if (count($activityData) > 0) {
                        $a = 1;
                        foreach ($activityData as $activityValue) {
                            $physical_activity_id = ((isset($activityValue->id) && !empty($activityValue->id)) ? $activityValue->id : '');
                            $human_userId = ((isset($activityValue->userId) && !empty($activityValue->userId)) ? $activityValue->userId : '');
                            $activity_startTime = ((isset($activityValue->startTime) && !empty($activityValue->startTime)) ? $activityValue->startTime : '');
                            $activity_endTime = ((isset($activityValue->endTime) && !empty($activityValue->endTime)) ? $activityValue->endTime : '');
                            $activity_tzOffset = ((isset($activityValue->tzOffset) && !empty($activityValue->tzOffset)) ? $activityValue->tzOffset : '');
                            $activity_type = ((isset($activityValue->type) && !empty($activityValue->type)) ? $activityValue->type : '');
                            $activity_source = ((isset($activityValue->source) && !empty($activityValue->source)) ? $activityValue->source : '');
                            $activity_duration = ((isset($activityValue->duration) && !empty($activityValue->duration)) ? $activityValue->duration : '');
                            $activity_distance = ((isset($activityValue->distance) && !empty($activityValue->distance)) ? $activityValue->distance : '');
                            $activity_steps = ((isset($activityValue->steps) && !empty($activityValue->steps)) ? $activityValue->steps : '');
                            $activity_calories = ((isset($activityValue->calories) && !empty($activityValue->calories)) ? $activityValue->calories : '');
                            $activity_createdAt = ((isset($activityValue->createdAt) && !empty($activityValue->createdAt)) ? $activityValue->createdAt : '');
                            $activity_updatedAt = ((isset($activityValue->updatedAt) && !empty($activityValue->updatedAt)) ? $activityValue->updatedAt : '');
                            $activity_humanId = ((isset($activityValue->humanId) && !empty($activityValue->humanId)) ? $activityValue->humanId : '');

                            $ExerciseRecorddate = date("Y-m-d", strtotime($activity_startTime));
                            $ExerciseRecordtime = date("H:i:s", strtotime($activity_startTime));
                            $savePhyData = array(
                                'pysical_activity_id' => $physical_activity_id,
                                'UserID' => $UserId,
                                'created_type' => $createdType,
                                'created_by' => $UserId,
                                'ActivityType' => $activity_type,
                                'ActivityTypeID' => $activity_type_id,
                                'Intensity' => '',
                                'ExerciseRecorddate' => $ExerciseRecorddate,
                                'ExerciseRecordtime' => $ExerciseRecordtime,
                                'Description' => '',
                                'Duration' => $activity_duration,
                                'Distance' => $activity_distance,
                                'NumberOfSteps' => $activity_steps,
                                'CaloriesBurned' => $activity_calories,
                                'source' => $activity_source,
                                'Note' => '',
                                'tzOffset' => $activity_tzOffset,
                                'human_user_id' => $human_userId,
                                'human_start_Time' => $activity_startTime,
                                'human_endTime' => $activity_endTime,
                                'humanId' => $activity_humanId,
                                'created_at' => $activity_createdAt,
                                'updated_at' => $activity_updatedAt,
                                'creationDate' => round(microtime(true) * 1000)
                            );
                            //Check if there is already an entry
                            $paArr = array("UserID" => $UserId, "pysical_activity_id" => $physical_activity_id);
                            $physicalActivityRecord = $phyCollection->find($paArr);
                            if (!count($physicalActivityRecord)) {
                                $phyIns = $phyCollection->insert($savePhyData);
                                if ($phyIns) {
                                    if (strtotime($user_details[0]['validic_updated_date']['activity']) < strtotime($activity_updatedAt)) {
                                        $user_details[0]['validic_updated_date']['activity'] = $activity_updatedAt;
                                        $dateActivity = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateActivity);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        //  echo "success-activaty-".$a."<br>";
                                    }
                                }
                            }
                            $a++;
                        }
                    }
                    //exit;
                    /*                     * *******************ACTIVTY DATA END ********************************* */

                    /*                     * *******************BLOOD GLUCOSE DATA START ********************************* */
                    //Returns the latest blood glucose reading
                    //GET https://api.humanapi.co/v1/human/blood_glucose
                    //  Returns all blood glucose readings
                    //GET https://api.humanapi.co/v1/human/blood_glucose/readings

                    $blood_glucose_url = $HumanBaseUrl . "blood_glucose?access_token=$access_token";
                    $blood_glucoseJsonData = array();
                    $blood_glucoseData = array();
                    $error = "some error occured";
                    $blood_glucoseJsonData = $CusConPlug->httpGet($blood_glucose_url);
                    $blood_glucoseData = json_decode($blood_glucoseJsonData);
                    if (count($blood_glucoseData) > 0) {

                        foreach ($blood_glucoseData as $blood_glucoseValue) {
                            $human_blood_glucose_id = ((isset($blood_glucoseValue->id) && !empty($blood_glucoseValue->id)) ? $blood_glucoseValue->id : '');
                            $human_userId = ((isset($blood_glucoseValue->userId) && !empty($blood_glucoseValue->userId)) ? $blood_glucoseValue->userId : '');
                            $blood_glucose_timestamp = ((isset($blood_glucoseValue->timestamp) && !empty($blood_glucoseValue->timestamp)) ? $blood_glucoseValue->timestamp : '');
                            $blood_glucose_tzOffset = ((isset($blood_glucoseValue->tzOffset) && !empty($blood_glucoseValue->tzOffset)) ? $blood_glucoseValue->tzOffset : '');
                            $blood_glucose_value = ((isset($blood_glucoseValue->value) && !empty($blood_glucoseValue->value)) ? $blood_glucoseValue->value : 0);
                            $blood_glucose_source = ((isset($blood_glucoseValue->source) && !empty($blood_glucoseValue->source)) ? $blood_glucoseValue->source : '');
                            $blood_glucose_unit = ((isset($blood_glucoseValue->unit) && !empty($blood_glucoseValue->unit)) ? $blood_glucoseValue->unit : '');
                            $blood_glucose_createdAt = ((isset($blood_glucoseValue->createdAt) && !empty($blood_glucoseValue->createdAt)) ? $blood_glucoseValue->createdAt : '');
                            $blood_glucose_updatedAt = ((isset($blood_glucoseValue->updatedAt) && !empty($blood_glucoseValue->updatedAt)) ? $blood_glucoseValue->updatedAt : '');
                            $blood_glucose_humanId = ((isset($blood_glucoseValue->humanId) && !empty($blood_glucoseValue->humanId)) ? $blood_glucoseValue->humanId : '');
                            $blood_glucose_record_date = date('Y-m-d', strtotime($blood_glucose_timestamp));
                            $blood_glucose_record_time = date('H:i:s', strtotime($blood_glucose_timestamp));
                            $BloodGlucoseData = array(
                                'blood_glucose_id' => $human_blood_glucose_id,
                                'UserID' => $UserId,
                                'created_type' => $createdType,
                                'created_by' => $UserId,
                                'measurement' => $blood_glucose_value,
                                'unit' => $blood_glucose_unit,
                                'measurement_context' => '',
                                'type' => 0,
                                'source' => $blood_glucose_source,
                                'blood_record_date' => $blood_glucose_record_date,
                                'blood_record_time' => $blood_glucose_record_time,
                                'note' => '',
                                'tzOffset' => $blood_glucose_tzOffset,
                                'human_user_id' => $human_userId,
                                'blood_glucose_timestamp' => $blood_glucose_timestamp,
                                'humanId' => $blood_glucose_humanId,
                                'created_at' => $blood_glucose_createdAt,
                                'updated_at' => $blood_glucose_updatedAt,
                                'updation_date' => '',
                                'creation_date' => round(microtime(true) * 1000)
                            );

                            //Check if there is already an entry
                            $bgArr = array("UserID" => $UserId, "blood_glucose_id" => $human_blood_glucose_id);
                            $bloorGlucoseRecord = $BGCollection->find($bgArr);
                            if (!count($bloorGlucoseRecord)) {
                                $BgDataIns = $BGCollection->insert($BloodGlucoseData);
                                if ($BgDataIns) {
                                    if (strtotime($user_details[0]['validic_updated_date']['blood_glucose']) < strtotime($blood_glucose_updatedAt)) {
                                        $user_details[0]['validic_updated_date']['blood_glucose'] = $blood_glucose_updatedAt;
                                        $dateBg = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateBg);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-fitness-".$a."<br>";
                                    }
                                }
                            }
                        }
                    }
                    /*                     * *******************BLOOD GLUCOSE DATA END ********************************* */

                    /*                     * *******************BLOOD OXYGEN DATA START ********************************* */
                    //Returns the latest blood oxygen reading
                    //GET https://api.humanapi.co/v1/human/blood_oxygen
                    //  Returns all blood oxygen readings
                    //GET https://api.humanapi.co/v1/human/blood_oxygen/readings

                    $blood_oxygen_url = $HumanBaseUrl . "blood_oxygen?access_token=$access_token";
                    $blood_oxygenJsonData = array();
                    $blood_oxygenData = array();
                    $error = "some error occured";
                    $blood_oxygenJsonData = $CusConPlug->httpGet($blood_oxygen_url);
                    $blood_oxygenData = json_decode($blood_oxygenJsonData);
                    if (count($blood_oxygenData) > 0) {
                        foreach ($blood_oxygenData as $blood_oxygenValue) {
                            $human_blood_oxygen_id = ((isset($blood_oxygenValue->id) && !empty($blood_oxygenValue->id)) ? $blood_oxygenValue->id : '');
                            $human_userId = ((isset($blood_oxygenValue->userId) && !empty($blood_oxygenValue->userId)) ? $blood_oxygenValue->userId : '');
                            $blood_oxygen_startTime = ((isset($blood_oxygenValue->startTime) && !empty($blood_oxygenValue->startTime)) ? $blood_oxygenValue->startTime : '');
                            $blood_oxygen_endTime = ((isset($blood_oxygenValue->endTime) && !empty($blood_oxygenValue->endTime)) ? $blood_oxygenValue->endTime : '');
                            $blood_oxygen_tzOffset = ((isset($blood_oxygenValue->tzOffset) && !empty($blood_oxygenValue->tzOffset)) ? $blood_oxygenValue->tzOffset : '');
                            $blood_oxygen_value = ((isset($blood_oxygenValue->value) && !empty($blood_oxygenValue->value)) ? $blood_oxygenValue->value : '');
                            $blood_oxygen_source = ((isset($blood_oxygenValue->source) && !empty($blood_oxygenValue->source)) ? $blood_oxygenValue->source : '');
                            $blood_oxygen_unit = ((isset($blood_oxygenValue->unit) && !empty($blood_oxygenValue->unit)) ? $blood_oxygenValue->unit : '');
                            $blood_oxygen_createdAt = ((isset($blood_oxygenValue->createdAt) && !empty($blood_oxygenValue->createdAt)) ? $blood_oxygenValue->createdAt : '');
                            $blood_oxygen_updatedAt = ((isset($blood_oxygenValue->updatedAt) && !empty($blood_oxygenValue->updatedAt)) ? $blood_oxygenValue->updatedAt : '');
                            $blood_oxygen_humanId = ((isset($blood_oxygenValue->humanId) && !empty($blood_oxygenValue->humanId)) ? $blood_oxygenValue->humanId : '');
                        }
                    }
                    /*                     * *******************BLOOD OXYGEN DATA END ********************************* */

                    /*                     * *******************BLOOD PRESSURE DATA START ********************************* */

                    //Returns the latest blood pressure reading
                    //GET https://api.humanapi.co/v1/human/blood_pressure
                    //  Returns all blood pressure readings
                    //GET https://api.humanapi.co/v1/human/blood_pressure/readings

                    $blood_pressure_url = $HumanBaseUrl . "blood_pressure?access_token=$access_token";
                    $blood_pressureJsonData = array();
                    $blood_pressureData = array();
                    $error = "some error occured";
                    $blood_pressureJsonData = $CusConPlug->httpGet($blood_pressure_url);
                    $blood_pressureData = json_decode($blood_pressureJsonData);
                    if (count($blood_pressureData) > 0) {
                        foreach ($blood_pressureData as $blood_pressureValue) {
                            $human_blood_pressure_id = ((isset($blood_pressureValue->id) && !empty($blood_pressureValue->id)) ? $blood_pressureValue->id : '');
                            $human_userId = ((isset($blood_pressureValue->userId) && !empty($blood_pressureValue->userId)) ? $blood_pressureValue->userId : '');
                            $blood_pressure_timestamp = ((isset($blood_pressureValue->timestamp) && !empty($blood_pressureValue->timestamp)) ? $blood_pressureValue->timestamp : '');
                            $blood_pressure_tzOffset = ((isset($blood_pressureValue->tzOffset) && !empty($blood_pressureValue->tzOffset)) ? $blood_pressureValue->tzOffset : '');
                            $blood_pressure_systolic = ((isset($blood_pressureValue->systolic) && !empty($blood_pressureValue->systolic)) ? $blood_pressureValue->systolic : '');
                            $blood_pressure_diastolic = ((isset($blood_pressureValue->diastolic) && !empty($blood_pressureValue->diastolic)) ? $blood_pressureValue->diastolic : '');
                            $blood_pressure_unit = ((isset($blood_pressureValue->unit) && !empty($blood_pressureValue->unit)) ? $blood_pressureValue->unit : '');
                            $blood_pressure_heartRate = ((isset($blood_pressureValue->heartRate) && !empty($blood_pressureValue->heartRate)) ? $blood_pressureValue->heartRate : '');
                            $blood_pressure_source = ((isset($blood_pressureValue->source) && !empty($blood_pressureValue->source)) ? $blood_pressureValue->source : '');
                            $blood_pressure_createdAt = ((isset($blood_pressureValue->createdAt) && !empty($blood_pressureValue->createdAt)) ? $blood_pressureValue->createdAt : '');
                            $blood_pressure_updatedAt = ((isset($blood_pressureValue->updatedAt) && !empty($blood_pressureValue->updatedAt)) ? $blood_pressureValue->updatedAt : '');
                            $blood_pressure_humanId = ((isset($blood_pressureValue->humanId) && !empty($blood_pressureValue->humanId)) ? $blood_pressureValue->humanId : '');
                            $blood_pressure_record_date = date('Y-d-m', strtotime($blood_pressure_timestamp));
                            $blood_pressure_record_time = date('H:i:s', strtotime($blood_pressure_timestamp));
                            $bloodPressData = array(
                                'blood_pressure_id' => $human_blood_pressure_id,
                                'UserID' => $UserId,
                                'created_type' => $createdType,
                                'created_by' => $UserId,
                                'systolic' => $blood_pressure_systolic,
                                'diastolic' => $blood_pressure_diastolic,
                                'pulse' => $blood_pressure_heartRate,
                                'unit' => $blood_pressure_unit,
                                'Irregular_heartbeat_detected' => '',
                                'bp_record_date' => $blood_pressure_record_date,
                                'bp_record_time' => $blood_pressure_record_time,
                                'timestamp' => $blood_pressure_timestamp,
                                'tzOffset' => $blood_pressure_tzOffset,
                                'source' => $blood_pressure_source,
                                'note' => '',
                                'humanId' => $blood_pressure_humanId,
                                'created_at' => $blood_pressure_createdAt,
                                'updated_at' => $blood_pressure_updatedAt,
                                'creation_date' => round(microtime(true) * 1000),
                                'updation_date' => '',
                            );

                            //Check if there is already an entry
                            $bpArr = array("UserID" => $UserId, "blood_pressure_id" => $human_blood_pressure_id);
                            $bloorPressureRecord = $BPCollection->find($bpArr);
                            if (!count($bloorPressureRecord)) {
                                $BpDataIns = $BPCollection->insert($bloodPressData);
                                if ($BpDataIns) {
                                    if (strtotime($user_details[0]['validic_updated_date']['blood_pressure']) < strtotime($blood_pressure_updatedAt)) {
                                        $user_details[0]['validic_updated_date']['blood_pressure'] = $blood_pressure_updatedAt;
                                        $dateBp = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateBp);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-fitness-".$a."<br>";
                                    }
                                }
                            }
                        }
                    }
                    /*                     * *******************BLOOD PRESSURE DATA END ********************************* */


                    /*                     * *******************BMI DATA START ********************************* */

                    //        Returns the latest bmi reading
                    //
        //GET https://api.humanapi.co/v1/human/bmi
                    //
        //Returns all bmi readings
                    //
        //GET https://api.humanapi.co/v1/human/bmi/readings

                    $bmi_url = $HumanBaseUrl . "bmi?access_token=$access_token";
                    $bmi_pressureJsonData = array();
                    $bmi_pressureData = array();
                    $error = "some error occured";
                    $bmi_pressureJsonData = $CusConPlug->httpGet($bmi_url);
                    $bmi_pressureData = json_decode($bmi_pressureJsonData);
                    if (count($bmi_pressureData) > 0) {
                        foreach ($bmi_pressureData as $bmiValue) {
                            $bmi_id = ((isset($bmiValue->id) && !empty($bmiValue->id)) ? $bmiValue->id : '');
                            $human_userId = ((isset($bmiValue->userId) && !empty($bmiValue->userId)) ? $bmiValue->userId : '');
                            $bmi_timestamp = ((isset($bmiValue->timestamp) && !empty($bmiValue->timestamp)) ? $bmiValue->timestamp : '');
                            $bmi_tzOffset = ((isset($bmiValue->tzOffset) && !empty($bmiValue->tzOffset)) ? $bmiValue->tzOffset : '');
                            $bmi_value = ((isset($bmiValue->value) && !empty($bmiValue->value)) ? $bmiValue->value : '');
                            $bmi_unit = ((isset($bmiValue->unit) && !empty($bmiValue->unit)) ? $bmiValue->unit : '');
                            $bmi_source = ((isset($bmiValue->source) && !empty($bmiValue->source)) ? $bmiValue->source : '');
                            $bmi_createdAt = ((isset($bmiValue->createdAt) && !empty($bmiValue->createdAt)) ? $bmiValue->createdAt : '');
                            $bmi_updatedAt = ((isset($bmiValue->updatedAt) && !empty($bmiValue->updatedAt)) ? $bmiValue->updatedAt : '');
                            $bmi_humanId = ((isset($bmiValue->humanId) && !empty($bmiValue->humanId)) ? $bmiValue->humanId : '');
                            $bmi_record_date = date('Y-d-m', strtotime($bmi_timestamp));
                            $bmi_record_time = date('H:i:s', strtotime($bmi_timestamp));
                            $bmiData = array(
                                'bmi_id' => $bmi_id,
                                'UserID' => $UserId,
                                'created_type' => $createdType,
                                'created_by' => $UserId,
                                'bmi' => $bmi_value,
                                'weight' => '',
                                'height' => '',
                                'waist' => '',
                                'unit' => $bmi_unit,
                                'bmi_record_date' => $bmi_record_date,
                                'bmi_record_time' => $bmi_record_time,
                                'timestamp' => $bmi_timestamp,
                                'tzOffset' => $bmi_tzOffset,
                                'source' => $bmi_source,
                                'note' => '',
                                'humanId' => $bmi_humanId,
                                'created_at' => $bmi_createdAt,
                                'updated_at' => $bmi_updatedAt,
                                'creation_date' => round(microtime(true) * 1000),
                                'updation_date' => '',
                            );
                            //Check if there is already an entry
                            $bmiArr = array("UserID" => $UserId, "bmi_id" => $bmi_id);
                            $bmiRecord = $BMICollection->find($bmiArr);
                            if (!count($bmiRecord)) {
                                $BmiDataIns = $BMICollection->insert($bmiData);
                                if ($BmiDataIns) {
                                    if (strtotime($user_details[0]['validic_updated_date']['bmi']) < strtotime($bmi_updatedAt)) {
                                        $user_details[0]['validic_updated_date']['bmi'] = $bmi_updatedAt;
                                        $dateBmi = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateBmi);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-fitness-".$a."<br>";
                                    }
                                }
                            }
                        }
                    }
                    /*                     * *******************BMI PRESSURE DATA END ********************************* */

                    /*                     * *******************BODY FAT DATA START ********************************* */

                    //        Returns the latest body_fat reading
                    //
        //GET https://api.humanapi.co/v1/human/body_fat
                    //
        //Returns all body_fat readings
                    //
        //GET https://api.humanapi.co/v1/human/body_fat/readings

                    $body_fat_url = $HumanBaseUrl . "body_fat?access_token=$access_token";
                    $body_fat_pressureJsonData = array();
                    $body_fat_pressureData = array();
                    $error = "some error occured";
                    $body_fatJsonData = $CusConPlug->httpGet($body_fat_url);
                    $body_fatData = json_decode($body_fatJsonData);
                    if (count($body_fatData) > 0) {
                        foreach ($body_fatData as $body_fatValue) {
                            $body_fat_id = ((isset($body_fatValue->id) && !empty($body_fatValue->id)) ? $body_fatValue->id : '');
                            $human_userId = ((isset($body_fatValue->userId) && !empty($body_fatValue->userId)) ? $body_fatValue->userId : '');
                            $body_fat_timestamp = ((isset($body_fatValue->timestamp) && !empty($body_fatValue->timestamp)) ? $body_fatValue->timestamp : '');
                            $body_fat_tzOffset = ((isset($body_fatValue->tzOffset) && !empty($body_fatValue->tzOffset)) ? $body_fatValue->tzOffset : '');
                            $body_fat_value = ((isset($body_fatValue->value) && !empty($body_fatValue->value)) ? $body_fatValue->value : '');
                            $body_fat_unit = ((isset($body_fatValue->unit) && !empty($body_fatValue->unit)) ? $body_fatValue->unit : '');
                            $body_fat_source = ((isset($body_fatValue->source) && !empty($body_fatValue->source)) ? $body_fatValue->source : '');
                            $body_fat_createdAt = ((isset($body_fatValue->createdAt) && !empty($body_fatValue->createdAt)) ? $body_fatValue->createdAt : '');
                            $body_fat_updatedAt = ((isset($body_fatValue->updatedAt) && !empty($body_fatValue->updatedAt)) ? $body_fatValue->updatedAt : '');
                            $body_fat_humanId = ((isset($body_fatValue->humanId) && !empty($body_fatValue->humanId)) ? $body_fatValue->humanId : '');
                            $body_fat_record_date = date('Y-d-m', strtotime($body_fat_timestamp));
                            $body_fat_record_time = date('H:i:s', strtotime($body_fat_timestamp));
                            $body_fatData = array(
                                'body_fat_id' => $body_fat_id,
                                'UserID' => $UserId,
                                'created_type' => $createdType,
                                'created_by' => $UserId,
                                'body_fat' => $body_fat_value,
                                'unit' => $body_fat_unit,
                                'body_fat_record_date' => $body_fat_record_date,
                                'body_fat_record_time' => $body_fat_record_time,
                                'timestamp' => $body_fat_timestamp,
                                'tzOffset' => $body_fat_tzOffset,
                                'source' => $body_fat_source,
                                'note' => '',
                                'humanId' => $body_fat_humanId,
                                'created_at' => $body_fat_createdAt,
                                'updated_at' => $body_fat_updatedAt,
                                'creation_date' => round(microtime(true) * 1000),
                                'updation_date' => '',
                            );
                            //Check if there is already an entry
                            $bodyfatArr = array("UserID" => $UserId, "body_fat_id" => $body_fat_id);
                            $bodyfatRecord = $BodyFatCollection->find($bodyfatArr);
                            if (!count($bodyfatRecord)) {
                                $BodayfatDataIns = $BodyFatCollection->insert($body_fatData);
                                if ($BodayfatDataIns) {
                                    if (strtotime($user_details[0]['validic_updated_date']['body_fat']) < strtotime($body_fat_updatedAt)) {
                                        $user_details[0]['validic_updated_date']['body_fat'] = $body_fat_updatedAt;
                                        $dateBodyFat = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateBodyFat);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-fitness-".$a."<br>";
                                    }
                                }
                            }
                        }
                    }
                    /*                     * *******************BODDY FAT DATA END ********************************* */

                    /*                     * *******************HART RATE DATA START ********************************* */

                    //        Returns the latest heart_rate reading
                    //
        //GET https://api.humanapi.co/v1/human/heart_rate
                    //
        //Returns all heart_rate readings
                    //
        //GET https://api.humanapi.co/v1/human/heart_rate/readings

                    $heart_rate_url = $HumanBaseUrl . "heart_rate?access_token=$access_token";
                    $heart_rate_pressureJsonData = array();
                    $heart_rate_pressureData = array();
                    $error = "some error occured";
                    $heart_rateJsonData = $CusConPlug->httpGet($heart_rate_url);
                    $heart_rateData = json_decode($heart_rateJsonData);
                    if (count($heart_rateData) > 0) {
                        foreach ($heart_rateData as $heart_rateValue) {
                            $heart_rate_id = ((isset($heart_rateValue->id) && !empty($heart_rateValue->id)) ? $heart_rateValue->id : '');
                            $human_userId = ((isset($heart_rateValue->userId) && !empty($heart_rateValue->userId)) ? $heart_rateValue->userId : '');
                            $heart_rate_timestamp = ((isset($heart_rateValue->timestamp) && !empty($heart_rateValue->timestamp)) ? $heart_rateValue->timestamp : '');
                            $heart_rate_tzOffset = ((isset($heart_rateValue->tzOffset) && !empty($heart_rateValue->tzOffset)) ? $heart_rateValue->tzOffset : '');
                            $heart_rate_value = ((isset($heart_rateValue->value) && !empty($heart_rateValue->value)) ? $heart_rateValue->value : '');
                            $heart_rate_unit = ((isset($heart_rateValue->unit) && !empty($heart_rateValue->unit)) ? $heart_rateValue->unit : '');
                            $heart_rate_source = ((isset($heart_rateValue->source) && !empty($heart_rateValue->source)) ? $heart_rateValue->source : '');
                            $heart_rate_createdAt = ((isset($heart_rateValue->createdAt) && !empty($heart_rateValue->createdAt)) ? $heart_rateValue->createdAt : '');
                            $heart_rate_updatedAt = ((isset($heart_rateValue->updatedAt) && !empty($heart_rateValue->updatedAt)) ? $heart_rateValue->updatedAt : '');
                            $heart_rate_humanId = ((isset($heart_rateValue->humanId) && !empty($heart_rateValue->humanId)) ? $heart_rateValue->humanId : '');
                            $heart_rate_record_date = date('Y-d-m', strtotime($heart_rate_timestamp));
                            $heart_rate_record_time = date('H:i:s', strtotime($heart_rate_timestamp));
                            $heart_rateData = array(
                                'heart_rate_id' => $heart_rate_id,
                                'UserID' => $UserId,
                                'created_type' => $createdType,
                                'created_by' => $UserId,
                                'heart_rate' => $heart_rate_value,
                                'unit' => $heart_rate_unit,
                                'heart_rate_record_date' => $heart_rate_record_date,
                                'heart_rate_record_time' => $heart_rate_record_time,
                                'timestamp' => $heart_rate_timestamp,
                                'tzOffset' => $heart_rate_tzOffset,
                                'source' => $heart_rate_source,
                                'note' => '',
                                'humanId' => $heart_rate_humanId,
                                'created_at' => $heart_rate_createdAt,
                                'updated_at' => $heart_rate_updatedAt,
                                'creation_date' => round(microtime(true) * 1000),
                                'updation_date' => '',
                            );
                            //Check if there is already an entry
                            $heartRateArr = array("UserID" => $UserId, "heart_rate_id" => $heart_rate_id);
                            $heartRateRecord = $HeartRateCollection->find($heartRateArr);
                            if (!count($heartRateRecord)) {
                                $hearterateDataIns = $HeartRateCollection->insert($heart_rateData);
                                if ($hearterateDataIns) {
                                    if (strtotime($user_details[0]['validic_updated_date']['heart_rate']) < strtotime($heart_rate_updatedAt)) {
                                        $user_details[0]['validic_updated_date']['heart_rate'] = $heart_rate_updatedAt;
                                        $dateHeartRate = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateHeartRate);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-fitness-".$a."<br>";
                                    }
                                }
                            }
                        }
                    }
                    /*                     * *******************HEART RATE DATA END ********************************* */


                    /*                     * *******************HEIGHT DATA START ********************************* */

                    //        Returns the latest height reading
                    //
        //GET https://api.humanapi.co/v1/human/height
                    //
        //Returns all height readings
                    //
        //GET https://api.humanapi.co/v1/human/height/readings

                    $height_url = $HumanBaseUrl . "height?access_token=$access_token";
                    $height_pressureJsonData = array();
                    $height_pressureData = array();
                    $error = "some error occured";
                    $heightJsonData = $CusConPlug->httpGet($height_url);
                    $heightData = json_decode($heightJsonData);
                    if (count($heightData) > 0) {
                        foreach ($heightData as $heightValue) {
                            $height_id = ((isset($heightValue->id) && !empty($heightValue->id)) ? $heightValue->id : '');
                            $human_userId = ((isset($heightValue->userId) && !empty($heightValue->userId)) ? $heightValue->userId : '');
                            $height_timestamp = ((isset($heightValue->timestamp) && !empty($heightValue->timestamp)) ? $heightValue->timestamp : '');
                            $height_tzOffset = ((isset($heightValue->tzOffset) && !empty($heightValue->tzOffset)) ? $heightValue->tzOffset : '');
                            $height_value = ((isset($heightValue->value) && !empty($heightValue->value)) ? $heightValue->value : '');
                            $height_unit = ((isset($heightValue->unit) && !empty($heightValue->unit)) ? $heightValue->unit : '');
                            $height_source = ((isset($heightValue->source) && !empty($heightValue->source)) ? $heightValue->source : '');
                            $height_createdAt = ((isset($heightValue->createdAt) && !empty($heightValue->createdAt)) ? $heightValue->createdAt : '');
                            $height_updatedAt = ((isset($heightValue->updatedAt) && !empty($heightValue->updatedAt)) ? $heightValue->updatedAt : '');
                            $height_humanId = ((isset($heightValue->humanId) && !empty($heightValue->humanId)) ? $heightValue->humanId : '');
                            $height_record_date = date('Y-d-m', strtotime($height_timestamp));
                            $height_record_time = date('H:i:s', strtotime($height_timestamp));
                            $heightData = array(
                                'height_id' => $height_id,
                                'UserID' => $UserId,
                                'created_type' => $createdType,
                                'created_by' => $UserId,
                                'Height' => $height_value,
                                'Unit' => $height_unit,
                                'HeightRecordDate' => $height_record_date,
                                'HeightRecordTime' => $height_record_time,
                                'timestamp' => $height_timestamp,
                                'tzOffset' => $height_tzOffset,
                                'source' => $height_source,
                                'note' => '',
                                'humanId' => $height_humanId,
                                'created_at' => $height_createdAt,
                                'updated_at' => $height_updatedAt,
                                'creation_date' => round(microtime(true) * 1000),
                                'updation_date' => '',
                            );
                            //Check if there is already an entry
                            $HeightArr = array("UserID" => $UserId, "height_id" => $height_id);
                            $heightRecord = $HeightCollection->find($HeightArr);
                            if (!count($heightRecord)) {
                                $heightDataIns = $HeightCollection->insert($heightData);
                                if ($heightDataIns) {
                                    if (strtotime($user_details[0]['validic_updated_date']['height']) < strtotime($height_updatedAt)) {
                                        $user_details[0]['validic_updated_date']['height'] = $height_updatedAt;
                                        $dateHeartRate = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateHeartRate);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-fitness-".$a."<br>";
                                    }
                                }
                            }
                        }
                    }
                    /*                     * *******************HEIGHT DATA END ********************************* */

                    /*                     * *******************meals DATA START ********************************* */

                    //        Returns the latest meals reading
                    //
        //GET https://api.humanapi.co/v1/human/food/meals
                    //
        //Returns all meals readings
                    //
        //GET https://api.humanapi.co/v1/human/food/meals/{id}

                    $meals_url = $HumanBaseUrl . "food/meals?access_token=$access_token";
                    $meals_pressureJsonData = array();
                    $meals_pressureData = array();
                    $error = "some error occured";
                    $mealsJsonData = $CusConPlug->httpGet($meals_url);
                    $mealsData = json_decode($mealsJsonData);
                    if (count($mealsData) > 0) {
                        foreach ($mealsData as $mealsValue) {
                            $meals_id = ((isset($mealsValue->id) && !empty($mealsValue->id)) ? $mealsValue->id : '');
                            $human_userId = ((isset($mealsValue->userId) && !empty($mealsValue->userId)) ? $mealsValue->userId : '');
                            $meals_timestamp = ((isset($mealsValue->timestamp) && !empty($mealsValue->timestamp)) ? $mealsValue->timestamp : '');
                            $meals_tzOffset = ((isset($mealsValue->tzOffset) && !empty($mealsValue->tzOffset)) ? $mealsValue->tzOffset : '');
                            $meals_type = ((isset($mealsValue->type) && !empty($mealsValue->type)) ? $mealsValue->type : '');
                            $meals_name = ((isset($mealsValue->name) && !empty($mealsValue->name)) ? $mealsValue->name : '');
                            $meals_source = ((isset($mealsValue->source) && !empty($mealsValue->source)) ? $mealsValue->source : '');
                            $meals_calories = ((isset($mealsValue->calories) && !empty($mealsValue->calories)) ? $mealsValue->calories : '');
                            $meals_carbohydrate = ((isset($mealsValue->carbohydrate) && !empty($mealsValue->carbohydrate)) ? $mealsValue->carbohydrate : '');
                            $meals_fat = ((isset($mealsValue->fat) && !empty($mealsValue->fat)) ? $mealsValue->fat : '');
                            $meals_protein = ((isset($mealsValue->protein) && !empty($mealsValue->protein)) ? $mealsValue->protein : '');
                            $meals_sugar = ((isset($mealsValue->sugar) && !empty($mealsValue->sugar)) ? $mealsValue->sugar : '');
                            $meals_calcium = ((isset($mealsValue->calcium) && !empty($mealsValue->calcium)) ? $mealsValue->calcium : '');
                            $meals_sodium = ((isset($mealsValue->sodium) && !empty($mealsValue->sodium)) ? $mealsValue->sodium : '');
                            $meals_cholesterol = ((isset($mealsValue->cholesterol) && !empty($mealsValue->cholesterol)) ? $mealsValue->cholesterol : '');
                            $meals_fiber = ((isset($mealsValue->fiber) && !empty($mealsValue->fiber)) ? $mealsValue->fiber : '');
                            $meals_iron = ((isset($mealsValue->iron) && !empty($mealsValue->iron)) ? $mealsValue->iron : '');
                            $meals_monounsaturatedFat = ((isset($mealsValue->monounsaturatedFat) && !empty($mealsValue->monounsaturatedFat)) ? $mealsValue->monounsaturatedFat : '');
                            $meals_polyunsaturatedFat = ((isset($mealsValue->polyunsaturatedFat) && !empty($mealsValue->polyunsaturatedFat)) ? $mealsValue->polyunsaturatedFat : '');
                            $meals_potassium = ((isset($mealsValue->potassium) && !empty($mealsValue->potassium)) ? $mealsValue->potassium : '');
                            $meals_saturatedFat = ((isset($mealsValue->saturatedFat) && !empty($mealsValue->saturatedFat)) ? $mealsValue->saturatedFat : '');
                            $meals_vitaminA = ((isset($mealsValue->vitaminA) && !empty($mealsValue->vitaminA)) ? $mealsValue->vitaminA : '');
                            $meals_vitaminC = ((isset($mealsValue->vitaminC) && !empty($mealsValue->vitaminC)) ? $mealsValue->vitaminC : '');
                            $meals_createdAt = ((isset($mealsValue->createdAt) && !empty($mealsValue->createdAt)) ? $mealsValue->createdAt : '');
                            $meals_updatedAt = ((isset($mealsValue->updatedAt) && !empty($mealsValue->updatedAt)) ? $mealsValue->updatedAt : '');
                            $meals_humanId = ((isset($mealsValue->humanId) && !empty($mealsValue->humanId)) ? $mealsValue->humanId : '');
                            $meals_record_date = date('Y-d-m', strtotime($meals_timestamp));
                            $meals_record_time = date('H:i:s', strtotime($meals_timestamp));
                            $mealsData = array(
                                'meals_id' => $meals_id,
                                'UserID' => $UserId,
                                'created_type' => $createdType,
                                'created_by' => $UserId,
                                'type' => $meals_type,
                                'name' => $meals_name,
                                'mealsRecordDate' => $meals_record_date,
                                'mealsRecordTime' => $meals_record_time,
                                'timestamp' => $meals_timestamp,
                                'tzOffset' => $meals_tzOffset,
                                'source' => $meals_source,
                                'calories' => $meals_calories,
                                'carbohydrate' => $meals_carbohydrate,
                                'fat' => $meals_fat,
                                'protein' => $meals_protein,
                                'sodium' => $meals_sodium,
                                'sugar' => $meals_sugar,
                                'calcium' => $meals_calcium,
                                'cholesterol' => $meals_cholesterol,
                                'fiber' => $meals_fiber,
                                'iron' => $meals_iron,
                                'monounsaturatedFat' => $meals_monounsaturatedFat,
                                'polyunsaturatedFat' => $meals_polyunsaturatedFat,
                                'potassium' => $meals_potassium,
                                'saturatedFat' => $meals_saturatedFat,
                                'vitaminA' => $meals_vitaminA,
                                'vitaminC' => $meals_vitaminC,
                                'note' => '',
                                'humanId' => $meals_humanId,
                                'created_at' => $meals_createdAt,
                                'updated_at' => $meals_updatedAt,
                                'creation_date' => round(microtime(true) * 1000),
                                'updation_date' => '',
                            );
                            //Check if there is already an entry
                            $MealsArr = array("UserID" => $UserId, "meals_id" => $meals_id);
                            $mealsRecord = $MealsCollection->find($MealsArr);
                            if (!count($mealsRecord)) {
                                $mealsDataIns = $MealsCollection->insert($mealsData);

                                if ($mealsDataIns) {
                                    if (strtotime($user_details[0]['validic_updated_date']['meals']) < strtotime($meals_updatedAt)) {
                                        $user_details[0]['validic_updated_date']['meals'] = $meals_updatedAt;
                                        $dateMeals = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateMeals);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-fitness-".$a."<br>";
                                    }
                                }
                            }
                        }
                    }
                    /*                     * *******************meals DATA END ********************************* */


                    /*                     * *******************sleeps DATA START ********************************* */

                    //        Returns the latest sleeps reading
                    //
        //GET https://api.humanapi.co/v1/human/sleeps
                    //
        //Returns all sleeps readings
                    //
        //GET https://api.humanapi.co/v1/human/sleeps/{id}

                    $sleeps_url = $HumanBaseUrl . "sleeps?access_token=$access_token";
                    $sleeps_pressureJsonData = array();
                    $sleeps_pressureData = array();
                    $error = "some error occured";
                    $sleepsJsonData = $CusConPlug->httpGet($sleeps_url);
                    $sleepsData = json_decode($sleepsJsonData);
                    if (count($sleepsData) > 0) {
                        foreach ($sleepsData as $sleepsValue) {
                            $sleeps_id = ((isset($sleepsValue->id) && !empty($sleepsValue->id)) ? $sleepsValue->id : '');
                            $human_userId = ((isset($sleepsValue->userId) && !empty($sleepsValue->userId)) ? $sleepsValue->userId : '');
                            $sleeps_startTime = ((isset($sleepsValue->startTime) && !empty($sleepsValue->startTime)) ? $sleepsValue->startTime : '');
                            $sleeps_endTime = ((isset($sleepsValue->endTime) && !empty($sleepsValue->endTime)) ? $sleepsValue->endTime : '');
                            $sleeps_tzOffset = ((isset($sleepsValue->tzOffset) && !empty($sleepsValue->tzOffset)) ? $sleepsValue->tzOffset : '');
                            $sleeps_day = ((isset($sleepsValue->day) && !empty($sleepsValue->day)) ? $sleepsValue->day : '');
                            $sleeps_source = ((isset($sleepsValue->source) && !empty($sleepsValue->source)) ? $sleepsValue->source : '');
                            $sleeps_mainSleep = ((isset($sleepsValue->mainSleep) && !empty($sleepsValue->mainSleep)) ? $sleepsValue->mainSleep : '');
                            $sleeps_timeAsleep = ((isset($sleepsValue->timeAsleep) && !empty($sleepsValue->timeAsleep)) ? $sleepsValue->timeAsleep : '');
                            $sleeps_timeAwake = ((isset($sleepsValue->timeAwake) && !empty($sleepsValue->timeAwake)) ? $sleepsValue->timeAwake : '');
                            $sleeps_efficiency = ((isset($sleepsValue->efficiency) && !empty($sleepsValue->efficiency)) ? $sleepsValue->efficiency : '');
                            $sleeps_timeToFallAsleep = ((isset($sleepsValue->timeToFallAsleep) && !empty($sleepsValue->timeToFallAsleep)) ? $sleepsValue->timeToFallAsleep : '');
                            $sleeps_timeAfterWakeup = ((isset($sleepsValue->timeAfterWakeup) && !empty($sleepsValue->timeAfterWakeup)) ? $sleepsValue->timeAfterWakeup : '');
                            $sleeps_timeInBed = ((isset($sleepsValue->timeInBed) && !empty($sleepsValue->timeInBed)) ? $sleepsValue->timeInBed : '');
                            $sleeps_createdAt = ((isset($sleepsValue->createdAt) && !empty($sleepsValue->createdAt)) ? $sleepsValue->createdAt : '');
                            $sleeps_updatedAt = ((isset($sleepsValue->updatedAt) && !empty($sleepsValue->updatedAt)) ? $sleepsValue->updatedAt : '');
                            $sleeps_humanId = ((isset($sleepsValue->humanId) && !empty($sleepsValue->humanId)) ? $sleepsValue->humanId : '');
                            $sleeps_record_date = date('Y-d-m', strtotime($sleeps_startTime));
                            $sleeps_record_time = date('H:i:s', strtotime($sleeps_startTime));
                            $sleepsData = array(
                                'sleeps_id' => $sleeps_id,
                                'UserID' => $UserId,
                                'created_type' => $createdType,
                                'created_by' => $UserId,
                                'sleep_record_date' => $sleeps_record_date,
                                'sleep_record_time' => $sleeps_record_time,
                                'sleeps_day' => $sleeps_day,
                                'startTime' => $sleeps_startTime,
                                'endTime' => $sleeps_endTime,
                                'tzOffset' => $sleeps_tzOffset,
                                'source' => $sleeps_source,
                                'mainSleep' => $sleeps_mainSleep,
                                'timeAsleep' => $sleeps_timeAsleep,
                                'awake' => $sleeps_timeAwake,
                                'efficiency' => $sleeps_efficiency,
                                'timeToFallAsleep' => $sleeps_timeToFallAsleep,
                                'timeAfterWakeup' => $sleeps_timeAfterWakeup,
                                'timeInBed' => $sleeps_timeInBed,
                                'deep' => '',
                                'light' => '',
                                'rem' => '',
                                'note' => '',
                                'times_woken' => '',
                                'total_sleep' => '',
                                'humanId' => $sleeps_humanId,
                                'created_at' => $sleeps_createdAt,
                                'updated_at' => $sleeps_updatedAt,
                                'creation_date' => round(microtime(true) * 1000),
                                'updation_date' => '',
                            );
                            //Check if there is already an entry
                            $SleepsArr = array("UserID" => $UserId, "sleeps_id" => $sleeps_id);
                            $sleepsRecord = $SleepsCollection->find($SleepsArr);
                            if (!count($sleepsRecord)) {
                                $sleepsDataIns = $SleepsCollection->insert($sleepsData);
                                if ($sleepsDataIns) {
                                    if (strtotime($user_details[0]['validic_updated_date']['sleeps']) < strtotime($sleeps_updatedAt)) {
                                        $user_details[0]['validic_updated_date']['sleeps'] = $sleeps_updatedAt;
                                        $dateSleeps = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateSleeps);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-fitness-".$a."<br>";
                                    }
                                }
                            }
                        }
                    }
                    /*                     * *******************sleeps DATA END ********************************* */

                    /*                     * *******************weight DATA START ********************************* */

                    //        Returns the latest weight reading
                    //
        //GET https://api.humanapi.co/v1/human/weight
                    //
        //Returns all weight readings
                    //
        //GET https://api.humanapi.co/v1/human/weight/readings

                    $weight_url = $HumanBaseUrl . "weight?access_token=$access_token";
                    $weight_pressureJsonData = array();
                    $weight_pressureData = array();
                    $error = "some error occured";
                    $weightJsonData = $CusConPlug->httpGet($weight_url);
                    $weightData = json_decode($weightJsonData);
                    if (count($weightData) > 0) {
                        foreach ($weightData as $weightValue) {
                            $weight_id = ((isset($weightValue->id) && !empty($weightValue->id)) ? $weightValue->id : '');
                            $human_userId = ((isset($weightValue->userId) && !empty($weightValue->userId)) ? $weightValue->userId : '');
                            $weight_timestamp = ((isset($weightValue->timestamp) && !empty($weightValue->timestamp)) ? $weightValue->timestamp : '');
                            $weight_tzOffset = ((isset($weightValue->tzOffset) && !empty($weightValue->tzOffset)) ? $weightValue->tzOffset : '');
                            $weight_value = ((isset($weightValue->value) && !empty($weightValue->value)) ? $weightValue->value : '');
                            $weight_unit = ((isset($weightValue->unit) && !empty($weightValue->unit)) ? $weightValue->unit : '');
                            $weight_source = ((isset($weightValue->source) && !empty($weightValue->source)) ? $weightValue->source : '');
                            $weight_createdAt = ((isset($weightValue->createdAt) && !empty($weightValue->createdAt)) ? $weightValue->createdAt : '');
                            $weight_updatedAt = ((isset($weightValue->updatedAt) && !empty($weightValue->updatedAt)) ? $weightValue->updatedAt : '');
                            $weight_humanId = ((isset($weightValue->humanId) && !empty($weightValue->humanId)) ? $weightValue->humanId : '');
                            $weight_record_date = date('Y-d-m', strtotime($weight_timestamp));
                            $weight_record_time = date('H:i:s', strtotime($weight_timestamp));
                            $weightData = array(
                                'weight_id' => $weight_id,
                                'UserID' => $UserId,
                                'created_type' => $createdType,
                                'created_by' => $UserId,
                                'Weight' => $weight_value,
                                'Unit' => $weight_unit,
                                'WeightRecordDate' => $weight_record_date,
                                'WeightRecordTime' => $weight_record_time,
                                'timestamp' => $weight_timestamp,
                                'tzOffset' => $weight_tzOffset,
                                'source' => $weight_source,
                                'note' => '',
                                'humanId' => $weight_humanId,
                                'created_at' => $weight_createdAt,
                                'updated_at' => $weight_updatedAt,
                                'CreationDate' => round(microtime(true) * 1000),
                                'UpdationDate' => '',
                            );
                            //Check if there is already an entry
                            $WeightArr = array("UserID" => $UserId, "weight_id" => $weight_id);
                            $WeightRecord = $WeightCollection->find($WeightArr);
                            if (!count($WeightRecord)) {
                                $weightDataIns = $WeightCollection->insert($weightData);
                                if ($weightDataIns) {
                                    if (strtotime($user_details[0]['validic_updated_date']['weight']) < strtotime($weight_updatedAt)) {
                                        $user_details[0]['validic_updated_date']['weight'] = $weight_updatedAt;
                                        $dateWeight = json_encode($user_details[0]['validic_updated_date']);
                                        $clooumUpadate = array('validic_updated_date' => $dateWeight);
                                        $IndUserTableObj->updateUser($whereUserAr, $clooumUpadate);
                                        // echo "success-fitness-".$a."<br>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            /*             * *******************weight DATA END ********************************* */
        } catch (Exception $e) {
            $sucess = FALSE;
            $error = $e->getMessage();
        }

        if (!empty($validicID)) {
            if ($sucess === TRUE) {
                return array("errstr" => "Success.", "success" => 1);
            } else {
                return array("errstr" => $error, "success" => 0);
            }
        }

        // die();
    }

}
