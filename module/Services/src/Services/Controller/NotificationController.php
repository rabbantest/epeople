<?php
/* * ***********************
 * PAGE: USE TO MANAGE THE NOTIFICATIONS.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain.
 * CREATOR: 29/01/2015.
 * UPDATED:  .
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\EventManager\EventManagerInterface;
use Zend\View\Model\JsonModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Mail;
use \Exception;

class NotificationController extends AbstractRestfulController
{

    public function __construct() {
		
    }


	public function getList(){
		
    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id){

	}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);
		//echo '<pre>';print_r($requestArr);exit;

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'getnotifications':
				$responseArr = $this->getnotifications($service_key);
				break;
			case 'readnotification':
				$responseArr = $this->readnotification($service_key);
				break;
			case 'deletenotification':
				$responseArr = $this->deletenotification($service_key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}


	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {

	}


	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}


	/*
      @ Used:This function is used to get notifications
      @ Created : 03-02-2015
      @ By : Rachit Jain
     */
    public function getnotifications($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");

        try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID)){
				throw new Exception("Please enter user id.");
			}

			$whereArr = array('receiver_id'=>$UserID, 'user_type'=>1, 'pass_id'=>1, 'is_read'=>0);

			$totalCount = count($NotificationTableObj->getNotifications($whereArr,array(),'',array(),'',''));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$notificationlist = array();
			$notificationlist = $NotificationTableObj->getNotifications($whereArr,array(),'',array(),$lowerLimit,$maxLimit);

			$i=0;
			foreach($notificationlist as $key1=>$val1){
				//Get full image url
				$image = "";
				if($val1['Image'] != ""){
					$server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
					$image = $server_url."/upload_image/".$val1['sender_id']."/".$val1['Image'];
				}
				$notificationlist[$i]['Image'] = $image;

				//Modified Time
				$originalTime =round($val1['created_date']/1000);
				$finalTime = $plugin->facebook_style_date_time($originalTime);
				$notificationlist[$i]['final_time'] = $finalTime;

				$i++;
			}
			//echo '<pre>';print_r($notificationlist);exit;
		}
        catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"notificationlist"=>$notificationlist,"total"=>$totalpage);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


    /*
      @ Used:This function is used to read notification
      @ Created : 03-02-2015
      @ By : Rachit Jain
     */
    public function readnotification($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");

        try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$NotificationID = isset($data['notification_id']) ? $data['notification_id'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID) || empty($NotificationID)){
				throw new Exception("Required parameter missing.");
			}

			$whereArr = array("receiver_id" => $UserID, 'id' => $NotificationID);
			$NotificationTableObj->updateNotification($whereArr,array('is_read'=>1));
		}
        catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


    /*
      @ Used:This function is used to delete notification
      @ Created : 03-02-2015
      @ By : Rachit Jain
     */
    public function deletenotification($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();  // custom plugin
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");

        try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$NotificationID = isset($data['notification_id']) ? $data['notification_id'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID) || empty($NotificationID)){
				throw new Exception("Required parameter missing.");
			}

			$whereArr = array("sender_id" => $UserID, 'id' => $NotificationID);
			$NotificationTableObj->deleteNotification($whereArr);
		}
        catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }

}
?>
