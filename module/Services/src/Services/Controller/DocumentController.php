<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE DOCUMENT OF INDIVIDUAL  FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain.
 * CREATOR: 04/02/2015.
 * UPDATED: 04/02/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\EventManager\EventManagerInterface;
use Zend\View\Model\JsonModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Mail;
use \Exception;

class DocumentController extends AbstractRestfulController {

    public function __construct(){

    }

    public function getList(){
		
    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id) {

	}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);
		//echo '<pre>';print_r($requestArr);exit;

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'getdocuments':
				$responseArr = $this->getdocuments($service_key);
				break;
			case 'deletedocument':
				$responseArr = $this->deletedocument($service_key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}

	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {
	
	}

	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}


	/*****
     * Action: Get documents
     * @author: Rachit Jain
     * Created Date: 06-02-2015.
     *****/
    public function getdocuments($service_key) {
        $sucess = TRUE;
        $IndDocTableObj = $this->getServiceLocator()->get("ServicesDocumentTable");

        try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : '';
			$keyword = isset($data['keyword']) ? $data['keyword'] : '';
			$cat = isset($data['cat']) ? $data['cat'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if (empty($userId)) {
                throw new Exception("Please enter user id.");
            }
			//category list
			$catList = $IndDocTableObj->getDocCatlist();  

			$whereArray = array();
			$columns = array();
			$lookingfor = "creation_date DESC";
			$searchcontent = array();

			$searchcontent= array('keyword'=>$keyword,'cat' => $cat);

			$whereArray['UserID'] = $userId;
			$whereArray['status'] = 0;
			
			$totalCount = count($IndDocTableObj->getDocRecord($whereArray, $columns, $lookingfor, $searchcontent, '', ''));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}
			
			$docRecord = $IndDocTableObj->getDocRecord($whereArray, $columns, $lookingfor, $searchcontent, $lowerLimit, $maxLimit);

			foreach($docRecord as $key=>$val){
				//Get Full image path
				if($val['filename'] != ""){
					$server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
					$image = $server_url."/uploads/".$userId."/".$val['filename'];
					$docRecord[$key]['filename'] = $image;
				}
				$datecreation = ($val['creation_date']/1000);
                $docRecord[$key]['creation_date'] = date("Y-m-d H:i:s",$datecreation);
			}
			//echo '<pre>';print_r($docRecord);exit;
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"docRecord"=>$docRecord,"catList"=>$catList,"total"=>$totalpage);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


    /****
     * Action: Delete document
     * @author: Rachit Jain
     * Created Date: 06-02-2015.
     *****/
    public function deletedocument($service_key) {
        $sucess = TRUE;
        $IndDocTableObj = $this->getServiceLocator()->get("ServicesDocumentTable");

        try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : '';
			$docId = isset($data['doc_id']) ? $data['doc_id'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($userId) || empty($docId)){
				throw new Exception("Required parameter missing.");
			}

			$whereArr = array("UserID" => $userId,'doc_id'=>$docId);
			$columns = array('status' => 1);
			$res = $IndDocTableObj->updateDoc($whereArr, $columns);
			if(!$res){
				throw new Exception("Some error occured.");
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}
		
		if($sucess === TRUE){
			return array("errstr"=>"Document deleted successfully.","success"=>1);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }

}
