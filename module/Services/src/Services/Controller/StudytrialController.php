<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE DOCUMENT OF INDIVIDUAL  FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rachit Jain.
 * CREATOR: 05/02/2015.
 * UPDATED: 05/02/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use \Exception;

class StudytrialController extends AbstractRestfulController {

    public function __construct() {
	
    }


	public function getList(){
		
    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id){

	}

	/**
	 * Create a new resource
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'getstudytrialscreening':
				$responseArr = $this->getstudytrialscreening($service_key);
				break;
			case 'getstudytrialmain':
				$responseArr = $this->getstudytrialmain($service_key);
				break;
			case 'participatestudytrial':
				$responseArr = $this->participatestudytrial($service_key);
				break;
			case 'getonsitestudytrialcategory':
				$responseArr = $this->getonsitestudytrialcategory($service_key);
				break;
			case 'getonsitestudytrial':
				$responseArr = $this->getonsitestudytrial($service_key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}

	/**
	 * Update an existing resource
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {
	
	}


	/**
	 * Delete an existing resource
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}

	/*
     * @Author:Rachit Jain 
     * @Action: This action used to get screening studies
     */
    public function getstudytrialscreening($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$IndStudObj = $this->getServiceLocator()->get("ServicesStudyTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$domainID = isset($data['domain_id']) ? $data['domain_id'] : '1'; //current domain id
			$condition = isset($data['condition']) ? $data['condition'] : '';
			$subcondition = isset($data['subcondition']) ? $data['subcondition'] : '';
			$location = isset($data['location']) ? $data['location'] : '';
			$start_date = isset($data['start_date']) ? $data['start_date'] : '';
			$end_date = isset($data['end_date']) ? $data['end_date'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID)){
				throw new Exception("Please enter user id.");
			}

			//Check Panel Member 
			$wherePanelArr = array("UserID"=>$UserID);
			$checkPanel = $IndUserTableObj->getUser($wherePanelArr);
			if($checkPanel[0]['PanelMember'] == 0){
				throw new Exception("You are not a panel member.");
			}
			
			$whereArrCheck = array("screening" => 1, "study_trials" => 1, "publish_status" => 1, "created_by" => $domainID, "end_date" => date("Y-m-d"));

			$studyListCheck = $IndStudObj->getStudytrials($whereArrCheck, array("study_id", "screening"), false, array(), 0);
			$stdId = '';
			if (count($studyListCheck) > 0) {
				$creation_date = round(microtime(true) * 1000);
				foreach ($studyListCheck as $svalue) {
					$stdId = $svalue['study_id'];
					$whereArrCheck1 = array("study_id" => $stdId, "user_id" => $UserID);
					$bucketInvi = $IndStudObj->getStudytrialsbuck($whereArrCheck1, array("study_id"), false, array(), 0);
					if (count($bucketInvi) <= 0) {
						$type = ($svalue['screening'] == 0) ? 1 : 0;
						$IndStudObj->insertbucket(array("user_id" => $UserID, "study_id" => $stdId, "bucket" => -2, "is_accepted" => 1, 'type' => $type, 'eligible_for_main' => $type, "creation_date" => $creation_date));
					}
				}
			}

			$searchcontent = array();
			$searchcontent = array('condation' => $condition, 'sub_condation' => $subcondition, 'location' => $location, 'start_date' => $start_date, 'end_date' => $end_date);

			$lookingfor = "start_date DESC";
			$whereArr = array("stud.study_id" => $stdId, "bucket" => -2, "user_id" => $UserID, "screening" => 1, "study_trials" => 1, "publish_status" => 1, "created_by" => $domainID);
			
			$totalCount = count($IndStudObj->getStudylist($whereArr, array(), $lookingfor, $searchcontent,0,"",""));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$studyList = $IndStudObj->getStudylist($whereArr, array(), $lookingfor, $searchcontent, 0,$lowerLimit, $maxLimit);

			foreach($studyList as $key=>$val){
				/////////////////////  Get Status Text Start///////////////////////
				if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
					$labelText = 'Coming Soon..';
				}elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null) && $val['is_accepted']==1){
						$labelText = 'Finished';
					}
					else{
						$labelText = 'Expired';
					}
				}
				else{
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null)){
						$labelText = 'Completed';
					}
					else{
						$labelText = 'Pending';
					}
				}
				$studyList[$key]['labelText'] = $labelText;
				/////////////////////  Get Status Text End///////////////////////
				
				/////////////////////  Get Button Text Start///////////////////////
				if($val['is_accepted']==1){
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null)){
						$buttonText = 'Finished';
					}
					else{
						if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
							$buttonText = 'Coming soon..';
						}
						elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
							$buttonText = 'Expired';
						}
						else{
							$buttonText = 'Participate';
						}
					}
				}else{
					if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
						$buttonText = 'Coming soon..';
					}
					elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
						$buttonText = 'Expired';
					}
					else{
						$buttonText = 'Accept';
					}
				}
				$studyList[$key]['buttonText'] = $buttonText;
				/////////////////////  Get Button Text End///////////////////////
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}
		
		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"studyArr"=>$studyList,"total"=>$totalpage);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}

    }
    

    /*
     * @Author:Rachit Jain 
     * @Action: This action used to get main studies
     */
    public function getstudytrialmain($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$IndStudObj = $this->getServiceLocator()->get("ServicesStudyTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$domainID = isset($data['domain_id']) ? $data['domain_id'] : '1'; //current domain id
			$condition = isset($data['condition']) ? $data['condition'] : '';
			$subcondition = isset($data['subcondition']) ? $data['subcondition'] : '';
			$location = isset($data['location']) ? $data['location'] : '';
			$start_date = isset($data['start_date']) ? $data['start_date'] : '';
			$end_date = isset($data['end_date']) ? $data['end_date'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID)){
				throw new Exception("Please enter user id.");
			}

			//Check Panel Member 
			$wherePanelArr = array("UserID"=>$UserID);
			$checkPanel = $IndUserTableObj->getUser($wherePanelArr);
			if($checkPanel[0]['PanelMember'] == 0){
				throw new Exception("You are not a panel member.");
			}

			$whereArrCheck = array("screening" => 0, "study_trials" => 1, "publish_status" => 1, "created_by" => $domainID, "end_date" => date("Y-m-d"));

			$studyListCheck = $IndStudObj->getStudytrials($whereArrCheck, array("study_id", "screening"), false, array(), 0);

			$stdId = '';
			if (count($studyListCheck) > 0) {
				$creation_date = round(microtime(true) * 1000);
				foreach ($studyListCheck as $svalue) {
					$stdId = $svalue['study_id'];
					$whereArrCheck1 = array("study_id" => $stdId, "user_id" => $UserID);
					$bucketInvi = $IndStudObj->getStudytrialsbuck($whereArrCheck1, array("study_id"), false, array(), 0);
					if (count($bucketInvi) <= 0) {
						$type = ($svalue['screening'] == 0) ? 1 : 0;
						$IndStudObj->insertbucket(array("user_id" => $UserID, "study_id" => $stdId, "bucket" => -2, "is_accepted" => 1, 'type' => $type, 'eligible_for_main' => $type, "creation_date" => $creation_date));
					}
				}
			}

			$searchcontent = array();
			$searchcontent = array('condation' => $condition, 'sub_condation' => $subcondition, 'location' => $location, 'start_date' => $start_date, 'end_date' => $end_date);

			$lookingfor = "start_date DESC";
			$whereArr = array("eligible_for_main" => 1, "bucket" => -2, "user_id" => $UserID, "study_trials" => 1, "publish_status" => 1, "created_by" => $domainID);
			
			$totalCount = count($IndStudObj->getStudylist($whereArr, array(), $lookingfor, $searchcontent,0,"",""));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$studyList = $IndStudObj->getStudylist($whereArr, array(), $lookingfor, $searchcontent, 0,$lowerLimit, $maxLimit);
			
			foreach($studyList as $key=>$val){
				/////////////////////  Get Status Text Start///////////////////////
				if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
					$labelText = 'Coming Soon..';
				}elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null) && $val['is_accepted']==1){
						$labelText = 'Finished';
					}
					else{
						$labelText = 'Expired';
					}
				}
				else{
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null)){
						$labelText = 'Completed';
					}
					else{
						$labelText = 'Pending';
					}
				}
				$studyList[$key]['labelText'] = $labelText;
				/////////////////////  Get Status Text End///////////////////////
				
				/////////////////////  Get Button Text Start///////////////////////
				if($val['is_accepted']==1){
					if((isset($val['part_status']) && $val['part_status']==0 && $val['part_status']!=null)){
						$buttonText = 'Finished';
					}
					else{
						if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
							$buttonText = 'Coming soon..';
						}
						elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
							$buttonText = 'Expired';
						}
						else{
							$buttonText = 'Participate';
						}
					}
				}else{
					if(strtotime(date("d-m-Y")) < strtotime($val['start_date']) ){
						$buttonText = 'Coming soon..';
					}
					elseif(strtotime(date("d-m-Y")) > strtotime($val['end_date'])){
						$buttonText = 'Expired';
					}
					else{
						$buttonText = 'Accept';
					}
				}
				$studyList[$key]['buttonText'] = $buttonText;
				/////////////////////  Get Button Text End///////////////////////
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"studyArr"=>$studyList,"total"=>$totalpage);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


    /*
     * @Author:Rachit Jain 
     * @Action: This action used for first time partishipation
     */
    public function participatestudytrial($service_key) {
        $sucess = TRUE;
        $CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $IndStudObj = $this->getServiceLocator()->get("ServicesStudyTable");

        try{
			$data = $this->params;
			$userId = isset($data['user_id']) ? $data['user_id'] : '';
			$study_id = isset($data['study_id']) ? $data['study_id'] : '';
			$study_id = $CusConPlug->encryptdata($study_id);
			$study_type = isset($data['study_type']) ? $data['study_type'] : '';
			$study_type = $CusConPlug->encryptdata($study_type);
			$type = 'mobile';
			$type = $CusConPlug->encryptdata($type);
			$study_trial = 'study_trial';
			$study_trial = $CusConPlug->encryptdata($study_trial);
			$subdomain = isset($data['subdomain']) ? $data['subdomain'] : 'individual';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($userId) || empty($study_id) || empty($study_type) || empty($subdomain)){
				throw new Exception("Required parameter missing.");
			}

			$serverScheme = $_SERVER['REQUEST_SCHEME'];
			$serverHost = $_SERVER['HTTP_HOST'];

			//Code to replace subdomain
			$serverArray = explode('.', $serverHost);
			if (strtolower($serverArray[0]) == 'www') {
				array_splice($serverArray,1,1,array($subdomain));
			} else {
				array_splice($serverArray,0,1,array($subdomain));
			}

			$HOST = implode('.',$serverArray);

			$Url = $serverScheme.'://'.$HOST."/individual/index/login?study_trial=".$study_trial."&study_id=".$study_id."&study_type=".$study_type."&type=".$type;
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"url"=>$Url);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}
    }


	/*
     * @Author:Rachit Jain 
     * @Action: This action used to get screening studies
     */
    public function getonsitestudytrialcategory($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$IndStudObj = $this->getServiceLocator()->get("ServicesStudyTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID)){
				throw new Exception("Please enter user id.");
			}
			
			$columns = array('category');
			$where = array();

			$totalCount = count($IndStudObj->getOnsiteStudyList($where, $columns, "1", array(), "", ""));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$StudyTrials = $IndStudObj->getOnsiteStudyList($where, $columns, "1", array(), $lowerLimit, $maxLimit);
			
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}
		
		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"studyArr"=>$StudyTrials,"total"=>$totalpage);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}

    }


	/*
     * @Author:Rachit Jain 
     * @Action: This action used to get screening studies
     */
    public function getonsitestudytrial($service_key) {
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$IndStudObj = $this->getServiceLocator()->get("ServicesStudyTable");

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$category = isset($data['category']) ? $data['category'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID) && empty($category)){
				throw new Exception("Required parameter missing.");
			}
			
			$columns = array('id', 'category', 'description', 'start_date', 'end_date', 'address');
			$where = array('category'=>$category);

			$totalCount = count($IndStudObj->getOnsiteStudyList($where, $columns));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$StudyTrials = $IndStudObj->getOnsiteStudyList($where, $columns, 0, array(), $lowerLimit, $maxLimit);
			
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}
		
		if($sucess === TRUE){
			return array("errstr"=>"Success.","success"=>1,"studyArr"=>$StudyTrials,"total"=>$totalpage);
		}
		else{
			return array("errstr"=>$error,"success"=>0);
		}

    }
}
