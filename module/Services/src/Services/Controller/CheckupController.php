<?php
/*
@ Used: This class is used to manage Checkups
@ Created By: Rachit Jain
@ Created On: 09/03/2015
@ Updated On: 09/03/2015
@ Zend Framework (http://framework.zend.com/)
*/
namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\EventManager\EventManagerInterface;
use Zend\View\Model\JsonModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Mail;
use \Exception;

class CheckupController extends AbstractRestfulController{

	public function __construct() {
		
    }

	public function getList(){
		
    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id){

	}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'getcheckups':
				$responseArr = $this->getcheckups($service_key);
				break;
			case 'addcheckup':
				$responseArr = $this->addcheckup($service_key);
				break;
			case 'viewcheckup':
				$responseArr = $this->viewcheckup($service_key);
				break;
			case 'editcheckup':
				$responseArr = $this->editcheckup($service_key);
				break;
			case 'deletecheckup':
				$responseArr = $this->deletecheckup($service_key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}

	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {

	}

	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}


	public function getcheckups($service_key){
		$sucess = TRUE;
		$CheckupUserTableObj = $this->getServiceLocator()->get("ServicesUserCheckupTable");
		$IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin

		try{
			$data = $this->params;
			$UserID = isset($data['userid']) ? $data['userid'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID)){
				throw new Exception("Please enter user id.");
			}

			$coloumns = $searchContent =  array();
			$where = array('user_id' =>$UserID, 'status' => 1);
			$lookingfor = "checkup_date DESC, checkup_time DESC";

			$totalCount = count($CheckupUserTableObj->getUserCheckups($where, $coloumns, $lookingfor, $searchContent, "", "", ""));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$checkupData = $CheckupUserTableObj->getUserCheckups($where, $coloumns, $lookingfor, $searchContent, "", $lowerLimit, $maxLimit);

			/*foreach($checkupData as $key=>$val){
				$checkupData[$key]['checkup_date'] = date('d M Y', strtotime($val['checkup_date']));
			}*/

			//Questions List
			$checkupListData = array();
            $whereArray = array("UserID" => $UserID);
            $coloumnarray = array("Dob", "Gender");
            $user_details =  $IndUserTableObj->getUser($whereArray,$coloumnarray);
            $UserAge = $CusConPlug->agecalculater($user_details[0]['Dob']);
            $UserGender = $user_details[0]['Gender'];

            $wherecheckupArray= array('age'=> $UserAge, 'checkup_gender' =>$UserGender, 'checkup_status' => 0 );
            $columns = array('checkup_question_id','checkup_question',);
            $checkupListData = $CheckupUserTableObj->checkupList($wherecheckupArray, $columns);
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){ 
            return array("errstr"=>"Success.","success"=>1,"checkupData"=>$checkupData, "checkupListData"=>$checkupListData,"total"=>$totalpage);
        }
        else{
            return array("errstr"=>$error,"success"=>0);
        }
	}


	public function addcheckup($service_key){
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $CheckUserTableObj = $this->getServiceLocator()->get("ServicesUserCheckupTable");

		try{
			$data = $this->params;
			$UserID = isset($data['userid']) ? $data['userid'] : '';
			$checkupName = isset($data['checkup_name']) ? $data['checkup_name'] : '';
			$checkupDate = date("Y-m-d", strtotime($data['checkup_date']));
			$checkupTime = date("H:i:s", strtotime($data['checkup_time']));

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID) || empty($checkupName)){
				throw new Exception("Required parameter missing.");
			}

			$whereArr = array('checkup_name'=>$checkupName,'checkup_date'=>$checkupDate,
			'checkup_time'=>$checkupTime,'user_id'=>$UserID);
			$checkupRes = $CheckUserTableObj->getUserCheckups($whereArr);

			if(count($checkupRes)){
				throw new Exception("Repeated checkup entry not allowed.");
			}


			$question_ids = $data['questions_ids']; // Comma separated value
			$question_ids = explode(',',$question_ids);
			$answer_ids = $data['ans']; // Comma separated value
			$answer_ids = explode(',',$answer_ids);
			if((count($question_ids) != count($answer_ids)) || (!empty($data['questions_ids']) && empty($data['questions_ids']))){
				throw new Exception("Please fill appropriate answers.");
			}

			$checkupNameArray = array(
				'user_id' => $UserID,
				'checkup_name' => $checkupName,
				'checkup_date' => $checkupDate,
				'checkup_time' => $checkupTime,
				'creation_date' => round(microtime(true) * 1000),
				'updation_date' => round(microtime(true) * 1000)
				);
			$users_checkup_id = $CheckUserTableObj->SaveUserCheckup($checkupNameArray);

			if(!empty($users_checkup_id)){
				//insert user answered question and answere in hm_user_checkup_answer
				$question_count= count($question_ids);
				for($i=0; $i < $question_count; $i++){
					$dataQuesArray = array(
						'user_id' => $UserID,
						'users_checkup_id' => $users_checkup_id,
						'checkup_question_id' => $question_ids[$i],
						'checkup_answer' => $answer_ids[$i],
						'creation_date' => round(microtime(true) * 1000)
						);
					$CheckUserTableObj->insertUsercheckupQA($dataQuesArray);
				}
			}else{
				throw new Exception("Some error occured.");
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
            return array("errstr"=>"Checkup added successfully.","success"=>1);
        }
        else{
            return array("errstr"=>$error,"success"=>0);
        }
	}
	
	
	public function viewcheckup($service_key){
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $CheckUserTableObj = $this->getServiceLocator()->get("ServicesUserCheckupTable");

		try{
			$data = $this->params;
			$UserID = isset($data['userid']) ? $data['userid'] : '';
			$checkupID = isset($data['checkupid']) ? $data['checkupid'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID) || empty($checkupID)){
				throw new Exception("Required parameter missing.");
			}
			
			$wherecheckupArr = array('id'=>$checkupID,'user_id'=>$UserID);
			$checkupRes = $CheckUserTableObj->getUserCheckups($wherecheckupArr);
			
			/*foreach($checkupRes as $key=>$val){
				$checkupRes[$key]['checkup_date'] = date('d M Y', strtotime($val['checkup_date']));
			}*/

			$whereArr = array('users_checkup_id'=>$checkupID,'user_id'=>$UserID);
			$checkupQARes = $CheckUserTableObj->getCheckupDetails($whereArr);
			
			$checkupRes[0]['checkupQAarr'] = $checkupQARes;
			//echo '<pre>';print_r($checkupRes);exit;

		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
            return array("errstr"=>"Success.","success"=>1,"checkupArr"=>$checkupRes);
        }
        else{
            return array("errstr"=>$error,"success"=>0);
        }
	}


	public function editcheckup($service_key){
		$sucess = TRUE;
		$CusConPlug = $this->CustomControllerPlugin1();  // custom plugin
        $CheckUserTableObj = $this->getServiceLocator()->get("ServicesUserCheckupTable");

		try{
			$data = $this->params;
			$UserID = isset($data['userid']) ? $data['userid'] : '';
			$checkupID = isset($data['checkupid']) ? $data['checkupid'] : '';
			$checkupName = isset($data['checkup_name']) ? $data['checkup_name'] : '';
			$checkupDate = date("Y-m-d", strtotime($data['checkup_date']));
			$checkupTime = date("H:i:s", strtotime($data['checkup_time']));

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			if(empty($UserID) || empty($checkupID) || empty($checkupName)){
				throw new Exception("Required parameter missing.");
			}

			$checkwhereArr = array('checkup_name'=>$checkupName,'checkup_date'=>$checkupDate,
			'checkup_time'=>$checkupTime,'user_id'=>$UserID);
			$checkupRes = $CheckUserTableObj->getUserCheckups($checkwhereArr);

			if(count($checkupRes) && $checkupRes[0]['id'] != $checkupID){
				throw new Exception("Repeated checkup entry not allowed.");
			}

			$checkupNameArray = array(
				'id'=> $checkupID,
				'user_id' => $UserID,
				'checkup_name' => $checkupName,
				'checkup_date' => $checkupDate,
				'checkup_time' => $checkupTime,
				'creation_date' => round(microtime(true) * 1000),
				'updation_date' => round(microtime(true) * 1000)
				);
			$whereArr = array('id'=>$checkupID,'user_id'=>$UserID);
			$users_checkup_id = $CheckUserTableObj->updateUserCheckup($whereArr, $checkupNameArray);

			//insert user answered question and answered in hm_user_checkup_answer
			$question_ids = $data['questions_ids']; // Comma separated value
			$question_ids = explode(',',$question_ids);
			$answer_ids = $data['ans']; // Comma separated value
			$answer_ids = explode(',',$answer_ids);
			$question_count= count($question_ids);
			for($i=0; $i < $question_count; $i++){
				$dataQuesArray = array(
					'user_id' => $UserID,
					'users_checkup_id' => $checkupID,
					'checkup_question_id' => $question_ids[$i],
					'checkup_answer' => $answer_ids[$i],
					'creation_date' => round(microtime(true) * 1000)
					);
				$whereQA = array('users_checkup_id'=>$checkupID,'user_id'=>$UserID,'checkup_question_id'=>$question_ids[$i]);
				$CheckUserTableObj->updateUserCheckupQA($whereQA, $dataQuesArray);
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
            return array("errstr"=>"Checkup updated successfully.","success"=>1);
        }
        else{
            return array("errstr"=>$error,"success"=>0);
        }
	}


    public function deletecheckup($service_key) {
        $sucess = TRUE;
        $CheckupUserTableObj = $this->getServiceLocator()->get("ServicesUserCheckupTable");

        try{
			$data = $this->params;
			$UserID = isset($data['userid']) ? $data['userid'] : '';
			$checkupId = isset($data['checkupid']) ? $data['checkupid'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($UserID) || empty($checkupId)){
				throw new Exception("Required parameter missing.");
			}

			$whereArr = array("user_id" => $UserID, 'id' => $checkupId);
			$CheckupUserTableObj->updateUserCheckup($whereArr, array('status' => 0));
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
            return array("errstr"=>"Checkup deleted successfully.","success"=>1);
        }
        else{
            return array("errstr"=>$error,"success"=>0);
        }
    }
}
?>
