<?php
/*
  @ Used: This class is used to manage Imunization
  @ Created By: Rachit Jain
  @ Created On: 22/01/2015
  @ Updated On: 22/01/2015
  @ Zend Framework (http://framework.zend.com/)
 */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Zend\EventManager\EventManagerInterface;
use Zend\View\Model\JsonModel;
use Zend\Json\Json as jsonDecoder;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Mail;
use \Exception;

class ProfileController extends AbstractRestfulController {

    public function __construct() {
		
    }


	public function getList(){
		
    }

	/**
	 * Return single resource
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id){

	}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {
		//fetch array from json data
		$request = $this->request->getContent();
		$requestArr = jsonDecoder::decode($request, jsonDecoder::TYPE_ARRAY);

		$method = $requestArr['method'];
		$service_key = isset($requestArr['service_key']) ? $requestArr['service_key'] : '';
		if(isset($requestArr['params'])){
			$params = $requestArr['params'];
			$this->params = $params;
		}

		switch ($method) {
			case 'updatebasicinfo':
				$responseArr = $this->updatebasicinfo($service_key);
				break;
			case 'updatecontactinfo':
				$responseArr = $this->updatecontactinfo($service_key);
				break;
			case 'privacysettings':
				$responseArr = $this->privacysettings($service_key);
				break;
			case 'caregiveraccepted':
				$responseArr = $this->caregiveraccepted($service_key);
				break;
			case 'caregiverrequest':
				$responseArr = $this->caregiverrequest($service_key);
				break;
			case 'respondtocaregiver':
				$responseArr = $this->respondtocaregiver($service_key);
				break;
			case 'researcherlist':
				$responseArr = $this->researcherlist($service_key);
				break;
			default:
				break;
		}

	    echo json_encode($responseArr);
		exit;
	}

	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {

	}

	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {

	}


    /*
     * @this is for individual basic setting 
     * @ Auth: Rachit Jain
     * Date: 06-04-2015
     */
    public function updatebasicinfo($service_key) {
        $sucess = TRUE;
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$ContactInfoTableObj = $this->getServiceLocator()->get("ServicesContactInfoTable");
		$ProfileInfoTableObj = $this->getServiceLocator()->get("ServicesProfileInfoTable");
        $CountryTableObj = $this->getServiceLocator()->get("ServicesCountryTable");
        $IndStateTableObj = $this->getServiceLocator()->get("ServicesStateTable");
        $CountyTableObj = $this->getServiceLocator()->get("ServicesCountyTable");
        $CusConPlug = $this->CustomControllerPlugin1();

		try{
			$data = $this->params;

			$userId = isset($data['userid']) ? $data['userid'] : '';
			$firstname = isset($data['firstname']) ? $data['firstname'] : '';
			$middlename = isset($data['middlename']) ? $data['middlename'] : '';
			$lastname = isset($data['lastname']) ? $data['lastname'] : '';
			$nickname = isset($data['nickname']) ? $data['nickname'] : '';
			$dob = isset($data['dob']) ? date("Y-m-d", strtotime($data['dob'])) : '';
			$gender = isset($data['gender']) ? $data['gender'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

            if(empty($userId)){
				throw new Exception("Please enter user id.");
			}
			if(empty($firstname)){
				throw new Exception("Please enter firstname.");
			}
			if(empty($lastname)){
				throw new Exception("Please enter lastname.");
			}
			if(empty($dob)){
				throw new Exception("Please enter dob.");
			}

			//Validate Dob
			/*$whereDobArr = array("UserID" => $userId);
			$res = $IndUserTableObj->getUser($whereDobArr);
			$curr = date("Y-m-d");
			$diff = abs(strtotime($curr) - strtotime($dob));
			$years = floor($diff / (365 * 60 * 60 * 24));
			if ($years != $res[0]['Age']) {
				throw new Exception("Your age does not match with the entered DOB");
			}
			*/

			if(empty($gender)){
				throw new Exception("Please enter gender.");
			}

			$dataArr = array();
			$dataArr['FirstName']=$firstname;
			$dataArr['MiddleName']=$middlename;
			$dataArr['LastName']=$lastname;
			$dataArr['NickName']=$nickname;
			$dataArr['Dob']=$dob;
            $dataArr['Gender']=$gender;

			$updateBasic = $IndUserTableObj->updateUser(array('UserID'=>$userId),$dataArr);
			if(!$updateBasic){
				 throw new Exception("Some error occured please try again later.");
			}

			///////////////////*******************************///////////////////
			//GET USER FULL INFO
			$where = array("UserID"=>$userId);
			$userData = $IndUserTableObj->getUser($where);
			$userData[0]['Dob'] = date('d M Y', strtotime($userData[0]['Dob']));

			//Get Full image path
			$image = "";
			if($userData[0]['Image'] != ""){
				$server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
				//Get Original Image
				//$image = $server_url."/upload_image/".$userData[0]['UserID']."/".$userData[0]['Image'];

				//Get Image From CacheImg Folder
				$imagepath = $server_url."/upload_image/".$userData[0]['UserID']."/";
				$imagename = $userData[0]['Image'];
				$newWidth = 150;
				$newHeight = 150;
				$image = $CusConPlug->resizeimage($imagepath, $imagename, $newWidth, $newHeight);
				$userData[0]['Image'] = $image;
			}

			//Get Contact info
			$whereArr = array("user_id"=>$userId);
			$userContDetails = $ContactInfoTableObj->getUserContactInfo($whereArr);
			$userCountry = $userState = $userCounty = $userEthnicity = $userOrigin = '';
			//user current country name 
			if(!empty($userContDetails[0]['country_id'])){
				$whereCountrySingleArr = array("country_id" => $userContDetails[0]['country_id']);
				$country_current = $CountryTableObj->getCountry($whereCountrySingleArr);
				$userCountry = isset($country_current[0]['country_name'])? $country_current[0]['country_name']: "" ;
			}
			//user current state name 
			if(!empty($userContDetails[0]['state_id'])){
				$whereStateSingleArr = array("state_id" => $userContDetails[0]['state_id']);
				$states_current = $IndStateTableObj->getStates($whereStateSingleArr);
				$userState = isset($states_current[0]['state_name'])? $states_current[0]['state_name']: "" ;
			}
			//user current county name 
			if(!empty($userContDetails[0]['county_id'])){
				$whereCountySingleArr = array("county_id" => $userContDetails[0]['county_id']);
				$county_current = $CountyTableObj->getCounty($whereCountySingleArr);
				$userCounty = isset($county_current[0]['county_name'])? $county_current[0]['county_name']: "" ;
			}
			//select current Ethnicity of user
			if(!empty($userContDetails[0]['ethnicity'])){
				$whereEthArray = array("ethnicity_id" => $userContDetails[0]['ethnicity']);
				$ethnicity_current  = $IndUserTableObj->getEthnicity($whereEthArray);
				$userEthnicity = isset($ethnicity_current[0]['ethnicity_name'])? $ethnicity_current[0]['ethnicity_name']: "" ;
			}
			//select current origin of user
			if(!empty($userContDetails[0]['origin_id'])){
				$whereOriArray = array("country_id" => $userContDetails[0]['origin_id']);
				$origin_current  = $CountryTableObj->getCountry($whereOriArray);
				$userOrigin = isset($origin_current[0]['country_name'])? $origin_current[0]['country_name']: "" ;
			}

			$userData[0]['country_id'] = isset($userContDetails[0]['country_id'])? $userContDetails[0]['country_id']: "" ;
			$userData[0]['country_name'] = $userCountry;
			$userData[0]['state_id'] = isset($userContDetails[0]['state_id'])? $userContDetails[0]['state_id']: "" ;
			$userData[0]['state_name'] = $userState;
			$userData[0]['county_id'] = isset($userContDetails[0]['county_id'])? $userContDetails[0]['county_id']: "" ;
			$userData[0]['county_name'] = $userCounty;
			$userData[0]['ethnicity_id'] = isset($userContDetails[0]['ethnicity'])? $userContDetails[0]['ethnicity']: "" ;
			$userData[0]['ethnicity'] = $userEthnicity;
			$userData[0]['origin_id'] = isset($userContDetails[0]['origin_id'])? $userContDetails[0]['origin_id']: "" ;
			$userData[0]['origin_name'] = $userOrigin;
			$userData[0]['city'] = isset($userContDetails[0]['city'])? $userContDetails[0]['city']: "" ;
			$userData[0]['street_address1'] = isset($userContDetails[0]['street_address1'])? $userContDetails[0]['street_address1']: "" ;
			$userData[0]['street_address2'] = isset($userContDetails[0]['street_address2'])? $userContDetails[0]['street_address2']: "" ;
			$userData[0]['postal_code'] = isset($userContDetails[0]['postal_code'])? $userContDetails[0]['postal_code']: "" ;
			$userData[0]['phone_no'] = isset($userContDetails[0]['phone_no'])? $userContDetails[0]['phone_no']: "" ;

			//Get Profile Info
			$userProfileDetails = $ProfileInfoTableObj->getUserProfileInfo($whereArr);
			$userLanguage = $userEducation = $userBlood = '';
			//select current language of user 
			if(!empty($userProfileDetails[0]['language'])){
				$whereLangArray = array("laguage_id" => $userProfileDetails[0]['language']);
				$language_current = $IndUserTableObj->getLanguage($whereLangArray);
				$userLanguage = isset($language_current[0]['language'])? $language_current[0]['language']: "" ;
			}
			//select current education of user 
			if(!empty($userProfileDetails[0]['education_degree'])){
				$whereEducationArray = array("education_id" => $userProfileDetails[0]['education_degree']);
				$education_current  = $IndUserTableObj->getEducation($whereEducationArray);
				$userEducation = isset($education_current[0]['highest_degree_name'])? $education_current[0]['highest_degree_name']: "" ;
			}
			//select blood type of user 
			if(!empty($userProfileDetails[0]['blood_group'])){
				$whereBloodArray = array("blood_id" => $userProfileDetails[0]['blood_group']);
				$blood_current  = $IndUserTableObj->getBloodType($whereBloodArray);
				$userBlood = isset($blood_current[0]['blood_type'])? $blood_current[0]['blood_type']: "" ;
			}

			$userData[0]['language'] = $userLanguage;
			$userData[0]['education_degree'] = $userEducation;
			$userData[0]['income'] = isset($userProfileDetails[0]['income'])? $userProfileDetails[0]['income']: "" ;
			$userData[0]['maritial_status'] = isset($userProfileDetails[0]['maritial_status'])? $userProfileDetails[0]['maritial_status']: "" ;
			$userData[0]['blood_group'] = $userBlood;
			///////////////////*******************************///////////////////
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){ 
            return array("errstr"=>"Basic information updated successfully.","success"=>1,"data"=>$userData[0]);
        }
        else{
            return array("errstr"=>$error,"success"=>0);
        }
    }


    /*
     * @this is for individual contact setting 
     * @ Auth: Rachit Jain
     * Date: 06-04-2015
     */
    public function updatecontactinfo($service_key) {
        $sucess = TRUE;
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$ContactInfoTableObj = $this->getServiceLocator()->get("ServicesContactInfoTable");
		$ProfileInfoTableObj = $this->getServiceLocator()->get("ServicesProfileInfoTable");
        $CountryTableObj = $this->getServiceLocator()->get("ServicesCountryTable");
        $IndStateTableObj = $this->getServiceLocator()->get("ServicesStateTable");
        $CountyTableObj = $this->getServiceLocator()->get("ServicesCountyTable");
        $CusConPlug = $this->CustomControllerPlugin1();

		try{
			$data = $this->params;
			$userId = isset($data['userid']) ? $data['userid'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}
			
			//Initial contact details
			$initialwhereArr = array("user_id"=>$userId);
			$initialContactDetails = $ContactInfoTableObj->getUserContactInfo($initialwhereArr);
			$oldValueCountryId = isset($initialContactDetails[0]['country_id']) ? $initialContactDetails[0]['country_id'] : "";
			$oldValueStateId = isset($initialContactDetails[0]['state_id']) ? $initialContactDetails[0]['state_id'] : "";
			$oldValueCountyId = isset($initialContactDetails[0]['county_id']) ? $initialContactDetails[0]['county_id'] : "";
			$oldValueEthnicity = isset($initialContactDetails[0]['ethnicity']) ? $initialContactDetails[0]['ethnicity'] : "";
			$oldValueOriginId = isset($initialContactDetails[0]['origin_id']) ? $initialContactDetails[0]['origin_id'] : "";
			
			$address1 = isset($data['address1']) ? $data['address1'] : '';
			$address2 = isset($data['address2']) ? $data['address2'] : '';
			$country = !(isset($data['country_id']) && empty($data['country_id'])) ? $data['country_id'] : $oldValueCountryId;
			$state = !(isset($data['state_id']) && empty($data['state_id'])) ? $data['state_id'] : $oldValueStateId;
			$county = !(isset($data['county_id']) && empty($data['county_id'])) ? $data['county_id'] : $oldValueCountyId;
			$city = isset($data['city']) ? $data['city'] : '';
			$phone_number = isset($data['phone_number']) ? $data['phone_number'] : '';
			$postal_code = isset($data['postal_code']) ? $data['postal_code'] : '';
			$ethnicity = !(isset($data['ethnicity']) && empty($data['ethnicity'])) ? $data['ethnicity'] : $oldValueEthnicity;
			$origin_id = !(isset($data['origin_id']) && empty($data['origin_id'])) ? $data['origin_id'] : $oldValueOriginId;

			if(empty($userId)){
				throw new Exception("Please enter user id.");
			}
			if(empty($address1)){
				throw new Exception("Please enter address1.");
			}
			if(empty($country)){
				throw new Exception("Please enter country.");
			}
			if(empty($state)){
				throw new Exception("Please enter state.");
			}
			if(empty($county)){
				throw new Exception("Please enter county.");
			}
			if(empty($ethnicity)){
				throw new Exception("Please enter ethnicity.");
			}
			if(empty($city)){
				throw new Exception("Please enter city.");
			}
			if(empty($phone_number)){
				throw new Exception("Please enter phone number.");
			}
			if(empty($postal_code)){
				throw new Exception("Please enter postal code.");
			}

			$dataArr=array();
			$dataArr['user_id']=$userId;
			$dataArr['street_address1']=$address1;
			$dataArr['street_address2']=$address2;
			$dataArr['country_id']=$country;
			$dataArr['state_id']=$state;
			$dataArr['county_id']=$county;
			$dataArr['ethnicity']=$ethnicity;
			$dataArr['origin_id']=$origin_id;
			$dataArr['city']=$city;
			$dataArr['phone_no']=$phone_number;
			$dataArr['postal_code']=$postal_code;
			$contactUpdate = $ContactInfoTableObj->updateContact(array('user_id'=>$userId),$dataArr);
			if(!$contactUpdate){
				throw new Exception("Some error occured please try again later.");
			}

			///////////////////*******************************///////////////////
			//GET USER FULL INFO
			$where = array("UserID"=>$userId);
			$userData = $IndUserTableObj->getUser($where);
			$userData[0]['Dob'] = date('d M Y', strtotime($userData[0]['Dob']));

			//Get Full image path
			$image = "";
			if($userData[0]['Image'] != ""){
				$server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
				//Get Original Image
				//$image = $server_url."/upload_image/".$userData[0]['UserID']."/".$userData[0]['Image'];

				//Get Image From CacheImg Folder
				$imagepath = $server_url."/upload_image/".$userData[0]['UserID']."/";
				$imagename = $userData[0]['Image'];
				$newWidth = 150;
				$newHeight = 150;
				$image = $CusConPlug->resizeimage($imagepath, $imagename, $newWidth, $newHeight);
				$userData[0]['Image'] = $image;
			}

			//Get Contact info
			$whereArr = array("user_id"=>$userId);
			$userContDetails = $ContactInfoTableObj->getUserContactInfo($whereArr);
			$userCountry = $userState = $userCounty = $userEthnicity = $userOrigin = '';
			//user current country name 
			if(!empty($userContDetails[0]['country_id'])){
				$whereCountrySingleArr = array("country_id" => $userContDetails[0]['country_id']);
				$country_current = $CountryTableObj->getCountry($whereCountrySingleArr);
				$userCountry = isset($country_current[0]['country_name'])? $country_current[0]['country_name']: "" ;
			}
			//user current state name 
			if(!empty($userContDetails[0]['state_id'])){
				$whereStateSingleArr = array("state_id" => $userContDetails[0]['state_id']);
				$states_current = $IndStateTableObj->getStates($whereStateSingleArr);
				$userState = isset($states_current[0]['state_name'])? $states_current[0]['state_name']: "" ;
			}
			//user current county name 
			if(!empty($userContDetails[0]['county_id'])){
				$whereCountySingleArr = array("county_id" => $userContDetails[0]['county_id']);
				$county_current = $CountyTableObj->getCounty($whereCountySingleArr);
				$userCounty = isset($county_current[0]['county_name'])? $county_current[0]['county_name']: "" ;
			}
			//select current Ethnicity of user
			if(!empty($userContDetails[0]['ethnicity'])){
				$whereEthArray = array("ethnicity_id" => $userContDetails[0]['ethnicity']);
				$ethnicity_current  = $IndUserTableObj->getEthnicity($whereEthArray);
				$userEthnicity = isset($ethnicity_current[0]['ethnicity_name'])? $ethnicity_current[0]['ethnicity_name']: "" ;
			}
			//select current origin of user
			if(!empty($userContDetails[0]['origin_id'])){
				$whereOriArray = array("country_id" => $userContDetails[0]['origin_id']);
				$origin_current  = $CountryTableObj->getCountry($whereOriArray);
				$userOrigin = isset($origin_current[0]['country_name'])? $origin_current[0]['country_name']: "" ;
			}

			$userData[0]['country_id'] = isset($userContDetails[0]['country_id'])? $userContDetails[0]['country_id']: "" ;
			$userData[0]['country_name'] = $userCountry;
			$userData[0]['state_id'] = isset($userContDetails[0]['state_id'])? $userContDetails[0]['state_id']: "" ;
			$userData[0]['state_name'] = $userState;
			$userData[0]['county_id'] = isset($userContDetails[0]['county_id'])? $userContDetails[0]['county_id']: "" ;
			$userData[0]['county_name'] = $userCounty;
			$userData[0]['ethnicity_id'] = isset($userContDetails[0]['ethnicity'])? $userContDetails[0]['ethnicity']: "" ;
			$userData[0]['ethnicity'] = $userEthnicity;
			$userData[0]['origin_id'] = isset($userContDetails[0]['origin_id'])? $userContDetails[0]['origin_id']: "" ;
			$userData[0]['origin_name'] = $userOrigin;
			$userData[0]['city'] = isset($userContDetails[0]['city'])? $userContDetails[0]['city']: "" ;
			$userData[0]['street_address1'] = isset($userContDetails[0]['street_address1'])? $userContDetails[0]['street_address1']: "" ;
			$userData[0]['street_address2'] = isset($userContDetails[0]['street_address2'])? $userContDetails[0]['street_address2']: "" ;
			$userData[0]['postal_code'] = isset($userContDetails[0]['postal_code'])? $userContDetails[0]['postal_code']: "" ;
			$userData[0]['phone_no'] = isset($userContDetails[0]['phone_no'])? $userContDetails[0]['phone_no']: "" ;

			//Get Profile Info
			$userProfileDetails = $ProfileInfoTableObj->getUserProfileInfo($whereArr);
			$userLanguage = $userEducation = $userBlood = '';
			//select current language of user 
			if(!empty($userProfileDetails[0]['language'])){
				$whereLangArray = array("laguage_id" => $userProfileDetails[0]['language']);
				$language_current = $IndUserTableObj->getLanguage($whereLangArray);
				$userLanguage = isset($language_current[0]['language'])? $language_current[0]['language']: "" ;
			}
			//select current education of user 
			if(!empty($userProfileDetails[0]['education_degree'])){
				$whereEducationArray = array("education_id" => $userProfileDetails[0]['education_degree']);
				$education_current  = $IndUserTableObj->getEducation($whereEducationArray);
				$userEducation = isset($education_current[0]['highest_degree_name'])? $education_current[0]['highest_degree_name']: "" ;
			}
			//select blood type of user 
			if(!empty($userProfileDetails[0]['blood_group'])){
				$whereBloodArray = array("blood_id" => $userProfileDetails[0]['blood_group']);
				$blood_current  = $IndUserTableObj->getBloodType($whereBloodArray);
				$userBlood = isset($blood_current[0]['blood_type'])? $blood_current[0]['blood_type']: "" ;
			}

			$userData[0]['language'] = $userLanguage;
			$userData[0]['education_degree'] = $userEducation;
			$userData[0]['income'] = isset($userProfileDetails[0]['income'])? $userProfileDetails[0]['income']: "" ;
			$userData[0]['maritial_status'] = isset($userProfileDetails[0]['maritial_status'])? $userProfileDetails[0]['maritial_status']: "" ;
			$userData[0]['blood_group'] = $userBlood;
			///////////////////*******************************///////////////////
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){ 
            return array("errstr"=>"Contact information updated successfully.","success"=>1,"data"=>$userData[0]);
        }
        else{
            return array("errstr"=>$error,"success"=>0);
        }
    }


    /*
     * @this is for individual profile Privacy  setting 
     * @Auth: Rachit Jain
     * Date: 06-03-2015
     */
	public function privacysettings($service_key) {
        $sucess = TRUE;
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
        $IndPrivSetTableObj = $this->getServiceLocator()->get("ServicesPrivacySettingsTable");

        try{
			$data = $this->params;
			$userId = isset($data['user_id']) ? $data['user_id'] : '';
			$all_settings = isset($data['all_settings']) ? $data['all_settings'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$userId, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			if(empty($userId)){
				throw new Exception("Please enter user id.");
			}

			$whereArr = array("UserID" => $userId);
			$privacydata = $IndPrivSetTableObj->getPrivacySettings($whereArr);

			$how_i_feel = (isset($data['how_i_feel']) && strlen($data['how_i_feel']) != 0) ? $data['how_i_feel'] : $privacydata['how_i_feel'];
			$survey_request = (isset($data['survey_request']) && strlen($data['survey_request']) != 0) ? $data['survey_request'] : $privacydata['survey_request'];
			$demographic_info = (isset($data['demographic_info']) && strlen($data['demographic_info']) != 0) ? $data['demographic_info'] : $privacydata['demographic_info'];
			$receive_study = (isset($data['receive_study']) && strlen($data['receive_study']) != 0) ? $data['receive_study'] : $privacydata['receive_study'];
			$receive_self_assesment = (isset($data['receive_self_assesment']) && strlen($data['receive_self_assesment']) != 0) ? $data['receive_self_assesment'] : $privacydata['receive_self_assesment'];
			$receive_trail = (isset($data['receive_trail']) && strlen($data['receive_trail']) != 0) ? $data['receive_trail'] : $privacydata['receive_trail'];
			$news_latter = (isset($data['news_latter']) && strlen($data['news_latter']) != 0) ? $data['news_latter'] : $privacydata['news_latter'];
			$want_takecare = (isset($data['want_takecare']) && strlen($data['want_takecare']) != 0) ? $data['want_takecare'] : $privacydata['want_takecare'];
			$survay_per_month = (isset($data['survay_per_month']) && strlen($data['survay_per_month']) != 0) ? $data['survay_per_month'] : $privacydata['survay_per_month'];
			$get_follow_request = (isset($data['get_follow_request']) && strlen($data['get_follow_request']) != 0) ? $data['get_follow_request'] : $privacydata['get_follow_request'];
			//$accepted_follow_request = (isset($data['accepted_follow_request']) && strlen($data['accepted_follow_request']) != 0) ? $data['accepted_follow_request'] : $privacydata['accepted_follow_request'];
			$get_caregiver_request = (isset($data['get_caregiver_request']) && strlen($data['get_caregiver_request']) != 0) ? $data['get_caregiver_request'] : $privacydata['get_caregiver_request'];
			//$accepted_caregiver_request = (isset($data['accepted_caregiver_request']) && strlen($data['accepted_caregiver_request']) != 0) ? $data['accepted_caregiver_request'] : $privacydata['accepted_caregiver_request'];
			$get_geo_request = (isset($data['get_geo_request']) && strlen($data['get_geo_request']) != 0) ? $data['get_geo_request'] : $privacydata['get_geo_request'];
			//$accepted_geo_request = (isset($data['accepted_geo_request']) && strlen($data['accepted_geo_request']) != 0) ? $data['accepted_geo_request'] : $privacydata['accepted_geo_request'];


			$dataArr = array();
			$updation_date = round(microtime(true) * 1000);
			$dataArr = array(
				"how_i_feel" => $how_i_feel, 
				"survey_request" => $survey_request, 
				"demographic_info" => $demographic_info, 
				"receive_study" => $receive_study, 
				"receive_self_assesment" => $receive_self_assesment, 
				"receive_trail" => $receive_trail, 
				"news_latter" => $news_latter, 
				"want_takecare" => $want_takecare, 
				"survay_per_month" => $survay_per_month, 
				"get_follow_request" => $get_follow_request, 
				//"accepted_follow_request" => $accepted_follow_request, 
				"get_caregiver_request" => $get_caregiver_request, 
				//"accepted_caregiver_request" => $accepted_caregiver_request, 
				"get_geo_request" => $get_geo_request, 
				//"accepted_geo_request" => $accepted_geo_request, 
				"updation_date" => $updation_date);
			$updatedata = $IndPrivSetTableObj->updatePrivacySettings($whereArr, $dataArr);

			$columnArr = array();
			if($all_settings != 1){
				if((isset($data['how_i_feel']) && strlen($data['how_i_feel']) != 0)){
					array_push($columnArr, 'how_i_feel');
				}
				if((isset($data['survey_request']) && strlen($data['survey_request']) != 0)){
					array_push($columnArr, 'survey_request');
				}
				if((isset($data['demographic_info']) && strlen($data['demographic_info']) != 0)){
					array_push($columnArr, 'demographic_info');
				}
				if((isset($data['receive_study']) && strlen($data['receive_study']) != 0)){
					array_push($columnArr, 'receive_study');
				}
				if((isset($data['receive_self_assesment']) && strlen($data['receive_self_assesment']) != 0)){
					array_push($columnArr, 'receive_self_assesment');
				}
				if((isset($data['receive_trail']) && strlen($data['receive_trail']) != 0)){
					array_push($columnArr, 'receive_trail');
				}
				if((isset($data['news_latter']) && strlen($data['news_latter']) != 0)){
					array_push($columnArr, 'news_latter');
				}
				if((isset($data['want_takecare']) && strlen($data['want_takecare']) != 0)){
					array_push($columnArr, 'want_takecare');
				}
				if((isset($data['survay_per_month']) && strlen($data['survay_per_month']) != 0)){
					array_push($columnArr, 'survay_per_month');
				}
				if((isset($data['get_follow_request']) && strlen($data['get_follow_request']) != 0)){
					array_push($columnArr, 'get_follow_request');
				}
				if((isset($data['get_caregiver_request']) && strlen($data['get_caregiver_request']) != 0)){
					array_push($columnArr, 'get_caregiver_request');
				}
				if((isset($data['get_geo_request']) && strlen($data['get_geo_request']) != 0)){
					array_push($columnArr, 'get_geo_request');
				}
			}
			//Get Updated privacy settings
			$updatedprivacydata = $IndPrivSetTableObj->getPrivacySettings($whereArr,$columnArr);
			
			if(!$updatedata) {
				throw new Exception("Some error occured.");
			}
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
            return array("errstr"=>"Success.","success"=>1,"privacydata"=>$updatedprivacydata);
        }
        else{
            return array("errstr"=>$error,"success"=>0);
        }
    }


    /*
     * @this is for individual profile caregive accepted list 
     * @Auth: Rachit Jain
     * Date: 02-03-2015
     */
	public function caregiveraccepted($service_key) {
        $sucess = TRUE;
        $CareGiverTableObj = $this->getServiceLocator()->get("ServicesCaregiverTable");
        $BeaconTableObj = $this->getServiceLocator()->get("ServicesAssociatedBeaconTable");
		$plugin = $this->CustomControllerPlugin1();  // custom plugin

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$BeaconID = isset($data['beacon_id']) ? $data['beacon_id'] : '';
			$page = isset($data['page']) ? $data['page'] : '';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			$CareGiveRes = array();
			$whereArr = array('care.UserID'=>$UserID,'care.status'=>1);

			if($page != ''){
				$totalCount = count($CareGiverTableObj->getCareGiver($whereArr, array(),'','',''));
				$totalpage = $page = '';
				$page = (int)($totalCount/$maxLimit);
				if(($page*$maxLimit) < $totalCount){
					$totalpage = $page+1;
				}
				else{
					$totalpage = $page;
				}

				$CareGiveRes = $CareGiverTableObj->getCareGiver($whereArr, array(), '', $lowerLimit, $maxLimit);
			}
			else{
				$CareGiveRes = $CareGiverTableObj->getCareGiver($whereArr);
				$totalpage = 1;
			}
			//print_r($CareGiveRes);exit;

			$i=0;
			foreach($CareGiveRes as $key1=>$val1){
				//Get full image url
				$image = "";
				if($val1['Image'] != ""){
					$server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
					//Get Original Image
					//$image = $server_url."/upload_image/".$val1['UserID']."/".$val1['Image'];

					//Get Image From CacheImg Folder
					$imagepath = $server_url."/upload_image/".$val1['UserID']."/";
					$imagename = $val1['Image'];
					$newWidth = 150;
					$newHeight = 150;
					$image = $plugin->resizeimage($imagepath, $imagename, $newWidth, $newHeight);
				}
				$CareGiveRes[$i]['Image'] = $image;

				$where1 = array('UserID'=>$UserID,'beacon_id'=>$BeaconID,'caregiver_id'=>$val1['caregiver_id']);
				$caregiverBeacon = $BeaconTableObj->getCaregiverBecons($where1);
				//print_r($caregiverBeacon);exit;
				
				if(count($caregiverBeacon)){
					$CareGiveRes[$i]['beacon_status'] = count($caregiverBeacon);
				}
				else{
					$CareGiveRes[$i]['beacon_status'] = 0;
				}
				$i++;
			}
			//print_r($CareGiveRes);exit;
		}
		catch(Exception $e){
			$sucess = TRUE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
            return array("errstr"=>"Success.","success"=>1,"user_caregivers"=>$CareGiveRes,"total"=>$totalpage);
        }
        else{
            return array("errstr"=>$error,"success"=>0);
        }
    }


    /*
     * @this is for individual profile caregiver request list 
     * @Auth: Rachit Jain
     * Date: 02-03-2015
     */
    public function caregiverrequest($service_key) {
        $sucess = TRUE;
        $CareGiverTableObj = $this->getServiceLocator()->get("ServicesCaregiverTable");
		$plugin = $this->CustomControllerPlugin1();  // custom plugin

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			$CareGiveRes = array();
			$whereArr = array('care.UserID'=>$UserID,'care.status'=>0);

			$totalCount = count($CareGiverTableObj->getCareGiver($whereArr, array(),'','',''));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$CareGiveRes = $CareGiverTableObj->getCareGiver($whereArr, array(), '', $lowerLimit, $maxLimit);

			$i=0;
			foreach($CareGiveRes as $key1=>$val1){
				//Get full image url
				$image = "";
				if($val1['Image'] != ""){
					$server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
					//Get Original Image
					//$image = $server_url."/upload_image/".$val1['UserID']."/".$val1['Image'];

					//Get Image From CacheImg Folder
					$imagepath = $server_url."/upload_image/".$val1['UserID']."/";
					$imagename = $val1['Image'];
					$newWidth = 150;
					$newHeight = 150;
					$image = $plugin->resizeimage($imagepath, $imagename, $newWidth, $newHeight);
				}
				$CareGiveRes[$i]['Image'] = $image;
				$i++;
			}
		}
		catch(Exception $e){
			$sucess = TRUE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
            return array("errstr"=>"Success.","success"=>1,"user_caregivers"=>$CareGiveRes,"total"=>$totalpage);
        }
        else{
            return array("errstr"=>$error,"success"=>0);
        }
    }


    /*
      @ : THis method is used to accept reject caregiver request
      @ : Created 15-04-2015
     */
    public function respondtocaregiver($service_key) {
        $sucess = TRUE;
        $plugin = $this->CustomControllerPlugin1();
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
        $CareGiverTableObj = $this->getServiceLocator()->get("ServicesCaregiverTable");
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");

        try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$reqID = isset($data['req_id']) ? $data['req_id'] : '';
			$response = isset($data['response']) ? $data['response'] : '';

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			$whereArr = array("Id" => $reqID);

			if(empty($UserID) || empty($reqID) || empty($response)){
				throw new Exception("Required parameter missing");
			}

			if ($response == "accept") {
				$status = 1;
				$CareGiverTableObj->updateCareGiver($whereArr, array('status' => $status));

				//UserInfo
				$where = array("UserID"=>$UserID);
				$columns = array('FirstName','LastName','Email');
				$userData = $IndUserTableObj->getUser($where,$columns);
				$UserName = $userData[0]['FirstName'].' '.$userData[0]['LastName'];

				//CaregiverInfo
				$whereArr1 = array("care.Id" => $reqID);
				$caregiverData = $CareGiverTableObj->getCaregiver($whereArr1);
				$CaregiverID = $caregiverData[0]['UserID'];
				$CaregiverName = $caregiverData[0]['FirstName'];
				$CaregiverEmail = $caregiverData[0]['Email'];

				//Save Notification
				$type = 'caregiver_accept';
				$notDataArr = array("sender_id"=>$UserID,"receiver_id"=>$CaregiverID,"user_type"=>1,"created_date"=>round(microtime(true) * 1000));
				$saveNoti = $NotificationTableObj->SaveNotification($notDataArr, $type);

				//Send Mail
				$message = "Dear $CaregiverName,<br>";
				$message .= "<br>$UserName has accepted your Caregiver request.  You can easily manage all your connections in the My Connections section of your account on your web platform or mobile Q app.";
				$message .="<br><br>Here’s to your health,<br>The Quantextual Health Team";
				$subject = "Caregiver Request Accepted";
				$plugin->senInvitationMail($CaregiverEmail, $subject, $message);

			} else {
				$CareGiverTableObj->deleteCareGiver($whereArr);
			}
		}
		catch(Exception $e){
			$sucess = TRUE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
            return array("errstr"=>"Success.","success"=>1);
        }
        else{
            return array("errstr"=>$error,"success"=>0);
        }
    }



    /*
     * @this is for researcher list 
     * @Auth: Rachit Jain
     * Date: 02-03-2015
     */
	public function researcherlist($service_key) {
        $sucess = TRUE;
        $IndUserTableObj = $this->getServiceLocator()->get("ServicesUsersTable");
		$IndStudObj = $this->getServiceLocator()->get("ServicesStudyTable");
		$plugin = $this->CustomControllerPlugin1();  // custom plugin

		try{
			$data = $this->params;
			$UserID = isset($data['user_id']) ? $data['user_id'] : '';
			$page = isset($data['page']) ? $data['page'] : '1';
			$maxLimit = 10;
			$lowerLimit = ($page -1) * $maxLimit ;

			$SecurityKeyObj = $this->getServiceLocator()->get("ServicesSecurityTable");
			$whereArrKey = array("user_id"=>$UserID, "service_key"=>$service_key);
			$userSecurityKey = $SecurityKeyObj->getSecurityKey($whereArrKey);
			if (COUNT($userSecurityKey) <= 0){
				throw new Exception("Authentication Failed");
			}

			$lookingfor = '';
			$searchcontent = array();
			$whereArr = array("part.UserID" => $UserID);
			$columnArr = array('part_id','UserID','study_id');
			$researcherRes = array();
			
			$totalCount = count($IndStudObj->getStudyResearcher($whereArr, $columnArr, $lookingfor, $searchcontent,"",""));
			$totalpage = $page = '';
			$page = (int)($totalCount/$maxLimit);
			if(($page*$maxLimit) < $totalCount){
				$totalpage = $page+1;
			}
			else{
				$totalpage = $page;
			}

			$researcherRes = $IndStudObj->getStudyResearcher($whereArr, $columnArr, $lookingfor, $searchcontent, $lowerLimit, $maxLimit);
			//print_r($researcherRes);exit;

			
			$i=0;
			foreach($researcherRes as $key1=>$val1){
				//Get full image url
				$image = "";
				if($val1['Image'] != ""){
					$server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
					//Get Original Image
					//$image = $server_url."/upload_image/".$val1['pass_researcher_id']."/".$val1['Image'];

					//Get Image From CacheImg Folder
					$imagepath = $server_url."/upload_image/".$val1['pass_researcher_id']."/";
					$imagename = $val1['Image'];
					$newWidth = 150;
					$newHeight = 150;
					$image = $plugin->resizeimage($imagepath, $imagename, $newWidth, $newHeight);
				}
				$researcherRes[$i]['Image'] = $image;
				$i++;
			}
			
			//print_r($researcherRes);exit;
		}
		catch(Exception $e){
			$sucess = FALSE;
			$error = $e->getMessage();
		}

		if($sucess === TRUE){
            return array("errstr"=>"Success.","success"=>1,"researcherArr"=>$researcherRes,"total"=>$totalpage);
        }
        else{
            return array("errstr"=>$error,"success"=>0);
        }
    }

}
