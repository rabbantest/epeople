<?php

/* * *****
 * Purpose:This file is used to manage the MongoDB in 
 * 
 * ********* */

namespace Services\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\PluginManager;

Class MongoapiController extends AbstractActionController {

    protected $_mongoDB;
    protected $_table = 'ipl_admin';

    public function __construct() {
       	$mongo = new \MongoClient(MongoConnect);
        $this->mongoDB = $mongo->ipl_rajkot;
    }

    //usercounter, teamcounter
    public function getUsers($where = array(), $findOne = null) {
        try {
        	$returnData = array();
            $collection = $this->mongoDB->ipl_Users;
            if ($findOne) {
                $returnData = $collection->findOne($where);
            } else {
                $NEWcursor = $collection->find($where);
                 foreach ($NEWcursor as $doc) {
                    $returnData[] = $doc;
                }
            }
            return $returnData;
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function saveUsers($data = array()) {
        try {
            $collection = $this->mongoDB->ipl_Users;
            $sequence = $this->getNextSequence("user_id", "usercounter");
            $data['id'] = strval($sequence);
            $NEWcursor = $collection->insert($data);

            return $data['id'];
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateUsers($where = array(), $data = array()) {
        try {
            $collection = $this->mongoDB->ipl_Users;
            $newdata = array('$set' => $data);
            $Update = $collection->update($where, $newdata, array('upsert' => true));
            return true;
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /* For dream teams */

    public function saveDreamTeams($data = array()) {
        try {
            $collection = $this->mongoDB->ipl_Dreamteam;
            $sequence = $this->getNextSequenceTeam("team_id", "teamcounter");
            $data['team_id'] = strval($sequence);
            $NEWcursor = $collection->insert($data);

            return $data['team_id'];
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateTeam($where = array(), $data = array()) {
        try {
            $collection = $this->mongoDB->ipl_Dreamteam;
            $newdata = array('$set' => $data);
            $Update = $collection->update($where, $newdata, array('upsert' => true));
            return true;
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function deleteTeam($where = array()) {
        $collection = $this->mongoDB->ipl_Dreamteam;
        $Update = $collection->remove($where);
    }

    public function getTeam($where = array(), $findOne = null,$limit=null,$skip=null) {
        try {
            $returnData = array();
            $collection = $this->mongoDB->ipl_Dreamteam;
            if ($findOne) {
                $returnData = $collection->findOne($where);
            } else {
                $NEWcursor = $collection->find($where);
                if(!empty($limit)){
                	$NEWcursor->limit($limit);
                }
                if(!empty($skip)){
                	$NEWcursor->skip($skip);
                }
                foreach ($NEWcursor as $doc) {
                    $returnData[] = $doc;
                }
            }


            return $returnData;
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function removeTeamPlayers($where = array()) {
        $collection = $this->mongoDB->ipl_Dreamteam_players;
        $Update = $collection->remove($where);
    }

    public function find($table, $where = array(), $findOne = null) {
        try {
        	$returnData = array();
        	$collection = $this->mongoDB->$table;
            if ($findOne) {
                $returnData = $collection->findOne($where);
            } else {
                $NEWcursor = $collection->find($where);
                 foreach ($NEWcursor as $doc) {
                    $returnData[] = $doc;
                }
            }

            return $returnData;
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function addTeamPlayers($data = array()) {
        try {

            $collection = $this->mongoDB->ipl_Dreamteam_players;
            $NEWcursor = $collection->insert($data);
            return $data['_id'];
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getNextSequence($name, $collectionname) {
        $collection = $this->mongoDB->$collectionname;
        $retval = $collection->findAndModify(
                array('id' => $name), array('$inc' => array("seq" => 1)), null, array(
            "new" => true,
                )
        );
        return $retval['seq'];
    }

    public function getNextSequenceTeam($name, $collectionname) {
        $collection = $this->mongoDB->$collectionname;
        $retval = $collection->findAndModify(
                array('team_id' => $name), array('$inc' => array("seq" => 1)), null, array(
            "new" => true,
                )
        );
        return $retval['seq'];
    }
    
      public function getQuestions($where = array(), $findOne = null,$limit=null) {
        try {
        	$returnData = array();
            $collection = $this->mongoDB->ipl_Trivia_Questions;
            if ($findOne) {
                $returnData = $collection->findOne($where);
            } else {
                $NEWcursor = $collection->find($where);
				if(!empty($limit)){
					$NEWcursor->limit($limit);
				}
                 foreach ($NEWcursor as $doc) {
                    $returnData[] = $doc;
                }
            }
            return $returnData;
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
      public function addUsersQuestions($data = array()) {
        try {

            $collection = $this->mongoDB->ipl_Get_Questions;
            $NEWcursor = $collection->insert($data);
            return $data['_id'];
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
     public function updateQuestions($where = array(), $data = array()) {
        try {
            $collection = $this->mongoDB->ipl_Get_Questions;
            $newdata = array('$set' => $data);
            $Update = $collection->update($where, $newdata, array('upsert' => true));
            return true;
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}

?>
