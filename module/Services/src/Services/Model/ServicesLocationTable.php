<?php
/*************************
    * PAGE: USE TO MANAGE User follower relation
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Rabban Ahmad
    * CREATOR: 22/1/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class ServicesLocationTable extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_user_geofence';
    public $geodatatable = 'hm_geofence_data';
    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }
    
      /*********
        *
        * @param array $where            
	*****/
    
    public function SaveFollower($data = array()) {
        try {
            $insert =   $this->insert($data); 
            if($insert){
				$lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                return $lastId;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
    
	/*
	@ : This function is used to udpate follower status accordingly
	@ : Created: 22/1/15
	*/
	 public function updateGeofence($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute(); 
			return true;
        } catch (\Exception $e) {
			echo $e->getPrevious()->getMessage();die;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	
    public function getGeofenceRequest($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $lowerLimit='', $maxLimit='') {
	try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'geo' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

			$user=array('UserID','FirstName','LastName','UserName','Image','Email','Dob');
			$select->join(array('us' =>'hm_users'), "geo.caregiver_id = us.UserID", $user, 'INNER');
			$select->join(array('hm_con' =>'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
			$select->join(array('states' =>'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
			$select->join(array('country' =>'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

			$select->group('geo.id');
			//$select->order('common_name ASC');

			if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	
	/*
	@ : This function used to delete the geofence request
	*/
	public function deleteGeofecneRequest($where = array()) {
        try {
				$this->delete($where);
				return true;
		} catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }


}
