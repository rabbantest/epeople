<?php
/*************************
    * PAGE: USE TO MANAGE User follower relation
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Rabban Ahmad
    * CREATOR: 22/1/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class ServicesBeaconDataTable extends AbstractTableGateway
{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_beacon_data';
    public $rule_table = 'hm_beacon_rule';
    public $associated_table = 'hm_associated_beacons';
    public $users_becons = 'hm_users_becons';
    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }
    
      /*********
        *
        * @param array $where            
	*****/
    
    public function SaveBeaconData($data = array()) {
        try {
            $insert =   $this->insert($data); 
            if($insert){
				$lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                return $lastId;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
    
	/*
	@ : This function is used to udpate follower status accordingly
	@ : Created: 22/1/15
	*/
	 public function updateBeaconData($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            //echo $update->getSqlString($this->getAdapter()->getPlatform());exit;
            $result = $statement->execute(); 
			return true;
        } catch (\Exception $e) {
			echo $e->getPrevious()->getMessage();die;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	
    public function getBeaconData($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $lowerLimit='', $maxLimit='') {
	try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'bec' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            $ubArr = array('becon_title');
            $select->join(array('ub' =>$this->users_becons), "bec.beacon_id = ub.id", $ubArr, 'INNER');

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            /*if (count($where) > 0) {
                $select->where($where);
            }*/
            
            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
					if($key == 'start') {
						$select->where(new \Zend\Db\Sql\Predicate\Expression("CONCAT(bec.date, ' ', bec.time) >= '$value'"));
                    }
                    elseif($key == 'end') {
						$select->where(new \Zend\Db\Sql\Predicate\Expression("CONCAT(bec.date, ' ', bec.time) < '$value'"));
                    }
                     else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }

            $select->order(array('time DESC'));

            if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
    public function getBeaconDataRelatedCaregivers($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array()) {
	try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'bec' => $this->rule_table
            ));

            $rule=array();
            $select->join(array('rt' =>$this->table), "rt.beacon_id = bec.beacon_id", $rule, 'INNER');
            $user=array('FirstName','LastName','Image','Email');
			$select->join(array('us' =>'hm_users'), "bec.caregiver_id = us.UserID", $user, 'LEFT');

			if (count($where) > 0) {
                $select->where($where);
            }

			if($lookingfor){
				/*$select->where->equalTo('time', $lookingfor)
						->where->nest->equalTo('rt.start_time', $lookingfor)
						->or->where->equalTo('rt.end_time', $lookingfor);*/
				$select->where(new \Zend\Db\Sql\Predicate\Expression("bec.start_time < '$lookingfor'"));
				$select->where(new \Zend\Db\Sql\Predicate\Expression("bec.end_time >= '$lookingfor'"));
			}

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            $select->group('bec.caregiver_id');

            $statement = $sql->prepareStatementForSqlObject($select);
			//echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


	/*
	@ : This function is used to delete the symtoms
	*/
	public function deleteBeaconData($where = array()) {
        try {
				$this->delete($where);
				return true;
		} catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
    
    
    public function checkBeaconData($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array()) {
	try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'bec' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
}
	
