<?php
/*************************
    * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Shivendra Suman
    * CREATOR: 10/12/2014.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class ServicesSleepRecordTable extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_sleep_records';

    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getsleepRecord($where = array(), $columns = array(), $lookingfor = false, $notEqual = array(), $limit = '', $group = '') {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'sle' => $this->table
            ));
            
            $select->join(array('source' => 'hm_validic_app_source'), "source.source = sle.source", array('source_name','source','source_image'), 'INNER');
            
			if (is_array($where) && count($where) > 0) {         
                    foreach ($where as $key => $value) {
                    if ($key == 'sleep_record_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("sleep_record_date <= 'DATE_SUB(CURDATE(),$value)'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            
            if (count($notEqual)) {
                foreach ($notEqual as $k => $v) {
                    $select->where->NotequalTo('sle.source', $v);
                }
            }
            
            if($limit == 1){
				$select->limit(1)->offset(0);
			}
			if($group == 1){
				$select->group('sle.source');
			}
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
			
			//echo $sql->getSqlstringForSqlObject($select);exit;
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function checksleepRecordForValidic($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'sle' => $this->table
            ));

			if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
			
			//echo $sql->getSqlstringForSqlObject($select);exit;
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
      /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    
    public function insertsleepRecord($data = array()) {
        try {
            
         $insert =   $this->insert($data); 
            if($insert){
                return true;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
    
                /*
     * get record by year, month, week
     */

    public function getsleepRecordByYear($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'sle' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'start_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("sleep_record_date >= '$value'"));
                    } elseif ($key == 'end_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("sleep_record_date <= '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(sleep_record_date) As Day'),
                        new \Zend\Db\Sql\Expression('monthname(sleep_record_date) As Month'),
                        new \Zend\Db\Sql\Expression('year(sleep_record_date) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(awake) As Avgawake'),
                        new \Zend\Db\Sql\Expression('avg(deep) As Avgdeep'),
                        new \Zend\Db\Sql\Expression('avg(light) As Avglight'),
                        new \Zend\Db\Sql\Expression('avg(rem) As Avgrem'),
                        new \Zend\Db\Sql\Expression('avg(times_woken) As Avgtimes_woken'),
                        new \Zend\Db\Sql\Expression('avg(total_sleep) As Avgtotal_sleep'),
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
    /*
     * get record of one day
     */
    public function getSleepRecordOfDay($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'sle' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'current_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("sleep_record_date = '$value'"));
                    } elseif ($key == 'start_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("sleep_record_time >= '$value'"));
                    } elseif ($key == 'end_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("sleep_record_time < '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(sleep_record_date) As Day'),
                        new \Zend\Db\Sql\Expression('monthname(sleep_record_date) As Month'),
                        new \Zend\Db\Sql\Expression('year(sleep_record_date) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(awake) As Avgawake'),
                        new \Zend\Db\Sql\Expression('avg(deep) As Avgdeep'),
                        new \Zend\Db\Sql\Expression('avg(light) As Avglight'),
                        new \Zend\Db\Sql\Expression('avg(rem) As Avgrem'),
                        new \Zend\Db\Sql\Expression('avg(times_woken) As Avgtimes_woken'),
                        new \Zend\Db\Sql\Expression('avg(total_sleep) As Avgtotal_sleep'),
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $sql->getSqlstringForSqlObject($select);exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
}
