<?php
/*************************
    * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Rabban Ahmad
    * CREATOR: 10/12/2014.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/

namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;



class ServicesUsersTable extends AbstractTableGateway
{

    /*********
        *
        * @var String
    *****/
    public $table = 'hm_users';
    public $devicetable = 'hm_user_devices';

    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }
    
    
    public function getRedeemHistoy($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $lowerLimit='',$maxLimit='') {

        $orderBy = "id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'Red' => 'hm_users_redeem_history'
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
               
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);

			if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}
			
			//echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


    public function insertRedeemHistoy($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert('hm_users_redeem_history');
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getUser($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {

            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

	public function updateUser($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            //echo $update->getSqlString($this->getAdapter()->getPlatform());exit;
            $result = $statement->execute(); 
            if($result){
                return true;
            }  else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    } 

	public function saveUser($dataArry) {
        try {
			 $this->insert($dataArry);
			 $lastId = $this->adapter->getDriver()->getLastGeneratedValue();
			 return $lastId;
            } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    } 

	public function sendMail($to,$from,$subject,$body) {
        try {
			 $message = new Message();
		     $message->addTo($to)
					->addFrom($from)
					->setSubject($subject)
					->setBody($body);

			 $transport = new SendmailTransport();
			 $transport->send($message);
            } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

	/* Used for getting the list of language */
	public function getLanguage($where = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_language'
            ));
			if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $sql->getSqlstringForSqlObject($select);exit;
            $country = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $country;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

	/* Used for getting the list of Blood type */
	public function getBloodType($where = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_blood_type'
            ));
			if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            $country = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $country;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

	/* Used for getting the list of Ethinicy */
	public function getEthnicity($where = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_ethnicity'
            ));
			if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $sql->getSqlstringForSqlObject($select);exit;
            $country = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $country;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

	/* Used for getting the list of Education */
	public function getEducation($where = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_highest_education'
            ));
			if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            $country = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $country;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    /* Used for getting the list of income */
	public function getIncome($where = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_income'
            ));
			if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $sql->getSqlstringForSqlObject($select);exit;
            $country = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $country;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getvalidicconnectedUser($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => $this->table
            ));

			$select->where->NotequalTo('validic_id', '');
			$select->where->NotequalTo('validic_access_token', '');

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {

            }
            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $sql->getSqlstringForSqlObject($select);exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


	public function SaveDeviceToken($dataArr) {
        try {
				$sql = new Sql($this->getAdapter());
				$insert = $sql->insert('hm_user_devices');
				$insert->values($dataArr);
				$selectString = $sql->getSqlStringForSqlObject($insert);
				//echo $selectString = $sql->getSqlStringForSqlObject($insert);exit;
				$results = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
				$lastId = $this->getAdapter()->getDriver()->getLastGeneratedValue();
				return $lastId;
            } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
    public function getDeviceToken($where = array(), $columns = array(), $lookingfor = false, $notequalto = '') {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'dt' => $this->devicetable
            ));
            
            if (count($where) > 0) {
                $select->where($where);
            }
            
            if(!empty($notequalto)){
				$select->where->NotequalTo('DeviceUdid', $notequalto);
			}
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {

            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
    public function updateDeviceToken($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->devicetable);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            //echo $update->getSqlString($this->getAdapter()->getPlatform());exit;
            $result = $statement->execute(); 
            if($result){
                return true;
            }  else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    } 
    
    
    public function deleteDeviceToken ($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->devicetable);

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


	//Get Default Source
    public function getSourceList($where = array(), $columns = array(), $lookingfor = false, $searchContent = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'vs' => 'hm_validic_app_source'
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if ($lookingfor) {
                $select->join(array('cs' => 'hm_connected_sources'), "cs.sourceId = vs.id", array(), 'INNER');
            }
            
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


	public function SaveConnectedSource($dataArr) {
        try {
				$sql = new Sql($this->getAdapter());
				$insert = $sql->insert('hm_connected_sources');
				$insert->values($dataArr);
				$selectString = $sql->getSqlStringForSqlObject($insert);
				//echo $selectString = $sql->getSqlStringForSqlObject($insert);exit;
				$results = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
				$lastId = $this->getAdapter()->getDriver()->getLastGeneratedValue();
				return $lastId;
            } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


    public function updateConnectedSource($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table('hm_connected_sources');
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            //echo $update->getSqlString($this->getAdapter()->getPlatform());exit;
            $result = $statement->execute(); 
            if($result){
                return true;
            }  else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


    public function deleteConnectedSource($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from('hm_connected_sources');

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


    //Get Connected Source
    public function getConnectedSource($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cs' => 'hm_connected_sources'
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            
            $sourceFields = array('source','source_name');
            $select->join(array('vs' => 'hm_validic_app_source'), "vs.id = cs.sourceId", $sourceFields, 'LEFT');

            if ($lookingfor) {
                
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


	public function SaveDefaultSource($dataArr) {
        try {
				$sql = new Sql($this->getAdapter());
				$insert = $sql->insert('hm_measurement_defaults');
				$insert->values($dataArr);
				$selectString = $sql->getSqlStringForSqlObject($insert);
				//echo $selectString = $sql->getSqlStringForSqlObject($insert);exit;
				$results = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
				$lastId = $this->getAdapter()->getDriver()->getLastGeneratedValue();
				return $lastId;
            } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
    public function updateDefaultSource($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table('hm_measurement_defaults');
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            //echo $update->getSqlString($this->getAdapter()->getPlatform());exit;
            $result = $statement->execute(); 
            if($result){
                return true;
            }  else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


    //Get Default Source
    public function getDefaultSource($where = array(), $columns = array(), $lookingfor = false, $type = "") {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'ds' => 'hm_measurement_defaults'
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

			$sourceFields = array('id','source_name','source','source_image');
			if($type == 'Height/Weight'){
				$select->join(array('vs' => 'hm_validic_app_source'), "vs.id = ds.bodyweight", $sourceFields, 'LEFT');
			}
			if($type == 'Steps'){
				$select->join(array('vs' => 'hm_validic_app_source'), "vs.id = ds.step", $sourceFields, 'LEFT');
			}
			if($type == 'PhysicalData'){
				$select->join(array('vs' => 'hm_validic_app_source'), "vs.id = ds.physicalactivity", $sourceFields, 'LEFT');
			}
			if($type == 'Cholesterol'){
				$select->join(array('vs' => 'hm_validic_app_source'), "vs.id = ds.cholesterol", $sourceFields, 'LEFT');
			}
			if($type == 'Pressure'){
				$select->join(array('vs' => 'hm_validic_app_source'), "vs.id = ds.bloodpressure", $sourceFields, 'LEFT');
			}
			if($type == 'Glucose'){
				$select->join(array('vs' => 'hm_validic_app_source'), "vs.id = ds.bloodglucose", $sourceFields, 'LEFT');
			}
			if($type == 'SleepRecord'){
				$select->join(array('vs' => 'hm_validic_app_source'), "vs.id = ds.sleep", $sourceFields, 'LEFT');
			}
			if($type == 'Temperature'){
				$select->join(array('vs' => 'hm_validic_app_source'), "vs.id = ds.temperature", $sourceFields, 'LEFT');
			}

            if ($lookingfor) {
                
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    public function SaveDashboardSettings($dataArr) {
        try {
				$sql = new Sql($this->getAdapter());
				$insert = $sql->insert('hm_dashboard_settings');
				$insert->values($dataArr);
				$selectString = $sql->getSqlStringForSqlObject($insert);
				//echo $selectString = $sql->getSqlStringForSqlObject($insert);exit;
				$results = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
				$lastId = $this->getAdapter()->getDriver()->getLastGeneratedValue();
				return $lastId;
            } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
    public function updateDashboardSettings($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table('hm_dashboard_settings');
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            //echo $update->getSqlString($this->getAdapter()->getPlatform());exit;
            $result = $statement->execute(); 
            if($result){
                return true;
            }  else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


    //Get Default Source
    public function getDashboardSettings($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'ds' => 'hm_dashboard_settings'
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if ($lookingfor) {
                
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    /****This function is used to get the Domain name ******/
    public function getDomainName($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'subdomains' => 'hm_pass_subdomain'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }
           
            $statement = $sql->prepareStatementForSqlObject($select);
           
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user[0];
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
}
