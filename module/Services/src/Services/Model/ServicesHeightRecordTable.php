<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman
 * CREATOR: 10/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class ServicesHeightRecordTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_height_records';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getHeightRecord($where = array(), $columns = array(), $lookingfor = false, $notEqual = array(), $limit = '', $group = '') {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'hr' => $this->table
            ));
            
            $select->join(array('source' => 'hm_validic_app_source'), "source.source = hr.source", array('source_name','source','source_image'), 'INNER');

			//$select->join(array('cs' => 'hm_connected_sources'), "cs.sourceId = source.id", array(), 'INNER');
			
            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'HeightRecordDate') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("HeightRecordDate <= 'DATE_SUB(CURDATE(),$value)'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }

            if (count($notEqual)) {
                foreach ($notEqual as $k => $v) {
                    $select->where->NotequalTo('hr.source', $v);
                }
            }
            
            if($limit == 1){
				$select->limit(1)->offset(0);
			}
			if($group == 1){
				$select->group('hr.source');
			}

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }

			//echo $sql->getSqlstringForSqlObject($select);exit;
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function insertHeightRecord($data = array()) {
        try {

            $insert = $this->insert($data);
            if ($insert) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*
     * retrive one year data 
     */

    public function getOneYearMontlyData($where = array(), $lookingfor = false, $forwhich = 'all') {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'hr' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'HeightRecordDate'  ) {
                        if($value != 'all'){
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("HeightRecordDate <= 'DATE_SUB(NOW(),$value)'"));
                        }
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            if ($forwhich == 'all') {
                $select->columns(
                        array(
                            new \Zend\Db\Sql\Expression('monthname(HeightRecordDate) As Month'),
                            new \Zend\Db\Sql\Expression('year(HeightRecordDate) AS Year'),
                            new \Zend\Db\Sql\Expression('avg(Height) As AvgValue')
                        )
                );
                if ($lookingfor) {
                    $select->order($lookingfor);
                }

                $select->group(
                        array(
                            new \Zend\Db\Sql\Expression('monthname(HeightRecordDate)'),
                            new \Zend\Db\Sql\Expression('year(HeightRecordDate)')
                        )
                );
            } else {
                $select->columns(
                        array(
                            new \Zend\Db\Sql\Expression('DAYNAME(HeightRecordDate) As Month'),
                            new \Zend\Db\Sql\Expression('monthname(HeightRecordDate) AS Year'),
                            new \Zend\Db\Sql\Expression('avg(Height) As AvgValue')
                        )
                );

                if ($lookingfor) {
                    $select->order($lookingfor);
                }
                $select->group(
                        array(
                            new \Zend\Db\Sql\Expression('DAYNAME(HeightRecordDate)'),
                        // new \Zend\Db\Sql\Expression('monthname(HeightRecordDate)')
                        )
                );
            }




            $statement = $sql->prepareStatementForSqlObject($select);
//            $statement->prepare();
//            echo $statement->getSql();
//            exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();

            return $data;
        } catch (\Exception $e) {
            
        }
    }
    
    /*
	@ This method is used to get record by year, month, week
	@Created : 10/12/14
	@By : Shivendra Suman
	*/

    public function getHeightRecordByYear($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'hr' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'start_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("HeightRecordDate >= '$value'"));
                    } elseif ($key == 'end_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("HeightRecordDate <= '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(HeightRecordDate) As Day'),
                        new \Zend\Db\Sql\Expression('monthname(HeightRecordDate) As Month'),
                        new \Zend\Db\Sql\Expression('year(HeightRecordDate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(Height) As Avgheight')
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
    /*
     * get record of one day
     */
    public function getHeightRecordOfDay($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'hr' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
					if ($key == 'current_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("HeightRecordDate = '$value'"));
                    } elseif ($key == 'start_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("HeightRecordTime >= '$value'"));
                    } elseif ($key == 'end_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("HeightRecordTime < '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(HeightRecordDate) As Day'),
                        new \Zend\Db\Sql\Expression('monthname(HeightRecordDate) As Month'),
                        new \Zend\Db\Sql\Expression('year(HeightRecordDate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(Height) As Avgheight')
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $sql->getSqlstringForSqlObject($select);exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
