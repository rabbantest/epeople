<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE INDIVIDUAL Blog.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman
 * CREATOR: 04/02/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class ServicesBlogTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_super_blog';
    public $tablecat = 'hm_super_blog_cat';
    public $tableaut = 'hm_super_blog_author';



    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getBlogs($where = array(), $columns = array(), $lookingfor = false, $group = false, $searchcontent = array(), $lowerLimit='', $maxLimit='') {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'blog' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }
            $select->join(array('cat'=>$this->tablecat), "blog.cat_id=cat.cat_id", array("cat_id","cat_name"),"LEFT");
            $select->join(array('auth'=>$this->tableaut), "blog.author_id=auth.author_id", array("author_id","author_name"),"LEFT");
        
            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if ($lookingfor) {
                $select->order($lookingfor);              
            }
            if ($group) {             
                $select->group($group);
            }

			if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}
          //echo $select->getSqlString($this->getAdapter()->getPlatform());

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getBlogcat($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'blogc' => $this->tablecat
            ));

            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if ($lookingfor) {
                $select->order($lookingfor);
            }


            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
        /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getBlogauth($where = array(), $columns = array(), $lookingfor = false, $group = false, $searchcontent = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'blogauth' => $this->tableaut
            ));

            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if ($lookingfor) {
                $select->order($lookingfor);              
            }
            if ($group) {             
                $select->group($group);
            }

          //echo $select->getSqlString($this->getAdapter()->getPlatform());

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
