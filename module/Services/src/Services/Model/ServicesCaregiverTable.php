<?php
/*************************
    * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Rabban Ahmad
    * CREATOR: 16/2/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/

namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;


class ServicesCaregiverTable extends AbstractTableGateway
{

    /*********
        *
        * @var String
    *****/
    public $table = 'hm_caregivers';

    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }
	/*********
	*This function is used to save contact iformation
	*
	*********/
	public function saveCareGiver($dataArry) {
        try {
			 $this->insert($dataArry);
            } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    } 
	
	public function updateCareGiver($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            // echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute(); 
             if($result){
                return true;
            }  else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    } 
    
	/*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    
    public function deleteCareGiver($where = array()) {
        try {
            
         $insert =   $this->delete($where); 
            if($insert){
				$lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                return $lastId;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
      /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getCareGiver($where = array(), $columns = array(), $lookingfor = false, $lowerLimit='', $maxLimit='') {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'care' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
			$user=array('UserID','FirstName','LastName','UserName','Image','Email','Dob');
			$select->join(array('us' =>'hm_users'), "care.caregiver_id = us.UserID", $user, 'INNER');
            $select->join(array('hm_con' =>'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
			$select->join(array('states' =>'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
			$select->join(array('country' =>'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');

            $select->group('care.caregiver_id');
            if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}
			//echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	 
	
}
