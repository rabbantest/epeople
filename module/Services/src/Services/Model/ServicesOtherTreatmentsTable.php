<?php
/*************************
    * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Shivendra Suman
    * CREATOR: 10/12/2014.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class ServicesOtherTreatmentsTable extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_super_othertreatments';
    public $subTable = 'hm_super_subconditions';

    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }


    
      /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    
    public function SaveOtherTreatment($data = array()) {
        try {
            
         $insert =   $this->insert($data); 
            if($insert){
				$lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                return $lastId;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
	
	/**
	@ This method is used to get the other treatment
	@ Created:8/1/15
	**/
	public function getOtherTreatment($where = array(), $columns = array(),$searchContent=array()) {
	try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'su_ot_tre' => $this->table
            ));
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }
			$statement = $sql->prepareStatementForSqlObject($select);
			//$statement->prepare();
			//echo $statement->getSql();exit;
            $Ucondtions = $this->resultSetPrototype->initialize($statement->execute())->toArray();
			return $Ucondtions;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	
    /*
	@ : This function is used to update the user treatment
	@ : Created: 6/1/15
	*/
	 public function updateUserOtherTreatments($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            //echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute(); 
        } catch (\Exception $e) {
			echo $e->getPrevious()->getMessage();die;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	/*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    
    public function deleteUserOtherTreatments($where = array()) {
        try {
            
         $insert =   $this->delete($where); 
            if($insert){
				$lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                return $lastId;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
	
    public function getUserConditionsList($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array()) {

      try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => $this->table
            ));
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                //$select->join(array('sb' => $this->subTable), "us.id = sb.condition_id", $subcondition, 'LEFT');
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            //$select->order('common_name ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
