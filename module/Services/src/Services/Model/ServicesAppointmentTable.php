<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE INDIVIDUAL Appointment.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman
 * CREATOR: 04/02/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class ServicesAppointmentTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_user_appointment';
    public $apstatus = 'hm_user_appointment_status';



    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAppoiRecord($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array()) {

		try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'Appoi' => $this->table
            ));
            
            $select->join(array('upt' => 'hm_user_purpose_type'), "upt.purpose_type_id = Appoi.purpose", array('purpose_type'), 'INNER');

            //if (is_array($where) && count($where) > 0) {
            //    $select->where($where);
            //}

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'month') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("MONTHNAME(appointment_date) = '$value'"));
                    }
                    elseif($key == 'year') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("YEAR(appointment_date) = '$value'"));
                    }
                    elseif($key == 'start') {
						$select->where(new \Zend\Db\Sql\Predicate\Expression("CONCAT(appointment_date, ' ', appointment_time) >= '$value'"));
                    }
                    elseif($key == 'end') {
						$select->where(new \Zend\Db\Sql\Predicate\Expression("CONCAT(appointment_date, ' ', appointment_time) < '$value'"));
                    }
                     else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if ($lookingfor) {
                $select->order($lookingfor);
            }

			//echo $select->getSqlString($this->getAdapter()->getPlatform());exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function insertAppoi($data = array()) {
        try {

            $insert = $this->insert($data);
            if ($insert) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateAppoi($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }

            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            $rowsAffected = $result->getAffectedRows();
            if ($rowsAffected) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAppStatus($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'Aps' => $this->apstatus
            ));

            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if ($lookingfor) {
                $select->order($lookingfor);
            }


            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
