<?php
/*************************
    * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Rabban ahmad
    * CREATOR: 10/12/2014.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class ServicesUserCheckupTable extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_users_checkup';
    public $subTable = 'hm_super_subconditions';
    public $checkuplistTable = 'hm_checkup_questions_list';
    public $checkupansTable = 'hm_user_checkup_answer';

    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }


    
     /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    
    public function SaveUserCheckup($data = array()) {
        try {
            
         $insert =   $this->insert($data); 
            if($insert){
				$lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                return $lastId;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
    
    
	/* This function is used to get the User checkup */
	public function getUserCheckups($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $group=null, $lowerLimit='', $maxLimit='') {
	try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => $this->table
            ));
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                 $select->order($lookingfor);	
                //$select->join(array('sb' => $this->subTable), "us.id = sb.condition_id", $subcondition, 'LEFT');
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }
           
            if($group){
                $select->group($group);
            }
            //$select->order('common_name ASC');
            if(!empty($maxLimit)){
				$select->limit($maxLimit)->offset($lowerLimit);
			}
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	
	/*
	@ : This function is used to update the user symtoms
	@ : Created: 6/1/15
	*/
	public function updateUserCheckup($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            //echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute(); 
        } catch (\Exception $e) {
			echo $e->getPrevious()->getMessage();die;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    } 
	/*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    
    public function deleteUserCheckup($where = array()) {
        try {
            
         $insert =   $this->delete($where); 
            if($insert){
				$lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                return $lastId;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
   	public function getImunizationType($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array()) {

      try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'imu' => 'hm_imunizations'
            ));
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            //$select->order('common_name ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    /*
     * @ Get list of checkup according to age and gender
     * @author: shivendra Suman
     * @Created Date: 27-01-2015.
     */
    
    function checkupList($where = array(), $columns = array(), $lookingfor = NULL) {
        try {
             $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cklist' => $this->checkuplistTable
            ));
            
              foreach ($where as $key => $value) {
                    if ($key == 'age') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("checkup_from_age <= '$value'"));
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("checkup_to_age >= '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
          
            $statement = $sql->prepareStatementForSqlObject($select);
//            $statement->prepare();
//            echo $statement->getSql();exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        }
         /*
     * @ Insert user question answer of checkup 
     * @author: shivendra Suman
     * @Created Date: 27-01-2015.
     */
    public function insertUsercheckupQA($data = array()) {
        try {
           $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->checkupansTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }


    /*
	@ : This function is used to update the user checkup answers
	@ : Created: 21/04/15
	*/
	public function updateUserCheckupQA($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->checkupansTable);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }

            $statement = $sql->prepareStatementForSqlObject($update);
            //echo $update->getSqlString($this->getAdapter()->getPlatform());exit;
            $result = $statement->execute(); 
        } catch (\Exception $e) {
			echo $e->getPrevious()->getMessage();die;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    } 


	/* This function is used to get checkup full details with questions and answers */
	public function getCheckupDetails($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $group=null) {
	try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'ca' => $this->checkupansTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

			$cqArr = array('checkup_question');
            $select->join(array('cq' =>$this->checkuplistTable), "ca.checkup_question_id = cq.checkup_question_id", $cqArr, 'INNER');

            if ($lookingfor) {

            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            if($group){
                $select->group($group);
            }
            //$select->order('common_name ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
}
