<?php
/*************************
    * PAGE: USE TO MANAGE THE Individual USER FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Shivendra Suman
    * CREATOR: 08/01/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Services\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class ServicesActivityStageTable extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_activity_stages';


    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getActivityStage($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'acs' => $this->table
            ));
              if (is_array($where) && count($where) > 0) {         
                    foreach ($where as $key => $value) {
                    if($key == 'range') {
                        $select ->where->lessThanOrEqualTo('stage_min_val', $value)
                                ->where->greaterThanOrEqualTo('stage_max_val', $value);
                               } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
           
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
       // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
      /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    
    public function insertActivityStage($data = array()) {
        try {
            
         $insert =   $this->insert($data); 
            if($insert){
                return true;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
    
    public function deleteActivityStage($where=array()) {
       try {
            
         $insert =   $this->delete($where); 
            if($insert){
	     $lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                return $lastId;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }


}
