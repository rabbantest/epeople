<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE MODULE CONFIG FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Hemant Chuphal
 * CREATOR: 17/04/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */
return array(
    'router' => array(
        'routes' => array(
            // using the path /application/:controller/:action
            'nonmember' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/nonmember',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Nonmember\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                )
            ),
            'nonmember assessment' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/nonmember-assessment',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Nonmember\Controller',
                        'controller' => 'Index',
                        'action' => 'selfassessment',
                    ),
                ),
            ), 'nonmember assessmentScore' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/nonmember-assessment-score',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Nonmember\Controller',
                        'controller' => 'Index',
                        'action' => 'scorelist',
                    ),
                ),
            ), 'nonmember assessmentScoreQuestion' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/nonmember-assessment-score-question',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Nonmember\Controller',
                        'controller' => 'Index',
                        'action' => 'scorequestionlist',
                    ),
                ),
            )
            , 'knwbase' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/knwbase',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Nonmember\Controller',
                        'controller' => 'Index',
                        'action' => 'knwbase',
                    ),
                ),
            )
        )
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Nonmember\Controller\Index' => 'Nonmember\Controller\IndexController',
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'CustomControllerPlugin' => 'Individual\Controller\Plugin\CustomControllerPlugin',
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
        //'displayDate' => 'Nonmember\View\Helper\DisplayDateHelper',
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'header/nonmemberheader' => __DIR__ . '/../view/header/nonmemberheader.phtml',
            'paginator/paginators' => __DIR__ . '/../view/nonmember/paginator/paginators.phtml',
            'layout' => __DIR__ . '/../view/layout/nonmemberlayout.phtml',
            'footer/nonmemberfooter' => __DIR__ . '/../view/footer/nonmemberfooter.phtml',
            'left-menus/nonmember-left-menu' => __DIR__ . '/../view/left-menus/nonmember-left-menu.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
?>