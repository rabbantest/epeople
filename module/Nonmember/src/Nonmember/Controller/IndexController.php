<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ROOT CONTROLLER FOR CAREGIVER APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: RABBAN AHMAD.
 * CREATOR: 27/3/15
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Nonmember\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class IndexController extends AbstractActionController {

    public function __construct() {
        
    }

    function _getCategory() {
        $selfObj = $this->getServiceLocator()->get("AssessmentNonmember");

        return $Category = $selfObj->getCategory(array('cat_status' => 1), array('cat_id', 'cat_name'), array(), '1');
    }

    function indexAction() {
        $selfObj = $this->getServiceLocator()->get("AssessmentNonmember");

        $Category = $this->_getCategory();

        $cat_id = base64_decode($this->params()->fromQuery('cat_id'));
        $cat_id = isset($Category[0]['cat_id']) && empty($cat_id) ? $Category[0]['cat_id'] : $cat_id;
        $limit = array(0, 10);
        $page = 1;
        $Assessment = array();
        if (!empty($cat_id)) {

            $columns = array('ass_id', 'ass_name', 'assessment_desc', 'assessment_image', 'publish_date');
            $where = array('ass_category' => $cat_id, 'is_publish' => 1, 'ass_status' => 1, 'ass_targets' => array(1, 3));
            if (base64_decode($this->params()->fromQuery('page')) > 0) {
                $page = base64_decode($this->params()->fromQuery('page'));
                $limit = array($limit[1] * $page, $limit[1]);
                $page++;
            }

            $Assessment = $selfObj->getAssessment($where, $columns, '1', array(), $limit);
        }

        $sendArr = new ViewModel();
        $request = $this->getRequest();
        ($request->isXmlHttpRequest()) ? $sendArr->setTerminal(true) : '';

        return $sendArr->setVariables(array(
                    'pageTitle' => 'Non-Member',
                    'Category' => $Category,
                    'Assessment' => $Assessment,
                    'page' => $page,
                    'cat_id' => $cat_id
        ));
    }

    function saveDataAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $selfObj = $this->getServiceLocator()->get("AssessmentNonmember");
            $Count = $selfObj->getAssessmentPart(array('assessment_id' => $postDataArr['assessment_id'], 'email' => $postDataArr['email']), array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'), 'status', 'id'));
            if ($Count[0]['num'] == 1 && $Count[0]['status'] == 1) {
                echo json_encode(array('feedback', base64_encode($Count[0]['id'])));
                die;
            }
            if ($Count[0]['num'] == 0) {
                $id = $selfObj->insertAssessmentPart($postDataArr);
                if ($id) {
                    echo json_encode(array('true', base64_encode($id)));
                    die;
                } else {
                    echo json_encode(array('false'));
                    die;
                }
            } else
            if ($Count[0]['num'] == 1 && $Count[0]['status'] == 0) {
                echo json_encode(array('true', base64_encode($Count[0]['id'])));
                die;
            } else {
                echo json_encode(array('false'));
                die;
            }
        }die;
    }

    function selfassessmentAction() {
        $Category = $this->_getCategory();
        $selfObj = $this->getServiceLocator()->get("AssessmentNonmember");
        $part_id = base64_decode($this->params()->fromQuery('id'));
        $Count = $selfObj->getAssessmentPart(array('id' => $part_id), array('assessment_id', 'email', 'id'));
        $assessment_id = isset($Count[0]['assessment_id']) ? $Count[0]['assessment_id'] : NULL;
        // print_r($Count);die;
        if (isset($Count[0]['assessment_id']) && !empty($Count[0]['assessment_id'])) {
            $userId = $Count[0]['id'];
            $CusConPlug = $this->CustomControllerPlugin();  // custome plugin

            if (!empty($assessment_id)) {
                //get assessment name 
                $whereAss = array("ass_id" => $assessment_id);
                $assessmentname = $selfObj->getAssessment($whereAss);

                if (count($assessmentname) > 0) {


                    $assesmentName = $assessmentname[0]['ass_name']; //assesment name
                    $assesmentDesc = $assessmentname[0]['assessment_desc']; //assesment description  
                    $assesmentQuestDesc = $assessmentname[0]['assessment_que_desc']; //assesment question description 
                    $assesmentImage = $assessmentname[0]['assessment_image']; //assesment image
                    $assesmentCopyRight = $assessmentname[0]['copyright_text']; //assesment image

                    $searchcontent = array();
                    // $itemsPerPage = 10;
                    $columns = array("ques_id", "ques_question", "ques_mendatory", "ques_variable");
                    $where = array('assessment_id' => $assessment_id);
                    $lookingfor = "ques_order ASC, ques_id asc";

                    $limit = array(0, 10); //limit of content parpage
                    $page = base64_decode($this->params()->fromQuery('page'));

                    $request = $this->getRequest();
                    if ($request->isPost()) {
                        $postDataArr = $request->getPost()->toArray();
                        for ($r = 0; $r < count($postDataArr['quest_id']); $r++) {
                            if ($postDataArr['opt_value'][$r] != '') {
                                $QuesAnsArr = array(
                                    "nonmember_id" => $userId,
                                    "assessment_id" => $assessment_id,
                                    "option_id" => $postDataArr['opt_value'][$r],
                                    "ques_variable" => $postDataArr['ques_variable'][$r],
                                    "quest_id" => $postDataArr['quest_id'][$r]
                                );
                                $insQAn = $selfObj->insertPartQuAns($QuesAnsArr);
                            }
                        }
                    }

                    if (base64_decode($this->params()->fromQuery('page')) >= 0) {
                        $page = base64_decode($this->params()->fromQuery('page'));
                        $limit = array($limit[1] * $page, $limit[1]);
                        $page++;
                    }

                    if (base64_decode($this->params()->fromQuery('page')) === 0) {
                        $selfObj->deletePartQuAns(array('nonmember_id' => $userId));
                    }

                    $assessmentData = $selfObj->getassessmentques($where, $columns, $lookingfor, array(), $limit);

                    //get tota data
                    $columnsCount = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'));
                    $aascount = $selfObj->getassessmentques($where, $columnsCount);
                    $totaldata = $aascount[0]['num']; //total data count
                    //echo $last_page*$limit[1];die;
                    //make form action 
                    if ($totaldata <= ($limit[0] + $limit[1])) { //if all assesment is finish 
                        $formaction = "./nonmember-assessment?id=" . base64_encode($part_id) . "&page=" . base64_encode('complete');
                        $button_name = "Finish";
                    } else {

                        $formaction = "./nonmember-assessment?id=" . base64_encode($part_id) . "&page=" . base64_encode($page);
                        $button_name = "Next";
                    }

                    $question = array();
                    if (is_array($assessmentData)) {
                        if (count($assessmentData) > 0) {
                            $i = 0;
                            foreach ($assessmentData as $avalue) {
                                $question_id = $avalue['ques_id'];
                                $whereOpt = array("question_id" => $question_id);
                                $coloumOpt = array("opt_option", "opt_value", "opt_status", "opt_id");
                                $optdata = $selfObj->getassquesopt($whereOpt, $coloumOpt);

                                $j = 0;
                                //append option and value in question list 
                                if (count($optdata) > 0) {
                                    foreach ($optdata as $ovalue) {
                                        $assessmentData[$i]['opt_option'][$j] = $ovalue['opt_option'];
                                        $assessmentData[$i]['opt_value'][$j] = $ovalue['opt_value'];
                                        $assessmentData[$i]['opt_id'][$j] = $ovalue['opt_id'];
                                        $j++;
                                    }
                                }
                                $i++;
                            }
                            unset($optdata);
                        }
                    }

                    //if finished all  assesment question 
                    if (base64_decode($this->params()->fromQuery('page')) == "complete") {
                        //update last date in parishipation
                        $columnsUpdaPart = array("status" => 1);
                        $selfObj->updatePartish(array("id" => $userId), $columnsUpdaPart);
                        //redirect page to mesaage     
                        $redirectUrl = "./nonmember-assessment-score?id=" . base64_encode($part_id);
                        $this->redirect()->toUrl($redirectUrl);
                    }
                } else {
                    //send error if assesment id not found
                    $this->getResponse()->setStatusCode(404);
                    return;
                }
            } else {
                //send error if assesment id not found
                $this->getResponse()->setStatusCode(404);
                return;
            }
            $viewModel = new ViewModel();
            return $viewModel->setVariables(array(
                        'pageTitle' => "Nonmember Assessment",
                        "Category" => $Category,
                        'CusConPlug' => $CusConPlug, // custome plugin
                        'assesmentDesc' => $assesmentDesc,
                        'assesmentQuestDesc' => $assesmentQuestDesc,
                        'assesmentImage' => $assesmentImage,
                        'assesmentCopyRight' => $assesmentCopyRight,
                        'formaction' => $formaction,
                        'button_name' => $button_name,
                        'assessment_id' => $assessment_id,
                        'totaldata' => $totaldata,
                        'part_id' => $part_id,
                        'cat_id' => $assessmentname[0]['ass_category'],
                        'page' => $page,
                        'limit' => $limit,
                        'assessmentData' => $assessmentData,
                        'AssessmentName' => $assesmentName,
                        'routeParams' => $searchcontent,
            ));
        } else {
            $redirectUrl = "./nonmember";
            $this->redirect()->toUrl($redirectUrl);
        }
    }

    function scorelistAction() {
        $Category = $this->_getCategory();
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $selfObj = $this->getServiceLocator()->get("AssessmentNonmember");
        $IndAssFormulaTableObj = $this->getServiceLocator()->get("AssesFormulaNonmember");
        $part_id = base64_decode($this->params()->fromQuery('id'));
        $Count = $selfObj->getAssessmentPart(array('id' => $part_id), array('assessment_id', 'email', 'id'));
        $assessment_id = isset($Count[0]['assessment_id']) ? $Count[0]['assessment_id'] : NULL;
        $response = $response = 0;

        if (!empty($assessment_id)) {
            //get assessment name 
            $whereAss = array("ass_id" => $assessment_id);
            $assessmentname = $selfObj->getAssessment($whereAss);

            $formulaDataval = $IndAssFormulaTableObj->getAssformula(array("formula_assessment" => $assessment_id), array(), "0", "formula_type");
            if (count($formulaDataval) > 0) {
                foreach ($formulaDataval as $formvalue) {
                    $formula_type = $formvalue['formula_type'];
                    if ($formula_type == "complex") {
                        $complex = 1;
                    }
                    if ($formula_type == "response") {
                        $response = 1;
                    }
                }
            } else {
                return $this->redirect()->toUrl('/nonmember-assessment');
            }
//  

            if ($response == 1 && $complex == 0) {
                $resId = base64_encode($part_id);
                $link = "/nonmember-assessment-score-question?id=$resId";
                return $this->redirect()->toUrl($link);
            }
            if ($response == 0 && $complex == 0) {
                $resId = base64_encode($part_id);
                $link = "/nonmember-assessment";
                return $this->redirect()->toUrl($link);
            }

            // exit;
            if (count($assessmentname) > 0) {
                $assesmentName = $assessmentname[0]['ass_name']; //assesment name  
                //
                //get partihipation details
                $wherePart = array("id" => $part_id, "assessment_id" => $assessment_id, "status" => 1);
                $partishipData = $selfObj->getAssessmentPart($wherePart, array(), "creation_date DESC");
                if (count($partishipData) > 0) {
                    $assID = $partishipData[0]['assessment_id'];
                    $part_id = $partishipData[0]['id'];
                } else {
                    //send error if partishipation  not found
                    $this->getResponse()->setStatusCode(404);
                    return;
                }

                //get options
                $assessmentData = array();
                $whereOptAns = array('nonmember_id' => $part_id, 'assessment_id' => $assessment_id);
                $coloum = array('option_id', 'ques_variable');
                $optAnsData = $selfObj->getPartQuAns($whereOptAns, $coloum);

//                echo '<pre>';
//print_r($assessmentData);die;
                if (count($optAnsData) > 0) {
                    $k = 0;
                    foreach ($optAnsData as $ansvalue) {
                        $assessmentData[$ansvalue['ques_variable']] = $ansvalue['option_id'];
                        $k++;
                    }
                }

                $whereForm = array("formula_assessment" => $assessment_id, "formula_type" => 'complex');
                $coloumn = array('formula_health', 'formula', 'formula_type', 'formula_id');
                $formulaData = $IndAssFormulaTableObj->getAssformula($whereForm, $coloumn, "1");

//                echo '<pre>';
//                echo $assessment_id;
//                print_r($formulaData);
//                print_r($assessmentData);
                $formulaResult = array();
                $operator = array('+', '-', '*', '/');
                if (!empty($formulaData)) {
                    foreach ($formulaData as $formula) {
                        $formulaSplit = explode(' ', $formula['formula']);
                        if (!empty($formulaSplit)) {
                            $i = 0;
                            foreach ($formulaSplit as $ForMula) {

                                if (!in_array($ForMula, $operator)) {
                                    $formulaSplit[$i] = $assessmentData[$ForMula];
                                }
                                $i++;
                            }
                        }

                        $str = implode(' ', $formulaSplit);
//                        echo '<pre>';
//                        print_r($str);
//                        print_r($formulaSplit);
//                        die;
// die;

                        if (!empty($str)) {
                            $formulaResult[$formula['formula_health']]['formula_result'] = eval('return ' . $str . ';');
                        } else {
                            $formulaResult[$formula['formula_health']]['formula_result'] = 0;
                        }
                        $formulaResult[$formula['formula_health']]['formula_id'] = $formula['formula_id'];
                        $formulaResult[$formula['formula_health']]['name'] = $formula['type'] . " - " . $formula['col_name'];
                        $formulaResult[$formula['formula_health']]['id'] = $formula['health_id'];
                    }
                }

                if (!empty($formulaResult)) {

                    $ColArray = array();
                    $colsData = array();
                    foreach ($formulaResult as $key => $value) {
                        //get vlaue base on columan
                        $whereFormulaidArra = array('display_formula' => $value['formula_id']);
                        $colsDispFormula = array("columns");
                        $displayFormula = $IndAssFormulaTableObj->getdisplayFormula($whereFormulaidArra, $colsDispFormula);
                        $ColArray = explode(',', $displayFormula[0]['columns']);
                        $where = array('col_value' => $value['formula_result'], 'col_health' => $value['id']);
                        $colsel = array('row_no');

                        $Cols = $IndAssFormulaTableObj->getcols($where, $colsel);
                        if ($Cols) {
                            $colsData[$key] = $IndAssFormulaTableObj->getcols(array('row_no' => $Cols[0]['row_no']), array('*'), "1", $ColArray);
                        }
                    }
                }
            } else {
                //send error if assesment id not found
                $this->getResponse()->setStatusCode(404);
                return;
            }
        } else {
            //send error if assesment id not found
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => "Nonmember Assessment",
                    'CusConPlug' => $CusConPlug, // custome plugin
                    'response' => $response,
                    'complex' => $complex,
                    'assID' => $assID,
                    'part_id' => $part_id,
                    'cat_id' => $assessmentname[0]['ass_category'],
                    'Category' => $Category,
                    'AssessmentName' => $assesmentName,
                    'formulaResult' => $formulaResult,
                    'rowData' => $colsData
        ));
    }

    function scorequestionlistAction() {

        $Category = $this->_getCategory();
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $selfObj = $this->getServiceLocator()->get("AssessmentNonmember");
        $IndAssFormulaTableObj = $this->getServiceLocator()->get("AssesFormulaNonmember");
        $part_id = base64_decode($this->params()->fromQuery('id'));
        $Count = $selfObj->getAssessmentPart(array('id' => $part_id), array('assessment_id', 'email', 'id'));
        $assessment_id = isset($Count[0]['assessment_id']) ? $Count[0]['assessment_id'] : NULL;
        if (!empty($assessment_id)) {
            //get assessment name 
            $whereAss = array("ass_id" => $assessment_id);
            $assessmentname = $selfObj->getAssessment($whereAss);

            $formulaDataval = $IndAssFormulaTableObj->getAssformula(array("formula_assessment" => $assessment_id), array(), "0", "formula_type");
            if (count($formulaDataval) > 0) {
                foreach ($formulaDataval as $formvalue) {
                    $formula_type = $formvalue['formula_type'];
                    if ($formula_type == "complex") {
                        $complex = 1;
                    }
                    if ($formula_type == "response") {
                        $response = 1;
                    }
                }
            } else {
                return $this->redirect()->toUrl('/nonmember-assessment');
            }
//  

            if ($response == 0 && $complex == 1) {
                $resId = base64_encode($part_id);
                $link = "/nonmember-assessment-score?id=$resId";
                return $this->redirect()->toUrl($link);
            }
            if ($response == 0 && $complex == 0) {
                $resId = base64_encode($part_id);
                $link = "/nonmember-assessment";
                return $this->redirect()->toUrl($link);
            }

            if (count($assessmentname) > 0) {
                $assesmentName = $assessmentname[0]['ass_name']; //assesment name  
                $assesmentDesc = $assessmentname[0]['assessment_desc']; //assesment description  
                $assesmentQuestDesc = $assessmentname[0]['assessment_que_desc']; //assesment question description 
                $assesmentImage = $assessmentname[0]['assessment_image']; //assesment image
                //get partihipation details
                $wherePart = array("id" => $part_id, "assessment_id" => $assessment_id, "status" => 1);
                $partishipData = $selfObj->getAssessmentPart($wherePart, array(), "creation_date DESC");
                if (count($partishipData) > 0) {
                    $assID = $partishipData[0]['assessment_id'];
                    $part_id = $partishipData[0]['id'];
                } else {
                    //send error if partishipation  not found
                    $this->getResponse()->setStatusCode(404);
                    return;
                }


                $whereForm = array("formula_assessment" => $assessment_id, "formula_type" => 'response');
                $coloumn = array('formula_health', 'response_question', 'formula_type', 'formula_id');
                $formulaData = $IndAssFormulaTableObj->getAssformula($whereForm, $coloumn, "1");
                $displayFormula = $optAnsData = array();
                if (!empty($formulaData)) {
                    $i = 0;
                    foreach ($formulaData as $fdvalue) {
                        $assQuestion = $selfObj->getassessmentques(array('ques_id' => $fdvalue['response_question'], 'assessment_id' => $assessment_id), array('ques_question'));
                        $formulaData[$i]['question'] = $assQuestion[0]['ques_question'];
                        $whereOptAns = array('nonmember_id' => $part_id, 'assessment_id' => $assessment_id);
                        $coloum = array('option_id', 'ques_variable');
                        $optAnsData[$fdvalue['formula_health']] = $selfObj->getPartQuAns($whereOptAns, $coloum, "1");
                        $ColArray = array();


                        $healthCols[$fdvalue['formula_health']] = $IndAssFormulaTableObj->gethm(array('assessment_id' => $assessment_id, 'type' => $formulaData[0]['type']), array('health_id', 'col_name'));

                        if (!empty($healthCols[$fdvalue['formula_health']])) {
                            foreach ($healthCols[$fdvalue['formula_health']] as $cols) {
                                $ColArray[$fdvalue['formula_health']][] = $cols['health_id'];
                            }
                        }

                        $rows = $IndAssFormulaTableObj->getcols(array('col_health' => $fdvalue['formula_health'], 'col_value' => $optAnsData[$fdvalue['formula_health']][0]['option_id']), array('row_no'));
                        if (count(isset($rows)) > 0) {
                            $rnumbe = (isset($rows[0]['row_no']) ? $rows[0]['row_no'] : '' );
                            $optAnsData[$fdvalue['formula_health']]['result'] = $IndAssFormulaTableObj->getcols(array('row_no' => $rnumbe), array('col_value'), '1', $ColArray[$fdvalue['formula_health']]);
                        }
                        $i++;
                    }
                }


//                
            } else {
                //send error if assesment id not found
                $this->getResponse()->setStatusCode(404);
                return;
            }
        } else {
            //send error if assesment id not found
            $this->getResponse()->setStatusCode(404);
            return;
        }


        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => "Nonmember Assessment",
                    'CusConPlug' => $CusConPlug, // custome plugin
                    'assesmentDesc' => $assesmentDesc,
                    'assesmentQuestDesc' => $assesmentQuestDesc,
                    'assesmentImage' => $assesmentImage,
                    'response' => $response,
                    'complex' => $complex,
                    'assID' => $assID,
                    'Category' => $Category,
                    'cat_id' => $assessmentname[0]['ass_category'],
                    'part_id' => $part_id,
                    'AssessmentName' => $assesmentName,
                    'formulaResult' => $formulaData,
                    'rowData' => $optAnsData
        ));
    }

    /*     * ****This action is used to get the Healthfinder.gov API data base on search string******* */

    public function knwbaseAction() {
        $res['Result'] = array();
        $srch_string = '';
        $postDataArr = array();
        $request = $this->getRequest();
        $newArr = array();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $srch_string = '';
            if ($postDataArr['who'] != 0) {
                $srch_string .='&who=' . $postDataArr['who'];
            }
            if ($postDataArr['gender']) {
                $srch_string .='&gender=' . $postDataArr['gender'];
            }

            if (isset($postDataArr['pregnant']) && $postDataArr['pregnant'] != 0) {
                $srch_string .='&pregnant=' . $postDataArr['pregnant'];
            }

            if ($postDataArr['age'] != 0) {
                $srch_string .='&age=' . $postDataArr['age'];
            }

            $url = $url = "http://healthfinder.gov/developer/MyHFSearch.json?api_key=ybaicuvtirhkrgwg" . $srch_string;
            $fileContents = file_get_contents($url);
            $res = json_decode($fileContents, true);

            if (!empty($res)) {
                foreach ($res as $key => $index) {
                    $newArr['MainHeading'] = $index['MyHFHeading'];
                    $i = 1;
                    foreach ($index['Topics'] as $topics) {
                        $newArr[$topics['MyHFCategoryHeading']][$i]['MyHFDescription'] = $topics['MyHFDescription'];
                        $newArr[$topics['MyHFCategoryHeading']][$i]['AccessibleVersion'] = $topics['AccessibleVersion'];
                        $newArr[$topics['MyHFCategoryHeading']][$i]['Title'] = $topics['Title'];

                        $i++;
                    }
                }
            }

            ksort($newArr);
        }
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'QuantextualHealth', 'res' => $newArr, 'post' => $srch_string, 'postData' => $postDataArr
        ));
    }

}
