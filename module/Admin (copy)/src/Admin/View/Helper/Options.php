<?php 
namespace Admin\View\Helper;

use Admin\Model\VariablesTable;
use Zend\View\Helper\AbstractHelper;
 
class Options extends AbstractHelper
{

    public function __invoke ($quesOj, $question_id=NULL) {
        
        $optionArr = array();
        if ($question_id) {
        	// $this->_quesObj = $this->getServiceLocator()->getServiceLocator()->get("QuesTable");
        	$columns = array("opt_id", "opt_option", "opt_value");
	        $whereArr = array("opt_status"=>1, "question_id"=>$question_id);
	        $optionArr = $quesOj->getAdminQuestionsOption($whereArr, $columns);
        }
        return $optionArr;
    }
}