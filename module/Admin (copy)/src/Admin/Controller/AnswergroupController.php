<?php

/*
 * Purpose:This controller is used to manage the answer group
 * @Copyright:Appstudioz
 * Created On : 14-09-15
 * Zend Framework (http://framework.zend.com/)
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

Use Zend\Mvc\Controller\AbstractActionController;
Use Zend\View\Model\ViewModel;
Use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class AnswergroupController extends AbstractActionController {

    protected $_layoutObj;
    protected $_sessionObj;

    public function init() {
        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');
        $this->_sessionObj = new Container('super_admin');
    }

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $form = new \Admin\Form\Questionnaire();
        $page = 1;
        $Questionnaires=array();
        $sendArr = array(
            'form' => $form,
            'page' => $page,
            'Questionnaires'=>$Questionnaires,
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

}
