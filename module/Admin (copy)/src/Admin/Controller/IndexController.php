<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ROOT CONTROLLER FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: ANKIT SHUKLA(fb/gshukla67).
 * CREATOR: 12/11/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use Admin\Form\Login;
use Admin\Form\ChangePassword;
use Admin\Form\LoginValidator;
use Admin\Form\ChangePasswordValidator;

class IndexController extends AbstractActionController {

    protected $_layoutObj;
    protected $_sessionObj;
    protected $_authservice;

    public function init() {

        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');

        $this->_sessionObj = new Container('super_admin');
    }
    
    

    /*     * *****
     * Action:- Use to login Admin Pannel.
     * @author: Ankit Shukla(fb/gshukla67).
     * Created Date: 07-12-2014.
     * *** */

    public function indexAction() {
        $this->init();
        if ($this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('dashboard');
        }

        $siteURL = $_SERVER['HTTP_HOST'];
        $siteArray = explode('.', $siteURL);
        if (strtolower($siteArray[0]) == 'www') {
            $siteDomain = $siteArray[1];
        } else {
            $siteDomain = $siteArray[0];
        }
       
        $userTable = $this->getServiceLocator()->get("UserTable");
        $DomainDetail = $userTable->getDomain(array('status'=>1,'subdomain' => $siteDomain), array('logo', 'id'), array(), array(), array(), '0');
        if (isset($DomainDetail[0])) {
            $this->_sessionObj->adminLogo = (!empty($DomainDetail[0]['logo'])) ? $DomainDetail[0]['logo'] : 'health_matrix_logo_prelogin.png';
        } else {
            return $this->redirect()->toUrl('http://quantextualhealth.com');
        }
        $form = new Login('login');
        $request = $this->getRequest();

        if ($request->isPost()) {
            // $user = new ();

            $postDataArr = $request->getPost()->toArray();
            if (isset($postDataArr['email']) && $postDataArr['email'] != "") {
                $formValidator = new LoginValidator();
                {
                    $form->setInputFilter($formValidator->getInputFilter());
                    $form->setData($request->getPost());
                }
                if ($form->isValid()) {
                    // $user->exchangeArray($form->getData()); 
                    $postDataArr = $request->getPost()->toArray();
                    // if (isset($postDataArr['remember'])) {
                    $encyptPass = md5($postDataArr['password']);
                    $this->getAuthService()->getAdapter()
                            ->setIdentity($postDataArr['email'])
                            ->setCredential($encyptPass);
                    $resultArr = $this->getAuthService()->authenticate();

                    if ($resultArr->isValid()) {

                        $columns = array("id", "admin_name", "admin_user_name");
                        $whereArr = array("admin_user_name" => $postDataArr['email'],'status'=>1,'subdomain' => $siteDomain);
                        $userArr = $userTable->getDomain($whereArr, $columns, array(), array(), array(), '0');

                        if (count($userArr) > 0) {
                            $this->_sessionObj->admin = $userArr[0];
                            $this->_sessionObj->islogin = '1';
                            // echo '<pre>'; print_r($container->admin); die; 
                            return $this->redirect()->toUrl('admin-dashboard');
                        } else {
                            $message = "The given credentials does not match, please try again.";
                            $this->flashMessenger()->addMessage(array('error' => $message));
                        }
                    } else {
                        $message = "The given credentials does not match, please try again.";
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                    // } else {
                    //     $message = "You must be checked the option remember me.";
                    //     $this->flashMessenger()->addMessage(array('error'=>$message));
                    // }
                }
            } else {
                if (isset($postDataArr['e_mail'])) {
                    $columns = array("id", "admin_name", "admin_user_name");
                    $whereArr = array("admin_user_name" => $postDataArr['e_mail'], "status" => 1);
                    $userArr = $userTable->getDomain($whereArr, $columns, array(), array(), array(), '0');
                    if (count($userArr) > 0) {
                        $newPwd = substr(md5(rand(100000, 9999999)), 0, 9);
                        $columns = array("admin_password" => md5($newPwd));
                        $rowAffect = $userTable->updateUser($whereArr, $columns);
                        // SENDING THIS PASSWORD TO MAIL.
                        $subject = SITE_NAME." : Forgot password";
                        $body = "Dear " . $userArr[0]['admin_user_name'] . ",<br /><br />";
                        $body .= "Your Temporary password is : <b>" . $newPwd . "</b> <br />";
                        $body .= "After login please change your passwrod first.<br /><br />";
                        $plugin = $this->PluginController();
                        $plugin->sendMailAdmin($postDataArr['e_mail'], $subject, $body);
                        $message = "Your password changed and sent on your mail.";
                        $this->flashMessenger()->addMessage(array('email_success' => $message));
                    } else {
                        $message = "The given email does not exist.";
                        $this->flashMessenger()->addMessage(array('email_error' => $message));
                    }
                }
            }
        }
        return ['form' => $form];
        //return new ViewModel();
    }

    /*     * ********
     * Action: Use to manage the dashboard pannel.
     * @author: Ankit Shukla(fb/gshukla67).
     * Created Date: 08-12-2014.
     * *** */

    public function dashboardAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $user_email = $this->_sessionObj->admin['admin_user_name'];
        $form = new ChangePassword('changepassword');
        $request = $this->getRequest();
        $colArr = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'));
        $userTable = $this->getServiceLocator()->get("UserTable");
        $DomainDetail = $userTable->getDomain(array('status' => 1), $colArr, array(), array(), array(), '1');
        $StuTable = $this->getServiceLocator()->get("StuTable");
        $Study = $StuTable->getAdminStudy(array('study_status' => 1, 'created_by' => $this->_sessionObj->admin['id']), $colArr);
        $MembersTable = $this->getServiceLocator()->get("MembersTable");
        $exp = '"' . $this->_sessionObj->admin['id'] . '":\\\[("[[:digit:]]*",)*"1"';             
        $whereMemberArr = array('us.Verified' => 1,new \Zend\Db\Sql\Predicate\Expression(" UserTypeId REGEXP '$exp'"));
        $Individuals=$MembersTable->getUserMembers($whereMemberArr,$colArr);
        
        
        $exp = '"' . $this->_sessionObj->admin['id'] . '":\\\[("[[:digit:]]*",)*"3"';  
        $whereResArr = array('us.Verified' => 1,new \Zend\Db\Sql\Predicate\Expression(" UserTypeId REGEXP '$exp'"));
        $Researchers=$MembersTable->getUserMembers($whereResArr,$colArr);
        
        
        $sendArr=array(
            'Domain' => isset($DomainDetail[0]['num']) ? $DomainDetail[0]['num'] : 0,
            'passId' => $this->_sessionObj->admin['id'],
            'Study' => isset($Study[0]['num']) ? $Study[0]['num'] : 0,
            'Individuals'=>isset($Individuals[0]['num']) ? $Individuals[0]['num'] : 0,
            'Researchers'=>isset($Researchers[0]['num']) ? $Researchers[0]['num'] : 0,
        );
        if ($this->_sessionObj->admin['id'] == 1) {
            $SelfAssTable = $this->getServiceLocator()->get("SelfAssTable");
            $Assessment = $SelfAssTable->getAdminAssessment(array('ass_status' => 1), $colArr);
            $sendArr['Assessment']=isset($Assessment[0]['num']) ? $Assessment[0]['num'] : 0;
        }
        if ($request->isPost()) {
            // $user = new ();
            $postDataArr = $request->getPost()->toArray();
            $formValidator = new ChangePasswordValidator();
            {
                $form->setInputFilter($formValidator->getInputFilter());
                $form->setData($request->getPost());
            }
            if ($form->isValid()) {
                $postDataArr = $request->getPost()->toArray();
                $userTable = $this->getServiceLocator()->get("UserTable");
                $columns = array("id", "admin_name", "admin_user_name", "creation_date");
                $whereArr = array("id" => $user_id, "admin_user_name" => $user_email, "admin_password" => md5($postDataArr['old_pwd']), "status" => 1);
                $userArr = $userTable->getDomain($whereArr, $columns,NULl,array(),array(),'0');
                 if (count($userArr) > 0) {
                    if ($postDataArr['new_pwd'] == $postDataArr['cnfm_pwd']) {
                        $whereArr = array("id" => $user_id, "admin_user_name" => $user_email, "admin_password" => md5($postDataArr['old_pwd']), "status" => 1);
                        $columns = array("admin_password" => md5($postDataArr['new_pwd']));
                        $rowAffect = $userTable->updateDomain($whereArr, $columns);
                        // SENDING THIS PASSWORD TO MAIL.
                        $subject = SITE_NAME." : Change password";
                        $body = "Dear " . $userArr[0]['admin_name'] . ",<br /><br />";
                        $body .= "Welcome to the Health Metrix.<br />";
                        $body .= "Your new password is : <b>" . $postDataArr['new_pwd'] . "</b><br />";
                        $body .= "Please do not share this mail to others.<br /><br />";
                        $plugin = $this->PluginController();
                        $plugin->sendMailAdmin($user_email, $subject, $body);
                        $message = "Congratulations! your password has been changed.";
                        $this->flashMessenger()->addMessage(array('cngpwd_suss' => $message));
                    } else {
                        $message = "New Password and Confirm Password must be same.";
                        $this->flashMessenger()->addMessage(array('cngpwd_err' => $message));
                    }
                } else {
                    $message = "Old password doesnot match! Please try again.";
                    $this->flashMessenger()->addMessage(array('cngpwd_err' => $message));
                }
                return $this->redirect()->toRoute('dashboard');
            }
        }

        // $this->layout()->form = $form;
        //return ['form' => $form]; 
        return new ViewModel($sendArr);
    }

    /*     * ********
     * Action: Use to signoff the super admin pannel.
     * @author: Ankit Shukla(fb/gshukla67).
     * Created Date: 09-12-2014.
     * *** */

    public function logoutAction() {
        $this->init();
        $this->_sessionObj->getManager()->getStorage()->clear('super_admin');
        return $this->redirect()->toRoute('admin');
    }

    private function getAuthService() {
        if (!$this->_authservice) {
            $this->_authservice = $this->getServiceLocator()->get('AdminAuth');
        }
        return $this->_authservice;
    }
    
    function uploadAction(){
        $phpExcel = new \PHPExcel();
        $path = BASE_PATH . "/".'test.xls';
        $sheetData = $phpExcel->load($path)->getActiveSheet()->toArray(null, true, true, true);
        $cond_id=NULL;
        $ConditionTableObj = $this->getServiceLocator()->get("ConditionTable");
        
        foreach($sheetData as $Cells){
            if($Cells['B']){
                $data=array(
                    'media_name'=>$Cells['A'],
                    'common_name'=>$Cells['A'],
                    'code_minrange'=>'',
                    'code_maxrange'=>'',
                    'description'=>$Cells['A'],
                    'status'=>1,
                    'creation_date'=>date('Y-m-d H:i:s')
                );
                $cond_id=$ConditionTableObj->insertCond($data);
            }else if($cond_id!=NULL){
                $data=array(
                    'condition_id'=>$cond_id,
                    'sub_media_name'=>$Cells['A'],
                    'sub_common_name'=>$Cells['A'],
                    'unique_code'=>'',
                    'sub_description'=>$Cells['A'],
                    'sub_status'=>1,
                    'creation_date'=>date('Y-m-d H:i:s')
                );
                $ConditionTableObj->insertSubCond($data);
            }
        }
        echo 'Done';
        die;
    }

}
