<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE Subscription PART OF ADMIN.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 14/05/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class SubscriptionController extends AbstractActionController {

    protected $_passDomObj;

    public function init() {
        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');
        $this->_sessionObj = new Container('super_admin');
        if ($this->_sessionObj->admin['id'] == 1) {
            return $this->redirect()->toRoute('admin');
        }
    }

    /*     * *****
     * Action:- Pass admin Subscription status to show .
     * @author: Hemant.
     * Created Date: 14-05-2015.
     * *** */

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable"); //CODE FOR SCROLL PAGINATION
        $columns = array('id', 'admin_user_name', 'panel_member', 'study_survey', 'subdomain', 'survey_que_per_mon');
        $PassDomain = $this->_passDomObj->getDomain(array('status' => 1), $columns, "0", array()); //CODE FOR SCROLL PAGINATION
        $colArr = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'));

        $StuTable = $this->getServiceLocator()->get("StuTable");
        $Study = $StuTable->getAdminStudy(array('created_by' => $this->_sessionObj->admin['id']), $colArr);

        $MembersTable = $this->getServiceLocator()->get("MembersTable");
        $exp = '(.*"' . $this->_sessionObj->admin['id'] . '".*)("\[.*"1".*\]")';
        $whereMemberArr = array('us.Verified' => 1, new \Zend\Db\Sql\Predicate\Expression(" UserTypeId REGEXP '$exp'"));
        $Individuals = $MembersTable->getUserMembers($whereMemberArr, $colArr);

        $QuestionTable = $this->getServiceLocator()->get("QuesTable");
        $startDate = strtotime(date('Y-m-01', strtotime(date('Y-m-d H:i:s'))));
        $endDate = strtotime(date('Y-m-t', strtotime(date('Y-m-d H:i:s'))));
        $whereArr = array(new \Zend\Db\Sql\Predicate\Expression("creation_date >= '$startDate)'"),
            new \Zend\Db\Sql\Predicate\Expression("creation_date <= '$endDate)'"),
            'created_by' => $this->_sessionObj->admin['id']);
        $StudyQueCount = $QuestionTable->getAdminQuestions($whereArr, array('num' => new \Zend\Db\Sql\Expression('COUNT(DISTINCT ques_variable)')), '0', array(), 1);

        $sendArr = new ViewModel();
        return $sendArr->setVariables(array(
                    'PassDomain' => $PassDomain[0],
                    'Individuals' => isset($Individuals[0]['num']) ? $Individuals[0]['num'] : 0,
                    'Study' => isset($Study[0]['num']) ? $Study[0]['num'] : 0,
                    'StudyQueCount' => isset($StudyQueCount[0]['num']) ? $StudyQueCount[0]['num'] : 0,
                    'session' => $this->_sessionObj->admin
        ));
    }

}
