<?php

/* * ***********************
 * PAGE: CONTROLLER USE TO MANAGE THE USER FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: ANKIT SHUKLA(fb/gshukla67).
 * CREATOR: 11/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use Admin\Form\SearchUser;
use Admin\Form\SearchUserValidator;

class UserController extends AbstractActionController {

    protected $_layoutObj;
    protected $_sessionObj;

    public function init() {

        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');

        $this->_sessionObj = new Container('super_admin');
//        if ($this->_sessionObj->admin['id'] != 1) {
//            return $this->redirect()->toRoute('admin');
//        }
    }

    /*     * *****
     * Action:- Use to list of user and searching on it.
     * @author: Ankit Shukla(fb/gshukla67).
     * Created Date: 11-12-2014.
     * *** */

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $user_id = $this->_sessionObj->admin['id'];
        $form = new SearchUser('user');
        $request = $this->getRequest();

        if ($request->isPost()) {
            // $user = (); 

            $formValidator = new SearchUserValidator();
            {
                $form->setInputFilter($formValidator->getInputFilter());
                $form->setData($request->getPost());
            }

            if ($form->isValid()) {
                // $user->exchangeArray($form->getData()); 
                $postDataArr = $request->getPost()->toArray();

                // echo '<pre>'; print_r($postDataArr); die;
            }
        }
        $MembersTable = $this->getServiceLocator()->get("MembersTable");
        $columns = array("UserID", "UserName", 'verified_by_admin');
        $exp = '"' . $user_id . '":\\\[("[[:digit:]]*",)*"1"';
        $whereArr = array(new \Zend\Db\Sql\Predicate\Expression(" UserTypeId REGEXP '$exp'"));
        $whereArr['Verified'] = 1;

        $membersArr = $MembersTable->getUserMembers($whereArr, $columns, 0, array(), 0, 1);
        // GETTING COUNTRY LIST.
        $CountryTable = $this->getServiceLocator()->get("CountryTable");
        $columns = array("country_id", "country_name", "status");
        $whereArr = array("status" => 1);
        $countryArr = $CountryTable->getCountry($whereArr, $columns);
        // GETTING STATE LIST.
        $StateTable = $this->getServiceLocator()->get("StateTable");
        $columns = array("state_id", "state_name", "country_id", "status");
        $whereArr = array("country_id" => 1, "status" => 1);
        $stateArr = $StateTable->getState($whereArr, $columns);
        //echo '<pre>'; print_r($membersArr); die;

        $sendArr = array(
            'form' => $form,
            'members' => $membersArr,
            'session' => $this->_sessionObj->admin,
            "country" => $countryArr,
            "state" => $stateArr,
        );
        return new ViewModel($sendArr);
    }

    public function usersAction() {
        
        $this->init();
        $searchData = $_GET;
        // $searchData = $this->getResponse();
        $MembersTable = $this->getServiceLocator()->get("MembersTable");
        $columns = array("UserID", "UserTypeId", "FirstName", "MiddleName", "LastName", "UserName", "Gender", "Image", "Email", "CreationDate", 'verified_by_admin','preferably_device');
        $user_id = $this->_sessionObj->admin['id'];

        $exp = '"' . $user_id . '":\\\[("[[:digit:]]*",)*"1"';
        $whereArr = array(new \Zend\Db\Sql\Predicate\Expression(" UserTypeId REGEXP '$exp'"));
        $whereArr['Verified'] = 1;
        if ($searchData['Gender'] != "0")
            $whereArr["Gender"] = $searchData['Gender'];
        if ($searchData['State'])
            $whereArr["cont.state_id"] = $searchData['State'];
        if ($searchData['Country'])
            $whereArr["cont.country_id"] = $searchData['Country'];
        $searchContentArr = array(
            "FirstName" => $searchData['FirstName'],
            "LastName" => $searchData['LastName'],
            "UserName" => $searchData['UserName'],
            "cont.city" => $searchData['City'],
            "Email" => $searchData['Email'],
        );
        $MembersArr = $MembersTable->getUserMembers($whereArr, $columns, "1", $searchContentArr, $searchData['offset'], $searchData['limit']);
        // echo '<pre>';print_r($MembersArr); die;

        $sendArr = array(
            'members' => $MembersArr
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($sendArr)->setTerminal(true);
        return $viewModel;
    }

    function changeStatusAction() {
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $MembersTable = $this->getServiceLocator()->get("MembersTable");

        if (($request->isXmlHttpRequest())) {
            $UserData = $MembersTable->getUserMembers(array('UserID' => $postDataArr['id']), array('*'));
            $status = ($postDataArr['verified_by_admin'] == 0) ? 1 : 0;
            if ($MembersTable->updateUser(array('UserID' => $postDataArr['id']), array('verified_by_admin' => $status))) {
                $this->_sendUserStatusEmail($UserData[0], $status);
                echo json_encode(array('true', $postDataArr['id'], $status));
            } else {
                echo json_encode(array('false'));
            }
        }
        die;
    }

    /*     * *****
     * Action:- Send Email to Pass Domain User at time of status change (Activated/De-Activated) .
     * @author: Hemant.
     * Created Date: 13-05-2015.
     * *** */

    function _sendUserStatusEmail($data = NULL, $status = NULL) {
        if (!empty($data)) {
            $plugin = $this->PluginController();
            $Status = ($status == 0) ? 'De-Activated' : 'Activated';
            
            $message = "<b>Dear " . $data['UserName'] . "</b>,<br><br>";
            $message .= "&nbsp;&nbsp;Your Account has been " . $Status . " by admin.<br> ";
            $message .= ($status == 1) ? "&nbsp;&nbsp;Click here to <a href='" . SITE_URL . "'> Login</a>.<br> " : "&nbsp;&nbsp;Please contact admin for further detail.<br>";
            
            $subject = SITE_NAME." : (" . $data['UserName'] . ") Your Account has been " . $Status;
            $plugin->sendMailAdmin($data['Email'], $subject, $message);
        }
    }

}
