<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE CONDITION PART OF ADMIN.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 2/3/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class PassController extends AbstractActionController {

    protected $_passDomObj;

    public function init() {
        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');
        $this->_sessionObj = new Container('super_admin');
    }

    /*     * *****
     * Action:- Check on ajax call for type and email existance.
     * @author: Hemant.
     * Created Date: 26-04-2015.
     * *** */

    public function checkAvailableAction() {
        $this->init();
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable");
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();

        if ($this->_sessionObj->admin['id'] != 1) {
            return $this->redirect()->toRoute('admin');
        }
        switch ($postDataArr['type']) {
            case 'name':
                $whereCountArr = array('subdomain' => $postDataArr['subdomain'], 'status' => 1);
                if (!empty($postDataArr['PassId'])) {
                    $domCountArr = $this->_passDomObj->checkAvilableDomain($whereCountArr, $postDataArr['PassId']);
                } else {
                    $domCountArr = $this->_passDomObj->checkAvilableDomain($whereCountArr);
                }
                echo ($domCountArr) ? 'true' : 'false';
                die;
                break;
            case 'email':
                $whereCountArr = array('admin_user_name' => $postDataArr['admin_user_name'], 'status' => 1);
                if (!empty($postDataArr['PassId'])) {
                    $domCountArr = $this->_passDomObj->checkAvilableDomain($whereCountArr, $postDataArr['PassId']);
                } else {
                    $domCountArr = $this->_passDomObj->checkAvilableDomain($whereCountArr);
                }
                echo ($domCountArr) ? 'true' : 'false';
                die;
                break;
        }die;
    }

    /*     * *****
     * Action:- Change status on ajax call for pass admin.
     * @author: Hemant.
     * Created Date: 28-04-2015.
     * *** */

    function changeStatusAction() {
        $request = $this->getRequest();

        if ($this->_sessionObj->admin['id'] != 1) {
            return $this->redirect()->toRoute('admin');
        }

        $postDataArr = $request->getPost()->toArray();
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable");
        if (($request->isXmlHttpRequest())) {

            $ColArr = array('id', 'subdomain', 'admin_user_name', 'admin_name');
            $Pass = $this->_passDomObj->getDomain(array('id' => $postDataArr['id']), $ColArr, '0');
            $status = ($postDataArr['status'] == 0) ? 1 : 0;
            $this->_sendPassStatusEmail($Pass[0], $status);

            if ($this->_passDomObj->updateDomain(array('id' => $postDataArr['id']), array('status' => $status))) {
                echo json_encode(array('true', $postDataArr['id'], $status));
            } else {
                echo json_encode(array('false'));
            }
        }
        die;
    }

    /*     * *****
     * Action:- Pass admin listing with lazy loading paging.
     * @author: Hemant.
     * Created Date: 26-04-2015.
     * *** */

    public function indexAction() {
        $this->init();

        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        if ($this->_sessionObj->admin['id'] != 1) {
            return $this->redirect()->toRoute('admin');
        }
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable"); //CODE FOR SCROLL PAGINATION
        $columns = array('id', 'admin_user_name', 'creation_date', 'contact_number', 'subdomain', 'status');
        $where = array();
        $limit = array(0, 10);
        $page = 1;
        if (base64_decode($this->params()->fromQuery('page')) > 0) {
            $page = base64_decode($this->params()->fromQuery('page'));
            $limit = array($limit[1] * $page, $limit[1]);
            $page++;
        }
        $Pass = $this->_passDomObj->getDomain($where, $columns, "0", array(), $limit); //CODE FOR SCROLL PAGINATION
        $request = $this->getRequest();
        $sendArr = new ViewModel();

        ($request->isXmlHttpRequest()) ? $sendArr->setTerminal(true) : '';
        return $sendArr->setVariables(array('Pass' => $Pass, 'page' => $page, 'session' => $this->_sessionObj->admin));
    }

    /*     * *****
     * Action:- study detail popup.
     * @author: Hemant.
     * Created Date: 28-04-2015.
     * *** */

    function detailAction() {

        $request = $this->getRequest();
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable");
        if (($request->isXmlHttpRequest())) {
            $columns = array('*');
            $where = array('id' => $sid);
            $Pass = $this->_passDomObj->getDomain($where, $columns, "0"); //CODE FOR SCROLL PAGINATION
            echo json_encode($Pass[0]);
        }die;
    }

    /*     * *****
     * Action:- Fund an account.
     * @author: Rabban.
     * Created Date: 01-05-2015.
     * *** */

//    function fundaccAction() {
//        $request = $this->getRequest();
//        $this->_passDomObj = $this->getServiceLocator()->get("UserTable");
//        if (($request->isXmlHttpRequest())) {
//            $ip = $_SERVER['REMOTE_ADDR'];
//            $postDataArr = $request->getPost()->toArray();
//            $amount = $postDataArr['amount'];
//            $security_card = $postDataArr['security_card'];
//            $credit_card_token = $postDataArr['credit_card_token'];
//            $DataArr = array('customer' => 'Healtmetrix', 'account_identifier' => 'Rabbanhealth', 'amount' => $amount,
//                'client_ip' => $ip, 'cc_token' => $credit_card_token, 'security_code' => $security_card);
//
//            $url = "https://sandbox.tangocard.com/raas/v1/orders";
//            $fields_string = json_encode($DataArr);
//            //open connection
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, $url);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
//            curl_setopt($ch, CURLOPT_POST, true);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
//            curl_setopt($ch, CURLOPT_HEADER, false);
//            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                'Content-Type: application/json',
//                'Content-Length: ' . strlen($fields_string)
//            ));
//            $result = curl_exec($ch);
//            $encodeRes = json_decode($result, true);
//            curl_close($ch);
//            if ($encodeRes['success'] == false) {
//                echo $encodeRes['error_message'];
//            } else {
//                echo $encodeRes['error_message'];
//            }
//        }die;
//    }

    function deleteCardAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable");
        $Pass = $this->_passDomObj->getDomain(array('id' => $this->_sessionObj->admin['id']), array('tango_customer'), '0', array(), array(), '0');
        $DataCard = $this->_passDomObj->getDomainCard(array('pass_id' => $this->_sessionObj->admin['id']), array('cc_token', 'id'));

        if (!empty($Pass) && !empty($DataCard)) {
            $DataArr = array('customer' => $Pass[0]['tango_customer'], 'account_identifier' => $Pass[0]['tango_customer'], 'cc_token' => $DataCard[0]['cc_token']);

            $Auth = "Basic " . base64_encode(TANGOCARD_PLATEFORM . ":" . TANGOCARD_KEY);

            $url = TANGOCARD_URL . "cc_unregister";
            $fields_string = json_encode($DataArr);
            //open connection
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($fields_string),
                'Authorization: ' . $Auth
            ));
            $result = curl_exec($ch);
            $encodeRes = json_decode($result, true);

            curl_close($ch);
            if ($encodeRes['success'] == true) {
                if ($this->_passDomObj->deleteDomainCard(array('id' => $DataCard[0]['id']))) {
                    $message = 'Your card successfully un-registeration with tango account.';
                    $this->flashMessenger()->addMessage(array('success' => $message));
                    return $this->redirect()->toUrl('admin-reward');
                }
            } else {
                $message = 'Your card un-registeration process with tango account failed.';
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toUrl('admin-reward');
            }
        } else {
            $message = 'Your card un-registeration process with tango account failed.';
            $this->flashMessenger()->addMessage(array('error' => $message));
            return $this->redirect()->toUrl('admin-reward');
        }
    }

    /*     * *****
     * Action:- Manage reward section.
     * @author: Rabban.
     * Created Date: 01-05-2015.
     * *** */

    function rewardAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $request = $this->getRequest();
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable");
        $Data = $this->_passDomObj->getDomain(array('id' => $this->_sessionObj->admin['id']), array('admin_name', 'subdomain', 'admin_user_name', 'company_name', 'tango_email', 'tango_customer'), '0', array(), array(), '0');
        $DataCard = $this->_passDomObj->getDomainCard(array('pass_id' => $this->_sessionObj->admin['id']), array('cc_token', 'card_no'));

        $encodeRes = array();
        $Auth = "Basic " . base64_encode(TANGOCARD_PLATEFORM . ":" . TANGOCARD_KEY);

        $request = $this->getRequest();
        if ($request->isPost() && !empty($DataCard)) {
            $ip = $_SERVER['REMOTE_ADDR'];
            $postDataArr = $request->getPost()->toArray();

            $DataArr = array('customer' => $Data[0]['tango_customer'], 'account_identifier' => $Data[0]['tango_customer'], 'amount' => (int) $postDataArr['amount'],
                'client_ip' => $ip, 'cc_token' => $DataCard[0]['cc_token'], 'security_code' => $postDataArr['security_card']);

            $url = TANGOCARD_URL . "cc_fund";
            $fields_string = json_encode($DataArr);
            //open connection
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($fields_string),
                'Authorization: ' . $Auth
            ));
            $result = curl_exec($ch);
            $encodeRes = json_decode($result, true);

            curl_close($ch);
            if ($encodeRes['success'] == true) {

                //Email
                $plugin = $this->PluginController();
                $message = "<b>Dear " . $Data[0]['admin_name'] . "</b>,<br><br>";
                $message .= "&nbsp;&nbsp;Your <b>Tango Card</b> account with Customer name (" . $Data[0]['tango_customer'] . ") has been credit by $" . $encodeRes['amount'] . " .<br> ";

                $message .="<br>&nbsp;&nbsp; <b>Transaction Details</b>";
                $message .="<br>&nbsp;&nbsp; <b>Card Number</b> : " . $DataCard[0]['card_no'];
                $message .="<br>&nbsp;&nbsp; <b>Amount</b> : " . $encodeRes['amount'];
                $message .="<br>&nbsp;&nbsp; <b>Fund ID</b> : " . $encodeRes['fund_id'];

                $subject = SITE_NAME . " : Fund Added To Tango Account " . $Status;
                $plugin->sendMailAdmin(array($Data[0]['admin_user_name'], $Data[0]['tango_email']), $subject, $message);
                //Email

                $message = 'Your fund successfully added to tango account.';
                $this->flashMessenger()->addMessage(array('success' => $message));
                return $this->redirect()->toUrl('admin-reward');
            } else {
                $message = $encodeRes['denial_message'];
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toUrl('admin-reward');
            }
        }

        if (!empty($Data)) {


            $url = TANGOCARD_URL . "accounts/" . $Data[0]['tango_customer'] . "/" . $Data[0]['tango_customer'];
            //open connection
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: ' . $Auth
            ));
            $result = curl_exec($ch);
            $encodeRes = json_decode($result, true);

            curl_close($ch);
        }

//        echo '<pre>';
//        print_r($encodeRes);
//        die;
        $sendArr = array(
            'CardData' => !empty($DataCard[0]) ? $DataCard[0] : array(),
            'Data' => !empty($Data[0]) ? $Data[0] : array(),
            'TangoData' => $encodeRes,
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- Manage reward section.
     * @author: Rabban.
     * Created Date: 1-05-2015.
     * *** */

    function registerccAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $request = $this->getRequest();
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable");
        if ($request->isPost()) {

            $ip = $_SERVER['REMOTE_ADDR'];
            $postDataArr = $request->getPost()->toArray();

            $ColArr = array('tango_customer');
            $Pass = $this->_passDomObj->getDomain(array('id' => $this->_sessionObj->admin['id']), $ColArr, '0', array(), array(), '0');

            $DataArr = array('customer' => $Pass[0]['tango_customer'], 'account_identifier' => $Pass[0]['tango_customer'],
                'client_ip' => $ip, 'credit_card' => (Object) array('number' => $postDataArr['cc_no'],
                    'security_code' => $postDataArr['cc_security_card'], 'expiration' => $postDataArr['cc_expire_date'],
                    'billing_address' => array('f_name' => $postDataArr['f_name'], 'l_name' => $postDataArr['l_name'],
                        'address' => $postDataArr['address'], 'city' => $postDataArr['city'], 'state' => $postDataArr['state'],
                        'zip' => $postDataArr['zip'], 'country' => $postDataArr['country'], 'email' => $postDataArr['email'])));

            $Auth = "Basic " . base64_encode(TANGOCARD_PLATEFORM . ":" . TANGOCARD_KEY);

            $url = TANGOCARD_URL . "cc_register";
            $fields_string = json_encode($DataArr);
            //open connection
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($fields_string),
                'Authorization: ' . $Auth
            ));
            $result = curl_exec($ch);
            $encodeRes = json_decode($result, true);

            curl_close($ch);
            if ($encodeRes['success'] == true) {
                $data = array('pass_id' => $this->_sessionObj->admin['id'], 'card_no' => $postDataArr['cc_no'], 'cc_token' => $encodeRes['cc_token'], 'creation_date' => time());
                if ($this->_passDomObj->insertDomainCard($data)) {
                    return $this->redirect()->toUrl('admin-reward');
                }
            } else {
                $message = 'Your card register process with tango account failed.';
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
        $Data = $this->_passDomObj->getDomainCard(array('pass_id' => $this->_sessionObj->admin['id']));

        if (!empty($Data[0])) {
            return $this->redirect()->toUrl('admin-reward');
        }

//        echo '<pre>';
//        print_r($Data[0]);die;
        $sendArr = array(
        );
        return new ViewModel($sendArr);
    }

    /*     * *****
     * Action:- Add/Edit Study with client and server side validation.
     * @author: Hemant.
     * Created Date: 26-04-2015.
     * *** */

    function addAction() {
        $this->init();

        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        if ($this->_sessionObj->admin['id'] != 1) {
            return $this->redirect()->toRoute('admin');
        }

        $form = new \Admin\Form\Pass();
        $request = $this->getRequest();
        $sid = base64_decode($this->params()->fromQuery('sid'));
        $Pass = array();
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable");
        //check Pass edit to prefill edit form
        if ($sid) {
            $form->setAttribute('id', 'EditPassForm');
            $form->setAttribute('action', 'admin-passDomain-add?sid=' . base64_encode($sid));
            $ColArr = array('id', 'subdomain', 'company_name', 'tango_email', 'tango_customer', 'admin_user_name', 'admin_password', 'admin_name', 'logo', 'contact_number', 'panel_member', 'study_survey', 'survey_que_per_mon');
            $Pass = $this->_passDomObj->getDomain(array('id' => $sid), $ColArr, '0');
            foreach ($Pass[0] as $key => $index) {
                $form->get($key)->setValue($index);
            }
        }

        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $PassValidator = new \Admin\Form\PassValidator();
            $form->setInputFilter($PassValidator->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $postDataArr;
                //function to save file
                if (!empty($_FILES['logo']['name'])) {
                    $path = BASE_PATH . "/img/";
                    $ext = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
                    $file_name = md5(microtime());
                    $des = $path . $file_name . '.' . $ext;
                    $allowed = array('jpg', 'jpeg');
                    $src = $_FILES['logo']["tmp_name"];
                    if (in_array($ext, $allowed)) {
                        $Resize = $this->ResizeController();
                        //New Code to Resize
                        if (move_uploaded_file($src, $des)) {
                            list($width, $height, $type, $attr) = getimagesize($des);
                            // creating medium pictures
                            $smallImgSizw = $Resize->getSquireImageSize($width, $height, 200);

                            $Resize->load($des);
                            $Resize->resize('400', '100');
                            $Resize->save($des);

                            $data['logo'] = $file_name . '.' . $ext;

                            if (!empty($Pass[0]['logo'])) {
                                @unlink($path . $Pass[0]['logo']);
                            }
                        }
                        //New Code to Resize
                    }
                }
                //function to save file
                unset($data['submit']);
                if ($sid) {
                    if ($this->_passDomObj->updateDomain(array('id' => $sid), $data)) {

                        return $this->redirect()->toUrl('admin-passDomain');
                    } else {
                        $message = 'Error occurred while saving please try again.';
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                } else {
                    $data['creation_date'] = date('Y-m-d');
                    $password = $data['admin_password'];
                    $data['admin_password'] = md5($data['admin_password']);
                    $sid = $this->_passDomObj->insertDomain($data);
                    if ($sid) {

                        //Curl Request for account registration
                        $Auth = "Basic " . base64_encode(TANGOCARD_PLATEFORM . ":" . TANGOCARD_KEY);
                        $url = TANGOCARD_URL . "accounts";
                        $fields = array(
                            'customer' => $data['tango_customer'],
                            'email' => $data['tango_email'],
                            'identifier' => $data['tango_customer']
                        );
                        $fields_string = json_encode($fields);
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                        curl_setopt($ch, CURLOPT_HEADER, false);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($fields_string),
                            'Authorization: ' . $Auth
                        ));
                        $result = curl_exec($ch);
                        $encodeRes = json_decode($result, true);

                        if ($encodeRes['success'] != false) {
                            $dataTango = array('tango_acc_status' => 1);
                            $this->_passDomObj->updateDomain(array('id' => $sid), $dataTango);
                        }
                        //Curl Request for account registration

                        $this->_sendMail($data, $password);
                        return $this->redirect()->toUrl('admin-passDomain');
                    } else {
                        $message = 'Error occurred while saving please try again.';
                        $this->flashMessenger()->addMessage(array('error' => $message));
                    }
                }
            } else {
                $message = 'Please Provide valid data.';

                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }

        $sendArr = array(
            'form' => $form,
            'Pass' => $Pass,
            'sid' => $sid,
            'session' => $this->_sessionObj->admin,
        );
        return new ViewModel($sendArr);
    }

    function checkTangoAvailableAction() {
        $this->init();
        $request = $this->getRequest();
        $data = $request->getPost()->toArray();
        if (($request->isXmlHttpRequest())) {
            $Auth = "Basic " . base64_encode(TANGOCARD_PLATEFORM . ":" . TANGOCARD_KEY);
            $url = TANGOCARD_URL . "accounts/" . $data['tango_customer'] . "/" . $data['tango_customer'];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_POST, false);
//          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: ' . $Auth
            ));
            $result = curl_exec($ch);
            $encodeRes = json_decode($result, true);

            if ($encodeRes['success'] == 1) {
                echo 'false';
            } else {
                echo 'true';
            }
        }die;
    }

    /*     * *****
     * Action:- Send Email to Pass Domain User at time of creation.
     * @author: Hemant.
     * Created Date: 3-05-2015.
     * *** */

    function _sendMail($data = NULL, $password = NULL) {

        if (!empty($data) && !empty($password)) {
            $plugin = $this->PluginController();
            $message = "<b>Dear " . $data['admin_name'] . "</b>,<br><br>";
            $message .= "&nbsp;&nbsp;Your requested domian (" . $data['subdomain'] . ") has been successfully created by admin.<br> &nbsp;&nbsp;Please change your password once you logged in<br><br>";
            $message .= "&nbsp;&nbsp;<b>Domain detail are mention below</b><br><br>";
            $message .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Domain Url:</b> <a href='" . $data['subdomain'] . ".quantextualhealth.com'>" . $data['subdomain'] . ".quantextualhealth.com</a> <br>";
            $message .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Domain Username:</b>" . $data['admin_user_name'] . "<br>";
            $message .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Domain Password:</b>" . $password . "<br>";
            $subject = SITE_NAME . " : Domain (" . $data['subdomain'] . ") created";
            $plugin->sendMailAdmin($data['admin_user_name'], $subject, $message);
        }
    }

    /*     * *****
     * Action:- Send Email to Pass Domain User at time of status change (Activated/De-Activated) .
     * @author: Hemant.
     * Created Date: 13-05-2015.
     * *** */

    function _sendPassStatusEmail($data = NULL, $status = NULL) {
        if (!empty($data)) {
            $plugin = $this->PluginController();
            $Status = ($status == 0) ? 'De-Activated' : 'Activated';
            $message = "<b>Dear " . $data['admin_name'] . "</b>,<br><br>";
            $message .= "&nbsp;&nbsp;Your domian (" . $data['subdomain'] . ") has been " . $Status . " by admin.<br> ";

            $message .= ($status == 1) ? "&nbsp;&nbsp;Click here to access doamin <a href='" . $data['subdomain'] . ".quantextualhealth.com'> " . $data['subdomain'] . ".quantextualhealth.com</a>.<br> " : "&nbsp;&nbsp;Please contact admin for further detail.<br>";
            $message .='<br><br> <b>Thanks</b> <br> <b>Admin Health Metrix</b>';

            $subject = SITE_NAME . " : Domain (" . $data['subdomain'] . ") has been " . $Status;
            $plugin->sendMailAdmin($data['admin_user_name'], $subject, $message);
        }
    }

}
