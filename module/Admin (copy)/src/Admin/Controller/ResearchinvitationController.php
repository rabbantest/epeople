<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE Invitation PART OF ADMIN.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 14/05/2015.
 * UPDATED: 15/05/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class ResearchinvitationController extends AbstractActionController {

    protected $_passDomObj;

    public function init() {

        $this->_layoutObj = $this->layout();
        $this->_layoutObj->setTemplate('layouts/layout');
        $this->_sessionObj = new Container('super_admin');
        if ($this->_sessionObj->admin['id'] == 1) {
            return $this->redirect()->toRoute('admin');
        }
    }

    /*     * *****
     * Action:- This controller is used to manage researcher invitation .
     * @author: Rabban.
     * Created Date: 03-06-2015.
     * *** */

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $plugin = $this->PluginController();
            if (!empty($postDataArr['email_address']) && !empty($postDataArr['researcher_name'])) {
                $ResearcherInviteTableObj = $this->getServiceLocator()->get("ResearcherInviteTable");
                $whereDelete = array('researcher_email' => $postDataArr['email_address'], 'pass_id' => $this->_sessionObj->admin['id']);
                $ResearcherInviteTableObj->removeResearcherInvitationHash($whereDelete);
                $code = md5(time());
                $dataArr = array('pass_id' => $this->_sessionObj->admin['id'], 'hash_code' => $code,
                    'creation_date' => date("Y-m-d"), 'researcher_email' => $postDataArr['email_address'], 'researcher_name' => $postDataArr['researcher_name']);
                $ResearcherInviteTableObj->saveResearcherInvitation($dataArr);

                $uname = $postDataArr['researcher_name'];
                $domain = "http://" . $_SERVER['HTTP_HOST'];
                $rearcherlogin = "http://" . $_SERVER['HTTP_HOST'] . "/researcher-login";
                $url = "http://" . $_SERVER['HTTP_HOST'] . "/register/$code";
                if ($postDataArr['email_registered']) {
                    $loginmessage = "Dear $uname,<br>";
                    $loginmessage .= "You have received a researcher invitation from $domain <br>";
                    $loginmessage .= "If you wish to join please go through the below link.";
                    $loginmessage .= "<br><br><a href='$rearcherlogin' target='_blank'>$rearcherlogin</a>";
                    $loginmessage .= "<br><br>If the link deos not work,please copy the entire link and past it into your browser";
                    $loginmessage .="<br><br>Thank you<br>";
                   
                } else {
                    $message = "Dear $uname,<br>";
                    $message .= "You have received a researcher invitation from $domain <br>";
                    $message .= "If you want to register yourself as researcher.Please go through the below link.";
                    $message .= "<br><br><a href='$url' target='_blank'>$url</a>";
                    $message .= "<br><br>If the link deos not work,please copy the entire link and past it into your browser";
                    $message .="<br><br>Thank you<br>";
                    
                }
                if ($postDataArr['email_registered']) {
                    $body = $loginmessage;
                } else {
                    $body = $message;
                }
                $subject = SITE_NAME." : Researcher invitation";
                if ($plugin->sendMailAdmin($postDataArr['email_address'], $subject, $body)) {
                    $message = 'Invitation email has been send successfully.';
                    $this->flashMessenger()->addMessage(array('success' => $message));
                } else {
                    $message = 'Error occurred while sending email please try again.';
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            } else {
                $message = 'Please enter email address and researcher name.';
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
        $sendArr = new ViewModel();
        return $sendArr->setVariables(array(
                    'session' => $this->_sessionObj->admin
        ));
    }

    /*     * *****
     * Action:- This controller is used to manage researcher invitation .
     * @author: Rabban.
     * Created Date: 03-06-2015.
     * *** */

    public function manageinvitationsAction() {
        $this->init();
        if (!$this->_sessionObj->islogin) {
            return $this->redirect()->toRoute('admin');
        }
        $ResearcherInviteTableObj = $this->getServiceLocator()->get("ResearcherInviteTable");
        $researcherData = $ResearcherInviteTableObj->getResearcherInvitations(array('pass_id' => $this->_sessionObj->admin['id']));
        $sendArr = new ViewModel();
        return $sendArr->setVariables(array(
                    'researcherData' => $researcherData
        ));
    }

    /*     * *****
     * Action:- Function to check ajax call that email address already exists or not.
     * @author: Hemant.
     * Created Date: 15-05-2015.
     * *** */

    function alreadyinviteAction() {
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        if (($request->isXmlHttpRequest())) {
            $MembersTable = $this->getServiceLocator()->get("MembersTable");
            $passId = $postDataArr['id'];
            $exp = '"' . $passId . '":\\\[("[[:digit:]]*",)*"3"';
            $whereArr = array(new \Zend\Db\Sql\Predicate\Expression(" UserTypeId REGEXP '$exp'"));
            $whereArr['Email'] = $postDataArr['email_address'];
            $membersCount = $MembersTable->getUserMembers($whereArr, array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));
            if ($membersCount[0]['num'] == 0) {
                echo 'true';
            } else {
                echo 'false';
            }
            die;
        }
    }

    function checkmailAction() {
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        if (($request->isXmlHttpRequest())) {
            $MembersTable = $this->getServiceLocator()->get("MembersTable");
            $whereArr = array('Email' => $postDataArr['email']);
            $membersCount = $MembersTable->getUserMembers($whereArr, array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));
            if ($membersCount[0]['num'] == 0) {
                echo 0;
            } else {
                echo 1;
            }
            die;
        }
    }

}
