<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE CONDITION PART OF ADMIN.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 2/3/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use Admin\Form\Study;
use Admin\Form\StudyValidator;
use Admin\Form\Demographic;

class CronsController extends AbstractActionController {

    protected $_studiesObj;
    protected $_queAnsObj;
    var $objPHPExcel = array();

    /*     * *****
     * Action:-Function for cron to get study data.
     * @author: Hemant Chuphal.
     * Created Date: 14-4-2015.
     * *** */

    public function studyPassiveDataAction() {
        $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
        $StudyEligibleTableObj = $this->getServiceLocator()->get("StudyEligibleTable");
        $curDate = date('Y-m-d');
        ini_set('max_execution_time', 2000);
        $StudyData = $this->_studiesObj->getAdminStudy(array('activexls_status' => 0, /* 'study_type' => 2, */ new \Zend\Db\Sql\Predicate\Expression("end_date < '$curDate'")), array('study_id', 'study_title'), '0', array(), array());

        if (!empty($StudyData)) {
            $j = 1;
            foreach ($StudyData as $Study) {
                echo date('H:i:s') . " Start.\r\n";
                $page = 0;
                $this->_saveExcel($Study, $page, $j, $StudyEligibleTableObj);
                echo date('H:i:s') . " End\n";
            }
        }
        die('done');
    }

    function _saveExcel($study = NULL, $page = NULL, $j, $StudyEligibleTableObj) {
        $set = 1;
        $this->objPHPExcel[$set] = new \PHPExcel();

        $limit = array(0, 300);
        $cols = array('Email', 'varName' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(var_name)'),
            'optionVal' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(opt_option)')
        );
        if ($page > 0) {
            $limit = array($limit[1] * $page, $limit[1]);
        }
        $page++;
        $data = $StudyEligibleTableObj->getAdminViewData(array('study_id' => $study['study_id']), $cols, '0', array(), $limit);

        if (!empty($data)) {



            for ($k = 0; $k <= 120000; $k++) {
                $this->objPHPExcel[$set]->setActiveSheetIndex(0);
                foreach ($data as $Data) {
                    $i = 0;
                    if ($j == 1) {
                        $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, 'Email');
                    }

                    $j = $j + 1;
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $Data['Email']);
                    $newArray = array_combine(explode(',', $Data['varName']), explode(',', $Data['optionVal']));
                    if (!empty($newArray)) {

                        foreach ($newArray as $var => $opt) {
                            $i++;
                            if ($j == 2) {
                                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $var);
                            }

                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $opt);
                        }
                    }
                }

                if (($k % 8000 == 0 && $k != 0) || $k == 120000) {
                    echo $k;
                    sleep(2);
                    $objWriter[$set] = new \PHPExcel_Writer_Excel2007($this->objPHPExcel[$set]);
                    $dir = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Active/";
                    if (!file_exists($dir . $study['study_id'])) {
                        mkdir($dir . $study['study_id'], 777);
                        chmod($dir . $study['study_id'], 0777);
                    }
                    $this->objPHPExcel[$set]->getActiveSheet()->setTitle($study['study_title']);
                    $objWriter[$set]->save($dir . $study['study_id'] . '/' . $study['study_title'] . $set . '.xlsx');
                    unset($objWriter[$set]);
                    unset($this->objPHPExcel[$set]);
                    $j=1;
                    $set++;
                    $this->objPHPExcel[$set] = new \PHPExcel();
                }
            }

            // Rename sheet

            $this->_saveExcel($study, $page, $j, $StudyEligibleTableObj);
        } else {
            //$this->_studiesObj = $this->getServiceLocator()->get("StuTable");
            // $this->_studiesObj->updateAdminStudy(array('study_id' => $study['study_id']), array('activexls_status' => 1));
        }
//        $objWriter = new \PHPExcel_Writer_Excel2007($this->objPHPExcel);
//        $dir = $_SERVER['DOCUMENT_ROOT'] . "/StudyDump/Active/";
//        if (!file_exists($dir . $study['study_id'])) {
//            mkdir($dir . $study['study_id'], 777);
//            chmod($dir . $study['study_id'], 0777);
//        }
//        $objWriter->save($dir . $study['study_id'] . '/' . $study['study_title'] . '.xlsx');
    }

    /*     * *****
     * Action:-Function for cron.
     * @author: Hemant Chuphal.
     * Created Date: 14-4-2015.
     * *** */

    public
            function indexAction() {
        $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
        $this->_queAnsObj = $this->getServiceLocator()->get("StudyEligibleTable");
        $curDate = date('Y-m-d');
        $StudyData = $this->_studiesObj->getAdminStudy(array('publish_status' => 1), array('study_id', 'screening', 'study_title', 'created_by'), '0', array(), array(), $curDate);
        $Study = array_column($StudyData, 'study_id');
        $StudyPass = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'created_by'));
        $StudyType = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'screening'));
        $StudyName = array_combine(array_column($StudyData, 'study_id'), array_column($StudyData, 'study_title'));
        $user = array();
        $ids = array();

        if ($Study) {
            $StudyVarSet = $this->_studiesObj->getAdminStudyVarSet(array('set_study' => $Study), array('set_study', 'json', 'total_combination'));
            if (!empty($StudyVarSet)) {
                foreach ($StudyVarSet as $SetData) {
                    $JsonArray = isset($SetData['json']) ? json_decode($SetData['json'], true) : array();
                    $conditionsArray = !empty($JsonArray) ? $this->_getCondition($JsonArray, $SetData['total_combination']) : array();
                    if (!empty($conditionsArray['Variables'])) {
                        foreach ($conditionsArray['Variables'] as $key => $Variables) {

                            $Cron = $this->_queAnsObj->getAdminStudyBucketCronSetting(array(
                                'study_id' => $SetData['set_study'],
                                'bucket' => $key,
                                'sent' => 0
                                    ), array(
                                'number', 'id'
                            ));

                            if (!empty($Cron)) {
                                $limit = 0;
                                foreach ($Cron as $number) {

                                    $existingUser = $this->_queAnsObj->getAdminStudyBucketInvitation(array('study_id' => $SetData['set_study'], 'bucket' => $key), array('user_id'));
                                    $existingUsers = array_combine(array_column($existingUser, 'user_id'), array_column($existingUser, 'user_id'));

                                    $user[$SetData['set_study']][$key][] = $this->_getEligibleUsersList($Variables, isset($conditionsArray['Common']) ? $conditionsArray['Common'] : array(), array($limit, $number['number']), $existingUsers, $StudyPass[$SetData['set_study']]);
                                    $limit = $limit + $number['number'];
                                    $ids[] = $number['id'];
                                }
                            }
                        }
                    } else {
                        $Cron = $this->_queAnsObj->getAdminStudyBucketCronSetting(array(
                            'study_id' => $SetData['set_study'],
                            'bucket' => 0,
                            'sent' => 0
                                ), array(
                            'number', 'id'
                        ));

                        if (!empty($Cron)) {
                            $limit = 0;
                            foreach ($Cron as $number) {

                                $existingUser = $this->_queAnsObj->getAdminStudyBucketInvitation(array('study_id' => $SetData['set_study'], 'bucket' => 0), array('user_id'));
                                $existingUsers = array_combine(array_column($existingUser, 'user_id'), array_column($existingUser, 'user_id'));

                                $user[$SetData['set_study']][0][] = $this->_getEligibleUsersList(NULL, isset($conditionsArray['Common']) ? $conditionsArray['Common'] : array(), array($limit, $number['number']), $existingUsers, $StudyPass[$SetData['set_study']]);
                                $limit = $limit + $number['number'];
                                $ids[] = $number['id'];
                            }
                        }
                    }
                }
            }
        }
        if (!empty($user)) {
            $data = array();

            foreach ($user as $key => $indexMain) {
                $data['study_id'] = $key;
                foreach ($indexMain as $bucket => $index) {
                    $data['bucket'] = $bucket;
                    $data['type'] = ($StudyType[$key]) ? 0 : 1;
                    $data['eligible_for_main'] = ($StudyType[$key]) ? 0 : 1;
                    $this->_saveAndSendInvitation($data, $this->_queAnsObj, $index, $StudyName[$data['study_id']]);
                }
            }
        }
        if (!empty($ids)) {
            $this->_queAnsObj->updateAdminStudyBucketCronSetting(array('id' => $ids), array('sent' => 1));
        }
        exit('done');
    }

    /*     * *****
     * Action:- Send Email AND Save Invitation
     * @author: Hemant Chuphal.
     * Created Date: 13-04-2015.
     * *** */

    function _saveAndSendInvitation($data = NULL, $Obj = NULL, $index = NULL, $studyName) {
        $plugin = $this->PluginController();

        foreach ($index as $bucket => $users) {
            if (!empty($users)) {
                foreach ($users as $id => $email) {
                    $data['user_id'] = $id;
                    $data['creation_date'] = time();

                    $Obj->insertAdminStudyBucketInvitation($data);
                }
                $message = "Dear User,<br>";
                $message .= "You are invited to attempty Study name ($studyName).";
                $subject = "Study Invitation";
                $plugin->sendMailAdmin($users, $subject, $message);
            }
        }
    }

    /*     * *****
     * Action:- Get Eligible Users for study
     * @author: Hemant Chuphal.
     * Created Date: 13-04-2015.
     * *** */

    function _getEligibleUsersList($VarData, $CommonData, $limit, $existingUsers, $pass) {
        $this->_queAnsObj = $this->getServiceLocator()->get("StudyEligibleTable");
        $where = array();
        $cols = array('user_id', 'Email');
        $VarResultData = array();

        if (!empty($VarData)) {
            $i = 0;
            if (is_array($VarData)) {
                foreach ($VarData as $Var) {
                    $Var = json_decode($Var, TRUE);
                    $where[$i] = array('ques_variable' => $Var[0], 'option_id' => $Var[1]);
                    $i++;
                }
            } else {
                $Var = json_decode($VarData, TRUE);
                $where[$i] = array('ques_variable' => $Var[0], 'option_id' => $Var[1]);
                $i++;
            }

            $VarResultData = $this->_queAnsObj->getAdminVarStudyEligibleUser($where, $cols, '0', $CommonData, $limit, '0', '1', $existingUsers, $pass);
        } else {
            $VarResultData = $this->_queAnsObj->getAdminVarStudyEligibleUser(NULL, $cols, '0', $CommonData, $limit, '0', '1', $existingUsers, $pass);
        }

        return array_combine(array_column($VarResultData, 'user_id'), array_column($VarResultData, 'Email'));
    }

    /*     * *****
     * Action:- Get condition set for study
     * @author: Hemant Chuphal.
     * Created Date: 13-04-2015.
     * *** */

    function _getCondition($JsonArray, $totalComb = NULL) {
        $result = array();
        if ($totalComb < 16) {
            foreach ($JsonArray as $key => $Data) {
                if (isset($Data[2])) {
                    $var_id = $Data[2]['var_id'];
                    $resultData = array();
                    array_walk($Data[2]['option_id'], function($value) use (& $var_id, & $resultData) {
                        $resultData[] = json_encode(array($var_id, $value));
                    });
                    $result['Variables'][$var_id] = $resultData;
                    unset($JsonArray[$key]);
                }
            }
        } else {
            $resultData = array();
            foreach ($JsonArray as $key => $Data) {
                if (isset($Data[2])) {
                    $var_id = $Data[2]['var_id'];

                    array_walk($Data[2]['option_id'], function($value) use (& $var_id, & $resultData) {
                        $resultData = array_merge($resultData, array(json_encode(array($var_id, $value))));
                    });


                    unset($JsonArray[$key]);
                }
            }
            $result['Variables'][$var_id] = $resultData;
        }


        $result['Variables'] = !empty($result['Variables']) ? array_values($result['Variables']) : array();
        $result['Variables'] = ($totalComb < 16) ? $this->_combinations($result['Variables']) : $result['Variables'];
        $result['Common'] = $JsonArray;

        return $result;
    }

    /*     * *****
     * Action:- get combination based on varrable for study
     * @author: Hemant Chuphal.
     * Created Date: 10-04-2015.
     * *** */

    function _combinations($arrays, $i = 0) {
        if (!isset($arrays[$i])) {
            return array();
        }
        if ($i == count($arrays) - 1) {
            return $arrays[$i];
        }
        $tmp = $this->_combinations($arrays, $i + 1);

        $result = array();
        foreach ($arrays[$i] as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t) ?
                        array_merge(array($v), $t) :
                        array($v, $t);
            }
        }

        return $result;
    }

}
