<?php

/* * ***********************
 * PAGE: USE TO MANAGE SUPER ADMIN CONDITION PANNEL.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 23/2/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Delete;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Db\Adapter\Driver\DriverInterface;

class HealthmetrixTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_super_health_metrix';
    public $colTable = 'hm_super_health_metrix_cols';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAdminHealthMetrix($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array()) {

        $orderBy = array("health_id ASC");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'healthMatrix' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $options = array("col_value", 'row_no');
                $select->join(array('healthMatrixCol' => $this->colTable), "healthMatrix.health_id = healthMatrixCol.col_health", $options, 'LEFT');
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                if (!empty($searchContent)) {
                    foreach ($searchContent AS $fields => $data) {
                        if (!empty($data)) {
                            $likeWhr->addPredicate(
                                    new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                            );
                            $select->where($likeWhr);
                        }
                    }
                }
                $orderBy = "health_id ASC";
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function saveMultipleRecord($dataCols = NULL) {
        $arrayDataType = array();
        try {
            $sql = new Sql($this->getAdapter());
            if (!empty($dataCols)) {
                foreach ($dataCols as $DataVal) {
                    $colData = $DataVal;
                    unset($colData['value']);

                    //check for unique record
                    $select = $sql->select()->from(array(
                        'healthMatrix' => $this->table
                    ));
                    $select->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'), 'health_id'));
                    $select->where(array('col_name' => $colData['col_name'], 'assessment_id' => $colData['assessment_id'], 'type' => $colData['type']));
                    $statement = $sql->prepareStatementForSqlObject($select);
                    $Check = $this->resultSetPrototype->initialize($statement->execute())->toArray();

                    //check for unique record
                    if (!($Check["0"]['num'])) {
                        $insert = $sql->insert($this->table);
                        $insert->values($colData);
                        $selectString = $sql->getSqlStringForSqlObject($insert);
                        $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
                        $lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                    } else {
                        $lastId = $Check["0"]['health_id'];
                    }

                    if (!empty($DataVal['value']) && $lastId) {
                        foreach ($DataVal['value'] as $key => $ValueData) {

                            $select = $sql->select()->from(array(
                                'healthMatrixCol' => $this->colTable
                            ));
                            $select->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));
                            $select->where(array('row_no' => $key, 'col_value' => $ValueData['col_value'], 'col_health' => $lastId));
                            $statement = $sql->prepareStatementForSqlObject($select);
                            $Check = $this->resultSetPrototype->initialize($statement->execute())->toArray();


                            $colVal = $ValueData['col_value'];

                            $arrayDataType[$lastId] = (filter_var($colVal, FILTER_VALIDATE_INT) || filter_var($colVal, FILTER_VALIDATE_INT) === 0 ) ? 'int' : 'other';
                            if (!($Check["0"]['num'])) {
                                $DataColVal = array(
                                    "col_health" => $lastId,
                                    "col_value" => $ValueData['col_value'],
                                    "row_no" => $key,
                                    "creation_date" => time()
                                );
                                $insert = $sql->insert($this->colTable);
                                $insert->values($DataColVal);
                                $selectString = $sql->getSqlStringForSqlObject($insert);
                                $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
                            }
                        }
                    }
                }
            }

            if (!empty($arrayDataType)) {
                foreach ($arrayDataType as $key => $index) {
                    $update = $sql->update();
                    $update->table($this->table);
                    $update->set(array('datatype' => $index));
                    $update->where(array('health_id' => $key));
                    $statement = $sql->prepareStatementForSqlObject($update);
                    $result = $statement->execute();
                }
            }

            return $result;
        } catch (\Exception $e) {
            
            foreach ($dataCols as $data) {
                if(!empty($data['assessment_id']) && !empty($data['type'])) {
                    $WhereDeleteExcel = array('assessment_id' => $data['assessment_id'], 'type' => $data['type']);
                    $this->deleteMetrixData($WhereDeleteExcel);
                    break;
                }
            }
            return false;
        }
    }

    public function deleteMetrixData($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->table);

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
