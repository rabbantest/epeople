<?php

/* * ***********************
 * PAGE: USE TO MANAGE SUPER ADMIN CONDITION PANNEL.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: ANKIT SHUKLA(fb/gshukla67).
 * CREATOR: 26/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Delete;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Db\Adapter\Driver\DriverInterface;

class StudyEligibleTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_selfassess_ques_answer';
    public $partTable = 'hm_selfassess_participation';
    public $settingTable = 'hm_super_study_bucket_cron_setting';
    public $bucketInvitationTable = 'hm_super_study_bucket_invitation';
    public $mainFilterTable = 'hm_super_study_main_que_filter';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function getAdminViewData($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array(), $group = 1,$notIn=0) {
        $orderBy = "ques_variable ASC";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'SEU' => $this->table
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
             if ($notIn) {
                $select->where->NotequalTo('SEU.bucket', '-1');
                $select->where->NotequalTo('SEU.bucket', '-2');
            }

            $options = array('Email','UserID');
            $select->join(array('User' => 'hm_users'), new \Zend\Db\Sql\Expression("User.UserID = SEU.user_id"), $options, 'LEFT');
            $options = array();
            $select->join(array('Var' => 'hm_super_ass_study_questions_variable'), new \Zend\Db\Sql\Expression("Var.var_id = SEU.ques_variable"), $options, 'LEFT');
            $options = array();
            $select->join(array('Opt' => 'hm_super_ass_study_questions_option'), new \Zend\Db\Sql\Expression("Opt.question_id = SEU.quest_id AND Opt.opt_value = SEU.option_id"), $options, 'LEFT');


            if ($lookingfor) {
                
            }
            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            if ($group) {
                $select->group(array('user_id'));
            }
            $select->order($orderBy);
//          $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()) . $limit);
//die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertAdminStudyMainFilter($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->mainFilterTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateAdminStudyMainFilter($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->mainFilterTable);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudyMainFilter($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array()) {
        $orderBy = "id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'mainfilter' => $this->mainFilterTable
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }
            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertAdminStudyBucketInvitation($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->bucketInvitationTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudyBucketCronSetting($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array()) {
        $orderBy = "id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'inviSet' => $this->settingTable
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }
            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateAdminStudyBucketCronSetting($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->settingTable);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertAdminStudyBucketCronSetting($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->settingTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudyBucketInvitation($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array(), $group = 0) {
        $orderBy = "id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'invi' => $this->bucketInvitationTable
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->join(array('part' => $this->partTable), "invi.study_id = part.study_id AND invi.user_id = part.UserID", array(), 'LEFT');
            }
            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }
            if ($group) {
                $select->group(array('invi.is_accepted'));
            }

            if (count($where) > 0) {
                $select->where($where);
            }
            $select->order($orderBy);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function checkAdminStudyBucketInvitation($where = array(), $notIn = NULL) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'invi' => $this->bucketInvitationTable
            ));
            $select->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));
            if (count($where) > 0) {
                $select->where($where);
            }
            if (!empty($notIn)) {
                $select->where->NotequalTo('invi.id', $notIn);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            $status = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return ($status[0]['num'] == 0) ? true : false;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getSegregateStudyEligibleUser($users = NULL) {
        try {
            $result = array();
            if (!empty($users)) {
                $sql = new Sql($this->getAdapter());
                $selectpart = $sql->select()->from(array(
                    'part' => $this->partTable
                ));
                //echo date('Y-m-d H:i:s','1427785409501'/1000);die;
                $query_date = date('Y-m-d H:i:s');
                // echo round(microtime(true)*1000);die;
                $startDate = round(strtotime(date('Y-m-01', strtotime($query_date))) * 1000);
                $endDate = round(strtotime(date('Y-m-t', strtotime($query_date))) * 1000);
                $selectpart->columns(array('UserID'));
                $selectpart->where(new \Zend\Db\Sql\Predicate\Expression("part_startdate >= '$startDate)'"));
                $selectpart->where(new \Zend\Db\Sql\Predicate\Expression("part_startdate <= '$endDate)'"));
                $selectpart->where(array('UserID' => $users));
                $having = new \Zend\Db\Sql\Having();
                $having->expression('count(*) >= ?', 'SELECT receive_study FROM hm_users_privacy_settings WHERE hm_users_privacy_settings.UserID = part.UserID )');
                $selectpart->group(array('part.UserID'))->having($having);
                //echo $selectpart->getSqlString();
                $statement = $sql->prepareStatementForSqlObject($selectpart);
                $userNew = $this->resultSetPrototype->initialize($statement->execute())->toArray();
                $thisMonthUser = array_diff($users, array_column($userNew, 'UserID'));
                // $nextMonthUser = array_diff($users, $thisMonthUser);
                //$result['next_month'] = $nextMonthUser;
                $result = $thisMonthUser;
            }
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAdminVarStudyEligibleUser($where = array(), $columns = array(), $lookingfor = 1, $CommonData = array(), $limit = array(), $count = NULL, $curMonth = NULL, $existingUsers = array(), $passId = NULL) {
        $orderBy = array(new \Zend\Db\Sql\Expression("RAND()"));
        try {

            $query_date = date('Y-m-d H:i:s');
            // echo round(microtime(true)*1000);die;
            $startDate = round(strtotime(date('Y-m-01', strtotime($query_date))) * 1000);
            $endDate = round(strtotime(date('Y-m-t', strtotime($query_date))) * 1000);

            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'User' => 'hm_users'
            ));

            $whereUser = array();
            $whereSetting = '';

            if ($passId) {
                $exp = '"' . $passId . '":\\\[("[[:digit:]]*",)*"1"';             
                $whereUser = array(new \Zend\Db\Sql\Predicate\Expression(" User.UserTypeId REGEXP '$exp'"));
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (!empty($existingUsers)) {
                $select->where->notIn('User.UserID', $existingUsers);
            }

            $whereSetting = 'Setting.survey_request=1';
            $whereContactInfo = '';
            $whereCondition = 0;
            $wheresymptom = 0;
            $wheretreatment = 0;
            $having = array();
            if (count($CommonData) > 0) {
                foreach ($CommonData as $data) {

                    if (isset($data[1])) {
                        if (isset($data[1]['country']) && !empty($data[1]['country'])) {
                            $whereContactInfo = $whereContactInfo . ' AND Contact.country_id=' . $data[1]['country'];
                        }
                        if (isset($data[1]['state']) && !empty($data[1]['state'])) {
                            $whereContactInfo = $whereContactInfo . ' AND Contact.state_id=' . $data[1]['state'];
                        }
                        if (isset($data[1]['counties']) && !empty($data[1]['counties'])) {
                            $whereContactInfo = $whereContactInfo . ' AND Contact.county_id=' . $data[1]['counties'];
                        }
                        if (isset($data[1]['gender']) && !empty($data[1]['gender'])) {
                            $whereUser['User.Gender'] = $data[1]['gender'];
                        }
                        if (isset($data[1]['age_from']) && !empty($data[1]['age_from']) && isset($data[1]['age_to']) && !empty($data[1]['age_to'])) {
                            $select->where->between('User.Age', $data[1]['age_from'], $data[1]['age_to']);
                        }
                    }

                    if (isset($data[3])) {
                        if (isset($data[3]['condition_id']) && !empty($data[3]['condition_id'])) {
                            $whereCondition = 1;
                            foreach ($data[3]['condition_id'] as $Condition) {
                                $Data = explode(':', $Condition);
                                if ($Data[1]) {
                                    $having[] = new \Zend\Db\Sql\Predicate\Expression("FIND_IN_SET($Data[0],GROUP_CONCAT(Conditions.condition_id))");
                                } else {
                                    $having[] = new \Zend\Db\Sql\Predicate\Expression("NOT FIND_IN_SET($Data[0],GROUP_CONCAT(Conditions.condition_id))");
                                }
                            }
                        }
                    }
                    if (isset($data[4])) {
                        if (isset($data[4]['symptom_id']) && !empty($data[4]['symptom_id'])) {
                            $wheresymptom = 1;
                            foreach ($data[4]['symptom_id'] as $Symtoms) {
                                $Data = explode(':', $Symtoms);
                                if ($Data[1]) {
                                    $having[] = new \Zend\Db\Sql\Predicate\Expression("FIND_IN_SET($Data[0],GROUP_CONCAT(Symptom.symtoms_id))");
                                } else {
                                    $having[] = new \Zend\Db\Sql\Predicate\Expression("NOT FIND_IN_SET($Data[0],GROUP_CONCAT(Symptom.symtoms_id))");
                                }
                            }
                        }
                    }

                    if (isset($data[5])) {
                        if (isset($data[5]['treatment_id']) && !empty($data[5]['treatment_id'])) {
                            $wheretreatment = 1;
                            foreach ($data[5]['treatment_id'] as $Treatment) {
                                $Data = explode(':', $Treatment);
                                if ($Data[1]) {
                                    $having[] = new \Zend\Db\Sql\Predicate\Expression("FIND_IN_SET($Data[0],GROUP_CONCAT(Treatment.treatment_id))");
                                } else {
                                    $having[] = new \Zend\Db\Sql\Predicate\Expression("NOT FIND_IN_SET($Data[0],GROUP_CONCAT(Treatment.treatment_id))");
                                }
                            }
                        }
                    }
                }
            }

            if ($lookingfor) {
                $options = array();
                $select->join(array('Setting' => 'hm_users_privacy_settings'), new \Zend\Db\Sql\Expression("User.UserID = Setting.UserID AND " . $whereSetting), $options, 'RIGHT');

                if ($whereContactInfo != '') {
                    $select->join(array('Contact' => 'hm_contacts_info'), new \Zend\Db\Sql\Expression("User.UserID = Contact.user_id" . $whereContactInfo), $options, 'RIGHT');
                }
                if ($whereCondition) {
                    $select->join(array('Conditions' => 'hm_users_conditions'), new \Zend\Db\Sql\Expression("User.UserID = Conditions.user_id"), $options, 'RIGHT');
                }
                if ($wheresymptom) {
                    $select->join(array('Symptom' => 'hm_users_symtoms'), new \Zend\Db\Sql\Expression("User.UserID = Symptom.user_id"), $options, 'RIGHT');
                }

                if ($wheretreatment) {
                    $select->join(array('Treatment' => 'hm_users_treatments'), new \Zend\Db\Sql\Expression("User.UserID = Treatment.user_id"), $options, 'RIGHT');
                }
            }



            if (count($where) > 0) {
//                $options = array('ques_variable');
//                $select->join(array('Answer' => 'hm_selfassess_ques_answer'), new \Zend\Db\Sql\Expression("User.UserID = Answer.user_id"), $options, 'LEFT');
                $i = 1;
                foreach ($where as $Where) {
                    $selectSubQuery = $sql->select()->from(array(
                        'queSubQuery' . $i => $this->table
                    ));
                    $selectSubQuery->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));
                    $selectSubQuery->where(array('queSubQuery' . $i . '.ques_variable' => $Where['ques_variable']));
                    $selectSubQuery->order(array("queSubQuery" . $i . ".id desc"));
                    $subQuery = "(SELECT option_id from hm_selfassess_ques_answer AS UserEle" . $i . " where UserEle" . $i . ".user_id=User.UserID AND UserEle" . $i . ".ques_variable=" . $Where['ques_variable'] . " ORDER BY id DESC LIMIT 0,1)";
                    $selectSubQuery->where
                            ->equalTo($subQuery, $Where['option_id']);
                   
                    $query = \str_replace(array("`"), array(""), $selectSubQuery->getSqlString($this->getAdapter()->getPlatform()).' LIMIT 0,1');

                    $whereUser[] = '(' . $query . ')';
                    $i++;
                }
            }

//            echo '<pre>';
//            print_r($query);die;
            if (!empty($whereUser)) {
                $select->where($whereUser);
            }



            if (count($limit) > 1) {
                $limit = " LIMIT $limit[1] OFFSET $limit[0]";
            } else {
                $limit = " ";
            }
            if ($curMonth) {
                $subQuery = "(SELECT count(*) FROM hm_selfassess_participation WHERE hm_selfassess_participation.UserID = que.user_id AND hm_selfassess_participation.part_startdate >= " . $startDate . " AND hm_selfassess_participation.part_startdate <= " . $endDate . ")";
                $select->where->expression('survay_per_month > ?', $subQuery);
            }
            $select->group(array('User.UserID'))->having($having);
             $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()) . $limit);

            $statement = $this->adapter->query($query);
            if ($count == 1) {
                $user = $this->resultSetPrototype->initialize($statement->execute())->count();
            } else {
                $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            }
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
