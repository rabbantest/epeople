<?php

/* * ***********************
 * PAGE: USE TO MANAGE SUPER ADMIN CONDITION PANNEL.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT CHUPHAL.
 * CREATOR: 02/06/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Admin\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Delete;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Db\Adapter\Driver\DriverInterface;

class PassiveTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_selfassess_ques_answer';
    public $partTable = 'hm_selfassess_participation';
    public $bucketInvitationTable = 'hm_super_study_bucket_invitation';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function getAdminViewData($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array(), $group = 1, $Dates,$notIn=0) {
        $orderBy = "ques_variable ASC";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'SEU' => $this->table
            ));
            
            if ($notIn) {
                $select->where->NotequalTo('SEU.bucket', '-1');
                $select->where->NotequalTo('SEU.bucket', '-2');
            }

            $day = 60 * 60 * 24;

            $end_date = $Dates['end_date'];
            $start_date = $Dates['start_date'];
            if (is_array($Dates['source'])) {
                $source = $Dates['source'];
            } else {
                $source = ($Dates['source'] == 'manual') ? '' : $Dates['source'];
            }
            
            if (isset($source['Height'])) {
                $source = ($Dates['Height'] == 'manual') ? '' : $Dates['Height'];
                $columns['Height'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(ROUND(MIN(Height),2),"-",ROUND(MAX(Height),2)) from hm_height_records where UserID=SEU.user_id AND HeightRecordDate>"'
                        . $start_date . '" AND HeightRecordDate<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            } else if (!is_array($Dates['source'])) {
                
                $columns['Height'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(ROUND(MIN(Height),2),"-",ROUND(MAX(Height),2)) from hm_height_records where UserID=SEU.user_id AND HeightRecordDate>"'
                        . $start_date . '" AND HeightRecordDate<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($source['Weight'])) {
                $source = ($Dates['Weight'] == 'manual') ? '' : $Dates['Weight'];
                $columns['Weight'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(ROUND(MIN(Weight)*0.00220462,2),"-",ROUND(MAX(Weight)*0.00220462,2)) from hm_weight_records where UserID=SEU.user_id AND WeightRecordDate>"'
                        . $start_date . '" AND WeightRecordDate<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            } else if (!is_array($Dates['source'])) {
                $columns['Weight'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(ROUND(MIN(Weight)*0.00220462,2),"-",ROUND(MAX(Weight)*0.00220462,2)) from hm_weight_records where UserID=SEU.user_id AND WeightRecordDate>"'
                        . $start_date . '" AND WeightRecordDate<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($source['PhysicalData'])) {
                $source = ($Dates['PhysicalData'] == 'manual') ? '' : $Dates['PhysicalData'];
                
                $columns['PhysicalData'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(ROUND(SUM(NumberOfSteps))'
                        . ',"-",SUM(Duration)'
                        . ',"-",ROUND(SUM(Distance),2)'
                        . ',"-",ROUND(SUM(CaloriesBurned),3)'
                        . ') from hm_physical_acitivity where UserID=SEU.user_id AND ExerciseRecorddate>"'
                        . $start_date . '" AND ExerciseRecorddate<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            } else if (!is_array($Dates['source'])) {
                $columns['PhysicalData'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(ROUND(SUM(NumberOfSteps))'
                        . ',"-",SUM(Duration)'
                        . ',"-",ROUND(SUM(Distance),2)'
                        . ',"-",ROUND(SUM(CaloriesBurned),3)'
                        . ') from hm_physical_acitivity where UserID=SEU.user_id AND ExerciseRecorddate>"'
                        . $start_date . '" AND ExerciseRecorddate<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($source['Cholesterol'])) {
                $source = ($Dates['Cholesterol'] == 'manual') ? '' : $Dates['Cholesterol'];
                
                $columns['Cholesterol'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(ROUND(MIN(LDL),2)'
                        . ',"-",ROUND(MAX(LDL),2)'
                        . ',"-",ROUND(MIN(HDL),2)'
                        . ',"-",ROUND(MAX(HDL),2)'
                        . ',"-",ROUND(MIN(Triglycerides),2)'
                        . ',"-",ROUND(MAX(Triglycerides),2)'
                        . ')'
                        . ' from hm_cholesterol_records where UserID=SEU.user_id AND CholesterolRecordDate>"'
                        . $start_date . '" AND CholesterolRecordDate<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            } else if (!is_array($Dates['source'])) {
                $columns['Cholesterol'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(ROUND(MIN(LDL),2)'
                        . ',"-",ROUND(MAX(LDL),2)'
                        . ',"-",ROUND(MIN(HDL),2)'
                        . ',"-",ROUND(MAX(HDL),2)'
                        . ',"-",ROUND(MIN(Triglycerides),2)'
                        . ',"-",ROUND(MAX(Triglycerides),2)'
                        . ')'
                        . ' from hm_cholesterol_records where UserID=SEU.user_id AND CholesterolRecordDate>"'
                        . $start_date . '" AND CholesterolRecordDate<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($source['Blood_Pressure'])) {
                $source = ($Dates['Blood Pressure'] == 'manual') ? '' : $Dates['Blood_Pressure'];
                
                $columns['Blood_Pressure'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(MIN(systolic)'
                        . ',"-",MAX(systolic)'
                        . ',"-",MIN(diastolic)'
                        . ',"-",MAX(diastolic)'
                        . ',"-",MIN(pulse)'
                        . ',"-",MAX(pulse)'
                        . ')'
                        . ' from hm_blood_pressure_records where UserID=SEU.user_id AND bp_record_date>"'
                        . $start_date . '" AND bp_record_date<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            } else if (!is_array($Dates['source'])) {
                $columns['Blood_Pressure'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(MIN(systolic)'
                        . ',"-",MAX(systolic)'
                        . ',"-",MIN(diastolic)'
                        . ',"-",MAX(diastolic)'
                        . ',"-",MIN(pulse)'
                        . ',"-",MAX(pulse)'
                        . ')'
                        . ' from hm_blood_pressure_records where UserID=SEU.user_id AND bp_record_date>"'
                        . $start_date . '" AND bp_record_date<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($source['Glucose'])) {
                $source = ($Dates['Glucose'] == 'manual') ? '' : $Dates['Glucose'];
                
                $columns['Glucose'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(MIN(measurement)'
                        . ',"-",MAX(measurement)'
                        . ')'
                        . ' from hm_blood_glucose_records where UserID=SEU.user_id AND blood_record_date>"'
                        . $start_date . '" AND blood_record_date<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            } else if (!is_array($Dates['source'])) {
                $columns['Glucose'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(MIN(measurement)'
                        . ',"-",MAX(measurement)'
                        . ')'
                        . ' from hm_blood_glucose_records where UserID=SEU.user_id AND blood_record_date>"'
                        . $start_date . '" AND blood_record_date<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($source['SleepRecord'])) {
                $source = ($Dates['SleepRecord'] == 'manual') ? '' : $Dates['SleepRecord'];
                
                $columns['SleepRecord'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(SUM(total_sleep)'
                        . ',"-",SUM(awake)'
                        . ',"-",SUM(deep)'
                        . ',"-",SUM(light)'
                        . ',"-",SUM(rem)'
                        . ')'
                        . ' from hm_sleep_records where UserID=SEU.user_id AND sleep_record_date>"'
                        . $start_date . '" AND sleep_record_date<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            } else if (!is_array($Dates['source'])) {
                $columns['SleepRecord'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(SUM(total_sleep)'
                        . ',"-",SUM(awake)'
                        . ',"-",SUM(deep)'
                        . ',"-",SUM(light)'
                        . ',"-",SUM(rem)'
                        . ')'
                        . ' from hm_sleep_records where UserID=SEU.user_id AND sleep_record_date>"'
                        . $start_date . '" AND sleep_record_date<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($source['Temperature'])) {
                $source = ($Dates['Temperature'] == 'manual') ? '' : $Dates['Temperature'];
                
                $columns['Temperature'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(MIN(temperature)'
                        . ',"-",MAX(temperature)'
                        . ')'
                        . ' from hm_temperature_records where UserID=SEU.user_id AND temperature_record_date>"'
                        . $start_date . '" AND temperature_record_date<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            } else if (!is_array($Dates['source'])) {
                $columns['Temperature'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(MIN(temperature)'
                        . ',"-",MAX(temperature)'
                        . ')'
                        . ' from hm_temperature_records where UserID=SEU.user_id AND temperature_record_date>"'
                        . $start_date . '" AND temperature_record_date<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            $columns['CONDITIONS'] = new \Zend\Db\Sql\Predicate\Expression('(select GROUP_CONCAT(common_name) from hm_super_conditions where id IN ('
                    . 'SELECT condition_id FROM hm_users_conditions WHERE user_id=SEU.user_id AND status=1'
                    . '))');

            $columns['SYMPTOMS'] = new \Zend\Db\Sql\Predicate\Expression('(select GROUP_CONCAT(common_name) from hm_super_symptoms where id IN ('
                    . 'SELECT symtoms_id FROM hm_users_symtoms WHERE user_id=SEU.user_id AND status=1'
                    . '))');

            $columns['TREATMENTS'] = new \Zend\Db\Sql\Predicate\Expression('(select GROUP_CONCAT(common_name) from hm_super_treatments where id IN ('
                    . 'SELECT treatment_id FROM hm_users_treatments WHERE user_id=SEU.user_id AND status=1'
                    . '))');



            if (count($columns) > 0) {
                $select->columns($columns);
            }

            $options = array('Email','UserID');
            $select->join(array('User' => 'hm_users'), new \Zend\Db\Sql\Expression("User.UserID = SEU.user_id"), $options, 'LEFT');
            $options = array();
            $select->join(array('Var' => 'hm_super_ass_study_questions_variable'), new \Zend\Db\Sql\Expression("Var.var_id = SEU.ques_variable"), $options, 'LEFT');
            $select->join(array('Opt' => 'hm_super_ass_study_questions_option'), new \Zend\Db\Sql\Expression("Opt.question_id = SEU.quest_id AND Opt.opt_value = SEU.option_id"), $options, 'LEFT');

            if ($lookingfor) {
                
            }
            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            if ($group) {
                $select->group(array('SEU.user_id'));
            }
            $select->order($orderBy);
//            echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//            die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    

}
