<?php

namespace Admin\Form;

use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Form;

class CreateSelf extends Form {

    public function __construct($name = null) {
        parent::__construct('self');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', 'admin-self');
        $this->setAttribute('id', 'ass_form');

        $this->add(array( 
            'name' => 'ass_name', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'id' => 'ass_name', 
                'placeholder' => 'Assessment Name', 
                'autocomplete' => 'off', 
                //'onKeyUp' => 'suggestNameOfAssessment(this);', 
            ), 
            'options' => array( 
            ), 
        )); 
        
        $this->add(array( 
            'name' => 'assessment_desc', 
            'type' => 'Zend\Form\Element\Textarea', 
            'attributes' => array( 
                'id' => 'assessment_desc', 
                'placeholder' => 'Assessment Description'
            ), 
            'options' => array( 
            ), 
        )); 
        
        $this->add(array( 
            'name' => 'assessment_que_desc', 
            'type' => 'Zend\Form\Element\Textarea', 
            'attributes' => array( 
                'id' => 'assessment_que_desc', 
                'placeholder' => 'Assessment Questions Description'
            ), 
            'options' => array( 
            ), 
        )); 
        
        $this->add(array(
            'name' => 'assessment_image',
            'type' => 'Zend\Form\Element\File',
            'attributes' => array(
                'id' => 'Consent'
            )
        ));
 
        $this->add(array( 
            'name' => 'ass_target', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                'id' => 'ass_target',
                'class' => 'ass_target',
                'class' => 'select', 
                'title' => 'Choose Audience', 
            ), 
            'options' => array( 
                'value_options' => array(
                    '' => 'Choose Audience', 
                    '1' => 'Both', 
                    '2' => 'Members', 
                    '3' => 'Non-Members', 
                ),
            ), 
        )); 
        
        
        
        $this->add(array( 
            'name' => 'copyright_text', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'id' => 'copyright_text', 
                'placeholder' => 'Copyright Text'
            ), 
            'options' => array( 
            ), 
        ));
 
        $this->add(array( 
            'name' => 'ass_cat', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'id' => 'ass_cat', 
                'placeholder' => 'Category Name', 
                'autocomplete' => 'off', 
                'onKeyUp' => 'suggestNameOfCategorys(this);', 
            ), 
            'options' => array( 
            ), 
        ));

        $this->add(array('name' => 'c_submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class'=>'submit_button',
                'value' => 'Submit',
                'id' => 'c_submit'
            ),
        ));

        // $this->add(
        //     array( 
        //         'name' => 'csrf', 
        //         'type' => 'Zend\Form\Element\Csrf', 
        //     )
        // ); 
    }
}