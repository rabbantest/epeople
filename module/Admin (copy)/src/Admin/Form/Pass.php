<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Pass extends Form {

    public function __construct($name = null) {
        parent::__construct('pass');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', 'admin-passDomain-add');
        $this->setAttribute('id', 'AddPassForm');


        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'PassId'
            )
        ));

        $this->add(array(
            'name' => 'subdomain',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'Subdomain',
                'placeholder' => 'Subdomain Name',
            ),
            'options' => array(
            ),
        ));
        
        
        $this->add(array(
            'name' => 'company_name',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'CompanyName',
                'placeholder' => 'Company Name',
            ),
            'options' => array(
            ),
        ));
        

        $this->add(array(
            'name' => 'admin_user_name',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'EmailAddress',
                'placeholder' => 'Subdomain Email Address'
                //'onblur'=>'return validateAdminUser();'
            ),
            'options' => array(
            ),
        ));
        
        $this->add(array(
            'name' => 'admin_name',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'AdminName',
                'placeholder' => 'Subdomain Admin Name',
            ),
            'options' => array(
            ),
        ));

        $this->add(array(
            'name' => 'admin_password',
            'type' => 'Zend\Form\Element\Password',
            'attributes' => array(
                'id' => 'AdminPassword',
                'placeholder' => 'Admin Password',
            ),
            'options' => array(
            ),
        ));

        $this->add(array(
            'name' => 'contact_number',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'ContactNumber',
                'placeholder' => 'Contact Number',
            ),
            'options' => array(
            ),
        ));

        $this->add(array(
            'name' => 'panel_member',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'PanelMember',
                'placeholder' => 'Total Number of Panel Members to be recruited',
            ),
            'options' => array(
            ),
        ));

        $this->add(array(
            'name' => 'study_survey',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'StudySurvey',
                'placeholder' => 'Total Number of Studies & Surveys conducted',
            ),
            'options' => array(
            ),
        ));
        
        
        $this->add(array(
            'name' => 'tango_customer',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'TangoCustomerName',
                'placeholder' => 'Customer Name For Tango Card ',
            ),
            'options' => array(
            ),
        ));
        
        $this->add(array(
            'name' => 'tango_email',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'TangoEmail',
                'placeholder' => 'Email Address For Tango Card',
            ),
            'options' => array(
            ),
        ));
        
        $this->add(array(
            'name' => 'tango_acc_status',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'tango_acc_status'
            )
        )); 
        
        $this->add(array(
            'name' => '	account_balance',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => '	account_balance'
            )
        )); 

        $this->add(array(
            'name' => 'creation_date',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'creation_date'
            ),
            'options' => array(),
        ));

        $this->add(array(
            'name' => 'status',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'status'
            ),
            'options' => array(),
        ));

        $this->add(array(
            'name' => 'survey_que_per_mon',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'SurveyQuePerMon',
                'placeholder' => 'Total Number of Survey Questions per Month',
            ),
            'options' => array(),
        ));


        $this->add(array(
            'name' => 'logo',
            'type' => 'Zend\Form\Element\File',
            'attributes' => array(
                'id' => 'UploadDomainPic',
                'onchange'=>'readURLDomain(this);'
            )
        ));



        $this->add(array('name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class' => 'bb_btn',
                'value' => 'Save',
                'id' => 'submit'
            ),
        ));

    }

}
