<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Demographic extends Form {

    public function __construct($name = null) {
        parent::__construct('demographic');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', 'javascript:void(0)');
        $this->setAttribute('id', 'AddDemographicForm');

        $this->add(array(
            'name' => 'country',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'Country',
                'class' => 'select ',
                'title' => 'Select Country',
                'custom' => 'State',
            ),
            'options' => array(
            )
        ));

        $this->add(array(
            'name' => 'state',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'State',
                'class' => 'select ',
                'title' => 'Select State',
                'custom' => 'Counties',
            ),
            'options' => array(
            )
        ));

        $this->add(array(
            'name' => 'counties',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'Counties',
                'class' => 'select ',
                'title' => 'Select Counties',
            ),
            'options' => array(
            )
        ));

        $this->add(array(
            'name' => 'gender',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'Gender',
                'class' => 'select ',
                'title' => 'Select Gender',
            ),
            'options' => array( 
                'value_options' => array(
                    ''=>'Select Gender',
                'm' => 'Male',
                'f' => 'Female',
                ),
            ), 
            
        ));
        
        $this->add(array(
            'name' => 'age_from',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'AgeFrom',
                'class'=>'AgeFrom',
                'placeholder' => 'Min',
            ),
            'options' => array(
            ),
        ));
        
        $this->add(array(
            'name' => 'age_to',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'AgeTo',
                'placeholder' => 'Max',
            ),
            'options' => array(
            ),
        ));


        $this->add(array('name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class' => 'submit_button bb_btn',
                'value' => 'Save',
                'id' => 'submit'
            ),
        ));
    }

}
