<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Questionnaire extends Form {

    public function __construct($name = null) {
        parent::__construct('questionnaire');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', 'admin-questionnaire');
        $this->setAttribute('id', 'AddQuestionnaireForm');

        $this->add(array(
            'name' => 'name',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'QuestionnaireName',
                'placeholder' => 'Questionnaire Name',
            ),
            'options' => array(
            ),
        ));

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'QuestionnaireId'
            )
        ));


        $this->add(array(
            'name' => 'status',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'Status',
                'class' => 'select required',
                'title' => 'Select Status'
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Draft',
                    '1' => 'Publish'
                )
            )
        ));

        $this->add(array('name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class' => 'submit_button',
                'value' => 'Submit',
                'id' => 'c_submit'
            ),
        ));

        // $this->add(
        //     array( 
        //         'name' => 'csrf', 
        //         'type' => 'Zend\Form\Element\Csrf', 
        //     )
        // ); 
    }

}
