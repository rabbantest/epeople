<?php

namespace Admin\Form;

use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Form;

class ChangePassword extends Form {

    public function __construct($name = null) {
        parent::__construct('');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/');

        $this->add(array(
            'name' => 'old_pwd', 
            'type' => 'Zend\Form\Element\Password', 
            'attributes' => array( 
                'id' => 'old_pwd', 
                'placeholder' => 'Old Password', 
            ), 
            'options' => array( 
            ), 
        )); 

        $this->add(array(
            'name' => 'new_pwd', 
            'type' => 'Zend\Form\Element\Password', 
            'attributes' => array( 
                'id' => 'new_pwd', 
                'placeholder' => 'New Password', 
            ), 
            'options' => array( 
            ), 
        )); 

        $this->add(array(
            'name' => 'cnfm_pwd', 
            'type' => 'Zend\Form\Element\Password', 
            'attributes' => array( 
                'id' => 'cnfm_pwd', 
                'placeholder' => 'Confirm Password', 
            ), 
            'options' => array( 
            ), 
        )); 

        $this->add(array(
            'name' => 'c_submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class'=>'submit_button',
                'value' => 'Submit',
                'id' => 'c_submit'
            ),
        ));

        // $this->add(
        //     array( 
        //         'name' => 'csrf', 
        //         'type' => 'Zend\Form\Element\Csrf', 
        //     )
        // ); 
    }
}