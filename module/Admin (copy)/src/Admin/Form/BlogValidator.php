<?php
namespace Admin\Form;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class BlogValidator implements InputFilterAwareInterface
{
      protected $inputFilter;

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
     public function getInputFilter() {
          if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            
            $inputFilter->add(array(
                'name' => 'title',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100
                        ),
                    ),
                ),
            ));
            
            $inputFilter->add(array(
                'name' => 'category',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 100
                        ),
                    ),
                ),
            ));
            
            $inputFilter->add(array(
                'name' => 'author',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 100
                        ),
                    ),
                ),
            ));
            
              $inputFilter->add(array(
                'name' => 'short_description',      
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 800
                        ),
                    ),
                ),
            ));
              
               $inputFilter->add(array(
                'name' => 'long_description', 
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            //'max' => 800
                        ),
                    ),
                ),
            ));
               
        $inputFilter->add(array(
                    'name' => 'blog_image',
                    'required' => false,
                    'validators' => array(
                        array(
                            'name' => 'Zend\Validator\File\Size',
                            'options' => array(
                                'min' => 120,
                                'max' => 200000,
                            ),
                        ),
                        array(
                            'name' => 'Zend\Validator\File\Extension',
                            'options' => array(
                                'extension' => array('jpg', 'png', 'gif', 'jpeg', 'JPG', 'PNG', 'GIF', 'JPEG'),
                            ),
                        ),
                    ),
                )
            );
              
         $this->inputFilter = $inputFilter;
//         echo "<pre>";
//         print_r($inputFilter);exit;
          }
          return $this->inputFilter;
     }
}
 
