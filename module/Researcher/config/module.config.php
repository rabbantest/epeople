<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE MODULE CONFIG FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: SHIVENDRA SUMAN
 * CREATOR: 27/04/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */
return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Index',
                        'action' => 'index',
                    ),
                )
            ),
            // using the path /application/:controller/:action
            'researcher' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/researcher',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Researcher\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
            'researcher_login' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-login',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Index',
                        'action' => 'login',
                    ),
                ),
            ),
            /* For getting caregiver profile req info */
            'researcher_getpro' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-getprofileinfo',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Index',
                        'action' => 'getprofileinfo',
                    ),
                ),
            ),
            /* For dashboard page for pending caregiver profile */
            'researcher_basic_profile' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-basic-info',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Profile',
                        'action' => 'index',
                    ),
                ),
            ),
            /* For dashboard page for pending caregiver profile */
            'researcher_contact_info' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-contact-info',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Profile',
                        'action' => 'contactinfo',
                    ),
                ),
            ),
            /* For dashboard page for pending caregiver profile */
            'researcher_profile_info' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-profile-info',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Profile',
                        'action' => 'profileinfo',
                    ),
                ),
            ),
            'researcher_temp' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-tempass',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Index',
                        'action' => 'tempass',
                    ),
                ),
            ),
            'researcher_basic' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-basicinfo',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Index',
                        'action' => 'basicinfo',
                    ),
                ),
            ),
            'researcher_contactinfo' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-contactinfo',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Index',
                        'action' => 'contactinfo',
                    ),
                ),
            ),
            'researcher_profileinfo' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-profileinfo',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Index',
                        'action' => 'profileinfo',
                    ),
                ),
            ),
            'researcher_archival' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-archival',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Archival',
                        'action' => 'index',
                    ),
                ),
            ),
            'researcher_archival_list' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-archival-list',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Archival',
                        'action' => 'listarchival',
                    ),
                ),
            ),
            'researcher_archival_data' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-archival-data',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Archival',
                        'action' => 'archivaldata',
                    ),
                ),
            ),
            'researcher_studies_list' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'index',
                    ),
                ),
            ),
             'researcher_StudyDel' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-del',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'deleteStudy',
                    ),
                ),
            ),
            'researcher_studies_create' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-create',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'studiescreate',
                    ),
                ),
            ),
            'researcher_StudyPassiveVar' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-passive-var',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'passiveStudyVar',
                    ),
                ),
            ),
            /*             * Start researcher subsciption routing code * */
            'researcher_subscription' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-subscription',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Subscription',
                        'action' => 'index',
                    ),
                ),
            ),
            'researcher_crrstatus' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-crrstatus',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Subscription',
                        'action' => 'crrstatus',
                    ),
                ),
            ),
            'researcher_billing' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-billing',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Subscription',
                        'action' => 'billing',
                    ),
                ),
            ),
            'researcher_plans' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-subsplans',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Subscription',
                        'action' => 'plans',
                    ),
                ),
            ),
            'researcher_notify' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-notify',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Subscription',
                        'action' => 'notify',
                    ),
                ),
            ),
            /*             * end researcher subsciption routing code* */
            'researcher_StudyMainQueFilter' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-mainque-filter',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'mainQuestionFilter',
                    ),
                ),
            ),
            'researcher_StudySrcResult' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-scrresult',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'studyResult',
                    ),
                ),
            ),
            'researcher_StudyCodebook' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-codebook',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'codebook',
                    ),
                ),
            ),
            'researcher_StudyMainResult' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-mainresult',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'studyMainResult',
                    ),
                ),
            ),
            'researcher_StudyQues' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-ques',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'question',
                    ),
                ),
            ),
             'researcher_StudyQuestionnaire' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-questionnaire',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'questionnaire',
                    ),
                ),
            ),
            'researcher_StudyQuesScr' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-screening-ques',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'questionscreening',
                    ),
                ),
            ),
            'researcher_studies_create_var' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-varrable-set',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'studiescreatevar',
                    ),
                ),
            ),
            'researcher_StudyVarSetAdd' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-varrable-set-add',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'addAttribute',
                    ),
                ),
            ),
            'researcher_StudyInvitationSet' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-invitation-set',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'studiesInvitationsSet',
                    ),
                ),
            ),
            'researcher_StudyEligibleUsers' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-studies-eligible-users',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studies',
                        'action' => 'studyEligibleSetUsers',
                    ),
                ),
            ),
            'researcher_archivalcron' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-archival-cron',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Crons',
                        'action' => 'index',
                    ),
                ),
            ),
            /*             * ******Pass Studies ************ */
            'pass_researcher_studies_list' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-researcher-studies',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'index',
                    ),
                ),
            ),
              'pass_StudyDel' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-del',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'deleteStudy',
                    ),
                ),
            ),
            'pass_CreateStudies' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-create',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'studies',
                    ),
                ),
            ),
            'pass_StudyPassiveVar' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-passive-var',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'passiveStudyVar',
                    ),
                ),
            ),
            'pass_StudyQuestionnaire' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-questionnaire',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'questionnaire',
                    ),
                ),
            ),
            'pass_StudyQues' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-ques',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'question',
                    ),
                ),
            ),
            'pass_StudyQuesScr' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-screening-ques',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'questionscreening',
                    ),
                ),
            ),
            'pass_StudyVarSet' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-varrable-set',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'studiesvarset',
                    ),
                ),
            ),
            'pass_StudyVarSetAdd' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-varrable-set-add',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'addAttribute',
                    ),
                ),
            ),
            'pass_StudyInvitationSet' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-invitation-set',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'studiesInvitationsSet',
                    ),
                ),
            ),
             'pass_StudyInvite' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-instant-invitation',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Studyinvite',
                        'action' => 'index',
                    ),
                ),
            ), 
            'pass_StudyEligibleUsers' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-eligible-users',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'studyEligibleSetUsers',
                    ),
                ),
            ),
            'pass_StudyMainQueFilter' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-mainque-filter',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'mainQuestionFilter',
                    ),
                ),
            ),
            'pass_StudyCodebook' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-codebook',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'codebook',
                    ),
                ),
            ),
            'pass_StudySrcResult' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-scrresult',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'studyResult',
                    ),
                ),
            ),
            'pass_StudyMainResult' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/pass-studies-mainresult',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Passstudies',
                        'action' => 'studyMainResult',
                    ),
                ),
            ),
            /*             * ****Routing start for panel member dashboard******** */
            'researcher_panelmem' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-panelmember',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Panelmember',
                        'action' => 'index',
                    ),
                ),
            ),
            'researcher_beacon' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/researcher-beaconlist[/:id]',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Panelmember',
                        'action' => 'beaconlist',
                    ),
                ),
            ),
            'researcher_trackmove' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/researcher-trackmove[/:id]',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Panelmember',
                        'action' => 'trackmove',
                    ),
                ),
            ),
            'researcher_followongps' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/researcher-followongps[/:id]',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Panelmember',
                        'action' => 'followongps',
                    ),
                ),
            ),            
            'researcher_Questionnaire' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-questionnaire',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Resquestionnaire',
                        'action' => 'index',
                    ),
                ),
            ), 'researcher_QuestionnaireDel' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-questionnaire-delete',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Resquestionnaire',
                        'action' => 'deleteQuestionnaire',
                    ),
                ),
            ), 
            'researcher_QuestionnaireQue' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-questionnaire-question',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Resquestionnaire',
                        'action' => 'question',
                    ),
                ),
            ),
            'researcher_Group_Questionnaire' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-group-questionnaire',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Answergroup',
                        'action' => 'index',
                    ),
                ),
            ),
             'researcher_Group_setans' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/researcher-group-setanswer',
                    'defaults' => array(
                        'controller' => 'Researcher\Controller\Answergroup',
                        'action' => 'setanswer',
                    ),
                ),
            ),
        /*         * ****Routing end for panel member dashboard******** */
        )
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Researcher\Controller\Index' => 'Researcher\Controller\IndexController',
            'Researcher\Controller\ResearcherAjax' => 'Researcher\Controller\ResearcherAjaxController',
            'Researcher\Controller\Profile' => 'Researcher\Controller\ProfileController',
            'Researcher\Controller\Archival' => 'Researcher\Controller\ArchivalController',
            'Researcher\Controller\Studies' => 'Researcher\Controller\StudiesController',
            'Researcher\Controller\Crons' => 'Researcher\Controller\CronsController',
            'Researcher\Controller\Subscription' => 'Researcher\Controller\SubscriptionController',
            'Researcher\Controller\Panelmember' => 'Researcher\Controller\PanelmemberController',
            'Researcher\Controller\Passstudies' => 'Researcher\Controller\PassstudiesController',
            'Researcher\Controller\Studyinvite' => 'Researcher\Controller\StudyinviteController',
            'Researcher\Controller\Resquestionnaire' => 'Researcher\Controller\ResquestionnaireController',
            'Researcher\Controller\Answergroup' => 'Researcher\Controller\AnswergroupController',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
        //  'displayDate' => 'Caregiver\View\Helper\DisplayDateHelper',
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'header/researcherheader' => __DIR__ . '/../view/header/researcherheader.phtml',
            'left-menu/researcher-left-menu' => __DIR__ . '/../view/left-menus/researcher-left-menu.phtml',
            'paginator/paginators' => __DIR__ . '/../view/researcher/paginator/paginators.phtml',
            'researcherlayout' => __DIR__ . '/../view/layout/researcherlayout.phtml',
            'footer/researcherfooter' => __DIR__ . '/../view/footer/researcherfooter.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
?>