<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE MODULE CONFIG FOR APPLICATION.
 * #COPYRIGHT: Affle
 * @AUTHOR: Rabban Ahmad
 * CREATOR: 27/3/2015.
 * UPDATED: 27/3/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Researcher;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Researcher\Model\ResearcherUsersTable;
use Researcher\Model\TempResearcherUsersTable;
use Researcher\Model\ResearcherCountryTable;
use Researcher\Model\ResearcherCandiTable;
use Researcher\Model\ResearcherSymptTable;
use Researcher\Model\ResearcherTreatsTable;
use Researcher\Model\ResearcherArchivalTable;
use Researcher\Model\ResearcherStudyTable;
use Researcher\Model\ResearcherVariablesTable;
use Researcher\Model\ResearcherQuestionTable;
use Researcher\Model\ResearcherStudyEligibleTable;
use Researcher\Model\ResearcherQuesAnalyzeTable;
use Researcher\Model\ResearcherAssessmentTable;
use Zend\Session\Container;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        date_default_timezone_set('UTC');
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, function($e) {
            $result = $e->getResult();
            $result->setTerminal(TRUE);
        });
        $eventManager->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($event) {
            $controller = $event->getTarget();
            $controllerName = get_class($controller);
            $moduleNamespace = substr($controllerName, 0, strpos($controllerName, '\\'));
            $sessionObj = new Container('Researcher');
            $siteURL = $_SERVER['HTTP_HOST'];
            $siteArray = explode('.', $siteURL);
            if (strtolower($siteArray[0]) == 'www') {
                $siteDomain = $siteArray[1];
            } else {
                $siteDomain = $siteArray[0];
            }
            $explodurl = explode("/", $_SERVER["REQUEST_URI"]);
            $basename = $explodurl[1];
            if (($sessionObj->isCaregiverlogin && ($siteDomain != $sessionObj->domain)) || ($basename != 'researcher-getprofileinfo' && $basename != 'researcher-archival-cron' && $basename != 'researcher-login' && !$sessionObj->isResearcherlogin && $moduleNamespace == 'Researcher')) {
                $sessionObj->getManager()->getStorage()->clear('Researcher');
                $url = $event->getRouter()->assemble(array(), array('name' => 'login'));
                $response = $event->getResponse();
                $response->getHeaders()->addHeaderLine('Location', '/researcher-login');
                $response->setStatusCode(302);
                $response->sendHeaders();

                // When an MvcEvent Listener returns a Response object,
                // It automatically short-circuit the Application running 
                // -> true only for Route Event propagation see Zend\Mvc\Application::run
                // To avoid additional processing
                // we can attach a listener for Event Route with a high priority
                $stopCallBack = function($event) use ($response) {
                    $event->stopPropagation();
                    return $response;
                };
                //Attach the "break" as a listener with a high priority
                $e->getApplication()->getEventManager()->attach(MvcEvent::EVENT_ROUTE, $stopCallBack, -10000);
                return $response;
            }
            $configs = $event->getApplication()->getServiceManager()->get('config');
            if (isset($configs['module_layouts'][$moduleNamespace])) {
                $controller->layout($configs['module_layouts'][$moduleNamespace]);
            }
        }, 100);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
                'options' => function($sm) {
                    $helper = new View\Helper\Options;
                    return $helper;
                },
                'Percentage' => function($sm) {
                    $helper = new View\Helper\Percentage;
                    return $helper;
                },
                'PercentageNonmember' => function($sm) {
                    $helper = new View\Helper\PercentageNonmember;
                    return $helper;
                },
                'Getdata' => function($sm) {
                    $helper = new View\Helper\Getdata;
                    return $helper;
                },
                'GetStudy' => function($sm) {
                    $helper = new View\Helper\GetStudy;
                    return $helper;
                },
                'Resize' => function($sm) {
                    $helper = new View\Helper\Resize;
                    return $helper;
                }
            )
        );
    }

    public function getServiceConfig() {
        return array('factories' => array(
                'ResearcherUsers' => function ($serviceManager) {
                    return new ResearcherUsersTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ResCountry' => function ($serviceManager) {
                    return new ResearcherCountryTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ResConditionTable' => function ($serviceManager) {
                    return new ResearcherCandiTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ResSymptomsTable' => function ($serviceManager) {
                    return new ResearcherSymptTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ResTreatmentsTable' => function ($serviceManager) {
                    return new ResearcherTreatsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'TempResUsers' => function ($serviceManager) {
                    return new TempResearcherUsersTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ResearcherArchivalTable' => function ($serviceManager) {
                    return new ResearcherArchivalTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                }, 'ResStuTable' => function ($serviceManager) {
                    return new ResearcherStudyTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ResVarTable' => function ($serviceManager) {
                    return new ResearcherVariablesTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ResQuesTable' => function ($serviceManager) {
                    return new ResearcherQuestionTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ResStudyEligibleTable' => function ($serviceManager) {
                    return new ResearcherStudyEligibleTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ResAnalyzeTable' => function ($serviceManager) {
                    return new ResearcherQuesAnalyzeTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
                'ResAssessTable' => function ($serviceManager) {
                    return new ResearcherAssessmentTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
                },
            )
        );
    }

    /**
     * @param MvcEvent $e
     * @return null|\Zend\Http\PhpEnvironment\Response
     */
    public function postProcess(MvcEvent $e) {
        $routeMatch = $e->getRouteMatch();
        $formatter = $routeMatch->getParam('formatter', false);

        /** @var \Zend\Di\Di $di */
        $di = $e->getTarget()->getServiceLocator()->get('di');

        if ($formatter !== false) {
            if ($e->getResult() instanceof \Zend\View\Model\ViewModel) {
                if (is_array($e->getResult()->getVariables())) {
                    $vars = $e->getResult()->getVariables();
                } else {
                    $vars = null;
                }
            } else {
                $vars = $e->getResult();
            }

            /** @var PostProcessor\AbstractPostProcessor $postProcessor */
            $postProcessor = $di->get($formatter . '-pp', array(
                'response' => $e->getResponse(),
                'vars' => $vars,
            ));

            $postProcessor->process();

            return $postProcessor->getResponse();
        }

        return null;
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}
