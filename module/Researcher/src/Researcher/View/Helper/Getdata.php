<?php

namespace Researcher\View\Helper;

use Researcher\Model\VariablesTable;
use Zend\View\Helper\AbstractHelper;

class Getdata extends AbstractHelper {

    public function __invoke($data, $ModelObj, $ModelName) {
        switch ($ModelName) {
            case 'Variable':
                $Obj = $ModelObj[$ModelName];
                $where['Var'] = array('ques_variable' => $data[0]);
                $where['Opt'] = $data[1];
                $fieldSet['Var'] = array('ques_id');
                $fieldSet['Opt'] = array('opt_option');

                return $Obj->getAdminVariable($where, $fieldSet);

            case 'Demographic':
                $Obj = $ModelObj[$ModelName];
                $Country = $Obj->getCountry(array('country_id' => $data['country']), array('country_name'));
                $State = $Obj->getState(array('state_id' => $data['state']), array('state_name'));
                $County = $Obj->getCounties(array('county_id' => $data['counties']), array('county_name'));
                return array('Country' => isset($Country[0]['country_name']) ? $Country[0]['country_name'] : NULL,
                    'State' => isset($State[0]['state_name']) ? $State[0]['state_name'] : NULL,
                    'County' => isset($County[0]['county_name']) ? $County[0]['county_name'] : NULL
                );
            case 'Condition':
                $Obj = $ModelObj[$ModelName];
                $Condition = $Obj->getAdminConditions(array('id' => $data), array('common_name'));
                return isset($Condition[0]['common_name']) ? $Condition[0]['common_name'] : 'N/A';
            case 'Symptom':
                $Obj = $ModelObj[$ModelName];
                $Symptom=$Obj->getAdminSymptoms(array('id' => $data),array('common_name'));
                return isset($Symptom[0]['common_name']) ? $Symptom[0]['common_name'] : 'N/A';
            case 'Treatment':
                 $Obj = $ModelObj[$ModelName];
                $Treatment=$Obj->getAdminTreatments(array('id' => $data),array('common_name'));
                return isset($Treatment[0]['common_name']) ? $Treatment[0]['common_name'] : 'N/A';
        }
        
        
    }

}
