<?php
//change fgsdffg sdf 
namespace Researcher\View\Helper;

use Researcher\Model\VariablesTable;
use Zend\View\Helper\AbstractHelper;

class GetStudy extends AbstractHelper {

    public function __invoke($sid, $Obj,$type) {
        $col = array('total' => new \Zend\Db\Sql\Expression('SUM(number)'));
        $whereArr = array('study_id' => $sid, 'sent' => 1);
        $TotalInvite = $Obj->getAdminStudyBucketCronSetting($whereArr, $col);
        $Result['total'] = isset($TotalInvite[0]['total']) ? $TotalInvite[0]['total'] : 0;


        $TotalInviteEmail = $Obj->getAdminStudyBucketInvitation(array('bucket' => -1, 'eligible_for_main' => $type, 'study_id' => $sid), array('number' => new \Zend\Db\Sql\Expression('count(*)'), 'id'));
        $TotalInviteTrial = $Obj->getAdminStudyBucketInvitation(array('bucket' => -2, 'eligible_for_main' => $type, 'study_id' => $sid), array('number' => new \Zend\Db\Sql\Expression('count(*)'), 'id'));
//        print_r($TotalInviteEmail);
//        print_r($TotalInviteTrial);die;
        
        $Result['total']=$Result['total']+$TotalInviteEmail[0]['number'];
        $Result['total']=$Result['total']+$TotalInviteTrial[0]['number'];
        

        $col = array('Participated' => new \Zend\Db\Sql\Expression('count(DISTINCT(part.UserID))'));
        $TotalPart = $Obj->getAdminStudyParticipation(array('study_id' => $sid), $col);
        $Result['Participated'] = isset($TotalPart[0]['Participated']) ? $TotalPart[0]['Participated'] : 0;

        return $Result;
    }

}