<?php

namespace Researcher\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class VariablePass extends Form {

    public function __construct($name = null) {
        parent::__construct('variable');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', 'javascript:void(0)');
        $this->setAttribute('id', 'AddVariableForm');

        $this->add(array(
            'name' => 'var_id',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'VarId',
                'class' => 'select required',
                'title' => 'Select Variable'
            ),
            'options' => array(
            )
        ));

        $this->add(array(
            'name' => 'option_id',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'OptionId',
                'class' => 'selectM_condition required',
                'multiple' => 'multiple',
                'custom' => 'Counties',
            ),
            'options' => array(
            )
        ));

        $this->add(array('name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class' => 'submit_button bb_btn',
                'value' => 'Save',
                'id' => 'submit'
            ),
        ));
    }

}
