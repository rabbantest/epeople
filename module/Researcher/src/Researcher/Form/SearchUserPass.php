<?php

namespace Researcher\Form;

use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Form;

class SearchUserPass extends Form {

    public function __construct($name = null) {
        parent::__construct('user');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/');

        $this->add(array( 
            'name' => 'first_name', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'id' => 'first_name', 
                'placeholder' => 'First Name', 
            ), 
            'options' => array( 
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'last_name', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'id' => 'last_name', 
                'placeholder' => 'Last Name', 
            ), 
            'options' => array( 
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'username', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'id' => 'username', 
                'placeholder' => 'User Name', 
            ), 
            'options' => array( 
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'gender', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                'class' => 'select', 
                'id' => 'gender', 
                'value' => '0', 
            ), 
            'options' => array( 
                'value_options' => array(
                    '0' => 'Gender', 
                    'm' => 'Male', 
                    'f' => 'Female', 
                ),
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'email', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'id' => 'email', 
                'placeholder' => 'Email', 
            ), 
            'options' => array( 
            ), 
        )); 

        $this->add(array( 
            'name' => 'city', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'id' => 'city', 
                'placeholder' => 'City', 
            ), 
            'options' => array( 
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'state', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                'class' => 'select', 
                'id' => 'state', 
                'value' => '0', 
            ), 
            'options' => array( 
                'value_options' => array(
                    '0' => 'State', 
                ),
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'country', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                'class' => 'select', 
                'id' => 'country', 
                'value' => '0', 
            ), 
            'options' => array( 
                'value_options' => array(
                    '0' => 'Country', 
                ),
            ), 
        )); 

        $this->add(array(
            'name' => 'us_submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class'=>'su_srch',
                'value' => 'Search',
                'id' => 'us_submit'
            ),
        ));

        $this->add(array(
            'name' => 'us_button',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class'=>'su_reset',
                'value' => 'Reset All',
                'id' => 'us_button'
            ),
        ));

        // $this->add(
        //     array( 
        //         'name' => 'csrf', 
        //         'type' => 'Zend\Form\Element\Csrf', 
        //     )
        // ); 
    }
}