<?php

namespace Researcher\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class SymptomPass extends Form {

    public function __construct($name = null) {
        parent::__construct('symptom');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', 'javascript:void(0)');
        $this->setAttribute('id', 'AddSymptomForm');

        $this->add(array(
            'name' => 'symptom_id',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'SymptomId',
                'class' => 'selectM_condition required',
                'multiple' => 'multiple'
            ),
            'options' => array(
            )
        ));

        $this->add(array('name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class' => 'submit_button bb_btn',
                'value' => 'Save',
                'id' => 'submit'
            ),
        ));
    }

}
