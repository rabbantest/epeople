<?php

namespace Researcher\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class InvitationSet extends Form {

    public function __construct($name = null) {
        parent::__construct('invitationset');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', 'javascript:void(0)');
        $this->setAttribute('id', 'AddInvitationSetForm');

        $this->add(array(
            'name' => 'invitation_set_study',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'StudyId'
            )
        ));

        $this->add(array(
            'name' => 'invitation_email',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'invitation_email',
                'class' => 'required email',
                'placeholder' => 'Email Address'
            ),
            'options' => array(
            )
        ));
        
        $this->add(array('name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'submit',
                'class' => 'addMoreQOption',
                'src' => '../../img/add_icon_normal.png',
                'value' => 'Next >',
                'id' => 'submit'
            ),
        ));
    }

}
