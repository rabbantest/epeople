<?php
/*************************
    * PAGE: USE TO MANAGE THE ROOT CONTROLLER FOR CAREGIVER APPLICATION.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: RABBAN AHMAD.
    * CREATOR: 27/3/15
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/

namespace Researcher\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\Plugin\Redirect;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
class BaseController extends AbstractActionController
{
    //protected $_sessionObj;
	
	
    public function __construct() {
		$this->_sessionObj = new Container('Researcher');
		$UserDataArr = $this->_sessionObj->userDetails;
		$siteURL = $_SERVER['HTTP_HOST'];
		$siteArray = explode('.',$siteURL);		
		$pieces = parse_url($siteURL);
		
		
		if ($this->_sessionObj->isCaregiverlogin) {
			//return $this->redirect()->toUrl('/cg-login');
			/* $url="";
			$this->plugin('redirect')->toUrl($url);
			return FALSE; */
		}
    }
}
