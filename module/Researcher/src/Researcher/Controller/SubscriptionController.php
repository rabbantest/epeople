<?php

namespace Researcher\Controller;

/* * ***********************
 * PAGE: USE TO MANAGE SUBSCRIPTION IN CAREGIVER.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: RABBAN AHMAD.
 * CREATOR: 29/04/2015
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\Plugin\Redirect;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class SubscriptionController extends AbstractActionController {

    protected $_passDomObj;

    public function __construct() {
        $this->_sessionObj = new Container('Researcher');
    }

    /*     * *****
     * Action:- Pass Subscription status to show .
     * @author: Rabban.
     * Created Date: 25-06-2015.
     * *** */

    public function indexAction() {
        $this->_passDomObj = $this->getServiceLocator()->get("UserTable"); //CODE FOR SCROLL PAGINATION
        $columns = array('id', 'admin_user_name', 'panel_member', 'study_survey', 'subdomain', 'survey_que_per_mon');
        $PassDomain = $this->_passDomObj->getDomain(array('status' => 1), $columns, "0", array()); //CODE FOR SCROLL PAGINATION
        $colArr = array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'));

        $StuTable = $this->getServiceLocator()->get("StuTable");
        $Study = $StuTable->getAdminStudy(array('created_by' => $this->_sessionObj->userDetails['DomainID']), $colArr);

        $MembersTable = $this->getServiceLocator()->get("MembersTable");
        $exp = '(.*"' . $this->_sessionObj->userDetails['DomainID'] . '".*)("\[.*"1".*\]")';
        $whereMemberArr = array('us.Verified' => 1, new \Zend\Db\Sql\Predicate\Expression(" UserTypeId REGEXP '$exp'"));
        $Individuals = $MembersTable->getUserMembers($whereMemberArr, $colArr);

        $QuestionTable = $this->getServiceLocator()->get("QuesTable");
        $startDate = strtotime(date('Y-m-01', strtotime(date('Y-m-d H:i:s'))));
        $endDate = strtotime(date('Y-m-t', strtotime(date('Y-m-d H:i:s'))));
        $whereArr = array(new \Zend\Db\Sql\Predicate\Expression("creation_date >= '$startDate)'"),
            new \Zend\Db\Sql\Predicate\Expression("creation_date <= '$endDate)'"),
            'created_by' => $this->_sessionObj->userDetails['DomainID']);
        $StudyQueCount = $QuestionTable->getAdminQuestions($whereArr, array('num' => new \Zend\Db\Sql\Expression('COUNT(DISTINCT ques_variable)')), '0', array(), 1);

        $sendArr = new ViewModel();
        return $sendArr->setVariables(array(
                    'PassDomain' => $PassDomain[0],
                    'Individuals' => isset($Individuals[0]['num']) ? $Individuals[0]['num'] : 0,
                    'Study' => isset($Study[0]['num']) ? $Study[0]['num'] : 0,
                    'StudyQueCount' => isset($StudyQueCount[0]['num']) ? $StudyQueCount[0]['num'] : 0,
                    'session' => $this->_sessionObj->admin
        ));
    }

    public function plansAction() {
        $CareSubscriptionObj = $this->getServiceLocator()->get("CareSubscriptionObj");
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $where = array('subs.type' => 2, 'subs.status' => 1);
        $columns = array('noOfUsers' => 'user', 'price', 'id');
        $subscriptionRes = $CareSubscriptionObj->getSubscriptionDetails($where, $columns);
        $Host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'];
        $paymentParams = array('rm' => 2, 'cmd' => '_xclick', 'business' => 'membership@quantextualhealth.com', 'return' => $Host . '/researcher-billing'
            , 'cancel_return' => $Host . '/researcher-billing', 'notify' => $Host . '/researcher-notify', 'item_name' => 'Healthmetrix Payment', 'currency_code' => 'USD', 'custom' => $UserID);
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Researcher', 'subscriptionRes' => $subscriptionRes, 'paymentParams' => $paymentParams
        ));
    }

    public function billingAction() {
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $CareSubscriptionObj = $this->getServiceLocator()->get("CareSubscriptionObj");
        $txn = $_POST;
        $success = '';
        if ($txn) {
            $txn_id = $txn['txn_id'];
            $payment_status = $txn['payment_status'];
            $custom = $txn['custom'];
            $CareSubscriptionObj = $this->getServiceLocator()->get("CareSubscriptionObj");
            $where = array('hash_code' => $custom);
            $UpdateData = array('transaction_id' => $txn_id, 'payment_status' => $payment_status, 'status' => 1);
            $CareSubscriptionObj->UpdatePaymentDetails($where, $UpdateData);
            $success = 1;
        }
        $whereHistory = array('user_id' => $UserID, 'payment_by' => 2,'status' => 1);
        $result = $CareSubscriptionObj->getBillingHistory($whereHistory);
//        echo '<pre>';
//        print_r($result);die;
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Caregiver', 'success' => $success, 'history' => $result
        ));
    }

    public function notifyAction() {
        $txn = (object) $_POST;
        $plugin = $this->CustomControllerPlugin();
        //$plugin->senInvitationMail('rabban.ahmad@gmail.com', 'notifydata', json_encode($txn));
        exit;
    }

    public function crrstatusAction() {
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $CareGiverTableObj = $this->getServiceLocator()->get("IndividualCaregiverTable");
        $CareSubscriptionObj = $this->getServiceLocator()->get("CareSubscriptionObj");
        $where = array('pms.user_id' => $UserID, 'spm.type' => 2,'pms.payment_by'=>2);
        $results = $CareSubscriptionObj->getLastSubscriptionStatus($where, array('amount', 'end_date'));
        $whereC = array('researcher_id' => $UserID);
        $amount = '';
        $NextBillingDate = '';
        $NoOfUsers = '';
        if (isset($results[0]['amount'])) {
            $amount = $results[0]['amount'];
        }
        if (isset($results[0]['end_date'])) {
            $NextBillingDate = $results[0]['end_date'];
        }
        if (isset($results[0]['NoOfUsers'])) {
            $NoOfUsers = $results[0]['NoOfUsers'];
        }

        $res = $CareSubscriptionObj->getArchivalUserCount($whereC,array('users'));
        $returnArray = array('userCount' => $res, 'amount' => $amount, 'NextBillingDate' => $NextBillingDate,
            'userPlan' => $NoOfUsers);
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Caregiver', 'result' => $returnArray
        ));
    }

    /*     * ***This function is used to delete individual from the caregive dashboard******** */

    public function savepaymentdetailsAction() {
        // $this->init();
        $userId = $this->_sessionObj->userDetails['UserID'];
        $CareGiverTableObj = $this->getServiceLocator()->get("IndividualCaregiverTable");
        $CareSubscriptionObj = $this->getServiceLocator()->get("CareSubscriptionObj");
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        /*         * * Code to validate palan *** */
        $where = array('pms.user_id' => $userId, 'spm.type' => 1);
        $results = $CareSubscriptionObj->getSubscriptionStatus($where, array('amount', 'end_date'));
        $whereC = array('care.caregiver_id' => $userId, 'care.status' => 1);
        $res = $CareGiverTableObj->getCareGiverCount($whereC);
        if ($results) {
            $userPlan = $results[0]['NoOfUsers'];
            $existIndividualUser = $res;
            $currentPlanUsers = $postDataArr['noUser'];
            if ($currentPlanUsers >= $res) {
                $priceId = $postDataArr['priceId'];
                $amount = $postDataArr['amount'];
                $hashCode = $postDataArr['hashCode'];
                $start_date = date("Y-m-d");
                $time = strtotime("+1 month", time());
                $end_date = date("Y-m-d", $time);
                $billing_date = date("Y-m-d");
                $billing_time = date("H:i:s");
                $saveData = array('price_id' => $priceId, 'user_id' => $userId, 'amount' => $amount, 'hash_code' => $hashCode,
                    'payment_by' => 2, 'start_date' => $start_date, 'end_date' => $end_date, 'billing_date' => $billing_date, 'billing_time' => $billing_time);
                $CareSubscriptionObj->insertPaymentDetails($saveData);
                echo 1;
            } else {
                echo 2;
            }
        } else {
            $priceId = $postDataArr['priceId'];
            $amount = $postDataArr['amount'];
            $hashCode = $postDataArr['hashCode'];
            $start_date = date("Y-m-d");
            $time = strtotime("+1 month", time());
            $end_date = date("Y-m-d", $time);
            $billing_date = date("Y-m-d");
            $billing_time = date("H:i:s");
            $saveData = array('price_id' => $priceId, 'user_id' => $userId, 'amount' => $amount, 'hash_code' => $hashCode,
                'start_date' => $start_date, 'end_date' => $end_date, 'billing_date' => $billing_date, 'billing_time' => $billing_time,'payment_by' => 2,);
            $CareSubscriptionObj->insertPaymentDetails($saveData);
            echo 1;
        }
        /*         * **End**** */

        exit;
    }

}
