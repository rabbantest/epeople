<?php

namespace Researcher\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class CronsController extends AbstractActionController {

    protected $_studiesObj;
    protected $_queAnsObj;
    protected $_ArchivalTableObj;
    var $objPHPExcel = array();

    function _getCodeBook($ArchivalRequest) {
        $this->_ArchivalTableObj = $this->getServiceLocator()->get("ResearcherArchivalTable");
        $cols = array(
            "Variable Name" => new \Zend\Db\Sql\Predicate\Expression("var_name"),
            'Question' => new \Zend\Db\Sql\Predicate\Expression("ques_question"),
            'Options Value' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(DISTINCT(opt_value))'),
            'Options Label' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(DISTINCT(opt_option))')
        );

        $QueData = $this->_ArchivalTableObj->getAssQuestion(array('assessment_id' => $ArchivalRequest['assesment_id']), $cols, 1, array(), 1);

        $objPHPExcel = new \PHPExcel();

        $Assessment = $this->_ArchivalTableObj->getAssesment(array('ass_id' => $ArchivalRequest['assesment_id']), array('ass_id', 'ass_name', 'assessment_desc'));
        $jCode = 1;
        $LableArray = array('ass_name' => 'Assessment Name', 'assessment_desc' => 'Assessment Description');
        if (!empty($Assessment[0])) {
            foreach ($Assessment[0] as $Key => $Index) {
                if (in_array($Key, array('ass_name', 'assessment_desc'))) {
                    $iCode = 0;
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($iCode, $jCode)->getFont()->setBold(true);

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $LableArray[$Key]);
                    $iCode++;
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $Index);
                    $jCode++;
                }
            }
            $jCode++;
            $jCode++;
            $jCode++;
        }

        if (!empty($QueData)) {
            foreach ($QueData as $Que) {
                $iCode = 0;
                $j = 1;
                foreach ($Que as $QKey => $QIndex) {
                    if (in_array($QKey, array('Variable Name', 'Question'))) {

                        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($iCode, $jCode)->getFont()->setBold(true);

                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $QKey);

                        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode + 1, $QIndex);
                        $iCode++;
                    }

                    if (in_array($QKey, array('Options Value', 'Options Label'))) {

                        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($iCode, $jCode)->getFont()->setBold(true);

                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode, $QKey);
                        $DataArr = explode(',', $QIndex);
                        $j = 1;
                        foreach ($DataArr as $dataArr) {
                            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($iCode)->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($iCode, $jCode + $j, $dataArr);
                            $j++;
                        }

                        $iCode++;
                    }
                }
                $jCode = $jCode + $j;
                $jCode = $jCode + 3;
            }
        }

        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objPHPExcel->getActiveSheet()->setTitle('Codebook');
        $dir = $_SERVER['DOCUMENT_ROOT'] . "/ArchivalData/";
        if (!file_exists($dir . $ArchivalRequest['id'])) {
            mkdir($dir . $ArchivalRequest['id'], 777);
            chmod($dir . $ArchivalRequest['id'], 0777);
        }
        $objWriter->save($dir . $ArchivalRequest['id'] . '/' . 'Code-Book.xlsx');

        unset($objPHPExcel);
        unset($objWriter);
    }

    function indexAction() {
        $this->_ArchivalTableObj = $this->getServiceLocator()->get("ResearcherArchivalTable");

        ini_set('max_execution_time', 7200);
        $ArchivalRequests = $this->_ArchivalTableObj->getArchivallist(array('status' => 0), array(), FALSE, array(0, 2));

        if (!empty($ArchivalRequests)) {
            foreach ($ArchivalRequests as $ArchivalRequest) {
                $page = 0;
                $count = 0;
                $j = 1;
                echo date('H:i:s') . " Start.<br>";
                $set = 1;
                if (!empty($ArchivalRequest['assesment_id']) && $ArchivalRequest['assesment_id'] != 0) {
                    $this->_getCodeBook($ArchivalRequest);
                }
                $total = $this->_saveExcel($ArchivalRequest, $page, $j, $this->_ArchivalTableObj, $count, $set);
                $filter = new \Zend\Filter\Compress(array(
                    'adapter' => 'Zip',
                    'options' => array(
                        'archive' => $_SERVER['DOCUMENT_ROOT'] . '/ArchivalData/' . $ArchivalRequest['id'] . "/" . $ArchivalRequest['id'] . '.zip'
                )));
                $compress = $filter->filter($_SERVER['DOCUMENT_ROOT'] . '/ArchivalData/' . $ArchivalRequest['id']);
//                $this->_studiesObj = $this->getServiceLocator()->get("StuTable");
//                $this->_studiesObj->updateAdminStudy(array('study_id' => $Study['study_id']), array('passive_status' => 1));

                $ArchivalRequest['creation_date'] = date("Y-m-d", ($ArchivalRequest['creation_date'] / 1000));
                
                $where=array(new \Zend\Db\Sql\Predicate\Expression('start_date<="'.$ArchivalRequest['creation_date'].'" AND end_date>="'.$ArchivalRequest['creation_date'].'"')
                    ,'user_id'=>$ArchivalRequest['UserID'],
                    'payment_by'=>2);
                
                $Subscription=$this->_ArchivalTableObj->getSubscriptionData($where,array('id'));
                
                if ($total > 0 && !empty($Subscription[0]['id'])) {
                    $this->_ArchivalTableObj->updateArchivalData(array('id' => $ArchivalRequest['id']), array('status' => 1));
                    $this->_ArchivalTableObj->insertArchivalUses(array('researcher_id' => $ArchivalRequest['id'],
                        'creation_date' => $ArchivalRequest['creation_date'],
                        'users' => $total,
                        'subscription_id'=>$Subscription[0]['id'],
                    ));
                }

                echo date('H:i:s') . " End<br>";
            }
        }
        die('done');
    }

    function _saveExcel($ArchivalData, $page, $j, $ArchivalTableObj, $count, $set) {
        $source = json_decode($ArchivalData['source'], true);

        if ($count == 0) {
            $this->objPHPExcel[$set] = new \PHPExcel();
        }
//        echo '<pre>';
//        print_r($ArchivalData);die;
        $Dates['source'] = json_decode($ArchivalData['source'], true);


        $SetArray = array(
            'Height' => array('MIN HEIGHT IN CENTIMETER', 'MAX HEIGHT IN CENTIMETER'),
            'Weight' => array('MIN WEIGHT IN POUNDS', 'MAX WEIGHT IN POUNDS'),
            'PhysicalData' => array('TOTAL STEPS', 'TOTAL DURATION', 'TOTAL DISTANCE', 'TOTAL CALORIES BURNED'),
            'Cholesterol' => array('MIN LDL', 'MAX LDL', 'MIN HDL', 'MAX HDL', 'MIN TRIGLYCERIDES', 'MAX TRIGLYCERIDES'),
            'Pressure' => array('MIN SYSTOLIC', 'MAX SYSTOLIC', 'MIN DIASTOLIC', 'MAX DIASTOLIC', 'MIN PULSE', 'MAX PULSE'),
            'Glucose' => array('MIN GLUCOSE', 'MAX GLUCOSE'),
            'SleepRecord' => array('TOTAL SLEEP', 'TOTAL AWAKE', 'TOTAL DEEP', 'TOTAL LIGHT', 'TOTAL RIM'),
            'Temperature' => array('MIN TEMPERATURE', 'MAX TEMPERATURE')
        );

        $ofset = ($ArchivalData['no_of_individual'] > 200) ? 200 : $ArchivalData['no_of_individual'];
        $limit = array(0, $ofset);

        if ($page > 0) {
            $limit = array($limit[1] * $page, $limit[1]);
        }
        $ArchivalData['no_of_individual'] = $ArchivalData['no_of_individual'] - $limit[1];
        $page++;


        if (is_array($Dates['source'])) {
            foreach ($SetArray as $Srckey => $Srcindex) {
                if (!isset($Dates['source'][$Srckey])) {
                    unset($SetArray[$Srckey]);
                }
            }
        }

        //uset($ArchivalData);die;
        $cols = array('varName' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(var_name)'),
            'optionVal' => new \Zend\Db\Sql\Predicate\Expression('GROUP_CONCAT(opt_value)')
            , 'UserID');


        $data = $ArchivalTableObj->getViewData($ArchivalData, array(), '0', array(), $limit, '1', $source);

        if (!empty($data)) {

            $this->objPHPExcel[$set]->setActiveSheetIndex(0);
            foreach ($data as $Data) {

                $i = 0;
                if ($j == 1) {
                    $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, 'UserID');
                }

                $j = $j + 1;
                $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $Data['UserID']);

                if (!empty($ArchivalData['assesment_id']) && $ArchivalData['assesment_id'] != 0) {
                    $newArray = array_combine(explode(',', $Data['varName']), explode(',', $Data['optionVal']));
                    if (!empty($newArray)) {

                        foreach ($newArray as $var => $opt) {
                            $i++;
                            if ($j == 2) {
                                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $var);
                            }
                            $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $opt);
                        }
                    }
                    unset($Data['varName']);
                    unset($Data['optionVal']);
                }

                unset($Data['Email']);
                unset($Data['UserID']);

                foreach ($SetArray as $key => $index) {
                    if (!empty($Data[$key])) {
                        $dataFieldsVal = explode('-', $Data[$key]);
                        $pos = 0;
                        foreach ($index as $field) {
                            $i++;
                            if ($j == 2) {
                                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $field . ' (' . $Dates['source'][$key] . ')');
                            }
                            $val = !empty($dataFieldsVal[$pos]) ? $dataFieldsVal[$pos] : '';
                            $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $val);
                            $pos++;
                        }
                    } else {
                        foreach ($index as $field) {
                            $i++;
                            if ($j == 2) {
                                $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $field . ' (' . $Dates['source'][$key] . ')');
                            }
                            $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                            $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, '');
                        }
                    }
                    unset($Data[$key]);
                }
                foreach ($Data as $key => $index) {
                    $i++;
                    if ($j == 2) {
                        $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, '1', $key);
                    }
                    $this->objPHPExcel[$set]->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
                    $this->objPHPExcel[$set]->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $index);
                }

                $count++;
            }

            if (($count % 10000 == 0 && $count != 0)) {
                // echo $count . '<br>';
                sleep(1);

                $objWriter[$set] = new \PHPExcel_Writer_Excel2007($this->objPHPExcel[$set]);
                $dir = $_SERVER['DOCUMENT_ROOT'] . "/ArchivalData/";
                if (!file_exists($dir . $ArchivalData['id'])) {
                    mkdir($dir . $ArchivalData['id'], 777);
                    chmod($dir . $ArchivalData['id'], 0777);
                }

                $this->objPHPExcel[$set]->getActiveSheet()->setTitle('Archival data');

                $objWriter[$set]->save($dir . $ArchivalData['id'] . '/' . $ArchivalData['from_date'] . '-to-' . $ArchivalData['to_date'] . '-set-' . $set . '.xlsx');

                unset($objWriter[$set]);
                unset($this->objPHPExcel[$set]);
                $j = 1;
                $set++;
                $this->objPHPExcel[$set] = new \PHPExcel();
            }
            sleep(1);

            $this->_saveExcel($ArchivalData, $page, $j, $ArchivalTableObj, $count, $set);
        } else {

            $dir = $_SERVER['DOCUMENT_ROOT'] . "/ArchivalData/";
            if (($count % 10000 != 0)) {
                // echo $count . '<br>';
                sleep(1);

                $objWriter[$set] = new \PHPExcel_Writer_Excel2007($this->objPHPExcel[$set]);

                if (!file_exists($dir . $ArchivalData['id'])) {
                    mkdir($dir . $ArchivalData['id'], 777);
                    chmod($dir . $ArchivalData['id'], 0777);
                }

                $this->objPHPExcel[$set]->getActiveSheet()->setTitle('Archival data');

                $objWriter[$set]->save($dir . $ArchivalData['id'] . '/' . $ArchivalData['from_date'] . '-to-' . $ArchivalData['to_date'] . '-set-' . $set . '.xlsx');

                unset($objWriter[$set]);
                unset($this->objPHPExcel[$set]);
            }
        }

        return $count;
    }

}
