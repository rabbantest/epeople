<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ROOT CONTROLLER FOR CAREGIVER APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: RABBAN AHMAD.
 * CREATOR: 27/3/15
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Researcher\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Researcher\Form\ResearcherLogin;
use Researcher\Form\ResearcherLoginValidator;
use Researcher\Form\BasicInfo;
use Researcher\Form\BasicInfoValidator;

class ArchivalController extends AbstractActionController {

    protected $_sessionObj;
    protected $profileinfo;

    public function __construct() {
        //parent::__construct();
        $this->_sessionObj = new Container('Researcher');
    }

    public function checkAvailabilityAction() {
        $getData = $_GET;
        $creation_date = date('Y-m-d');
        $userId = $this->_sessionObj->userDetails['UserID'];
        $ArchivalTableObj = $this->getServiceLocator()->get("ResearcherArchivalTable");
        $where = array(new \Zend\Db\Sql\Predicate\Expression('start_date<="' . $creation_date . '" AND end_date>="' . $creation_date . '"')
            , 'user_id' => $userId,
            'payment_by' => 2);

        $Subscription = $ArchivalTableObj->getSubscriptionData($where, array('id', 'start_date', 'end_date'), array('price_id'), 1);

        $totalUser = isset($Subscription[0]['user']) ? $Subscription[0]['user'] : 0;

        if ($totalUser > 0) {
            $start_date = round(strtotime($Subscription[0]['start_date']) * 1000);
            $end_date = round(strtotime($Subscription[0]['end_date']) * 1000);

            $where = array(new \Zend\Db\Sql\Predicate\Expression('creation_date>="' . $start_date . '" AND creation_date<="' . $end_date . '"')
                , 'UserID' => $userId);
            $RequestedUsers = $ArchivalTableObj->getArchivallist($where, array('no_of_individual'));
            $RequestedUsers = isset($RequestedUsers[0]['no_of_individual']) ? $RequestedUsers[0]['no_of_individual'] : 0;
            $RequestedUsers = $RequestedUsers + $getData['no_of_individuals'];

            if ($RequestedUsers <= $totalUser) {
                echo 'true';
            } else {
                echo 'false';
            }
        } else {
            echo 'false';
        }
        die;
    }
    
    
    

    public function indexAction() {
        if ($this->_sessionObj->userDetails['DomainID'] != 1) {
            return $this->redirect()->toUrl('/researcher-panelmember');
        }        
        $ResAricTableObj = $this->getServiceLocator()->get('ResearcherArchivalTable');
        $CountryTableObj = $this->getServiceLocator()->get("CountryTable");
        $UserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        
        $userId = $this->_sessionObj->userDetails['UserID'];
        $domain_id = $this->_sessionObj->userDetails['DomainID'];

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $error = 0;
            if (empty($postDataArr['no_of_individuals'])) {
                $error = 1;
                $message = "Please enter  number of indivuduals.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }

            if ($error == 0) {
                $saveArray = array(
                    'UserID' => $userId,
                    'domain_id' => $domain_id,
                    'assesment_id' => isset($postDataArr['assessment']) ? $postDataArr['assessment'] : '',
                    'from_date' => isset($postDataArr['from_date']) ? date("Y-m-d", strtotime($postDataArr['from_date'])) : '',
                    'to_date' => isset($postDataArr['to_date']) ? date("Y-m-d", strtotime($postDataArr['to_date'])) : '',
                    'gender' => isset($postDataArr['gender']) ? $postDataArr['gender'] : '',
                    'no_of_individual' => isset($postDataArr['no_of_individuals']) ? $postDataArr['no_of_individuals'] : '',
                    'education' => isset($postDataArr['education']) ? $postDataArr['education'] : '',
                    'ethinicity' => isset($postDataArr['ethinicity']) ? $postDataArr['ethinicity'] : '',
                    'country' => isset($postDataArr['country']) ? $postDataArr['country'] : '',
                    'state' => isset($postDataArr['state']) ? $postDataArr['state'] : '',
                    'county' => isset($postDataArr['county']) ? $postDataArr['county'] : '',
                    'creation_date' => round(microtime(true) * 1000)
                );
                //save source in json
                foreach ($postDataArr['element'] as $key => $index) {
                    if (!empty($postDataArr['sourceType'][$key])) {
                        $saveArray['source'][$postDataArr['element'][$key]] = $postDataArr['sourceType'][$key];
                    }
                }
                $saveArray['source'] = json_encode($saveArray['source']);

                $insData = $ResAricTableObj->insertArchival($saveArray);
                if ($insData) {
                    $message = "Request successfully send.";
                    $this->flashMessenger()->addMessage(array('success' => $message));
                } else {
                    $message = "Some errore occured , please try again later.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                }
            }
        }

        //get assesment list 
        $where = array('is_publish' => 1, 'ass_status' => 1);
        $coloumn = array('ass_id', 'ass_name');
        $lookingfor = 'ass_name ASC';
        $assesmentList = $ResAricTableObj->getAssesment($where, $coloumn, $lookingfor);
        //get country list 
        $countryArr = $CountryTableObj->getCountry();
        //get education list
        $EducationcArr = $UserTableObj->getEducation();
        //get ethinicity list 
       // $EthnicityArr = $UserTableObj->getEthnicity();
        //get validic source list
        $ValidicsourceArray = array();

        $SetArray = array('Height', 'Weight', 'PhysicalData', 'Cholesterol', 'Pressure', 'Glucose', 'SleepRecord', 'Temperature');

        foreach ($SetArray as $setElement) {
            $Validicsource[$setElement] = $ResAricTableObj->getvalidicAppSource(array(), array('source_name', 'source'), array('allocation' => $setElement));
            $ValidicsourceArray[$setElement] = array_combine(array_column($Validicsource[$setElement], 'source'), array_column($Validicsource[$setElement], 'source_name'));
        }
        
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Researcher Arichival Data',
                    'assesmentList' => $assesmentList,
                    'country' => $countryArr,
                    'education' => $EducationcArr,
                    //'ethnicity' => $EthnicityArr,
                    'SetArray' => $SetArray,
                    'validic_source' => $ValidicsourceArray
        ));
    }

    function listarchivalAction() {
        $ResAricTableObj = $this->getServiceLocator()->get('ResearcherArchivalTable');
        $userId = $this->_sessionObj->userDetails['UserID'];
        $domain_id = $this->_sessionObj->userDetails['DomainID'];
        $whereArray = array("UserID" => $userId, "domain_id" => $domain_id);
        $archData = $ResAricTableObj->getArchivallist($whereArray);
//        echo '<pre>';
//        print_r($archData);
//        exit;
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Researcher Arichival Data',
                    'archData' => $archData
        ));
    }

    function archivaldataAction() {
        $ResAricTableObj = $this->getServiceLocator()->get('ResearcherArchivalTable');
        $userId = $this->_sessionObj->userDetails['UserID'];
        $domain_id = $this->_sessionObj->userDetails['DomainID'];
        $whereArray = array("UserID" => $userId, "domain_id" => $domain_id, "status" => 0);
        $archData = $ResAricTableObj->getArchivallist($whereArray);
        $hmwhereProfile = '';
        $hmwhereuser = '';
        $hspwhere = '';
        foreach ($archData as $arData) {

            if (!empty($arData['gender'])) {
                $gender = $arData['gender'];
                $hmwhereuser .=" AND hu.Gender='$gender' ";
            }
            if (!empty($arData['gender'])) {
                $gender = $arData['gender'];
                $hmwhereuser .=" AND hu.Gender='$gender' ";
            }

            if (!empty($arData['education'])) {
                $education = $arData['education'];
                $hmwhereProfile .=" AND hpi.education_degree='$education' ";
            }
            if (!empty($arData['ethinicity'])) {
                $ethinicity = $arData['ethinicity'];
                $hmwhereProfile .=" AND hpi.ethinicity='$ethinicity' ";
            }
            if (!empty($arData['assesment_id'])) {
                $assesment_id = $arData['assesment_id'];
                $hspwhere = "  hsp.ass_id='$assesment_id' AND ";
            }


            $st = "select hsp.UserID from hm_selfassess_participation as hsp";
            if (!empty($hmwhereuser)) {
                $st .= " LEFT JOIN  hm_users as hu ON hsp.UserID = hu.UserID  $hmwhereuser";
            }
            if (!empty($hmwhereProfile)) {
                $st .= " LEFT JOIN  hm_profile_info hpi ON hsp.UserID = hpi.user_id $hmwhereProfile ";
            }
            $st .= "where $hspwhere  hsp.part_status=0";
            $data[] = $ResAricTableObj->getUserlistArchival($st);
        }
        echo '<pre>';
        print_r($data);
        exit;

//        echo '<pre>';
//        print_r($archData);
        exit;
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Researcher Arichival Data',
        ));
    }

}
