<?php
/* * ***********************
 * PAGE: USE TO MANAGE THE ROOT CONTROLLER FOR CAREGIVER APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: RABBAN AHMAD.
 * CREATOR: 27/3/15
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Researcher\Controller;

use Caregiver\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class ResearcherAjaxController extends BaseController {

    public function init() {
        $this->_sessionObj = new Container('Researcher');
        $this->IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
    }

    public function addemailstoarrayAction() {
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $email = $postDataArr['email'];
        ?>
        <li><input type="hidden" name="emailArr[]" value="<?= $email ?>"><span><?= $email ?></span><a href="javascript:void(0);" class="daleteEmailID">X</a></li>
        <?php
        exit;
    }

    public function senduserinviteAction() {
        $this->init();
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $emailArr = $postDataArr['emailArr'];
        $yourtxt = $postDataArr['yourtxt'];
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $UserName = $this->_sessionObj->userDetails['FirstName'];
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $plugin = $this->CustomControllerPlugin();
        $emailExistArr = array();
        $emailNotExistArr = array();
        if (count($emailArr)) {
            foreach ($emailArr as $key => $email) {
                $where = array("Email" => $email, 'Verified' => 1);
                $UserExist = $IndUserTableObj->getUserExist($where);
                if ($UserExist) {
                    //$emailExistArr[]=$email;
                    $CareGiverTableObj = $this->getServiceLocator()->get("IndividualCaregiverTable");
                    $hash = md5(time() . rand(1, 100));
                    $CargiverDataArr = array("UserID" => $UserExist[0]['UserID'], 'caregiver_id' => $UserID, 'hash' => $hash, 'creation_date' => date("Y-m-d"));
                    $CareGiverTableObj->saveCareGiver($CargiverDataArr);
                    $uname = ucfirst($postDataArr['user_name']);
                    $url = "http://" . $_SERVER['HTTP_HOST'] . "/individual/index/respondcaregiver?hash=$hash&reply=1";
                    $Rjecturl = "http://" . $_SERVER['HTTP_HOST'] . "/individual/index/respondcaregiver?hash=$hash&reply=0";
                    $message = "Hi,<br>";
                    $message .= $UserName . " would like to take care of you.";
                    $message .= "<br><br>If you want to accept click on <a href='$url' target='_blank'>accept </a>.";
                    $message .= "<br><br>OR";
                    $message .= "<br><br>If you want to reject click on <a href='$Rjecturl' target='_blank'>reject </a>.";
                    $message .="<br><br>Thank you<br>";
                    $message .="<br><br>Regards<br>Healthmetrix.io Team";
                    $subject = "Caregiver Request";
                    $plugin->senInvitationMail($email, $subject, $message);
                } else {

                    //$emailNotExistArr[]=$email;
                    $TempCareTableObj = $this->getServiceLocator()->get("TempCareUsers");
                    $TempCargiverData = array("email" => $email, 'caregiver_id' => $UserID, 'creation_date' => date("Y-m-d"));
                    $TempCareTableObj->saveTempCareGiverUser($TempCargiverData);
                    $url = "http://" . $_SERVER['HTTP_HOST'] . "/individual/";
                    $message = "Hi,<br>";
                    $message .= $UserName . " would like invite you to join the Healthmetrix.co.";
                    $message .= "<br><br>If you want to join please <a href='$url' target='_blank'>click</a> here";
                    $message .= "<br><br>After signup/login you will seen the caregiver request in setting section.";
                    $message .="<br><br>Thank you<br>";
                    $message .="<br><br>Regards<br>Healthmetrix.io Team";
                    $subject = "Join Healthmetrix";
                    $plugin->senInvitationMail($email, $subject, $message);
                }
            }
        }
        exit;
    }

    /*
      @ This function is used to send an email to Individual
     */

    public function sendanemailAction() {
        $this->init();
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $subject = $postDataArr['subject'];
        $email = $postDataArr['email'];
        $body = $postDataArr['body'];
        $caregiver_id = $this->_sessionObj->userDetails['UserID'];
        $UserName = $this->_sessionObj->userDetails['FirstName'];
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $plugin = $this->CustomControllerPlugin();
        $emailExistArr = array();
        $emailNotExistArr = array();

        if ($email && $body && $subject) {
            /* $url      = "http://".$_SERVER['HTTP_HOST']."/individual/index/respondcaregiver?hash=$hash&reply=1";
              $Rjecturl      = "http://".$_SERVER['HTTP_HOST']."/individual/index/respondcaregiver?hash=$hash&reply=0";
              $message  = "Hi,<br>";
              $message .= $UserName." would like to take care of you.";
              $message .= "<br><br>If you want to accept click on <a href='$url' target='_blank'>accept </a>.";
              $message .= "<br><br>OR";
              $message .= "<br><br>If you want to reject click on <a href='$Rjecturl' target='_blank'>reject </a>.";
              $message .="<br><br>Thank you<br>";
              $message .="<br><br>Regards<br>Healthmetrix.io Team";
              $subject="Caregiver Request"; */
            $plugin->senInvitationMail($email, $subject, $body);
            echo "1";
        }
        exit;
    }

    public function sendinviteAction() {
        $this->init();
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $userid = $postDataArr['userid'];
        $email = $postDataArr['email'];
        $caregiver_id = $this->_sessionObj->userDetails['UserID'];
        $UserName = $this->_sessionObj->userDetails['FirstName'];
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $plugin = $this->CustomControllerPlugin();
        $emailExistArr = array();
        $emailNotExistArr = array();
        if ($email) {
            $CareGiverTableObj = $this->getServiceLocator()->get("IndividualCaregiverTable");
            $hash = md5(time() . rand(1, 100));
            $CargiverDataArr = array("UserID" => $userid, 'caregiver_id' => $caregiver_id, 'hash' => $hash, 'creation_date' => date("Y-m-d"));
            $CareGiverTableObj->saveCareGiver($CargiverDataArr);
            $url = "http://" . $_SERVER['HTTP_HOST'] . "/individual/index/respondcaregiver?hash=$hash&reply=1";
            $Rjecturl = "http://" . $_SERVER['HTTP_HOST'] . "/individual/index/respondcaregiver?hash=$hash&reply=0";
            $message = "Hi,<br>";
            $message .= $UserName . " would like to take care of you.";
            $message .= "<br><br>If you want to accept click on <a href='$url' target='_blank'>accept </a>.";
            $message .= "<br><br>OR";
            $message .= "<br><br>If you want to reject click on <a href='$Rjecturl' target='_blank'>reject </a>.";
            $message .="<br><br>Thank you<br>";
            $message .="<br><br>Regards<br>Healthmetrix.io Team";
            $subject = "Caregiver Request";
            $plugin->senInvitationMail($email, $subject, $message);
        }
        exit;
    }

    /*     * ********
     * Action: Used for get States with Ajax.
     * @author: Rabban Ahmad
     * Created Date: 15-12-2014.
     * *** */

    public function getstatesdataAction() {
        $this->init();
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $IndStateTableObj = $this->getServiceLocator()->get("IndStateTable");
        $where = array("country_id" => $postDataArr['countryId']);
        //die('out');
        $states = $IndStateTableObj->getStates($where);
        ?>				

        <select title="Select Your State" class="select" name="state" onchange="return getCounty(this.value)">
            <?php if (count($states) > 0) { ?>
                <option value="">Select State</option>
                <?php foreach ($states as $val) { ?>
                    <option value="<?php echo $val['state_id']; ?>"><?php echo $val['state_name']; ?></option>
                <?php } ?>
            <?php } else { ?>
                <option value="">No State</option>
        <?php } ?>
        </select>

        <?php
        exit;
    }

    function getethnicitydataAction() {
        $this->init();
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $UserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $where = array("country_id" => $postDataArr['countryId']);
        //die('out');
        // print_r($where);
        $ethnicity = $UserTableObj->getEthnicity($where);
        //  echo '<pre>';
        //   print_r($ethnicity);
        // exit;
        ?>				

        <select title="Select Ethnicity" class="select" name="ethnicity" id="ethnicity">
            <?php if (count($ethnicity) > 0) { ?>
                <option value="">select ethnicity</option>
                <?php foreach ($ethnicity as $val) { ?>
                    <option value="<?php echo $val['ethnicity_id']; ?>"><?php echo $val['ethnicity_name']; ?></option>
                <?php } ?>
            <?php } else { ?>
                <option value="">No Ethnicity</option>
            <?php } ?>
        </select>

        <?php
        exit;
    }

    /*     * ********
     * Action: Used for get County with Ajax.
     * @author: Rabban Ahmad
     * Created Date: 15-12-2014.
     * *** */

    public function getcountyAction() {
        $this->init();
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $CountyTableObj = $this->getServiceLocator()->get("CountyTable");
        $where = array("state_id" => $postDataArr['stateId']);
        $county = $CountyTableObj->getCounty($where);
        ?>				

        <select title="Select Your County" class="select" name="county">
            <?php if (count($county) > 0) { ?>
                <option value="">Select County</option>
                <?php foreach ($county as $val) { ?>
                    <option value="<?php echo $val['county_id']; ?>"><?php echo $val['county_name']; ?></option>
                <?php } ?>
            <?php } else { ?>
                <option>No County</option>
            <?php } ?>
        </select>

        <?php
        exit;
    }

    /*     * This function is used to udpate the account profile staae* */

    public function udpateprofilestageAction() {
        $this->init();
        $userId = $this->_sessionObj->userDetails['UserID'];
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $dataArr = array('ProfileCompletedStage' => 3, 'ProfileCompleted' => 1);
        $this->IndUserTableObj->updateUser(array('UserID' => $userId), $dataArr);
        exit();
    }

    /*     * ********
     * Action: change password with Ajax
     * @author: Rabban Ahmad
     * Created Date: 10-12-2014.
     * *** */

    public function changepasswordAction() {
        $this->init();
        $userId = $this->_sessionObj->userDetails['UserID'];
        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $whereArr = array("UserID" => $userId);
        $res = $IndUserTableObj->getUser($whereArr);
        if ($res[0]['Password'] == md5($postDataArr['oldPass'])) {
            $IndUserTableObj->updateUser(array('UserID' => $userId), array('Password' => md5($postDataArr['newPass'])));
            echo "1";
            die;
        } else {
            echo "2";
            die;
        }
    }

    public function notLoginAction() {
        $this->init();
        $link = '';
        if (!$this->_sessionObj->isResearcherlogin) {
            $link = "/researcher-login";
        }
        echo $link;
        exit;
    }

}
