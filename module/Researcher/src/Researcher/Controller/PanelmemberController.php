<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ROOT CONTROLLER FOR CAREGIVER APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: RABBAN AHMAD.
 * CREATOR: 23/6/15
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Researcher\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class PanelmemberController extends AbstractActionController {

    protected $_sessionObj;
    protected $profileinfo;

    public function init() {
        $this->_sessionObj = new Container('Researcher');
        if ($this->_sessionObj->userDetails['DomainID'] == 1) {
            return $this->redirect()->toUrl('researcher-archival');
        }
    }

    public function __construct() {
        //parent::__construct();
    }

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->isResearcherlogin) {
            return $this->redirect()->toUrl('/researcher-login');
        }
        $userId = $this->_sessionObj->userDetails['UserID'];
        $DomainID = $this->_sessionObj->userDetails['DomainID'];
        $ResStuTableObj = $this->getServiceLocator()->get('ResStuTable');
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $where = array('pass_researcher_id' => $userId,'created_by'=>$DomainID);
        $coloumn = array('source');
        $returnArr=array();
        $panelMemberlist = $ResStuTableObj->getUserListParticipatedDomainStudy($where, $coloumn, $userId);
        if ($panelMemberlist) {
            foreach ($panelMemberlist as $key => $val) {
                $result['Gender'] = $val['Gender'];
                $result['UserID'] = $val['UserID'];
                $result['FirstName'] = $val['FirstName'];
                $result['LastName'] = $val['LastName'];
                $result['Image'] = $val['Image'];
                $result['Email'] = $val['Email'];
                $result['Dob'] = $val['Dob'];
                $result['state_name'] = $val['state_name'];
                $result['country_name'] = $val['country_name'];
                $result['ShowsData'] = $ResStuTableObj->ShowDataBasedOnSuperThreadhold($val['UserID'], $val['Gender'], $CusConPlug->GetAge($val['Dob']));
                $returnArr[] = $result;
            }
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Panel member', 'IndividualUserArr' => $returnArr, 'custom_plugin' => $CusConPlug,
        ));
    }

    public function beaconlistAction() {
        $this->init();
        $caregiver_id = $this->_sessionObj->userDetails['UserID']; // caregiver id
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $CareBeaconTableObj = $this->getServiceLocator()->get("CareBeacon");
        $CareAssBecTabObj = $this->getServiceLocator()->get('CareBeaconAssoc');
        $CareBeDataTabObj = $this->getServiceLocator()->get('CareBeaconData');
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin   
        // encryptdata
        $userid = $CusConPlug->decryptdata($this->params('id'));
        $whereArray = array('UserID' => $userid);
        $coloumArray = array("UserID", "FirstName", "LastName", "Gender", "Dob", "Image", "short_description");
        $userDetails = $IndUserTableObj->getUser($whereArray, $coloumArray);
        // $associateStatus=0; // 0 means no permission to edit 

        $wherebecon = array(' becon.UserID' => $userid,);
        $beconList = $CareBeaconTableObj->getUserBeacon($wherebecon);
        $beconData = array();
        if (count($beconList) > 0) {
            $i = 0;
            //beacon data 
            $current_date = date("Y-m-d");
            $beconData = $CareBeDataTabObj->getBeaconData(array("bd.UserID" => $userid, "date" => $current_date));
            foreach ($beconList as $bvalue) {
                //   $beconData[] =  
                $AssId = $CareAssBecTabObj->getAssocBeacon(array("beacon_id" => $bvalue['id'], 'caregiver_id' => $caregiver_id));
                if (count($AssId) > 0) {
                    $beconList[$i]['associateStatus'] = 1;
                    $beconList[$i]['ass_beacon_id'] = $AssId[0]['beacon_id']; //assocate becone id 
                } else {
                    $beconList[$i]['associateStatus'] = 0;  //0 means no permission to edit 
                    $beconList[$i]['ass_beacon_id'] = ''; //assocate becone id
                }
                $i++;
            }
        }
        /*         * ********Code to update unread notification************** */
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
        $whereUpdate = array('receiver_id' => $caregiver_id, 'sender_id' => $userid, 'type' => array('7', '8'));
        $updateData = array('is_read' => 1);
        $NotificationTableObj->updateNotification($whereUpdate, $updateData);
        /*         * ********End Code to update unread notification************** */
        $sendArr = array(
            'pageTitle' => "View Beacons",
            'session' => $this->_sessionObj->isResearcherlogin,
            'userDetails' => $userDetails,
            'CusConPlug' => $CusConPlug,
            'beconList' => $beconList,
            'beconData' => $beconData
        );
        return new ViewModel($sendArr);
    }

    public function trackmoveAction() {
        $this->init();
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin   
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $IndPhyTableObj = $this->getServiceLocator()->get("IndividualPhysicalActivityTable");
        $caregiver_id = $this->_sessionObj->userDetails['UserID'];  // caregiveri id 
        $userid = $CusConPlug->decryptdata($this->params('id')); //user id
        //user details
        $whereArray = array('UserID' => $userid);
        $coloumArray = array("UserID", "FirstName", "LastName", "Gender", "Dob", "Image", "short_description");
        $userDetails = $IndUserTableObj->getUser($whereArray, $coloumArray);
        //moves data 
        $whereMovesArray = array('UserID' => $userid, 'ExerciseRecorddate' => date('Y-m-d'), 'source' => 'movesapp');
        $movesData = $IndPhyTableObj->getPhysicalRecords($whereMovesArray);
        $i = 0;
        $coloumn = array('color');
        foreach ($movesData as $mvalue) {
            $colordata = $IndPhyTableObj->getActivityColor(array('activity' => $mvalue['ActivityType']), $coloumn);
            $colorname = (isset($colordata[0]['color']) ? $colordata[0]['color'] : '');
            if (empty($colorname)) {
                $colorname = '#e1e1e1';
            }
            $movesData[$i]['color'] = $colorname;
            $i++;
        }

        $sendArr = array(
            'pageTitle' => "Track  Moves",
            'session' => $this->_sessionObj->isResearcherlogin,
            'userDetails' => $userDetails,
            'CusConPlug' => $CusConPlug,
            'movesData' => $movesData
        );
        return new ViewModel($sendArr);
    }

    /*     * ********
     * Action: LIST DATA OF FOLLOW IN GPS USER DATA 
     * @author: SHIVENDRA SUMAN
     * Created Date: 14-04-2015.
     * *** */

    public function followongpsAction() {
        $this->init();
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin   
        $CareGeofenceTableObj = $this->getServiceLocator()->get("CareGeofence");
        $caregiver_id = $this->_sessionObj->userDetails['UserID'];  // caregiveri id 
        $userid = $CusConPlug->decryptdata($this->params('id'));

        //user details
        $data_date = date("Y-m-d");
        $whereArray = array('UserID' => $userid);
        $coloumArray = array("UserID", "FirstName", "LastName", "Gender", "Dob", "Image", "short_description");
        $userDetails = $IndUserTableObj->getUser($whereArray, $coloumArray);
        //geofence list
        $whereArrayGps = array("caregiver_id" => $caregiver_id, "UserID" => $userid, "status" => 1);
        $geofenceData = $CareGeofenceTableObj->getGpsData($whereArrayGps);
        $wgereGpsArr = array("gpsudata.UserID" => $userid, "date" => $data_date);
        $geofenceUserData = $CareGeofenceTableObj->getGpsUserData($wgereGpsArr);
        /*         * ********Code to update unread notification************** */
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
        $whereUpdate = array('receiver_id' => $caregiver_id, 'sender_id' => $userid, 'type' => 6);
        $updateData = array('is_read' => 1);
        $NotificationTableObj->updateNotification($whereUpdate, $updateData);
        /*         * ********End Code to update unread notification************** */
        $sendArr = array(
            'pageTitle' => "Geofence Details",
            'session' => $this->_sessionObj->isResearcherlogin,
            'userDetails' => $userDetails,
            'CusConPlug' => $CusConPlug,
            'geofenceData' => $geofenceData,
            'geofenceUserData' => $geofenceUserData
        );
        return new ViewModel($sendArr);
    }

    public function listbeacondataAction() {
        $this->init();
        $CareBeDataTabObj = $this->getServiceLocator()->get('CareBeaconData');
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin 
        $data_date = date("Y-m-d");
        $request = $this->getRequest();
        $beconData = array();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $user_id = isset($postDataArr['user_id']) ? $CusConPlug->decryptdata($postDataArr['user_id']) : '';
            $beacon_id = isset($postDataArr['beacon_id']) ? $CusConPlug->decryptdata($postDataArr['beacon_id']) : '';
            $data_date = (isset($postDataArr['data_date']) && !empty($postDataArr['data_date'])) ? date("Y-m-d", strtotime($postDataArr['data_date'])) : $data_date;
            if (empty($beacon_id)) {
                $whereArray = array("bd.UserID" => $user_id, "date" => $data_date);
            } else {
                $whereArray = array("bd.beacon_id" => $beacon_id, "bd.UserID" => $user_id, "date" => $data_date);
            }

            $beconData = $CareBeDataTabObj->getBeaconData($whereArray);
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        return $viewModel->setVariables(array(
                    'pageTitle' => "View Beacons",
                    'session' => $this->_sessionObj->isResearcherlogin,
                    'beconData' => $beconData,
                    'CusConPlug' => $CusConPlug,
                        // 'userid' => $CusConPlug->encryptdata($user_id),
        ));
    }
     public function movesgraphAction() {
        $this->init();
        // $this->notLogin();
        //  $userId = $this->_sessionObj->userDetails['UserID'];
        $request = $this->getRequest();
        $error = 1;
        $link = "";
        if ($this->_sessionObj->isResearcherlogin) {
            if ($request->isPost()) {
                $CusConPlug = $this->CustomControllerPlugin();  // custome plugin  
                $IndPhyTableObj = $this->getServiceLocator()->get("IndividualPhysicalActivityTable");
                $postDataArr = $request->getPost()->toArray();
                $coloumns = array();
                $date = $postDataArr['date'];
                $userId = $CusConPlug->decryptdata($postDataArr['userId']);
                $whereMovesArray = array('UserID' => $userId, 'ExerciseRecorddate' => $date, 'source' => 'movesapp');
                $movesData = $IndPhyTableObj->getPhysicalRecords($whereMovesArray, $coloumns, "ExerciseRecorddate DESC, ExerciseRecordtime ASC");
                // print_r($movesData);exit;
                $i = 0;
                $coloumn = array('color');
                foreach ($movesData as $mvalue) {
                    $colordata = $IndPhyTableObj->getActivityColor(array('activity' => $mvalue['ActivityType']), $coloumn);
                    $colorname = (isset($colordata[0]['color']) ? $colordata[0]['color'] : '');
                    if (empty($colorname)) {
                        $colorname = '#e1e1e1';
                    }
                    $movesData[$i]['color'] = $colorname;
                    $i++;
                }

                if (count($movesData) > 0) {
                    $form = '<ul class="commutations_graph" id="movesgraph">';
                    foreach ($movesData as $movvalue) {
                        if (!empty($movvalue['Duration'])) {
                            $color = $movvalue['color'];
                            $datemoves = date('g:i A', strtotime($movvalue['ExerciseRecordtime']));
                            $Activitytype_and_duration = ucfirst($movvalue['ActivityType']) . " " . $movvalue['Duration'] . 'Min';
                            $form.= '<li class="" style="border-left:' . $color . ' 10px solid;">
                                <span class="time">' . $datemoves . '</span>
                                <span class="task">' . $Activitytype_and_duration . '</span>
                                <span class="bullet"></span>
                               </li>';
                        }
                    }
                    $form.= '</ul>';
                } else {

                    $form = '<div class="errorMsg">
		<span class="ui_icon error"></span>Sorry no record found. </div>';
                }

                $form;
            }
        } else {
            $error = 1;
            $form = "some error occured";
            //$link = $this->serverUrl()->toUrl('/individual/index/login');
            $link = "/individual/index/login";
        }
        echo json_encode(array("error" => $error, "msg" => $form, "link" => $link));
        exit;
    }
    
    
     public function followongpslistAction() {
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin   
        $CareGeofenceTableObj = $this->getServiceLocator()->get("CareGeofence");
        $caregiver_id = $this->_sessionObj->userDetails['UserID'];  // caregiveri id 
        $data_date = date("Y-m-d");
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $user_id = isset($postDataArr['user_id']) ? $CusConPlug->decryptdata($postDataArr['user_id']) : '';
            $geofence_id = isset($postDataArr['geofence_id']) ? $CusConPlug->decryptdata($postDataArr['geofence_id']) : '';
            $data_date = (isset($postDataArr['data_date']) && !empty($postDataArr['data_date'])) ? date("Y-m-d", strtotime($postDataArr['data_date'])) : $data_date;
            $wgereGpsArr = array('geofence_id' => $geofence_id, "gpsudata.UserID" => $user_id, 'date' => $data_date);
            $geofenceUserData = $CareGeofenceTableObj->getGpsUserData($wgereGpsArr);
        }

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        return $viewModel->setVariables(array(
                    'pageTitle' => "View Beacons",
                    'session' => $this->_sessionObj->isCaregiverlogin,
                    'geofenceUserData' => $geofenceUserData,
                    'CusConPlug' => $CusConPlug,
        ));
    }


}
