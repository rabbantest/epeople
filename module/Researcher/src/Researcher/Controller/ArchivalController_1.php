<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ROOT CONTROLLER FOR CAREGIVER APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: RABBAN AHMAD.
 * CREATOR: 27/3/15
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Researcher\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Researcher\Form\ResearcherLogin;
use Researcher\Form\ResearcherLoginValidator;
use Researcher\Form\BasicInfo;
use Researcher\Form\BasicInfoValidator;


class ArchivalController extends AbstractActionController {

    protected $_sessionObj;
    protected $profileinfo;

    public function __construct() {
        //parent::__construct();
        $this->_sessionObj = new Container('Researcher');
    }

    public function indexAction() {
    $ResAricTableObj = $this->getServiceLocator()->get('ResearcherArchivalTable');
    $CountryTableObj = $this->getServiceLocator()->get("CountryTable");
    $UserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");    
        $userId = $this->_sessionObj->userDetails['UserID'];
        $domain_id = $this->_sessionObj->userDetails['DomainID'];
        
    $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
//            echo '<pre>';
//            print_r($postDataArr);
//            exit;
            $error=0;
            if (empty($postDataArr['assessment'])) {
                $message = "Please select assesment.";
                $error=1;
                $this->flashMessenger()->addMessage(array('error' => $message));
              }
              
              if (empty($postDataArr['no_of_individuals'])) {
                  $error=1;
                $message = "Please enter  number of indivuduals.";
                $this->flashMessenger()->addMessage(array('error' => $message));
              }
              
              if($error==0){
                  $source_height ='';  $source_weight=''; $source_steps=''; $source_sleep='';
                  $source_cholesterol = ''; $source_bloodpressure=''; $source_fastingglucose = '';
                  $source_temperature = '';
                  
                    if (isset($postDataArr['height_meas']) && $postDataArr['height_meas']=='on') {
                       $source_height = $postDataArr['source_height'];
                   }
                   if (isset($postDataArr['weight_meas']) && $postDataArr['weight_meas']=='on') {
                       $source_weight = $postDataArr['source_weight'];
                   }
                    if (isset($postDataArr['steps_meas']) && $postDataArr['steps_meas']=='on') {
                       $source_steps = $postDataArr['source_steps'];
                   }
                   if (isset($postDataArr['sleep_meas']) && $postDataArr['sleep_meas']=='on') {
                       $source_sleep = $postDataArr['source_sleep'];
                   }
                    if (isset($postDataArr['cholesterol_meas']) && $postDataArr['cholesterol_meas']=='on') {
                       $source_cholesterol = $postDataArr['source_cholesterol'];
                   }
                   if (isset($postDataArr['bloodpressure_meas']) && $postDataArr['bloodpressure_meas']=='on') {
                       $source_bloodpressure = $postDataArr['source_bloodpressure'];
                   }
                    if (isset($postDataArr['fastingglucose_meas']) && $postDataArr['fastingglucose_meas']=='on') {
                       $source_fastingglucose = $postDataArr['source_fastingglucose'];
                   }
                   
                    if (isset($postDataArr['temperature_meas']) && $postDataArr['temperature_meas']=='on') {
                       $source_temperature = $postDataArr['source_temperature'];
                   }
                   
                   
            $saveArray = array(
                'UserID' => $userId,
                'domain_id' => $domain_id,
                'assesment_id' => isset($postDataArr['assessment'])? $postDataArr['assessment'] : '',
                'from_date' => isset($postDataArr['from_date'])? date("Y-m-d",strtotime($postDataArr['from_date'])) : '',
                'to_date' => isset($postDataArr['to_date'])? date("Y-m-d",strtotime($postDataArr['to_date'])) : '',
                'gender' => isset($postDataArr['gender'])? $postDataArr['gender'] : '',
                'no_of_individual' => isset($postDataArr['no_of_individuals'])? $postDataArr['no_of_individuals'] : '',
                'education' => isset($postDataArr['education'])? $postDataArr['education'] : '',
                'ethinicity' => isset($postDataArr['ethinicity'])? $postDataArr['ethinicity'] : '',
                'country' => isset($postDataArr['country'])? $postDataArr['country'] : '',
                'state' => isset($postDataArr['state'])? $postDataArr['state'] : '',
                'county' => isset($postDataArr['county'])? $postDataArr['county'] : '',
               // 'source' => isset($postDataArr['source'])? $postDataArr['source'] : '',
                'source_height' => $source_height,
                'source_weight' => $source_weight,
                'source_steps' => $source_steps,
                'source_sleep' => $source_sleep,
                'source_cholesterol' => $source_cholesterol,
                'source_bloodpressure' => $source_bloodpressure,
                'source_fastingglucose' => $source_fastingglucose,
                'source_temperature' => $source_temperature,
                'creation_date'=> round(microtime(true) * 1000)  
            );
            $insData = $ResAricTableObj->insertArchival($saveArray);
              if ($insData) {
                $message = "Request successfully send.";
                $this->flashMessenger()->addMessage(array('success' => $message));
            } else {
                $message = "Some errore occured , please try again later.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
        }
    
    //get assesment list 
    $where = array('is_publish'=>1, 'ass_status'=>1);
    $coloumn = array('ass_id','ass_name');
    $lookingfor = 'ass_name ASC';
    $assesmentList =  $ResAricTableObj->getAssesment($where,$coloumn, $lookingfor);
    //get country list 
   
    $dataArr = array();
    $countryArr = $CountryTableObj->getCountry();
    
    //get education list
    $EducationcArr = $UserTableObj->getEducation();
    //get ethinicity list 
    $EthnicityArr = $UserTableObj->getEthnicity();
    //get validic source list
    $ValidicsourceArray = $ResAricTableObj->getvalidicAppSource();

    
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Researcher Arichival Data',
                    'assesmentList' => $assesmentList,
                    'country' => $countryArr,
                    'education' => $EducationcArr,
                    'ethnicity' => $EthnicityArr,
                    'validic_source' => $ValidicsourceArray
        ));
    }
    
    function archivaldataAction(){
        $ResAricTableObj = $this->getServiceLocator()->get('ResearcherArchivalTable');
        $userId = $this->_sessionObj->userDetails['UserID'];
        $domain_id = $this->_sessionObj->userDetails['DomainID'];
        $whereArray= array("UserID"=>$userId,"domain_id"=>$domain_id,"status"=>0);
        $archData = $ResAricTableObj->getArchivallist($whereArray);
       $hmwhereProfile='';
       $hmwhereuser ='';
       $hspwhere='';
        foreach ($archData as $arData){
            
            if(!empty($arData['gender'])){
                $gender= $arData['gender'];
                $hmwhereuser .=" AND hu.Gender='$gender' ";
            }
             if(!empty($arData['gender'])){
                $gender= $arData['gender'];
                $hmwhereuser .=" AND hu.Gender='$gender' ";
            }
            
             if(!empty($arData['education'])){
                $education= $arData['education'];
                $hmwhereProfile .=" AND hpi.education_degree='$education' ";
            }
              if(!empty($arData['ethinicity'])){
                $ethinicity= $arData['ethinicity'];
                $hmwhereProfile .=" AND hpi.ethinicity='$ethinicity' ";
            }
             if(!empty($arData['assesment_id'])){
                $assesment_id= $arData['assesment_id'];
                $hspwhere ="  hsp.ass_id='$assesment_id' AND ";
            }
            
            
        $st ="select hsp.UserID from hm_selfassess_participation as hsp";
         if(!empty($hmwhereuser)){
          $st .=  " LEFT JOIN  hm_users as hu ON hsp.UserID = hu.UserID  $hmwhereuser";
         }
          if(!empty($hmwhereProfile)){
          $st .=  " LEFT JOIN  hm_profile_info hpi ON hsp.UserID = hpi.user_id $hmwhereProfile ";
          }
           $st .= "where $hspwhere  hsp.part_status=0";           
          $data[]=   $ResAricTableObj->getUserlistArchival($st);
         
          
        }
        echo '<pre>';
        print_r($data);
       exit;
         
//        echo '<pre>';
//        print_r($archData);
        exit;
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Researcher Arichival Data',
                   
        ));
        
    }
    
}