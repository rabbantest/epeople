<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ARICHIVAL DATA USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: SHIVENDRA SUMAN.
 * CREATOR: 01/06/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Researcher\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class ResearcherArchivalTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_super_assesments';
    public $vertable = 'hm_super_ass_study_questions_variable';
    public $tablearchival = 'hm_archival_request';
    public $tablepart = 'hm_selfassess_participation';
    public $tablequesans = 'hm_selfassess_ques_answer';
    public $tablequestion = 'hm_super_ass_study_questions';
    public $tablevalidic = 'hm_validic_app_source';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }
    
    
    
    public function getSubscriptionData($where = array(), $columns = array(),$lookingfor=NULL) {

        $orderBy = array("id asc");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'SUB' => 'hm_pricing_matrix_subscription'
            ));
            
            if ($lookingfor) {
                $options = array("user");
                $select->join(array('MAT' => 'hm_super_pricing_matrix'), "SUB.price_id = MAT.id", $options, 'RIGHT');
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);


            $statement = $sql->prepareStatementForSqlObject($select);
//            echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//            die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertArchivalUses($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert('researcher_subscription_uses');
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateArchivalData($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->tablearchival);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminQuestions($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $group = NULL, $limit = array()) {

        $orderBy = array("ques_order ASC", "ques_id asc");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asq' => 'hm_super_ass_study_questions'
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $options = array("var_name");
                $select->join(array('vr' => 'hm_super_ass_study_questions_variable'), "asq.ques_variable = vr.var_id", $options, 'LEFT');
            }


            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                if (!empty($searchContent)) {
                    foreach ($searchContent AS $fields => $data) {
                        if (!empty($data)) {
                            $likeWhr->addPredicate(
                                    new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                            );
                            $select->where($likeWhr);
                        }
                    }
                }
                $orderBy = "ques_variable ASC";
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            if ($group) {
                $select->group('ques_variable');
            }

            $select->order($orderBy);


            $statement = $sql->prepareStatementForSqlObject($select);
//            $statement->prepare();
//            echo $statement->getSql();
//            exit;
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getViewData($ArchivalData = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array(), $group = 1, $Source) {
        $orderBy = "UserID ASC";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'User' => 'hm_users'
            ));
            
            $where = array();
            if (!empty($ArchivalData['gender'])) {
                $where['gender'] = $ArchivalData['gender'];
            }

            $start_date = $ArchivalData['from_date'];
            $end_date = $ArchivalData['to_date'];
            if (isset($Source['Height'])) {
                $source = ($Source['Height'] == 'manual') ? '' : $Source['Height'];
                $columns['Height'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(ROUND(MIN(Height),2),"-",ROUND(MAX(Height),2)) from hm_height_records where UserID=User.UserID AND HeightRecordDate>"'
                        . $start_date . '" AND HeightRecordDate<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($Source['Weight'])) {
                $source = ($Source['Weight'] == 'manual') ? '' : $Source['Weight'];
                $columns['Weight'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(ROUND(MIN(Weight)*0.00220462,2),"-",ROUND(MAX(Weight)*0.00220462,2)) from hm_weight_records where UserID=User.UserID AND WeightRecordDate>"'
                        . $start_date . '" AND WeightRecordDate<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($Source['PhysicalData'])) {
                $source = ($Source['PhysicalData'] == 'manual') ? '' : $Source['PhysicalData'];

                $columns['PhysicalData'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(ROUND(SUM(NumberOfSteps))'
                        . ',"-",SUM(Duration)'
                        . ',"-",ROUND(SUM(Distance),2)'
                        . ',"-",ROUND(SUM(CaloriesBurned),3)'
                        . ') from hm_physical_acitivity where UserID=User.UserID AND ExerciseRecorddate>"'
                        . $start_date . '" AND ExerciseRecorddate<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($Source['Cholesterol'])) {
                $source = ($Source['Cholesterol'] == 'manual') ? '' : $Source['Cholesterol'];

                $columns['Cholesterol'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(ROUND(MIN(LDL),2)'
                        . ',"-",ROUND(MAX(LDL),2)'
                        . ',"-",ROUND(MIN(HDL),2)'
                        . ',"-",ROUND(MAX(HDL),2)'
                        . ',"-",ROUND(MIN(Triglycerides),2)'
                        . ',"-",ROUND(MAX(Triglycerides),2)'
                        . ')'
                        . ' from hm_cholesterol_records where UserID=User.UserID AND CholesterolRecordDate>"'
                        . $start_date . '" AND CholesterolRecordDate<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($Source['Pressure'])) {
                $source = ($Source['Pressure'] == 'manual') ? '' : $Source['Pressure'];

                $columns['Pressure'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(MIN(systolic)'
                        . ',"-",MAX(systolic)'
                        . ',"-",MIN(diastolic)'
                        . ',"-",MAX(diastolic)'
                        . ',"-",MIN(pulse)'
                        . ',"-",MAX(pulse)'
                        . ')'
                        . ' from hm_blood_pressure_records where UserID=User.UserID AND bp_record_date>"'
                        . $start_date . '" AND bp_record_date<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($Source['Glucose'])) {
                $source = ($Source['Glucose'] == 'manual') ? '' : $Source['Glucose'];

                $columns['Glucose'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(MIN(measurement)'
                        . ',"-",MAX(measurement)'
                        . ')'
                        . ' from hm_blood_glucose_records where UserID=User.UserID AND blood_record_date>"'
                        . $start_date . '" AND blood_record_date<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($Source['SleepRecord'])) {
                $source = ($Source['SleepRecord'] == 'manual') ? '' : $Source['SleepRecord'];

                $columns['SleepRecord'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(SUM(total_sleep)'
                        . ',"-",SUM(awake)'
                        . ',"-",SUM(deep)'
                        . ',"-",SUM(light)'
                        . ',"-",SUM(rem)'
                        . ')'
                        . ' from hm_sleep_records where UserID=User.UserID AND sleep_record_date>"'
                        . $start_date . '" AND sleep_record_date<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            if (isset($Source['Temperature'])) {
                $source = ($Source['Temperature'] == 'manual') ? '' : $Source['Temperature'];

                $columns['Temperature'] = new \Zend\Db\Sql\Predicate\Expression(''
                        . '(select CONCAT(MIN(temperature)'
                        . ',"-",MAX(temperature)'
                        . ')'
                        . ' from hm_temperature_records where UserID=User.UserID AND temperature_record_date>"'
                        . $start_date . '" AND temperature_record_date<"' . $end_date
                        . '" AND source="' . $source . '")'
                );
            }

            $columns['CONDITIONS'] = new \Zend\Db\Sql\Predicate\Expression('(select GROUP_CONCAT(common_name) from hm_super_conditions where id IN ('
                    . 'SELECT condition_id FROM hm_users_conditions WHERE user_id=User.UserID AND status=1'
                    . '))');

            $columns['SYMPTOMS'] = new \Zend\Db\Sql\Predicate\Expression('(select GROUP_CONCAT(common_name) from hm_super_symptoms where id IN ('
                    . 'SELECT symtoms_id FROM hm_users_symtoms WHERE user_id=User.UserID AND status=1'
                    . '))');

            $columns['TREATMENTS'] = new \Zend\Db\Sql\Predicate\Expression('(select GROUP_CONCAT(common_name) from hm_super_treatments where id IN ('
                    . 'SELECT treatment_id FROM hm_users_treatments WHERE user_id=User.UserID AND status=1'
                    . '))');



            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (!empty($ArchivalData['assesment_id']) && $ArchivalData['assesment_id'] != 0) {
                $where['SEU.assessment_id'] = $ArchivalData['assesment_id'];
                $options = array();
                $select->join(array('SEU' => 'hm_selfassess_ques_answer'), new \Zend\Db\Sql\Expression("User.UserID = SEU.user_id"), $options, 'RIGHT');
                $select->join(array('Var' => 'hm_super_ass_study_questions_variable'), new \Zend\Db\Sql\Expression("Var.var_id = SEU.ques_variable"), $options, 'LEFT');
                $select->join(array('Opt' => 'hm_super_ass_study_questions_option'), new \Zend\Db\Sql\Expression("Opt.question_id = SEU.quest_id AND Opt.opt_value = SEU.option_id"), $options, 'LEFT');
            }

            //hm_profile_info
            $profile = false;
            if (!empty($ArchivalData['education']) && $ArchivalData['education'] != 0) {
                $where['Profile.education_degree'] = $ArchivalData['education'];
                $profile = True;
            }
            if (!empty($ArchivalData['ethinicity']) && $ArchivalData['ethinicity'] != 0) {
                $where['Profile.ethnicity'] = $ArchivalData['ethinicity'];
                $profile = True;
            }

            $options = array();
            if ($profile) {
                $select->join(array('Profile' => 'hm_profile_info'), new \Zend\Db\Sql\Expression("User.UserID = Profile.user_id"), $options, 'RIGHT');
            }


            //hm_contacts_info
            $contact = false;
            if (!empty($ArchivalData['country']) && $ArchivalData['country'] != 0) {
                $where['CONTACT.country_id'] = $ArchivalData['country'];
                $contact = True;
            }
            if (!empty($ArchivalData['state']) && $ArchivalData['state'] != 0) {
                $where['CONTACT.state_id'] = $ArchivalData['state'];
                $contact = True;
            }
            if (!empty($ArchivalData['county']) && $ArchivalData['county'] != 0) {
                $where['CONTACT.county_id'] = $ArchivalData['county'];
                $contact = True;
            }

            $options = array();
            if ($contact) {
                $select->join(array('CONTACT' => 'hm_contacts_info'), new \Zend\Db\Sql\Expression("User.UserID = CONTACT.user_id"), $options, 'RIGHT');
            }

            if ($lookingfor) {
                
            }
            //print_r($limit);die;
            if (count($limit) > 1) {
               $select->limit(intval($limit[1]))->offset(intval($limit[0]));
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            if ($group) {
                $select->group(array('User.UserID'));
            }
            $select->order($orderBy);
//            echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//            die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
           
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function insertArchival($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->tablearchival);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getArchivallist($where = array(), $columns = array(), $lookingfor = false, $limit = array()) {
        // $ethnicity = array("ethnicity_name");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'archi' => $this->tablearchival
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }

            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    function getArchivalData($where = array(), $hmwhereProfile = '', $hmwhereContact = '', $hmwhereuser = '', $source_height = '', $source_weight = '', $source_steps = '', $source_sleep = '', $source_cholesterol = '', $source_bloodpressure = '', $source_fastingglucose = '', $source_temperature = '', $from_date = false, $to_date = false, $assesment_id = false, $limit, $columns = array(), $lookingfor = false, $group = 1) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'hsp' => $this->tablepart
            ));
            if (!empty($source_height) && $source_height != 'manuel') {
                $source_height = " AND hhr.source = '$source_height' ";
            }
            if ($source_height == 'manuel') {
                $source_height = ' AND hhr.source = "" ';
            }
            if (!empty($source_weight) && $source_weight != 'manuel') {
                $source_weight = " AND hwr.source = '$source_weight' ";
            }
            if ($source_weight == 'manuel') {
                $source_weight = " AND hwr.source = '' ";
            }

            if (!empty($source_steps) && $source_steps != 'manuel') {
                $source_steps = " AND hpa.source = '$source_steps' ";
            }
            if ($source_steps == 'manuel') {
                $source_steps = " AND hpa.source = '' ";
            }

            if (!empty($source_sleep) && $source_sleep != 'manuel') {
                $source_sleep = " AND hsr.source = '$source_sleep' ";
            }
            if ($source_sleep == 'manuel') {
                $source_sleep = " AND hsr.source = '' ";
            }
            if (!empty($source_cholesterol) && $source_cholesterol != 'manuel') {
                $source_cholesterol = " AND hcr.source = '$source_cholesterol' ";
            }
            if ($source_cholesterol == 'manuel') {
                $source_cholesterol = " AND hcr.source = '' ";
            }

            if (!empty($source_bloodpressure) && $source_bloodpressure != 'manuel') {
                $source_bloodpressure = " AND hbpr.source = '$source_bloodpressure' ";
            }
            if ($source_bloodpressure == 'manuel') {
                $source_bloodpressure = ' AND hbpr.source = "" ';
            }

            if (!empty($source_fastingglucose) && $source_fastingglucose != 'manuel') {
                $source_fastingglucose = " AND hbgr.source = '$source_fastingglucose' ";
            }
            if ($source_fastingglucose == 'manuel') {
                $source_fastingglucose = ' AND hbgr.source = "" ';
            }
            if (!empty($source_temperature) && $source_temperature != 'manuel') {
                $source_temperature = " AND htr.source = '$source_temperature' ";
            }
            if ($source_temperature == 'manuel') {
                $source_temperature = " AND htr.source = '' ";
            }

            $columns['Height_IN_Centimeter'] = new \Zend\Db\Sql\Predicate\Expression('(select  CONCAT(ROUND(MIN(Height),2), "/", ROUND(MAX(Height),2)) from hm_height_records as hhr where hhr.UserID=hsp.UserID ' . $source_height . ' AND hhr.HeightRecordDate <= "' . $from_date . '"  AND hhr.HeightRecordDate >= "' . $to_date . '")');
            // $columns['Height_IN_Centimeter']=new \Zend\Db\Sql\Predicate\Expression('(select  CONCAT(ROUND(MIN(Height),2), "/", ROUND(MAX(Height),2)) from hm_height_records as hhr where hhr.UserID=hsp.UserID )');
            // $columns['MaxHeight_IN_Centimeter']=new \Zend\Db\Sql\Predicate\Expression('(select ROUND(MAX(Height),2) from hm_height_records as hhr2 where hhr2.UserID=hsp.UserID)');

            $columns['Weight_IN_Pound'] = new \Zend\Db\Sql\Predicate\Expression('(select CONCAT(ROUND(MIN(Weight)*0.00220462,2), "/", ROUND(MAX(Weight)*0.00220462,2)) from hm_weight_records as hwr where hwr.UserID=hsp.UserID ' . $source_weight . ' AND hwr.WeightRecordDate <= "' . $from_date . '"  AND hwr.WeightRecordDate >= "' . $to_date . '")');
            //$columns['MaxWeight_IN_Pound']=new \Zend\Db\Sql\Predicate\Expression('(select ROUND(MAX(Weight)*0.00220462,2) from hm_weight_records as hwr2 where hwr2.UserID=hsp.UserID)');

            $columns['Physical_Activity'] = new \Zend\Db\Sql\Predicate\Expression('(select CONCAT(SUM(NumberOfSteps), "/", SUM(Duration), "/", SUM(Distance), "/",SUM(CaloriesBurned)) '
                    . ' from hm_physical_acitivity as hpa where hpa.UserID=hsp.UserID ' . $source_steps . ' AND hpa.ExerciseRecorddate <= "' . $from_date . '"  AND hpa.ExerciseRecorddate >= "' . $to_date . '")');

            $columns['Sleep_Record'] = new \Zend\Db\Sql\Predicate\Expression('(select CONCAT(ROUND(MIN(awake)), "/", ROUND(MAX(awake)), "/", ROUND(MIN(deep)), "/", ROUND(MAX(deep)), "/", ROUND(MIN(light)), "/", ROUND(MAX(light)), "/", ROUND(MIN(rem)), "/", ROUND(MAX(rem))) '
                    . ' from hm_sleep_records as hsr where hsr.UserID=hsp.UserID ' . $source_sleep . ' AND hsr.sleep_record_date <= "' . $from_date . '"  AND hsr.sleep_record_date >= "' . $to_date . '")');

            $columns['Cholesterol_Record'] = new \Zend\Db\Sql\Predicate\Expression('(select CONCAT(ROUND(MIN(LDL)), "/", ROUND(MAX(LDL)), "/",ROUND(MIN(HDL)), "/", ROUND(MAX(HDL)), "/",ROUND(MIN(Triglycerides)), "/", ROUND(MAX(Triglycerides))) '
                    . ' from hm_cholesterol_records as hcr where hcr.UserID=hsp.UserID ' . $source_cholesterol . ' AND hcr.CholesterolRecordDate <= "' . $from_date . '"   AND hcr.CholesterolRecordDate >= "' . $to_date . '")');

            $columns['Pressure_Record'] = new \Zend\Db\Sql\Predicate\Expression('(select CONCAT(ROUND(MIN(systolic)), "/", ROUND(MAX(systolic)), "/",ROUND(MIN(diastolic)), "/", ROUND(MAX(diastolic)), "/",ROUND(MIN(pulse)), "/", ROUND(MAX(pulse))) '
                    . ' from hm_blood_pressure_records as hbpr where hbpr.UserID=hsp.UserID ' . $source_bloodpressure . ' AND hbpr.bp_record_date <= "' . $from_date . '"  AND hbpr.bp_record_date >= "' . $to_date . '")');

            $columns['Glucose_Record'] = new \Zend\Db\Sql\Predicate\Expression('(select CONCAT(ROUND(MIN(measurement)), "/", ROUND(MAX(measurement))) '
                    . ' from hm_blood_glucose_records as hbgr where hbgr.UserID=hsp.UserID ' . $source_fastingglucose . ' AND hbgr.blood_record_date <= "' . $from_date . '"  AND hbgr.blood_record_date >= "' . $to_date . '")');

            $columns['Temperature_Record'] = new \Zend\Db\Sql\Predicate\Expression('(select CONCAT(ROUND(MIN(temperature)), "/", ROUND(MAX(temperature))) '
                    . ' from hm_temperature_records as htr where htr.UserID=hsp.UserID ' . $source_temperature . ' AND htr.temperature_record_date <= "' . $from_date . '"  AND htr.temperature_record_date >= "' . $to_date . '")');


            $columns['conditions'] = new \Zend\Db\Sql\Predicate\Expression('(select GROUP_CONCAT(common_name) from hm_super_conditions  where id IN ('
                    . 'SELECT condition_id FROM hm_users_conditions as huc WHERE huc.user_id=hsp.UserID AND huc.status=1 AND huc.creation_date <= "' . $from_date . '"  AND huc.creation_date >= "' . $to_date . '"'
                    . '))');

            $columns['symptoms'] = new \Zend\Db\Sql\Predicate\Expression('(select GROUP_CONCAT(common_name) from hm_super_symptoms where id IN ('
                    . 'SELECT symtoms_id FROM hm_users_symtoms as hus WHERE hus.user_id=hsp.UserID AND hus.status=1 AND hus.creation_date <= "' . $from_date . '"  AND hus.creation_date >= "' . $to_date . '"'
                    . '))');

            $columns['treatments'] = new \Zend\Db\Sql\Predicate\Expression('(select GROUP_CONCAT(common_name) from hm_super_treatments where id IN ('
                    . 'SELECT treatment_id FROM hm_users_treatments as hut WHERE hut.user_id=hsp.UserID AND hut.status=1 AND hut.creation_date <= "' . $from_date . '"  AND hut.creation_date >= "' . $to_date . '"'
                    . '))');
            //   $columns['Question_Ans_Earliest']=new \Zend\Db\Sql\Predicate\Expression('(select hsasq.ques_question from hm_super_ass_study_questions as hsasq where hsasq.assessment_id= "'.$assesment_id.'" )');

            $options = array("FirstName", "UserID");
            //$options = array();
            $select->join(array('hu' => 'hm_users'), new \Zend\Db\Sql\Expression("hsp.UserID = hu.UserID $hmwhereuser"), $options, 'LEFT');
            $select->join(array('hpi' => 'hm_profile_info'), new \Zend\Db\Sql\Expression("hsp.UserID = hpi.user_id $hmwhereProfile"), array(), 'LEFT');
            $select->join(array('hci' => 'hm_contacts_info'), new \Zend\Db\Sql\Expression("hsp.UserID = hci.user_id $hmwhereContact"), array(), 'LEFT');


            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }

            if ($group) {
                $select->group(array('hsp.UserID'));
            }
            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            // echo  $select->getSqlString();die();
            $statement = $sql->prepareStatementForSqlObject($select);

            //echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getQuesionAnswer($where = array(), $columns = array(), $lookingfor = false, $userId = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'ans' => $this->tablequestion
            ));
            $columns['answer'] = new \Zend\Db\Sql\Predicate\Expression('(select opt_option from hm_super_ass_study_questions_option as sopt  where sopt.opt_id = ('
                    . 'SELECT option_id FROM hm_selfassess_ques_answer as hsqa WHERE hsqa.quest_id=ans.ques_id AND hsqa.user_id="' . $userId . '" ORDER BY id DESC LIMIT 0,1 '
                    . '))');
            $columns['answer_oldest'] = new \Zend\Db\Sql\Predicate\Expression('(select opt_option from hm_super_ass_study_questions_option as sopt  where sopt.opt_id = ('
                    . 'SELECT option_id FROM hm_selfassess_ques_answer as hsqa WHERE hsqa.quest_id=ans.ques_id AND hsqa.user_id="' . $userId . '" ORDER BY id ASC LIMIT 0,1 '
                    . '))');
            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAssesment($where = array(), $columns = array(), $lookingfor = false) {
        // $ethnicity = array("ethnicity_name");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'ass' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    //get veriable 
    public function getVeriable($where = array(), $columns = array(), $lookingfor = false) {
        // $ethnicity = array("ethnicity_name");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'ass_ver' => $this->vertable
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }


            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    //get veriable 
    public function getvalidicAppSource($where = array(), $columns = array(), $likeArray = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'Source' => 'hm_validic_app_source'
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (count($likeArray) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($likeArray AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }
            $select->order('source ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAssQuestion($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $group = NULL, $limit = array()) {


        $orderBy = array("ques_order ASC", "ques_id asc");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asq' => 'hm_super_ass_study_questions'
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $options = array();
                $select->join(array('vr' => 'hm_super_ass_study_questions_variable'), "asq.ques_variable = vr.var_id", $options, 'LEFT');
                $options = array();
                $select->join(array('option' => 'hm_super_ass_study_questions_option'), "asq.ques_id = option.question_id", $options, 'LEFT');
            }


            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }

            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                if (!empty($searchContent)) {
                    foreach ($searchContent AS $fields => $data) {
                        if (!empty($data)) {
                            $likeWhr->addPredicate(
                                    new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                            );
                            $select->where($likeWhr);
                        }
                    }
                }
                $orderBy = "ques_variable ASC";
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            if ($group) {
                $select->group('ques_variable');
            }

            $select->order($orderBy);


            $statement = $sql->prepareStatementForSqlObject($select);
//            $statement->prepare();
//            echo $statement->getSql();
//            exit;
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
