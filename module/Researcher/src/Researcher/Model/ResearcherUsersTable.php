<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rabban Ahmad
 * CREATOR: 16/2/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Researcher\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;

class ResearcherUsersTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_users';
    public $domainTable = 'hm_pass_subdomain';
    public $weighttable = 'hm_weight_records';
    public $tablesubd = 'hm_pass_subdomain';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function getDomain($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array(), $not = '1') {

        $orderBy = "id asc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'dom' => $this->domainTable
            ));
            if ($not == 1) {
                $select->where->NotequalTo('dom.id', '1');
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if ($lookingfor) {
                
            }

            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);
//echo $select->getSqlString();die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getUser($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }
            // echo '<pre>'; print_r($select); die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getIndividualList($where = array(), $columns = array(), $notEqual = false, $lookingfor = false, $limit = array(), $nameSearch = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_users'
            ));
            if (count($where) > 0) {
                $select->where($where);
            }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($nameSearch) {
                $select->where->nest->expression("CONCAT('', us.FirstName, us.MiddleName, us.LastName) LIKE ?", '%' . trim($nameSearch) . '%');
            }
            if (isset($where['hmu_privacy.want_takecare']))
                $select->join(array('hmu_privacy' => 'hm_users_privacy_settings'), "hmu_privacy.UserID = us.UserID", array(), 'LEFT');
            $select->join(array('hm_con' => 'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
            $select->join(array('states' => 'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
            $select->join(array('country' => 'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');

            if (count($notEqual)) {
                foreach ($notEqual as $k => $v) {
                    $select->where->NotequalTo('us.UserID', $v);
                }
            }
            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            $select->group('us.UserID');
            //echo $select->getSqlString();
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getCareGiver($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'care' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $user = array('UserID', 'FirstName', 'LastName', 'UserName', 'Image', 'Email', 'Dob');
            $select->join(array('us' => 'hm_users'), "care.UserID = us.UserID", $user, 'INNER');
            $select->join(array('hm_con' => 'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
            $select->join(array('states' => 'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
            $select->join(array('country' => 'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');
            $select->join(array('weiTable' => $this->weighttable), "care.UserID=weiTable.UserID", array('Weight', 'Unit'), 'LEFT');

            if ($lookingfor) {
                
            }
            $select->group('us.UserID');
            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());die;
            $users = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $users;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getIndividualUser($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'care' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $user = array('UserID', 'FirstName', 'LastName', 'UserName', 'Image', 'Email', 'Dob');
            $select->join(array('us' => 'hm_users'), "care.UserID = us.UserID", $user, 'INNER');
            $select->join(array('hm_con' => 'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
            $select->join(array('states' => 'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
            $select->join(array('country' => 'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');

            if ($lookingfor) {
                
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getsubDomainAdmin($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'subda' => $this->tablesubd
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
