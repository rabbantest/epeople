<?php

/* * ***********************
 * PAGE: USE TO MANAGE SUPER ADMIN CONDITION PANNEL.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 3/3/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Researcher\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Delete;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Db\Adapter\Driver\DriverInterface;

class ResearcherStudyTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_super_studies';
    public $varSetTable = 'hm_super_study_varrable_set';
    public $invitationSetTable = 'hm_super_study_invitation_set';
    public $inviBucketTable = 'hm_super_study_bucket_invitation';
    public $settingTable = 'hm_super_study_bucket_cron_setting';
    public $partTable = 'hm_selfassess_participation';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

     public function getSource($where = array(), $columns = array(),$likeArray=array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'Source' => 'hm_validic_app_source'
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            
            if (count($likeArray) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($likeArray AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }
            
            if (count($where) > 0) {
                $select->where($where);
            }
            $select->order('source ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    public function insertAnsStudy($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert('hm_selfassess_ques_answer');
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAnsStudy($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'answe' => 'hm_selfassess_ques_answer'
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudyBucketInvi($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'inviBucket' => $this->inviBucketTable
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudyParticipation($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'part' => $this->partTable
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            /// $select->group(array('part.UserID'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudyBucketInvitation($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array(), $group = 0) {
        $orderBy = "id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'invi' => 'hm_super_study_bucket_invitation'
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->join(array('part' => 'hm_selfassess_participation'), "invi.study_id = part.study_id AND invi.user_id = part.UserID", array(), 'LEFT');
            }
            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }
            if ($group) {
                $select->group(array('invi.is_accepted'));
            }

            if (count($where) > 0) {
                $select->where($where);
            }
            $select->order($orderBy);
            //echo $select->getSqlString($this->getAdapter()->getPlatform());die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    public function getAdminStudyBucketCronSetting($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'bucket' => $this->settingTable
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

   public function getAdminStudyInvitationSet($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array()) {

        $orderBy = "invitation_set_id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'inv' => $this->invitationSetTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $options = array('study_title', 'start_date','end_date');
                $select->join(array('Stu' => 'hm_super_studies'), new \Zend\Db\Sql\Expression("Stu.study_id = inv.invitation_set_study"), $options, 'LEFT');
            
                $options = array('subdomain');
                $select->join(array('Dom' => 'hm_pass_subdomain'), new \Zend\Db\Sql\Expression("inv.domain_id = Dom.id"), $options, 'LEFT');
            
            }

            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudy($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array(), $date = NULL) {

        $orderBy = "study_id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stu' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
               /* $options = array('total' => new \Zend\Db\Sql\Expression('SUM(number)/'));
                $select->join(array('set' => $this->settingTable), new \Zend\Db\Sql\Expression("stu.study_id = set.study_id AND set.sent=1"), $options, 'LEFT');
                $select->group(array('stu.study_id'));
                
                $options = array('Participated' => new \Zend\Db\Sql\Expression('count(part.study_id)'));
                $select->join(array('part' => $this->partTable), "stu.study_id = part.study_id", $options, 'LEFT');*/
            }

            if (!empty($date)) {
                $select->where(new \Zend\Db\Sql\Predicate\Expression("start_date >= '$date'"));
            }

            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);
//           echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//die;
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getAdminStudyVarSet($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array()) {

        $orderBy = "set_id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'varSet' => $this->varSetTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }

            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function insertAdminStudy($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->table);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertAdminStudyVarSet($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->varSetTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateAdminStudyVarSet($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->varSetTable);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertAdminStudyInvitationSet($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->invitationSetTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function deleteAdminStudyInvitation($where = array(), $notIn = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from($this->invitationSetTable);

            if (count($where) > 0) {
                $delete->where($where);
            }

            if (count($notIn) > 0) {
                $delete->where->notIn('opt_id', $notIn);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateAdminStudy($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function checkAvilableStudy($where = array(), $notIn = NULL) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stu' => $this->table
            ));

            $select->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (!empty($notIn)) {
                $select->where->NotequalTo('stu.study_id', $notIn);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $status = $this->resultSetPrototype->initialize($statement->execute())->toArray();

            return ($status[0]['num'] == 0) ? true : false;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ****This function is used to get the list of users who has participated in Domain studies********* */

    public function getUserListParticipatedDomainStudy($where = array(), $columns = array(), $notIn = '') {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stu' => $this->table
            ));
            if (count($where) > 0) {
                $select->where($where);
            }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->join(array('self_part' => 'hm_selfassess_participation'), "self_part.study_id = stu.study_id", array('eligible_for_main_study', 'UserID'), 'INNER');
            $select->join(array('us' => 'hm_users'), "self_part.UserID = us.UserId", array('Email', 'FirstName', 'LastName', 'Gender', 'Dob', 'Image'), 'INNER');
            $select->join(array('hm_con' => 'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
            $select->join(array('states' => 'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
            $select->join(array('country' => 'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');
            $select->group('self_part.UserID');

            if (!empty($notIn)) {
                $select->where->NotequalTo('self_part.UserID', $notIn);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
           // echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;
            $result = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $result;
        } catch (Exception $ex) {
            throw new \Exception($ex->getPrevious()->getMessage());
        }
    }

    public function ShowDataBasedOnSuperThreadhold($IndividualId = null, $gender = null, $age = null) {
        try {
            $i = 0;
            $returnDataArr = array();
            $session_obj = new Container('Researcher');
            $DomainID = $session_obj->userDetails['DomainID'];
            $whereSystolic = array('created_by' => $DomainID, 'gender' => $gender);
            $columnsHdl = array('min_systolic', 'max_systolic');
            /*             * ****Code to get systolic Data****** */
            $SystolicData = $this->getThresholdSystolic($whereSystolic, $columnsHdl, $age);
            if ($SystolicData && $SystolicData[0]['min_systolic'] && $SystolicData[0]['max_systolic']) {
                $wheresystolic = array('UserID' => $IndividualId);
                $min_systolic = $SystolicData[0]['min_systolic'];
                $max_systolic = $SystolicData[0]['max_systolic'];
                $Showsystolic = $this->getUpDownSystolic($wheresystolic, $min_systolic, $max_systolic, array('systolic'));
                if ($Showsystolic['Data']) {
                    $i++;
                    $returnDataArr1[] = $Showsystolic;
                } else {
                    $returnDataArr1[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Systolic', 'class' => '');
                }
            } else {
                $returnDataArr1[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Systolic', 'class' => '');
            }
            //}
            /*             * ****Code to get dystolic Data****** */
            // if ($i < 3) {
            $whereDystolic = array('created_by' => $DomainID, 'gender' => $gender);
            $columnsHdl = array('min_diastolic', 'max_diastolic');
            $DystolicData = $this->getThresholdDystolic($whereDystolic, $columnsHdl, $age);

            if ($DystolicData && isset($DystolicData[0]['min_diastolic']) && isset($DystolicData[0]['max_diastolic'])) {
                $wheresystolic = array('UserID' => $IndividualId);
                $min_diastolic = $DystolicData[0]['min_diastolic'];
                $max_diastolic = $DystolicData[0]['max_diastolic'];
                $Showdystolic = $this->getUpDownDystolic($wheresystolic, $min_diastolic, $max_diastolic, array('diastolic'));
                if ($Showdystolic['Data']) {
                    $i++;
                    $returnDataArr2[] = $Showdystolic;
                } else {
                    $returnDataArr2[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Diastolic', 'class' => '');
                }
            } else {
                $returnDataArr2[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Diastolic', 'class' => '');
            }
            $returnDataArr[] = array('Data' => $returnDataArr1[0]['Data'] . "/" . $returnDataArr2[0]['Data'], 'displayData' => $returnDataArr1[0]['displayData'] . "/" . $returnDataArr2[0]['displayData'], 'label' => 'Systolic/Diastolic', 'class' => $returnDataArr1[0]['class'] . "/" . $returnDataArr2[0]['class']);

            /*             * ****Code to get Temperature Data****** */
            // if ($i < 3) {
            $whereHdl = array('created_by' => $DomainID);
            $columnsHdl = array('min_temp', 'max_temp');
            $getThresholdTempData = $this->getThresholdTemp($whereHdl, $columnsHdl);
            if ($getThresholdTempData && $getThresholdTempData[0]['min_temp'] && $getThresholdTempData[0]['max_temp']) {
                $whereHdl = array('UserID' => $IndividualId);
                $min_temp = $getThresholdTempData[0]['min_temp'];
                $max_temp = $getThresholdTempData[0]['max_temp'];
                $ShowLdl = $this->getUpDownTemp($whereHdl, $min_temp, $max_temp, array('temperature'));
                if ($ShowLdl['Data']) {
                    $i++;
                    $returnDataArr[] = $ShowLdl;
                } else {
                    $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Temperature');
                }
            } else {
                $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Temperature');
            }

            // }
            /*             * ****Code to get Fasting glucose Data****** */
//            if ($i < 3) {
//                $whereHdl = array('caregiver_id' => $UserID);
//                $columnsglucose = array('min_glucose', 'max_glucose');
//                $GlucoseData = $this->getThresholdGlucose($whereHdl, $columnsglucose);
//                if ($GlucoseData && $GlucoseData[0]['min_glucose'] && $GlucoseData[0]['max_glucose']) {
//                    $whereHeight = array('UserID' => $IndividualId);
//                    $min_glucose = $GlucoseData[0]['min_glucose'];
//                    $max_glucose = $GlucoseData[0]['max_glucose'];
//                    $ShowHeight = $this->getUpDownGlucose($whereHeight, $min_glucose, $max_glucose);
//                    if ($ShowHeight['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowHeight;
//                    }
//                }
//            }


            /*             * ****Code to get systolic Data****** */
            // if ($i < 3) {
            // }

            /*             * ****Code to get Sleep Data****** */
//            if ($i < 3) {
//                $whereHdl = array('caregiver_id' => $UserID);
//                $columnsHdl = array('min_sleep', 'max_sleep');
//                $HdlLdlTriglycData = $this->getThresholdsleep($whereHdl, $columnsHdl);
//                if ($HdlLdlTriglycData && $HdlLdlTriglycData[0]['min_sleep'] && $HdlLdlTriglycData[0]['max_sleep']) {
//                    $whereHdl = array('UserID' => $IndividualId);
//                    $min_sleep = $HdlLdlTriglycData[0]['min_sleep'];
//                    $max_sleep = $HdlLdlTriglycData[0]['max_sleep'];
//                    $ShowLdl = $this->getUpDownSleep($whereHdl, $min_sleep, $max_sleep, array('total_sleep'));
//                    if ($ShowLdl['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowLdl;
//                    }
//                }
//            }
            /*             * ****Code to get Plus Data****** */
            //if ($i < 3) {
            $whereDystolic = array('created_by' => $DomainID, 'gender' => $gender);
            $columnsHdl = array('min_pulse', 'max_pulse');
            $PluseData = $this->getThresholdPluse($whereDystolic, $columnsHdl, $age);

            if ($PluseData && $PluseData[0]['min_pulse'] && $PluseData[0]['max_pulse']) {
                $wherepluse = array('UserID' => $IndividualId);
                $min_pulse = $PluseData[0]['min_pulse'];
                $max_pulse = $PluseData[0]['max_pulse'];
                $Showdystolic = $this->getUpDownPluse($wherepluse, $min_pulse, $max_pulse, array('pulse'));
                if ($Showdystolic['Data']) {
                    $i++;
                    $returnDataArr[] = $Showdystolic;
                } else {
                    $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Pulse');
                }
            } else {
                $returnDataArr[] = array('Data' => 1, 'displayData' => 'N/A', 'label' => 'Pulse');
            }
            // }


            /*             * ****Code to get HDL Data****** */
//            if ($i < 3) {
//                $whereHdl = array('caregiver_id' => $UserID);
//                $columnsHdl = array('min_hld', 'max_hld');
//                $HdlLdlTriglycData = $this->getThresholdcholestrol($whereHdl, $columnsHdl);
//                if ($HdlLdlTriglycData && $HdlLdlTriglycData[0]['min_hld'] && $HdlLdlTriglycData[0]['max_hld']) {
//                    $whereHdl = array('UserID' => $IndividualId);
//                    $min_hld = $HdlLdlTriglycData[0]['min_hld'];
//                    $max_hld = $HdlLdlTriglycData[0]['max_hld'];
//                    $ShowHdl = $this->getUpDownHdl($whereHdl, $min_hld, $max_hld, array('HDL'));
//                    if ($ShowHdl['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowHdl;
//                    }
//                }
//            }
            /*             * ****Code to get LDL Data****** */
//            if ($i < 3) {
//                $whereHdl = array('caregiver_id' => $UserID);
//                $columnsHdl = array('min_ldl', 'max_ldl', 'min_triglyc', 'max_triglyc');
//                $HdlLdlTriglycData = $this->getThresholdcholestrol($whereHdl, $columnsHdl);
//                if ($HdlLdlTriglycData && $HdlLdlTriglycData[0]['min_ldl'] && $HdlLdlTriglycData[0]['max_ldl']) {
//                    $whereHdl = array('UserID' => $IndividualId);
//                    $min_ldl = $HdlLdlTriglycData[0]['min_ldl'];
//                    $max_ldl = $HdlLdlTriglycData[0]['max_ldl'];
//                    $ShowLdl = $this->getUpDownLdl($whereHdl, $min_ldl, $max_ldl, array('LDL'));
//                    if ($ShowLdl['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowLdl;
//                    }
//                }
//            }
            /*             * ****Code to get Triglyc Data****** */
//            if ($i < 3) {
//                $whereHdl = array('caregiver_id' => $UserID);
//                $columnsHdl = array('min_triglyc', 'max_triglyc');
//                $HdlLdlTriglycData = $this->getThresholdcholestrol($whereHdl, $columnsHdl);
//                if ($HdlLdlTriglycData && $HdlLdlTriglycData[0]['min_triglyc'] && $HdlLdlTriglycData[0]['max_triglyc']) {
//                    $whereHdl = array('UserID' => $IndividualId);
//                    $min_triglyc = $HdlLdlTriglycData[0]['min_triglyc'];
//                    $max_triglyc = $HdlLdlTriglycData[0]['max_triglyc'];
//                    $ShowLdl = $this->getUpDownTriglycerides($whereHdl, $min_triglyc, $max_triglyc, array('Triglycerides'));
//                    if ($ShowLdl['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowLdl;
//                    }
//                }
//            }
            /*             * ****Code to get height Data****** */
//            if ($i < 3) {
//                $ThresholdForWaitHeiWaist = $this->getBodyMeasurement(array("status" => 1, "caregiver_id" => $UserID), array('min_height', 'max_height', 'min_wast', 'max_wast', 'min_weight', 'max_weight'));
//                if ($ThresholdForWaitHeiWaist && $ThresholdForWaitHeiWaist[0]['min_height'] && $ThresholdForWaitHeiWaist[0]['max_height']) {
//                    $whereHeight = array('UserID' => $IndividualId);
//                    $MinHeight = $ThresholdForWaitHeiWaist[0]['min_height'];
//                    $MaxHeight = $ThresholdForWaitHeiWaist[0]['max_height'];
//                    $ShowHeight = $this->getUpDownThresholdHeight($whereHeight, $MinHeight, $MaxHeight);
//                    if ($ShowHeight['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowHeight;
//                    }
//                }
//            }
            /*             * ****Code to get Waist Data****** */
//            if ($i < 3) {
//                $ThresholdForWaitHeiWaist = $this->getBodyMeasurement(array("status" => 1, "caregiver_id" => $UserID), array('min_height', 'max_height', 'min_wast', 'max_wast', 'min_weight', 'max_weight'));
//                if ($ThresholdForWaitHeiWaist && $ThresholdForWaitHeiWaist[0]['min_wast'] && $ThresholdForWaitHeiWaist[0]['max_wast']) {
//                    $whereHeight = array('UserID' => $IndividualId);
//                    $min_wast = $ThresholdForWaitHeiWaist[0]['min_wast'];
//                    $max_wast = $ThresholdForWaitHeiWaist[0]['max_wast'];
//                    $ShowWaist = $this->getUpDownThresholdWaist($whereHeight, $min_wast, $max_wast);
//                    if ($ShowWaist['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowWaist;
//                    }
//                }
//            }
            /*             * ****Code to get Weight Data****** */
//            if ($i < 3) {
//                $ThresholdForWaitHeiWaist = $this->getBodyMeasurement(array("status" => 1, "caregiver_id" => $UserID), array('min_height', 'max_height', 'min_wast', 'max_wast', 'min_weight', 'max_weight'));
//                if ($ThresholdForWaitHeiWaist && $ThresholdForWaitHeiWaist[0]['min_weight'] && $ThresholdForWaitHeiWaist[0]['max_weight']) {
//                    $whereHeight = array('UserID' => $IndividualId);
//                    $min_weight = $ThresholdForWaitHeiWaist[0]['min_weight'];
//                    $max_weight = $ThresholdForWaitHeiWaist[0]['max_weight'];
//                    $ShowHeight = $this->getUpDownThresholdWeight($whereHeight, $min_weight, $max_weight);
//                    if ($ShowHeight['Data']) {
//                        $i++;
//                        $returnDataArr[] = $ShowHeight;
//                    }
//                }
//            }


            return $returnDataArr;
        } catch (Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUpDownTemp($where, $min_temp, $max_temp, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_temperature_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('temperature_id DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $min_temp && $max_temp) {
                if ($min_temp < $res[0]['temperature'] && $max_temp > $res[0]['temperature']) {
                    $returnArray = array('Data' => 0);
                } elseif ($min_temp > $res[0]['temperature']) {
                    $displayData = ($min_temp - $res[0]['temperature']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' &#8457', 'label' => 'Temperature');
                } else {
                    $displayData = ($res[0]['temperature'] - $max_temp);
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' &#8457', 'label' => 'Temperature');
                }
            } else {
                $returnArray = array('Data' => '0');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getThresholdTemp($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_super_threshold_temperature'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUpDownPluse($where, $min_pulse, $max_pulse, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_blood_pressure_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('bpr_id DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $min_pulse && $max_pulse) {
                if ($min_pulse < $res[0]['pulse'] && $max_pulse > $res[0]['pulse']) {
                    $returnArray = array('Data' => 0);
                } elseif ($min_pulse > $res[0]['pulse']) {
                    $displayData = ($min_pulse - $res[0]['pulse']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' bpm', 'label' => 'Pulse');
                } else {
                    $displayData = ($res[0]['pulse'] - $max_pulse);
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' bpm', 'label' => 'Pulse');
                }
            } else {
                $returnArray = array('Data' => '0');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getThresholdPluse($where = array(), $columns = array(), $age = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_super_threshold_pulse'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->where(new \Zend\Db\Sql\Predicate\Expression("us.age_from <= '$age'"));
            $select->where(new \Zend\Db\Sql\Predicate\Expression("us.age_to >= '$age'"));
            $select->order('id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUpDownDystolic($where, $min_diastolic, $max_diastolic, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_blood_pressure_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('bpr_id DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $min_diastolic && $max_diastolic) {
                if ($min_diastolic < $res[0]['diastolic'] && $max_diastolic > $res[0]['diastolic']) {
                    $returnArray = array('Data' => 0);
                } elseif ($min_diastolic > $res[0]['diastolic']) {
                    $displayData = ($min_diastolic - $res[0]['diastolic']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' mm Hg', 'label' => 'Diastolic');
                } else {
                    $displayData = ($res[0]['diastolic'] - $max_diastolic);
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' mm Hg', 'label' => 'Diastolic');
                }
            } else {
                $returnArray = array('Data' => '0');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getThresholdDystolic($where = array(), $columns = array(), $age = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_super_threshold_diastolic'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->where(new \Zend\Db\Sql\Predicate\Expression("us.age_from <= '$age'"));
            $select->where(new \Zend\Db\Sql\Predicate\Expression("us.age_to >= '$age'"));
            $select->order('id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getUpDownSystolic($where, $min_systolic, $max_systolic, $columns = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'choles_rec' => 'hm_blood_pressure_records'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('bpr_id DESC');
            $select->limit(1);
            $statement = $sql->prepareStatementForSqlObject($select);
            $res = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            $returnArray = array();
            if (count($res) && $min_systolic && $max_systolic) {
                if ($min_systolic < $res[0]['systolic'] && $max_systolic > $res[0]['systolic']) {
                    $returnArray = array('Data' => 0);
                } elseif ($min_systolic > $res[0]['systolic']) {
                    $displayData = ($min_systolic - $res[0]['systolic']);
                    $returnArray = array('Data' => 1, 'class' => 'low', 'displayData' => $displayData . ' mm Hg', 'label' => 'Systolic');
                } else {
                    $displayData = ($res[0]['systolic'] - $max_systolic);
                    $returnArray = array('Data' => 1, 'class' => 'high', 'displayData' => $displayData . ' mm Hg', 'label' => 'Systolic');
                }
            } else {
                $returnArray = array('Data' => '0');
            }
            return $returnArray;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getThresholdSystolic($where = array(), $columns = array(), $age = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_super_threshold_systolic'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->where(new \Zend\Db\Sql\Predicate\Expression("us.age_from <= '$age'"));
            $select->where(new \Zend\Db\Sql\Predicate\Expression("us.age_to >= '$age'"));
            $select->order('id DESC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
