<?php

/* * ***********************
 * PAGE: USE TO MANAGE SUPER ADMIN CONDITION PANNEL.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: ANKIT SHUKLA(fb/gshukla67).
 * CREATOR: 26/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Researcher\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class ResearcherCandiTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_super_conditions';
    public $subTable = 'hm_super_subconditions';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function insertSubCond($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->subTable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertCond($data = array()) {
        
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->table);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAdminConditions($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array()) {

        $subcondition = array("sub_id", "sub_common_name");
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => $this->table
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->join(array('sb' => $this->subTable), "us.id = sb.condition_id", $subcondition, 'LEFT');
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order('common_name ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAdminSubConditions($where = array(), $columns = array(), $lookingfor = NULL) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'sb' => $this->subTable
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if ($lookingfor) {
                
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order('sub_common_name ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            // echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
