<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rabban Ahmad
 * CREATOR: 16/2/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;

class IndividualCaregiverTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_caregivers';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     * This function is used to save contact iformation
     *
     * ******* */

    public function saveCareGiver($dataArry) {
        try {
            $this->insert($dataArry);
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateCareGiver($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            // echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            if ($result) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function deleteCareGiver($where = array()) {
        try {

            $delete = $this->delete($where);
            if ($delete) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getCareGiver($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'care' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $user = array('FirstName', 'LastName', 'UserName', 'Image', 'Email', 'Dob','short_description');
            $select->join(array('us' => 'hm_users'), "care.caregiver_id = us.UserID", $user, 'INNER');
            $select->join(array('hm_con' => 'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
            $select->join(array('states' => 'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
            $select->join(array('country' => 'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');

            if ($lookingfor) {
                foreach ($lookingfor as $k => $v) {
                    $select->where->NotequalTo('care.caregiver_id', $v);
                }
            }
            $select->group('care.caregiver_id');
            $statement = $sql->prepareStatementForSqlObject($select);
            //echo  $select->getSqlString();exit;

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            //echo $select->getSqlString($this->getAdapter()->getPlatform());die;
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
     /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getCaregiverThresholdSysDysPluse($where = array(), $columns = array(), $lookingfor = false,$age='',$gender) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'care' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $user = array();
            $select->join(array('thpulse' => 'hm_care_threshold_pulse'), new \Zend\Db\Sql\Predicate\Expression("care.caregiver_id = thpulse.caregiver_id AND thpulse.gender='".$gender."' AND thpulse.age_from <='".$age."' AND thpulse.age_to >='".$age."'"), array('min_pulse','max_pulse'), 'LEFT');
            $select->join(array('thsys' => 'hm_care_threshold_systolic'), new \Zend\Db\Sql\Predicate\Expression("care.caregiver_id = thsys.caregiver_id AND thsys.gender='".$gender."' AND thsys.age_from <='".$age."' AND thsys.age_to >='".$age."'"), array('max_systolic','min_systolic'), 'LEFT');
            $select->join(array('thdys' => 'hm_care_threshold_diastolic'), new \Zend\Db\Sql\Predicate\Expression("care.caregiver_id = thdys.caregiver_id AND thdys.gender='".$gender."' AND thdys.age_from <='".$age."' AND thdys.age_to >='".$age."'"), array('min_diastolic','max_diastolic'), 'LEFT');
            $select->join(array('thtemp' => 'hm_care_threshold_temperature'),"care.caregiver_id = thdys.caregiver_id", array('min_temp','max_temp'), 'LEFT');
            $select->join(array('hmUser' => 'hm_users'),"care.caregiver_id = hmUser.UserId", array('Email','FirstName'), 'LEFT');
           

            if ($lookingfor) {
                foreach ($lookingfor as $k => $v) {
                    $select->where->NotequalTo('care.caregiver_id', $v);
                }
            }
            $select->group('care.caregiver_id');
            $statement = $sql->prepareStatementForSqlObject($select);
              //echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//die;

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            //echo $select->getSqlString($this->getAdapter()->getPlatform());die;
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getCareGiverCount($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'care' => $this->table
            ));

            $userArr = array('count' => new \Zend\Db\Sql\Expression('COUNT(*)'));
            if (count($where) > 0) {
                $select->where($where);
            }
            if (count($userArr) > 0) {
                $select->columns($userArr);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            if ($user) {
                return $user[0]['count'];
            }
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ******This function is used to delete the Geofence************* */

    public function deleteGps($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from('hm_user_geofence');

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ******This function is used to delete the Geofence************* */

    public function deleteAssociatedBeacons($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from('hm_associated_beacons');

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ******This function is used to delete the Geofence************* */

    public function deleteBeaconsRule($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from('hm_beacon_rule');

            if (count($where) > 0) {
                $delete->where($where);
            }

            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}

