<?php

/* * *******
 * Purpose:This Model class is used to Manage Subscription Table
 * Created:7/04/15
 * By: Affle Appstudioz
 *   
 * ****************** */

namespace Caregiver\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

Class CareSubscriptionTable Extends AbstractTableGateway {

    public $table = 'hm_super_pricing_matrix';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getSubscriptionDetails($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'subs' => $this->table
            ));
            if (count($where) > 0) {
                $select->where($where);
            }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->order('noOfUsers ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
            $subscription = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $subscription;
        } catch (Exception $ex) {
            throw new \Exception($ex->getPrevious()->getMessage());
        }
    }
    
    /*********This function is used to check this user is subscribed or not and check for subscription status**************/
    public function getSubscriptionStatus($where=array(),$columns=array()){
        try{
             $sql = new Sql($this->getAdapter());
                $select = $sql->select()->from(array(
                    'pms' => 'hm_pricing_matrix_subscription'
                ));

                if (count($where) > 0) {
                    $select->where($where);
                }

                if (count($columns) > 0) {
                    $select->columns($columns);
                }
                $date = date("Y-m-d");
                $user = array('NoOfUsers'=>'user');
                $select->join(array('spm' => 'hm_super_pricing_matrix'), "pms.price_id = spm.id",$user, 'INNER');
                $select->where(new \Zend\Db\Sql\Predicate\Expression("pms.start_date <= '$date'"));
                $select->where(new \Zend\Db\Sql\Predicate\Expression("pms.end_date >= '$date'"));
                $statement = $sql->prepareStatementForSqlObject($select);
                
                //$statement->prepare();
                //echo $statement->getSql();exit;
                //echo     $select->getSqlString();exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (Exception $ex) {
            throw new \Exception($ex->getPrevious()->getMessage());
        }
    }

}
