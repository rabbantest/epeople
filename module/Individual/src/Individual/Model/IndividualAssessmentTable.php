<?php
/*************************
    * PAGE: USE TO MANAGE THE Individual USER FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Shivendra Suman
    * CREATOR: 26/02/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class IndividualAssessmentTable extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_super_assesments';
    public $tableques = 'hm_super_ass_study_questions';
    public $tablequesans = 'hm_selfassess_ques_answer';
    public $tablesqo = 'hm_super_ass_study_questions_option';
    public $tablepart = 'hm_selfassess_participation';
    public $tablequans = 'hm_selfassess_ques_answer';
    public $tablecat = "hm_super_asscategorys";



    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getassessment($where = array(), $columns = array(), $lookingfor = false,$notEqual=array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asses' => $this->table
            ));
            
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }
            
             $select->join(array('cat' => $this->tablecat), "cat.cat_id = asses.ass_category", array("cat_name"), 'LEFT');
          
            if (count($notEqual)) {
                foreach ($notEqual as $k => $v) {
                    $select->where->NotequalTo($k, $v);
                }
            }
           
 
            if (count($columns) > 0) {
                $select->columns($columns);
            }
           
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
       // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
      /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
       * @get question list
    *****/
    
    public function getassessmentques($where = array(), $columns = array(), $lookingfor = false, $searchContent = array(), $limit = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'assesqul' => $this->tableques
            ));
            
            if (is_array($where) && count($where) > 0) {
                $select->where($where); 
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
           
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
            
            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
           //echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            //$statement->prepare();
          //  echo $statement->getSql();exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
        
    public function getassquesopt($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'sqo' => $this->tablesqo
            ));
            
            if (is_array($where) && count($where) > 0) {
                $select->where($where); 
        
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
           
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
       // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
       /*
     * @get  partishipation data 
     */
    
            
    public function getPartish($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'part' => $this->tablepart
            ));
            
            if (is_array($where) && count($where) > 0) {
                $select->where($where); 
        
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
           
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
       // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    /*
     * @update partishipation
     */
    
      public function updatePartish($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->tablepart);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            //echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute(); 
            if($result){
                return true;
            }  else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    } 
	 /*
     * @Insert partishipation
     */
	public function insertPartish($data) {
         try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->tablepart);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $resultDta = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE); 
            if($resultDta){
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            }else{
            $result = false;
            }
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    } 
    
    
          /*
     * @get  partishipation data  question answer
     */
    
            
    public function getPartQuAns($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'partquans' => $this->tablequans
            ));
            
            if (is_array($where) && count($where) > 0) {
                $select->where($where); 
        
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
           
            if ($lookingfor) {
            $select->join(array('opt' => $this->tablesqo), "opt.opt_id = partquans.option_id", array("opt_value"), 'LEFT');
            //$select->order($lookingfor);	
            }
       // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
    /*
     * @update partishipation question answer
     */
    
      public function updatePartQuAns($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->tablequans);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            //echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute(); 
            if($result){
                return true;
            }  else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    } 
	 /*
     * @Insert partishipation question answer
     */
	public function insertPartQuAns($data) {
         try {             
             $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->tablequans);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
           // echo $selectString;exit;
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
            
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());           
        }
    } 
    
    
    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getcat($where = array(), $columns = array(), $lookingfor = false,$notEqual=array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cat' => $this->tablecat
            ));
            
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }
            
            if (count($notEqual)) {
                foreach ($notEqual as $k => $v) {
                    $select->where->NotequalTo($k, $v);
                }
            }
           
 
            if (count($columns) > 0) {
                $select->columns($columns);
            }
           
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
       // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
     
}
