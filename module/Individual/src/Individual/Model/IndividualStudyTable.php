<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE STUDY USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman
 * CREATOR: 20/04/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class IndividualStudyTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_super_study_bucket_invitation';
    public $tablestud = 'hm_super_studies';
    public $tablepart = 'hm_selfassess_participation';
    public $tablesqo = 'hm_super_ass_study_questions_option';
    public $tableprivacy = 'hm_users_privacy_settings';
    public $tableeligable = 'hm_study_eligible_users';
    public $tableadminfilter = 'hm_super_study_main_que_filter';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function insertbucket($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->table);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getStudy($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0, $study_status= "NOTAVA") {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud' => $this->table
            ));
            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'end_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("end_date >= '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                      //  $select->where->
                    }
                }
            }
//            if($study_status !== "NOTAVA"){
//               // $select->where->notEqualTo("part.part_status", $study_status);
//            }
            $select->where->notIn("bucket", array("-2"));
            $select->join(array("stli" => $this->tablestud), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=stli.study_id and stli.study_status=1 and stli.publish_status=1"), array("*"), "LEFT");
            $select->join(array("part" => $this->tablepart), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=part.study_id and stud.user_id=part.UserID"), array("last_page", "part_status", "study_type", "part_startdate", "part_enddate"), "LEFT");
           
          //  $select->join(array("part" => $this->tablepart), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=part.study_id and stud.user_id=part.UserID and part.study_type=$type"), array("last_page", "part_status", "study_type", "part_startdate", "part_enddate"), "LEFT");
//           if($study_status !== "NOTAVA"){
//            $select->join(array("part" => $this->tablepart), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=part.study_id and stud.user_id=part.UserID AND part.part_status != $study_status"), array("last_page", "part_status", "study_type", "part_startdate", "part_enddate"), "LEFT");
//           }else{
//               
//           }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchcontent) > 0) {
                
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
       //   echo $select->getSqlString();exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
 public function getStudyArchive($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $orwhere= '') {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud' => $this->table
            ));
            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'start_date') {
                       // $select->where(new \Zend\Db\Sql\Predicate\Expression("start_date <= 'DATE_SUB(CURDATE(),$value)'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            
            if(!empty($orwhere)){
               // echo $orwhere;exit;
                $select->where($orwhere);
              //  echo $orwhere;
            }
            $select->where->notIn("bucket", array("-2"));
            $select->join(array("stli" => $this->tablestud), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=stli.study_id and stli.study_status=1 and stli.publish_status=1"), array("*"), "LEFT");

            $select->join(array("part" => $this->tablepart), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=part.study_id and stud.user_id=part.UserID"), array("last_page", "part_status", "study_type", "part_startdate", "part_enddate"), "LEFT");

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchcontent) > 0) {
                
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
           //echo $select->getSqlString();exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getAllStudiesPoints($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'pari_stu' => 'hm_selfassess_participation'
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }
            $select->join(array('stu' => 'hm_super_studies'), "stu.study_id = pari_stu.study_id", array(), 'LEFT');

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            if ($data && $data[0]['points'] != '') {
                $data = $data[0];
            } else {
                $data = array('points' => 0);
            }
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*****
     * This function is used to get the redeem points
     * **********/
    
     public function getUserAllRedeemPoints($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'points_history' => 'hm_users_redeem_history'
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }
            //$select->join(array('stu' => 'hm_super_studies'), "stu.study_id = pari_stu.study_id", array(), 'LEFT');

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            if ($data && $data[0]['Redeem_points'] != '') {
                $data = $data[0];
            } else {
                $data = array('Redeem_points' => 0);
            }
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getSupeStudyOnly($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud' => $this->tablestud
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //  echo $select->getSqlString();exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function isStudy($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'studls' => $this->tablestud
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //   echo $select->getSqlString();exit;            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateBucket($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
//               echo $update->getSqlString();exit; 
//            echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            if ($result) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * @Action check filteration for main
     * *** */

    public function filterForMain($where = array(), $columns = array(), $lookingfor = false, $wherrFil = false, $count = NULL) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'elmain' => $this->tableeligable
            ));

            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()) . $wherrFil);
            //echo $query;exit;
            $statement = $this->adapter->query($query);
            if ($count == 1) {
                $user = $this->resultSetPrototype->initialize($statement->execute())->count();
            } else {
                $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            }
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * @Action check filteration for main set by admin
     * *** */

    public function filterForMainByAdmin($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'elmainbyad' => $this->tableadminfilter
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    public function get($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array()) {
        $orderBy = "category asc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'trial' => 'hm_super_onsite_study_trial'
            ));
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }
            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }
            if (count($where) > 0) {
                $select->where($where);
            }
            $select->order($orderBy);
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getStudytrials($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0, $notEqual = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud' => $this->tablestud
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchcontent) > 0) {
                
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //  echo $select->getSqlString();
            //  exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getStudytrialsbuck($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0, $notEqual = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'studbuc' => $this->table
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchcontent) > 0) {
                
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            //  echo $select->getSqlString();
            //  exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getStudylist($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud' => $this->table
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            $select->join(array("stli" => $this->tablestud), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=stli.study_id and stli.study_status=1 and stli.publish_status=1"), array("*"), "LEFT");

            $select->join(array("part" => $this->tablepart), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=part.study_id and stud.user_id=part.UserID and part.study_type=$type"), array("last_page", "part_status", "study_type", "part_startdate", "part_enddate"), "LEFT");

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchcontent) > 0) {
                
            }
            if ($lookingfor) {
                //   $select->order($lookingfor);
            }
            //   echo $select->getSqlString();exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getStudyInvitationSet($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stud_invi' => 'hm_super_study_invitation_set'
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->join(array('stu' => 'hm_super_studies'), "stu.study_id = stud_invi.invitation_set_study", array('screening'), 'lEFT');

            //echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * **This function is used to in**** */

    public function DeleteStudyInvitaion($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from('hm_super_study_invitation_set');
            if (count($where) > 0) {
                $delete->where($where);
            }
            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * **This function is used to in**** */

    public function insertStudyInvitaion($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert('hm_super_study_bucket_invitation');
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /* public function getStudytrials($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array(), $type = 0, $notEqual = false) {

      try {
      $sql = new Sql($this->getAdapter());
      $select = $sql->select()->from(array(
      'stud' => $this->table
      ));
      if (is_array($where) && count($where) > 0) {
      $select->where($where);
      }
      if ($notEqual) {
      $select->where->nest()->notEqualTo("stud.user_id", $notEqual)
      ->or->nest()->equalTo("stud.user_id", $notEqual)
      ->equalTo("bucket", "-2");
      }


      $select->join(array("stli" => $this->tablestud), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=stli.study_id and stli.study_status=1 and stli.publish_status=1"), array("*"), "LEFT");
      $select->join(array("part" => $this->tablepart), new \Zend\Db\Sql\Predicate\Expression("stud.study_id=part.study_id and stud.user_id=part.UserID and part.study_type=$type"), array("last_page", "part_status", "study_type", "part_startdate", "part_enddate"), "LEFT");
      if (count($columns) > 0) {
      $select->columns($columns);
      }
      if (count($searchcontent) > 0) {

      }
      if ($lookingfor) {
      $select->order($lookingfor);
      }
      // echo $select->getSqlString();exit;

      $statement = $sql->prepareStatementForSqlObject($select);
      $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
      return $data;
      } catch (\Exception $e) {
      throw new \Exception($e->getPrevious()->getMessage());
      }
      } */
}
