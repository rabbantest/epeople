<?php
/*************************
    * PAGE: USE TO MANAGE THE Individula Formula FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Shivendra Suman
    * CREATOR: 12/03/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class IndividualAssesFormulaTable extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
        public $table    ='hm_super_health_metrix';
        public $tableformula = 'hm_selfassess_formulas';
        public $tableqopt   ='hm_super_ass_study_questions_option';
        public $tablecols = 'hm_super_health_metrix_cols';
        public $tabledisform = 'hm_selfassess_formula_display';
        public $tablequest = 'hm_super_ass_study_questions';
        


    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function gethm($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'hm' => $this->table
            ));
            
           if (is_array($where) && count($where) > 0) {         
                $select->where($where);
            }
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
              $select->order($lookingfor);
            }
          
			//echo $select;die;
            $statement = $sql->prepareStatementForSqlObject($select);
			  /* $statement->prepare();
			  echo $statement->getSql();exit; */
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
      /*      *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getAssformula($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'formula' => $this->tableformula
            ));
            
           if (is_array($where) && count($where) > 0) {         
                $select->where($where);
            }
            if (is_array($where) && count($where) > 0) {         
                $select->where($where);
            }
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                    $select->join(array('hm' => $this->table), "hm.health_id = formula.formula_health", array("type","health_id","col_name"), 'LEFT');
             // $select->order($lookingfor);
            }
            $statement = $sql->prepareStatementForSqlObject($select);			
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
       public function getAssformuladQuest($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'formula' => $this->tableformula
            ));
            
           if (is_array($where) && count($where) > 0) {         
                $select->where($where);
            }
            if (is_array($where) && count($where) > 0) {         
                $select->where($where);
            }
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                    $select->join(array('hm' => $this->table), "hm.health_id = formula.formula_health", array("type","col_name"), 'LEFT');
                    
             // $select->order($lookingfor);
            }
            $statement = $sql->prepareStatementForSqlObject($select);			
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
        /*      *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getAssOpionList($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'formula' => $this->tableformula
            ));
            
           if (is_array($where) && count($where) > 0) {         
                $select->where($where);
            }
            if (is_array($in) && count($in) > 0) {         
                $select->where($where);
            }
            
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
              $select->order($lookingfor);
            }
            $statement = $sql->prepareStatementForSqlObject($select);			
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    
    
    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getcols($where = array(), $columns = array(), $lookingfor = false,$in=array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cols' => $this->tablecols
            ));
            
           if (is_array($where) && count($where) > 0) {         
                $select->where($where);
            }
            if (is_array($in) && count($in) > 0) {  
                  $select->where->in("col_health",$in);
                
            }
            
         //   exit;
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
             // $select->order($lookingfor);
                 $select->join(array('hm' => $this->table), "hm.health_id = cols.col_health", array("col_name"), 'LEFT');
            }
          
			//echo $select;die;
            $statement = $sql->prepareStatementForSqlObject($select);
//			  $statement->prepare();
//			  echo $statement->getSql();exit; 
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    //display formula
    public function getdisplayFormula($where = array(), $columns = array(), $lookingfor = false){ //      
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'disformula' => $this->tabledisform
            ));
            
           if (is_array($where) && count($where) > 0) {         
                $select->where($where);
            }
           
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
              $select->order($lookingfor);
            }
            $statement = $sql->prepareStatementForSqlObject($select);			
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
   
}
