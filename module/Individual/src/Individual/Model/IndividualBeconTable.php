<?php
/*************************
    * PAGE: USE TO MANAGE User follower relation
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Rabban Ahmad
    * CREATOR: 22/1/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class IndividualBeconTable extends AbstractTableGateway
{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_users_becons';
    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }
    
      /*********
        *
        * @param array $where            
	*****/
    
    public function SaveBecon($data = array()) {
        try {
            $insert =   $this->insert($data); 
            if($insert){
				$lastId = $this->adapter->getDriver()->getLastGeneratedValue();
                return $lastId;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
    
	/*
	@ : This function is used to udpate follower status accordingly
	@ : Created: 22/1/15
	*/
	 public function updateBecon($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute(); 
			return true;
        } catch (\Exception $e) {
			echo $e->getPrevious()->getMessage();die;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	
    public function getBecons($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array()) {
	try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'bec' => $this->table
            ));
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
				$user=array('FirstName','LastName','UserName','Image','Email','Dob');
				//$select->join(array('us' =>'hm_users'), "bec.UserID = us.UserID", $user, 'INNER');
				// $select->join(array('hm_con' =>'hm_contacts_info'), "hm_con.user_id = bec.UserID", array(), 'INNER');
				// $select->join(array('states' =>'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'INNER');
				// $select->join(array('country' =>'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'INNER');
				
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            //$select->group('bec.UserID');
            $statement = $sql->prepareStatementForSqlObject($select);
            //echo '<pre>'; print_r($statement); die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	
	/*********
	@ : This method is used to get the associated  caregvier ids
	@ : Created 10/04/15
	***********/
	public function getAssociatedCaregiverIds($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array()) {
	try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asb' => 'hm_associated_beacons'
            ));
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
				
				$select->join(array('user_b' =>'hm_users_becons'), "user_b.id = asb.beacon_id", array(), 'INNER');
				// $select->join(array('hm_con' =>'hm_contacts_info'), "hm_con.user_id = bec.UserID", array(), 'INNER');
				// $select->join(array('states' =>'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'INNER');
				// $select->join(array('country' =>'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'INNER');
				
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            //$select->group('bec.UserID');
            $statement = $sql->prepareStatementForSqlObject($select);
           //echo $select->getSqlString($this->getAdapter()->getPlatform());die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	
	/*********
	@ : This method is used to get the associated  caregvier ids
	@ : Created 10/04/15
	***********/
	public function getAssociatedCaregiverDetails($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array()) {
	try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'asb' => 'hm_associated_beacons'
            ));
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
				
				$user=array('FirstName','LastName','UserName','Image','Email','Dob');
				$select->join(array('us' =>'hm_users'), "asb.caregiver_id = us.UserID", $user, 'INNER');
				//$select->join(array('user_b' =>'hm_users_becons'), "user_b.id = asb.beacon_id", array(), 'INNER');
				//$select->join(array('hm_con' =>'hm_contacts_info'), "hm_con.user_id = bec.UserID", array(), 'INNER');
				// $select->join(array('states' =>'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'INNER');
				// $select->join(array('country' =>'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'INNER');
				
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields=>$data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                            new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            //$select->group('bec.UserID');
            $statement = $sql->prepareStatementForSqlObject($select);
           //echo $select->getSqlString($this->getAdapter()->getPlatform());die;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	
	/***********************
	@ : This method is used to get the connected users details
	@ : 22/1/15
	*************/
    public function getUserFollowerDetails($where = array(), $columns = array(), $notEqual = array(),$searchContent = array()) 
	{
	try {
			$this->_sessionObj = new Container('frontend');
			$UserID = $this->_sessionObj->userDetails['UserID'];
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'follow' => $this->table
            ));
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
			$user=array('UserID','FirstName','LastName','UserName','Image','Email','Dob');
			$select->join(array('us' =>'hm_users'), "follow.follower_id = us.UserID", $user, 'INNER');
            $select->join(array('hm_con' =>'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
			$select->join(array('states' =>'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
			$select->join(array('country' =>'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');
			if(count($notEqual)){
				foreach($notEqual as $k=>$v){
					$select->where->NotequalTo($k, $v);
				}
			}
            if (count($where) > 0) {
                $select->where($where);
            }
            
             if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }
                $select->group('us.UserID');
			//$select->where(new \Zend\Db\Sql\Predicate\Expression("follow.creation_date <= 'DATE_SUB(CURDATE(),INTERVAL 30 DAYS)'"));
            //$select->order('common_name ASC');
            $statement = $sql->prepareStatementForSqlObject($select);
//             $statement->prepare();
//			echo $statement->getSql();exit; 
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
	/*
	@ : This function is used to delete the symtoms
	*/
	public function deleteBecon($where = array()) {
        try {
				$this->delete($where);
				return true;
		} catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
}
	