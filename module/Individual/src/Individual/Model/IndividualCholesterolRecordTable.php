<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman
 * CREATOR: 16/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class IndividualCholesterolRecordTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_cholesterol_records';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getCholestrolRecord($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cho' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'CholesterolRecordDate') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("CholesterolRecordDate <= 'DATE_SUB(CURDATE(),$value)'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
//            echo '<pre>';
//            print_r($data);exit;
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function insertCholestrolRecord($data = array()) {
        try {

            $insert = $this->insert($data);
            if ($insert) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getCholestrolYearMontlyData($where = array(), $lookingfor = false) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cho' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'CholesterolRecordDate') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("CholesterolRecordDate <= 'DATE_SUB(CURDATE(),$value)'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('monthname(CholesterolRecordDate) As Month'),
                        new \Zend\Db\Sql\Expression('year(CholesterolRecordDate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(LDL) As AvgLDL'),
                        new \Zend\Db\Sql\Expression('avg(HDL) As AvgHDL'),
                        new \Zend\Db\Sql\Expression('avg(Triglycerides) As AvgTriglycerides'),
                    )
            );
      
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            $select->group(
                   array( new \Zend\Db\Sql\Expression('monthname(CholesterolRecordDate)'), new \Zend\Db\Sql\Expression('year(CholesterolRecordDate)'))
            );
          
            $statement = $sql->prepareStatementForSqlObject($select);

            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            
        }
    }
    
    /*
     * get record by year, month, week
     */
    
    public function getCholestrolRecordByYear($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cho' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
              foreach ($where as $key => $value) {
                    if ($key == 'start_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("CholesterolRecordDate >= '$value'"));
                             
                    }elseif ($key == 'end_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("CholesterolRecordDate <= '$value'"));
                             
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
             $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(CholesterolRecordDate) AS Day'),
                        new \Zend\Db\Sql\Expression('monthname(CholesterolRecordDate) As Month'),
                        new \Zend\Db\Sql\Expression('year(CholesterolRecordDate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(LDL) As AvgLDL'),
                        new \Zend\Db\Sql\Expression('avg(HDL) As AvgHDL'),
                        new \Zend\Db\Sql\Expression('avg(Triglycerides) As AvgTriglycerides'),
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }
            
            $statement = $sql->prepareStatementForSqlObject($select);
//               $statement->prepare();
//                echo $statement->getSql();
//               exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    /*
     * get record of one day
     */
    public function getCholestrolRecordOfDay($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cho' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'current_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("CholesterolRecordDate = '$value'"));
                    } elseif ($key == 'start_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("CholesterolRecordTime >= '$value'"));
                    } elseif ($key == 'end_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("CholesterolRecordTime < '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(CholesterolRecordDate) AS Day'),
                        new \Zend\Db\Sql\Expression('monthname(CholesterolRecordDate) As Month'),
                        new \Zend\Db\Sql\Expression('year(CholesterolRecordDate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(LDL) As AvgLDL'),
                        new \Zend\Db\Sql\Expression('avg(HDL) As AvgHDL'),
                        new \Zend\Db\Sql\Expression('avg(Triglycerides) As AvgTriglycerides'),
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $sql->getSqlstringForSqlObject($select);exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
