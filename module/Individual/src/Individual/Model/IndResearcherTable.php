<?php

/* * ***********************
 * PAGE: USE TO MANAGE SUPER ADMIN CONDITION PANNEL.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: HEMANT SINGH CHUPHAL.
 * CREATOR: 3/3/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Delete;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Db\Adapter\Driver\DriverInterface;

class IndResearcherTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_super_studies';
    public $partTable = 'hm_selfassess_participation';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

      /*     * ****This function is used to get the list of users who has participated in Domain studies********* */

    public function getResearcherlist($where = array(), $columns = array(), $notIn = '') {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'part_table' => $this->partTable
            ));
            if (count($where) > 0) {
                $select->where($where);
            }
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            $select->join(array('stu' =>$this->table), "stu.study_id = part_table.study_id", array('pass_researcher_id'), 'INNER');
            $select->join(array('us' => 'hm_users'), "stu.pass_researcher_id = us.UserId", array('Email', 'FirstName', 'LastName', 'Gender', 'Dob', 'Image'), 'INNER');
            $select->join(array('hm_con' => 'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
            $select->join(array('states' => 'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
            $select->join(array('country' => 'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');
            $select->group('stu.pass_researcher_id');

            if (!empty($notIn)) {
                $select->where->NotequalTo('stu.pass_researcher_id', $notIn);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;
            $result = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $result;
        } catch (Exception $ex) {
            throw new \Exception($ex->getPrevious()->getMessage());
        }
    }

    
}
