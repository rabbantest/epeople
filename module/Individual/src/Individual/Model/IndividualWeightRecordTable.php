<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman
 * CREATOR: 10/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class IndividualWeightRecordTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_weight_records';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getWeightRecord($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'wr' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'WeightRecordDate') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("WeightRecordDate <= 'DATE_SUB(CURDATE(),$value)'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function insertWeightRecord($data = array()) {
        try {
			
			
			
			
			
            //$insert = $this->insert($data);
            if ($insert) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getweightOneYearMontlyData($where = array(), $lookingfor = false, $forwhich = 'all') {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'wr' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'WeightRecordDate') {
                        if($value != 'all'){
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("WeightRecordDate <= 'DATE_SUB(CURDATE(),$value)'"));
                        }
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }

            if ($forwhich == 'all') {
                $select->columns(
                        array(
                            new \Zend\Db\Sql\Expression('monthname(WeightRecordDate) As Month'),
                            new \Zend\Db\Sql\Expression('year(WeightRecordDate) AS Year'),
                            new \Zend\Db\Sql\Expression('avg(Weight) As AvgValue')
                        )
                );
                if ($lookingfor) {
                    $select->order($lookingfor);
                }

                $select->group(
                        array(new \Zend\Db\Sql\Expression('monthname(WeightRecordDate)'),
                            new \Zend\Db\Sql\Expression('year(WeightRecordDate)'))
                );
            } else {
                $select->columns(
                        array(
                            new \Zend\Db\Sql\Expression('DAYNAME(WeightRecordDate) As Month'),
                            new \Zend\Db\Sql\Expression('MONTHNAME(WeightRecordDate) AS Year'),
                            new \Zend\Db\Sql\Expression('avg(Weight) As AvgValue')
                        )
                );

                if ($lookingfor) {
                    $select->order($lookingfor);
                }

                $select->group(
                        array(new \Zend\Db\Sql\Expression('DAYNAME(WeightRecordDate)'),
                        //  new \Zend\Db\Sql\Expression('year(WeightRecordDate)')
                        )
                );
            }

            $statement = $sql->prepareStatementForSqlObject($select);
			$statement->prepare();
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();

            return $data;
        } catch (\Exception $e) {
            
        }
    }
    
        /*
     * get record by year, month, week
     */

    public function getWeightRecordByYear($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cho' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'start_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("WeightRecordDate >= '$value'"));
                    } elseif ($key == 'end_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("WeightRecordDate <= '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(WeightRecordDate) As Day'),
                        new \Zend\Db\Sql\Expression('monthname(WeightRecordDate) As Month'),
                        new \Zend\Db\Sql\Expression('year(WeightRecordDate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(Weight) As Avgweight')
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
     /*
     * get record of one day
     */
    public function getWeightRecordOfDay($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cho' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'current_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("WeightRecordDate = '$value'"));
                    } elseif ($key == 'start_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("WeightRecordTime >= '$value'"));
                    } elseif ($key == 'end_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("WeightRecordTime < '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(WeightRecordDate) As Day'),
                        new \Zend\Db\Sql\Expression('monthname(WeightRecordDate) As Month'),
                        new \Zend\Db\Sql\Expression('year(WeightRecordDate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(Weight) As Avgweight')
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $sql->getSqlstringForSqlObject($select);exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }


}
