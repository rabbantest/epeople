<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE Individual USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman
 * CREATOR: 08/01/2015.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class IndividualDataSourceTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_validic_app_source';
    public $tablemd = 'hm_measurement_defaults';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    // insert user defalut source
    public function insertUserDefaultSource($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->tablemd);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            //echo $query = \str_replace(array("`"), array(""), $insert->getSqlString($this->getAdapter()->getPlatform()));
            //die;
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    // update user defalut source
    public function updateUserDefaultSource($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->tablemd);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            // echo $update->getSqlString();exit;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function geteDefaultSource($where = array(), $columns = array(), $lookingfor = false, $searchContent = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'mds' => $this->tablemd
            ));
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if (count($searchContent) > 0) {
                if (count($searchContent) > 0) {
                    $likeWhr = new \Zend\Db\Sql\Where();
                    foreach ($searchContent AS $fields => $data) {
                        if (!empty($data)) {
                            $likeWhr->addPredicate(
                                    new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                            );
                            $select->where($likeWhr);
                        }
                    }
                }
            }
            if ($lookingfor) {
                $select->order($lookingfor);
            }
            // echo $select->getSqlString();exit;

            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getSource($where = array(), $columns = array(), $lookingfor = false, $searchContent = array(), $filter = false) {

        //exit;
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'vas' => $this->table
            ));


            if ($filter) {
                $select->join(array('hcs' => 'hm_connected_sources'), "hcs.sourceId = vas.id", array(), 'INNER');
            }
            if (count($columns) > 0) {
                $select->columns($columns);
            }

            if (count($searchContent) > 0) {
                if (count($searchContent) > 0) {
                    $likeWhr = new \Zend\Db\Sql\Where();
                    foreach ($searchContent AS $fields => $data) {
                        if (!empty($data)) {
                            $likeWhr->addPredicate(
                                    new \Zend\Db\Sql\Predicate\Like("$fields", "%$data%")
                            );
                            $select->where($likeWhr);
                        }
                    }
                }
            }
            if (is_array($where) && count($where) > 0) {
                $select->where($where);
            }

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            // echo $select->getSqlString();
            $statement = $sql->prepareStatementForSqlObject($select);
            ///echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
            //die;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
