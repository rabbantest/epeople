<?php

namespace Individual\Model;

/* * ***********************
 * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rabban Ahmad
 * CREATOR: 10/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;

class IndividualUsersTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_users';
    public $condevictable = 'hm_connected_sources';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function getRedeemHistoy($where = array(), $columns = array(), $lookingfor = NULL, $searchContent = array(), $limit = array()) {

        $orderBy = "id desc";
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'Red' => 'hm_users_redeem_history'
            ));

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }

            if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
            if (count($searchContent) > 0) {
                $likeWhr = new \Zend\Db\Sql\Where();
                foreach ($searchContent AS $fields => $data) {
                    if (!empty($data)) {
                        $likeWhr->addPredicate(
                                new \Zend\Db\Sql\Predicate\Like("$fields", "$data%")
                        );
                        $select->where($likeWhr);
                    }
                }
            }

            if (count($where) > 0) {
                $select->where($where);
            }

            $select->order($orderBy);
//           echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//die;
            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function insertRedeemHistoy($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert('hm_users_redeem_history');
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getUser($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }
            $user = array('UserID', 'FirstName', 'LastName', 'UserName', 'Image', 'Email', 'Dob', 'IsTempPasswordUsed', 'short_description');
            $select->join(array('hm_con' => 'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
            $select->join(array('states' => 'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
            $select->join(array('country' => 'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');
            $statement = $sql->prepareStatementForSqlObject($select);
            //$statement->prepare();
            //echo $statement->getSql();exit;
            //echo     $select->getSqlString();exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function getConnectedValidicApp($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'connected' => 'hm_connected_sources'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }
            $select->join(array('source' => 'hm_validic_app_source'), "source.id = connected.sourceId", array("source_name", "source"), 'INNER');
            $statement = $sql->prepareStatementForSqlObject($select);
            //$statement->prepare();
            //echo $statement->getSql();exit;
            //echo     $select->getSqlString();exit;
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getIndividualUser($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }
            $user = array('UserID', 'FirstName', 'LastName', 'UserName', 'Image', 'Email', 'Dob', 'IsTempPasswordUsed');
            $select->join(array('hm_con' => 'hm_contacts_info'), "hm_con.user_id = us.UserID", array(), 'LEFT');
            $select->join(array('states' => 'hm_states'), "states.state_id = hm_con.state_id", array('state_name'), 'LEFT');
            $select->join(array('country' => 'hm_country'), "country.country_id = hm_con.country_id", array('country_name'), 'LEFT');
            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getUserExist($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }
            $statement = $sql->prepareStatementForSqlObject($select);
//echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//die;
           $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateUser($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            if ($result) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function saveUser($dataArry) {
        try {
            $this->insert($dataArry);
            $lastId = $this->adapter->getDriver()->getLastGeneratedValue();
            return $lastId;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function sendMail($to, $from, $subject, $body) {
        try {

            $message = new Message();
            $message->addTo($to)
                    ->addFrom($from)
                    ->setSubject($subject)
                    ->setBody($body);

            $transport = new SendmailTransport();
            $transport->send($message);
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /* Used for getting the Pass user limit */

    public function getPassUserLimit($where = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'pass' => 'hm_pass_subdomain'
            ));
            if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
//echo $query = \str_replace(array("`"), array(""), $select->getSqlString($this->getAdapter()->getPlatform()));
//die;
            $limitUser = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $limitUser[0]['panel_member'];
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /* Used for getting the list of language */

    public function getIncomeData() {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_income'
            ));

            $statement = $sql->prepareStatementForSqlObject($select);
            $country = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $country;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /* Used for getting the list of language */

    public function getLanguage() {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_language'
            ));

            $statement = $sql->prepareStatementForSqlObject($select);
            $country = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $country;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /* Used for getting the list of Blood type */

    public function getBloodType() {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_blood_type'
            ));

            $statement = $sql->prepareStatementForSqlObject($select);
            $country = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $country;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /* Used for getting the list of Ethinicy */

    public function getEthnicity($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_ethnicity'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $country = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $country;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /* Used for getting the list of Education */

    public function getEducation() {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'us' => 'hm_highest_education'
            ));

            $statement = $sql->prepareStatementForSqlObject($select);
            $country = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $country;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ******
      @ Purpose : This funciton is used to validate domain
      @ Created : Rabban
      @ Created Date : 16/04/15
     * ***** */

    public function validateDomain($domain = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'pass_sub' => 'hm_pass_subdomain'
            ));
            $domain['status'] = 1;
            if (count($domain) > 0) {
                $select->where($domain);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            $domain = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            if (count($domain)) {
                return $domain[0];
            }
        } catch (\Exception $e) {
            return false;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ***This function is used to get the researcher invitation hash******* */

    public function getResearcherInvitationHash($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'hash' => 'hm_pass_researcher_invitations_hash'
            ));
            if (count($where) > 0) {
                $select->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($select);
            $hash = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            if (count($hash)) {
                return $hash[0];
            }
        } catch (\Exception $e) {
            return false;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * ***This function is used to get the researcher invitation hash******* */

    public function removeResearcherInvitationHash($where = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $delete = $sql->delete();
            $delete->from('hm_pass_researcher_invitations_hash');
            if (count($where) > 0) {
                $delete->where($where);
            }
            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result = $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $result;
        } catch (\Exception $e) {
            return false;
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateResearcherInvitationStatus($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table('hm_pass_researcher_invitations_hash');
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            if ($result) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

// insert connected device 
    public function insertDevice($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert($this->condevictable);
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function saveStates($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert('hm_states');
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function saveCounties($data = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $insert = $sql->insert('hm_counties');
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->getAdapter()->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result = $this->getAdapter()->getDriver()->getLastGeneratedValue();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

//get connected device 
    public function getConDevice($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'cdt' => $this->condevictable
            ));

            if (count($where) > 0) {
                $select->where($where);
            }
// $select->join(array('hvas' => 'hm_validic_app_source'), "hm_con.user_id = us.UserID", array(), 'LEFT');
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }
            $statement = $sql->prepareStatementForSqlObject($select);
//  echo     $select->getSqlString();
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * **This function is used to get the Domain name ***** */

    public function getDomainName($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'subdomains' => 'hm_pass_subdomain'
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                
            }

            $statement = $sql->prepareStatementForSqlObject($select);

            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user[0];
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
