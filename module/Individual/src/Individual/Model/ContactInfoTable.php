<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rabban Ahmad
 * CREATOR: 10/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;

class ContactInfoTable extends AbstractTableGateway {
    /*     * *******
     *
     * @var String
     * *** */

    public $table = 'hm_contacts_info';

    /*     * *******
     *
     * @param Adapter $adapter            
     * *** */

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*     * *******
     * This function is used to save contact iformation
     *
     * ******* */

    public function save($dataArry) {
        try {
            $this->insert($dataArry);
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    public function updateContact($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }

            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            if ($result) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

    /*     * *******
     *
     * @param array $where            
     * @param array $columns            
     * @param string $lookingfor            
     * @throws \Exception
     * @return mixed
     * *** */

    public function getUserContactInfo($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'ci' => $this->table
            ));

            if (count($where) > 0) {
                $select->where($where);
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
                $select->join(array('states' => 'hm_states'), "states.state_id = ci.state_id", array('state_name'), 'LEFT');
                $select->join(array('country' => 'hm_country'), "country.country_id = ci.country_id", array('country_name'), 'LEFT');
                $select->join(array('coun' => 'hm_country'), "ci.origin_id = coun.country_id", array('origin_name'=>'country_name'), 'LEFT');
                $select->join(array('eth' => 'hm_ethnicity'), "ci.ethnicity = eth.ethnicity_id", array('ethnicity_name','ethnicity_id'), 'LEFT');
                $select->join(array('counti' => 'hm_counties'), "ci.county_id = counti.county_id", array('county_name','county_id'), 'LEFT');
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }

}
