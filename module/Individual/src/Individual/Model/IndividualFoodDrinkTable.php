<?php
/*************************
    * PAGE: USE TO MANAGE THE ADMIN USER FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Shivendra Suman
    * CREATOR: 11/12/2014.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class IndividualFoodDrinkTable extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_food_drink_records';

    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getWeightTarget($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'wair' => $this->table
            ));
            
           if (is_array($where) && count($where) > 0) {         
                    foreach ($where as $key => $value) {
                    if ($key == 'WeightRecordDate') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("WeightRecordDate <= 'DATE_SUB(CURDATE(),$value)'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
              $select->order($lookingfor);
            }
          
			//echo $select;die;
            $statement = $sql->prepareStatementForSqlObject($select);
			  /* $statement->prepare();
			  echo $statement->getSql();exit; */
            $user = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $user;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
      /*********
        *
        * @param array $data  array           
        * @throws \Exception
        * @return mixed
    *****/
    
    public function insertFoodDrinkRecord($data = array()) {
        try {
            
           $insert =   $this->insert($data); 
            if($insert){
                return true;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
	/*
	@ This method is used to udpate the target weight 
	@Created : 30/12/14
	@By : Rabban Ahmad
	*/
	 public function updateTargetWeight($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }
            //echo '<pre>'; print_r($update); die;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute(); 
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
        /*
	@ This method is used to get record by year, month, week
	@Created : 10/12/14
	@By : Shivendra Suman
	*/

    public function getFoodDrinkRecordByYear($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'fd' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'start_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("FoodDrinkRecordDate >= '$value'"));
                    } elseif ($key == 'end_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("FoodDrinkRecordDate <= '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(FoodDrinkRecordDate) As Day'),
                        new \Zend\Db\Sql\Expression('date(FoodDrinkRecordDate) As Datelog'),
                        new \Zend\Db\Sql\Expression('monthname(FoodDrinkRecordDate) As Month'),
                        new \Zend\Db\Sql\Expression('year(FoodDrinkRecordDate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(ServingSize) As AvgServingSize'),
                        new \Zend\Db\Sql\Expression('avg(NoOfServings) As AvgNoOfServings'),
                        new \Zend\Db\Sql\Expression('avg(Calories) As AvgCalories'),
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
//            $statement->prepare();
//            echo $statement->getSql();
//            exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    /*
    * get record of one day
     */
    public function getFoodDrinkRecordOfDay($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'fd' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'current_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("FoodDrinkRecordDate = '$value'"));
                    } elseif ($key == 'start_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("FoodDrinkRecordTime >= '$value'"));
                    } elseif ($key == 'end_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("FoodDrinkRecordTime < '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(FoodDrinkRecordDate) As Day'),
                        new \Zend\Db\Sql\Expression('date(FoodDrinkRecordDate) As Datelog'),
                        new \Zend\Db\Sql\Expression('monthname(FoodDrinkRecordDate) As Month'),
                        new \Zend\Db\Sql\Expression('year(FoodDrinkRecordDate) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(ServingSize) As AvgServingSize'),
                        new \Zend\Db\Sql\Expression('avg(NoOfServings) As AvgNoOfServings'),
                        new \Zend\Db\Sql\Expression('avg(Calories) As AvgCalories'),
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            //echo $sql->getSqlstringForSqlObject($select);exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
   
}
