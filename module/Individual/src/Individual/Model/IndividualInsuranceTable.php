<?php
/*************************
    * PAGE: USE TO MANAGE THE INDIVIDUAL INSURANCE.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Shivendra Suman
    * CREATOR: 05/02/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class IndividualInsuranceTable extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_user_insurance';
    public $instype = 'hm_insurance_type';


    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getInsRecord($where = array(), $columns = array(), $lookingfor = false, $searchcontent = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'insu' => $this->table
            ));
            
           if (count($where) > 0) {   
              
                   $select->where($where);
            }
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            
               if (count($searchcontent) > 0) {
                if (isset($searchcontent['keyword']) && !empty($searchcontent['keyword'])) {                    
                    $select->where->nest->like('plan_name', '%' . $searchcontent['keyword'] . '%')
                    ->or->like('group_number', '%' . $searchcontent['keyword'] . '%')
                    ->or->like('plan_code', '%' . $searchcontent['keyword'] . '%')
                    ->or->like('sucbscriber_name', '%' . $searchcontent['keyword'] . '%')
                    ->or->like('sucbscriber_number', '%' . $searchcontent['keyword'] . '%')
                    ->or->like('email', '%' . $searchcontent['keyword'] . '%')
                    ->or->like('alternate_email', '%' . $searchcontent['keyword'] . '%')
                    ->or->like('address', '%' . $searchcontent['keyword'] . '%')
                       ;
                    
                }
                  
                
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
        
  
            $statement = $sql->prepareStatementForSqlObject($select);
//           echo $select->getSqlString($this->getAdapter()->getPlatform());
//           exit;

            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
      /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    
    public function insertIns($data = array()) {
        try {
            
         $insert =   $this->insert($data); 
            if($insert){
                return true;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
    
    public function updateIns($where = array(), $columns = array()) {
        try {
            $sql = new Sql($this->getAdapter());
            $update = $sql->update();
            $update->table($this->table);
            if (count($columns) > 0) {
                $update->set($columns);
            }
            if (count($where) > 0) {
                $update->where($where);
            }  
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute(); 
            if($result){
                return true;
            }  else {
                return false;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    } 
    
        public function getInsType($where = array(), $columns = array(), $lookingfor = false, $searchconten = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'insty' => $this->instype
            ));
            
           if (is_array($where) && count($where) > 0) {         
                   $select->where($where);
            }
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
        
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
           
          
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    


}
