<?php
/*************************
    * PAGE: USE TO MANAGE THE Individual USER FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Shivendra Suman
    * CREATOR: 08/01/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class IndividualMoodRecordTable extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_mood_records';
    public $moodl = 'hm_mood_list';

    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getmoodRecord($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'mood' => $this->table
            ));
            
           if (is_array($where) && count($where) > 0) {         
                    foreach ($where as $key => $value) {
                    if ($key == 'mood_record_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("mood_record_date <= 'DATE_SUB(CURDATE(),$value)'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
          
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
      /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    
    public function insertmoodRecord($data = array()) {
        try {
            
         $insert =   $this->insert($data); 
            if($insert){
                return true;
            }  else {
                return false;
            }
            
        } catch (Exception $ex) {
             throw new \Exception($e->getPrevious()->getMessage());
        }
        
    }
    
                /*
     * get record by year, month, week
     */

    public function getmoodRecordByYear($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'mood' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'start_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("mood_record_date >= '$value'"));
                    } elseif ($key == 'end_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("mood_record_date <= '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(mood_record_date) As Day'),
                        new \Zend\Db\Sql\Expression('monthname(mood_record_date) As Month'),
                        new \Zend\Db\Sql\Expression('year(mood_record_date) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(general_mood_value) As AvgGMV'),
                        new \Zend\Db\Sql\Expression('avg(physical_mood_value) As AvgPMV'),
                        new \Zend\Db\Sql\Expression('avg(mental_mood_value) As AvgMMV')
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
//                        $statement->prepare();
//            echo $statement->getSql();
//            exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
     /*
     * get record of one day
     */
    public function getmoodRecordOfDay($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'mood' => $this->table
            ));

            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    if ($key == 'current_date') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("mood_record_date = '$value'"));
                    } elseif ($key == 'start_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("mood_record_time >= '$value'"));
                    } elseif ($key == 'end_time') {
                        $select->where(new \Zend\Db\Sql\Predicate\Expression("mood_record_time < '$value'"));
                    } else {
                        $select->where->equalTo($key, $value);
                    }
                }
            }
            $select->columns(
                    array(
                        new \Zend\Db\Sql\Expression('dayname(mood_record_date) As Day'),
                        new \Zend\Db\Sql\Expression('monthname(mood_record_date) As Month'),
                        new \Zend\Db\Sql\Expression('year(mood_record_date) AS Year'),
                        new \Zend\Db\Sql\Expression('avg(general_mood_value) As AvgGMV'),
                        new \Zend\Db\Sql\Expression('avg(physical_mood_value) As AvgPMV'),
                        new \Zend\Db\Sql\Expression('avg(mental_mood_value) As AvgMMV')
                    )
            );

            if ($lookingfor) {
                $select->order($lookingfor);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
          //  echo $sql->getSqlstringForSqlObject($select)."<br>";
            
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    /*
     *@ Mood list 
     */
       public function getMoodList($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'ml' => $this->moodl
            ));
            
            if (count($where) > 0) {
                $select->where($where);
            }
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
              $select->order($lookingfor);
            }	
            $statement = $sql->prepareStatementForSqlObject($select);
//            $statement->prepare();
//            echo $statement->getSql();
//            exit;
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
            return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
}
