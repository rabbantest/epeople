<?php
/*************************
    * PAGE: USE TO MANAGE THE STUDY USER FOR DB.
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Shivendra Suman
    * CREATOR: 21/04/2015.
    * UPDATED: --/--/----.
    * Zend Framework (http://framework.zend.com/)
    *
    * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/


namespace Individual\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Complysight\Service\UserAuthAdapter;
use Zend\Session\Container;
use Complysight\Service\UserPassword;
use Zend\Db\Sql\Update;
use Zend\Validator\Explode;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\XmlRpc\Value\String;


class IndividualStudyQuestion extends AbstractTableGateway{
    
       /*********
        *
        * @var String
    *****/
    public $table = 'hm_super_ass_study_questions';
    public $tablequeopt = 'hm_super_ass_study_questions_option';
    public  $tablepart = 'hm_selfassess_participation';
    public $tablesqo = 'hm_super_ass_study_questions_option';
    public $tablequans = 'hm_selfassess_ques_answer';
    public $tableqq = 'hm_super_questionnaire_question';
    public $tableqo = 'hm_super_questionnaire_option';
    public $tableq = 'hm_super_questionnaire';


    /*********
        *
        * @param Adapter $adapter            
    *****/
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getStudyQues($where = array(), $columns = array(), $lookingfor = false, $limit = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stq' => $this->table
            ));
              if (is_array($where) && count($where) > 0) {         
                 $select->where($where);
            }
           if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
             if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
          // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
 /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getQuesOpt($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'qutopt' => $this->tablequeopt
            ));
              if (is_array($where) && count($where) > 0) {         
                 $select->where($where);
            }
           if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
          // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
        /*
     * @get  partishipation data  question answer
     */
    
            
    public function getPartQuAns($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'partquans' => $this->tablequans
            ));
            
            if (is_array($where) && count($where) > 0) {
                $select->where($where); 
        
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
           
            if ($lookingfor) {
            $select->join(array('opt' => $this->tablesqo), "opt.opt_id = partquans.option_id", array("opt_value"), 'LEFT');
            //$select->order($lookingfor);	
            }
       // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
  /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getQuestionarQues($where = array(), $columns = array(), $lookingfor = false, $limit = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'tqq' => $this->tableqq 
            ));
              if (is_array($where) && count($where) > 0) {         
                 $select->where($where);
            }
           if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
             if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
          // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    /*********
        *
        * @param array $where            
        * @param array $columns            
        * @param string $lookingfor            
        * @throws \Exception
        * @return mixed
    *****/
    public function getQuestionairOpt($where = array(), $columns = array(), $lookingfor = false, $groupby = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'tqo' => $this->tableqo
            ));
              if (is_array($where) && count($where) > 0) {         
                 $select->where($where);
            }
           if (count($columns) > 0) {
                $select->columns($columns);
            }
            if($groupby){
                $select->group($groupby);
            }
            if ($lookingfor) {
            $select->order('tqo.opt_value ASC');	
            }
          // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
     public function getPartQuestionarAns($where = array(), $columns = array(), $lookingfor = false) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'partquans' => $this->tablequans
            ));
            
            if (is_array($where) && count($where) > 0) {
                $select->where($where); 
        
            }

            if (count($columns) > 0) {
                $select->columns($columns);
            }
           
            if ($lookingfor) {
            $select->join(array('opt' => $this->tableqo), "opt.opt_id = partquans.option_id", array("opt_value"), 'LEFT');
            //$select->order($lookingfor);	
            }
       // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    public function getGroupQuesDetails($where = array(), $columns = array(), $lookingfor = false, $limit = array()) {

        try {
            $sql = new Sql($this->getAdapter());
            $select = $sql->select()->from(array(
                'stq' => $this->tableq
            ));
              if (is_array($where) && count($where) > 0) {         
                 $select->where($where);
            }
           if (count($columns) > 0) {
                $select->columns($columns);
            }
            if ($lookingfor) {
            $select->order($lookingfor);	
            }
             if (count($limit) > 1) {
                $select->limit($limit[1])->offset($limit[0]);
            }
          // echo $select->getSqlString();exit;
            
            $statement = $sql->prepareStatementForSqlObject($select);
            $data = $this->resultSetPrototype->initialize($statement->execute())->toArray();
             return $data;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
}
