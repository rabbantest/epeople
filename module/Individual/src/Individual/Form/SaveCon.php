<?php
namespace Individual\Form;

use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Form;

class SaveCon extends Form{
	public function __construct ($name = null){
		parent::__construct('selectedcondition');
		$this->SetAttribute('method','post');
		$this->SetAttribute('action','individual-add-history');
		
		$this->add(array(
			'name' => 'selected_conditions', 
            'type' => 'Zend\Form\Element\Select', 
			'attributes' => array( 
                'title' => 'Select Condition', 
				'onClick'=>'addCondition()',
				'class'=>'select',
				'id'=>'selected_conditions'
            ), 
			'options' => array(
			), 
		));
		 $this->add(array('name' => 'c_submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'type' => 'button',
                'class'=>'orange_btn',
                'value' => '+ Add',
                'id' => 'addUserCondition'
            ),
        ));
			
	}
	
}

?>