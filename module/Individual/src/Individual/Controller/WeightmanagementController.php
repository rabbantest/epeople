<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ROOT CONTROLLER FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 07/12/2014.
 * UPDATED: 07/12/2014.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class WeightmanagementController extends AbstractActionController {

    protected $IndividualUserTable;
    protected $IndividualHeightRecordTable;
    protected $IndividualWeightRecordTable;
    protected $IndividualWaistRecordTable;
    protected $IndividualPhysicalActivityTable;
    protected $IndividualBmiRecordTable;
    protected $IndividualCholesterolRecordTable;
    protected $IndividualBloodPressureRecordTable;
    protected $IndividualBloodGlucoseRecordTable;
    protected $IndividualSleepRecordTable;

    public function __construct() {
        
    }

    public function init() {
        $this->_sessionObj = new Container('frontend');
    }

    /* This is the index action for the current controller */

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $IndWeightTargetTableObj = $this->getServiceLocator()->get("IndividualWeightTargetTable");
        $IndWeightTableObj = $this->getServiceLocator()->get("IndividualWeightRecordTable");
        $IndFoodDrinkTblObj = $this->getServiceLocator()->get("IndividualFoodDrinkTable");
        $IndHeightTableObj = $this->getServiceLocator()->get("IndividualHeightRecordTable");
        $IndPhyTableObj = $this->getServiceLocator()->get("IndividualPhysicalActivityTable");
        $IndUserConditionTableObj = $this->getServiceLocator()->get("IndividualUsersConditionTable");

        $userId = $this->_sessionObj->userDetails['UserID'];
        $time_zone =  $this->_sessionObj->userDetails['time_zone'];
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $filter_select = 'oneweek'; // asigne veriable by default all
        $charttype = 'column';
        $weightUnit = 'pound';
        $heightUnit = 'in';
        $empty_height_graph_array = array();
        $empty_weight_graph_array = array();
        $empty_steps_graph_array = array();
        $empty_distance_graph_array = array();
        $empty_duration_graph_array = array();
        $empty_calories_graph_array = array();
        $empty_foode_servingSize_graph_array = array();
        $empty_food_NoOfServings_graph_array = array();
        $empty_food_calories_graph_array = array();

        $steps_val_avg_new_week = $cb_val_avg_new_week = $servingsize_val_avg_new_week = $noofserving_values_new_week = $calories_val_avg_new_week = $weight_val_avg_new_week = $height_val_avg_new_week = $duration_val_avg_new_week = $avg_distance_values_new_week = $steps_val_avg_new = $cb_val_avg_new = $servingsize_val_avg_new = $noofserving_values_new = $calories_val_avg_new = $weight_val_avg_new = $height_val_avg_new = $duration_val_avg_new = $avg_distance_values_new = '';
        $IndWeightDatas = $IndWaistDatas = $columns = $month_array = array();
        $month_values = $year_values = $avg_values = $heightValue_current = $weightValue_current = $waistValue_current = '';
        $request = $this->getRequest();
        $whereWeightArray = array(
            'UserID' => $userId
        );
        $weight_val = "";
        $Target_weight_val = "";
        $IndWeightTarget_Res = $IndWeightTargetTableObj->getWeightTarget($whereWeightArray, array()); // weight data
        if ($IndWeightTarget_Res) {
            $Target_weight_val = number_format((float) $CusConPlug->gramToPound($IndWeightTarget_Res[0]['Weight']), 1, '.', '');
        }
        $IndWeightDatas = $IndWeightTableObj->getWeightRecord($whereWeightArray, array(), 'WeiID DESC'); // weight data
        if (count($IndWeightDatas)) {
            foreach ($IndWeightDatas as $wvalue) {
                if ($wvalue['Weight'] != '') {
                    $weight_val = $wvalue['Weight'];
                    break;
                }
            }
            $weight_val = number_format((float) $CusConPlug->gramToPound($weight_val), 1, '.', '');
        }
        //echo $avg_values_weight;die;

        /*
         * condations symtomps treatment list
         */
        $whereArr = array("user_id" => $userId, 'con.status' => 1);
        $conditionslist = $IndUserConditionTableObj->getUserAllConditions($whereArr);

        /*         * ********
         * Action: graph data list .
         * @author: Shivendra Suman
         * Created Date: 02-01-2015.
         * *** */

        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $filter_select = $postDataArr['filter_select'];
        }

        $start_date = $end_date = date('Y-m-d');

        if ($filter_select == 'oneyear' || $filter_select == 'sixmonth' || $filter_select == 'threemonth') {

            $time = new \DateTime('now');
            if ($filter_select == 'oneyear') {
                $duration = 12;
                $formate = 'Y-m';
                $formate2 = 'M';
                $month_array = $CusConPlug->month_year_lists($duration, $formate);
                $month_array_list = $CusConPlug->month_year_lists($duration, $formate2);
            }
            if ($filter_select == 'sixmonth') {
                $duration = 5;
                $formate = 'Y-m';
                $formate2 = 'M';
                $month_array = $CusConPlug->month_year_lists($duration, $formate);
                $month_array_list = $CusConPlug->month_year_lists($duration, $formate2);
            }
            if ($filter_select == 'threemonth') {
                $duration = 2;
                $formate = 'Y-m';
                $formate2 = 'M';
                $month_array = $CusConPlug->month_year_lists($duration, $formate);
                $month_array_list = $CusConPlug->month_year_lists($duration, $formate2);
            }
            $graph_month_list = $CusConPlug->graphmontlist($month_array_list); // arrange month list a/c to graph
            // get one by one month daata 
            $i = 0;
            foreach ($month_array as $mvalue) {

                $start_date = date("$mvalue-01");
                $end_date = date("$mvalue-t");

                $wheredataArrayDay = array(
                    'UserID' => $userId,
                    'start_date' => $start_date,
                    'end_date' => $end_date
                );
                $weightdata[$i] = $IndWeightTableObj->getWeightRecordByYear($wheredataArrayDay);
                $heightdata[$i] = $IndHeightTableObj->getHeightRecordByYear($wheredataArrayDay);
                $phydata[$i] = $IndPhyTableObj->getPhyAcRecordByYear($wheredataArrayDay);
                $fooddata[$i] = $IndFoodDrinkTblObj->getFoodDrinkRecordByYear($wheredataArrayDay);
                $i++;
            }

            // weight data of graph
            $counth_data_weight = 1;
            foreach ($weightdata as $wevalue) {
                $cout_data_weight = count($weightdata);

                if ($counth_data_weight == $cout_data_weight) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }

                $weight_val_avg = (!empty($wevalue[0]['Avgweight']) ? $wevalue[0]['Avgweight'] : '0.0');
                $weight_val_avg_new .= number_format((float) $CusConPlug->gramToPound($weight_val_avg), 2, '.', '') . $delimeter;
                $empty_weight_graph_array[] = (!empty($wevalue[0]['Avgweight']) ? $wevalue[0]['Avgweight'] : '');
                $counth_data_weight++;
            }

            // height data of graph
            $counth_data_height = 1;
            foreach ($heightdata as $heivalue) {
                $cout_data_height = count($heightdata);

                if ($counth_data_height == $cout_data_height) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $height_val_avg = (!empty($heivalue[0]['Avgheight']) ? $heivalue[0]['Avgheight'] : '0.0');
                $height_val_avg_new .= number_format((float) $CusConPlug->centimetersToMeter($height_val_avg), 2, '.', '') . $delimeter;
                $empty_height_graph_array[] = (!empty($heivalue[0]['Avgheight']) ? $heivalue[0]['Avgheight'] : '');
                $counth_data_height++;
            }

            /*
              @ PHYSICAL AVCTIVITY GRAPH
             */

            $counth_data = 1;
            foreach ($phydata as $svalue) {
                $cout_data = count($phydata);

                if ($counth_data == $cout_data) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $duration_val_avg = (!empty($svalue[0]['AvgDuration']) ? $svalue[0]['AvgDuration'] : '0.0');
                $distance_val_avg = (!empty($svalue[0]['AvgDistance']) ? $svalue[0]['AvgDistance'] : '0.0');
                $steps_val_avg = (!empty($svalue[0]['AvgSteps']) ? $svalue[0]['AvgSteps'] : '0.0');
                $cb_val_avg = (!empty($svalue[0]['AvgCB']) ? $svalue[0]['AvgCB'] : '0.0');

                $duration_val_avg_new .= round($duration_val_avg) . $delimeter;
                $avg_distance_values_new .= number_format((float) $CusConPlug->MToKm($distance_val_avg), 2, '.', '') . $delimeter;
                $steps_val_avg_new .= round($steps_val_avg) . $delimeter;
                $cb_val_avg_new .= number_format((float) $CusConPlug->calorieTokilojule($cb_val_avg), 2, '.', '') . $delimeter;
                $empty_duration_graph_array[] = (!empty($svalue[0]['AvgDuration']) ? $svalue[0]['AvgDuration'] : '');
                $empty_distance_graph_array[] = (!empty($svalue[0]['AvgDistance']) ? $svalue[0]['AvgDistance'] : '');
                $empty_steps_graph_array[] = (!empty($svalue[0]['AvgSteps']) ? $svalue[0]['AvgSteps'] : '');
                $empty_calories_graph_array[] = (!empty($svalue[0]['AvgCB']) ? $svalue[0]['AvgCB'] : '');                
                $counth_data++;
            }

            /*
              @ Food GRAPH
             */

            $counth_data_food = 1;
            foreach ($fooddata as $foodvalue) {
                $cout_data_food = count($fooddata);

                if ($counth_data_food == $cout_data_food) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $servingsize_val_avg = (!empty($foodvalue[0]['AvgServingSize']) ? $foodvalue[0]['AvgServingSize'] : '0.0');
                $noofserving_val_avg = (!empty($foodvalue[0]['AvgNoOfServings']) ? $foodvalue[0]['AvgNoOfServings'] : '0.0');
                $calories_val_avg = (!empty($foodvalue[0]['AvgCalories']) ? $foodvalue[0]['AvgCalories'] : '0.0');

                $servingsize_val_avg_new .= $servingsize_val_avg . $delimeter;
                $noofserving_values_new .= $noofserving_val_avg . $delimeter;
                $calories_val_avg_new .= number_format((float) $CusConPlug->calorieTokilojule($calories_val_avg), 2, '.', '') . $delimeter;
                $empty_foode_servingSize_graph_array[] = (!empty($foodvalue[0]['AvgServingSize']) ? $foodvalue[0]['AvgServingSize'] : '');
                $empty_food_NoOfServings_graph_array[] = (!empty($foodvalue[0]['AvgNoOfServings']) ? $foodvalue[0]['AvgNoOfServings'] : '');
                $empty_food_calories_graph_array[] = (!empty($foodvalue[0]['AvgCalories']) ? $foodvalue[0]['AvgCalories'] : '');
               
                $counth_data_food++;
            }

            // assign data to veriables
            $month_values = $graph_month_list;

            $avg_values_weight = $weight_val_avg_new;
            $avg_values_height = $height_val_avg_new;
            $avg_duration_values = $duration_val_avg_new;
            $avg_diatance_values = $avg_distance_values_new;
            $avg_steps_values = $steps_val_avg_new; //steps
            $avg_cb_values = $cb_val_avg_new;
            $avg_servingsize_values = $servingsize_val_avg_new;
            $avg_noofserving_values = $noofserving_values_new;
            $avg_calories_values = $calories_val_avg_new;
        }


        if ($filter_select == 'oneweek' || $filter_select == 'twoweek' || $filter_select == 'fourweek') {

            $time = new \DateTime('now');
            if ($filter_select == 'oneweek') {
                $duration_value_start = '-1 week';
                $duration_value_end = '1 week';
            }
            if ($filter_select == 'twoweek') {
                $duration_value_start = '-2 week';
                $duration_value_end = '2 week';
            }
            if ($filter_select == 'fourweek') {
                $duration_value_start = '-4 week';
                $duration_value_end = '4 week';
            }
            //calculate date to get data 
            $start_dates = $time->modify($duration_value_start)->format('Y-m-d');
            $end_dates = $time->modify($duration_value_end)->format('Y-m-d');
            $week_array = $CusConPlug->date_difference($start_dates, $end_dates);
            $week_array_list = $CusConPlug->datelist($start_dates, $end_dates);
            if ($filter_select == 'oneweek') {
                $week_array_list = $CusConPlug->daylist($start_dates, $end_dates);
            }

            // set arry of week day alter nate day
            if ($filter_select == 'twoweek') {
                $week_array = $CusConPlug->date_difference_alternate($start_dates, $end_dates);
                $week_array_list = $CusConPlug->daylist_alternate($start_dates, $end_dates);
            }

            // set arry of week day alter nate day
            if ($filter_select == 'fourweek') {
                $week_array = $CusConPlug->date_difference_alternate($start_dates, $end_dates);
                $week_array_list = $CusConPlug->datelist_alternate($start_dates, $end_dates);
            }

            $graph_day_list = $CusConPlug->graphmontlist($week_array_list);

            // get one by one month daata 
            $i = 0;
            $count_loop_data = 1;

            foreach ($week_array as $wvalue) {

                $count_total_week = count($week_array);

                $start_date_week = date('Y-m-d', strtotime($wvalue));
                $end_date_week = date('Y-m-d', strtotime($wvalue));
                if ($count_total_week > $count_loop_data && ($filter_select == 'fourweek' || $filter_select == 'twoweek')) {
                    $date = new \DateTime($start_date_week);
                    $end_date_week = $date->modify('+1 day')->format('Y-m-d');
                }
                $whereWeekArrayDay = array(
                    'UserID' => $userId,
                    'start_date' => $start_date_week,
                    'end_date' => $end_date_week
                );


                $week_weightdata[$i] = $IndWeightTableObj->getWeightRecordByYear($whereWeekArrayDay);
                $week_heightdata[$i] = $IndHeightTableObj->getHeightRecordByYear($whereWeekArrayDay);
                $phyWdata[$i] = $IndPhyTableObj->getPhyAcRecordByYear($whereWeekArrayDay);
                $week_fooddata[$i] = $IndFoodDrinkTblObj->getFoodDrinkRecordByYear($whereWeekArrayDay);
                $i++;
                $count_loop_data++;
            }

            // get data from loop for week weight
            $counth_weight_week = 1;
            foreach ($week_weightdata as $weWkvalue) {
                $count_weight_week = count($week_weightdata);

                if ($counth_weight_week == $count_weight_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $weight_val_avg_week = (!empty($weWkvalue[0]['Avgweight']) ? $weWkvalue[0]['Avgweight'] : '0.0');
                $weight_val_avg_new_week .= number_format((float) $CusConPlug->gramToPound($weight_val_avg_week), 2, '.', '') . $delimeter;
                $empty_weight_graph_array[] = (!empty($weWkvalue[0]['Avgweight']) ? $weWkvalue[0]['Avgweight'] : '');
                $counth_weight_week++;
            }

            // get data from loop for week height
            $counth_height_week = 1;
            foreach ($week_heightdata as $heWkvalue) {
                $count_height_week = count($week_heightdata);

                if ($counth_height_week == $count_height_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $height_val_avg_week = (!empty($heWkvalue[0]['Avgheight']) ? $heWkvalue[0]['Avgheight'] : '0.0');
                $height_val_avg_new_week .= number_format((float) $CusConPlug->centimetersToMeter($height_val_avg_week), 2, '.', '') . $delimeter;
                $empty_height_graph_array[] = (!empty($heWkvalue[0]['Avgheight']) ? $heWkvalue[0]['Avgheight'] : '');
                $counth_height_week++;
            }

            /*
              @ physcial activity weekly graph
             */
            $counth_data = 1;
            foreach ($phyWdata as $svalue) {
                $cout_data = count($phyWdata);

                if ($counth_data == $cout_data) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $duration_val_avg = (!empty($svalue[0]['AvgDuration']) ? $svalue[0]['AvgDuration'] : '0.0');
                $distance_val_avg = (!empty($svalue[0]['AvgDistance']) ? $svalue[0]['AvgDistance'] : '0.0');
                $steps_val_avg = (!empty($svalue[0]['AvgSteps']) ? $svalue[0]['AvgSteps'] : '0.0');
                $cb_val_avg = (!empty($svalue[0]['AvgCB']) ? $svalue[0]['AvgCB'] : '0.0');

                $duration_val_avg_new_week .= round($duration_val_avg) . $delimeter;
                $avg_distance_values_new_week .= number_format((float) $CusConPlug->MToKm($distance_val_avg), 2, '.', '') . $delimeter;
                $steps_val_avg_new_week .= round($steps_val_avg) . $delimeter;
                $cb_val_avg_new_week .= number_format((float) $CusConPlug->calorieTokilojule($cb_val_avg), 2, '.', '') . $delimeter;
                $empty_duration_graph_array[] = (!empty($svalue[0]['AvgDuration']) ? $svalue[0]['AvgDuration'] : '');
                $empty_distance_graph_array[] = (!empty($svalue[0]['AvgDistance']) ? $svalue[0]['AvgDistance'] : '');
                $empty_steps_graph_array[] = (!empty($svalue[0]['AvgSteps']) ? $svalue[0]['AvgSteps'] : '');
                $empty_calories_graph_array[] = (!empty($svalue[0]['AvgCB']) ? $svalue[0]['AvgCB'] : '');
                $counth_data++;
            }
            /*
              @ food graph data
             */

            $counth_data_food_week = 1;
            foreach ($week_fooddata as $foodwvalue) {

                $cout_data_food_week = count($week_fooddata);

                if ($counth_data_food_week == $cout_data_food_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $servingsize_val_avg_week = (!empty($foodwvalue[0]['AvgServingSize']) ? $foodwvalue[0]['AvgServingSize'] : '0.0');
                $noofserving_val_avg_week = (!empty($foodwvalue[0]['AvgNoOfServings']) ? $foodwvalue[0]['AvgNoOfServings'] : '0.0');
                $calories_val_avg_week = (!empty($foodwvalue[0]['AvgCalories']) ? $foodwvalue[0]['AvgCalories'] : '0.0');

                $servingsize_val_avg_new_week .= $servingsize_val_avg_week . $delimeter;
                $noofserving_values_new_week .= $noofserving_val_avg_week . $delimeter;
                $calories_val_avg_new_week .= number_format((float) $CusConPlug->calorieTokilojule($calories_val_avg_week), 2, '.', '') . $delimeter;
                $empty_foode_servingSize_graph_array[] = (!empty($foodwvalue[0]['AvgServingSize']) ? $foodwvalue[0]['AvgServingSize'] : '');
                $empty_food_NoOfServings_graph_array[] = (!empty($foodwvalue[0]['AvgNoOfServings']) ? $foodwvalue[0]['AvgNoOfServings'] : '');
                $empty_food_calories_graph_array[] = (!empty($foodwvalue[0]['AvgCalories']) ? $foodwvalue[0]['AvgCalories'] : '');
              
                $counth_data_food_week++;
            }

            // assign data to veriables
            $month_values = $graph_day_list;

            $avg_values_weight = $weight_val_avg_new_week;
            $avg_values_height = $height_val_avg_new_week;
            $avg_duration_values = $duration_val_avg_new_week;
            $avg_diatance_values = $avg_distance_values_new_week;
            $avg_steps_values = $steps_val_avg_new_week; //steps
            $avg_cb_values = $cb_val_avg_new_week;
            $avg_servingsize_values = $servingsize_val_avg_new_week;
            $avg_noofserving_values = $noofserving_values_new_week;
            $avg_calories_values = $calories_val_avg_new_week;
        }
        /*
         * Hourly graph data 
         */
        if ($filter_select == 'oneday') {
            $charttype = 'line';
            //calculate time to get data 
           // $time_zone = $CusConPlug->getTimezone();
            $today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0, 0);
            $today->format('Y-m-d H:i:s');

            //Get UTC midnight time
            $today->setTimezone(new \DateTimeZone("UTC"));
            $start = $today->format('Y-m-d H:i:s');
            $week_array = $CusConPlug->time_difference($start);
            
            $i = 0;
            $count_loop_data = 1;
            foreach ($week_array as $wvalue) {
                $date = date('Y-m-d', strtotime($wvalue));
                $start_time = date('H:i:s', strtotime($wvalue));
                $end_time = date('H:i:s', strtotime($wvalue . ' +1 hour'));
                $whereWeekArrayDay = array(
                    'UserID' => $userId,
                    'current_date' => $date,
                    'start_time' => $start_time,
                    'end_time' => $end_time
                );

                $week_weightdata[$i] = $IndWeightTableObj->getWeightRecordOfDay($whereWeekArrayDay);
                $week_heightdata[$i] = $IndHeightTableObj->getHeightRecordOfDay($whereWeekArrayDay);
                $phyWdata[$i] = $IndPhyTableObj->getPhyAcRecordOfDay($whereWeekArrayDay);
                $week_fooddata[$i] = $IndFoodDrinkTblObj->getFoodDrinkRecordOfDay($whereWeekArrayDay);
                $i++;
                $count_loop_data++;
            }

            // weight data of graph
            $counth_weight_week = 1;
            foreach ($week_weightdata as $weWkvalue) {
                $count_weight_week = count($week_weightdata);

                if ($counth_weight_week == $count_weight_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $weight_val_avg_week = (!empty($weWkvalue[0]['Avgweight']) ? $weWkvalue[0]['Avgweight'] : '0.0');
                $weight_val_avg_new_week .= number_format((float) $CusConPlug->gramToPound($weight_val_avg_week), 1, '.', '') . $delimeter;
                $empty_weight_graph_array[] = (!empty($weWkvalue[0]['Avgweight']) ? $weWkvalue[0]['Avgweight'] : '');
                $counth_weight_week++;
            }

            // height data of graph
            $counth_height_week = 1;
            foreach ($week_heightdata as $heWkvalue) {
                $count_height_week = count($week_heightdata);

                if ($counth_height_week == $count_height_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $height_val_avg_week = (!empty($heWkvalue[0]['Avgheight']) ? $heWkvalue[0]['Avgheight'] : '0.0');
                $height_val_avg_new_week .= number_format((float) $CusConPlug->centimetersToInches($height_val_avg_week), 1, '.', '') . $delimeter;
                $empty_height_graph_array[] = (!empty($heWkvalue[0]['Avgheight']) ? $heWkvalue[0]['Avgheight'] : '');
                $counth_height_week++;
            }

            // physical activity data of graph
            $counth_data = 1;
            foreach ($phyWdata as $svalue) {
                $cout_data = count($phyWdata);

                if ($counth_data == $cout_data) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $duration_val_avg = (!empty($svalue[0]['AvgDuration']) ? $svalue[0]['AvgDuration'] : '0.0');
                $distance_val_avg = (!empty($svalue[0]['AvgDistance']) ? $svalue[0]['AvgDistance'] : '0.0');
                $steps_val_avg = (!empty($svalue[0]['AvgSteps']) ? $svalue[0]['AvgSteps'] : '0.0');
                $cb_val_avg = (!empty($svalue[0]['AvgCB']) ? $svalue[0]['AvgCB'] : '0.0');
                $duration_val_avg_new_week .= round($CusConPlug->MinToHo($duration_val_avg)) . $delimeter;
                $avg_distance_values_new_week .= number_format((float) $CusConPlug->MToKm($distance_val_avg), 1, '.', '') . $delimeter;
                $steps_val_avg_new_week .= round($steps_val_avg) . $delimeter;
                $cb_val_avg_new_week .= number_format((float) $CusConPlug->calorieTokilojule($cb_val_avg), 1, '.', '') . $delimeter;
                
                $empty_duration_graph_array[] = (!empty($svalue[0]['AvgDuration']) ? $svalue[0]['AvgDuration'] : '');
                $empty_distance_graph_array[] = (!empty($svalue[0]['AvgDistance']) ? $svalue[0]['AvgDistance'] : '');
                $empty_steps_graph_array[] = (!empty($svalue[0]['AvgSteps']) ? $svalue[0]['AvgSteps'] : '');
                $empty_calories_graph_array[] = (!empty($svalue[0]['AvgCB']) ? $svalue[0]['AvgCB'] : '');
                $counth_data++;
            }

            // food data of graph
            $counth_data_food_week = 1;
            foreach ($week_fooddata as $foodwvalue) {
                $cout_data_food_week = count($week_fooddata);

                if ($counth_data_food_week == $cout_data_food_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $servingsize_val_avg_week = (!empty($foodwvalue[0]['AvgServingSize']) ? $foodwvalue[0]['AvgServingSize'] : '0.0');
                $noofserving_val_avg_week = (!empty($foodwvalue[0]['AvgNoOfServings']) ? $foodwvalue[0]['AvgNoOfServings'] : '0.0');
                $calories_val_avg_week = (!empty($foodwvalue[0]['AvgCalories']) ? $foodwvalue[0]['AvgCalories'] : '0.0');

                $servingsize_val_avg_new_week .= $servingsize_val_avg_week . $delimeter;
                $noofserving_values_new_week .= $noofserving_val_avg_week . $delimeter;
                $calories_val_avg_new_week .= number_format((float) $CusConPlug->calorieTokilojule($calories_val_avg_week), 1, '.', '') . $delimeter;
                $empty_foode_servingSize_graph_array[] = (!empty($foodwvalue[0]['AvgServingSize']) ? $foodwvalue[0]['AvgServingSize'] : '');
                $empty_food_NoOfServings_graph_array[] = (!empty($foodwvalue[0]['AvgNoOfServings']) ? $foodwvalue[0]['AvgNoOfServings'] : '');
                $empty_food_calories_graph_array[] = (!empty($foodwvalue[0]['AvgCalories']) ? $foodwvalue[0]['AvgCalories'] : '');
                $counth_data_food_week++;
            }
            $graph_month_list = "'0', '', '','3', '', '', '6', '', '', '9', '', '', '12', '', '', '15', '', '', '18', '', '', '21', '', '', '24'";

            // assign data to variables
            $month_values = $graph_month_list;
            $avg_values_weight = $weight_val_avg_new_week;
            $avg_values_height = $height_val_avg_new_week;
            $avg_duration_values = $duration_val_avg_new_week;
            $avg_diatance_values = $avg_distance_values_new_week;
            $avg_steps_values = $steps_val_avg_new_week;
            $avg_cb_values = $cb_val_avg_new_week;
            $avg_servingsize_values = $servingsize_val_avg_new_week;
            $avg_noofserving_values = $noofserving_values_new_week;
            $avg_calories_values = $calories_val_avg_new_week;
        }




        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  Weightmanagement',
                    'custom_plugin' => $CusConPlug, // custome plugin
                    'Curr_weight' => $weight_val,
                    'Target_weight' => $Target_weight_val,
                    'fromDate' => (isset($IndWeightTarget_Res[0]['TargetWeightStartDate']) ? $IndWeightTarget_Res[0]['TargetWeightStartDate'] : ""),
                    'toDate' => (isset($IndWeightTarget_Res[0]['TargetWeightEndDate']) ? $IndWeightTarget_Res[0]['TargetWeightEndDate'] : ""),
                    'filter_select' => $filter_select,
                    'user_Conditions' => $conditionslist,
                    'xaxisData' => $month_values, //weight height year  month, weeks list
                    'yaxisData_height' => ((!empty(array_filter($empty_height_graph_array)))? $avg_values_height : ""),
                    'yaxisData_weight' => ((!empty(array_filter($empty_weight_graph_array)))? $avg_values_weight : ""),
                    'yaxisData_duration' => ((!empty(array_filter($empty_duration_graph_array)))? $avg_duration_values : ""),
                    'yaxisData_distance' => ((!empty(array_filter($empty_distance_graph_array)))? $avg_diatance_values : ""),
                    'yaxisData_cb' => ((!empty(array_filter($empty_calories_graph_array)))? $avg_cb_values : ""),
                    'yaxisData_servingsize' => ((!empty(array_filter($empty_foode_servingSize_graph_array)))? $avg_servingsize_values : ""),
                    'yaxisData_noofserving' => ((!empty(array_filter($empty_food_NoOfServings_graph_array)))? $avg_noofserving_values : ""),
                    'yaxisData_calories' => ((!empty(array_filter($empty_food_calories_graph_array)))? $avg_calories_values : ""),
                    'charttype' => $charttype,
                    'weightUnit' => $weightUnit,
                    'heightUnit' => $heightUnit,
        ));
    }

}
