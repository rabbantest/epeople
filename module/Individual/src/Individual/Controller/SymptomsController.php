<?php

namespace Individual\Controller;

/* * ***********************
 * PAGE: USE TO MANAGE THE History CONTROLLER FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 30/12/2014.
 * UPDATED: 30/12/2014.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class SymptomsController extends \Zend\Mvc\Controller\AbstractActionController {

    protected $IndividualUserTable;
    protected $IndividualConditionTable;
    protected $IndUserConditionTableObj;
    protected $IndUserOtherSympTableObj;
    protected $IndUserSympTableObj;
    protected $IndCondiTableObj;

    public function __construct() {
        
    }

    public function init() {
        $this->_sessionObj = new Container('frontend');
        $this->IndUserConditionTableObj = $this->getServiceLocator()->get("IndividualUsersConditionTable");
        $this->IndUserOtherSympTableObj = $this->getServiceLocator()->get("IndividualUsersOtherSymtomsTable");
        $this->IndUserSympTableObj = $this->getServiceLocator()->get("IndividualUsersSymtomsTable");
        $this->IndCondiTableObj = $this->getServiceLocator()->get("IndividualConditionTable");
    }

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $whereArr = array("user_id" => $UserID, 'con.status' => 1);
        $conditionslist = $this->IndUserConditionTableObj->getUserAllConditions($whereArr);
        $user_Symptoms = array();
        $whereSymArr = array("user_id" => $UserID, 'symp.status' => 1);
        $user_Symptoms = $this->IndUserSympTableObj->getUserSymptoms($whereSymArr);
        $finalRes = array();
        if (count($user_Symptoms)) {
            foreach ($user_Symptoms as $val) {
                $result['symptoms_name'] = $val['common_name'];
                $result['media_name'] = $val['media_name'];
                $result['symtoms_id'] = $val['symtoms_id'];
                $result['sub_symptoms_name'] = $val['sub_symptoms_name'];
                $result['description'] = $val['description'];
                $result['SympStatus'] = $val['SympStatus'];
                $result['condition_name'] = '';
                if ($val['condition_ids']) {
                    $res = $this->IndCondiTableObj->getConditionsList(array('status' => 1, 'id' => explode(',', $val['condition_ids'])), array('common_name'), "0", array());
                    if ($res)
                        $result['condition_name'] = $res[0]['common_name'];
                }
                $finalRes[] = $result;
            }
        }
        $message = "";
        $this->flashMessenger()->addMessage(array('success' => $message));
        $otherwhere = array('user_id' => $UserID, 'ot_sym.status' => 1);
        $getUserOtherSymp = $this->IndUserOtherSympTableObj->getUserOtherSymtoms($otherwhere);
        if ($getUserOtherSymp) {
            $returnArr = $getUserOtherSymp[0];
        } else {
            $returnArr = array();
        }
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  History',
                    'user_Symptoms' => $finalRes, 'otherSymptoms' => $returnArr
        ));
    }

    /*
      @ This action is used to add the conditons and subcontios
      @ Created:31/12/14
     */

    public function addsymptomsAction() {

        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $IndCondiTableObj = $this->getServiceLocator()->get("IndividualConditionTable");
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $error = "";
        $otherwhere = array('user_id' => $UserID, 'ot_sym.status' => 1);
        $getUserOtherSymp = $this->IndUserOtherSympTableObj->getUserOtherSymtoms($otherwhere);
        $request = $this->getRequest();
        $success = '';
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $user_other_symptoms = $postDataArr['user_other_symptoms'];
            if ($user_other_symptoms && !$getUserOtherSymp) {
                $selected_status = $postDataArr['selected_status'];
                $dataArr = array('status_id' => $selected_status, 'user_id' => $UserID, 'other_symtoms' => $user_other_symptoms, 'creation_date' => round(microtime(true) * 1000),
                    'updation_date' => round(microtime(true) * 1000));
                $this->IndUserOtherSympTableObj->SaveUserOtherSymtoms($dataArr);
                $getUserOtherSymp = 1;
                $success = 1;
            }
            if (count($postDataArr['symptomsId_list'])) {
                foreach ($postDataArr['symptomsId_list'] as $k => $val) {
                    $symptomsStatusID = $postDataArr['symptoms_status'][$k];
                    $SubSymptomsId = $postDataArr['subsymptoms'][$k];
                    $ConditionsIds = '';
                    if (count($postDataArr['user_condiotion'][$k]))
                        $ConditionsIds = implode(',', $postDataArr['user_condiotion'][$k]);
                    $symtomsArr = array('user_id' => $UserID, 'symtoms_id' => $val, 'condition_ids' => $ConditionsIds,
                        'sub_symtoms_id' => $SubSymptomsId, 'symptoms_status' => $symptomsStatusID, 'creation_date' => round(microtime(true) * 1000),
                        'updation_date' => round(microtime(true) * 1000));
                    $this->IndUserSympTableObj->SaveUserSymtoms($symtomsArr);
                }
                $success = 1;
                /*                 * ********Start Code to get connected and send notification**************** */
                $this->NotificationTableObj = $this->getServiceLocator()->get("IndividualNotificationTable");
                $FollowerUserObj = $this->getServiceLocator()->get("IndividualFollowerUsersTable");
                $whereConnected['follow.follower_id'] = $UserID;
                $whereConnected['follow.status'] = 1;
                $whereConnected['follow.is_send_notification'] = 1;
                $ConnectedPeople = $FollowerUserObj->getUserFollower($whereConnected, array('user_id'));
                if (count($ConnectedPeople)) {
                    foreach ($ConnectedPeople as $val) {
                        $type = 'symptom_add';
                        $notDataArr = array("sender_id" => $UserID, "user_type" => 1, "receiver_id" => $val['user_id'], "created_date" => round(microtime(true) * 1000));
                        $saveNoti = $this->NotificationTableObj->SaveNotification($notDataArr, $type);
                    }
                }

                /*                 * ********End Code to get connected and send notification**************** */
            } else {
                $error = "<font color='red'>Please select condition first.</font>";
            }
            if ($success == 1) {
                $message = "Data added successfully. ";
                $this->flashMessenger()->addMessage(array('success' => $message));
            }
        }
        $Wherestatus = array('type' => 2);
        $statusArr = $this->IndUserConditionTableObj->getConditionSymptomsTreatmentStatus($Wherestatus);
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array('statusArr' => $statusArr, 'pageTitle' => 'Individual  History', 'otherSymptoms' => $getUserOtherSymp));
    }

    /*
      @ This action is used to add the conditons and subcontios
      @ Created:04/04/15
     */

    public function editothersymptomsAction() {

        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $ID = base64_decode($this->params('id'));
        $error = "";
        $request = $this->getRequest();
        $success = '';
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $user_other_symptoms = $postDataArr['user_other_symptoms'];
            if ($user_other_symptoms) {
                $selected_status = $postDataArr['selected_status'];
                $dataArr = array('status_id' => $selected_status, 'other_symtoms' => $user_other_symptoms, 'updation_date' => round(microtime(true) * 1000),
                    'updation_date' => round(microtime(true) * 1000));
                $where = array('id' => $ID);
                $this->IndUserOtherSympTableObj->updateUserOtherSymtoms($where, $dataArr);
                $getUserOtherSymp = 1;
                $success = 1;
            }
            if ($success == 1) {
                $message = "Data added successfully. ";
                $this->flashMessenger()->addMessage(array('success' => $message));
            }
        }
        $otherwhere = array('user_id' => $UserID, 'ot_sym.status' => 1);
        $getUserOtherSymp = $this->IndUserOtherSympTableObj->getUserOtherSymtoms($otherwhere);
        $Wherestatus = array('type' => 2);
        $statusArr = $this->IndUserConditionTableObj->getConditionSymptomsTreatmentStatus($Wherestatus);
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array('statusArr' => $statusArr, 'pageTitle' => 'Individual  History', 'otherSymptoms' => $getUserOtherSymp[0]));
    }

    /*
      @ This action is used to edit the conditons and subcontios
      @ Created:31/12/14
     */

    public function editsymptomsAction() {
        $this->init();
        //$condtionID = $this->params()->fromQuery('id');
        $symptomsID = base64_decode($this->params('id'));
        $this->_sessionObj = new Container('frontend');
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $IndSymtomsTableObj = $this->getServiceLocator()->get("IndividualSymtomsTable");

        $error = "";
        $IndUserOtherTreatObj = $this->getServiceLocator()->get("IndividualUsersOtherTreatmentsTable");
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $symptoms_status = $postDataArr['symptoms_status'];
            $subsymptoms = $postDataArr['subsymptoms'];
            $user_condiotion = $postDataArr['user_condiotion'];
            $data = array();
            $data['sub_symtoms_id'] = $postDataArr['subsymptoms'];
            $data['symptoms_status'] = $postDataArr['symptoms_status'];
            $data['condition_ids'] = implode(',', $user_condiotion);
            if (!$postDataArr['subsymptoms']) {
                $data['sub_symtoms_id'] = 0;
            }
            if (!$postDataArr['symptoms_status']) {
                $data['symptoms_status'] = 0;
            }
            if (!count($user_condiotion)) {
                $data['condition_ids'] = '';
            }
            $where = array('user_id' => $UserID, 'status' => 1, 'symtoms_id' => $symptomsID);
            $this->IndUserSympTableObj->updateUserSymtoms($where, $data);
            $message = "Data Updated successfully. ";
            $this->flashMessenger()->addMessage(array('success' => $message));
            /*             * ********Start Code to get connected and send notification**************** */
            $this->NotificationTableObj = $this->getServiceLocator()->get("IndividualNotificationTable");
            $FollowerUserObj = $this->getServiceLocator()->get("IndividualFollowerUsersTable");
            $whereConnected['follow.follower_id'] = $UserID;
            $whereConnected['follow.status'] = 1;
            $whereConnected['follow.is_send_notification'] = 1;
            $ConnectedPeople = $FollowerUserObj->getUserFollower($whereConnected, array('user_id'));
            if (count($ConnectedPeople)) {
                foreach ($ConnectedPeople as $val) {
                    $type = 'symptom_edit';
                    $notDataArr = array("sender_id" => $UserID, "user_type" => 1, "receiver_id" => $val['user_id'], "created_date" => round(microtime(true) * 1000));
                    $saveNoti = $this->NotificationTableObj->SaveNotification($notDataArr, $type);
                }
            }

            /*             * ********End Code to get connected and send notification**************** */
            return $this->redirect()->toRoute('indiv_symp');
        }
        $whereArrSymtoms = array("user_id" => $UserID, 'symp.status' => 1, "symtoms_id" => $symptomsID);
        $symtompDetails = $this->IndUserSympTableObj->getUserSymptoms($whereArrSymtoms);
        $whereStatus = array('type' => 2);
        $statusArr = $this->IndUserConditionTableObj->getConditionSymptomsTreatmentStatus($whereStatus);
        $whereArr = array("status" => 1, 'symptoms_id' => $symptomsID);
        $subcolumns = array();
        $subsymtomsArr = $IndSymtomsTableObj->getSymptoms($whereArr, $subcolumns, "1", array());
        $whereArrforCondition = array("user_id" => $UserID, 'con.status' => 1);
        $User_conditionslist = $this->IndUserConditionTableObj->getUserAllConditions($whereArrforCondition);
        //$statusArr = $this->IndUserConditionTableObj->getConditionSymptomsTreatmentStatus();
        // echo '<pre>'; print_r($subsymtomsArr);die;
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array('pageTitle' => 'Individual  History',
                    'User_conditionslist' => $User_conditionslist,
                    'symptomsDetails' => $symtompDetails[0],
                    'error' => $error, 'subsymtomsArr' => $subsymtomsArr, 'statusArr' => $statusArr));
    }

    /*
      @ Used: This method is used for symptoms description
      @ Created: 11/03/15
     */

    public function viewsymptomsAction() {

        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $symptomsID = base64_decode($this->params('id'));
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $user_Symptoms = array();
        $whereSymArr = array("user_id" => $UserID, 'symp.status' => 1, 'symp.symtoms_id' => $symptomsID);
        $user_Symptoms = $this->IndUserSympTableObj->getUserSymptoms($whereSymArr);
        $finalRes = array();
        if (count($user_Symptoms)) {
            foreach ($user_Symptoms as $val) {
                $result['symptoms_name'] = $val['common_name'];
                $result['symtoms_id'] = $val['symtoms_id'];
                $result['sub_symptoms_name'] = $val['sub_symptoms_name'];
                $result['description'] = $val['description'];
                $result['SympStatus'] = $val['SympStatus'];
                $result['condition_name'] = '';
                if ($val['condition_ids']) {
                    $res = $this->IndCondiTableObj->getConditionsList(array('status' => 1, 'id' => explode(',', $val['condition_ids'])), array('common_name'), "0", array());
                    foreach ($res as $k => $val) {
                        $resultConName[] = $val['common_name'];
                    }
                    $result['condition_name'] = $res;
                }

                $finalRes[] = $result;
            }
        }
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  History',
                    'custom_plugin' => $CusConPlug, // custome plugin
                    'user_Symptoms' => $finalRes[0]
        ));
    }

}
