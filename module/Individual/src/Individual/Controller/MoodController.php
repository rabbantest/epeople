<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE How I feel  FOR Individual.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 08/01/2015.
 * UPDATED:  .
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class MoodController extends AbstractActionController {

    public function __construct() {
        $this->_sessionObj = new Container('frontend');
    }

    public function indexAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        // echo 'suman';
        $userId = $this->_sessionObj->userDetails['UserID'];
        $UserName = $this->_sessionObj->userDetails['UserName'];
        $time_zone =  $this->_sessionObj->userDetails['time_zone'];
        $general_val_avg_week = $physical_val_avg_week = $mental_val_avg_week = '';
        $filter_select = 'oneday'; // asigne veriable by default all
        $IndMoodTableObj = $this->getServiceLocator()->get("IndividualMoodRecordTable");
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin     
        $general_today_val = $physical_today_val = $mental_today_val = $AvgPMV = $AvgPMV = $AvgMMV = $submit_mood = $general_mood_val_avg = $physical_mood_val_avg = $mental_mood_val_avg = '';
        $moodlist = $whereArr = $columns = $dataArray = $todaymooddata = array();
        $empty_general_graph_array = array();
        $empty_physical_graph_array = array();
        $empty_mental_graph_array = array();
        /* @get today last value
         * 
         */
        $wheredataArrayStart = array(
            'UserID' => $userId,
            'mood_record_date' => date("Y-m-d"),
                //'end_date' => date("Y-m-d")
        );
        //  $todaymooddata = $IndMoodTableObj->getmoodRecordByYear($wheredataArrayStart);
        $lookingfor = "creation_date DESC";
        $todaymooddata = $IndMoodTableObj->getmoodRecord($wheredataArrayStart, $columns, $lookingfor);

        $isavalabledata = 0;
        if (count($todaymooddata) > 0) {
            $general_today_valus = $todaymooddata[0]['general_mood_value'];
            $general_today_val = number_format((float) $general_today_valus, 2, '.', '');
            $physical_today_vals = $todaymooddata[0]['physical_mood_value'];
            $physical_today_val = number_format((float) $physical_today_vals, 2, '.', '');
            $mental_today_vals = $todaymooddata[0]['mental_mood_value'];
            $mental_today_val = number_format((float) $mental_today_vals, 2, '.', '');

            if (!empty($general_today_valus) || !empty($physical_today_vals) || !empty($physical_today_vals)) {
                $isavalabledata = 1;
            }
        }


        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $creationdate = round(microtime(true) * 1000);
            $filter_select = $postDataArr['filter_select'];

            /*
              @ Save mood list
             */
            if (isset($postDataArr['submit_mood'])) {
                $GMVD = $postDataArr['general_mood_value'];
                $PMVD = $postDataArr['physical_mood_value'];
                $MMVD = $postDataArr['mental_mood_value'];
                if (empty($GMVD)) {
                    $message = "Please check general mood.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                    return $this->redirect()->toRoute('indiv_mood');
                }
                if (empty($PMVD)) {
                    $message = "Please check physical mood.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                    return $this->redirect()->toRoute('indiv_mood');
                }
                if (empty($MMVD)) {
                    $message = "Please check mental mood.";
                    $this->flashMessenger()->addMessage(array('error' => $message));
                    return $this->redirect()->toRoute('indiv_mood');
                }
                $dataArray = array(
                    'UserID' => $userId,
                    'general_mood_value' => $GMVD,
                    'physical_mood_value' => $PMVD,
                    'mental_mood_value' => $MMVD,
                    'mood_record_date' => date('Y-m-d'),
                    'mood_record_time' => date('H:i:s'),
                    'creation_date' => $creationdate
                );
                $insmood = $IndMoodTableObj->insertmoodRecord($dataArray);
                if ($insmood) {
                    $message = "Mood submitted successfully.";
                    $this->flashMessenger()->addMessage(array('success' => $message));
                    return $this->redirect()->toRoute('indiv_mood');
                } else {
                    $message = "Please try again later some error occured";
                    $this->flashMessenger()->addMessage(array('success' => $message));
                    return $this->redirect()->toRoute('indiv_mood');
                }
            }
        }
        /*
         * @  Show graph
         */
        /*
         * monthly graph data 
         */
        if ($filter_select == 'oneyear' || $filter_select == 'sixmonth' || $filter_select == 'threemonth') {

            $time = new \DateTime('now');
            if ($filter_select == 'oneyear') {
                $duration = 12;
                $formate = 'Y-m';
                $formate2 = 'M';
                $month_array = $CusConPlug->month_year_lists($duration, $formate);
                $month_array_list = $CusConPlug->month_year_lists($duration, $formate2);
            }
            if ($filter_select == 'sixmonth') {
                $duration = 5;
                $formate = 'Y-m';
                $formate2 = 'M';
                $month_array = $CusConPlug->month_year_lists($duration, $formate);
                $month_array_list = $CusConPlug->month_year_lists($duration, $formate2);
            }
            if ($filter_select == 'threemonth') {
                $duration = 2;
                $formate = 'Y-m';
                $formate2 = 'M';
                $month_array = $CusConPlug->month_year_lists($duration, $formate);
                $month_array_list = $CusConPlug->month_year_lists($duration, $formate2);
            }
            $graph_month_list = $CusConPlug->graphmontlist($month_array_list); // arrange month list a/c to graph
            $i = 0;
            foreach ($month_array as $mvalue) {
                $start_date = date("$mvalue-01");
                $end_date = date("$mvalue-t");

                $wheredataArrayDay = array(
                    'UserID' => $userId,
                    'start_date' => $start_date,
                    'end_date' => $end_date
                );
                $mooddata[$i] = $IndMoodTableObj->getmoodRecordByYear($wheredataArrayDay);
                $i++;
            }

            $counth_data_mood = 1;
            foreach ($mooddata as $movalue) {
                $cout_data_mood = count($mooddata);

                if ($counth_data_mood == $cout_data_mood) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $general_mood_val_avg .= "[" . (!empty($movalue[0]['AvgGMV']) ? number_format((float) $movalue[0]['AvgGMV'], 1, '.', '') : '-1.0') . ", 30]" . $delimeter;
                $physical_mood_val_avg .= "[" . (!empty($movalue[0]['AvgPMV']) ? number_format((float) $movalue[0]['AvgPMV'], 1, '.', '') : '-1.0') . ", 20]" . $delimeter;
                $mental_mood_val_avg .= "[" . (!empty($movalue[0]['AvgMMV']) ? number_format((float) $movalue[0]['AvgMMV'], 1, '.', '') : '-1.0') . ", 10]" . $delimeter;

                $empty_general_graph_array[] = (!empty($movalue[0]['AvgGMV']) ? $movalue[0]['AvgGMV'] : '');
                $empty_physical_graph_array[] = (!empty($movalue[0]['AvgPMV']) ? $movalue[0]['AvgPMV'] : '');
                $empty_mental_graph_array[] = (!empty($movalue[0]['AvgMMV']) ? $movalue[0]['AvgMMV'] : '');

                $counth_data_mood++;
            }

            // assign data to veriables
            $xaxisData = $graph_month_list;

            $avg_values_general = $general_mood_val_avg;
            $avg_values_physical = $physical_mood_val_avg;
            $avg_values_mental = $mental_mood_val_avg;
        }

        /*
         * @ weekly graph
         */
        if ($filter_select == 'oneweek' || $filter_select == 'twoweek' || $filter_select == 'fourweek') {

            $time = new \DateTime('now');
            if ($filter_select == 'oneweek') {
                $duration_value_start = '-1 week';
                $duration_value_end = '1 week';
            }
            if ($filter_select == 'twoweek') {
                $duration_value_start = '-2 week';
                $duration_value_end = '2 week';
            }
            if ($filter_select == 'fourweek') {
                $duration_value_start = '-4 week';
                $duration_value_end = '4 week';
            }
            //calculate date to get data 
            $start_dates = $time->modify($duration_value_start)->format('Y-m-d');
            $end_dates = $time->modify($duration_value_end)->format('Y-m-d');
            $week_array = $CusConPlug->date_difference($start_dates, $end_dates);
            $week_array_list = $CusConPlug->datelist($start_dates, $end_dates);
            if ($filter_select == 'oneweek') {
                $week_array_list = $CusConPlug->daylist($start_dates, $end_dates);
            }

            // set arry of week day alter nate day
            if ($filter_select == 'twoweek') {
                $week_array = $CusConPlug->date_difference_alternate($start_dates, $end_dates);
                $week_array_list = $CusConPlug->daylist_alternate($start_dates, $end_dates);
            }
            // set arry of week day alter nate day
            if ($filter_select == 'fourweek') {
                $week_array = $CusConPlug->date_difference_alternate($start_dates, $end_dates);
                $week_array_list = $CusConPlug->datelist_alternate($start_dates, $end_dates);
            }
            $graph_day_list = $CusConPlug->graphmontlist($week_array_list);

            // get one by one month daata 
            $i = 0;
            $count_loop_data = 1;
            foreach ($week_array as $wvalue) {
                $count_total_week = count($week_array);

                $start_date_week = date('Y-m-d', strtotime($wvalue));
                $end_date_week = date('Y-m-d', strtotime($wvalue));
                if ($count_total_week > $count_loop_data && ($filter_select == 'fourweek' || $filter_select == 'twoweek')) {
                    $date = new \DateTime($start_date_week);
                    $end_date_week = $date->modify('+1 day')->format('Y-m-d');
                }
                $whereWeekArrayDay = array(
                    'UserID' => $userId,
                    'start_date' => $start_date_week,
                    'end_date' => $end_date_week
                );
                $week_mooddata[$i] = $IndMoodTableObj->getmoodRecordByYear($whereWeekArrayDay);

                $i++;
                $count_loop_data++;
            }

            $counth_data_mo_week = 1;
            foreach ($week_mooddata as $moweekvalue) {
                $cout_data_mo_week = count($week_mooddata);

                if ($counth_data_mo_week == $cout_data_mo_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }

                $general_val_avg_week .= "[" . (!empty($moweekvalue[0]['AvgGMV']) ? number_format((float) $moweekvalue[0]['AvgGMV'], 1, '.', '') : '-1.0') . ", 60]" . $delimeter;
                $physical_val_avg_week .= "[" . (!empty($moweekvalue[0]['AvgPMV']) ? number_format((float) $moweekvalue[0]['AvgPMV'], 1, '.', '') : '-1.0') . ", 30]" . $delimeter;
                $mental_val_avg_week .= "[" . (!empty($moweekvalue[0]['AvgMMV']) ? number_format((float) $moweekvalue[0]['AvgMMV'], 1, '.', '') : '-1.0') . ", 20]" . $delimeter;

                $empty_general_graph_array[] = (!empty($moweekvalue[0]['AvgGMV']) ? $moweekvalue[0]['AvgGMV'] : '');
                $empty_physical_graph_array[] = (!empty($moweekvalue[0]['AvgPMV']) ? $moweekvalue[0]['AvgPMV'] : '');
                $empty_mental_graph_array[] = (!empty($moweekvalue[0]['AvgMMV']) ? $moweekvalue[0]['AvgMMV'] : '');
                $counth_data_mo_week++;
            }

            // assign data to veriables
            $xaxisData = $graph_day_list;

            $avg_values_general = $general_val_avg_week;
            $avg_values_physical = $physical_val_avg_week;
            $avg_values_mental = $mental_val_avg_week;
        }

        /*
         * Hourly graph data 
         */
        if ($filter_select == 'oneday') {
            //calculate time to get data 
           // $time_zone = $CusConPlug->getTimezone();
            $today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0, 0);
            $today->format('Y-m-d H:i:s');

            //Get UTC midnight time
            $today->setTimezone(new \DateTimeZone("UTC"));
            $start = $today->format('Y-m-d H:i:s');
            $week_array = $CusConPlug->time_difference($start);

            //$charttype = 'line';
            // Get one by one hour data 
            $i = 0;
            $count_loop_data = 1;
            foreach ($week_array as $wvalue) {
                $date = date('Y-m-d', strtotime($wvalue));
                $start_time = date('H:i:s', strtotime($wvalue));
                $end_time = date('H:i:s', strtotime($wvalue . ' +1 hour'));
                $whereWeekArrayDay = array(
                    'UserID' => $userId,
                    'current_date' => $date,
                    'start_time' => $start_time,
                    'end_time' => $end_time
                );

                $week_mooddata[$i] = $IndMoodTableObj->getmoodRecordOfDay($whereWeekArrayDay);

                $i++;
                $count_loop_data++;
            }
            /*
             * Mood graph weekly
             */

            $counth_data_mo_week = 1;
            foreach ($week_mooddata as $moweekvalue) {
                $cout_data_mo_week = count($week_mooddata);

                if ($counth_data_mo_week == $cout_data_mo_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }

                $general_val_avg_week .= "[" . (!empty($moweekvalue[0]['AvgGMV']) ? number_format((float) $moweekvalue[0]['AvgGMV'], 1, '.', '') : '-1.00') . ", 60]" . $delimeter;
                $physical_val_avg_week .= "[" . (!empty($moweekvalue[0]['AvgPMV']) ? number_format((float) $moweekvalue[0]['AvgPMV'], 1, '.', '') : '-1.00') . ", 30]" . $delimeter;
                $mental_val_avg_week .= "[" . (!empty($moweekvalue[0]['AvgMMV']) ? number_format((float) $moweekvalue[0]['AvgMMV'], 1, '.', '') : '-1.00') . ", 20]" . $delimeter;

                $empty_general_graph_array[] = (!empty($moweekvalue[0]['AvgGMV']) ? $moweekvalue[0]['AvgGMV'] : '');
                $empty_physical_graph_array[] = (!empty($moweekvalue[0]['AvgPMV']) ? $moweekvalue[0]['AvgPMV'] : '');
                $empty_mental_graph_array[] = (!empty($moweekvalue[0]['AvgMMV']) ? $moweekvalue[0]['AvgMMV'] : '');
                $counth_data_mo_week++;
            }



            $graph_day_list = "'0', '', '','3', '', '', '6', '', '', '9', '', '', '12', '', '', '15', '', '', '18', '', '', '21', '', '', '24'";


            $xaxisData = $graph_day_list;

            $avg_values_general = $general_val_avg_week;
            $avg_values_physical = $physical_val_avg_week;
            $avg_values_mental = $mental_val_avg_week;
        }
      
//        echo $avg_values_general;exit;
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  How I Feel',
                    'filter_select' => $filter_select, // selected filter value 
                    'isavalabledata' => $isavalabledata,
                    'general_today_val' => $general_today_val,
                    'physical_today_val' => $physical_today_val,
                    'mental_today_val' => $mental_today_val,
                    'xaxisData' => $xaxisData,
                    'yaxisData_general' => ((!empty(array_filter($empty_general_graph_array))) ? $avg_values_general : ""),
                    'yaxisData_physical' => ((!empty(array_filter($empty_physical_graph_array))) ? $avg_values_physical : ""),
                    'yaxisData_mental' => ((!empty(array_filter($empty_mental_graph_array))) ? $avg_values_mental : "")
        ));
    }

    /*     * ********
     * Action: This action is show  activity stage 
     * @author: shivendra Suman
     * Created Date: 22-01-2015.
     * *** */

    public function activitystageAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $userId = $this->_sessionObj->userDetails['UserID'];
        $IndActStgTableObj = $this->getServiceLocator()->get("IndividualActivityStageTable");
        $where = $columns = array();
        $lookingfor = "creation_date DESC";
        $activitystagedata = $IndActStgTableObj->getActivityStage($where, $columns, $lookingfor);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $activitySavArr['activity_name'] = $postDataArr['activity_name'];
            $activitySavArr['stage_name'] = $postDataArr['stage_name'];
            $activitySavArr['stage_min_val'] = $postDataArr['stage_min_val'];
            $activitySavArr['stage_max_val'] = $postDataArr['stage_max_val'];
            $activitySavArr['stage_color'] = "#".$postDataArr['stage_color'];
            $activitySavArr['creation_date'] = round(microtime(true) * 1000);
            $insdata = $IndActStgTableObj->insertActivityStage($activitySavArr);
            if ($insdata) {
                $activitystagedata = $IndActStgTableObj->getActivityStage($where, $columns, $lookingfor);
                $message = " successfully added.";
                $this->flashMessenger()->addMessage(array('success' => $message));
            } else {
                $message = "some error occured, please try again later.";
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Add Activity Stage',
                    'activitystagedata' => $activitystagedata
        ));
    }

}
