<?php

namespace Individual\Controller\Plugin;

/* * ***********************
 * PAGE: Heplper plugin.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman
 * CREATOR: 18/12/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Http\Client;
use Zend\Http\Client\Adapter\Curl;
use Zend\Session\Container;

class CustomControllerPlugin extends AbstractPlugin {

    var $emailfromtext;
    var $subject;
    var $emailto;
    var $message;
    var $emailtotext;
    var $port;
    var $emailFrom;
    var $host;
    var $password;
    var $from;
    var $ssl;
    var $EmailConfig;
    protected $messageObj;
    protected $serviceLocator;

    public function __construct() {

        $this->port = 587;

        $this->emailFrom = "AKIAJBM7REUBMKQMFPRQ";

        $this->host = "email-smtp.us-east-1.amazonaws.com";

        $this->emailSentFrom = "no-reply@quantextualhealth.com";

        $this->password = "AoCmpywXjBxSLdxi8u14llGoF4uODEJdJKpQQgxPV80v";

        $this->from = "Admin";

        $this->ssl = "tls";

        $this->EmailConfig = array(
            'host' => $this->host,
            'port' => $this->port,
            'connection_class' => 'login',
            'connection_config' => array(
                'ssl' => $this->ssl,
                'username' => $this->emailFrom,
                'password' => $this->password,
            ),
        );
        $this->_sessionObj = new Container('frontend');
    }

    /*
     * micro time 
     */

    public function currentmicrotime() {
        $data = round(microtime(true) * 1000);
        return $data;
    }

    /*
     * change time  to 24 hour formate
     */

    public function timeformate($date) {
        $data = date("H:i:s", strtotime($date));
        return $date;
    }

    /*
     * set time in hr and min 
     */

    public function timeformatehrm($time_val) {
        if ($time_val < 60) {
            $min_val = $time_val;
            $time_val = $min_val . ' <span class="unitValueSpan">min</span>';
        } else {
            $hour_val_temp = ($time_val / 60);
            $hour_val = (int) ($hour_val_temp);
            $min_val = round(($hour_val_temp - $hour_val) * 60);
            $time_val = $hour_val . '  <span class="unitValueSpan">h</span> ' . $min_val . ' <span class="unitValueSpan">min</span>';
        }
        return $time_val;
    }

    public function centimetersToFeetInches($centimeters) {
        $m = $centimeters;
        $valInFeet = $m * 0.0328084;
        $valFeet = (int) $valInFeet;
        $valInches = round(($valInFeet - $valFeet) * 12);
        $data = $valFeet . "&prime;" . $valInches . "&Prime;";
        return $data;
    }

    public function centimetersToFeetIncheswd($centimeters) {
        $m = $centimeters;
        $valInFeet = $m * 0.0328084;
        $valFeet = (int) $valInFeet;
        $valInches = round(($valInFeet - $valFeet) * 12);
        $data = $valFeet . "." . $valInches;
        return $data;
    }

    /*
      @ This function is used to convert centimeters to inches
     */

    public function centimetersToInches($centimeters) {
        $m = $centimeters;
        $valInches = round($m * 0.393701);
        $data = $valInches . '"';
        return $data;
    }

    public function centimetersToIncheswd($centimeters) {
        $m = $centimeters;
        $valInches = round($m * 0.393701);
        $data = $valInches . '"';
        return $data;
    }

    /*
      @ This function is used to convert feet into centimeters
     */

    public function FeetToCentimeters($feet) {
        return $feet * 30.48;
    }

    /*
      @ This function is used to convert inches into centimeters
     */

    public function InchesToCentimeters($inches) {
        return $inches * 2.54;
    }

    public function centimetersToMeter($centimeters) {
        $valInM = $centimeters * 0.01;
        $valM = $valInM;
        $data = $valM;
        return $data;
    }

    public function gramToKg($grams) {
        $valInKg = $grams * 0.001;
        $valKg = $valInKg;
        $data = $valKg;
        return $data;
    }

    public function poundToGram($pound) {

        $gram = $pound * 453.592;
        return $gram;
    }

    public function gramToPound($grams) {

        $lb = $grams * 0.0022046;
        return $lb;
    }

    public function calBMI($weightingram, $heightincm) {

        $valInKg = $weightingram * 0.001;
        $valKg = $valInKg;
        $weightInKg = $valKg;

        $valInM = $heightincm * 0.01;
        $valM = $valInM;
        $heightInMeter = $valM;
        $hsqure = ($heightInMeter * $heightInMeter);

        $BMI = ($weightInKg / $hsqure);
        $BMI = number_format((float) $BMI, 1, '.', '');
        return $BMI;
    }

    /*
     * kilojule to calories
     */

    public function kilojuleTocalorie($kj) {
        $valInCalorie = $kj * 238.8458966275;
        $data = number_format((float) $valInCalorie, 1, '.', '');
        return $data;
    }

    /*
     * calorie to kilojule
     */

    public function calorieTokilojule($calorie) {
        $valInKj = $calorie * 0.004184;
        $data = number_format((float) $valInKj, 1, '.', '');
        return $data;
    }

    /*
     * kilometer to meter
     */

    public function kmToM($km) {
        $valInM = $km * 1000;
        $data = number_format((float) $valInM, 1, '.', '');
        return $data;
    }

    /*
     * meter to kilometer
     */

    public function MToKm($m) {
        $valInKM = $m * 0.001;
        $data = number_format((float) $valInKM, 1, '.', '');
        return $data;
    }

    /*
     * meter to Mile
     */

    public function MToMile($m) {
        $valInMil = $m * 0.000621371;
        $data = number_format((float) $valInMil, 1, '.', '');
        return $data;
    }

    /*
     * Mile to meter
     */

    public function MileToM($mil) {
        $valInMeter = $mil * 1609.34;
        $data = number_format((float) $valInMeter, 1, '.', '');
        return $data;
    }

    /*
     *  MMO/l to Mg/DL
     */

    public function MMOLLToMGDL($ldlVal) {
        $ldlValInMMOL = ($ldlVal * 18); // conver into mg/dL
        $data = number_format((float) $ldlValInMMOL, 1, '.', ''); // set vale up to two decimal place                

        return $data;
    }

    /*
     * meter to MMO/l to Mg/DL
     */

    public function MGDLTOMMOLL($MMOLDL) {
        //  $ldlValInMMOL = $MMOLDL * 0.055555555555556; // conver into MMOL/dL
        $ldlValInMMOL = ($MMOLDL / 18);
        $data = number_format((float) $ldlValInMMOL, 1, '.', ''); // set vale up to two decimal place                

        return $data;
    }

    /*
     * meter to Celsius to Fahrenheit
     */

    public function CelTOFah($Cel) {
        // °C  x  9/5 + 32 = °F
        $DataFah = $Cel * 9 / 5 + 32; // conver into fahrenhite
        if ($Cel == 0 || empty($Cel)) {
            $DataFah = 0;
        }
        $data = number_format((float) $DataFah, 1, '.', ''); // set vale up to two decimal place 
        return $data;
    }

    /*
     * meter to Celsius to Fahrenheit
     */

    public function FahTOCel($Fah) {

        $DataCel = ($Fah - 32) * 5 / 9; // conver into fahrenhite
        if ($Fah == 0 || empty($Fah)) {
            $DataCel = 0;
        }
        $data = number_format((float) $DataCel, 1, '.', ''); // set vale up to two decimal place 
        return $data;
    }

    public function MinToHo($min) {
        $hour = ($min / 60);
        return $hour;
    }

    public function seondtominute($second) {
        $minute = ($second / 60);
        return $minute;
    }

    /*
     * Number formate
     */

    public function numberformate($number) {
        $data = number_format((float) $number, 1, '.', ''); // set vale up to 1 decimal place 
        return $data;
    }

    /*
     * if empty return 0
     */

    public function emptyreturn($values) {
        $data = (empty($values) ? 0 : $values);
        return $data;
    }

    /*
     * weeak list array
     */

    public function weekarray() {
        $data = array(
            'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'
        );

        return $data;
    }

    /*
     * least all month between two date 
     */

    function get_months($date1, $date2) {

        $time1 = strtotime($date1);
        $time2 = strtotime($date2);
        $my = date('m Y', $time2);
        $months = array(date('M', $time1));
        $f = '';

        while ($time1 < $time2) {
            $time1 = strtotime((date('Y-m-d', $time1) . ' +15days'));

            if (date('M', $time1) != $f) {
                $f = date('M', $time1);

                if (date('mY', $time1) != $my && ($time1 < $time2))
                    $months[] = date('M', $time1);
            }
        }

        $months[] = date('M', $time2);
        return $months;
    }

    /*
     * least all month with year 
     */

    function get_months_year($date1, $date2) {

        $time1 = strtotime($date1);
        $time2 = strtotime($date2);
        $my = date('mY', $time2);
        $months = array(date('Y-m', $time1));
        $f = '';

        while ($time1 < $time2) {
            $time1 = strtotime((date('Y-m-d', $time1) . ' +15days'));

            if (date('F', $time1) != $f) {
                $f = date('F', $time1);

                if (date('mY', $time1) != $my && ($time1 < $time2))
                    $months[] = date('Y-m', $time1);
            }
        }

        $months[] = date('Y-m', $time2);
        return $months;
    }

    /*
     * Author: Shivendra Suman
      @ month date day list
      Date : 08-01-2015
     */

    function month_year_lists($duration, $formate) {
        for ($i = $duration; $i > 0; $i--) {
            $month[] = date($formate, strtotime("-$i month"));
        }
        $month[] = date($formate);
        return $month;
    }

    /*
     * return date betwwn duration 
     */

    function dateduration($val) {
        $time = new \DateTime('now');
        $date = $time->modify($val)->format('Y-m-d');
        return $date;
    }

    /*
     * month list for graph
     */

    function graphmontlist($param) {
        $count_m = 1;
        $month_list = '';
        foreach ($param as $value) {
            $cout_data = count($param);

            if ($count_m == $cout_data) {
                $delimeter = '';
            } else {
                $delimeter = ",";
            }
            $month_list .= "'" . $value . "'" . $delimeter;


            $count_m++;
        }
        return $month_list;
    }

    /*
     * list of all dates between two date like  [date] => 26-11-2014
     */

    function date_difference($start, $end) {
        $first_date = strtotime($start);
        $second_date = strtotime($end);
        $offset = $second_date - $first_date;
        $result = $data = array();
        for ($i = 0; $i <= floor($offset / 24 / 60 / 60); $i++) {
            $result[1 + $i]['date'] = date('d-m-Y', strtotime($start . ' + ' . $i . '  days'));
        }
        foreach ($result as $rvalue) {
            $data[] = $rvalue['date'];
        }
        return $data;
    }

    /*
     * list of all only dates between two date like  [date] => 26
     */

    function datelist($start, $end) {
        $first_date = strtotime($start);
        $second_date = strtotime($end);
        $offset = $second_date - $first_date;
        $result = $data = array();
        for ($i = 0; $i <= floor($offset / 24 / 60 / 60); $i++) {
            $result[1 + $i]['date'] = date('d', strtotime($start . ' + ' . $i . '  days'));
        }
        foreach ($result as $rvalue) {
            $data[] = $rvalue['date'];
        }
        return $data;
    }

    /*
     * list of all  days between two date like  [day] => Mon
     */

    function daylist($start, $end) {
        $first_date = strtotime($start);
        $second_date = strtotime($end);
        $offset = $second_date - $first_date;
        $result = $data = array();
        for ($i = 0; $i <= floor($offset / 24 / 60 / 60); $i++) {
            $result[1 + $i]['day'] = date('D', strtotime($start . ' + ' . $i . ' days'));
        }
        foreach ($result as $rvalue) {
            $data[] = $rvalue['day'];
        }
        return $data;
    }

    function date_difference_alternate($start, $end) {
        $first_date = strtotime($start);
        $second_date = strtotime($end);
        $offset = $second_date - $first_date;
        $result = $data = array();
        for ($i = 0; $i <= floor($offset / 24 / 60 / 60); $i++) {
            $result[1 + $i]['date'] = date('d-m-Y', strtotime($start . ' + ' . $i . '  days'));
            $i++;
        }
        foreach ($result as $rvalue) {
            $data[] = $rvalue['date'];
        }
        return $data;
    }

    function datelist_alternate($start, $end) {
        $first_date = strtotime($start);
        $second_date = strtotime($end);
        $offset = $second_date - $first_date;
        $result = $data = array();
        for ($i = 0; $i <= floor($offset / 24 / 60 / 60); $i++) {
            $result[1 + $i]['date'] = date('d', strtotime($start . ' + ' . $i . '  days'));
            $i++;
        }
        foreach ($result as $rvalue) {
            $data[] = $rvalue['date'];
        }
        return $data;
    }

    function daylist_alternate($start, $end) {
        $first_date = strtotime($start);
        $second_date = strtotime($end);
        $offset = $second_date - $first_date;
        $result = $data = array();
        for ($i = 0; $i <= floor($offset / 24 / 60 / 60); $i++) {
            $result[1 + $i]['date'] = date('D', strtotime($start . ' + ' . $i . '  days'));
            $i++;
        }
        foreach ($result as $rvalue) {
            $data[] = $rvalue['date'];
        }
        return $data;
    }

    /*
     * list of all dates between two date like  [date] => 26-11-2014
     */

    function time_difference($start) {
        $result = $data = array();
        for ($i = 0; $i < 24; $i++) {
            $result[1 + $i]['date'] = date('Y-m-d H:i:s', strtotime($start . ' + ' . $i . '  hour'));
        }
        foreach ($result as $rvalue) {
            $data[] = $rvalue['date'];
        }
        return $data;
    }

    function agecalculater($date) {
        # object orientednew \
        $from = new \DateTime($date);
        $to = new \DateTime('today');
        return $from->diff($to)->y;
    }

    function encryptdata($mprhase) {
//         $MASTERKEY = "healthmatrix";
//     $td = mcrypt_module_open('tripledes', '', 'ecb', '');
//     $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
//     mcrypt_generic_init($td, $MASTERKEY, $iv);
//      $return_value = base64_encode(mcrypt_generic($td, $mprhase));
//      mcrypt_generic_deinit($td);
//     mcrypt_module_close($td);
//     return $return_value;
        return base64_encode($mprhase);
    }

    function decryptdata($mprhase) {
//         $MASTERKEY = "healthmatrix";
//     $td = mcrypt_module_open('tripledes', '', 'ecb', '');
//     $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
//     mcrypt_generic_init($td, $MASTERKEY, $iv);
//      $return_value = mdecrypt_generic($td, base64_decode($mprhase));
//      mcrypt_generic_deinit($td);
//     mcrypt_module_close($td);
//     return $return_value;
        return base64_decode($mprhase);
    }

    /*
      @ curl method for restfull api hit
      @ 31-12-2014
      @ shivedra Suman
     */

    function curlPost($url, $postParams) {
        //some post params
        //$postParams = array('per_page' => 25);
        $adapter = new Curl();
        // -- add your curl options here (excluding CURLOPT_POST and CURLOP_POSTFIELDS --
        $client = new Client($url);
        $client->setAdapter($adapter);
        $client->setMethod('POST');
        $client->setParameterPost($postParams);
        $response = $client->send($client->getRequest());
        //output the response
        return $response->getBody();
    }

    function curlPostValidic($url, $data) {

        $fields_string = json_encode($data);
        //open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        // curl_setopt($ch, CURLetOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($fields_string)
        ));

        //execute post
        $result = curl_exec($ch);
//        echo"<pre >";
//        print_r($result);
        return $result;
        //close connection
        curl_close($ch);
    }

    function curlGet($url) {
        $adapter = new Curl();
        $client = new Client($url);
        $client->setAdapter($adapter);
        $client->setMethod('GET');
        $response = $client->send($client->getRequest());
        return $response->getBody();
    }

    /*     * **************

     * @function :  used for send forgot mail to admin

     * @params   :  array

     * @return   :  boo;l	

     * @author   :  Akhilesh Singh.

     * @Date     :  2014-7-7

     * *** */

    function senInvitationMail($email, $subject, $message, $from_email = null, $from_name = null) {
        $this->messageObj = new Message();
        $html = new MimePart($message);
        $html->type = "text/html";
        $body = new MimeMessage();
        $body->addPart($html);
        $_replyToEmail = '';
        $_fromName = 'Quantextual Health';
        if ($from_email) {
            $_replyToEmail = $from_email;
            $_fromName = $from_name;
        }

        //$message->setBody($body);
        if (is_array($email)) {
            $this->messageObj->addTo('no-reply@quantextualhealth.com');
            foreach ($email as $Email) {
                $this->messageObj->addBcc($Email);
            }
            $this->messageObj->addFrom($this->emailSentFrom, $_fromName)
                    ->setSubject($subject)
                    ->setBody($body);
        } else { //$message
            $this->messageObj->addTo($email)
                    //->setEncoding("UTF-8")
                    ->addFrom($this->emailSentFrom, $_fromName)
                    ->setSubject($subject)
                    ->setBody($body);
            if ($from_email)
                $this->messageObj->addreplyTo($_replyToEmail);
        }
        $transport = new SmtpTransport();
        $options = new SmtpOptions($this->EmailConfig);
        $transport->setOptions($options);
        //  pr($config);
        /* SMTP MAIL  START */
        try {
            $transport->send($this->messageObj);
//            //$transport->clearHeaders();
            $mailMes = 1;
        } catch (Exception $e) {
            pr($e->getMessage());
            $mailMes = 0;
        }

        //echo $mailMes;exit;
    }

    function GetAge($dob) {
        list($year, $month, $day) = explode("-", $dob);
        $year_diff = date("Y") - $year;
        $month_diff = date("m") - $month;
        $day_diff = date("d") - $day;
        if ($day_diff < 0 && $month_diff == 0)
            $year_diff--;
        if ($day_diff < 0 && $month_diff < 0)
            $year_diff--;
        return $year_diff;
    }

    function cal_birthday($dob) {
        $interval = date_diff(date_create(), date_create($dob));
        return $interval->format("%Y years %M months %d days Old");
    }

    function cal_upcoming_birthday($dob) {
        $interval = date_diff(date_create($dob), date_create());
        return $interval->format("%M months %d days from now");
    }

//$keys = "1234";
    /**
     * Returns an encrypted & utf8-encoded
     */
    function EncryptDecrypt($mprhase, $crypt = false) {
        $MASTERKEY = "KEY PHRASE!";
        $td = mcrypt_module_open('tripledes', '', 'ecb', '');
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $MASTERKEY, $iv);
        if ($crypt == 'encrypt') {
            //$return_value = base64_encode(mcrypt_generic($td, $mprhase));
            $return_value = base64_encode($mprhase);
        } else {
            $return_value = base64_decode($mprhase);
            //$return_value = mdecrypt_generic($td, base64_decode($mprhase));
        }
        //mcrypt_generic_deinit($td);
        //mcrypt_module_close($td);
        return $return_value;
    }

    /**
     * Returns decrypted original string
     */
    function decryptIds($encrypted_string, $keys = "healthmetrix123") {
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $keys, $encrypted_string, MCRYPT_MODE_ECB, $iv);
        return $decrypted_string;
    }

    /*     * **********This function is used to check the existance of PASS*********************** */

    function ValidateDomainIdentity($IndObj) {
        $siteURL = $_SERVER['HTTP_HOST'];
        $siteArray = explode('.', $siteURL);
        if (strtolower($siteArray[0]) == 'www') {
            $siteDomain = $siteArray[1];
        } else {
            $siteDomain = $siteArray[0];
        }
        $IndUserTableObj = $IndObj->get("IndividualUsersTable");
        $Valid = $IndUserTableObj->validateDomain(array('subdomain' => $siteDomain));
        return $Valid['id'];
    }

    /*     * ***This function is used to Convert time into UTC****** */

    public function getTimezone() {
        //$_SERVER["REMOTE_ADDR"]
        $ip = $_SERVER['REMOTE_ADDR']; // the IP address to query
        echo "ip" . $ip;
        if ($ip == '127.0.0.1') {
            $ip = '180.151.12.26';
        }
        $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
        $timeZone = $query['timezone'];
        return $timeZone;
    }

    public function convetUTC($datetime, $time_zone = false) {

        if (!$time_zone) {
            $time_zone = $this->_sessionObj->userDetails['time_zone'];
        }

        if (!$time_zone) {
            $ip = $_SERVER['REMOTE_ADDR']; // the IP address to query
            if ($ip == '127.0.0.1') {
                $ip = '180.151.12.26';
            }

            $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
            $time_zone = $query['timezone'];
        }
//        $date = new \DateTime($datetime, new \DateTimeZone($timeZone));
//        date_default_timezone_set('UTC');
//        $dateT = date("Y-m-d H:i:s", $date->format('U'));
//         echo date("Y-m-d H:i:s",$date->format('U'))."<br >";
//         echo $date->format('U');die;

        /*         * ******Changed by Rabban******** */

        $comingDateData = date("Y-m-d H:i:s", strtotime($datetime));
        $con = explode(" ", $comingDateData);
        $ComingDate = explode("-", $con[0]);
        $ComingTime = explode(":", $con[1]);

        $date = new \DateTime();
        $date->setTimezone(new \DateTimeZone($time_zone));
        $date->setDate($ComingDate[0], $ComingDate[1], $ComingDate[2]);
        $date->setTime($ComingTime[0], $ComingTime[1], $ComingTime[2]);
        $date->setTimezone(new \DateTimeZone('UTC'));
        $m = $date->format('Ymd\THis\Z'); // format string to match question
        $convertedDate = date("Y-m-d H:i:s", strtotime($m));
        date_default_timezone_set('UTC');

        /*         * *****End ***** */
        $dateTimeArr = explode(" ", $convertedDate);
        //echo '<pre>';print_r($dateTimeArr);die;
        return $dateTimeArr;
    }

    /*     * ****Thsi function is used to convert UTC to local time****** */

    public function convetLocalTime($datetime, $time_zone = false) {

        if (!$time_zone) {
            $time_zone = $this->_sessionObj->userDetails['time_zone'];
        }
        if (!$time_zone) {
            $ip = $_SERVER['REMOTE_ADDR']; // the IP address to query
            if ($ip == '127.0.0.1') {
                $ip = '180.151.12.26';
            }
            $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
            $time_zone = $query['timezone'];
        }

        $date = new \DateTime($datetime, new \DateTimeZone('UTC'));
        date_default_timezone_set($time_zone);
        $dateT = date("Y-m-d H:i:s", $date->format('U'));
        date_default_timezone_set('UTC');
        $dateTimeArr = explode(" ", $dateT);
        return $dateTimeArr;
    }

    public function isJSON($string) {
        return is_string($string) && is_object(json_decode($string)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

}
