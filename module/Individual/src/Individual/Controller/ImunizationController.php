<?php

/*
  @ Used: This class is used to manage Imunization
  @ Created By: Rabban Ahmad
  @ Created On: 10/1/15
  @ Updated On: 10/1/15
  @ Zend Framework (http://framework.zend.com/)
 */

namespace Individual\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class ImunizationController extends \Zend\Mvc\Controller\AbstractActionController {

    public function __construct() {
        
    }

    public function init() {
        $this->_sessionObj = new Container('frontend');
    }

    public function indexAction() {
           
        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $ImunizationUserTableObj = $this->getServiceLocator()->get("IndividualUsersImunizationTable");
        $userImunizations = $ImunizationUserTableObj->getUserImunizationList(array('user_id' => $UserID, 'imu.status' => 1));
        $this->flashMessenger()->addMessage(array('success' => '', 'error' => ''));
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  Imunization',
                    'user_Imunizations' => $userImunizations,'plugin'=>$CusConPlug
        ));
    }

    public function addimmunizationAction() {
        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $request = $this->getRequest();
        $error = "";
        if ($request->isPost()) {
            $requestArr = $request->getPost()->toArray();
            $UserID = $this->_sessionObj->userDetails['UserID'];
             $time_zone =  $this->_sessionObj->userDetails['time_zone'];
            if ($requestArr['immunization_type'] == "") {
                $error = "Please select immunization type";
                $this->flashMessenger()->addMessage(array('error' => $error));
            } else {
                $ImunizationUserTableObj = $this->getServiceLocator()->get("IndividualUsersImunizationTable");
                $userImunizations = $ImunizationUserTableObj->getUserImunizationList(array('user_id' => $UserID, 'imu.status' => 1, 'imu.imunization_id' => $requestArr['immunization_type']));
                if (count($userImunizations)) {
                    $error = "Immunization already added.Please select another immunization type.";
                    $this->flashMessenger()->addMessage(array('error' => $error));
                } else {
                    $taking_date = date('Y-m-d', strtotime($requestArr['taking_date']));
                    $taking_time = date('H:i:s', strtotime($requestArr['taking_time']));
                    $DateTimeRes = $CusConPlug->convetUTC($taking_date . " " . $taking_time, $time_zone);
                    $taking_date = $DateTimeRes[0];
                    $taking_time = $DateTimeRes[1];
                    $immuData = array('user_id' => $UserID, 'imunization_id' => $requestArr['immunization_type'], 'taking_date' => $taking_date, 'taking_time' => $taking_time, 'creation_date' => round(microtime(true) * 1000),
                        'updation_date' => round(microtime(true) * 1000));
                    $ImunizationUserTableObj->SaveUserImunization($immuData);
                    $message = "Immunization added successfully. ";
                    $this->flashMessenger()->addMessage(array('success' => $message));
                }
            }
        }
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $ImunizationUserTableObj = $this->getServiceLocator()->get("IndividualUsersImunizationTable");
        $ImunizationsType = $ImunizationUserTableObj->getImunizationType();
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  Imunization',
                    'immunization_type' => $ImunizationsType
        ));
    }

}

?>