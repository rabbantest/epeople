<?php
/* * ***********************
 * PAGE: USE TO MANAGE THE BLOG.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 25/02/2015.
 * UPDATED: 00/00/0000.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class BlogController extends AbstractActionController {

    public function __construct() {
        $this->_sessionObj = new Container('frontend');
    }
    

            
    public function indexAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }     
      $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
       $IndBlogTableObj = $this->getServiceLocator()->get("IndividualBlogTable");
       //get blog list 
       $sort =   $this->params()->fromQuery("sort");
  
       $searchcontent = array();
       $itemsPerPage = 10;
       $whereBlog= array('status' => 1, 'published_status' => 1);
       $coloumblog = array();
       $lookingfor = "creation_date DESC";

       //sort by 
       if(!empty($sort)){
           if ($sort == 'monthd') {
                $lookingfor = "creation_date DESC";
            }
            if ($sort == 'montha') {
                $lookingfor = "creation_date ASC";
            }
            if ($sort == 'cata') {
                $lookingfor = "cat.cat_name ASC";
            }
             if ($sort == 'catd') {
                $lookingfor = "cat.cat_name DESC";
            }
            if ($sort == 'autha') {
                $lookingfor = "auth.author_name ASC";
            }
            if ($sort == 'authd') {
                $lookingfor = "auth.author_name DESC";
            }
       }
    
       $blogs =   $IndBlogTableObj->getBlogs($whereBlog, $coloumblog, $lookingfor);
      
       //get blog cat list 
       $i=0;
       foreach ($blogs as $bvalue) {
            $where = array('cat_id'=>$bvalue['cat_id']);
            $columns=array('cat_id','cat_name');
           
            $blog_cat =   $IndBlogTableObj->getBlogcat($where, $columns);
            $blogs[$i]['cat_name'] = (isset($blog_cat[0]['cat_name'])? $blog_cat[0]['cat_name'] : '' );  
             
            $whereAuth = array('author_id'=>$bvalue['author_id']);
            $columnsAuth=array('author_id','author_name');
            
            $blog_auth =   $IndBlogTableObj->getBlogauth($whereAuth, $columnsAuth);
            $blogs[$i]['author_name'] = (isset($blog_auth[0]['author_name'])? $blog_auth[0]['author_name'] : '' );  
         
            $i++;
       }   
       //pagination data 
        if (is_array($blogs)) {
            $iterateddata = $blogs;
            $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($iterateddata));
            $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
            $paginator->setItemCountPerPage($itemsPerPage);
        }


        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => "Individual Blogs",
                    'CusConPlug' => $CusConPlug,
                    'sort' => $sort,
                    'blogData' =>  $paginator,  
                    'routeParams' => $searchcontent,
                    
        ));        
       
    }
    
     public function viewAction(){
             if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }     
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $IndBlogTableObj = $this->getServiceLocator()->get("IndividualBlogTable");
        $Id = $this->params('id');       
        $blogId = $CusConPlug->decryptdata($Id); // insurance id decode
        if(!empty($blogId)){
        $whereBlog = array('blog_id' => $blogId);
        $blogs =   $IndBlogTableObj->getBlogs($whereBlog);
      
        if(count($blogs)>0){
            $where = array('cat_id'=>$blogs[0]['cat_id']);
            $columns=array('cat_id','cat_name');
            $blog_cat =   $IndBlogTableObj->getBlogcat($where, $columns);
            $blogs[0]['cat_name'] = (isset($blog_cat[0]['cat_name'])? $blog_cat[0]['cat_name'] : '' );    
          
            $whereAuth = array('author_id'=>$blogs[0]['author_id']);
            $columnsAuth=array('author_id','author_name');
            
            $blog_auth =   $IndBlogTableObj->getBlogauth($whereAuth, $columnsAuth);
            $blogs[0]['author_name'] = (isset($blog_auth[0]['author_name'])? $blog_auth[0]['author_name'] : '' );  
         
             $blogData = $blogs[0];
      }else{
          //send error if insurance id not found
                $this->getResponse()->setStatusCode(404);
                return;
      }
        }else{
             //send error if insurance id not found
                $this->getResponse()->setStatusCode(404);
                return;
        }
       

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => "Individual Blogs ",
                    'CusConPlug' => $CusConPlug,
                     'blogData' =>  $blogData,  
                   // 'routeParams' => $searchcontent,
                    
        ));    
        }
    
}