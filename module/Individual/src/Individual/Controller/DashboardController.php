<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE ROOT CONTROLLER FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 07/12/2014.
 * UPDATED: 07/12/2014.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

//use Zend\Mvc

class DashboardController extends AbstractActionController {

    public function __construct() {
        
    }

    public function init() {
        $this->_sessionObj = new Container('frontend');
    }

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/');
        }

        $userId = $this->_sessionObj->userDetails['UserID'];
        $time_zone = $this->_sessionObj->userDetails['time_zone'];
        $charttype = 'column';
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin

        $filter_select = 'oneweek'; // asigne veriable by default all
        $request = $this->getRequest(); //get request 

        $IndHeightTableObj = $this->getServiceLocator()->get("IndividualHeightRecordTable");
        $IndWeightTableObj = $this->getServiceLocator()->get("IndividualWeightRecordTable");
        $IndWaistTableObj = $this->getServiceLocator()->get("IndividualWaistRecordTable");
        $IndBmiTableObj = $this->getServiceLocator()->get("IndividualBmiRecordTable");
        $IndBpTableObj = $this->getServiceLocator()->get('IndividualBloodPressureRecordTable');
        $TempTableObj = $this->getServiceLocator()->get('IndividualTemperatureRecordTable');
        $SleepTableObj = $this->getServiceLocator()->get('IndividualSleepRecordTable');
        $IndPhyTableObj = $this->getServiceLocator()->get("IndividualPhysicalActivityTable");
        $IndChoTableObj = $this->getServiceLocator()->get("IndividualCholesterolRecordTable");
        $IndBgTableObj = $this->getServiceLocator()->get('IndividualBloodGlucoseRecordTable');
        $IndUserConditionTableObj = $this->getServiceLocator()->get("IndividualUsersConditionTable");
        $IndMoodTableObj = $this->getServiceLocator()->get("IndividualMoodRecordTable");
        $IndDataSourceTableObj = $this->getServiceLocator()->get("DataSource");


        //Default source 
        $sleepSource = $this->_sessionObj->userDetails['sleep'];
        $bloodglucoseSource = $this->_sessionObj->userDetails['bloodglucose'];
        $bodyweightSource = $this->_sessionObj->userDetails['bodyweight'];
        $stepSource = $this->_sessionObj->userDetails['step'];
        $physicalactivitySource = $this->_sessionObj->userDetails['physicalactivity'];
        $CholesterolSource = $this->_sessionObj->userDetails['cholesterol'];
        $PressureSource = $this->_sessionObj->userDetails['bloodpressure'];
        $temperatureSource = $this->_sessionObj->userDetails['temperature'];

        //Default source  name
        $sleepSourceName = $this->_getSourceNameAction($sleepSource);
        $bloodglucoseSourceName = $this->_getSourceNameAction($bloodglucoseSource);
        $bodyweightSourceName = $this->_getSourceNameAction($bodyweightSource);
        $stepSourceName = $this->_getSourceNameAction($stepSource);
        $physicalactivitySourceName = $this->_getSourceNameAction($physicalactivitySource);
        $CholesterolSourceName = $this->_getSourceNameAction($CholesterolSource);
        $PressureSourceName = $this->_getSourceNameAction($PressureSource);
        $temperatureSourceName = $this->_getSourceNameAction($temperatureSource);

        /*
          @Author: Shivendra Suman
          @ get condation symtempous and treatment of current user
         */
        /*
         * defive all veriables 
         */
        $graph_month_list = array();
        $month_list = $weight_val_avg_new_week = $height_val_avg_new_week = $bmi_val_avg_new_week = $systolic_val_avg_new_week = $diastolic_val_avg_new_week = $puls_val_avg_new_week = $duration_val_avg_week = $avg_distance_values_week = $steps_val_avg_week = $cb_val_avg_week = $avg_ldl_values_week = $avg_hdl_values_week = $avg_triglycerides_values_week = $avg_measurement_values_week = $avg_temperature_values_week = $avg_awake_values_week = $avg_deep_values_week = $avg_light_values_week = $avg_rem_values_week = $avg_times_woken_values_week = $avg_total_sleep_values_week = $general_val_avg_week = $physical_val_avg_week = $mental_val_avg_week = $diastolic_val_avg_new = $bmi_val_avg_new = $systolic_val_avg_new = $diastolic_val_avg_new = $puls_val_avg_new = $duration_val_avg_new = $avg_distance_values_new = $steps_val_avg_new = $cb_val_avg_new = $avg_ldl_values_new = $avg_hdl_values_new = $avg_triglycerides_values_new = $avg_measurement_values_new = $avg_temperature_values_new = $avg_awake_values_new = $avg_deep_values_new = $avg_light_values_new = $avg_rem_values_new = $avg_times_woken_values_new = $avg_total_sleep_values_new = $general_mood_val_avg = $physical_mood_val_avg = $mental_mood_val_avg = $weight_val_avg_new = $height_val_avg_new = $TotalStep_val_week = $TotalStep_val = '';
        $empty_awake_graph_array = array();
        $empty_deep_graph_array = array();
        $empty_light_graph_array = array();
        $empty_rem_graph_array = array();
        $empty_general_graph_array = array();
        $empty_physical_graph_array = array();
        $empty_mental_graph_array = array();
        $empty_temperature_graph_array = array();
        $empty_bmi_graph_array = array();
        $empty_height_graph_array = array();
        $empty_weight_graph_array = array();
        $empty_steps_graph_array = array();
        $empty_distance_graph_array = array();
        $empty_duration_graph_array = array();
        $empty_calories_graph_array = array();
        $empty_ldl_graph_array = array();
        $empty_hdl_graph_array = array();
        $empty_Triglyceride_graph_array = array();
        $empty_systoli_graph_array = array();
        $empty_diastolic_graph_array = array();
        $empty_pulse_graph_array = array();
        $empty_glucose_graph_array = array();
        $empty_total_sleep_graph_array = array();

        $conditionslist = array();
        $whereArrCond = array("user_id" => $userId, 'con.status' => 1);
        $conditionslist = $IndUserConditionTableObj->getUserAllConditions($whereArrCond);

        $finalRes = array();
        $result = array();

        if (count($conditionslist) > 0) {

            foreach ($conditionslist as $val) {
                $result['condition_name'] = $val['common_name'];
                $result['sub_condition_name'] = $val['sub_condition_name'];
                $whereSynArr = array("user_id" => $userId, 'condition_id' => $val['condition_id']);
                $result['symtoms_name'] = '';
                $result['sub_symtoms_name'] = '';
                $Symtoms = $IndUserConditionTableObj->getUserAllSymtomsAccordingCondion($whereSynArr);
                if (count($Symtoms)) {
                    $result['symtoms_name'] = $Symtoms[0]['symtoms_name'];
                    $result['sub_symtoms_name'] = $Symtoms[0]['sub_symtoms_name'];
                }
                $result['treatment_name'] = '';
                $result['sub_treatment_name'] = '';
                $Treaments = $IndUserConditionTableObj->getUserAllTreatmentAccordingCondion($whereSynArr);
                if (count($Treaments)) {
                    $result['treatment_name'] = $Treaments[0]['treatment_name'];
                    $result['sub_treatment_name'] = $Treaments[0]['sub_treatment_name'];
                }
                $finalRes[] = $result;
            }
        }

        //  get filter current value 
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $filter_select = $postDataArr['filter_select'];
        }

        $IndHeightDatas = $IndWeightDatas = $IndWaistDatas = $columns = array();
        $avg_values_height = $avg_values_weight = $avg_values_bmi = $avg_values_systolic = $avg_values_diastolic = $avg_values_puls = $avg_values_duration = $avg_values_distance = $avg_values_steps = $avg_values_cb = $avg_values_ldl = $avg_values_hdl = $avg_values_triglycerides = $avg_values_measurement = $avg_values_temperature = $avg_values_total_sleep = '';
        $dataTimeInterval = 'INTERVAL 1 YEAR';
        $dataTimeIntervalGraph = $filter_select;

        /*
         * get data for dispaly graph for all 
         */
        /*
         * monthly graph data 
         */
        if ($filter_select == 'oneyear' || $filter_select == 'sixmonth' || $filter_select == 'threemonth') {

            $time = new \DateTime('now');
            if ($filter_select == 'oneyear') {
                $duration = 12;
                $formate = 'Y-m';
                $formate2 = 'M';
                $month_array = $CusConPlug->month_year_lists($duration, $formate);
                $month_array_list = $CusConPlug->month_year_lists($duration, $formate2);
            }
            if ($filter_select == 'sixmonth') {
                $duration = 5;
                $formate = 'Y-m';
                $formate2 = 'M';
                $month_array = $CusConPlug->month_year_lists($duration, $formate);
                $month_array_list = $CusConPlug->month_year_lists($duration, $formate2);
            }
            if ($filter_select == 'threemonth') {
                $duration = 2;
                $formate = 'Y-m';
                $formate2 = 'M';
                $month_array = $CusConPlug->month_year_lists($duration, $formate);
                $month_array_list = $CusConPlug->month_year_lists($duration, $formate2);
            }
            $graph_month_list = $CusConPlug->graphmontlist($month_array_list); // arrange month list a/c to graph
            // get one by one month daata 
            $i = 0;
            foreach ($month_array as $mvalue) {

                $start_date = date("$mvalue-01");
                $end_date = date("$mvalue-t");

                $wheredataArrayDayweightheight = array('UserID' => $userId, 'start_date' => $start_date, 'end_date' => $end_date, 'source' => $bodyweightSourceName);
                $wheredataArrayDaybmi = array('UserID' => $userId, 'start_date' => $start_date, 'end_date' => $end_date, 'source' => $bodyweightSourceName);
                $wheredataArrayDaybp = array('UserID' => $userId, 'start_date' => $start_date, 'end_date' => $end_date, 'source' => $PressureSourceName); //bp
                $wheredataArrayDayphydata = array('UserID' => $userId, 'start_date' => $start_date, 'end_date' => $end_date, 'source' => $physicalactivitySourceName); //physical data
                $wheredataArrayDayphydatastep = array('UserID' => $userId, 'start_date' => $start_date, 'end_date' => $end_date, 'source' => $stepSourceName); //steps
                $wheredataArrayDaychodata = array('UserID' => $userId, 'start_date' => $start_date, 'end_date' => $end_date, 'source' => $CholesterolSourceName); //cholestrol
                $wheredataArrayDaybgdata = array('UserID' => $userId, 'start_date' => $start_date, 'end_date' => $end_date, 'source' => $bloodglucoseSourceName); //bloodglucose
                $wheredataArrayDaytempdata = array('UserID' => $userId, 'start_date' => $start_date, 'end_date' => $end_date, 'source' => $temperatureSourceName); //tempdata
                $wheredataArrayDaysleepdata = array('UserID' => $userId, 'start_date' => $start_date, 'end_date' => $end_date, 'source' => $sleepSourceName); //sleepdata
                $wheredataArrayDaymooddata = array('UserID' => $userId, 'start_date' => $start_date, 'end_date' => $end_date); //mooddata

                $weightdata[$i] = $IndWeightTableObj->getWeightRecordByYear($wheredataArrayDayweightheight);
                $heightdata[$i] = $IndHeightTableObj->getHeightRecordByYear($wheredataArrayDayweightheight);
                $bmidata[$i] = $IndBmiTableObj->getBmiRecordByYear($wheredataArrayDaybmi);
                $bpdata[$i] = $IndBpTableObj->getBpRecordByYear($wheredataArrayDaybp);
                $phydata[$i] = $IndPhyTableObj->getPhyAcRecordByYear($wheredataArrayDayphydata);
                $phydatastep[$i] = $IndPhyTableObj->getPhyAcRecordByYearSteps($wheredataArrayDayphydatastep);
                $chodata[$i] = $IndChoTableObj->getCholestrolRecordByYear($wheredataArrayDaychodata);
                $bgdata[$i] = $IndBgTableObj->getBgRecordByYear($wheredataArrayDaybgdata);
                $tempdata[$i] = $TempTableObj->gettempRecordByYear($wheredataArrayDaytempdata);
                $sleepdata[$i] = $SleepTableObj->getSleepRecordByYear($wheredataArrayDaysleepdata);
                $mooddata[$i] = $IndMoodTableObj->getmoodRecordByYear($wheredataArrayDaymooddata);

                $i++;
            }

            // weight data of graph
            $counth_data_weight = 1;
            foreach ($weightdata as $wevalue) {
                $cout_data_weight = count($weightdata);

                if ($counth_data_weight == $cout_data_weight) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }

                $weight_val_avg = (!empty($wevalue[0]['Avgweight']) ? $wevalue[0]['Avgweight'] : '0.0');
                $weight_val_avg_new .= number_format((float) $CusConPlug->gramToPound($weight_val_avg), 1, '.', '') . $delimeter;
                $empty_weight_graph_array[] = (!empty($wevalue[0]['Avgweight']) ? $wevalue[0]['Avgweight'] : '');
                $counth_data_weight++;
            }

            // height data of graph
            $counth_data_height = 1;
            foreach ($heightdata as $heivalue) {
                $cout_data_height = count($heightdata);

                if ($counth_data_height == $cout_data_height) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $height_val_avg = (!empty($heivalue[0]['Avgheight']) ? $heivalue[0]['Avgheight'] : '0.0');

                $height_val_avg_new .= number_format((float) $CusConPlug->centimetersToIncheswd($height_val_avg), 1, '.', '') . $delimeter;
                $empty_height_graph_array[] = (!empty($heivalue[0]['Avgheight']) ? $heivalue[0]['Avgheight'] : '');
                $counth_data_height++;
            }

            // bmi data of graph
            $counth_data_bmi = 1;
            foreach ($bmidata as $bmvalue) {
                $cout_data_bmi = count($bmidata);

                if ($counth_data_bmi == $cout_data_bmi) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $bmi_val_avg = (!empty($bmvalue[0]['Avgbmi']) ? $bmvalue[0]['Avgbmi'] : '0.0');

                $bmi_val_avg_new .= number_format((float) $bmi_val_avg, 1, '.', '') . $delimeter;
                $empty_bmi_graph_array[] = (!empty($bmvalue[0]['Avgbmi']) ? $bmvalue[0]['Avgbmi'] : '');
                $counth_data_bmi++;
            }

            /*
             * Blood pressure data graph
             */

            $counth_data_bp = 1;
            foreach ($bpdata as $svalue) {
                $cout_data_bp = count($bpdata);

                if ($counth_data_bp == $cout_data_bp) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $systolic_val_avg = (!empty($svalue[0]['Avgsystolic']) ? $svalue[0]['Avgsystolic'] : '0.0');
                $diastolic_val_avg = (!empty($svalue[0]['Avgdiastolic']) ? $svalue[0]['Avgdiastolic'] : '0.0');
                $puls_val_avg = (!empty($svalue[0]['Avgpulse']) ? $svalue[0]['Avgpulse'] : '0.0');

                $systolic_val_avg_new .= number_format((float) $systolic_val_avg, 1, '.', '') . $delimeter;
                $diastolic_val_avg_new .= number_format((float) $diastolic_val_avg, 1, '.', '') . $delimeter;
                $puls_val_avg_new .= number_format((float) $puls_val_avg, 1, '.', '') . $delimeter;
                $empty_systoli_graph_array[] = (!empty($svalue[0]['Avgsystolic']) ? $svalue[0]['Avgsystolic'] : '');
                $empty_diastolic_graph_array[] = (!empty($svalue[0]['Avgdiastolic']) ? $svalue[0]['Avgdiastolic'] : '');
                $empty_pulse_graph_array[] = (!empty($svalue[0]['Avgpulse']) ? $svalue[0]['Avgpulse'] : '');
                $counth_data_bp++;
            }
            /*
             * Physical Activity
             */

            $counth_data_phy = 1;
            foreach ($phydata as $svalue) {
                $cout_data_phy = count($phydata);

                if ($counth_data_phy == $cout_data_phy) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $duration_val_avg = (!empty($svalue[0]['AvgDuration']) ? $svalue[0]['AvgDuration'] : '0.0');
                $distance_val_avg = (!empty($svalue[0]['AvgDistance']) ? $svalue[0]['AvgDistance'] : '0.0');
                // $steps_val_avg = (!empty($svalue[0]['AvgSteps']) ? $svalue[0]['AvgSteps'] : '0.0');
                $cb_val_avg = (!empty($svalue[0]['AvgCB']) ? $svalue[0]['AvgCB'] : '0.0');

                $duration_val_avg_new .= round($duration_val_avg) . $delimeter;
                $avg_distance_values_new .= number_format((float) $CusConPlug->MToMile($distance_val_avg), 1, '.', '') . $delimeter;
                $cb_val_avg_new .= number_format((float) $cb_val_avg, 1, '.', '') . $delimeter;
                $empty_distance_graph_array[] = (!empty($svalue[0]['AvgDistance']) ? $svalue[0]['AvgDistance'] : '');
                $empty_duration_graph_array[] = (!empty($svalue[0]['AvgDuration']) ? $svalue[0]['AvgDuration'] : '');
                $empty_calories_graph_array[] = (!empty($svalue[0]['AvgCB']) ? $svalue[0]['AvgCB'] : '');
                $counth_data_phy++;
            }
            /*
             * steps 
             */

            // get data from loop 
            $counth_data_step = 1;
            foreach ($phydatastep as $stvalue) {
                $cout_data = count($phydatastep);

                if ($counth_data_step == $cout_data) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $steps_val_avg = (!empty($stvalue[0]['TotalStep']) ? $stvalue[0]['TotalStep'] : '0.0');

                $steps_val_avg_new .= round($steps_val_avg) . $delimeter;
                $empty_steps_graph_array[] = (!empty($stvalue[0]['TotalStep']) ? $stvalue[0]['TotalStep'] : '');
                $TotalStep_val += $stvalue[0]['TotalStep'];
                $counth_data_step++;
            }
            /*
             * Cholesterol graph data 
             */

            $counth_data_cho = 1;
            foreach ($chodata as $chovalue) {
                $cout_data_cho = count($chodata);

                if ($counth_data_cho == $cout_data_cho) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $ldl_val_avg = (!empty($chovalue[0]['AvgLDL']) ? $chovalue[0]['AvgLDL'] : '0.0');
                $hdl_val_avg = (!empty($chovalue[0]['AvgHDL']) ? $chovalue[0]['AvgHDL'] : '0.0');
                $triglycerides_val_avg = (!empty($chovalue[0]['AvgTriglycerides']) ? $chovalue[0]['AvgTriglycerides'] : '0.0');
                $avg_ldl_values_new .= number_format((float) $ldl_val_avg, 1, '.', '') . $delimeter;
                $avg_hdl_values_new .= number_format((float) $hdl_val_avg, 1, '.', '') . $delimeter;
                $avg_triglycerides_values_new .= number_format((float) $triglycerides_val_avg, 1, '.', '') . $delimeter;
                $empty_ldl_graph_array[] = (!empty($chovalue[0]['AvgLDL']) ? $chovalue[0]['AvgLDL'] : '');
                $empty_hdl_graph_array[] = (!empty($chovalue[0]['AvgHDL']) ? $chovalue[0]['AvgHDL'] : '');
                $empty_Triglyceride_graph_array[] = (!empty($chovalue[0]['AvgTriglycerides']) ? $chovalue[0]['AvgTriglycerides'] : '');

                $counth_data_cho++;
            }
            /*
             * Blood glucose data $bgdata
             */
            $counth_data_bg = 1;
            foreach ($bgdata as $bgvalue) {
                $cout_data_bg = count($bgdata);

                if ($counth_data_bg == $cout_data_bg) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $measurement_val_avg = (!empty($bgvalue[0]['Avgmeasurement']) ? $bgvalue[0]['Avgmeasurement'] : '0.0');
                $avg_measurement_values_new .= number_format((float) $measurement_val_avg, 1, '.', '') . $delimeter;
                $empty_glucose_graph_array[] = (!empty($bgvalue[0]['Avgmeasurement']) ? $bgvalue[0]['Avgmeasurement'] : '');
                $counth_data_bg++;
            }


            /*
             * Temperature data $bgdata
             */
            $counth_data_temp = 1;
            foreach ($tempdata as $temprvalue) {
                $cout_data_temp = count($tempdata);

                if ($counth_data_temp == $cout_data_temp) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $temperature_val_avg = (!empty($temprvalue[0]['Avgtemperature']) ? $temprvalue[0]['Avgtemperature'] : '0.0');
                $avg_temperature_values_new .= number_format((float) $temperature_val_avg, 1, '.', '') . $delimeter;
                $empty_temperature_graph_array[] = (!empty($temprvalue[0]['Avgtemperature']) ? $temprvalue[0]['Avgtemperature'] : '');
                $counth_data_temp++;
            }
            /*
             * Sleep record monthly 
             * 
             */
            $counth_data_sleep = 1;
            foreach ($sleepdata as $sleepvalue) {
                $cout_data_sleep = count($sleepdata);

                if ($counth_data_sleep == $cout_data_sleep) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $awake_val_avg = (!empty($sleepvalue[0]['Avgawake']) ? $sleepvalue[0]['Avgawake'] : '0.0');
                $deep_val_avg = (!empty($sleepvalue[0]['Avgdeep']) ? $sleepvalue[0]['Avgdeep'] : '0.0');
                $light_val_avg = (!empty($sleepvalue[0]['Avglight']) ? $sleepvalue[0]['Avglight'] : '0.0');
                $rem_val_avg = (!empty($sleepvalue[0]['Avgrem']) ? $sleepvalue[0]['Avgrem'] : '0.0');
                $times_woken_val_avg = (!empty($sleepvalue[0]['Avgtimes_woken']) ? $sleepvalue[0]['Avgtimes_woken'] : '0.0');
                $total_sleep_val_avg = (!empty($sleepvalue[0]['Avgtotal_sleep']) ? $sleepvalue[0]['Avgtotal_sleep'] : '0.0');

                $avg_awake_values_new .= number_format((float) $awake_val_avg, 1, '.', '') . $delimeter;
                $avg_deep_values_new .= number_format((float) $deep_val_avg, 1, '.', '') . $delimeter;
                $avg_light_values_new .= number_format((float) $light_val_avg, 1, '.', '') . $delimeter;
                $avg_rem_values_new .= number_format((float) $rem_val_avg, 1, '.', '') . $delimeter;
                $avg_times_woken_values_new .= number_format((float) $times_woken_val_avg, 1, '.', '') . $delimeter;
                $avg_total_sleep_values_new .= number_format((float) $total_sleep_val_avg, 1, '.', '') . $delimeter;
                $empty_awake_graph_array[] = (!empty($sleepvalue[0]['Avgawake']) ? $sleepvalue[0]['Avgawake'] : '');
                $empty_deep_graph_array[] = (!empty($sleepvalue[0]['Avgdeep']) ? $sleepvalue[0]['Avgdeep'] : '');
                $empty_light_graph_array[] = (!empty($sleepvalue[0]['Avglight']) ? $sleepvalue[0]['Avglight'] : '');
                $empty_rem_graph_array[] = (!empty($sleepvalue[0]['Avgrem']) ? $sleepvalue[0]['Avgrem'] : '');
                $empty_total_sleep_graph_array[] = (!empty($sleepvalue[0]['Avgtotal_sleep']) ? $sleepvalue[0]['Avgtotal_sleep'] : '');
                $counth_data_sleep++;
            }
            /*
             * Mood graph record
             */
            $counth_data_mood = 1;
            foreach ($mooddata as $movalue) {
                $cout_data_mood = count($mooddata);

                if ($counth_data_mood == $cout_data_mood) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }

                $general_mood_val_avg .= "[" . (!empty($movalue[0]['AvgGMV']) ? number_format((float) $movalue[0]['AvgGMV'], 1, '.', '') : '-1.00') . ", 20]" . $delimeter;
                $physical_mood_val_avg .= "[" . (!empty($movalue[0]['AvgPMV']) ? number_format((float) $movalue[0]['AvgPMV'], 1, '.', '') : '-1.00') . ", 30]" . $delimeter;
                $mental_mood_val_avg .= "[" . (!empty($movalue[0]['AvgMMV']) ? number_format((float) $movalue[0]['AvgMMV'], 1, '.', '') : '-1.00') . ", 40]" . $delimeter;

                $empty_general_graph_array[] = (!empty($movalue[0]['AvgGMV']) ? $movalue[0]['AvgGMV'] : '');
                $empty_physical_graph_array[] = (!empty($movalue[0]['AvgPMV']) ? $movalue[0]['AvgPMV'] : '');
                $empty_mental_graph_array[] = (!empty($movalue[0]['AvgMMV']) ? $movalue[0]['AvgMMV'] : '');
                $counth_data_mood++;
            }

            // assign data to veriables
            $xaxisData = $graph_month_list;

            $avg_values_weight = $weight_val_avg_new;
            $avg_values_height = $height_val_avg_new;
            $avg_values_bmi = $bmi_val_avg_new;
            $avg_values_systolic = $systolic_val_avg_new;
            $avg_values_diastolic = $diastolic_val_avg_new;
            $avg_values_puls = $puls_val_avg_new;
            $avg_values_duration = $duration_val_avg_new;
            $avg_values_distance = $avg_distance_values_new;
            $avg_values_steps = $steps_val_avg_new;
            $avg_values_cb = $cb_val_avg_new;
            $avg_values_ldl = $avg_ldl_values_new;
            $avg_values_hdl = $avg_hdl_values_new;
            $avg_values_triglycerides = $avg_triglycerides_values_new;
            $avg_values_measurement = $avg_measurement_values_new;
            $avg_values_temperature = $avg_temperature_values_new;
            $avg_values_awake_sleep = $avg_awake_values_new;
            $avg_values_deep_sleep = $avg_deep_values_new;
            $avg_values_light_sleep = $avg_light_values_new;
            $avg_values_rem_sleep = $avg_rem_values_new;
            $avg_values_total_sleep = $avg_total_sleep_values_new;
            $total_steps_sum = $TotalStep_val;

            $avg_values_general = $general_mood_val_avg;
            $avg_values_physical = $physical_mood_val_avg;
            $avg_values_mental = $mental_mood_val_avg;
        }
        //echo 'ldl'.$avg_values_ldl;exit;

        if ($filter_select == 'oneweek' || $filter_select == 'twoweek' || $filter_select == 'fourweek') {

            $time = new \DateTime('now');
            if ($filter_select == 'oneweek') {
                $duration_value_start = '-1 week';
                $duration_value_end = '1 week';
            }
            if ($filter_select == 'twoweek') {
                $duration_value_start = '-2 week';
                $duration_value_end = '2 week';
            }
            if ($filter_select == 'fourweek') {
                $duration_value_start = '-4 week';
                $duration_value_end = '4 week';
            }
            //calculate date to get data 
            $start_dates = $time->modify($duration_value_start)->format('Y-m-d');
            $end_dates = $time->modify($duration_value_end)->format('Y-m-d');
            $week_array = $CusConPlug->date_difference($start_dates, $end_dates);
            $week_array_list = $CusConPlug->datelist($start_dates, $end_dates);
            if ($filter_select == 'oneweek') {
                $week_array_list = $CusConPlug->daylist($start_dates, $end_dates);
            }

            // set arry of week day alter nate day
            if ($filter_select == 'twoweek') {
                $week_array = $CusConPlug->date_difference_alternate($start_dates, $end_dates);
                $week_array_list = $CusConPlug->daylist_alternate($start_dates, $end_dates);
            }
            // set arry of week day alter nate day
            if ($filter_select == 'fourweek') {
                $week_array = $CusConPlug->date_difference_alternate($start_dates, $end_dates);
                $week_array_list = $CusConPlug->datelist_alternate($start_dates, $end_dates);
            }

            $graph_day_list = $CusConPlug->graphmontlist($week_array_list);

            // get one by one month daata 
            $i = 0;
            $count_loop_data = 1;
            foreach ($week_array as $wvalue) {
                $count_total_week = count($week_array);

                $start_date_week = date('Y-m-d', strtotime($wvalue));
                $end_date_week = date('Y-m-d', strtotime($wvalue));
                if ($count_total_week > $count_loop_data && ($filter_select == 'fourweek' || $filter_select == 'twoweek')) {
                    $date = new \DateTime($start_date_week);
                    $end_date_week = $date->modify('+1 day')->format('Y-m-d');
                }
                $whereWeekArrayDayweightheight = array('UserID' => $userId, 'start_date' => $start_date_week, 'end_date' => $end_date_week, 'source' => $bodyweightSourceName);
                $whereWeekArrayDaybmi = array('UserID' => $userId, 'start_date' => $start_date_week, 'end_date' => $end_date_week, 'source' => $bodyweightSourceName);
                $whereWeekArrayDaybp = array('UserID' => $userId, 'start_date' => $start_date_week, 'end_date' => $end_date_week, 'source' => $PressureSourceName); //bp
                $whereWeekArrayDayphydata = array('UserID' => $userId, 'start_date' => $start_date_week, 'end_date' => $end_date_week, 'source' => $physicalactivitySourceName); //physical data
                $whereWeekArrayDayphydatastep = array('UserID' => $userId, 'start_date' => $start_date_week, 'end_date' => $end_date_week, 'source' => $stepSourceName); //steps
                $whereWeekArrayDaychodata = array('UserID' => $userId, 'start_date' => $start_date_week, 'end_date' => $end_date_week, 'source' => $CholesterolSourceName); //cholestrol
                $whereWeekArrayDaybgdata = array('UserID' => $userId, 'start_date' => $start_date_week, 'end_date' => $end_date_week, 'source' => $bloodglucoseSourceName); //bloodglucose
                $whereWeekArrayDaytempdata = array('UserID' => $userId, 'start_date' => $start_date_week, 'end_date' => $end_date_week, 'source' => $temperatureSourceName); //tempdata
                $whereWeekArrayDaysleepdata = array('UserID' => $userId, 'start_date' => $start_date_week, 'end_date' => $end_date_week, 'source' => $sleepSourceName); //sleepdata
                $whereWeekArrayDaymooddata = array('UserID' => $userId, 'start_date' => $start_date_week, 'end_date' => $end_date_week); //mooddata
//                $whereWeekArrayDay = array(
//                    'UserID' => $userId,
//                    'start_date' => $start_date_week,
//                    'end_date' => $end_date_week
//                );

                $week_weightdata[$i] = $IndWeightTableObj->getWeightRecordByYear($whereWeekArrayDayweightheight);
                $week_heightdata[$i] = $IndHeightTableObj->getHeightRecordByYear($whereWeekArrayDayweightheight);
                $week_bmidata[$i] = $IndBmiTableObj->getBmiRecordByYear($whereWeekArrayDaybmi);
                $week_bpdata[$i] = $IndBpTableObj->getBpRecordByYear($whereWeekArrayDaybp);
                $week_phydata[$i] = $IndPhyTableObj->getPhyAcRecordByYear($whereWeekArrayDayphydata);
                $week_phydatastep[$i] = $IndPhyTableObj->getPhyAcRecordByYearSteps($whereWeekArrayDayphydatastep);
                $week_chodata[$i] = $IndChoTableObj->getCholestrolRecordByYear($whereWeekArrayDaychodata);
                $week_bgdata[$i] = $IndBgTableObj->getBgRecordByYear($whereWeekArrayDaybgdata);
                $week_tempdata[$i] = $TempTableObj->gettempRecordByYear($whereWeekArrayDaytempdata);
                $week_sleepdata[$i] = $SleepTableObj->getSleepRecordByYear($whereWeekArrayDaysleepdata);
                $week_mooddata[$i] = $IndMoodTableObj->getmoodRecordByYear($whereWeekArrayDaymooddata);
                $i++;
                $count_loop_data++;
            }

            // get data from loop for week weight
            $counth_weight_week = 1;
            foreach ($week_weightdata as $weWkvalue) {
                $count_weight_week = count($week_weightdata);

                if ($counth_weight_week == $count_weight_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $weight_val_avg_week = (!empty($weWkvalue[0]['Avgweight']) ? $weWkvalue[0]['Avgweight'] : '0.0');
                $weight_val_avg_new_week .= number_format((float) $CusConPlug->gramToPound($weight_val_avg_week), 1, '.', '') . $delimeter;
                $empty_weight_graph_array[] = (!empty($weWkvalue[0]['Avgweight']) ? $weWkvalue[0]['Avgweight'] : '');
                $counth_weight_week++;
            }

            // get data from loop for week height
            $counth_height_week = 1;
            foreach ($week_heightdata as $heWkvalue) {
                $count_height_week = count($week_heightdata);

                if ($counth_height_week == $count_height_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $height_val_avg_week = (!empty($heWkvalue[0]['Avgheight']) ? $heWkvalue[0]['Avgheight'] : '0.0');
                $height_val_avg_new_week .= number_format((float) $CusConPlug->centimetersToIncheswd($height_val_avg_week), 1, '.', '') . $delimeter;
                $empty_height_graph_array[] = (!empty($heWkvalue[0]['Avgheight']) ? $heWkvalue[0]['Avgheight'] : '');
                $counth_height_week++;
            }


            // get data from loop for week bmi
            $counth_bmi_week = 1;
            foreach ($week_bmidata as $bmWkvalue) {
                $count_bmi_week = count($week_bmidata);

                if ($counth_bmi_week == $count_bmi_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $bmi_val_avg_week = (!empty($bmWkvalue[0]['Avgbmi']) ? $bmWkvalue[0]['Avgbmi'] : '0.0');
                $bmi_val_avg_new_week .= number_format((float) $bmi_val_avg_week, 1, '.', '') . $delimeter;
                $empty_bmi_graph_array[] = (!empty($bmWkvalue[0]['Avgbmi']) ? $bmWkvalue[0]['Avgbmi'] : '');
                $counth_bmi_week++;
            }
            /*
             * Blood pressure
             */
            // get data from loop 
            $counth_data_week = 1;
            foreach ($week_bpdata as $wevalue) {
                $cout_data_week = count($week_bpdata);

                if ($counth_data_week == $cout_data_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $systolic_val_avg_week = (!empty($wevalue[0]['Avgsystolic']) ? $wevalue[0]['Avgsystolic'] : '0.0');
                $diastolic_val_avg_week = (!empty($wevalue[0]['Avgdiastolic']) ? $wevalue[0]['Avgdiastolic'] : '0.0');
                $puls_val_avg_week = (!empty($wevalue[0]['Avgpulse']) ? $wevalue[0]['Avgpulse'] : '0.0');

                $systolic_val_avg_new_week .= number_format((float) $systolic_val_avg_week, 1, '.', '') . $delimeter;
                $diastolic_val_avg_new_week .= number_format((float) $diastolic_val_avg_week, 1, '.', '') . $delimeter;
                $puls_val_avg_new_week .= number_format((float) $puls_val_avg_week, 1, '.', '') . $delimeter;
                $empty_systoli_graph_array[] = (!empty($wevalue[0]['Avgsystolic']) ? $wevalue[0]['Avgsystolic'] : '');
                $empty_diastolic_graph_array[] = (!empty($wevalue[0]['Avgdiastolic']) ? $wevalue[0]['Avgdiastolic'] : '');
                $empty_pulse_graph_array[] = (!empty($wevalue[0]['Avgpulse']) ? $wevalue[0]['Avgpulse'] : '');
                $counth_data_week++;
            }

            /*
             * Physical activity
             */
            $counth_data_phy_week = 1;
            foreach ($week_phydata as $svalue) {
                $cout_data_phy_week = count($week_phydata);

                if ($counth_data_phy_week == $cout_data_phy_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $duration_val_avg = (!empty($svalue[0]['AvgDuration']) ? $svalue[0]['AvgDuration'] : '0.0');
                $distance_val_avg = (!empty($svalue[0]['AvgDistance']) ? $svalue[0]['AvgDistance'] : '0.0');
                $cb_val_avg = (!empty($svalue[0]['AvgCB']) ? $svalue[0]['AvgCB'] : '0.0');

                $duration_val_avg_week .= round($duration_val_avg) . $delimeter;
                $avg_distance_values_week .= number_format((float) $CusConPlug->MToKm($distance_val_avg), 1, '.', '') . $delimeter;
                $cb_val_avg_week .= number_format((float) $cb_val_avg, 1, '.', '') . $delimeter;

                $empty_distance_graph_array[] = (!empty($svalue[0]['AvgDistance']) ? $svalue[0]['AvgDistance'] : '');
                $empty_duration_graph_array[] = (!empty($svalue[0]['AvgDuration']) ? $svalue[0]['AvgDuration'] : '');
                $empty_calories_graph_array[] = (!empty($svalue[0]['AvgCB']) ? $svalue[0]['AvgCB'] : '');
                $counth_data_phy_week++;
            }

            // get data from loop 
            $counth_data_step_week = 1;
            foreach ($week_phydatastep as $stvalue) {
                $cout_data = count($week_phydatastep);

                if ($counth_data_step_week == $cout_data) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $steps_val_avg = (!empty($stvalue[0]['TotalStep']) ? $stvalue[0]['TotalStep'] : '0.0');

                $steps_val_avg_week .= round($steps_val_avg) . $delimeter;
                $empty_steps_graph_array[] = (!empty($stvalue[0]['TotalStep']) ? $stvalue[0]['TotalStep'] : '');
                $TotalStep_val_week += $stvalue[0]['TotalStep'];
                $counth_data_step_week++;
            }
            /*
             * Cholesterol graph data 
             */

            $counth_data_cho_week = 1;
            foreach ($week_chodata as $wevalue) {
                $cout_data_cho_week = count($week_chodata);

                if ($counth_data_cho_week == $cout_data_cho_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $ldl_val_avg_week = (!empty($wevalue[0]['AvgLDL']) ? $wevalue[0]['AvgLDL'] : '0.0');
                $hdl_val_avg_week = (!empty($wevalue[0]['AvgHDL']) ? $wevalue[0]['AvgHDL'] : '0.0');
                $triglycerides_val_avg_week = (!empty($wevalue[0]['AvgTriglycerides']) ? $wevalue[0]['AvgTriglycerides'] : '0.0');
                $avg_ldl_values_week .= number_format((float) $ldl_val_avg_week, 1, '.', '') . $delimeter;
                $avg_hdl_values_week .= number_format((float) $hdl_val_avg_week, 1, '.', '') . $delimeter;
                $avg_triglycerides_values_week .= number_format((float) $triglycerides_val_avg_week, 1, '.', '') . $delimeter;
                $empty_ldl_graph_array[] = (!empty($wevalue[0]['AvgLDL']) ? $wevalue[0]['AvgLDL'] : '');
                $empty_hdl_graph_array[] = (!empty($wevalue[0]['AvgHDL']) ? $wevalue[0]['AvgHDL'] : '');
                $empty_Triglyceride_graph_array[] = (!empty($wevalue[0]['AvgTriglycerides']) ? $wevalue[0]['AvgTriglycerides'] : '');
                $counth_data_cho_week++;
            }

            /*
             * Blood glucose data $bgdata
             */
            $counth_data_bg_week = 1;
            foreach ($week_bgdata as $bgweekvalue) {
                $cout_data_bg_week = count($week_bgdata);

                if ($counth_data_bg_week == $cout_data_bg_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $measurement_val_avg_week = (!empty($bgweekvalue[0]['Avgmeasurement']) ? $bgweekvalue[0]['Avgmeasurement'] : '0.0');
                $avg_measurement_values_week .= number_format((float) $measurement_val_avg_week, 1, '.', '') . $delimeter;
                $empty_glucose_graph_array[] = (!empty($bgweekvalue[0]['Avgmeasurement']) ? $bgweekvalue[0]['Avgmeasurement'] : '');
                $counth_data_bg_week++;
            }

            /*
             * Temperature data week
             */
            $counth_data_temp_week = 1;
            foreach ($week_tempdata as $temweekprvalue) {
                $cout_data_temp_week = count($week_tempdata);

                if ($counth_data_temp_week == $cout_data_temp_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $temperature_val_avg_week = (!empty($temweekprvalue[0]['Avgtemperature']) ? $temweekprvalue[0]['Avgtemperature'] : '0.0');
                $avg_temperature_values_week .= number_format((float) $temperature_val_avg_week, 1, '.', '') . $delimeter;
                $empty_temperature_graph_array[] = (!empty($temweekprvalue[0]['Avgtemperature']) ? $temweekprvalue[0]['Avgtemperature'] : '');
                $counth_data_temp_week++;
            }


            /*
             * Sleep record weekly  
             * 
             */
            $counth_data_sleep_week = 1;
            foreach ($week_sleepdata as $sleep_weekvalue) {
                $cout_data_sleep = count($week_sleepdata);

                if ($counth_data_sleep_week == $cout_data_sleep) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $awake_val_avg_week = (!empty($sleep_weekvalue[0]['Avgawake']) ? $sleep_weekvalue[0]['Avgawake'] : '0.0');
                $deep_val_avg_week = (!empty($sleep_weekvalue[0]['Avgdeep']) ? $sleep_weekvalue[0]['Avgdeep'] : '0.0');
                $light_val_avg_week = (!empty($sleep_weekvalue[0]['Avglight']) ? $sleep_weekvalue[0]['Avglight'] : '0.0');
                $rem_val_avg_week = (!empty($sleep_weekvalue[0]['Avgrem']) ? $sleep_weekvalue[0]['Avgrem'] : '0.0');
                $times_woken_val_avg_week = (!empty($sleep_weekvalue[0]['Avgtimes_woken']) ? $sleep_weekvalue[0]['Avgtimes_woken'] : '0.0');
                $total_sleep_val_avg_week = (!empty($sleep_weekvalue[0]['Avgtotal_sleep']) ? $sleep_weekvalue[0]['Avgtotal_sleep'] : '0.0');

                $avg_awake_values_week .= number_format((float) $awake_val_avg_week, 1, '.', '') . $delimeter;
                $avg_deep_values_week .= number_format((float) $deep_val_avg_week, 1, '.', '') . $delimeter;
                $avg_light_values_week .= number_format((float) $light_val_avg_week, 1, '.', '') . $delimeter;
                $avg_rem_values_week .= number_format((float) $rem_val_avg_week, 1, '.', '') . $delimeter;
                $avg_times_woken_values_week .= number_format((float) $times_woken_val_avg_week, 1, '.', '') . $delimeter;
                $avg_total_sleep_values_week .= number_format((float) $total_sleep_val_avg_week, 1, '.', '') . $delimeter;

                $empty_awake_graph_array[] = (!empty($sleep_weekvalue[0]['Avgawake']) ? $sleep_weekvalue[0]['Avgawake'] : '');
                $empty_deep_graph_array[] = (!empty($sleep_weekvalue[0]['Avgdeep']) ? $sleep_weekvalue[0]['Avgdeep'] : '');
                $empty_light_graph_array[] = (!empty($sleep_weekvalue[0]['Avglight']) ? $sleep_weekvalue[0]['Avglight'] : '');
                $empty_rem_graph_array[] = (!empty($sleep_weekvalue[0]['Avgrem']) ? $sleep_weekvalue[0]['Avgrem'] : '');
                $empty_total_sleep_graph_array[] = (!empty($sleep_weekvalue[0]['Avgtotal_sleep']) ? $sleep_weekvalue[0]['Avgtotal_sleep'] : '');
                $counth_data_sleep_week++;
            }

            /*
             * Mood graph weekly
             */

            $counth_data_mo_week = 1;
            foreach ($week_mooddata as $moweekvalue) {
                $cout_data_mo_week = count($week_mooddata);

                if ($counth_data_mo_week == $cout_data_mo_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }

                $general_val_avg_week .= "[" . (!empty($moweekvalue[0]['AvgGMV']) ? number_format((float) $moweekvalue[0]['AvgGMV'], 1, '.', '') : '-1.00') . ", 60]" . $delimeter;
                $physical_val_avg_week .= "[" . (!empty($moweekvalue[0]['AvgPMV']) ? number_format((float) $moweekvalue[0]['AvgPMV'], 1, '.', '') : '-1.00') . ", 30]" . $delimeter;
                $mental_val_avg_week .= "[" . (!empty($moweekvalue[0]['AvgMMV']) ? number_format((float) $moweekvalue[0]['AvgMMV'], 1, '.', '') : '-1.00') . ", 20]" . $delimeter;

                $empty_general_graph_array[] = (!empty($moweekvalue[0]['AvgGMV']) ? $moweekvalue[0]['AvgGMV'] : '');
                $empty_physical_graph_array[] = (!empty($moweekvalue[0]['AvgPMV']) ? $moweekvalue[0]['AvgPMV'] : '');
                $empty_mental_graph_array[] = (!empty($moweekvalue[0]['AvgMMV']) ? $moweekvalue[0]['AvgMMV'] : '');
                $counth_data_mo_week++;
            }

            // assign data to veriables
            $xaxisData = $graph_day_list;       //category in graph    

            $avg_values_weight = $weight_val_avg_new_week;
            $avg_values_height = $height_val_avg_new_week;
            $avg_values_bmi = $bmi_val_avg_new_week;
            $avg_values_systolic = $systolic_val_avg_new_week;
            $avg_values_diastolic = $diastolic_val_avg_new_week;
            $avg_values_puls = $puls_val_avg_new_week;
            $avg_values_duration = $duration_val_avg_week;
            $avg_values_distance = $avg_distance_values_week;
            $avg_values_steps = $steps_val_avg_week;
            $total_steps_sum = $TotalStep_val_week;
            $avg_values_cb = $cb_val_avg_week;
            $avg_values_ldl = $avg_ldl_values_week;
            $avg_values_hdl = $avg_hdl_values_week;
            $avg_values_triglycerides = $avg_triglycerides_values_week;
            $avg_values_measurement = $avg_measurement_values_week;
            $avg_values_temperature = $avg_temperature_values_week;

            $avg_values_awake_sleep = $avg_awake_values_week;
            $avg_values_deep_sleep = $avg_deep_values_week;
            $avg_values_light_sleep = $avg_light_values_week;
            $avg_values_rem_sleep = $avg_rem_values_week;
            $avg_values_total_sleep = $avg_total_sleep_values_week;

            $avg_values_general = $general_val_avg_week;
            $avg_values_physical = $physical_val_avg_week;
            $avg_values_mental = $mental_val_avg_week;
        }


        /*
         * Hourly graph data 
         */
        if ($filter_select == 'oneday') {
            //calculate time to get data 
            // $time_zone = $CusConPlug->getTimezone();
            $today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0, 0);
            $today->format('Y-m-d H:i:s');

            //Get UTC midnight time
            $today->setTimezone(new \DateTimeZone("UTC"));
            $start = $today->format('Y-m-d H:i:s');
            $week_array = $CusConPlug->time_difference($start);

            $charttype = 'line';

            // Get one by one hour data 
            $i = 0;
            $count_loop_data = 1;
            foreach ($week_array as $wvalue) {
                $date = date('Y-m-d', strtotime($wvalue));
                $start_time = date('H:i:s', strtotime($wvalue));
                $end_time = date('H:i:s', strtotime($wvalue . ' +1 hour'));

                $whereWeekArrayDayweightheight = array('UserID' => $userId, 'current_date' => $date, 'start_time' => $start_time, 'end_time' => $end_time, 'source' => $bodyweightSourceName);
                $whereWeekArrayDaybmi = array('UserID' => $userId, 'current_date' => $date, 'start_time' => $start_time, 'end_time' => $end_time, 'source' => $bodyweightSourceName);
                $whereWeekArrayDaybp = array('UserID' => $userId, 'current_date' => $date, 'start_time' => $start_time, 'end_time' => $end_time, 'source' => $PressureSourceName); //bp
                $whereWeekArrayDayphydata = array('UserID' => $userId, 'current_date' => $date, 'start_time' => $start_time, 'end_time' => $end_time, 'source' => $physicalactivitySourceName); //physical data
                $whereWeekArrayDayphydatastep = array('UserID' => $userId, 'current_date' => $date, 'start_time' => $start_time, 'end_time' => $end_time, 'source' => $stepSourceName); //steps
                $whereWeekArrayDaychodata = array('UserID' => $userId, 'current_date' => $date, 'start_time' => $start_time, 'end_time' => $end_time, 'source' => $CholesterolSourceName); //cholestrol
                $whereWeekArrayDaybgdata = array('UserID' => $userId, 'current_date' => $date, 'start_time' => $start_time, 'end_time' => $end_time, 'source' => $bloodglucoseSourceName); //bloodglucose
                $whereWeekArrayDaytempdata = array('UserID' => $userId, 'current_date' => $date, 'start_time' => $start_time, 'end_time' => $end_time, 'source' => $temperatureSourceName); //tempdata
                $whereWeekArrayDaysleepdata = array('UserID' => $userId, 'current_date' => $date, 'start_time' => $start_time, 'end_time' => $end_time, 'source' => $sleepSourceName); //sleepdata
                $whereWeekArrayDaymooddata = array('UserID' => $userId, 'current_date' => $date, 'start_time' => $start_time, 'end_time' => $end_time); //mooddata
//                $whereWeekArrayDay = array(
//                    'UserID' => $userId,
//                    'current_date' => $date,
//                    'start_time' => $start_time,
//                    'end_time' => $end_time
//                );
                $week_weightdata[$i] = $IndWeightTableObj->getWeightRecordOfDay($whereWeekArrayDayweightheight);
                $week_heightdata[$i] = $IndHeightTableObj->getHeightRecordOfDay($whereWeekArrayDayweightheight);
                $week_bmidata[$i] = $IndBmiTableObj->getBmiRecordOfDay($whereWeekArrayDaybmi);
                $week_bpdata[$i] = $IndBpTableObj->getBpRecordOfDay($whereWeekArrayDaybp);
                $week_phydata[$i] = $IndPhyTableObj->getPhyAcRecordOfDay($whereWeekArrayDayphydata);
                $week_phydatastep[$i] = $IndPhyTableObj->getPhyAcRecordOfDay($whereWeekArrayDayphydatastep);
                $week_chodata[$i] = $IndChoTableObj->getCholestrolRecordOfDay($whereWeekArrayDaychodata);
                $week_bgdata[$i] = $IndBgTableObj->getBgRecordOfDay($whereWeekArrayDaybgdata);
                $week_tempdata[$i] = $TempTableObj->gettempRecordOfDay($whereWeekArrayDaytempdata);
                $week_sleepdata[$i] = $SleepTableObj->getSleepRecordOfDay($whereWeekArrayDaysleepdata);
                $week_mooddata[$i] = $IndMoodTableObj->getmoodRecordOfDay($whereWeekArrayDaymooddata);

                $i++;
                $count_loop_data++;
            }

            // get weight data for day
            $counth_weight_week = 1;
            foreach ($week_weightdata as $weWkvalue) {
                $count_weight_week = count($week_weightdata);

                if ($counth_weight_week == $count_weight_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $weight_val_avg_week = (!empty($weWkvalue[0]['Avgweight']) ? $weWkvalue[0]['Avgweight'] : '0.0');
                $weight_val_avg_new_week .= number_format((float) $CusConPlug->gramToPound($weight_val_avg_week), 1, '.', '') . $delimeter;
                $empty_weight_graph_array[] = (!empty($weWkvalue[0]['Avgweight']) ? $weWkvalue[0]['Avgweight'] : '');
                $counth_weight_week++;
            }

            // get height data for day
            $counth_height_week = 1;
            foreach ($week_heightdata as $heWkvalue) {
                $count_height_week = count($week_heightdata);

                if ($counth_height_week == $count_height_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $height_val_avg_week = (!empty($heWkvalue[0]['Avgheight']) ? $heWkvalue[0]['Avgheight'] : '0.0');
                $height_val_avg_new_week .= number_format((float) $CusConPlug->centimetersToInches($height_val_avg_week), 1, '.', '') . $delimeter;
                $empty_height_graph_array[] = (!empty($heWkvalue[0]['Avgheight']) ? $heWkvalue[0]['Avgheight'] : '');
                $counth_height_week++;
            }

            // get bmi data for day
            $counth_bmi_week = 1;
            foreach ($week_bmidata as $bmWkvalue) {
                $count_bmi_week = count($week_bmidata);

                if ($counth_bmi_week == $count_bmi_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $bmi_val_avg_week = (!empty($bmWkvalue[0]['Avgbmi']) ? $bmWkvalue[0]['Avgbmi'] : '0.0');
                $bmi_val_avg_new_week .= number_format((float) $bmi_val_avg_week, 1, '.', '') . $delimeter;
                $empty_bmi_graph_array[] = (!empty($bmWkvalue[0]['Avgbmi']) ? $bmWkvalue[0]['Avgbmi'] : '');
                $counth_bmi_week++;
            }

            /*
             * Blood pressure
             */
            // get data from loop 
            $counth_data_week = 1;
            foreach ($week_bpdata as $wevalue) {
                $cout_data_week = count($week_bpdata);

                if ($counth_data_week == $cout_data_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $systolic_val_avg_week = (!empty($wevalue[0]['Avgsystolic']) ? $wevalue[0]['Avgsystolic'] : '0.0');
                $diastolic_val_avg_week = (!empty($wevalue[0]['Avgdiastolic']) ? $wevalue[0]['Avgdiastolic'] : '0.0');
                $puls_val_avg_week = (!empty($wevalue[0]['Avgpulse']) ? $wevalue[0]['Avgpulse'] : '0.0');

                $systolic_val_avg_new_week .= number_format((float) $systolic_val_avg_week, 1, '.', '') . $delimeter;
                $diastolic_val_avg_new_week .= number_format((float) $diastolic_val_avg_week, 1, '.', '') . $delimeter;
                $puls_val_avg_new_week .= number_format((float) $puls_val_avg_week, 1, '.', '') . $delimeter;
                $empty_systoli_graph_array[] = (!empty($wevalue[0]['Avgsystolic']) ? $wevalue[0]['Avgsystolic'] : '');
                $empty_diastolic_graph_array[] = (!empty($wevalue[0]['Avgdiastolic']) ? $wevalue[0]['Avgdiastolic'] : '');
                $empty_pulse_graph_array[] = (!empty($wevalue[0]['Avgpulse']) ? $wevalue[0]['Avgpulse'] : '');
                $counth_data_week++;
            }

            /*
             * Physical activity
             */
            $counth_data_phy_week = 1;
            foreach ($week_phydata as $svalue) {
                $cout_data_phy_week = count($week_phydata);

                if ($counth_data_phy_week == $cout_data_phy_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $duration_val_avg = (!empty($svalue[0]['AvgDuration']) ? $svalue[0]['AvgDuration'] : '0.0');
                $distance_val_avg = (!empty($svalue[0]['AvgDistance']) ? $svalue[0]['AvgDistance'] : '0.0');
                $cb_val_avg = (!empty($svalue[0]['AvgCB']) ? $svalue[0]['AvgCB'] : '0.0');

                $duration_val_avg_week .= round($duration_val_avg) . $delimeter;
                $avg_distance_values_week .= number_format((float) $CusConPlug->MToKm($distance_val_avg), 1, '.', '') . $delimeter;
                $cb_val_avg_week .= number_format((float) $cb_val_avg, 1, '.', '') . $delimeter;
                $empty_distance_graph_array[] = (!empty($svalue[0]['AvgDistance']) ? $svalue[0]['AvgDistance'] : '');
                $empty_duration_graph_array[] = (!empty($svalue[0]['AvgDuration']) ? $svalue[0]['AvgDuration'] : '');
                $empty_calories_graph_array[] = (!empty($svalue[0]['AvgCB']) ? $svalue[0]['AvgCB'] : '');
                $counth_data_phy_week++;
            }

            // get data from loop 
            $counth_data_step_week = 1;
            foreach ($week_phydatastep as $stvalue) {
                $cout_data = count($week_phydatastep);

                if ($counth_data_step_week == $cout_data) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $steps_val_avg = (!empty($stvalue[0]['TotalStep']) ? $stvalue[0]['TotalStep'] : '0.0');

                $steps_val_avg_week .= round($steps_val_avg) . $delimeter;
                $empty_steps_graph_array[] = (!empty($stvalue[0]['TotalStep']) ? $stvalue[0]['TotalStep'] : '');
                $TotalStep_val_week += $stvalue[0]['TotalStep'];
                $counth_data_step_week++;
            }
            /*
             * Cholesterol graph data 
             */

            $counth_data_cho_week = 1;
            foreach ($week_chodata as $wevalue) {
                $cout_data_cho_week = count($week_chodata);

                if ($counth_data_cho_week == $cout_data_cho_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $ldl_val_avg_week = (!empty($wevalue[0]['AvgLDL']) ? $wevalue[0]['AvgLDL'] : '0.0');
                $hdl_val_avg_week = (!empty($wevalue[0]['AvgHDL']) ? $wevalue[0]['AvgHDL'] : '0.0');
                $triglycerides_val_avg_week = (!empty($wevalue[0]['AvgTriglycerides']) ? $wevalue[0]['AvgTriglycerides'] : '0.0');
                $avg_ldl_values_week .= number_format((float) $ldl_val_avg_week, 1, '.', '') . $delimeter;
                $avg_hdl_values_week .= number_format((float) $hdl_val_avg_week, 1, '.', '') . $delimeter;
                $avg_triglycerides_values_week .= number_format((float) $triglycerides_val_avg_week, 1, '.', '') . $delimeter;
                $empty_ldl_graph_array[] = (!empty($wevalue[0]['AvgLDL']) ? $wevalue[0]['AvgLDL'] : '');
                $empty_hdl_graph_array[] = (!empty($wevalue[0]['AvgHDL']) ? $wevalue[0]['AvgHDL'] : '');
                $empty_Triglyceride_graph_array[] = (!empty($wevalue[0]['AvgTriglycerides']) ? $wevalue[0]['AvgTriglycerides'] : '');
                $counth_data_cho_week++;
            }

            /*
             * Blood glucose data $bgdata
             */
            $counth_data_bg_week = 1;
            foreach ($week_bgdata as $bgweekvalue) {
                $cout_data_bg_week = count($week_bgdata);

                if ($counth_data_bg_week == $cout_data_bg_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $measurement_val_avg_week = (!empty($bgweekvalue[0]['Avgmeasurement']) ? $bgweekvalue[0]['Avgmeasurement'] : '0.0');
                $avg_measurement_values_week .= number_format((float) $measurement_val_avg_week, 1, '.', '') . $delimeter;
                $empty_glucose_graph_array[] = (!empty($bgweekvalue[0]['Avgmeasurement']) ? $bgweekvalue[0]['Avgmeasurement'] : '');
                $counth_data_bg_week++;
            }

            /*
             * Temperature data week
             */
            $counth_data_temp_week = 1;
            foreach ($week_tempdata as $temweekprvalue) {
                $cout_data_temp_week = count($week_tempdata);

                if ($counth_data_temp_week == $cout_data_temp_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $temperature_val_avg_week = (!empty($temweekprvalue[0]['Avgtemperature']) ? $temweekprvalue[0]['Avgtemperature'] : '0.0');
                $avg_temperature_values_week .= number_format((float) $temperature_val_avg_week, 1, '.', '') . $delimeter;
                $empty_temperature_graph_array[] = (!empty($temweekprvalue[0]['Avgtemperature']) ? $temweekprvalue[0]['Avgtemperature'] : '');
                $counth_data_temp_week++;
            }


            /*
             * Sleep record weekly  
             * 
             */
            $counth_data_sleep_week = 1;
            foreach ($week_sleepdata as $sleep_weekvalue) {
                $cout_data_sleep = count($week_sleepdata);

                if ($counth_data_sleep_week == $cout_data_sleep) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $awake_val_avg_week = (!empty($sleep_weekvalue[0]['Avgawake']) ? $sleep_weekvalue[0]['Avgawake'] : '0.0');
                $deep_val_avg_week = (!empty($sleep_weekvalue[0]['Avgdeep']) ? $sleep_weekvalue[0]['Avgdeep'] : '0.0');
                $light_val_avg_week = (!empty($sleep_weekvalue[0]['Avglight']) ? $sleep_weekvalue[0]['Avglight'] : '0.0');
                $rem_val_avg_week = (!empty($sleep_weekvalue[0]['Avgrem']) ? $sleep_weekvalue[0]['Avgrem'] : '0.0');
                $times_woken_val_avg_week = (!empty($sleep_weekvalue[0]['Avgtimes_woken']) ? $sleep_weekvalue[0]['Avgtimes_woken'] : '0.0');
                $total_sleep_val_avg_week = (!empty($sleep_weekvalue[0]['Avgtotal_sleep']) ? $sleep_weekvalue[0]['Avgtotal_sleep'] : '0.0');

                $avg_awake_values_week .= number_format((float) $awake_val_avg_week, 1, '.', '') . $delimeter;
                $avg_deep_values_week .= number_format((float) $deep_val_avg_week, 1, '.', '') . $delimeter;
                $avg_light_values_week .= number_format((float) $light_val_avg_week, 1, '.', '') . $delimeter;
                $avg_rem_values_week .= number_format((float) $rem_val_avg_week, 1, '.', '') . $delimeter;
                $avg_times_woken_values_week .= number_format((float) $times_woken_val_avg_week, 1, '.', '') . $delimeter;
                $avg_total_sleep_values_week .= number_format((float) $total_sleep_val_avg_week, 1, '.', '') . $delimeter;

                $empty_awake_graph_array[] = (!empty($sleep_weekvalue[0]['Avgawake']) ? $sleep_weekvalue[0]['Avgawake'] : '');
                $empty_deep_graph_array[] = (!empty($sleep_weekvalue[0]['Avgdeep']) ? $sleep_weekvalue[0]['Avgdeep'] : '');
                $empty_light_graph_array[] = (!empty($sleep_weekvalue[0]['Avglight']) ? $sleep_weekvalue[0]['Avglight'] : '');
                $empty_rem_graph_array[] = (!empty($sleep_weekvalue[0]['Avgrem']) ? $sleep_weekvalue[0]['Avgrem'] : '');
                $empty_total_sleep_graph_array[] = (!empty($sleep_weekvalue[0]['Avgtotal_sleep']) ? $sleep_weekvalue[0]['Avgtotal_sleep'] : '');
                $counth_data_sleep_week++;
            }

            /*
             * Mood graph daily
             */

            $counth_data_mo_week = 1;
            foreach ($week_mooddata as $moweekvalue) {
                $cout_data_mo_week = count($week_mooddata);

                if ($counth_data_mo_week == $cout_data_mo_week) {
                    $delimeter = '';
                } else {
                    $delimeter = ",";
                }
                $general_val_avg_week .= "[" . (!empty($moweekvalue[0]['AvgGMV']) ? number_format((float) $moweekvalue[0]['AvgGMV'], 1, '.', '') : '-1.00') . ", 60]" . $delimeter;
                $physical_val_avg_week .= "[" . (!empty($moweekvalue[0]['AvgPMV']) ? number_format((float) $moweekvalue[0]['AvgPMV'], 1, '.', '') : '-1.00') . ", 30]" . $delimeter;
                $mental_val_avg_week .= "[" . (!empty($moweekvalue[0]['AvgMMV']) ? number_format((float) $moweekvalue[0]['AvgMMV'], 1, '.', '') : '-1.00') . ", 20]" . $delimeter;

                $empty_general_graph_array[] = (!empty($moweekvalue[0]['AvgGMV']) ? $moweekvalue[0]['AvgGMV'] : '');
                $empty_physical_graph_array[] = (!empty($moweekvalue[0]['AvgPMV']) ? $moweekvalue[0]['AvgPMV'] : '');
                $empty_mental_graph_array[] = (!empty($moweekvalue[0]['AvgMMV']) ? $moweekvalue[0]['AvgMMV'] : '');
                $counth_data_mo_week++;
            }



            $graph_day_list = "'0', '', '','3', '', '', '6', '', '', '9', '', '', '12', '', '', '15', '', '', '18', '', '', '21', '', '', '24'";

            // assign data to veriables
            $xaxisData = $graph_day_list;       //category in graph    

            $avg_values_weight = $weight_val_avg_new_week;
            $avg_values_height = $height_val_avg_new_week;
            $avg_values_bmi = $bmi_val_avg_new_week;

            $avg_values_systolic = $systolic_val_avg_new_week;
            $avg_values_diastolic = $diastolic_val_avg_new_week;
            $avg_values_puls = $puls_val_avg_new_week;
            $avg_values_duration = $duration_val_avg_week;
            $avg_values_distance = $avg_distance_values_week;
            $avg_values_steps = $steps_val_avg_week;
            $total_steps_sum = $TotalStep_val_week;
            $avg_values_cb = $cb_val_avg_week;
            $avg_values_ldl = $avg_ldl_values_week;
            $avg_values_hdl = $avg_hdl_values_week;
            $avg_values_triglycerides = $avg_triglycerides_values_week;
            $avg_values_measurement = $avg_measurement_values_week;
            $avg_values_temperature = $avg_temperature_values_week;

            $avg_values_awake_sleep = $avg_awake_values_week;
            $avg_values_deep_sleep = $avg_deep_values_week;
            $avg_values_light_sleep = $avg_light_values_week;
            $avg_values_rem_sleep = $avg_rem_values_week;
            $avg_values_total_sleep = $avg_total_sleep_values_week;

            $avg_values_general = $general_val_avg_week;
            $avg_values_physical = $physical_val_avg_week;
            $avg_values_mental = $mental_val_avg_week;
        }


//        echo $avg_values_general;
//        exit;

        /*         * ********
         * Action: This action is show moves data 
         * @author: shivendra Suman
         * Created Date: 02-02-2015.
         * *** */
        $today1 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        $today_date = $today1->format('Y-m-d');
        $today2 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        $today_date_time = $today2->format('Y-m-d H:i:s');
        //moves data new implementation
        $today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0, 0);
        $today->format('Y-m-d H:i:s');
        //Get UTC midnight time
        $today->setTimezone(new \DateTimeZone("UTC"));
        $startDate = $today->format('Y-m-d H:i:s');
        $endDate = $today->modify('+24 hours')->format('Y-m-d H:i:s'); //$start + 24;
        $whereMovesArray = array('UserID' => $userId, 'startDate' => $startDate, 'endDate' => $endDate, 'source' => 'movesapp');
        $orderby = "ExerciseRecorddate DESC, ExerciseRecordtime DESC";
        // $whereMovesArray = array('UserID' => $userId, 'ExerciseRecorddate' => $today_date, 'source' => 'movesapp');
        $movesData = $IndPhyTableObj->getPhysicalRecords($whereMovesArray, array(), $orderby);
        //set activity on the basis of color
        $i = 0;
        $coloumn = array('color');
        $user_Symptoms = array();
        foreach ($movesData as $mvalue) {
            $colordata = $IndPhyTableObj->getActivityColor(array('activity' => $mvalue['ActivityType']), $coloumn);
            $colorname = (isset($colordata[0]['color']) ? $colordata[0]['color'] : '');
            if (empty($colorname)) {
                $colorname = '#e1e1e1';
            }
            $movesData[$i]['color'] = $colorname;
            $i++;
        }
        $IndCondiTableObj = $this->getServiceLocator()->get("IndividualConditionTable");
        $IndUserSymtomsTableObj = $this->getServiceLocator()->get("IndividualUsersSymtomsTable");
        $whereSymArr = array("user_id" => $userId, 'symp.status' => 1);
        $user_Symptoms_arr = $IndUserSymtomsTableObj->getUserSymptoms($whereSymArr);

        $finalRes = array();
        if (count($user_Symptoms_arr)) {
            foreach ($user_Symptoms_arr as $val) {
                $result['symptoms_name'] = $val['common_name'];
                $result['media_name'] = $val['media_name'];
                $result['symtoms_id'] = $val['symtoms_id'];
                $result['sub_symptoms_name'] = $val['sub_symptoms_name'];
                $result['description'] = $val['description'];
                $result['SympStatus'] = $val['SympStatus'];
                $result['condition_name'] = '';
                if ($val['condition_ids']) {
                    $res = $IndCondiTableObj->getConditionsList(array('status' => 1, 'id' => explode(',', $val['condition_ids'])), array('common_name'), "0", array());
                    $result['condition_name'] = (isset($res[0]['common_name']) ? $res[0]['common_name'] : '' );
                }

                $user_Symptoms[] = $result;
            }
        }
        $IndUserTreatmentTableObj = $this->getServiceLocator()->get("IndividualUsersTreatmentsTable");
        $user_Treatment = array();
        $whereSymArr = array("user_id" => $userId, 'treat.status' => 1);
        $user_Tret = $IndUserTreatmentTableObj->getUserTreatment($whereSymArr);
        $finalRes = array();
        if (count($user_Tret)) {
            foreach ($user_Tret as $val) {
                $result['teatment_name'] = $val['common_name'];
                $result['treatment_id'] = $val['treatment_id'];
                $result['sub_treatment_name'] = $val['sub_treatment_name'];
                $result['description'] = $val['description'];
                $result['TreatStatus'] = $val['TreatStatus'];
                $result['condition_name'] = '';
                if ($val['condition_ids']) {
                    $res = $IndCondiTableObj->getConditionsList(array('status' => 1, 'id' => explode(',', $val['condition_ids'])), array('common_name'), "0", array());
                    $result['condition_name'] = (isset($res[0]['common_name']) ? $res[0]['common_name'] : '' );
                }

                $user_Treatment[] = $result;
            }
        }
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  Dashboard',
                    'custom_plugin' => $CusConPlug, // custome plugin,
                    'movesData' => $movesData,
                    'filter_select' => $filter_select, // selected filter value 
                    'user_Conditions' => $conditionslist,
                    'user_Symptoms' => $user_Symptoms,
                    'user_Treatment' => $user_Treatment,
                    'xaxisData' => $xaxisData,
                    'yaxisData_height' => ((!empty(array_filter($empty_height_graph_array))) ? $avg_values_height : ""),
                    'yaxisData_weight' => ((!empty(array_filter($empty_weight_graph_array))) ? $avg_values_weight : ""),
                    'yaxisData_bmi' => ((!empty(array_filter($empty_bmi_graph_array))) ? $avg_values_bmi : ""),
                    'yaxisData_systolic' => ((!empty(array_filter($empty_systoli_graph_array))) ? $avg_values_systolic : ""),
                    'yaxisData_diastolic' => ((!empty(array_filter($empty_diastolic_graph_array))) ? $avg_values_diastolic : ""),
                    'yaxisData_puls' => ((!empty(array_filter($empty_pulse_graph_array))) ? $avg_values_puls : ""),
                    'yaxisData_duration' => ((!empty(array_filter($empty_duration_graph_array))) ? $avg_values_duration : ""),
                    'yaxisData_distance' => ((!empty(array_filter($empty_distance_graph_array))) ? $avg_values_distance : ""),
                    'yaxisData_steps' => ((!empty(array_filter($empty_steps_graph_array))) ? $avg_values_steps : ""),
                    'yaxisData_cb' => ((!empty(array_filter($empty_calories_graph_array))) ? $avg_values_cb : ""),
                    'yaxisData_ldl' => ((!empty(array_filter($empty_ldl_graph_array))) ? $avg_values_ldl : ""),
                    'yaxisData_hdl' => ((!empty(array_filter($empty_hdl_graph_array))) ? $avg_values_hdl : ""),
                    'yaxisData_triglycerides' => ((!empty(array_filter($empty_Triglyceride_graph_array))) ? $avg_values_triglycerides : ""),
                    'yaxisData_measurement' => ((!empty(array_filter($empty_glucose_graph_array))) ? $avg_values_measurement : ""),
                    'yaxisData_temp' => ((!empty(array_filter($empty_temperature_graph_array))) ? $avg_values_temperature : ""),
                    'yaxisData_general' => ((!empty(array_filter($empty_general_graph_array))) ? $avg_values_general : ""),
                    'yaxisData_physical' => ((!empty(array_filter($empty_physical_graph_array))) ? $steps_val_avg_new : ""),
                    'yaxisData_mental' => ((!empty(array_filter($empty_mental_graph_array))) ? $avg_values_mental : ""),
                    'yaxisData_awake' => ((!empty(array_filter($empty_awake_graph_array))) ? $steps_val_avg_new : ""),
                    'yaxisData_deep' => ((!empty(array_filter($empty_deep_graph_array))) ? $avg_values_deep_sleep : ""),
                    'yaxisData_light' => ((!empty(array_filter($empty_light_graph_array))) ? $avg_values_light_sleep : ""),
                    'yaxisData_rem' => ((!empty(array_filter($empty_rem_graph_array))) ? $avg_values_rem_sleep : ""),
                    'yaxisData_total_sleep' => ((!empty(array_filter($empty_total_sleep_graph_array))) ? $avg_values_total_sleep : ""),
                    'charttype' => $charttype,
                    'today_date' => $today_date_time,
                    'time_zone' => $time_zone,
                    'sleepSource' => $sleepSource,
        ));
    }

    /*
     * @Author: Shivendr Suman
     * @Date: 26-06-2015
     * return source name as a string
     */

    function _getSourceNameAction($sourceId) {
        $IndDataSourceTableObj = $this->getServiceLocator()->get("DataSource");
        $sourceName = '';
        if (!empty($sourceId)) {
            $wheresourArr = array("id" => $sourceId);
            $coloumn = array("source");
            $sourceArr = $IndDataSourceTableObj->getSource($wheresourArr, $coloumn);
            if (count($sourceArr) > 0) {
                $sourceName = $sourceArr[0]['source'];
            } else {
                $sourceName = '';
            }
        }
        return $sourceName;
    }

}
