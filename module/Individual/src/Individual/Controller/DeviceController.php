<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE Device FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 05/01/2015.
 * UPDATED:  .
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class DeviceController extends AbstractActionController {

    public function __construct() {
        $this->_sessionObj = new Container('frontend');
    }

    public function indexAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $DeviceSourceTableObj = $this->getServiceLocator()->get("DataSource");
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();

        $userId = $this->_sessionObj->userDetails['UserID'];
        $UserName = $this->_sessionObj->userDetails['UserName'];
        $VALIDIC_ACCESS_TOKEN = $VALIDIC_USER_ID = NULL;

        $ConnectedApps = $IndUserTableObj->getConnectedValidicApp(array("UserId" => $userId), array());
        $returnArr = array();
        if ($ConnectedApps) {
            foreach ($ConnectedApps as $conn) {
                $returnArr[] = $conn['source'];
            }
        }


        /*
          @ get user validic id and access token
         */
        $whereArray = array("UserID" => $userId);
        $coloumnarray = array("validic_id", "validic_access_token","human_public_token", "CreationDate",'UserID','Email');
        $user_details = $IndUserTableObj->getUser($whereArray, $coloumnarray);

        foreach ($user_details as $uservalue) {
            $VALIDIC_ACCESS_TOKEN = $uservalue['validic_access_token'];
            $VALIDIC_USER_ID = $uservalue['validic_id'];
            $CREATION_DATE = $uservalue['CreationDate'];
        }

        if (empty($VALIDIC_USER_ID) && empty($VALIDIC_ACCESS_TOKEN)) {
            $validic_result = 0;
            $final = array();
        } else {
            $validic_result = 1;
            $ORGANIZATION_ID = VALIDIC_ORGANIZATION_ID;
            $ORGANIZATION_ACCESS_TOKEN = VALIDIC_ACCESS_TOKEN;
            $human_url = "https://api.humanapi.co/v1/human/sources?access_token";
            $return_url = $server_url . "/individual-device";
            $final = array();
            $resp = array('result' => 'emtpy result');
            $resp = json_decode(file_get_contents($human_url."=".$VALIDIC_ACCESS_TOKEN),true);
           
        }
//       /echo '<pre>'; print_r($final);echo '<pre>'; print_r($final);die;
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  Device',
                    'custom_plugin' => $CusConPlug, // custome plugin
                    'UserName' => $UserName,
                    'validic_url' => 'https://app.validic.com',
                    'connected_apps' => $resp,
                    'user_details' => $user_details[0],
                    'session' => $this->_sessionObj->userDetails,
                    'ConnectedAppArr' => $returnArr,
                    'validic_access_tocken' => $VALIDIC_ACCESS_TOKEN,
                    'validic_id' => $VALIDIC_USER_ID,
                    'CREATION_DATE' => $CREATION_DATE
        ));
    }

    public function indextAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $DeviceSourceTableObj = $this->getServiceLocator()->get("DataSource");
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();

        $userId = $this->_sessionObj->userDetails['UserID'];
        $UserName = $this->_sessionObj->userDetails['UserName'];
        $VALIDIC_ACCESS_TOKEN = $VALIDIC_USER_ID = NULL;

        $ConnectedApps = $IndUserTableObj->getConnectedValidicApp(array("UserId" => $userId), array());
        $returnArr = array();
        if ($ConnectedApps) {
            foreach ($ConnectedApps as $conn) {
                $returnArr[] = $conn['source'];
            }
        }


        /*
          @ get user validic id and access token
         */
        $whereArray = array("UserID" => $userId);
        $coloumnarray = array("validic_id", "validic_access_token", "CreationDate");
        $user_details = $IndUserTableObj->getUser($whereArray, $coloumnarray);

        foreach ($user_details as $uservalue) {
            $VALIDIC_ACCESS_TOKEN = $uservalue['validic_access_token'];
            $VALIDIC_USER_ID = $uservalue['validic_id'];
            $CREATION_DATE = $uservalue['CreationDate'];
        }

        if (empty($VALIDIC_USER_ID) && empty($VALIDIC_ACCESS_TOKEN)) {
            $validic_result = 0;
            $final = array();
        } else {
            $validic_result = 1;
            $ORGANIZATION_ID = VALIDIC_ORGANIZATION_ID;
            $ORGANIZATION_ACCESS_TOKEN = VALIDIC_ACCESS_TOKEN;
            $validic_url = "https://api.validic.com/v1/organizations/";
            $return_url = $server_url . "/individual-device";
            $final = array();
            $resp = array('result' => 'emtpy result');
            $resp = json_decode(file_get_contents("$validic_url$ORGANIZATION_ID/apps.json?authentication_token=$VALIDIC_ACCESS_TOKEN&access_token=$ORGANIZATION_ACCESS_TOKEN&redirect_uri=$return_url"));
            $appsDataArr = isset($resp->apps) ? $resp->apps : array();
            $res = array();
            if (count($appsDataArr)) {
                foreach ($appsDataArr as $value) {
                    $res['name'] = $value->name;
                    $res['sync_url'] = $value->sync_url;
                    $res['unsync_url'] = $value->unsync_url;
                    $res['refresh_url'] = $value->refresh_url;
                    $res['subname'] = $value->subname;
                    $res['logo_url'] = $value->logo_url;
                    $res['kind'] = $value->kind;
                    $res['platform_types'] = $value->platform_types;
                    $final[] = $res;
                    if (!empty($value->subname)) {
                        $whereArr = array("source" => $value->subname);
                        $coloumnArr = array("id");
                        $device = $DeviceSourceTableObj->getSource($whereArr, $coloumnArr); //get device id

                        if (count($device) > 0) {// chek device id is avaliable or not
                            //get connected source app
                            $connectedsource = $IndUserTableObj->getConDevice(array("sourceId" => $device[0]['id'], "UserId" => $userId), array("id"));

                            if (count($connectedsource) <= 0 && !empty($value->refresh_url)) { // check previously saved device or not                            
                                $insDataArr = array("UserId" => $userId, "sourceId" => $device[0]['id'], "creation_date" => round(microtime(true) * 1000));
                                $insdata = $IndUserTableObj->insertDevice($insDataArr); // insert device which is not enrtered
                            }
                        }
                    }
                }
            }
        }
//       /echo '<pre>'; print_r($final);echo '<pre>'; print_r($final);die;
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  Device',
                    'custom_plugin' => $CusConPlug, // custome plugin
                    'UserName' => $UserName,
                    'validic_url' => 'https://app.validic.com',
                    'validic_result' => $validic_result,
                    'validic_device' => $final,
                    'ConnectedAppArr' => $returnArr,
                    'validic_access_tocken' => $VALIDIC_ACCESS_TOKEN,
                    'validic_id' => $VALIDIC_USER_ID,
                    'CREATION_DATE' => $CREATION_DATE
        ));
    }

    public function thankyouAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Thank You'
        ));
    }

    /*
     * @Authore: Shivendra Suman
     * @Date: 14-01-2014
     * @synchronized device 
     */
//    public function synchronizedevicAction(){
//        $request= $this->getRequest();
//        if($request->isPost()){            
//            $postDataArr = $request->getPost()->toArray();
//        }
//            $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
//            $CusConPlug = $this->CustomControllerPlugin();  // custome plugin            
//            $validicUrl = "https://api.validic.com/v1/";
//            $validic_access_token = $postDataArr['validic_access_token'];  
//            $validic_id = $postDataArr['validic_id'];  
//            $UserId = $postDataArr['UserId'];  
//            $start_date = $postDataArr['start_date'];
//            $end_date =   $postDataArr['end_date'];
//            $page = $postDataArr['page'];
//            $officet = $postDataArr['officet'];
//            $limit = $postDataArr['limit'];
//        if(!empty($validic_access_token)){          
//            $profile ="profile.json?authentication_token=$validic_access_token";            
//            $hitUrl = $validicUrl.$profile;
//            $profileJsonData =  $CusConPlug->curlGet($hitUrl);
//            $profileData = json_decode($profileJsonData);
//            echo '<pre>';
//            print_r($profileData);
//            exit;
//            
//        }
//            
//        
//        $viewModel = new ViewModel();
//        return $viewModel->setVariables(array(
//                    'pageTitle' => 'Synchronized Device Data' 
//            ));
//        
//    }
}
