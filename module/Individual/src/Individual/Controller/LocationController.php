<?php

/*
  @ Used: This class is used to manage Imunization
  @ Created By: Rabban Ahmad
  @ Created On: 10/1/15
  @ Updated On: 10/04/15
  @ Zend Framework (http://framework.zend.com/)
 */

namespace Individual\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class LocationController extends \Zend\Mvc\Controller\AbstractActionController {

    public function __construct() {
        
    }

    public function init() {
        $this->_sessionObj = new Container('frontend');
    }

    public function indexAction() {
        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $LocationObj = $this->getServiceLocator()->get("LocationTable");
        $wherArr = array('geo.UserID' => $UserID, 'geo.status' => 1);
        $GeofenceUsers = $LocationObj->getGeofenceRequest($wherArr);
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  Location', 'accept_geofence' => $GeofenceUsers
        ));
    }

    public function requestgeofenceAction() {
        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $error = "";
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $LocationObj = $this->getServiceLocator()->get("LocationTable");
        $wherArr = array('geo.UserID' => $UserID, 'geo.status' => 0);
        $GeofenceReq = $LocationObj->getGeofenceRequest($wherArr);
        /*         * ********Code to update unread notification************** */
        $NotificationTableObj = $this->getServiceLocator()->get("ServicesNotificationTable");
        $whereUpdate = array('receiver_id' => $UserID, 'type' => 5);
        $updateData = array('is_read' => 1);
        $NotificationTableObj->updateNotification($whereUpdate, $updateData);
        /*         * ********End Code to update unread notification************** */
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual Add Geofence',
                    'geofence_request' => $GeofenceReq
        ));
    }

    /*
      @ Thsi aciton is used to manage the Becons
      @ Created : 24/2/15
     */

    public function beconsAction() {
        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $BeconTableObj = $this->getServiceLocator()->get("BeconTableObj");
        $wherArr = array('bec.UserID' => $UserID, 'bec.status' => 1);
        $UserBecons = $BeconTableObj->getBecons($wherArr);
        /* echo "<pre>";
          print_r($UserBecons);die; */
        $CareGiverTableObj = $this->getServiceLocator()->get("IndividualCaregiverTable");
        $plugin = $this->CustomControllerPlugin();  // custome plugin
        $CareGiveRes = array();
        $whereArr = array('care.UserID' => $UserID, 'care.status' => 1);
        $UserCareGiver = $CareGiverTableObj->getCareGiver($whereArr);
        $AssociatedBeaconTableObj = $this->getServiceLocator()->get("ServicesAssociatedBeaconTable");
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  Becons', 'UserCareGiver' => $UserCareGiver,
                    'AssociatedBeaconTableObj' => $AssociatedBeaconTableObj, 'UserID' => $UserID, 'UserBeconsArr' => $UserBecons
        ));
    }

    /*     * ********
     * Action: TRACK MOVE DATA 
     * @author: SHIVENDRA SUMAN
     * Created Date: 23-04-2015.
     * *** */

    public function outdoorAction() {
        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin        
        $IndPhyTableObj = $this->getServiceLocator()->get("IndividualPhysicalActivityTable");
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $time_zone = $this->_sessionObj->userDetails['time_zone'];

        $today2 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        $today_date_time = $today2->format('Y-m-d H:i:s');
      //moves data 
        $today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0, 0);
        $today->format('Y-m-d H:i:s');
        //Get UTC midnight time
        $today->setTimezone(new \DateTimeZone("UTC"));
        $startDate = $today->format('Y-m-d H:i:s');
        $endDate = $today->modify('+24 hours')->format('Y-m-d H:i:s'); //$start + 24;
        
        $whereMovesArray = array('UserID' => $UserID, 'startDate' => $startDate,  'endDate' =>  $endDate,'source' => 'movesapp');
        $orderby = "ExerciseRecorddate DESC, ExerciseRecordtime DESC";
       // $whereMovesArray = array('UserID' => $UserID, 'ExerciseRecorddate' => $today_date, 'source' => 'movesapp');
        $movesData = $IndPhyTableObj->getPhysicalRecords($whereMovesArray, array(), $orderby);
        $i = 0;
        $coloumn = array('color');
        foreach ($movesData as $mvalue) {
            $colordata = $IndPhyTableObj->getActivityColor(array('activity' => $mvalue['ActivityType']), $coloumn);
            $colorname = (isset($colordata[0]['color']) ? $colordata[0]['color'] : '');
            if (empty($colorname)) {
                $colorname = '#e1e1e1';
            }
            $movesData[$i]['color'] = $colorname;
            $i++;
        }
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  Location',
                    'CusConPlug' => $CusConPlug,
                    'movesData' => $movesData,
                    'today_date' => $today_date_time,
                    'time_zone' => $time_zone
        ));
    }

    /*     * ********
     * Action: TRACK BEACON DATA 
     * @author: SHIVENDRA SUMAN
     * Created Date: 23-04-2015.
     * *** */

    public function locationAction() {
        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }

        $CareBeDataTabObj = $this->getServiceLocator()->get('CareBeaconData');
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin   
        $UserID = $this->_sessionObj->userDetails['UserID'];
        $time_zone = $this->_sessionObj->userDetails['time_zone'];

        $today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0, 0);
        $today->format('Y-m-d H:i:s');

        //Get UTC midnight time
        $today->setTimezone(new \DateTimeZone("UTC"));
        $start = $today->format('Y-m-d H:i:s');
        $end = $today->modify('+24 hours')->format('Y-m-d H:i:s'); //$start + 24;
//echo $start ." to ".$end;exit;

        $today2 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        
        $today_date_time = $today2->format('Y-m-d H:i:s');
        $coloumn = array();
        $lookingfor = 'bd.creation_date DESC';
        $bdata = array(
            "bd.UserID" => $UserID,
            "startDate" => $start,
            "endDate" => $end
        );
        $beconData = $CareBeDataTabObj->getBeaconData($bdata, $coloumn, $lookingfor);
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  Location outdoor',
                    'CusConPlug' => $CusConPlug,
                    'beconData' => $beconData,
                    'today_date' => $today_date_time,
                    'time_zone' => $time_zone
        ));
    }

    /*     * ********
     * Action: LIST BECON DATA ON AJAX
     * @author: SHIVENDRA SUMAN
     * Created Date: 23-04-2015.
     * *** */

    public function listbeacondataAction() {
        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $CareBeDataTabObj = $this->getServiceLocator()->get('CareBeaconData');
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin   

        $request = $this->getRequest();

        $UserID = $this->_sessionObj->userDetails['UserID'];
        $time_zone = $this->_sessionObj->userDetails['time_zone'];
        $today1 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        $today_date = $today1->format('Y-m-d H:i:s');

        $today2 = (new \DateTime("now", new \DateTimeZone($time_zone)));
        $today_date_time = $today2->format('Y-m-d H:i:s');

        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if (!empty($postDataArr['data_date'])) {
                // $todayUTC = (new \DateTime($postDataArr['data_date'] . " " . date('H:i:s', strtotime($today_date)), new \DateTimeZone('UTC')));
                // $utcDateTime = $todayUTC->format('Y-m-d H:i:s');

                $today = (new \DateTime($postDataArr['data_date'] . " " . date('H:i:s', strtotime("now")), new \DateTimeZone($time_zone)))->setTime(0, 0);
                $today->format('Y-m-d H:i:s');
                //Get UTC midnight time
                $today->setTimezone(new \DateTimeZone("UTC"));
                $start = $today->format('Y-m-d H:i:s');
                $end = $today->modify('+24 hours')->format('Y-m-d H:i:s'); //$start + 24;
            } else {
                $today = (new \DateTime("now", new \DateTimeZone($time_zone)))->setTime(0, 0);
                $today->format('Y-m-d H:i:s');
                //Get UTC midnight time
                $today->setTimezone(new \DateTimeZone("UTC"));
                $start = $today->format('Y-m-d H:i:s');
                $end = $today->modify('+24 hours')->format('Y-m-d H:i:s'); //$start + 24;
            }

            //   $data_date = (isset($postDataArr['data_date']) && !empty($postDataArr['data_date'])) ? date("Y-m-d", strtotime($utcDateTime)) : date("Y-m-d", strtotime($today_date));
            $whereArray = array("bd.UserID" => $UserID,'startDate' => $start, 'endDate' => $end );
             $coloumn = array();
            $lookingfor = 'date DESC, time DESC';
            $beconData = $CareBeDataTabObj->getBeaconData($whereArray, $coloumn, $lookingfor);
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        return $viewModel->setVariables(array(
                    'pageTitle' => "Individual Beacon Data",
                    'beconData' => $beconData,
                    'CusConPlug' => $CusConPlug,
                    'today_date' => $today_date_time,
                    'time_zone' => $time_zone
        ));
    }

}

?>