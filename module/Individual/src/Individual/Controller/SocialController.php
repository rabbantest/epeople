<?php

namespace Individual\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use Facebook;
use Facebook\FacebookApp;
use Facebook\Exceptions\FacebookAuthenticationException;
use Facebook\Authentication\AccessToken;
use Facebook\Helpers\FacebookRedirectLoginHelper;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;

class SocialController extends AbstractActionController {

    public function __construct() {
        
    }

    public function init() {
        $this->_sessionObj = new Container('frontend');
    }

    function indexAction() {
        $this->init();
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }

        $userId = $this->_sessionObj->userDetails['UserID'];
        $url = "http://individual.quantextualhealth.com/individual-social-fb";
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin      
        $code = $this->params()->fromQuery('code');
        // Create our Application instance (replace this with your appId and secret).
        $friendData = array();
        $response = array();
        $dB = array();
        $fbName = '';
        $logoutUrl = "";
        $accessToken = "";
        // $accessToken = 'CAACEdEose0cBABehen3ZCP1atreTBlVeUGE8mKWEu6fBSqaGojL49GG1hkYHdosDJNfl0OOoZAyqyaoM6v4jDYBUmYCdCdBfrVre16kIR6QYpZAcWn8Ya7iaHZBchqZAZAxDi0WHoS9sc7bHJx7qDJUrJu2XKA1dhELvxbcEFff1dZBL7rSxyWWAqSG8SfW8AfnSOjjjIyVXzS5dLh3mWvf';
        $loginUrl = "";
        $fb = new Facebook\Facebook(array(
            'app_id' => '1657081464526231',
            'app_secret' => '66149fd19ca8c1bf87169eab072b778e',
            'default_graph_version' => 'v2.4',
                //  'default_access_token' => 'CAACEdEose0cBABehen3ZCP1atreTBlVeUGE8mKWEu6fBSqaGojL49GG1hkYHdosDJNfl0OOoZAyqyaoM6v4jDYBUmYCdCdBfrVre16kIR6QYpZAcWn8Ya7iaHZBchqZAZAxDi0WHoS9sc7bHJx7qDJUrJu2XKA1dhELvxbcEFff1dZBL7rSxyWWAqSG8SfW8AfnSOjjjIyVXzS5dLh3mWvf', // optional
        ));
        //  $fb->getSe
//        $helper = new FacebookRedirectLoginHelper($url, '1657081464526231', '66149fd19ca8c1bf87169eab072b778e');
//
//        try {
//            $session = $helper->getSessionFromRedirect();
//        } catch (FacebookSDKException $e) {
//            $session = null;
//        }
//
//        if ($session) {
//            // User logged in, get the AccessToken entity.
//            $accessToken = $session->getAccessToken();
//            // Exchange the short-lived token for a long-lived token.
//            $longLivedAccessToken = $accessToken->extend();
//            // Now store the long-lived token in the database
//            // . . . $db->store($longLivedAccessToken);
//            // Make calls to Graph with the long-lived token.
//            // . . . 
//             $logoutUrl = $helper->getLogoutUrl($accessToken, $url);
//        } else {
//              $loginUrl = $helper->getLoginUrl($url);
//           // echo '<a href="' . $helper->getLoginUrl() . '">Login with Facebook</a>';
//        }

        $helper = $fb->getRedirectLoginHelper();
        //  $datas = $fb->getLastResponse();
        // var_dump($datas);exit;
        if (empty($accessToken)) {
            $accessToken = $helper->getAccessToken();
            //  $accessToken = 'CAACEdEose0cBABehen3ZCP1atreTBlVeUGE8mKWEu6fBSqaGojL49GG1hkYHdosDJNfl0OOoZAyqyaoM6v4jDYBUmYCdCdBfrVre16kIR6QYpZAcWn8Ya7iaHZBchqZAZAxDi0WHoS9sc7bHJx7qDJUrJu2XKA1dhELvxbcEFff1dZBL7rSxyWWAqSG8SfW8AfnSOjjjIyVXzS5dLh3mWvf';
        }

        if (empty($accessToken)) {
            $scope = array('scope' => 'user_birthday, user_religion_politics, user_relationships, user_relationship_details, user_hometown, user_location, user_likes, user_education_history, user_work_history, user_website, user_managed_groups, user_events, user_photos, user_videos, user_friends, user_about_me, user_status, user_tagged_places, user_posts, email, manage_pages, publish_pages, publish_actions, read_custom_friendlists, user_actions.fitness, public_profile');

            $loginUrl = $helper->getLoginUrl($url, $scope);
        } else {
            $logoutUrl = $helper->getLogoutUrl($accessToken, $url);
        }

        // $permissions = ['email']; // Optional permissions      
        if (!empty($accessToken)) {
            try {
                /* $me ='me?fields=id,name,about,address,age_range,bio,'
                  . 'birthday,education,email,picture,favorite_athletes,'
                  . 'favorite_teams,first_name,gender,hometown,inspirational_people,'
                  . 'interested_in,languages,last_name,location,meeting_for,middle_name,'
                  . 'quotes,relationship_status,religion,sports,website,work,political,'
                  . 'devices,link,name_format,timezone,updated_time,verified';
                 */
                $me = 'me?fields=id,name,about,address,age_range,bio,'
                        . 'birthday,education,email,picture,favorite_athletes,'
                        . 'favorite_teams,first_name,gender,hometown,inspirational_people,'
                        . 'interested_in,languages,last_name,location,meeting_for,middle_name,'
                        . 'quotes,relationship_status,religion,sports,website,work,political,'
                        . 'devices,link,name_format,timezone,updated_time,verified,'
                        . 'friends{name,bio,birthday,about,address,location,education,hometown,email},'
                        . 'friendlists{name,id,members{id,link,name,profile_type,username,picture}},'
                        . 'family{about,bio,birthday,address,age_range,devices,education,hometown,id,email,favorite_athletes,favorite_teams,name,work,interested_in,location,relationship,relationship_status,likes}';

                $response = $fb->get($me, $accessToken);
                $mefriend = "me/friends?fields=name,gender,hometown,location";
                $friendResonce = $fb->get($mefriend, $accessToken);
                
                $friendlistData = $fb->api('me?fields=friendlists{name,id,members{id,link,name,profile_type,username,picture}}',$accessToken);
                $friendsData = $fb->api('me?fields=friends{name,address,gender,hometown}',$accessToken);
                $familyData = $fb->api('me?fields=family{about,bio,birthday,address,age_range,devices,education,hometown,id,email,favorite_athletes,favorite_teams,name,work,interested_in,location,relationship,relationship_status,likes}',$accessToken);
                //$postsData = $facebook->api('me?fields=posts');
                $postsData = $fb->api('me?fields=posts{created_time,id,name,picture,story,comments,likes{name}}',$accessToken);
                $photoData = $fb->api('me?fields=photos{name,comments{like_count},likes{name,id,picture}}',$accessToken);
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            $gpu = $response->getGraphUser();
            $gevent = $response->getGraphEvent();
            $gRequest = $response->getRequest();
            $dB = $response->getDecodedBody();
            $friendData = $friendResonce->getDecodedBody();
            echo '<pre>';
            print_r($friendData);
            exit;

            $fbName = $gpu->getName();
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual  Dashboard',
                    'custom_plugin' => $CusConPlug, // cust
                    'code' => $code,
                    'helper' => $helper,
                    'accessToken' => $accessToken,
                    'url' => $url,
                    'fbName' => $fbName,
                    'decodedBody' => $dB,
                    'response' => $response,
                    "loginUrl" => $loginUrl,
                    "logoutUrl" => $logoutUrl
        ));
    }

}
