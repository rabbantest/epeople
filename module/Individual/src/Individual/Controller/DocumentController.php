<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE DOCUMENT OF INDIVIDUAL  FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 04/02/2015.
 * UPDATED: 04/02/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class DocumentController extends AbstractActionController {

    public function __construct() {
        $this->_sessionObj = new Container('frontend');
    }

    public function indexAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $userId = $this->_sessionObj->userDetails['UserID'];
        $IndDocTableObj = $this->getServiceLocator()->get("IndividualDocumentTable");
        $catList = $IndDocTableObj->getDocCatlist(); //category list  
        $whereArray = array();
        $columns = array();
        $itemsPerPage = 5;
        $paginator = array();
        $lookingfor = "creation_date DESC";
        $searchcontent = array();
        $postDataArr = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $searchcontent = array('keyword' => $postDataArr['keyword'], 'cat' => $postDataArr['cat']);
        }

        $whereArray['UserID'] = $userId;
        $whereArray['status'] = 0;
        $docRecord = $IndDocTableObj->getDocRecord($whereArray, $columns, $lookingfor, $searchcontent);
        if (is_array($docRecord)) {
            $iterateddata = $docRecord;
            $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($iterateddata));
            $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
            $paginator->setItemCountPerPage($itemsPerPage);
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => "Individual Document",
                    'catList' => $catList,
                    'docRecord' => $paginator,
                    'routeParams' => $searchcontent,
        ));
    }

    public function editdocAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $userId = $this->_sessionObj->userDetails['UserID'];

        $request = $this->getRequest();
        if ($request->isPost()) {
            $IndDocTableObj = $this->getServiceLocator()->get("IndividualDocumentTable");
            $postDataArr = $request->getPost()->toArray();
            $catList = $IndDocTableObj->getDocCatlist(); //category list
            $whereArray = array("UserID" => $userId, "doc_id" => $postDataArr['docid']);
            $documentData = $IndDocTableObj->getDocRecord($whereArray);
        }


        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => "Individual Document",
                    'catList' => $catList,
                    'documentData' => $documentData
        ));
    }

    /*     * ********
     * Action: This action download document 
     * @author: shivendra Suman
     * Created Date: 05-02-2015.
     * *** */

    public function downloadAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $userId = $this->_sessionObj->userDetails['UserID'];
        $userFirstName = $this->_sessionObj->userDetails['FirstName'];
        $userLastName = $this->_sessionObj->userDetails['LastName'];
        $fileName = base64_decode($this->params()->fromQuery('filename')); // get file name from url
        $title = base64_decode($this->params()->fromQuery('title')); // get file name from url 
        if (!is_file($fileName)) {
            //do something
        }
        $file_name = strtolower(basename($fileName));
        $ext = substr($file_name, strrpos($file_name, '.') + 1);
        $ext = "." . $ext;
        $file_name_download = $title . '_' . $userFirstName . $ext; //$userFirstName.'_'.$userLastName .'_'. time() . $ext; // download file using this name 
        $fileContents = file_get_contents($fileName);

        $response = $this->getResponse();
        $response->setContent($fileContents);

        $headers = $response->getHeaders();
        $headers->clearHeaders()
                ->addHeaderLine('Content-Type', 'application/octet-stream')
                ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $file_name_download . '"')
                ->addHeaderLine('Content-Length', strlen($fileContents));
        return $this->response;
    }

}
