<?php

namespace Individual\Controller;

/* * ***********************
 * PAGE: USE TO MANAGE THE INDEX CONTROLLER FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: RABBAN AHMAD.
 * CREATOR: 12/11/2014.
 * UPDATED: 21/04/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Individual\Form\BasicInfo;
use Individual\Form\BasicInfoValidator;

class IndexController extends AbstractActionController {

    protected $IndividualUserTable;
    protected $_sessionObj;

    public function __construct() {
        $this->_sessionObj = new Container('frontend');
        $UserDataArr = $this->_sessionObj->userDetails;
    }

    public function indexAction() {
        if ($this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual-dashboard');
        }
        $UserDataArr = $this->_sessionObj->userDetails;
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");

        $plugin = $this->CustomControllerPlugin();  // custome plugin
        $request = $this->getRequest();
        $IndPrivSetTableObj = $this->getServiceLocator()->get("IndividualPrivacySettingsTable");
        $error = array();
        $postDataArr = array('user_name' => '', 'preferably_device' => '', 'email' => '', 'fname' => '', 'lname' => '', 'age' => '', 'password' => '', 'confirm_password' => '');
        $emailExist = 0;
        $UsernameExist = 0;
        /*
          @auth : shivendra Suman
          @ validic veriables
         */
        $siteURL = $_SERVER['HTTP_HOST'];
        $siteArray = explode('.', $siteURL);
        if (strtolower($siteArray[0]) == 'www') {
            $siteDomain = $siteArray[1];
        } else {
            $siteDomain = $siteArray[0];
        }

        $ReqSchme = $_SERVER['REQUEST_SCHEME'];
        $ip = $_SERVER['REMOTE_ADDR'];
        if ($ReqSchme == 'http' && $siteArray[1] == 'health-epeople' && $ip != '127.0.0.1') {
            $host = $_SERVER['HTTP_HOST'];
            $DomainName = $_SERVER['REQUEST_SCHEME'] . "s" . "://" . $host . "/individual";
            return $this->redirect()->toUrl($DomainName);
        }
        //$siteDomain = 'individual';
        $isDomainValid = $IndUserTableObj->validateDomain(array('subdomain' => trim($siteDomain)));
        if ($isDomainValid) {
            $registrationId = '';
            $invalidHash = '';
            $invite_id = '';/** @invite_id is the study invitation id* */
            if ($this->params('invite_id')) {
                $invite_id = $this->params('invite_id');
            }
            if ($this->params('id') != '' && $this->params('invite_id') == '') {
                $registrationId = $this->params('id');
                $wherArr = array('pass_id' => $isDomainValid['id'], 'hash_code' => $registrationId);
                $ValidateHash = $IndUserTableObj->getResearcherInvitationHash($wherArr);
                if (!$ValidateHash) {
                    $invalidHash = true;
                }
            }

            $domainLogo = ($isDomainValid['logo'] ? $isDomainValid['logo'] : 'health_matrix_logo_prelogin.png');
            $this->_sessionObj->domainLogo = $domainLogo;
            $isDomainValid = $isDomainValid['id'];
            $ORGANIZATION_ID = "526ec0ca6dedda7b1c000025";
            $ORGANIZATION_ACCESS_TOKEN = "35f76b711c1b31c8c3365412fbfe584b74bc4a21f6d5e3737b83cebf4d6b6c7f";
            if ($request->isPost()) {
                $postDataArr = $request->getPost()->toArray();
                $error = array();
                if (trim($postDataArr['email']) == "") {
                    $error['EmailError'] = "Please enter email.";
                } else {
                    $whereArrEmail = array("Email" => $postDataArr['email']);
                    $emailExist = $IndUserTableObj->getUserExist($whereArrEmail);
                    if (count($emailExist)) {
                        $error['EmailError'] = "Email already exist.";
                    }
                }
                if (trim($postDataArr['user_name']) !== "") {
                    $whereArrUsername = array("UserName" => $postDataArr['user_name']);
                    $UsernameExist = $IndUserTableObj->getUserExist($whereArrUsername);
                    if (trim($postDataArr['user_name']) == "") {
                        $error['UserNameError'] = "Please enter user name.";
                    }
                }
                if (trim($postDataArr['fname']) == "") {
                    $error['FNameError'] = "Please enter First name.";
                }
                if (trim($postDataArr['age']) == "") {
                    $error['AgeError'] = "Please enter age.";
                }
                if (trim($postDataArr['password']) == "") {
                    $error['PasswordError'] = "Please enter password.";
                }
                if (trim($postDataArr['confirm_password']) == "") {
                    $error['CPasswordError'] = "Please enter confirm password.";
                }
                if (trim($postDataArr['confirm_password']) !== trim($postDataArr['password'])) {
                    $error['PasswordError'] = "Confirm password not matched.";
                }
                if (count($UsernameExist) > 0) {
                    $error['UserNameError'] = "That user name is taken, please select again.";
                }
                /*                 * *****Start Code to check User limit for requesting PAAS ****** */
                if ($isDomainValid != 1) {
                    $exp = '"' . $isDomainValid . '":\\\[("[[:digit:]]*",)*"1"';
                    $whereArr = array(new \Zend\Db\Sql\Predicate\Expression(" UserTypeId REGEXP '$exp'"));
                    $PassUsers = $IndUserTableObj->getUserExist($whereArr, array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')));
                    $PassUserLimit = $IndUserTableObj->getPassUserLimit(array('id' => $isDomainValid));
                    if ($PassUserLimit == $PassUsers[0]['num']) {
                        $error['UserLimit'] = "Allocated Panel member limit exceed.You can not register!";
                    }
                }
                /*                 * *****End Code to check User limit for requesting PAAS ****** */
                if (count($error) == 0) {
                    /*
                      @auth : shivendra Suman
                      @ validic user registration
                     */
                    $validic_user_id = $validicaccess_token = '';
                    $userTypeId = json_encode(array($isDomainValid => $postDataArr['usertype']));
                    $code = md5(time());
                    $Agetime = strtotime("-" . $postDataArr['age'] . " year", time());
                    $Dob = date("Y-m-d", $Agetime);
                    $data = array(
                        "Email" => $postDataArr['email'],
                        "UserName" => $postDataArr['user_name'],
                        "FirstName" => $postDataArr['fname'],
                        "LastName" => $postDataArr['lname'],
                        "preferably_device" => $postDataArr['preferably_device'],
                        "Age" => $postDataArr['age'],
                        "Dob" => $Dob,
                        "Password" => md5($postDataArr['password']),
                        "UserTypeId" => $userTypeId,
                        'UpdationDate' => round(microtime(true) * 1000), 'CreationDate' => round(microtime(true) * 1000),
                        'PassCode' => $code,
                            // "validic_id" => $validic_user_id,
                            //"validic_access_token" => $validicaccess_token
                    );
                    $res = $IndUserTableObj->saveUser($data);
                    /*                     * * Code to check for study invitation * */
                    if ($this->params('id') != '' && $this->params('invite_id') != '' && ($postDataArr['email'] == base64_decode($invite_id))) {

                        $registrationId = $this->params('id');
                        $invite_id = $this->params('invite_id');
                        $StudyTableObj = $this->getServiceLocator()->get('StudyTable');
                        $InvitationExist = $StudyTableObj->getStudyInvitationSet(array('invitation_email' => base64_decode($invite_id)), array('invitation_email', 'invitation_set_study'));
                        if ($InvitationExist) {
                            foreach ($InvitationExist as $Res) {
                                $sceering = 1;
                                if ($Res['screening'] == 1) {
                                    $sceering = 0;
                                }
                                $DataSave = array(
                                    'study_id' => $Res['invitation_set_study'],
                                    'bucket' => -1,
                                    'eligible_for_main' => $sceering,
                                    'user_id' => $res,
                                    'type' => $sceering,
                                    'updation_date' => round(microtime(true) * 1000),
                                    'creation_date' => round(microtime(true) * 1000)
                                );
                                $StudyTableObj->insertStudyInvitaion($DataSave);
                            }
                        }
                    }
                    /** End * */
                    if (!empty($postDataArr['registrationId'])) {
                        $IndUserTableObj->updateResearcherInvitationStatus(array('hash_code' => $postDataArr['registrationId'], 'status' => 1));
                    }
                    if ($res) {
                        $IndPrivSetTableObj->insertPrivacySettings(array("UserID" => $res, "creation_date" => round(microtime(true) * 1000)));
                        // set defalut source
                        $IndDataSourceTableObj = $this->getServiceLocator()->get("DataSource");
                        $DefaultData = $IndDataSourceTableObj->geteDefaultSource(array("UserId" => $res));
                        if (count($DefaultData) <= 0) {
                            $insArra = array("UserId" => $res, 'creation_date' => round(microtime(true) * 1000));
                            $dataIns = $IndDataSourceTableObj->insertUserDefaultSource($insArra);
                            if ($dataIns) {
                                $UserDataArr['bodyweight'] = 1;
                                $UserDataArr['physicalactivity'] = 1;
                                $UserDataArr['cholesterol'] = 1;
                                $UserDataArr['bloodpressure'] = 1;
                                $UserDataArr['bloodglucose'] = 1;
                                $UserDataArr['sleep'] = 1;
                                $UserDataArr['temperature'] = 1;
                                $UserDataArr['step'] = 1;
                            }
                        }
                        //save manual device
                        $connectedsource_manual = $IndUserTableObj->getConDevice(array("sourceId" => 1, "UserId" => $res), array("id"));
                        if (count($connectedsource_manual) <= 0) { // check previously saved device or not                            
                            $insDataArrManual = array("UserId" => $res, "sourceId" => 1, "creation_date" => round(microtime(true) * 1000));
                            $IndUserTableObj->insertDevice($insDataArrManual); // insert device which is not enrtered
                        }
                    }
                    $uname = ucfirst($postDataArr['user_name']);
                    $url = "https://" . $_SERVER['HTTP_HOST'] . "/individual/index/activateacount?passcode=$code";
                    $message = "<img src='http://my.health-epeople.com/img/mail_header_image.jpeg' alt='Company Logo'><br> <br>";
                    $message .= "<br><br>";
                    $message .= "Dear " . $postDataArr['fname'] . ",<br> <br>";
                    $message .= "Thank you for joining Health-ePeople. Please click the link below to activate your account. ";
                    $message .= "Your account is pending verification by the Site Administrator. Once confirmed, you will receive a confirmation email for the same and you will be able to log in to your account.";
                    $message .= "<br><br><a href='$url' target='_blank'>$url</a>";
                    $message .= "<br><br>If the link does not works, please copy the entire link and past it in your browser";
                    $message .="<br><br>Thank you<br>";
                    $message .="<br><br>Regards,<br>Health-ePeople Team";
                    $subject = "Account activation link";
                    $plugin->senInvitationMail($postDataArr['email'], $subject, $message);
                    return $this->redirect()->toUrl('/individual/index/verification');
                }
            }
        } else {
            return $this->redirect()->toUrl(Host);
        }
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual', 'postValue' => $postDataArr
                    , 'errorArr' => $error, 'domainLogo' => $domainLogo, 'pass_id' => $isDomainValid,
                    'registrationId' => $registrationId, 'invalidHash' => $invalidHash, 'invite_id' => $invite_id
        ));
    }

    public function loginAction() {
        $type = base64_decode($this->params()->fromQuery('type'));
        $assessment_id = base64_decode($this->params()->fromQuery('assessment_id'));
        $study_trial = base64_decode($this->params()->fromQuery('study_trial'));
        $study_id = base64_decode($this->params()->fromQuery('study_id'));
        $study_type = base64_decode($this->params()->fromQuery('study_type'));
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        if ($this->_sessionObj->isWeblogin) {
            if ($type == 'mobile') {
                if (!empty($assessment_id)) {
                    return $this->redirect()->toUrl('/individual-assessment-list?id=' . base64_encode($assessment_id));
                } elseif ($study_trial = 'study_trial') {
                    if ($study_type == 'screening') {
                        //return $this->redirect()->toUrl('/individual-virtualtrials');
                        return $this->redirect()->toUrl('/individual-study');
                    } else {
                        //return $this->redirect()->toUrl('/individual-studytri-main');
                        return $this->redirect()->toUrl('/individual-study');
                    }
                } else {
                    if ($study_type == 'screening') {
                        return $this->redirect()->toUrl('/individual-study');
                    } else {
                        return $this->redirect()->toUrl('/individual-study');
                    }
                }
            } else {
                return $this->redirect()->toUrl('/individual-dashboard');
            }
        }
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $siteURL = $_SERVER['HTTP_HOST'];
        $siteArray = explode('.', $siteURL);
        if (strtolower($siteArray[0]) == 'www') {
            $siteDomain = $siteArray[1];
        } else {
            $siteDomain = $siteArray[0];
        }
        $ReqSchme = $_SERVER['REQUEST_SCHEME'];
        $ip = $_SERVER['REMOTE_ADDR'];
        if ($ReqSchme == 'http' && $siteArray[1] == 'health-epeople' && $ip != '127.0.0.1') {
            $host = $_SERVER['HTTP_HOST'];
            $DomainName = $_SERVER['REQUEST_SCHEME'] . "s" . "://" . $host;
            return $this->redirect()->toUrl($DomainName);
        }
        //$siteDomain = 'individual';
        $isDomainValid = $IndUserTableObj->validateDomain(array('subdomain' => trim($siteDomain)));
        if ($isDomainValid) {
            $time_zoneP = '';
            $domainLogo = ($isDomainValid['logo'] ? $isDomainValid['logo'] : 'health_matrix_logo_prelogin.png');
            $this->_sessionObj->domainLogo = $domainLogo;
            $isDomainValid = $isDomainValid['id'];
            $request = $this->getRequest();
            $error = "";
            $emailP = '';
            $passwordP = '';
            if ($request->isPost()) {
                $postDataArr = $request->getPost()->toArray();
                $post_type = (isset($postDataArr['type']) ? $postDataArr['type'] : '');
                $type = base64_decode($post_type);
                $assessment_ids = (isset($postDataArr['assessment_id']) ? $postDataArr['assessment_id'] : "");
                $assessment_id = base64_decode($assessment_ids);
                $study_trials = (isset($postDataArr['study_trial']) ? $postDataArr['study_trial'] : '');
                $study_trial = base64_decode($study_trials);
                $study_ids = (isset($postDataArr['study_id']) ? $postDataArr['study_id'] : '');
                $study_id = base64_decode($study_ids);
                $study_types = (isset($postDataArr['study_type']) ? $postDataArr['study_type'] : '');
                $study_type = base64_decode($study_types);

                $whereArrEmail = array("Email" => $postDataArr['email']);
                $whereArr = array("Email" => $postDataArr['email'], "Password" => md5($postDataArr['password']));
                $emailExist = $IndUserTableObj->getUserExist($whereArrEmail);
                if (count($emailExist) > 0) {
                    $result = $IndUserTableObj->getUserExist($whereArr);
                    if (count($result) > 0) {
                        if (isset($postDataArr['joinYes']) && $postDataArr['joinYes'] == 1) {
                            $postDataArr['time_zone'] = $postDataArr['PtimeZone'];
                            $typeArr = json_decode($result[0]['UserTypeId'], true);
                            $typeArr[$isDomainValid][] = "1";
                            $UpdateUserIds = json_encode($typeArr);
                            $IndUserTableObj->updateUser(array('UserID' => $result[0]['UserID']), array('UserTypeId' => $UpdateUserIds));
                        }
                        $res = $IndUserTableObj->getUserExist($whereArr);
                        $UserTypeId = json_decode($res[0]['UserTypeId'], true);
                        if (array_key_exists($isDomainValid, $UserTypeId)) {
                            $IndDataSourceTableObj = $this->getServiceLocator()->get("DataSource");
                            $ActiveByadmin = json_decode($res[0]['Active_Users_By_Admin'], true);
                            if (array_key_exists($isDomainValid, $ActiveByadmin) && count($ActiveByadmin)) {
                                if ($res[0]['Verified'] == 1) {
                                    $DefaultData = $IndDataSourceTableObj->geteDefaultSource(array("UserId" => $res[0]['UserID']));
                                    $bodyweight = '';
                                    $physicalactivity = '';
                                    $cholesterol = '';
                                    $bloodpressure = '';
                                    $bloodglucose = '';
                                    $sleep = '';
                                    $temperature = '';
                                    $step = '';

                                    if (count($DefaultData) > 0) {
                                        $bodyweight = $DefaultData[0]['bodyweight'];
                                        $physicalactivity = $DefaultData[0]['physicalactivity'];
                                        $cholesterol = $DefaultData[0]['cholesterol'];
                                        $bloodpressure = $DefaultData[0]['bloodpressure'];
                                        $bloodglucose = $DefaultData[0]['bloodglucose'];
                                        $sleep = $DefaultData[0]['sleep'];
                                        $step = $DefaultData[0]['step'];
                                        $temperature = $DefaultData[0]['temperature'];
                                    }

                                    $this->_sessionObj->userDetails = array('UserID' => $res[0]['UserID'], 'UserTypeId' => $res[0]['UserTypeId'],
                                        'FirstName' => $res[0]['FirstName'], 'LastName' => $res[0]['LastName'], 'UserName' => $res[0]['UserName'], 'IsTempPasswordUsed' => $res[0]['IsTempPasswordUsed']
                                        , 'ProfileCompletedStage' => $res[0]['ProfileCompletedStage'], 'EverLogin' => 0, 'PanelMember' => $res[0]['PanelMember']
                                        , 'LastName' => $res[0]['LastName'], 'ProfileCompleted' => $res[0]['ProfileCompleted'], 'Image' => $res[0]['Image'], 'Age' => $res[0]['Age'],
                                        'Dob' => $res[0]['Dob'], 'Gender' => $res[0]['Gender'], 'DomainID' => $isDomainValid, 'domainLogo' => $domainLogo, 'Email' => $res[0]['Email'],
                                        'bodyweight' => $bodyweight, 'physicalactivity' => $physicalactivity, 'cholesterol' => $cholesterol,
                                        'bloodpressure' => $bloodpressure, 'bloodglucose' => $bloodglucose, 'sleep' => $sleep, 'step' => $step,
                                        'temperature' => $temperature, 'time_zone' => $postDataArr['time_zone']
                                    );
                                    $this->_sessionObj->isWeblogin = '1';
                                    $this->_sessionObj->domain = $siteDomain;
                                    $this->_sessionObj->browser = 'web';
                                    if ($type == 'mobile') {
                                        $this->_sessionObj->browser = 'mobile';
                                    }
                                    if ($res[0]['IsTempPasswordUsed'] == 1) {
                                        $this->_sessionObj->userDetails['EverLogin'] = 1;
                                        return $this->redirect()->toUrl('/individual/index/tempass');
                                    } elseif ($res[0]['EverLogin'] == 0) {
                                        return $this->redirect()->toUrl('/individual/index/congrates');
                                    } else {
                                        /** code to get profile filled by user * */
                                        if ($res[0]['ProfileCompleted'] == 1) {
                                            if ($type == 'mobile') {
                                                if (!empty($assessment_id)) {
                                                    return $this->redirect()->toUrl('/individual-assessment-list?id=' . base64_encode($assessment_id));
                                                } elseif ($study_trial = 'study_trial') {
                                                    if ($study_type == 'screening') {
                                                        return $this->redirect()->toUrl('/individual-study');
                                                    } else {
                                                        return $this->redirect()->toUrl('/individual-study');
                                                    }
                                                } else {
                                                    if ($study_type == 'screening') {
                                                        return $this->redirect()->toUrl('/individual-study');
                                                    } else {
                                                        return $this->redirect()->toUrl('/individual-study');
                                                    }
                                                }
                                            } else {
                                                return $this->redirect()->toUrl('/individual-dashboard');
                                            }
                                        } elseif ($res[0]['ProfileCompletedStage'] == 0) {
                                            return $this->redirect()->toUrl('/individual/index/basicinfo');
                                        } elseif ($res[0]['ProfileCompletedStage'] == 1) {
                                            return $this->redirect()->toUrl('/individual/index/contactinfo');
                                        } elseif ($res[0]['ProfileCompletedStage'] == 2) {
                                            return $this->redirect()->toUrl('/individual/index/profileinfo');
                                        }
                                    }
                                } else {
                                    $error = "Account is not verified.";
                                }
                            } else {
                                $error = "Your account is not verified by admin.";
                            }
                        } else {

                            $emailP = $postDataArr['email'];
                            $passwordP = $postDataArr['password'];
                            $time_zoneP = $postDataArr['time_zone'];
                            $error = 1;
                        }
                    } else {
                        $error = "Email id and password not matched.";
                    }
                } else {
                    $error = "Email id does not exist.";
                }
            }
        } else {
            return $this->redirect()->toUrl(Host);
        }
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'domainLogo' => $domainLogo, 'password' => $passwordP, 'Ptime_zone' => $time_zoneP,
                    'email' => $emailP, 'pageTitle' => 'Login', 'error' => $error, 'siteURL' => $siteURL, 'type' => $type,
                    'assessment_id' => $assessment_id, 'study_type' => $study_type, 'study_id' => $study_id,
                    'study_trial' => $study_trial, 'CusConPlug' => $CusConPlug));
    }

    /*     * ********
     * Action: Used to show congrates page.
     * @author: Rabban Ahmad
     * Created Date: 11-12-2014.
     * *** */

    public function tempassAction() {

        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/');
        }
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $UserArr = $this->_sessionObj->userDetails;
        $UserId = $this->_sessionObj->userDetails['UserID'];
        $request = $this->getRequest();
        $error = "";
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if (trim($postDataArr['password']) == "") {
                $error['PasswordError'] = "Please enter password.";
            }
            if (trim($postDataArr['confirm_password']) == "") {
                $error['CPasswordError'] = "Please enter confirm password.";
            }
            if (trim($postDataArr['confirm_password']) !== trim($postDataArr['password'])) {
                $error['PasswordError'] = "Confirm password not matched.";
            } else {
                $data = array("Password" => md5($postDataArr['password']), "IsTempPasswordUsed" => 0);
                $res = $IndUserTableObj->updateUser(array('UserID' => $UserId), $data);
                //echo $UserArr['EverLogin'];die;
                if ($UserArr['EverLogin'] == 0) {
                    return $this->redirect()->toUrl('/individual/index/congrates');
                } else {
                    if ($UserArr['ProfileCompleted'] == 1) {
                        return $this->redirect()->toUrl('/individual-dashboard');
                    } elseif ($UserArr['ProfileCompletedStage'] == 0) {
                        return $this->redirect()->toUrl('/individual/index/basicinfo');
                    } elseif ($UserArr['ProfileCompletedStage'] == 1) {
                        return $this->redirect()->toUrl('/individual/index/contactinfo');
                    } elseif ($UserArr['ProfileCompletedStage'] == 2) {
                        return $this->redirect()->toUrl('/individual/index/profileinfo');
                    }
                }
            }
        }
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual'
        ));
    }

    /*     * ********
     * Action: Used to show activate account
     * @author: Rabban Ahmad
     * Created Date: 18-12-2014.
     * *** */

    public function activateacountAction() {
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $data = array("Verified" => 1, 'PassCode' => '');
        $code = $_REQUEST['passcode'];
        $UserArr = $IndUserTableObj->getUserExist(array('PassCode' => $code));
        if (count($UserArr) > 0) {
            if ($UserArr[0]['Verified'] == 1) {
                $error = 2;
            } else {
                $IndUserTableObj->updateUser(array('UserID' => $UserArr[0]['UserID']), $data);
                $error = 3;
            }
        } else {
            $error = 1;
        }
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual', 'error' => $error
        ));
    }

    public function excAction() {
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $excObj = new \PHPExcel();
        $excObj->setActiveSheetIndex(0);
        echo 'hello';
        die;
    }

    /*     * ********
     * Action: Used to show congrates page.
     * @author: Rabban Ahmad
     * Created Date: 11-12-2014.
     * *** */

    public function congratesAction() {

        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/');
        }
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $UserArr = $this->_sessionObj->userDetails;
        $UserId = $UserArr['UserID'];
        //$data = array("EverLogin" => 1);
        //$res = $IndUserTableObj->updateUser(array('UserID' => $UserId), $data);

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual'
        ));
    }

    /*     * ********
     * Action: Used for forgot password with Ajax.
     * @author: Rabban Ahmad
     * Created Date: 12-12-2014.
     * *** */

    public function forgotpasswordAction() {

        $request = $this->getRequest();
        $postDataArr = $request->getPost()->toArray();
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $whereArrEmail = array("Email" => $postDataArr['Email']);
        $emailExist = $IndUserTableObj->getUserExist($whereArrEmail);
        if (count($emailExist) > 0) {
            echo "1";
        } else {
            echo "2";
        }
        exit;
    }

    /*     * ********
     * Action: Used to update basic info.
     * @author: Rabban Ahmad
     * Created Date: 11-12-2014.
     * *** */

    public function basicinfoAction() {
        $UserDataArr = $this->_sessionObj->userDetails;
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/');
        }
        if (($UserDataArr['ProfileCompletedStage'] == 1) || ($UserDataArr['ProfileCompletedStage'] == 2)) {
            return $this->redirect()->toUrl('/individual/index/contactinfo');
        }
        $UserId = $this->_sessionObj->userDetails['UserID'];
        $request = $this->getRequest();
        $dataArr = array();
        $form = new BasicInfo('basicinfo');
        $formValidator = new BasicInfoValidator();
        $form->setInputFilter($formValidator->getInputFilter());
        $form->setData($request->getPost());
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        if ($request->isPost()) {
            if ($form->isValid()) {
                $postDataArr = $request->getPost()->toArray();
                $dataArr['FirstName'] = $postDataArr['firstname'];
                $dataArr['MiddleName'] = $postDataArr['middlename'];
                $dataArr['LastName'] = $postDataArr['lastname'];
                $dataArr['short_description'] = $postDataArr['description'];
                $dataArr['NickName'] = $postDataArr['nickname'];
                $dataArr['Dob'] = date('Y-m-d', strtotime($postDataArr['dob']));
                $dataArr['Gender'] = $postDataArr['gender'];
                $dataArr['ProfileCompletedStage'] = 1;
                $this->_sessionObj->userDetails['ProfileCompletedStage'] = 1;
                $this->_sessionObj->userDetails['FirstName'] = $postDataArr['firstname'];
                $this->_sessionObj->userDetails['LastName'] = $postDataArr['lastname'];
                $IndUserTableObj->updateUser(array('UserID' => $UserId), $dataArr);
                return $this->redirect()->toUrl('/individual/index/contactinfo');
            } else {
                $message = 'Please Provide valid data.';
                $this->flashMessenger()->addMessage(array('error' => $message));
            }
        }
        $res = $IndUserTableObj->updateUser(array('UserID' => $UserId), array('EverLogin' => 1));
        $sendArr = array('pageTitle' => 'Individual', 'form' => $form);
        return new ViewModel($sendArr);
    }

    /*     * ********
     * Action: Used to update contact info.
     * @author: Rabban Ahmad
     * Created Date: 11-12-2014.
     * *** */

    public function contactinfoAction() {

        $UserDataArr = $this->_sessionObj->userDetails;
        $userId = $UserDataArr['UserID'];
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/');
        }

        if (($UserDataArr['ProfileCompletedStage'] == 2) || ($UserDataArr['ProfileCompletedStage'] == 3)) {
            return $this->redirect()->toUrl('/individual/index/profileinfo');
        }

        $request = $this->getRequest();
        $dataArr = array();
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $ContactInfoTableObj = $this->getServiceLocator()->get("ContactInfoTable");
        $CountryTableObj = $this->getServiceLocator()->get("CountryTable");
        $countryArr = $CountryTableObj->getCountry();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $dataArr['user_id'] = $userId;
            $dataArr['street_address1'] = $postDataArr['address1'];
            $dataArr['street_address2'] = $postDataArr['address2'];
            $dataArr['city'] = $postDataArr['city'];
            $dataArr['state_id'] = $postDataArr['state'];
            $dataArr['country_id'] = $postDataArr['country'];
            $dataArr['phone_no'] = $postDataArr['phone_number'];
            $dataArr['county_id'] = $postDataArr['county'];
            $dataArr['postal_code'] = $postDataArr['postal_code'];
            $dataArr['ethnicity'] = $postDataArr['ethnicity'];
            $dataArr['origin_id'] = $postDataArr['origin_id'];
            $ContactInfoTableObj->save($dataArr);
            $IndUserTableObj->updateUser(array('UserID' => $userId), array('ProfileCompletedStage' => 2));
            $this->_sessionObj->userDetails['ProfileCompletedStage'] = 2;
            return $this->redirect()->toUrl('/individual/index/profileinfo');
        }
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual', 'country' => $countryArr
        ));
    }

    /*     * ********
     * Action: Used to update profile info.
     * @author: Rabban Ahmad
     * Created Date: 11-12-2014.
     * *** */

    public function profileinfoAction() {
        $UserDataArr = $this->_sessionObj->userDetails;
        $userId = $UserDataArr['UserID'];
        if (!$this->_sessionObj->isWeblogin) {
            //  return $this->redirect()->toUrl('/');
            return $this->redirect()->toUrl('/individual-dashboard');
        }
        if ($UserDataArr['ProfileCompletedStage'] == 3) {
            return $this->redirect()->toUrl('/individual-dashboard');
        }
        $request = $this->getRequest();
        $ProfileInfoTableObj = $this->getServiceLocator()->get("ProfileInfoTable");
        $UserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $userTypeIDs = json_decode($UserDataArr['UserTypeId'], true);
        $domainID = $UserDataArr['DomainID'];
        $userTypeId = $userTypeIDs[$domainID];
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $dataArr['user_id'] = $userId;
            if (in_array(3, $userTypeId)) {
                $dataArr['institute_name'] = $postDataArr['institute'];
                $dataArr['department'] = $postDataArr['depratment'];
                $dataArr['dept_phone1'] = $postDataArr['dept_phone'];
                $dataArr['work_phone'] = $postDataArr['work_phone'];
                $dataArr['dept_head_name'] = $postDataArr['dept_head_name'];
            }
            if (in_array(2, $userTypeId)) {
                $dataArr['practice_name'] = $postDataArr['practice'];
            }
            $dataArr['education_degree'] = $postDataArr['education'];
            $dataArr['income'] = $postDataArr['income'];
            $dataArr['blood_group'] = $postDataArr['blood_type'];
            $dataArr['language'] = $postDataArr['language'];
            if (!isset($postDataArr['marital_status'])) {
                $marital_status = "m";
            } else {
                $marital_status = $postDataArr['marital_status'];
            }
            $dataArr['maritial_status'] = $marital_status;
            $dataArr['CreationDate'] = date("Y-m-d");

            /* start code for checking mime type */
            //$finfo = finfo_open(FILEINFO_MIME_TYPE);
            //$mime_type = finfo_file($finfo, $_FILES['profile_pic']['tmp_name']);
            //finfo_close($finfo);
            /* end code for checking mime type */
            $ProfileInfoTableObj->save($dataArr);

            $udpatedData = array();
            $udpatedData['Image'] = '';
            if ($_FILES['profile_pic']['tmp_name']) {
                $fileName = $_FILES['profile_pic']['name'];
                $tmp_name = $_FILES['profile_pic']['tmp_name'];
                $dir = $_SERVER['DOCUMENT_ROOT'] . "/upload_image/";
                mkdir($dir . $userId, 777);
                chmod($dir . $userId, 0777);
                $newfilename = md5(rand() * time());
                $file_name = strtolower(basename($fileName));
                $ext = substr($file_name, strrpos($file_name, '.') + 1);
                $ext = "." . $ext;
                $returnName = $newfilename . $ext;
                $newname = $dir . $userId . "/" . $newfilename . $ext;
                move_uploaded_file($tmp_name, $newname);
                $udpatedData['Image'] = $returnName;
                $this->_sessionObj->userDetails['Image'] = $returnName;
            }
            $udpatedData['ProfileCompletedStage'] = 3;
            $udpatedData['ProfileCompleted'] = 1;
            $UserTableObj->updateUser(array('UserID' => $userId), $udpatedData);
            $this->_sessionObj->userDetails['ProfileCompletedStage'] = 3;
            $this->_sessionObj->userDetails['ProfileCompleted'] = 1;
            return $this->redirect()->toUrl('/individual/index/profileinfo');
        }
        //$typeidArr = explode(',',$UserDataArr['UserTypeId']);
        $isResercher = 0;
        $isCaregiver = 0;
        if (in_array(2, $userTypeId)) {
            $isCaregiver = 1;
        }if (in_array(3, $userTypeId)) {
            $isResercher = 1;
        }
        $LanguageArr = $UserTableObj->getLanguage();
        $BloodTypeArr = $UserTableObj->getBloodType();
        $EthnicityArr = $UserTableObj->getEthnicity();
        $EducationcArr = $UserTableObj->getEducation();
        $IncomeArr = $UserTableObj->getIncomeData();
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual', 'language' => $LanguageArr
                    , 'education' => $EducationcArr, 'ethnicity' => $EthnicityArr, 'IncomeArr' => $IncomeArr,
                    'bloodtype' => $BloodTypeArr, 'isCaregiver' => $isCaregiver, 'isResearcher' => $isResercher
        ));
    }

    /*     * ********
     * Action: Used to show verification page.
     * @author: Rabban Ahmad
     * Created Date: 11-12-2014.
     * *** */

    public function verificationAction() {
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual'
        ));
    }

    /*     * ********
     * Action: Used to Response caregiver reqeust.
     * @author: Rabban Ahmad
     * Created Date: 01-04-2015.
     * *** */

    public function respondcaregiverAction() {
        $hash = $this->getRequest()->getQuery('hash');
        $reply = $this->getRequest()->getQuery('reply');
        $CareGiverTableObj = $this->getServiceLocator()->get("IndividualCaregiverTable");
        $where = array('hash' => $hash);
        $exist = $CareGiverTableObj->getCareGiver($where);
        $response = '';
        if (!$exist) {
            $response = 1;
        } elseif ($reply = 1) {
            $response = 2;
            $CareGiverTableObj->updateCareGiver($where, array('hash' => 1));
        } else {
            $response = 3;
            $CareGiverTableObj->deleteCareGiver($where);
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual', 'response' => $response
        ));
    }

    /*     * ****This action is used to get the Healthfinder.gov API data base on search string******* */

    public function knwbaseAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/');
        }
        $plugin = $this->CustomControllerPlugin();  // custome plugin
        $res['Result'] = array();
        $srch_string = '';
        $postDataArr = array();
        $request = $this->getRequest();
        $newArr = array();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $srch_string = '';



            if ($postDataArr['who'] != 0) {
                $srch_string .='&who=' . $postDataArr['who'];
            }

            if ($postDataArr['gender']) {
                $srch_string .='&gender=' . $postDataArr['gender'];
            }

            if (isset($postDataArr['pregnant']) && $postDataArr['pregnant'] != 0) {
                $srch_string .='&pregnant=' . $postDataArr['pregnant'];
            }

            if ($postDataArr['age'] != 0) {
                $srch_string .='&age=' . $postDataArr['age'];
            }

            $url = $url = "http://healthfinder.gov/developer/MyHFSearch.json?api_key=ybaicuvtirhkrgwg" . $srch_string;
            $fileContents = file_get_contents($url);
            $res = json_decode($fileContents, true);

            if (!empty($res)) {
                foreach ($res as $key => $index) {
                    $newArr['MainHeading'] = $index['MyHFHeading'];
                    $i = 1;
                    foreach ($index['Topics'] as $topics) {
                        $newArr[$topics['MyHFCategoryHeading']][$i]['MyHFDescription'] = $topics['MyHFDescription'];
                        $newArr[$topics['MyHFCategoryHeading']][$i]['AccessibleVersion'] = $topics['AccessibleVersion'];
                        $newArr[$topics['MyHFCategoryHeading']][$i]['Title'] = $topics['Title'];

                        $i++;
                    }
                }
            }

            ksort($newArr);
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual', 'res' => $newArr, 'post' => $srch_string, 'postData' => $postDataArr
        ));
    }

    /*     * ****This function is used to get the associated Domain list******* */

    public function associatedomainsAction() {
        $UserDataArr = $this->_sessionObj->userDetails;
        $IndUserTableObj = $this->getServiceLocator()->get("IndividualUsersTable");
        $DomainIds = json_decode($UserDataArr['UserTypeId'], true);
        if ($DomainIds) {
            $listDomainsId = array_keys($DomainIds);
            foreach ($listDomainsId as $k => $val) {
                $DomainName = $IndUserTableObj->getDomainName(array('id' => $val), array('subdomain', 'company_name', 'logo'));
                $domainsDetails['subdomain'] = $DomainName['subdomain'];
                $domainsDetails['company_name'] = $DomainName['company_name'];
                $domainsDetails['logo'] = $DomainName['logo'];
                $domains[] = $domainsDetails;
            }
        }
        $userId = $UserDataArr['UserID'];
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/');
        }
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => 'Individual', 'domains' => $domains
        ));
    }

    /*     * ****end****** */
    /*     * ********
     * Action: Use to logged out the super admin pannel.
     * @author: Rabban Ahmad
     * Created Date: 10-12-2014.
     * *** */

    public function mongoAction() {
        //   use MongoClient;
        echo phpinfo();
        die;
    }

    public function logoutAction() {

        $this->_sessionObj->getManager()->getStorage()->clear('frontend');
        return $this->redirect()->toUrl('/');
    }

}
