<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE DOCUMENT OF Emergency Contact  FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 05/02/2015.
 * UPDATED: 05/02/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class EmergencycontactController extends AbstractActionController {

    public function __construct() {
        $this->_sessionObj = new Container('frontend');
    }

    public function indexAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $userId = $this->_sessionObj->userDetails['UserID'];
        $IndEMCTableObj = $this->getServiceLocator()->get("IndividualEmergencycontactTable");
        $contactType = $IndEMCTableObj->getContactType();
        $itemsPerPage = 10;
        $paginator = array();
        $searchcontent = array();
        $whereArray = array('UserID' => $userId, 'status' => 0);
        $contactData = $IndEMCTableObj->getEMCRecord($whereArray);
        $i = 0;
        foreach ($contactData as $cvalue) {
            $content_type_id = $cvalue['contact_type'];
            $whereCTArray = array('appointment_type_id' => $content_type_id);
            $contentName = $IndEMCTableObj->getContactType($whereCTArray);
            $contactData[$i]['contact_type_name'] = $contentName[0]['appointment_type'];
            $i++;
        }

        if (is_array($contactData)) {
            $iterateddata = $contactData;
            $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($iterateddata));
            $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
            $paginator->setItemCountPerPage($itemsPerPage);
        }


        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => "Individual Emergency Contact",
                    'contactType' => $contactType,
                    'contactData' => $paginator,
                    'routeParams' => $searchcontent,
        ));
    }

    public function editAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $userId = $this->_sessionObj->userDetails['UserID'];
        $IndEMCTableObj = $this->getServiceLocator()->get("IndividualEmergencycontactTable");
        $contactType = $IndEMCTableObj->getContactType();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $whereArray = array('UserID' => $userId, 'status' => 0, "contacts_id" => $postDataArr['contacts_id']);
            $contactData = $IndEMCTableObj->getEMCRecord($whereArray);
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => "Individual Emergency Contact",
                    'contactType' => $contactType,
                    'contactData' => $contactData,
        ));
    }

    public function viewAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $userId = $this->_sessionObj->userDetails['UserID'];
        $IndEMCTableObj = $this->getServiceLocator()->get("IndividualEmergencycontactTable");


        $request = $this->getRequest();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $whereArray = array('UserID' => $userId, 'status' => 0, "contacts_id" => $postDataArr['contacts_id']);
            $contactData = $IndEMCTableObj->getEMCRecord($whereArray);
            //mearge emergency contanct name by id 
            $i = 0;
            foreach ($contactData as $cvalue) {
                $content_type_id = $cvalue['contact_type'];
                $whereCTArray = array('appointment_type_id' => $content_type_id);
                $contentName = $IndEMCTableObj->getContactType($whereCTArray);
                $contactData[$i]['contact_type_name'] = $contentName[0]['appointment_type'];
                $i++;
            }
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => "Individual Emergency Contact",
                    'contactData' => $contactData,
        ));
    }

}
