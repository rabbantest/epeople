<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE DOCUMENT OF INDIVIDUAL  FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Shivendra Suman.
 * CREATOR: 05/02/2015.
 * UPDATED: 05/02/2015.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

namespace Individual\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class InsuranceController extends AbstractActionController {

    public function __construct() {
        $this->_sessionObj = new Container('frontend');
    }

    public function indexAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $userId = $this->_sessionObj->userDetails['UserID'];
        $IndInsTableObj = $this->getServiceLocator()->get("IndividualInsuranceTable");
        $whereArray = array("UserID" => $userId, 'status' => 0);
        $lookingfor = "creation_date DESC";
        $columns = array();
        $itemsPerPage = 5;
        $paginator = array();
        $searchcontent= array();
          $request = $this->getRequest();
        if ($request->isPost()) {
           $postDataArr = $request->getPost()->toArray();
           $searchcontent= array('keyword'=>$postDataArr['keyword'],'status'=>0);
         //$whereArray = array("UserID" => $userId, 'status' => 0);
        }

        $insuranceData = $IndInsTableObj->getInsRecord($whereArray, $columns, $lookingfor, $searchcontent);
        $i = 0;
        foreach ($insuranceData as $typevalue) {
            $where = array('insurance_type_id' => $typevalue['coverage_type']);
            $insuranceType = $IndInsTableObj->getInsType($where);
            $insuranceData[$i]['insurance_type'] = $insuranceType[0]['insurance_type'];
            $i++;
        }
        
          if(is_array($insuranceData)){
            $iterateddata = $insuranceData;
            $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($iterateddata));
            $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
            $paginator->setItemCountPerPage($itemsPerPage);
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => "Individual Insurance",
                    'insuranceData' => $paginator,
                    'CusConPlug' => $CusConPlug,
                     'routeParams' => $searchcontent,
        ));
    }

    public function addAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $userId = $this->_sessionObj->userDetails['UserID'];
        $request = $this->getRequest();
        $IndInsTableObj = $this->getServiceLocator()->get("IndividualInsuranceTable");
        $insuranceType = $IndInsTableObj->getInsType();
        if ($request->isPost()) {

            $postDataArr = $request->getPost()->toArray();

            if (empty($postDataArr['plan_name'])) {
                $message = "Please enter plan name";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_add');
            }
            if (empty($postDataArr['group_number'])) {
                $message = "Please enter group  id";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_add');
            }
            if (empty($postDataArr['plan_code'])) {
                $message = "Please enter plan code";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_add');
            }
            if (empty($postDataArr['sucbscriber_number'])) {
                $message = "Please enter sucbscriber number";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_add');
            }
            if (empty($postDataArr['dob'])) {
                $message = "please select date";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_add');
            }
            if (empty($postDataArr['expiration_date'])) {
                $message = "please select expiration date";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_add');
            }
            if (empty($postDataArr['phone'])) {
                $message = "Please enter phone number";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_add');
            }
            if (empty($postDataArr['email'])) {
                $message = "Please enter email name";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_add');
            }

            $dataArray = array(
                'UserID' => $userId,
                'plan_name' => $postDataArr['plan_name'],
                'coverage_type' => $postDataArr['coverage_type'],
                'group_number' => $postDataArr['group_number'],
                'plan_code' => $postDataArr['plan_code'],
                'sucbscriber_name' => $postDataArr['sucbscriber_name'],
                'sucbscriber_number' => $postDataArr['sucbscriber_number'],
                'dob' => date("Y-m-d", strtotime($postDataArr['dob'])),
                'expiration_date' => date("Y-m-d", strtotime($postDataArr['expiration_date'])),
                'phone' => $postDataArr['phone'],
                'email' => $postDataArr['email'],
                'alternate_email' => $postDataArr['alternate_email'],
                'address' => $postDataArr['address'],
                'creation_date' => round(microtime(true) * 1000)
            );
            $insdata = $IndInsTableObj->insertIns($dataArray);
            if ($insdata) {
                $message = "Insurance added successfully.";
                $this->flashMessenger()->addMessage(array('success' => $message));
                return $this->redirect()->toRoute('indiv_insurance');
            } else {
                $message = "Some error occured please tray again.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_add');
            }
        }
        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => "Individual Insurance",
                    'insuranceType' => $insuranceType
        ));
    }

    public function editAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }

        $userId = $this->_sessionObj->userDetails['UserID'];
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $request = $this->getRequest();
        $IndInsTableObj = $this->getServiceLocator()->get("IndividualInsuranceTable");
        $insuranceType = $IndInsTableObj->getInsType();
       // echo "id".$Id = $this->params()->fromQuery('id');
        $Id = $this->params('id');
       
        $insuranceId = $CusConPlug->decryptdata($Id); // insurance id decode

        if (!empty($insuranceId)) {
            $whereArray = array("UserID" => $userId, 'insurance_id' => $insuranceId, 'status' => 0);
            $insuranceData = $IndInsTableObj->getInsRecord($whereArray);
            if (count($insuranceData) <= 0) {
                //send error if insurance id not found
                $this->getResponse()->setStatusCode(404);
                return;
            }
        } else {
            //send error if insurance id not found
            $this->getResponse()->setStatusCode(404);
            return;
        }
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            if (empty($postDataArr['plan_name'])) {
                $message = "Please enter plan name";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_edit');
            }
            if (empty($postDataArr['group_number'])) {
                $message = "Please enter group  id";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_edit');
            }
            if (empty($postDataArr['plan_code'])) {
                $message = "Please enter plan code";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_edit');
            }
            if (empty($postDataArr['sucbscriber_number'])) {
                $message = "Please enter sucbscriber number";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_edit');
            }
            if (empty($postDataArr['dob'])) {
                $message = "please select date";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_edit');
            }
            if (empty($postDataArr['expiration_date'])) {
                $message = "please select expiration date";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_edit');
            }
            if (empty($postDataArr['phone'])) {
                $message = "Please enter phone number";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_edit');
            }
            if (empty($postDataArr['email'])) {
                $message = "Please enter email name";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_edit');
            }
            $whereInsArray = array('UserID' => $userId, 'insurance_id' => $insuranceId);
            $dataArray = array(
                'plan_name' => $postDataArr['plan_name'],
                'coverage_type' => $postDataArr['coverage_type'],
                'group_number' => $postDataArr['group_number'],
                'plan_code' => $postDataArr['plan_code'],
                'sucbscriber_name' => $postDataArr['sucbscriber_name'],
                'sucbscriber_number' => $postDataArr['sucbscriber_number'],
                'dob' => date("Y-m-d", strtotime($postDataArr['dob'])),
                'expiration_date' => date("Y-m-d", strtotime($postDataArr['expiration_date'])),
                'phone' => $postDataArr['phone'],
                'email' => $postDataArr['email'],
                'alternate_email' => $postDataArr['alternate_email'],
                'address' => $postDataArr['address'],
                'updation_date' => round(microtime(true) * 1000)
            );
            $upddata = $IndInsTableObj->updateIns($whereInsArray, $dataArray);
            if ($upddata) {
                $message = "Insurance update successfully.";
                $this->flashMessenger()->addMessage(array('success' => $message));
                return $this->redirect()->toRoute('indiv_insurance');
            } else {
                $message = "Some error occured please tray again.";
                $this->flashMessenger()->addMessage(array('error' => $message));
                return $this->redirect()->toRoute('indiv_insurance_add');
            }
        }

        $viewModel = new ViewModel();
        return $viewModel->setVariables(array(
                    'pageTitle' => "Individual Insurance",
                    'insuranceType' => $insuranceType,
                    'insuranceData' => $insuranceData
        ));
    }

    public function viewAction() {
        if (!$this->_sessionObj->isWeblogin) {
            return $this->redirect()->toUrl('/individual/index/login');
        }

        $userId = $this->_sessionObj->userDetails['UserID'];
        $CusConPlug = $this->CustomControllerPlugin();  // custome plugin
        $request = $this->getRequest();
        $IndInsTableObj = $this->getServiceLocator()->get("IndividualInsuranceTable");
        $insuranceType = $IndInsTableObj->getInsType();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
            $whereArray = array("UserID" => $userId, 'insurance_id' => $postDataArr['insurance_id'], 'status' => 0);
            $insuranceData = $IndInsTableObj->getInsRecord($whereArray);
            if (count($insuranceData) > 0) {
                $where = array('insurance_type_id' => $insuranceData[0]['coverage_type']);
                $insuranceType = $IndInsTableObj->getInsType($where);
                $insuranceData[0]['insurance_type'] = $insuranceType[0]['insurance_type'];
            }
        }
        $viewModel = new ViewModel();
         $viewModel->setVariables(array(
                    'pageTitle' => "Individual Insurance",
                  //  'insuranceType' => $insuranceType,
                    'insuranceData' => $insuranceData
        ))
                ->setTerminal(true);
          return $viewModel;
        
    }

}
