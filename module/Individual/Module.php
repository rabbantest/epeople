<?php

namespace Individual;

/* * ***********************
 * PAGE: USE TO MANAGE THE MODULE CONFIG FOR APPLICATION.
 * #COPYRIGHT: APPSTUDIOZ
 * @AUTHOR: Rabban Ahmad
 * CREATOR: 09/11/2014.
 * UPDATED: 29/12/2014.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
/*
 * import all  model  
 */
use Individual\Model\IndividualUsersTable;
use Individual\Model\ContactInfoTable;
use Individual\Model\ProfileInfoTable;
use Individual\Model\CountryTable;
use Individual\Model\CountyTable;
use Individual\Model\IndStateTable;
use Individual\Model\IndividualHeightRecordTable;
use Individual\Model\IndividualWeightRecordTable;
use Individual\Model\IndividualWaistRecordTable;
use Individual\Model\IndividualPhysicalActivityTable;
use Individual\Model\IndividualBmiRecordTable;
use Individual\Model\IndividualCholesterolRecordTable;
use Individual\Model\IndividualBloodPressureRecordTable;
use Individual\Model\IndividualBloodGlucoseRecordTable;
use Individual\Model\IndividualTemperatureRecordTable;
use Individual\Model\IndividualSleepRecordTable;
use Individual\Model\IndividualWeightTargetTable;
use Individual\Model\IndividualFoodDrinkTable;
use Individual\Model\IndividualConditionTable;
use Individual\Model\IndividualSymtomsTable;
use Individual\Model\IndividualTreatmentTable;
use Individual\Model\IndividualUsersConditionTable;
use Individual\Model\IndividualUsersSymtomsTable;
use Individual\Model\IndividualUsersTreatmentsTable;
use Individual\Model\IndividualUsersOtherSymtomsTable;
use Individual\Model\IndividualUsersOtherTreatmentsTable;
use Individual\Model\IndividualOtherTreatmentsTable;
use Individual\Model\IndividualOtherSymtomsTable;
use Individual\Model\IndividualMoodRecordTable;
use Individual\Model\IndividualUsersImunizationTable;
use Individual\Model\IndividualUserCheckupTable;
use Individual\Model\IndividualBiometricsRecordTable;
use Individual\Model\IndividualFollowerUsersTable;
use Individual\Model\IndividualActivityStageTable;
use Individual\Model\IndividualPrivacySettingsTable;
use Individual\Model\IndividualDocumentTable;
use Individual\Model\IndividualInsuranceTable;
use Individual\Model\IndividualEmergencycontactTable;
use Individual\Model\IndividualAppointmentTable;
use Individual\Model\IndividualCaregiverTable;
use Individual\Model\IndividualLocationTable;
use Individual\Model\IndividualBlogTable;
use Individual\Model\IndividualBeconTable;
use Individual\Model\IndividualAssessmentTable;
use Individual\Model\IndividualAssesFormulaTable;
use Individual\Model\IndividualStudyTable;
use Individual\Model\IndividualStudyQuestion;
use Individual\Model\IndividualNotificationTable;
use Individual\Model\IndividualRewardTable;
use Individual\Model\IndividualDataSourceTable;
use Individual\Model\IndResearcherTable;
use Individual\Model\IndividualGroupQuestAnsTable;
use Zend\Session\Container;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        date_default_timezone_set('UTC');
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, function($e) {
            $result = $e->getResult();
            $result->setTerminal(TRUE);
        });

        //$this -> initAcl($e);
        //$e -> getApplication() -> getEventManager() -> attach('route', array($this, 'checkAcl'));
        $sessionObj = new Container('frontend');
        $siteURL = $_SERVER['HTTP_HOST'];
        $siteArray = explode('.', $siteURL);
        if (strtolower($siteArray[0]) == 'www') {
            $siteDomain = $siteArray[1];
        } else {
            $siteDomain = $siteArray[0];
        }
        $ValidDomain = $this->GetValidateDomain($e->getApplication()->getServiceManager());
        //$ValidDomain=1;
        if (($sessionObj->isWeblogin && ($siteDomain != $sessionObj->domain)) || !$ValidDomain) {
            $sessionObj->getManager()->getStorage()->clear('frontend');
            $url = $e->getRouter()->assemble(array(), array('name' => 'login'));
            $response = $e->getResponse();
            if ($ValidDomain)
                $response->getHeaders()->addHeaderLine('Location', $url);
            else
                $response->getHeaders()->addHeaderLine('Location', Host);
            $response->setStatusCode(302);
            $response->sendHeaders();

            // When an MvcEvent Listener returns a Response object,
            // It automatically short-circuit the Application running 
            // -> true only for Route Event propagation see Zend\Mvc\Application::run
            // To avoid additional processing
            // we can attach a listener for Event Route with a high priority
            $stopCallBack = function($event) use ($response) {
                $event->stopPropagation();
                return $response;
            };
            //Attach the "break" as a listener with a high priority
            $e->getApplication()->getEventManager()->attach(MvcEvent::EVENT_ROUTE, $stopCallBack, -10000);
            return $response;
        }
        /* $e->getApplication()->getEventManager()->attach(MvcEvent::EVENT_DISPATCH,
          function($e){
          $routInfo = $e->getRouteMatch();
          echo"<pre>";
          print_r($routInfo);
          exit;
          }
          ); */
    }

    public function GetValidateDomain($Sevice_Locator) {
        $newClass = new \Individual\Controller\Plugin\CustomControllerPlugin();
        $isDomainValid = $newClass->ValidateDomainIdentity($Sevice_Locator);
        return $isDomainValid;
    }

    public function initAcl(MvcEvent $e) {

        $acl = new \Zend\Permissions\Acl\Acl();
        $roles = include __DIR__ . '/config/module.acl.roles.php';
        $allResources = array();
        foreach ($roles as $role => $resources) {

            $role = new \Zend\Permissions\Acl\Role\GenericRole($role);
            $acl->addRole($role);
            $allResources = array_merge($resources, $allResources);
            //adding resources
            foreach ($resources as $resource) {
                // Edit 4
                if (!$acl->hasResource($resource))
                    $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
            }
            //adding restrictions
            foreach ($allResources as $resource) {
                $acl->allow($role, $resource);
            }
        }
        //setting to view
        $e->getViewModel()->acl = $acl;
    }

    public function checkAcl(MvcEvent $e) {
        $route = $e->getRouteMatch()->getMatchedRouteName();
        //you set your role
        $userRole = 'Individual';
        if (!$e->getViewModel()->acl->isAllowed($userRole, $route)) {
            $response = $e->getResponse();
            //location to page or what ever
            $response->getHeaders()->addHeaderLine('Location', $e->getRequest()->getBaseUrl() . '/404');
            $response->setStatusCode(404);
        }
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * This method returns an array of factories that are all merged together by the ModuleManager before passing to the ServiceManager
     * We also tell the ServiceManager that an UsersTableGateway is created by getting a Zend\Db\Adapter\Adapter (also from the ServiceManager) and using it to create a TableGateway object.
     */
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'IndividualUsersTable' => function ($serviceManager) {
            return new IndividualUsersTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualHeightRecordTable' => function ($serviceManager) {
            return new IndividualHeightRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualWeightRecordTable' => function ($serviceManager) {
            return new IndividualWeightRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualWaistRecordTable' => function ($serviceManager) {
            return new IndividualWaistRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualPhysicalActivityTable' => function ($serviceManager) {
            return new IndividualPhysicalActivityTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'ContactInfoTable' => function ($serviceManager) {
            return new ContactInfoTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'ProfileInfoTable' => function ($serviceManager) {
            return new ProfileInfoTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'CountryTable' => function ($serviceManager) {
            return new CountryTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndStateTable' => function ($serviceManager) {
            return new IndStateTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'CountyTable' => function ($serviceManager) {
            return new CountyTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualBmiRecordTable' => function ($serviceManager) {
            return new IndividualBmiRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualCholesterolRecordTable' => function ($serviceManager) {
            return new IndividualCholesterolRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualBloodPressureRecordTable' => function ($serviceManager) {
            return new IndividualBloodPressureRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualBloodGlucoseRecordTable' => function ($serviceManager) {
            return new IndividualBloodGlucoseRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualTemperatureRecordTable' => function ($serviceManager) {
            return new IndividualTemperatureRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualSleepRecordTable' => function ($serviceManager) {
            return new IndividualSleepRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualWeightTargetTable' => function ($serviceManager) {
            return new IndividualWeightTargetTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualFoodDrinkTable' => function ($serviceManager) {
            return new IndividualFoodDrinkTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualConditionTable' => function ($serviceManager) {
            return new IndividualConditionTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualSymtomsTable' => function ($serviceManager) {
            return new IndividualSymtomsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualTreatmentTable' => function ($serviceManager) {
            return new IndividualTreatmentTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualUsersConditionTable' => function ($serviceManager) {
            return new IndividualUsersConditionTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualUsersSymtomsTable' => function ($serviceManager) {
            return new IndividualUsersSymtomsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualUsersTreatmentsTable' => function ($serviceManager) {
            return new IndividualUsersTreatmentsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualUsersOtherSymtomsTable' => function ($serviceManager) {
            return new IndividualUsersOtherSymtomsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualUsersOtherTreatmentsTable' => function ($serviceManager) {
            return new IndividualUsersOtherTreatmentsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualOtherTreatmentsTable' => function ($serviceManager) {
            return new IndividualOtherTreatmentsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualOtherSymtomsTable' => function ($serviceManager) {
            return new IndividualOtherSymtomsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualMoodRecordTable' => function ($serviceManager) {
            return new IndividualMoodRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualUsersImunizationTable' => function ($serviceManager) {
            return new IndividualUsersImunizationTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualUserCheckupTable' => function ($serviceManager) {
            return new IndividualUserCheckupTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualBiometricsRecordTable' => function ($serviceManager) {
            return new IndividualBiometricsRecordTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualFollowerUsersTable' => function ($serviceManager) {
            return new IndividualFollowerUsersTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualActivityStageTable' => function ($serviceManager) {
            return new IndividualActivityStageTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualPrivacySettingsTable' => function ($serviceManager) {
            return new IndividualPrivacySettingsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualDocumentTable' => function ($serviceManager) {
            return new IndividualDocumentTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualInsuranceTable' => function ($serviceManager) {
            return new IndividualInsuranceTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualEmergencycontactTable' => function ($serviceManager) {
            return new IndividualEmergencycontactTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualAppointmentTable' => function ($serviceManager) {
            return new IndividualAppointmentTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualCaregiverTable' => function ($serviceManager) {
            return new IndividualCaregiverTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'LocationTable' => function ($serviceManager) {
            return new IndividualLocationTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'BeconTableObj' => function ($serviceManager) {
            return new IndividualBeconTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualBlogTable' => function ($serviceManager) {
            return new IndividualBlogTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualAssessmentTable' => function ($serviceManager) {
            return new IndividualAssessmentTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualAssesFormulaTable' => function ($serviceManager) {
            return new IndividualAssesFormulaTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'StudyTable' => function ($serviceManager) {
            return new IndividualStudyTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'StudyQues' => function ($serviceManager) {
            return new IndividualStudyQuestion($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualNotificationTable' => function ($serviceManager) {
            return new IndividualNotificationTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndividualRewardTable' => function ($serviceManager) {
            return new IndividualRewardTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'DataSource' => function ($serviceManager) {
            return new IndividualDataSourceTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                'IndResearcherTable' => function ($serviceManager) {
            return new IndResearcherTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                 'IndGrAns' => function ($serviceManager) {
            return new IndividualGroupQuestAnsTable($serviceManager->get('Zend\Db\Adapter\Adapter'));
        },
                
            ),
        );
    }

    /**
     * @param MvcEvent $e
     * @return null|\Zend\Http\PhpEnvironment\Response
     */
    public function postProcess(MvcEvent $e) {
        $routeMatch = $e->getRouteMatch();
        $formatter = $routeMatch->getParam('formatter', false);

        /** @var \Zend\Di\Di $di */
        $di = $e->getTarget()->getServiceLocator()->get('di');

        if ($formatter !== false) {
            if ($e->getResult() instanceof \Zend\View\Model\ViewModel) {
                if (is_array($e->getResult()->getVariables())) {
                    $vars = $e->getResult()->getVariables();
                } else {
                    $vars = null;
                }
            } else {
                $vars = $e->getResult();
            }

            /** @var PostProcessor\AbstractPostProcessor $postProcessor */
            $postProcessor = $di->get($formatter . '-pp', array(
                'response' => $e->getResponse(),
                'vars' => $vars,
            ));

            $postProcessor->process();

            return $postProcessor->getResponse();
        }

        return null;
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}
