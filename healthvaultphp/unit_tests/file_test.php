<?php
require_once "PHPUnit/Framework.php";
require_once "../../health_vault_library.php";

/**
 * Test class for File class
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Tests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class FileTest extends PHPUnit_Framework_TestCase
{
	
	/**
	 * The file object to be tested
	 *
	 * @var File 
	 *
	 */
	protected $file                = null;
	/**
	 * The file path to a valid file
	 * 
	 * @var string
	 */
	protected $validFileName       = null;
	
	/**
	 * The path to a file that cannot be written
	 *
	 * @var string 
	 *
	 */
	protected $unwritableFileName  = null;
	
	/**
	 * The path to a file that cannot be read
	 *
	 * @var string 
	 *
	 */
	protected $unreadableFileName  = null;
	
	/**
	 * The path to a file that does not exist
	 *
	 * @var string 
	 *
	 */
	protected $nonexistantFileName = null;
	
	const VALID_FILE_CONTENTS = 'Pellentesque sit amet felis a nisi pellentesque fermentum.';
	const INITIAL_FILE_CONTENTS = 'Initial foo';
	const INITIAL_FILE_NAME = 'init.bar';
	
	public function setUp()
	{
		// first if block could be avoided with sys_get_temp_dir, but that wasn't added until
		// PHP 5.2.1; this is done to keep the version requirement lower
		if(false !== ($this->validFileName = 
				tempnam('nonexistantDirToCauseSystemTempToBeUsed','test')))
		{
			file_put_contents($this->validFileName, self::VALID_FILE_CONTENTS);
			$this->unwritableFileName = tempnam('nonexistantDirToCauseSystemTempToBeUsed','test');
			chmod($this->unwritableFileName, 0444);
			if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')
			{
				$this->unreadableFileName = tempnam('nonexistantDirToCauseSystemTempToBeUsed','test');
				chmod($this->unreadableFileName, 0200);
			}
		}
		else
		{
			if(false !== ($this->validFileName = 
					tempnam('.','test')))
			{
				file_put_contents($this->validFileName, self::VALID_FILE_CONTENTS);
				$this->unwritableFileName = tempnam('.','test');
				chmod($this->unwritableFileName, 0444);
				if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')
				{
					$this->unreadableFileName = tempnam('.','test');
					chmod($this->unreadableFileName, 0200);
				}
				
			}
		}
		if($this->validFileName != null)
		{
			$this->nonexistantFileName = tempnam(dirname($this->validFileName), 'test');
			unlink($this->nonexistantFileName);
		}
		$this->file = new File(self::INITIAL_FILE_NAME, self::INITIAL_FILE_CONTENTS);
	}
	
	public function tearDown()
	{
		if($this->validFileName != null && file_exists($this->validFileName))
		{
			unlink($this->validFileName);
		}
		if($this->unwritableFileName != null && file_exists($this->unwritableFileName))
		{
			chmod($this->unwritableFileName, 0700);
			unlink($this->unwritableFileName);
		}
		if($this->unreadableFileName != null && file_exists($this->unreadableFileName))
		{
			unlink($this->unreadableFileName);
		}
	}
	
	// get tests
	
	public function testGetFileNameByMethod()
	{
		$this->assertEquals(self::INITIAL_FILE_NAME, $this->file->getFileName());
	}
	
	public function testGetFileNameByProperty1()
	{
		$this->assertEquals(self::INITIAL_FILE_NAME, $this->file->fileName);
	}
	
	public function testGetFileNameByProperty2()
	{
		$this->assertEquals(self::INITIAL_FILE_NAME, $this->file->FileName);
	}
	
	public function testGetDataByMethod()
	{
		$this->assertEquals(self::INITIAL_FILE_CONTENTS, $this->file->getData());
	}
	
	public function testGetDataByProperty1()
	{
		$this->assertEquals(self::INITIAL_FILE_CONTENTS, $this->file->data);
	}
	
	public function testGetDataByProperty2()
	{
		$this->assertEquals(self::INITIAL_FILE_CONTENTS, $this->file->Data);
	}
	// set tests
	
	public function testSetFileNameByMethodAndReturnValue()
	{
		$returnValue = $this->file->setFileName($this->validFileName);
		$this->assertEquals($this->validFileName, $this->file->fileName);
		$this->assertEquals(self::INITIAL_FILE_NAME, $returnValue);
	}
	
	public function testSetFileNameByMethodNull()
	{
		$this->file->setFileName(null);
		$this->assertEquals('', $this->file->fileName);
	}
	
	public function testSetFileNameByMethodArray()
	{
		$this->file->setFileName(array());
		$this->assertEquals('Array', $this->file->fileName);
	}	
	
	public function testSetFileNameByProperty1()
	{
		$this->file->fileName = ($this->validFileName);
		$this->assertEquals($this->validFileName, $this->file->fileName);
	}
	
	public function testSetFileNameByProperty1Null()
	{
		$this->file->fileName = (null);
		$this->assertEquals('', $this->file->fileName);
	}
	
	public function testSetFileNameByProperty1Array()
	{
		$this->file->fileName = (array());
		$this->assertEquals('Array', $this->file->fileName);
	}	
	
	public function testSetFileNameByProperty2()
	{
		$this->file->FileName = ($this->validFileName);
		$this->assertEquals($this->validFileName, $this->file->fileName);
	}
	
	public function testSetFileNameByProperty2Null()
	{
		$this->file->FileName = (null);
		$this->assertEquals('', $this->file->fileName);
	}
	
	public function testSetFileNameByProperty2Array()
	{
		$this->file->FileName = (array());
		$this->assertEquals('Array', $this->file->fileName);
	}
	
	public function testSetDataByMethodAndReturnValue()
	{
		$returnValue = $this->file->setData(self::VALID_FILE_CONTENTS);
		$this->assertEquals(self::VALID_FILE_CONTENTS, $this->file->data);
		$this->assertEquals(self::INITIAL_FILE_CONTENTS, $returnValue);
	}
	
	public function testSetDataByMethodNull()
	{
		$this->file->setData(null);
		$this->assertEquals('', $this->file->data);
	}
	
	public function testSetDataByMethodArray()
	{
		$this->file->setData(array());
		$this->assertEquals('Array', $this->file->data);
	}	
	
	public function testSetDataByProperty1()
	{
		$this->file->data = (self::VALID_FILE_CONTENTS);
		$this->assertEquals(self::VALID_FILE_CONTENTS, $this->file->data);
	}
	
	public function testSetDataByProperty1Null()
	{
		$this->file->data = (null);
		$this->assertEquals('', $this->file->data);
	}
	
	public function testSetDataByProperty1Array()
	{
		$this->file->data = (array());
		$this->assertEquals('Array', $this->file->data);
	}	
	
	public function testSetDataByProperty2()
	{
		$this->file->Data = (self::VALID_FILE_CONTENTS);
		$this->assertEquals(self::VALID_FILE_CONTENTS, $this->file->data);
	}
	
	public function testSetDataByProperty2Null()
	{
		$this->file->Data = (null);
		$this->assertEquals('', $this->file->data);
	}
	
	public function testSetDataByProperty2Array()
	{
		$this->file->Data = (array());
		$this->assertEquals('Array', $this->file->data);
	}
	
	// method tests
	
	public function testReadInitialSettings()
	{
		$this->assertEquals(self::INITIAL_FILE_CONTENTS, $this->file->read());
	}
	
	public function testReadValidFile()
	{
		if($this->validFileName == null)
		{
			$this->markTestSkipped('Valid test file was not created');
		}
		
		$file = new File($this->validFileName);
		$this->assertEquals(self::VALID_FILE_CONTENTS, $file->read());
	}
	
	/**
	 * @expectedException FileNotReadableException
	 */
	public function testReadUnreadableFile()
	{
		if($this->unreadableFileName == null)
		{
			$this->markTestSkipped('Unreadable file not created');
		}
		$file = new File($this->unreadableFileName);
		$file->read();
	}
	
	public function testReadUnwritableFile()
	{
		if($this->unwritableFileName == null)
		{
			$this->markTestSkipped('Unwritable file not created');
		}
		$file = new File($this->unwritableFileName);
		$this->assertEquals('', $file->read());
	}
	
	/**
	 * @expectedException FileNotFoundException
	 */
	public function testReadNonexistantFile()
	{
		$file = new File($this->nonexistantFileName);
		$file->read();
	}
	// TODO: determine a way to test FileIOExceptions 
	//       e.g. (failing to read though the file is readable, etc)
	
	public function testDeleteValidFile()
	{
		if($this->validFileName == null)
		{
			$this->markTestSkipped('No valid file to test');
		}
		$file = new File($this->validFileName);
		$this->assertEquals(true, $file->delete());
		$this->assertEquals(false, file_exists($this->validFileName));
	}

	public function testDeleteUnreadableFile()
	{
		if($this->unreadableFileName == null)
		{
			$this->markTestSkipped('No unreadable file to test');
		}
		$file = new File($this->unreadableFileName);
		$this->assertEquals(true, $file->delete());
		$this->assertEquals(false, file_exists($this->unreadableFileName));
	}
	
	/**
	 * @expectedException FileNotWritableException
	 */
	public function testDeleteUnwritableFile()
	{
		if($this->unwritableFileName == null)
		{
			$this->markTestSkipped('No unwritable file to test');
		}
		$file = new File($this->unwritableFileName);
		$file->delete();
	}
	
	/**
	 * @expectedException FileNotFoundException
	 */
	public function testDeleteNonexistantFile()
	{
		$file = new File($this->nonexistantFileName);
		$file->delete();
	}
	
	public function testWriteValidFileAndReadBack()
	{
		if($this->validFileName == null)
		{
			$this->markTestSkipped('No valid file to test');
		}
		$data = "Lorem ipsom...";
		$file = new File($this->validFileName, $data);
		$this->assertEquals(true, $file->save());
		$file2 = new File($this->validFileName);
		$this->assertEquals($data, $file2->read());
	}
	
	public function testWriteUnreadableFile()
	{
		if($this->unreadableFileName == null)
		{
			$this->markTestSkipped('No unreadable file to test');
		}
		$file = new File($this->unreadableFileName, self::VALID_FILE_CONTENTS);
		$this->assertEquals(true, $file->save());
		// the file should now be readable
		$file2 = new File($this->unreadableFileName);
		$this->assertEquals(self::VALID_FILE_CONTENTS, $file2->read());
	}
	
	/**
	 * @expectedException FileNotWritableException
	 */
	public function testWriteUnwritableFile()
	{
		if($this->unwritableFileName == null)
		{
			$this->markTestSkipped('No unwritable file to test');
		}
		$file = new File($this->unwritableFileName, self::VALID_FILE_CONTENTS);
		$file->save();
	}
	
	public function testWriteNonexistantFile()
	{
		if($this->validFileName == null)
		{
			$this->markTestSkipped('Nowhere to write the file');
		}
		$file = new File($this->nonexistantFileName, self::VALID_FILE_CONTENTS);
		$this->assertEquals(true, $file->save());
		$file2 = new File($this->nonexistantFileName);
		$this->assertEquals(self::VALID_FILE_CONTENTS, $file2->read());
		$this->assertEquals(true, $file->delete());
		$this->assertEquals(false, file_exists($this->nonexistantFileName));
	}	
	
	public function testAppendValidFileAndReadBack()
	{
		if($this->validFileName == null)
		{
			$this->markTestSkipped('No valid file to test');
		}
		$file = new File($this->validFileName, self::VALID_FILE_CONTENTS);
		$this->assertEquals(true, $file->append());
		$file2 = new File($this->validFileName);
		$this->assertEquals(str_repeat(self::VALID_FILE_CONTENTS, 2), $file2->read());
	}
	
	public function testAppendUnreadableFileAndReadBack()
	{
		if($this->unreadableFileName == null)
		{
			$this->markTestSkipped('No unreadable file to test');
		}
		$file = new FileTest($this->unreadableFileName, self::INITIAL_FILE_CONTENTS);
		$this->assertEquals(true, $file->append());
		// now readable...
		$file2 = new FileTest($this->unreadableFileName);
		$this->assertEquals(self::INITIAL_FILE_CONTENTS, $file2->read());
	}
	
	/**
	 * @expectedException FileNotWritableException
	 */
	public function testAppendUnwritableFile()
	{
		if($this->unwritableFileName == null)
		{
			$this->markTestSkipped('No unwritable file to test');
		}
		$file = new File($this->unwritableFileName, self::INITIAL_FILE_CONTENTS);
		$file->append();
	}
	
	public function testAppendNonexistantFile()
	{
		if($this->validFileName == null)
		{
			$this->markTestSkipped('No place to create the file');
		}
		$file = new File($this->nonexistantFileName, self::VALID_FILE_CONTENTS);
		$this->assertEquals(true, $file->append());
		$file2 = new File($this->nonexistantFileName);
		$this->assertEquals(self::VALID_FILE_CONTENTS, $file2->read());
		$this->assertEquals(true, $file->delete());
		$this->assertEquals(false, file_exists($this->nonexistantFileName));
	}	
	// construct tests
	
	public function testConstructBlankData()
	{
		$file = new File($this->validFileName);
		$this->assertEquals($this->validFileName, $file->fileName);
		$this->assertEquals('', $file->data);
	}
	
	public function testConstructNullData()
	{
		$file = new File($this->validFileName, null);
		$this->assertEquals($this->validFileName, $file->fileName);
		$this->assertEquals('', $file->data);
	}
	
	public function testConstructNullFilename()
	{
		$file = new File(null, self::INITIAL_FILE_CONTENTS);
		$this->assertEquals('', $file->fileName);
		$this->assertEquals(self::INITIAL_FILE_CONTENTS, $file->data);
	}
	
	public function testConstructBlank()
	{
		$file = new File();
		$this->assertEquals('', $file->fileName);
		$this->assertEquals('', $file->data);
	}
}
?>