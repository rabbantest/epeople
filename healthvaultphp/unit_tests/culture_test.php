<?php
require_once "PHPUnit/Framework.php";
require_once "../../health_vault_library.php";

/**
 * Test class for Culture class
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Tests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class CultureTest extends PHPUnit_Framework_TestCase
{
    
    /**
     * The culture to be tested
     *
     * @var Culture 
     *
     */
    protected $culture = null;
    const INITIAL_LANGUAGE = "en";
    const INITIAL_COUNTRY = "US";
    
    public function setUp()
    {
        $this->culture = new Culture(self::INITIAL_LANGUAGE, self::INITIAL_COUNTRY);
    }
    
    // get tests
    
    public function testGetLanguageByMethod()
    {
        $this->assertEquals(self::INITIAL_LANGUAGE, $this->culture->getLanguage());
    } 
    
    public function testGetLanguageByProperty1()
    {
        $this->assertEquals(self::INITIAL_LANGUAGE, $this->culture->language);
    } 
    
    public function testGetLanguageByProperty2()
    {
        $this->assertEquals(self::INITIAL_LANGUAGE, $this->culture->Language);
    }
    
    public function testGetCountryByMethod()
    {
        $this->assertEquals(self::INITIAL_COUNTRY, $this->culture->getCountry());
    } 
    
    public function testGetCountryByProperty1()
    {
        $this->assertEquals(self::INITIAL_COUNTRY, $this->culture->country);
    } 
    
    public function testGetCountryByProperty2()
    {
        $this->assertEquals(self::INITIAL_COUNTRY, $this->culture->Country);
    }
    // set tests
    
    public function testSetLanguageByMethod()
    {
        $newValue = 'ss';
        $this->culture->setLanguage($newValue);
        $this->assertEquals($newValue, $this->culture->language);
    } 
    
    public function testSetLanguageByMethodCapitalized()
    {
        $newValue = 'SS';
        $this->culture->setLanguage($newValue);
        $this->assertEquals('ss', $this->culture->language);
    } 
    
    public function testSetLanguageByMethodFirstCapitalized()
    {
        $newValue = 'Ss';
        $this->culture->setLanguage($newValue);
        $this->assertEquals('ss', $this->culture->language);
    } 
    
    public function testSetLanguageByMethodSecondCapitalized()
    {
        $newValue = 'sS';
        $this->culture->setLanguage($newValue);
        $this->assertEquals('ss', $this->culture->language);
    } 
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetLanguageByMethodInvalidLang()
    {
        $newValue = 'zz';
        $this->culture->setLanguage($newValue);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetLanguageByMethodNull()
    {
        $newValue = null;
        $this->culture->setLanguage($newValue);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetLanguageByMethodInvalidType()
    {
        $newValue = true;
        $this->culture->setLanguage($newValue);
    }
    
    public function testSetLanguageByProperty1()
    {
        $newValue = 'ss';
        $this->culture->language = ($newValue);
        $this->assertEquals($newValue, $this->culture->language);
    } 
    
    public function testSetLanguageByProperty1Capitalized()
    {
        $newValue = 'SS';
        $this->culture->language = ($newValue);
        $this->assertEquals('ss', $this->culture->language);
    } 
    
    public function testSetLanguageByProperty1FirstCapitalized()
    {
        $newValue = 'Ss';
        $this->culture->language = ($newValue);
        $this->assertEquals('ss', $this->culture->language);
    } 
    
    public function testSetLanguageByProperty1SecondCapitalized()
    {
        $newValue = 'sS';
        $this->culture->language = ($newValue);
        $this->assertEquals('ss', $this->culture->language);
    } 
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetLanguageByProperty1InvalidLang()
    {
        $newValue = 'zz';
        $this->culture->language = ($newValue);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetLanguageByProperty1Null()
    {
        $newValue = null;
        $this->culture->language = ($newValue);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetLanguageByProperty1InvalidType()
    {
        $newValue = true;
        $this->culture->language = ($newValue);
    }    
    
    public function testSetLanguageByProperty2()
    {
        $newValue = 'ss';
        $this->culture->Language = ($newValue);
        $this->assertEquals($newValue, $this->culture->language);
    } 
    
    public function testSetLanguageByProperty2Capitalized()
    {
        $newValue = 'SS';
        $this->culture->Language = ($newValue);
        $this->assertEquals('ss', $this->culture->language);
    } 
    
    public function testSetLanguageByProperty2FirstCapitalized()
    {
        $newValue = 'Ss';
        $this->culture->Language = ($newValue);
        $this->assertEquals('ss', $this->culture->language);
    } 
    
    public function testSetLanguageByProperty2SecondCapitalized()
    {
        $newValue = 'sS';
        $this->culture->Language = ($newValue);
        $this->assertEquals('ss', $this->culture->language);
    } 
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetLanguageByProperty2InvalidLang()
    {
        $newValue = 'zw';
        $this->culture->Language = ($newValue);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetLanguageByProperty2Null()
    {
        $newValue = null;
        $this->culture->Language = ($newValue);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetLanguageByProperty2InvalidType()
    {
        $newValue = true;
        $this->culture->Language = ($newValue);
    }
    
    public function testSetCountryByMethod()
    {
        $newValue = 'ZW';
        $this->culture->setCountry($newValue);
        $this->assertEquals($newValue, $this->culture->country);
    }
    
    public function testSetCountryByMethodLowerCase()
    {
        $newValue = 'zw';
        $this->culture->setCountry($newValue);
        $this->assertEquals('ZW', $this->culture->country);
    }
    
    public function testSetCountryByMethodFirstLowerCase()
    {
        $newValue = 'zW';
        $this->culture->setCountry($newValue);
        $this->assertEquals('ZW', $this->culture->country);
    }
    
    public function testSetCountryByMethodSecondLowerCase()
    {
        $newValue = 'Zw';
        $this->culture->setCountry($newValue);
        $this->assertEquals('ZW', $this->culture->country);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetCountryByMethodInvalidCountry()
    {
        $newValue = 'SS';
        $this->culture->setCountry($newValue);
    } 
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetCountryByMethodNull()
    {
        $newValue = null;
        $this->culture->setCountry($newValue);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetCountryByMethodInvalidType()
    {
        $newValue = true;
        $this->culture->setCountry($newValue);
    }    
    
    public function testSetCountryByProperty1()
    {
        $newValue = 'ZW';
        $this->culture->country = ($newValue);
        $this->assertEquals($newValue, $this->culture->country);
    }
    
    public function testSetCountryByProperty1LowerCase()
    {
        $newValue = 'zw';
        $this->culture->country = ($newValue);
        $this->assertEquals('ZW', $this->culture->country);
    }
    
    public function testSetCountryByProperty1FirstLowerCase()
    {
        $newValue = 'zW';
        $this->culture->country = ($newValue);
        $this->assertEquals('ZW', $this->culture->country);
    }
    
    public function testSetCountryByProperty1SecondLowerCase()
    {
        $newValue = 'Zw';
        $this->culture->country = ($newValue);
        $this->assertEquals('ZW', $this->culture->country);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetCountryByProperty1InvalidCountry()
    {
        $newValue = 'SS';
        $this->culture->country = ($newValue);
    } 
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetCountryByProperty1Null()
    {
        $newValue = null;
        $this->culture->country = ($newValue);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetCountryByProperty1InvalidType()
    {
        $newValue = true;
        $this->culture->country = ($newValue);
    }   
    
    public function testSetCountryByProperty2()
    {
        $newValue = 'ZW';
        $this->culture->Country = ($newValue);
        $this->assertEquals($newValue, $this->culture->country);
    }
    
    public function testSetCountryByProperty2LowerCase()
    {
        $newValue = 'zw';
        $this->culture->Country = ($newValue);
        $this->assertEquals('ZW', $this->culture->country);
    }
    
    public function testSetCountryByProperty2FirstLowerCase()
    {
        $newValue = 'zW';
        $this->culture->Country = ($newValue);
        $this->assertEquals('ZW', $this->culture->country);
    }
    
    public function testSetCountryByProperty2SecondLowerCase()
    {
        $newValue = 'Zw';
        $this->culture->Country = ($newValue);
        $this->assertEquals('ZW', $this->culture->country);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetCountryByProperty2InvalidCountry()
    {
        $newValue = 'SS';
        $this->culture->Country = ($newValue);
    } 
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetCountryByProperty2Null()
    {
        $newValue = null;
        $this->culture->Country = ($newValue);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testSetCountryByProperty2InvalidType()
    {
        $newValue = true;
        $this->culture->Country = ($newValue);
    }
    // construct tests
    
    public function testConstructNullLanguage()
    {
        $culture = new Culture(null, self::INITIAL_COUNTRY);
        $this->assertEquals(null, $culture->language);
        $this->assertEquals(self::INITIAL_COUNTRY, $culture->country);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testConstructInvalidTypeLanguage()
    {
        $culture = new Culture(true, self::INITIAL_COUNTRY);
    }
    
    public function testConstructNullCountry()
    {
        $culture = new Culture(self::INITIAL_LANGUAGE);
        $this->assertEquals(null, $culture->country);
        $this->assertEquals(self::INITIAL_LANGUAGE, $culture->language);
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testConstructInvalidTypeCountry()
    {
        $culture = new Culture(self::INITIAL_LANGUAGE, true);
    }
}
?>