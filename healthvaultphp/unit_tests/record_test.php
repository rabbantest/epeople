<?php
require_once "PHPUnit/Framework.php";
require_once "../../health_vault_library.php";

/**
 * Test class for Record class
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Tests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class RecordTest extends PHPUnit_Framework_TestCase
{
    /**
     * The internal record for testing
     */
    protected $record;
    /**
     * The Guid used for the initial record
     *
     * @var Guid 
     */
    protected $recordGuid;
    /**
     * The initial id for the record
     * 
     * @var string
     */
    const RECORD_INITIAL_ID = 'ee19ba3f-f62b-47aa-a563-d6adcb2c9cd0';
    /**
     * The initial value for whether or not the person is the custodian of the record
     * 
     * @var bool
     */
    const RECORD_INITIAL_CUSTODIAN = true;
    /**
     * The initial relation type for the record
     * 
     * @var int
     */
    const RECORD_INITIAL_REL_TYPE = 1;
    /**
     * The inital relation name for the record
     * 
     * @var string
     */
    const RECORD_INITIAL_REL_NAME = 'Self';
    /**
     * Initial expiration date for the authorization on the record
     * 
     * @var string
     */
    const RECORD_INITIAL_AUTH_EXPIRES = '9999-12-31T23:59:59.999Z';
    /**
     * Initial value for whether or not the authorization is expired
     * 
     * @var bool
     */
    const RECORD_INITIAL_AUTH_EXPIRED = false;
    /**
     * Initial display nae for the record
     * 
     * @var string
     */
    const RECORD_INITIAL_DISPLAY_NAME = 'J';
    /**
     * Initial state for the record
     * 
     * @var string
     */
    const RECORD_INITIAL_STATE = 'Active';
    /**
     * Initial setting for date created for the record
     * 
     * @var string
     */
    const RECORD_INITIAL_DATE_CREATED = '2008-06-18T17:41:53.563Z';
    /**
     * Inital setting for max size
     * 
     * @var int
     */
    const RECORD_INITIAL_MAX_SIZE_BYTES = 104857600;
    /**
     * Initial setting for size
     * 
     * @var int
     */
    const RECORD_INITIAL_SIZE_BYTES = 459;
    /**
     * Initial value
     * 
     * @var string
     */
    const RECORD_INITIAL_VALUE = 'Record Value';
    
    protected function setUp()
    {
        $this->recordGuid = new Guid(self::RECORD_INITIAL_ID);
        $this->record = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
    }
    
    public function testGetIDByMethod()
    {
        $this->assertEquals($this->recordGuid, $this->record->getID());
    }
    
    public function testGetIDByProperty1()
    {
        $this->assertEquals($this->recordGuid, $this->record->ID);
    }
    
    public function testGetIDByProperty2()
    {
        $this->assertEquals($this->recordGuid, $this->record->id);
    }
    
    public function testGetIDByProperty3()
    {
        $this->assertEquals($this->recordGuid, $this->record->Id);
    }
    
    public function testGetRecordCustodianByMethod()
    {
        $this->assertEquals(self::RECORD_INITIAL_CUSTODIAN, $this->record->getRecordCustodian());
    }
    
    public function testGetRecordCustodianByProperty1()
    {
        $this->assertEquals(self::RECORD_INITIAL_CUSTODIAN, $this->record->isRecordCustodian);
    }	
    
    public function testGetRecordCustodianByProperty2()
    {
        $this->assertEquals(self::RECORD_INITIAL_CUSTODIAN, $this->record->IsRecordCustodian);
    }	
    
    public function testGetRecordCustodianByProperty3()
    {
        $this->assertEquals(self::RECORD_INITIAL_CUSTODIAN, $this->record->recordCustodian);
    }
    
    public function testGetRecordCustodianByProperty4()
    {
        $this->assertEquals(self::RECORD_INITIAL_CUSTODIAN, $this->record->RecordCustodian);
    }
    
    public function testGetRelTypeByMethod()
    {
        $this->assertEquals(self::RECORD_INITIAL_REL_TYPE, $this->record->getRelType());
    }	
    
    public function testGetRelTypeByProperty1()
    {
        $this->assertEquals(self::RECORD_INITIAL_REL_TYPE, $this->record->relType);
    }	
    
    public function testGetRelTypeByProperty2()
    {
        $this->assertEquals(self::RECORD_INITIAL_REL_TYPE, $this->record->RelType);
    }	
    
    public function testGetRelNameByMethod()
    {
        $this->assertEquals(self::RECORD_INITIAL_REL_NAME, $this->record->getRelName());
    }
    
    public function testGetRelNameByProperty1()
    {
        $this->assertEquals(self::RECORD_INITIAL_REL_NAME, $this->record->relName);
    }
    
    public function testGetRelNameByProperty2()
    {
        $this->assertEquals(self::RECORD_INITIAL_REL_NAME, $this->record->RelName);
    }
    
    public function testGetAuthExpiresByMethod()
    {
        $this->assertEquals(self::RECORD_INITIAL_AUTH_EXPIRES, $this->record->getAuthExpires());
    }
    
    public function testGetAuthExpiresByProperty1()
    {
        $this->assertEquals(self::RECORD_INITIAL_AUTH_EXPIRES, $this->record->authExpires);
    }
    
    public function testGetAuthExpiresByProperty2()
    {
        $this->assertEquals(self::RECORD_INITIAL_AUTH_EXPIRES, $this->record->AuthExpires);
    }
    
    public function testGetAuthExpiredByMethod()
    {
        $this->assertEquals(self::RECORD_INITIAL_AUTH_EXPIRED, $this->record->getAuthExpired());
    }
    
    public function testGetAuthExpiredByProperty1()
    {
        $this->assertEquals(self::RECORD_INITIAL_AUTH_EXPIRED, $this->record->authExpired);
    }
    
    public function testGetAuthExpiredByProperty2()
    {
        $this->assertEquals(self::RECORD_INITIAL_AUTH_EXPIRED, $this->record->AuthExpired);
    }
    
    public function testGetDisplayNameByMethod()
    {
        $this->assertEquals(self::RECORD_INITIAL_DISPLAY_NAME, $this->record->getDisplayName());
    }
    
    public function testGetDisplayNameByProperty1()
    {
        $this->assertEquals(self::RECORD_INITIAL_DISPLAY_NAME, $this->record->displayName);
    }
    
    public function testGetDisplayNameByProperty2()
    {
        $this->assertEquals(self::RECORD_INITIAL_DISPLAY_NAME, $this->record->DisplayName);
    }
    
    public function testGetStateByMethod()
    {
        $this->assertEquals(self::RECORD_INITIAL_STATE, $this->record->getState());
    }
    
    public function testGetStateByProperty1()
    {
        $this->assertEquals(self::RECORD_INITIAL_STATE, $this->record->state);
    }
    
    public function testGetStateByProperty2()
    {
        $this->assertEquals(self::RECORD_INITIAL_STATE, $this->record->State);
    }
    
    public function testGetDateCreatedByMethod()
    {
        $this->assertEquals(self::RECORD_INITIAL_DATE_CREATED, $this->record->getDateCreated());
    }
    
    public function testGetDateCreatedByProperty1()
    {
        $this->assertEquals(self::RECORD_INITIAL_DATE_CREATED, $this->record->dateCreated);
    }
    
    public function testGetDateCreatedByProperty2()
    {
        $this->assertEquals(self::RECORD_INITIAL_DATE_CREATED, $this->record->DateCreated);
    }
    
    public function testGetMaxSizeBytesByMethod()
    {
        $this->assertEquals(self::RECORD_INITIAL_MAX_SIZE_BYTES,
                $this->record->getMaxSizeBytes());
    }
    
    public function testGetMaxSizeBytesByProperty1()
    {
        $this->assertEquals(self::RECORD_INITIAL_MAX_SIZE_BYTES, $this->record->maxSizeBytes);
    }
    
    public function testGetMaxSizeBytesByProperty2()
    {
        $this->assertEquals(self::RECORD_INITIAL_MAX_SIZE_BYTES, $this->record->MaxSizeBytes);
    }
    
    public function testGetSizeBytesByMethod()
    {
        $this->assertEquals(self::RECORD_INITIAL_SIZE_BYTES, $this->record->getSizeBytes());
    }
    
    public function testGetSizeBytesByProperty1()
    {
        $this->assertEquals(self::RECORD_INITIAL_SIZE_BYTES, $this->record->sizeBytes);
    }
    
    public function testGetSizeBytesByProperty2()
    {
        $this->assertEquals(self::RECORD_INITIAL_SIZE_BYTES, $this->record->SizeBytes);
    }
    
    public function testGetValueByMethod()
    {
        $this->assertEquals(self::RECORD_INITIAL_VALUE, $this->record->getValue());
    }
    
    public function testGetValueByProperty1()
    {
        $this->assertEquals(self::RECORD_INITIAL_VALUE, $this->record->value);
    }
    
    public function testGetValueByProperty2()
    {
        $this->assertEquals(self::RECORD_INITIAL_VALUE, $this->record->Value);
    }
    
    public function testSetRecordCustodianByMethod()
    {
        $newValue = false;
        $this->record->setRecordCustodian($newValue);
        $this->assertEquals($newValue, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByMethodNull()
    {
        $this->record->setRecordCustodian(null);
        $this->assertEquals(false, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByMethodInvalidType()
    {
        $this->record->setRecordCustodian(0);
        $this->assertEquals(false, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByProperty1()
    {
        $newValue = false;
        $this->record->isRecordCustodian = $newValue;
        $this->assertEquals($newValue, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByProperty1Null()
    {
        $newValue = null;
        $this->record->isRecordCustodian = $newValue;
        $this->assertEquals(false, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByProperty1InvalidType()
    {
        $newValue = 0;
        $this->record->isRecordCustodian = $newValue;
        $this->assertEquals(false, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByProperty2()
    {
        $newValue = false;
        $this->record->IsRecordCustodian = $newValue;
        $this->assertEquals($newValue, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByProperty2Null()
    {
        $newValue = null;
        $this->record->IsRecordCustodian = $newValue;
        $this->assertEquals(false, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByProperty2InvalidType()
    {
        $newValue = 0;
        $this->record->IsRecordCustodian = $newValue;
        $this->assertEquals(false, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByProperty3()
    {
        $newValue = false;
        $this->record->recordCustodian = $newValue;
        $this->assertEquals($newValue, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByProperty3Null()
    {
        $newValue = null;
        $this->record->recordCustodian = $newValue;
        $this->assertEquals(false, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByProperty3InvalidType()
    {
        $newValue = 0;
        $this->record->recordCustodian = $newValue;
        $this->assertEquals(false, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByProperty4()
    {
        $newValue = false;
        $this->record->RecordCustodian = $newValue;
        $this->assertEquals($newValue, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByProperty4Null()
    {
        $newValue = null;
        $this->record->RecordCustodian = $newValue;
        $this->assertEquals(false, $this->record->recordCustodian);
    }
    
    public function testSetRecordCustodianByProperty4InvalidType()
    {
        $newValue = 0;
        $this->record->RecordCustodian = $newValue;
        $this->assertEquals(false, $this->record->recordCustodian);
    }
    
    public function testSetRelTypeByMethod()
    {
        $newValue = 2;
        $this->record->setRelType($newValue);
        $this->assertEquals($newValue, $this->record->relType);
    }
    
    public function testSetRelTypeByMethodNull()
    {
        $newValue = null;
        $this->record->setRelType($newValue);
        $this->assertEquals(0, $this->record->relType);
    }
    
    public function testSetRelTypeByMethodInvalidType()
    {
        $newValue = false;
        $this->record->setRelType($newValue);
        $this->assertEquals(0, $this->record->relType);
    }
    
    public function testSetRelTypeByProperty1()
    {
        $newValue = 2;
        $this->record->relType = $newValue;
        $this->assertEquals($newValue, $this->record->relType);
    }
    
    public function testSetRelTypeByProperty1Null()
    {
        $newValue = null;
        $this->record->relType = $newValue;
        $this->assertEquals(0, $this->record->relType);
    }
    
    public function testSetRelTypeByProperty1InvalidType()
    {
        $newValue = false;
        $this->record->relType = $newValue;
        $this->assertEquals(0, $this->record->relType);
    }
    
    public function testSetRelTypeByProperty2()
    {
        $newValue = 2;
        $this->record->RelType = $newValue;
        $this->assertEquals($newValue, $this->record->relType);
    }
    
    public function testSetRelTypeByProperty2Null()
    {
        $newValue = null;
        $this->record->RelType = $newValue;
        $this->assertEquals(0, $this->record->relType);
    }
    
    public function testSetRelTypeByProperty2InvalidType()
    {
        $newValue = false;
        $this->record->RelType = $newValue;
        $this->assertEquals(0, $this->record->relType);
    }
    
    public function testSetRelNameByMethod()
    {
        $newValue = 'Not Self';
        $this->record->setRelName($newValue);
        $this->assertEquals($newValue, $this->record->relName);
    }
    
    public function testSetRelNameByMethodNull()
    {
        $newValue = null;
        $this->record->setRelName($newValue);
        $this->assertEquals('', $this->record->relName);
    }
    
    public function testSetRelNameByMethodInvalidType()
    {
        $newValue = false;
        $this->record->setRelName($newValue);
        $this->assertEquals('', $this->record->relName);
    }
    
    public function testSetRelNameByProperty1()
    {
        $newValue = 'Not Self';
        $this->record->relName = $newValue;
        $this->assertEquals($newValue, $this->record->relName);
    }
    
    public function testSetRelNameByProperty1Null()
    {
        $newValue = null;
        $this->record->relName = $newValue;
        $this->assertEquals('', $this->record->relName);
    }
    
    public function testSetRelNameByProperty1InvalidType()
    {
        $newValue = false;
        $this->record->relName = $newValue;
        $this->assertEquals('', $this->record->relName);
    }
    
    public function testSetRelNameByProperty2()
    {
        $newValue = 'Not Self';
        $this->record->RelName = $newValue;
        $this->assertEquals($newValue, $this->record->relName);
    }
    
    public function testSetRelNameByProperty2Null()
    {
        $newValue = null;
        $this->record->RelName = $newValue;
        $this->assertEquals('', $this->record->relName);
    }
    
    public function testSetRelNameByProperty2InvalidType()
    {
        $newValue = false;
        $this->record->RelName = $newValue;
        $this->assertEquals('', $this->record->relName);
    }
    
    public function testSetAuthExpiresByMethod()
    {
        $newValue = '2099-12-31T23:59:59.00Z';
        $this->record->setAuthExpires($newValue);
        $this->assertEquals($newValue, $this->record->authExpires);
    }
    
    public function testSetAuthExpiresByMethodNull()
    {
        $newValue = null;
        $this->record->setAuthExpires($newValue);
        $this->assertEquals('', $this->record->authExpires);
    }
    
    public function testSetAuthExpiresByMethodInvalidType()
    {
        $newValue = false;
        $this->record->setAuthExpires($newValue);
        $this->assertEquals('', $this->record->authExpires);
    }
    
    public function testSetAuthExpiresByProperty1()
    {
        $newValue = '2099-12-31T23:59:59.00Z';
        $this->record->authExpires = $newValue;
        $this->assertEquals($newValue, $this->record->authExpires);
    }
    
    public function testSetAuthExpiresByProperty1Null()
    {
        $newValue = null;
        $this->record->authExpires = $newValue;
        $this->assertEquals('', $this->record->authExpires);
    }
    
    public function testSetAuthExpiresByProperty1InvalidType()
    {
        $newValue = false;
        $this->record->authExpires = $newValue;
        $this->assertEquals('', $this->record->authExpires);
    }
    
    public function testSetAuthExpiresByProperty2()
    {
        $newValue = '2099-12-31T23:59:59.00Z';
        $this->record->AuthExpires = $newValue;
        $this->assertEquals($newValue, $this->record->authExpires);
    }
    
    public function testSetAuthExpiresByProperty2Null()
    {
        $newValue = null;
        $this->record->AuthExpires = $newValue;
        $this->assertEquals('', $this->record->authExpires);
    }
    
    public function testSetAuthExpiresByProperty2InvalidType()
    {
        $newValue = false;
        $this->record->AuthExpires = $newValue;
        $this->assertEquals('', $this->record->authExpires);
    }
    
    public function testSetAuthExpiredByMethod()
    {
        $newValue = true;
        $this->record->setAuthExpired($newValue);
        $this->assertEquals($newValue, $this->record->authExpired);
    }
    
    public function testSetAuthExpiredByMethodNull()
    {
        $newValue = null;
        $this->record->setAuthExpired($newValue);
        $this->assertEquals(false, $this->record->authExpired);
    }
    
    public function testSetAuthExpiredByMethodInvalidType()
    {
        $newValue = 28;
        $this->record->setAuthExpired($newValue);
        $this->assertEquals(true, $this->record->authExpired);
    }    
    
    public function testSetAuthExpiredProperty1()
    {
        $newValue = true;
        $this->record->authExpired = ($newValue);
        $this->assertEquals($newValue, $this->record->authExpired);
    }
    
    public function testSetAuthExpiredProperty1Null()
    {
        $newValue = null;
        $this->record->authExpired = ($newValue);
        $this->assertEquals(false, $this->record->authExpired);
    }
    
    public function testSetAuthExpiredProperty1InvalidType()
    {
        $newValue = 28;
        $this->record->authExpired = ($newValue);
        $this->assertEquals(true, $this->record->authExpired);
    } 
    
    public function testSetAuthExpiredProperty2()
    {
        $newValue = true;
        $this->record->AuthExpired = ($newValue);
        $this->assertEquals($newValue, $this->record->authExpired);
    }
    
    public function testSetAuthExpiredProperty2Null()
    {
        $newValue = null;
        $this->record->AuthExpired = ($newValue);
        $this->assertEquals(false, $this->record->authExpired);
    }
    
    public function testSetAuthExpiredProperty2InvalidType()
    {
        $newValue = 28;
        $this->record->AuthExpired = ($newValue);
        $this->assertEquals(true, $this->record->authExpired);
    }
    
    public function testSetDisplayNameByMethod()
    {
        $newValue = 'Michael';
        $this->record->setDisplayName($newValue);
        $this->assertEquals($newValue, $this->record->displayName);
    }
    
    public function testSetDisplayNameByMethodNull()
    {
        $newValue = null;
        $this->record->setDisplayName($newValue);
        $this->assertEquals('', $this->record->displayName);
    }
    
    public function testSetDisplayNameByMethodInvalidType()
    {
        $newValue = false;
        $this->record->setDisplayName($newValue);
        $this->assertEquals('', $this->record->displayName);
    }
    
    public function testSetDisplayNameByProperty1()
    {
        $newValue = 'Michael';
        $this->record->displayName = $newValue;
        $this->assertEquals($newValue, $this->record->displayName);
    }
    
    public function testSetDisplayNameByProperty1Null()
    {
        $newValue = null;
        $this->record->displayName = $newValue;
        $this->assertEquals('', $this->record->displayName);
    }
    
    public function testSetDisplayNameByProperty1InvalidType()
    {
        $newValue = false;
        $this->record->displayName = $newValue;
        $this->assertEquals('', $this->record->displayName);
    }
    
    public function testSetDisplayNameByProperty2()
    {
        $newValue = 'Michael';
        $this->record->DisplayName = $newValue;
        $this->assertEquals($newValue, $this->record->displayName);
    }
    
    public function testSetDisplayNameByProperty2Null()
    {
        $newValue = null;
        $this->record->DisplayName = $newValue;
        $this->assertEquals('', $this->record->displayName);
    }
    
    public function testSetDisplayNameByProperty2InvalidType()
    {
        $newValue = false;
        $this->record->DisplayName = $newValue;
        $this->assertEquals('', $this->record->displayName);
    }
    
    public function testSetStateByMethod()
    {
        $newValue = 'Inactive';
        $this->record->setState($newValue);
        $this->assertEquals($newValue, $this->record->state);
    }
    
    public function testSetStateByMethodNull()
    {
        $newValue = null;
        $this->record->setState($newValue);
        $this->assertEquals('', $this->record->state);
    }
    
    public function testSetStateByMethodInvalidType()
    {
        $newValue = false;
        $this->record->setState($newValue);
        $this->assertEquals('', $this->record->state);
    }
    
    public function testSetStateByProperty1()
    {
        $newValue = 'Inactive';
        $this->record->state = $newValue;
        $this->assertEquals($newValue, $this->record->state);
    }
    
    public function testSetStateByProperty1Null()
    {
        $newValue = null;
        $this->record->state = $newValue;
        $this->assertEquals('', $this->record->state);
    }
    
    public function testSetStateByProperty1InvalidType()
    {
        $newValue = false;
        $this->record->state = $newValue;
        $this->assertEquals('', $this->record->state);
    }
    
    public function testSetStateByProperty2()
    {
        $newValue = 'Inactive';
        $this->record->State = $newValue;
        $this->assertEquals($newValue, $this->record->state);
    }
    
    public function testSetStateByProperty2Null()
    {
        $newValue = null;
        $this->record->State = $newValue;
        $this->assertEquals('', $this->record->state);
    }
    
    public function testSetStateByProperty2InvalidType()
    {
        $newValue = false;
        $this->record->State = $newValue;
        $this->assertEquals('', $this->record->state);
    }
    
    // constructor tests
    
    public function testConstructNullCustodian()
    {
        $r = new Record($this->recordGuid,
            null,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals(false, $r->isRecordCustodian);
    }
    
    public function testConstructInvalidTypeCustodian()
    {
        $r = new Record($this->recordGuid,
            0,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals(false, $r->isRecordCustodian);
    }
    
    public function testConstructNullRelType()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            null,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals(0, $r->relType);
    }
    
    public function testConstructInvalidTypeRelType()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            false,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals(0, $r->relType);
    }
    
    public function testConstructNullRelName()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            null,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals('', $r->relName);
    }
    
    public function testConstructInvalidTypeRelName()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            false,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals('', $r->relName);
    }
    
    public function testConstructNullAuthExpires()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            null,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals('', $r->authExpires);
    }
    
    public function testConstructInvalidTypeAuthExpires()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            false,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals('', $r->authExpires);
    }    
    
    public function testConstructNullAuthExpired()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            null,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals(false, $r->authExpired);
    }
    
    public function testConstructInvalidTypeAuthExpired()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            0,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals(false, $r->authExpired);
    }
    
    public function testConstructNullDisplayName()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            null,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals('', $r->displayName);
    }
    
    public function testConstructInvalidTypeDisplayName()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            false,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals('', $r->displayName);
    }
    
    public function testConstructNullState()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            null,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals('', $r->state);
    }
    
    public function testConstructInvalidTypeState()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            false,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals('', $r->state);
    }
    
    public function testConstructNullDateCreated()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            null,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals('', $r->dateCreated);
    }
    
    public function testConstructInvalidTypeDateCreated()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            false,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals('', $r->dateCreated);
    }
    
    public function testConstructNullMaxSizeBytes()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            null,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals(0, $r->maxSizeBytes);
    }
    
    public function testConstructInvalidTypeMaxSizeBytes()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            false,
            self::RECORD_INITIAL_SIZE_BYTES,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals(0, $r->maxSizeBytes);
    }
    
    public function testConstructNullSizeBytes()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            null,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals(0, $r->sizeBytes);
    }
    
    public function testConstructInvalidTypeSizeBytes()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            false,
            self::RECORD_INITIAL_VALUE);
        $this->assertEquals(0, $r->sizeBytes);
    }
    
    public function testConstructNullValue()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            null);
        $this->assertEquals('', $r->value);
    } 
    
    public function testConstructInvalidTypeValue()
    {
        $r = new Record($this->recordGuid,
            self::RECORD_INITIAL_CUSTODIAN,
            self::RECORD_INITIAL_REL_TYPE,
            self::RECORD_INITIAL_REL_NAME,
            self::RECORD_INITIAL_AUTH_EXPIRES,
            self::RECORD_INITIAL_AUTH_EXPIRED,
            self::RECORD_INITIAL_DISPLAY_NAME,
            self::RECORD_INITIAL_STATE,
            self::RECORD_INITIAL_DATE_CREATED,
            self::RECORD_INITIAL_MAX_SIZE_BYTES,
            self::RECORD_INITIAL_SIZE_BYTES,
            false);
        $this->assertEquals('', $r->value);
    }
    
   // xml parsing tests

    public function testFromXMLAllAttr()
    {
        $xml =<<<EOXML
<record id="A13944F3-E57F-4083-83E7-7F27F1D4339A" record-custodian="true" rel-type="1" 
    rel-name="Self" auth-expires="9999-12-31T23:59:59.0Z" auth-expired="0" display-name="Joe Test" 
    state="Active" date-created="2008-05-29T13:22:07.0Z" max-size-bytes="512" size-bytes="20">
    the value
</record>
EOXML;
        $xmlObj = new SimpleXMLElement($xml);
        $r = Record::fromXML($xmlObj);
        $this->assertEquals(new Guid("A13944F3-E57F-4083-83E7-7F27F1D4339A"), $r->id);
        $this->assertEquals(true, $r->isRecordCustodian);
        $this->assertEquals(1, $r->relType);
        $this->assertEquals("Self", $r->relName);
        $this->assertEquals("9999-12-31T23:59:59.0Z", $r->authExpires);
        $this->assertEquals(false, $r->authExpired);
        $this->assertEquals("Joe Test", $r->displayName);
        $this->assertEquals("Active", $r->state);
        $this->assertEquals("2008-05-29T13:22:07.0Z", $r->dateCreated);
        $this->assertEquals(512, $r->maxSizeBytes);
        $this->assertEquals(20, $r->sizeBytes);
        $this->assertEquals('the value', trim($r->value));
    }
    
    public function testFromXMLMinAttr()
    {
        $xml = '<record id="A13944F3-E57F-4083-83E7-7F27F1D4339A" rel-type="1"  state="Active" />';
        $xmlObj = new SimpleXMLElement($xml);
        $r = Record::fromXML($xmlObj);
        $this->assertEquals(new Guid("A13944F3-E57F-4083-83E7-7F27F1D4339A"), $r->id);
        $this->assertEquals(false, $r->isRecordCustodian);
        $this->assertEquals(1, $r->relType);
        $this->assertEquals('', $r->relName);
        $this->assertEquals('', $r->authExpires);
        $this->assertEquals('', $r->authExpired);
        $this->assertEquals('', $r->displayName);
        $this->assertEquals("Active", $r->state);
        $this->assertEquals('', $r->dateCreated);
        $this->assertEquals(0, $r->maxSizeBytes);
        $this->assertEquals(0, $r->sizeBytes);
        $this->assertEquals('', $r->value);
    }
}
?>