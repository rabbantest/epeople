<?php
require_once "PHPUnit/Framework.php";
require_once "../../health_vault_library.php";

/**
 * Test class for global functions
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Tests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class GlobalFunctionsTest extends PHPUnit_Framework_TestCase
{
	
	/**
	 * A SimpleXMLElement used for testing
	 *
	 * @var SimpleXMLElement 
	 *
	 */
	protected $simpleXMLObj = null;
	/**
	 * An array based on what should be generated from the simpleXMLObj
	 * 
	 * @var array
	 */
	protected $xmlArr       = null;
	/**
	 * A simple version of the array from the xmlArr array
	 *
	 * @var array
	 */
	protected $simpleArr    = null;
	const XML_TO_PARSE      = 
	'<?xml version="1.0" encoding="utf-8"?><root><node1 attr1="bar"><child1>text</child1><child1>more text</child1></node1><node2>foobaz</node2></root>';
	
	public function setUp()
	{
		$this->simpleXMLObj = simplexml_load_string(self::XML_TO_PARSE);
		$this->xmlArr = 
			array(
				array(
					'@name'=>'node1', 
					'@attributes'=>
					array(
						'attr1'=>'bar'
					),
					'@children'=>
					array(
						array(
							'@name'=>'child1', 
							'@attributes'=>array(),
							'@text'=>'text',
							'@children'=>array()
						),
						array(
							'@name'=>'child1', 
							'@attributes'=>array(),
							'@text'=>'more text',
							'@children'=>array()
						),
					)
				),
				array(
					'@name'=>'node2', 
					'@attributes'=>array(), 
					'@text'=>'foobaz',
					'@children'=>array()
				)
			);
		$this->simpleArr = 
			array(
				'node1'=>
				array(
					// technically this will set child1 to 'text' then reset it
					'child1'=>'more text'
				),
				'node2'=>'foobaz'
			);
	}
	
	public function testConvertXMLObjToArr()
	{
		$array = array();
		convertXmlObjToArr($this->simpleXMLObj, $array);
		$this->assertEquals($this->xmlArr, $array);
	}
	
	public function testConvertXmlArrToSimpleArr()
	{
		$array = array();
		convertXmlArrToSimpleArr($this->xmlArr, $array);
		$this->assertEquals($this->simpleArr, $array);
	}
	
	
	/**
	 * @expectedException InvalidParameterException
	 */
	public function testConvertXmlArrToSimpleArrInvalidInput()
	{
		$inputArr = array('foo','bar','baz');
		$outputArr = array();
		convertXmlArrToSimpleArr($inputArr, $outputArr);
	}
	
	public function testGetArrayFromResponseXML()
	{
		$this->assertEquals($this->simpleArr, getArrayFromResponseXML(self::XML_TO_PARSE));
	}
	
	/**
	 * @expectedException InvalidParameterException
	 */
	public function testGetArrayFromResponseXMLInvalidType()
	{
		getArrayFromResponseXML(true);
	}
	
	/**
	 * @expectedException InvalidParameterException
	 */
	public function testGetArrayFromResponseXMLInvalidXMLInput()
	{
		getArrayFromResponseXML('foo bar baz');
	}
}
?>