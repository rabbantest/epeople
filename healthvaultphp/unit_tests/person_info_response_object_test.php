<?php
require_once "PHPUnit/Framework.php";
require_once "../../health_vault_library.php";

/**
 * Test class for PersonInfoResponseObject class
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Tests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class PersonInfoResponseObjectTest extends PHPUnit_Framework_TestCase
{
    
    /**
     * The PersonInfoResponseObject to be tested
     *
     * @var PersonInfoResponseObject 
     *
     */
    protected $responseObj;
    
    /**
     * The array of the two example records
     *
     * @var array[int]Record
     *
     */
    protected $recordArr;
    
    /**
     * The first of two example records
     *
     * @var Record 
     *
     */
    protected $record1;
    
    /**
     * The second of two example records
     *
     * @var Record 
     *
     */
    protected $record2;
    
    /**
     * The Culture object for the preferred culture
     *
     * @var Culture 
     *
     */
    protected $prefCulture;
    
    /**
     * The Culture object for the preferred ui culture
     *
     * @var Culture 
     *
     */
    protected $prefUICulture;
    
    const RECORD1_ID                = 'DDB22BED-02AB-4f3f-A4B6-02FC87D3A29F';
    const RECORD2_ID                = '93D8A9AA-A903-4c42-AC20-28C02F1DA736';
    const PERSON_ID                 = 'DB54ED17-E3AA-4c97-A459-09683CF113B4';
    const PERSON_NAME               = 'Jane Unit';
    const PERSON_APP_SETTINGS       = '<setting>7</setting>';
    const PERSON_SELECTED_RECORD_ID = 'A3862927-7DB1-411a-ADB4-0A581B88E315';
    const PERSON_MORE_RECORDS       = true;
    const PERSON_GROUPS             = 'group1,group2';
    const PERSON_PREF_CUL_LANG      = 'en';
    const PERSON_PREF_CUL_CNTRY     = 'US';
    const PERSON_PREF_UI_CUL_LANG   = 'ru';
    const PERSON_PREF_UI_CUL_CNTRY  = 'RU';
    
    public function setUp()
    {
        $this->record1 = new Record(new Guid(self::RECORD1_ID));
        $this->record2 = new Record(new Guid(self::RECORD2_ID));
        $this->recordArr = array($this->record1, $this->record2);
        $this->prefCulture = new Culture(self::PERSON_PREF_CUL_LANG, self::PERSON_PREF_CUL_CNTRY);
        $this->prefUICulture = new Culture(self::PERSON_PREF_UI_CUL_LANG, 
            self::PERSON_PREF_UI_CUL_CNTRY);
        $this->responseObj = new PersonInfoResponseObject(new Guid(self::PERSON_ID),
            self::PERSON_NAME,
            self::PERSON_APP_SETTINGS,
            new Guid(self::PERSON_SELECTED_RECORD_ID),
            self::PERSON_MORE_RECORDS,
            $this->recordArr,
            self::PERSON_GROUPS,
            $this->prefCulture, 
            $this->prefUICulture);
    }
    
    // get tests
    
    public function testGetPersonIDByMethod()
    {
        $this->assertEquals(new Guid(self::PERSON_ID), $this->responseObj->getPersonId());
    }
    
    public function testGetPersonIDByProperty1()
    {
        $this->assertEquals(new Guid(self::PERSON_ID), $this->responseObj->personId);
    }
    
    public function testGetPersonIDByProperty2()
    {
        $this->assertEquals(new Guid(self::PERSON_ID), $this->responseObj->personID);
    }
    
    public function testGetPersonIDByProperty3()
    {
        $this->assertEquals(new Guid(self::PERSON_ID), $this->responseObj->PersonId);
    }
    
    public function testGetPersonIDByProperty4()
    {
        $this->assertEquals(new Guid(self::PERSON_ID), $this->responseObj->PersonID);
    }
    
    public function testGetNameByMethod()
    {
        $this->assertEquals(self::PERSON_NAME, $this->responseObj->getName());
    }
    
    public function testGetNameByProperty1()
    {
        $this->assertEquals(self::PERSON_NAME, $this->responseObj->name);
    }
    
    public function testGetNameByProperty2()
    {
        $this->assertEquals(self::PERSON_NAME, $this->responseObj->Name);
    }
    
    public function testGetAppSettingsByMethod()
    {
        $this->assertEquals(self::PERSON_APP_SETTINGS, $this->responseObj->getAppSettings());
    }
    
    public function testGetAppSettingsByProperty1()
    {
        $this->assertEquals(self::PERSON_APP_SETTINGS, $this->responseObj->appSettings);
    }
    
    public function testGetAppSettingsByProperty2()
    {
        $this->assertEquals(self::PERSON_APP_SETTINGS, $this->responseObj->AppSettings);
    }
    
    public function testGetSelectedRecordIdByMethod()
    {
        $this->assertEquals(new Guid(self::PERSON_SELECTED_RECORD_ID), 
                $this->responseObj->getSelectedRecordId());
    }    
    
    public function testGetSelectedRecordIdByProperty1()
    {
        $this->assertEquals(new Guid(self::PERSON_SELECTED_RECORD_ID), 
                $this->responseObj->selectedRecordId);
    }
    
    public function testGetSelectedRecordIdByProperty2()
    {
        $this->assertEquals(new Guid(self::PERSON_SELECTED_RECORD_ID), 
                $this->responseObj->selectedRecordID);
    }
    
    public function testGetSelectedRecordIdByProperty3()
    {
        $this->assertEquals(new Guid(self::PERSON_SELECTED_RECORD_ID), 
                $this->responseObj->SelectedRecordId);
    }
    
    public function testGetSelectedRecordIdByProperty4()
    {
        $this->assertEquals(new Guid(self::PERSON_SELECTED_RECORD_ID), 
                $this->responseObj->SelectedRecordID);
    }
    
    public function testIsMoreRecordsByMethod()
    {
        $this->assertEquals(self::PERSON_MORE_RECORDS, $this->responseObj->getIsMoreRecords());
    }
    
    public function testIsMoreRecordsByProperty1()
    {
        $this->assertEquals(self::PERSON_MORE_RECORDS, $this->responseObj->isMoreRecords);
    }
    
    public function testIsMoreRecordsByProperty2()
    {
        $this->assertEquals(self::PERSON_MORE_RECORDS, $this->responseObj->IsMoreRecords);
    }
    
    public function testGetRecordsByMethod()
    {
        $this->assertEquals($this->recordArr, $this->responseObj->getRecords());
    }
    
    public function testGetRecordsByProperty1()
    {
        $this->assertEquals($this->recordArr, $this->responseObj->records);
    }
    
    public function testGetRecordsByProperty2()
    {
        $this->assertEquals($this->recordArr, $this->responseObj->Records);
    }
    
    public function testGetGroupsByMethod()
    {
        $this->assertEquals(self::PERSON_GROUPS, $this->responseObj->getGroups());
    }
    
    public function testGetGroupsByProperty1()
    {
        $this->assertEquals(self::PERSON_GROUPS, $this->responseObj->groups);
    }
    
    public function testGetGroupsByProperty2()
    {
        $this->assertEquals(self::PERSON_GROUPS, $this->responseObj->Groups);
    }
    
    public function testGetPreferredCultureByMethod()
    {
        $this->assertEquals($this->prefCulture, $this->responseObj->getPreferredCulture());
    }
    
    public function testGetPreferredCultureByProperty1()
    {
        $this->assertEquals($this->prefCulture, $this->responseObj->preferredCulture);
    }
    
    public function testGetPreferredCultureByProperty2()
    {
        $this->assertEquals($this->prefCulture, $this->responseObj->PreferredCulture);
    }
    
    public function testGetPreferredUICultureByMethod()
    {
        $this->assertEquals($this->prefUICulture, $this->responseObj->getPreferredUICulture());
    }
    
    public function testGetPreferredUICultureByProperty1()
    {
        $this->assertEquals($this->prefUICulture, $this->responseObj->preferredUICulture);
    }
    
    public function testGetPreferredUICultureByProperty2()
    {
        $this->assertEquals($this->prefUICulture, $this->responseObj->PreferredUICulture);
    }
    // construct tests
    
    public function testConstructNullInputs()
    {
        $piro = new PersonInfoResponseObject();
        $this->assertEquals(null, $piro->personId);
        $this->assertEquals('', $piro->name);
        $this->assertEquals('', $piro->appSettings);
        $this->assertEquals(null, $piro->selectedRecordId);
        $this->assertEquals(false, $piro->isMoreRecords);
        $this->assertEquals(array(), $piro->records);
        $this->assertEquals('', $piro->groups);
        $this->assertEquals(null, $piro->preferredCulture);
        $this->assertEquals(null, $piro->preferredUICulture);
    }
    // xml parsing tests
    
    public function testFromXMLAllValues()
    {
        $xml =<<<EOXML
<info>
<person-info>
    <person-id>0700081B-C8CB-4d84-AF4F-ADF28DD7A591</person-id>
    <name>Wolfgang Mozart</name>
    <app-settings><a-value attr="true" /><another-value>foo</another-value></app-settings>
    <selected-record-id>162B70EA-2C6A-4aec-9E27-9F5E461FB5CB</selected-record-id>
    <more-records>true</more-records>
    <record id="A13944F3-E57F-4083-83E7-7F27F1D4339A" record-custodian="true" rel-type="1" 
        rel-name="Self" auth-expires="9999-12-31T23:59:59.0Z" auth-expired="0" display-name="Joe Test" 
        state="Active" date-created="2008-05-29T13:22:07.0Z" max-size-bytes="512" size-bytes="20">
        the value
    </record>
    <record id="A13944F3-E57F-4083-83E7-7F27F1D4339A" rel-type="1"  state="Active" />
    <groups>the good,the bad,the ugly</groups>
    <preferred-culture>
        <language>en</language>
        <country>GB</country>
    </preferred-culture>
    <preferred-uiculture>
        <language>ru</language>
        <country>RU</country>
    </preferred-uiculture>
</person-info>
</info>
EOXML;
        $record1 = new Record(new Guid('A13944F3-E57F-4083-83E7-7F27F1D4339A'), true, 1, 'Self',
            '9999-12-31T23:59:59.0Z', false, 'Joe Test', 'Active', '2008-05-29T13:22:07.0Z', 512, 
            20, 'the value');
        $record2 = new Record(new Guid('A13944F3-E57F-4083-83E7-7F27F1D4339A'), null, 1, null,
            null, null, null, 'Active');
        $recArr = array($record1, $record2);
        $piro = PersonInfoResponseObject::fromXML(new SimpleXMLElement($xml));
        $this->assertEquals(new Guid('0700081B-C8CB-4d84-AF4F-ADF28DD7A591'), $piro->personId);
        $this->assertEquals('Wolfgang Mozart', $piro->name);
        $this->assertEquals('<a-value attr="true"/><another-value>foo</another-value>', 
                $piro->appSettings);
        $this->assertEquals(new Guid('162B70EA-2C6A-4aec-9E27-9F5E461FB5CB'), $piro->selectedRecordId);
        $this->assertEquals(true, $piro->isMoreRecords);
        $this->assertEquals($recArr, $piro->records);
        $this->assertEquals('the good,the bad,the ugly', $piro->groups);
        $this->assertEquals(new Culture('en','GB'), $piro->preferredCulture);
        $this->assertEquals(new Culture('ru','RU'), $piro->preferredUICulture);
    }
    
    public function testFromXMLMinValues()
    {
        $xml =<<<EOXML
<info>
<person-info>
    <person-id>0700081B-C8CB-4d84-AF4F-ADF28DD7A591</person-id>
    <name>Wolfgang Mozart</name>
</person-info>
</info>
EOXML;
        $piro = PersonInfoResponseObject::fromXML(new SimpleXMLElement($xml));
        $this->assertEquals(new Guid('0700081B-C8CB-4d84-AF4F-ADF28DD7A591'), $piro->personId);
        $this->assertEquals('Wolfgang Mozart', $piro->name);
        $this->assertEquals('', $piro->appSettings);
        $this->assertEquals(null, $piro->selectedRecordId);
        $this->assertEquals(false, $piro->isMoreRecords);
        $this->assertEquals(array(), $piro->records);
        $this->assertEquals('', $piro->groups);
        $this->assertEquals(null, $piro->preferredCulture);
        $this->assertEquals(null, $piro->preferredUICulture);
    }
}
?>