<?php
require_once "PHPUnit/Framework.php";
require_once "../../health_vault_library.php";

/**
 * Test class for RequestBuilder class
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Tests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class RequestBuilderTest extends PHPUnit_Framework_TestCase
{
    public function testGetXML()
    {
        $request = new MockIHealthVaultRequest();
        $builder = new RequestBuilder($request);
        $expectedXML  = '<?xml version="1.0" encoding="utf-8"?>';
        $expectedXML .= '<wc-request:request xmlns:wc-request="urn:com.microsoft.wc.request">';
        $expectedXML .= '<header>foo</header>';
        $expectedXML .= '<body>bar</body>';
        $expectedXML .= '<footer>baz</footer>';
        $expectedXML .= '</wc-request:request>';
        $this->assertEquals($expectedXML, $builder->getXml());
    }
}


/**
 * A class implementing IHealthVaultRequest for testing RequestBuilder
 *
 */
class MockIHealthVaultRequest implements IHealthVaultRequest
{
    public function getHeader()
    {
        return "<header>foo</header>";
    }
    
    public function getBody()
    {
        return "<body>bar</body>";
    }
    
    public function getFooter()
    {
        return "<footer>baz</footer>";
    }
    
    public function getMethodName()
    {
        return "Test";
    }
}
?>