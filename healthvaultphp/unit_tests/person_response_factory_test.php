<?php
require_once "PHPUnit/Framework.php";
require_once "../../health_vault_library.php";

/**
 * Test class for PersonResponseFactory class
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Tests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class PersonResponseFactoryTest extends PHPUnit_Framework_TestCase
{
    public function testGetResponseObjectForGetPersonInfo()
    {
        $xml =<<<EOXML
<?xml version="1.0" encoding="utf-8" ?> 
<response>
 <status>
  <code>0</code> 
 </status>
 <wc:info xmlns:wc="urn:com.microsoft.wc.methods.response.GetPersonInfo">
  <person-info>
    <person-id>0700081B-C8CB-4d84-AF4F-ADF28DD7A591</person-id>
    <name>Wolfgang Mozart</name>
  </person-info>
 </wc:info>
</response>
EOXML;
        $expectedResult = new PersonInfoResponseObject(
            new Guid('0700081B-C8CB-4d84-AF4F-ADF28DD7A591'), 'Wolfgang Mozart');
        $this->assertEquals($expectedResult, PersonResponseFactory::getResponseObject($xml,
                    'GetPersonInfo'));
    }
    
    /**
     * @expectedException InvalidParameterException
     */
    public function testGetResponseObjectForGetPersonInfoNotXML()
    {
        PersonResponseFactory::getResponseObject(true, 'GetPersonInfo');
    }
    
    /**
     * @expectedException InvalidParameterFormatException
     */
    public function testGetResponseObjectForGetPersonInfoInvalidXML()
    {
        PersonResponseFactory::getResponseObject('<root><cat/></root>', 'GetPersonInfo');
    }
    
    /**
     * @expectedException NotImplementedException
     */
    public function testGetResponseObjectInvalidMethod()
    {
        PersonResponseFactory::getResponseObject('', 'NonExistantMethod');
    }
}
?>