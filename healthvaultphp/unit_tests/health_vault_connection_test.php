<?php
require_once "PHPUnit/Framework.php";
require_once "../../health_vault_library.php";
/**
 * Test class for HealthVaultConnection class
 * 
 * The tests will assume that the settings needed (app id, thumbprint, cert) have all be properly
 * set and contain valid data
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Tests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class HealthVaultConnectionTest extends PHPUnit_Framework_TestCase
{
	
	/**
	 * The HealthVaultConnection to test
	 *
	 * @var HealthVaultConnection 
	 *
	 */
	protected $healthVaultConnection = null;
	
	const APP_ID = '05a059c9-c309-46af-9b86-b06d42510550';
	const REDIRECT_TO = 'http://localhost/foo.php';
	const EXPECTED_SHELL_URL = 'https://account.healthvault-ppe.com/redirect.aspx?target=AUTH&targetqs=?appid%3D05a059c9-c309-46af-9b86-b06d42510550%26redirect%3Dhttp%3A%2F%2Flocalhost%2Ffoo.php';
	const EXPECTED_SHELL_URL_NO_REDIRECT = 'https://account.healthvault-ppe.com/redirect.aspx?target=AUTH&targetqs=?appid%3D05a059c9-c309-46af-9b86-b06d42510550%26redirect%3D';
	const EXPECTED_SHELL_URL_ARRAY_REDIRECT = 'https://account.healthvault-ppe.com/redirect.aspx?target=AUTH&targetqs=?appid%3D05a059c9-c309-46af-9b86-b06d42510550%26redirect%3DArray';
	
	public function setUp()
	{
		$this->healthVaultConnection = HealthVaultConnection::getInstance();
	}
	
	public function testGetSharedSecret()
	{
		$this->assertRegexp('/[a-z0-9]{40}/',$this->healthVaultConnection->getSharedSecret());
	}
	
	public function testGetSecretDigestInitial()
	{
		$this->assertEquals(null, $this->healthVaultConnection->getSecretDigest());
	}
	
	public function testGetAuthenticationTokenThenSecretDigest()
	{
		$this->assertRegexp('@^[a-z0-9?_+=/-]+$@i', 
				$this->healthVaultConnection->getAuthenticationToken());
		$this->assertRegexp('@^[a-z0-9?_+=/-]+$@i', 
				$this->healthVaultConnection->getSecretDigest());
	}
	
	public function testGetShellUrlWithRedirect()
	{
		$this->assertEquals(self::EXPECTED_SHELL_URL, 
				$this->healthVaultConnection->getShellUrl(self::REDIRECT_TO));
	}
	
	public function testGetShellUrlWithoutRedirect()
	{
		$this->assertEquals(self::EXPECTED_SHELL_URL_NO_REDIRECT,
				$this->healthVaultConnection->getShellUrl());
	}
	
	public function testGetShellUrlArrayRedirect()
	{
		$this->assertEquals(self::EXPECTED_SHELL_URL_ARRAY_REDIRECT,
				$this->healthVaultConnection->getShellUrl(array()));
	}
	
	/**
	 * @expectedException InvalidParameterException
	 */
	public function testGetShellUrlObjectRedirectTo()
	{
		$this->healthVaultConnection->getShellUrl(new ArrayObject());
	}
	
	/**
	 * @expectedException NetworkIOException
	 */
	public function testSendRequestPayloadBlankPayloadInvlaidURL()
	{
		$this->healthVaultConnection->sendRequestPayload('', 's://blah');
	}
	
	public function testSendRequestPayloadInvalidData()
	{
		$request = '<?xml version="1.0" encoding="utf-8"?><invalid>GARBAGEDATA</invalid>';
		$response = $this->healthVaultConnection->sendRequestPayload($request);
		$simpleXML = new SimpleXMLElement($response);
		$response_code = (int)$simpleXML->status->code;
		$this->assertEquals($response_code, 2);
	}	
	
	public function testSendRequestPayloadAlmostValidData()
	{
		$request = <<<EOR
<?xml version="1.0" encoding="utf-8"?>
<wc-request:request xmlns:wc-request="urn:com.microsoft.wc.request">
    <header>
        <method>CreateAuthenticatedSessionToken</method>
        <method-version>1</method-version>
        <app-id>9ca84d74-1473-471d-940f-2699cb7198df</app-id>
        <language>en</language>
        <country>US</country>
        <msg-time>2008-05-29T21:48:45.189Z</msg-time>
        <msg-ttl>1800</msg-ttl>EXTRA
        <version>0.10.1851.2824</version>
    </header>
    <info>
        <auth-info>
            <app-id>9ca84d74-1473-471d-940f-2699cb7198df</app-id>
            <credential>
                <appserver>
                    <sig digestMethod="SHA1" sigMethod="RSA-SHA1" thumbprint="9CA9B4CD3BF947EC9AC25DE9A01C9CC68B0D0DDF">iYFaAs/S8h5+gpoTKwUovqjll0/4g5YkqFqkuxHW/lXJKv6d+7wnGpUKo0owcEaeIwfk88PQGBn+Tc7wltFLEujfG/qLS2GveBa2v5b54xI3Y+CQFyI1Cw7B0YEhrfDr+CERLUqM2yE9bTa+4jsmpiLbdcEhd/FbW2ql/3VdA4uzLHqRYXE+qA1IIvghqFzJVoBBn7hWbIPKes654DOyiOiIrAqf6B/dHIAHLbK8CThNzQTyg8fQ9BO5gCqIWWHqMxhlHsT5kG+8tGgVR+UllAjOWbBGC9qN9jbkldMUkXmI5duT9BLzqPtwE9CxH+CJpphvhQdMYrd1LZ6P3n0loA==</sig>
                    <content>
                    <app-id>9ca84d74-1473-471d-940f-2699cb7198df</app-id>
                    <shared-secret>
                        <hmac-alg algName="HMACSHA1">4FWdo9sZFAeDIargG9O65Was7Icql+6Wqe36YDwfKunNmFKYVZTMMhVc8xoGIecl/YNrVOoPzP61bvZNU6CNsIHBbink8m9gi1+bg9VkN6pfw/ovwKtFKz8AJcBVzwkse8JidTgtSXtpGThmdiuI7+GX7kObErOkHaT4QQxBFC907FXPas4SxoU07W7kF2oGK425xp4jvPadNe8P4dRYeiRdE4bRUCmMJPlKILKEm3NRlGlmHDMydJtV+L2yKOZA0/Ziw8LhOWhSJEqfoD0hoC7rVc04QAlbQZazclS48evnHp7fYqbda7zJXyOI4zDb8ShZnNBO4kvNLq5FWFRWhw==</hmac-alg>
                    </shared-secret>
                    </content>
                </appserver>
            </credential>
        </auth-info>
    </info>
</wc-request:request>
EOR;
		$response = $this->healthVaultConnection->sendRequestPayload($request);
		$simpleXML = new SimpleXMLElement($response);
		$response_code = (int)$simpleXML->status->code;
		$this->assertEquals($response_code, 3);
	}
	
	public function testSendRequestPayloadValidDataUnauthorizedApp()
	{
		$request = <<<EOR
<?xml version="1.0" encoding="utf-8"?>
<wc-request:request xmlns:wc-request="urn:com.microsoft.wc.request">
    <header>
        <method>CreateAuthenticatedSessionToken</method>
        <method-version>1</method-version>
        <app-id>9ca84d74-1473-471d-940f-2699cb7198df</app-id>
        <language>en</language>
        <country>US</country>
        <msg-time>2008-05-29T21:48:45.189Z</msg-time>
        <msg-ttl>1800</msg-ttl>
        <version>0.10.1851.2824</version>
    </header>
    <info>
        <auth-info>
            <app-id>9ca84d74-1473-471d-940f-2699cb7198df</app-id>
            <credential>
                <appserver>
                    <sig digestMethod="SHA1" sigMethod="RSA-SHA1" thumbprint="9CA9B4CD3BF947EC9AC25DE9A01C9CC68B0D0DDF">iYFaAs/S8h5+gpoTKwUovqjll0/4g5YkqFqkuxHW/lXJKv6d+7wnGpUKo0owcEaeIwfk88PQGBn+Tc7wltFLEujfG/qLS2GveBa2v5b54xI3Y+CQFyI1Cw7B0YEhrfDr+CERLUqM2yE9bTa+4jsmpiLbdcEhd/FbW2ql/3VdA4uzLHqRYXE+qA1IIvghqFzJVoBBn7hWbIPKes654DOyiOiIrAqf6B/dHIAHLbK8CThNzQTyg8fQ9BO5gCqIWWHqMxhlHsT5kG+8tGgVR+UllAjOWbBGC9qN9jbkldMUkXmI5duT9BLzqPtwE9CxH+CJpphvhQdMYrd1LZ6P3n0loA==</sig>
                    <content>
                    <app-id>9ca84d74-1473-471d-940f-2699cb7198df</app-id>
                    <shared-secret>
                        <hmac-alg algName="HMACSHA1">4FWdo9sZFAeDIargG9O65Was7Icql+6Wqe36YDwfKunNmFKYVZTMMhVc8xoGIecl/YNrVOoPzP61bvZNU6CNsIHBbink8m9gi1+bg9VkN6pfw/ovwKtFKz8AJcBVzwkse8JidTgtSXtpGThmdiuI7+GX7kObErOkHaT4QQxBFC907FXPas4SxoU07W7kF2oGK425xp4jvPadNe8P4dRYeiRdE4bRUCmMJPlKILKEm3NRlGlmHDMydJtV+L2yKOZA0/Ziw8LhOWhSJEqfoD0hoC7rVc04QAlbQZazclS48evnHp7fYqbda7zJXyOI4zDb8ShZnNBO4kvNLq5FWFRWhw==</hmac-alg>
                    </shared-secret>
                    </content>
                </appserver>
            </credential>
        </auth-info>
    </info>
</wc-request:request>
EOR;
		$response = $this->healthVaultConnection->sendRequestPayload($request);
		$simpleXML = new SimpleXMLElement($response);
		$response_code = (int)$simpleXML->status->code;
		$this->assertEquals($response_code, 11);
	}
	
	// testSendRequestPayload has a valid test in testGetAuthenticationToken
}
?>