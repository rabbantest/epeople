<?php
require_once "PHPUnit/Framework.php";
require_once "culture_test.php";
require_once "file_test.php";
require_once "global_functions_test.php";
require_once "guid_test.php";
require_once "health_vault_connection_test.php";
require_once "health_vault_helper_test.php";
require_once "person_info_response_object_test.php";
require_once "person_response_factory_test.php";
require_once "record_test.php";
require_once "request_builder_test.php";
require_once "response_factory_test.php";
// settings requires health_vault_library to NOT be used

/**
 * TestSuite for running all* of the unit tests
 * 
 * *Any tests that cannot be run with the health_vault_library being included are not run here and
 * must be run separately.
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Tests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class AllTests extends PHPUnit_Framework_TestSuite
{
	/**
	 * Builds and returns a TestSuite for all of the tests in the project
	 * 
	 * @return PHPUnit_Framework_TestSuite
	 */
	public static function suite()
	{
		$suite = new PHPUnit_Framework_TestSuite();
        $suite->addTestSuite('CultureTest');
		$suite->addTestSuite('FileTest');
		$suite->addTestSuite('GlobalFunctionsTest');
        $suite->addTestSuite('GuidTest');
		$suite->addTestSuite('HealthVaultConnectionTest');
		$suite->addTestSuite('HealthVaultHelperTest');
		$suite->addTestSuite('PersonInfoResponseObjectTest');
		$suite->addTestSuite('PersonResponseFactoryTest');
		$suite->addTestSuite('RecordTest');
		$suite->addTestSuite('RequestBuilderTest');
		$suite->addTestSuite('ResponseFactoryTest');
		return $suite;
	}
}
?>