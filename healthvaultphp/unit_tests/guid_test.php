<?php
require_once "PHPUnit/Framework.php";
require_once "../../health_vault_library.php";

/**
 * Test class for Guid class
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Tests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class GuidTest extends PHPUnit_Framework_TestCase
{
    public function testVerifyGuidValidGuidStringWithBraces()
    {
        $guidString = "{24DC31C8-9468-4ecb-A277-8BDFDB89FD6F}";
        $this->assertEquals(true, Guid::verifyGuid($guidString));
    }
    
    public function testVerifyGuidValidGuidStringWithoutBraces()
    {
        $guidString = "24DC31C8-9468-4ecb-A277-8BDFDB89FD6F";
        $this->assertEquals(true, Guid::verifyGuid($guidString));
    } 
    
    public function testVerifyGuidValidGuidStringMismatchedBraces1()
    {
        $guidString = "24DC31C8-9468-4ecb-A277-8BDFDB89FD6F}";
        $this->assertEquals(false, Guid::verifyGuid($guidString));
    }
    
    public function testVerifyGuidValidGuidStringMismatchedBraces2()
    {
        $guidString = "{24DC31C8-9468-4ecb-A277-8BDFDB89FD6F";
        $this->assertEquals(false, Guid::verifyGuid($guidString));
    }
    
    public function testVerifyGuidInvalidGuid()
    {
        $guidString = "foo-bar-baz";
        $this->assertEquals(false, Guid::verifyGuid($guidString));
    }
    
    public function testConstructValidGuidAnToString()
    {
        $guidString = "{24DC31C8-9468-4ecb-A277-8BDFDB89FD6F}";
        $guid = new Guid($guidString);
        $this->assertEquals($guidString, $guid->__toString());
    }
    
    /**
     * @expectedException InvalidParameterFormatException
     */
    public function testConstructInvalidGuid()
    {
        $guidString = "foo-bar-baz";
        $guid = new Guid($guidString);
    }
}
?>