<?php
require_once "PHPUnit/Framework.php";
require_once "../../health_vault_library.php";
/**
 * Test class for HealthVaultHelper class
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Tests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class HealthVaultHelperTest extends PHPUnit_Framework_TestCase
{
	/**
	 * The shared secret being used in the digest tests
	 * 
	 * @var string
	 */
	const SHARED_SECRET = "45939c2f7ce5681e4847fceb4d1ed90dbd975e6f";
	/**
	 * The cert text itself that will be used in the tests requiring a private key
	 * 
	 * @var string
	 */
	const VALID_CERT = "-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQDT8EwU3VehQ+jjYp/osiiWTL2uQJIYtQa33FI5sxF33ievp3e9
NnBa6x/NCQiWcU+lmzRB10ddm0RSjFYt+Ud1nFjgEMvpsYt+ywLhMkUoPOtZcm52
xGZ2RjJJabnHOHW7EDgVyP+hMXEjAsJkA5KNvCeEGf4r518nxsHeBQwYuQIDAQAB
AoGAByjpCRNpMHOA3J2qVUwffuRczBQ2W0tSTMfOp1BbWLdSk+aObv/1z7x9uImD
lsGBNVEG9Op26LykFD3Vc3wmY8z0z2oOqmL4zcEj6nIB4zKfaFfqGYHlGflDEleV
QVEZWGHPhJQ8V/3XboEdJYPueJDQQxaAeNSQA63tQx0liQECQQD/DIN8Ua24AAS7
IcHqjSeywT8dy+vDW6L32hiSS7CkBE10kgG+TsouY+UQ06VjYLDBKFpa4nQTgidw
VaKjBBQJAkEA1LqgszmpocLk6JqjGtER0yvXWEimwWBnuunteG43JV0jPcVAs9z7
wpkclMblWiA68/aeNgZJ3I2nFGfy8+DrMQJAUd2GZuUgyarX2ekgAVWFpI7632mR
J4fGa6AJuwuoiz0GT6B+BsPribBu2lPVBm0GzzlQp1OAxrJETn5uUb0BYQJAFedR
fKLHUJycrUae3LgCOdgdyD1Szj5678f+Z5QRoYtBga65xstOMO9K/hasrub6qvwN
tkpXeJotRfCeF3ZlQQJBAK/QnjRSf5szmwe7FAo3iq++bAJMVjJnIn4cR0IIgTiT
x6sWqraz+u5OTWWMHzkGkFhFUfTpicBNgQ24k9bZwe8=
-----END RSA PRIVATE KEY-----
";
	/**
	 * The resulting digest from the shared secred constant above
	 * 
	 * @var string
	 */
	const DIGEST_RESULT = "ZjdkNzZiMzE3NjFkN2YxMzQyMGE0OTI5YWJkYTM3OTNlOWQ3MGU2OA==";
	/**
	 * The resulting hash digest from the shared secret
	 * 
	 * @var string
	 */
	const HASH_DIGEST_RESULT = "i8+YsVyncvk7DKtUeeO4DcHn/OI=";
	/**
	 * The hmac sha1 when using the shared secret (base64_encoded as the key)
	 * 
	 * @var string
	 */
	const HMAC_SHA1_RESULT = '99drMXYdfxNCCkkpq9o3k+nXDmg=';
	
	protected $expectedPrivateKey  = null;
	protected $keyFile             = null;
	protected $unreadableFileName  = null;
	protected $nonexistantFileName = null;
	protected $emptyFileName       = null;
	// Methods HealthVaultHelper::getAppId and HealthVaultHelper::getThumbPrint
	//  are not going to be tested because they are just wrappers for Settings
	//  with a specific ini file. Settings is testing with SetingsTest
	
	public function setUp()
	{
		if(($this->keyFile = tempnam('nonExistantDirToRedirectToTempDir','t')) === false && // system temp
				($this->keyFile = tempnam(HV_BASE_PATH . '/writable','t')) === false) // default writable
		{
			$this->keyFile = tempnam('.', 't');
		}
		if($this->keyFile != null)
		{
			$this->nonexistantFileName = tempnam(dirname($this->keyFile),'t');
			$this->emptyFileName       = tempnam(dirname($this->keyFile),'t');
			unlink($this->nonexistantFileName);
			if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')
			{
				$this->unreadableFileName = tempnam(dirname($this->keyFile),'t');
				chmod($this->unreadableFileName, 0200);
			}
			file_put_contents($this->keyFile, self::VALID_CERT);
			$this->expectedPrivateKey = openssl_pkey_get_private(self::VALID_CERT);
		}
	}
	
	public function tearDown()
	{
		if($this->keyFile != null)
		{
			unlink($this->keyFile);
		}
		if($this->unreadableFileName != null && file_exists($this->unreadableFileName))
		{
			unlink($this->unreadableFileName);
		}
		if($this->emptyFileName != null)
		{
			unlink($this->emptyFileName);
		}
	}
	
	public function testGetPrivateKeyValidFile()
	{
		if($this->keyFile == null)
		{
			$this->markTestSkipped('No key file to test against');
		}
		$privateKey = HealthVaultHelper::getPrivateKey($this->keyFile);
		// we can't actually compare resources because they aren't the _same_ resource,
		// but we can check if they are arguably equivalent
		$this->assertEquals(openssl_pkey_get_details($this->expectedPrivateKey),
				openssl_pkey_get_details($privateKey));
	}
	
	/**
	 * @expectedException FileNotFoundException
	 */
	public function testGetPrivateKeyNonexistantFile()
	{
		if($this->nonexistantFileName == null)
		{
			$this->markTestSkipped('No nonexistant file determined');
		}
		HealthVaultHelper::getPrivateKey($this->nonexistantFileName);
	}
	
	/**
	 * @expectedException FileNotReadableException
	 */
	public function testGetPrivateKeyNonreadableFile()
	{
		if($this->unreadableFileName == null)
		{
			$this->markTestSkipped('no unreadable file to test');
		}
		HealthVaultHelper::getPrivateKey($this->unreadableFileName);
	}
	
	/**
	 * @expectedException UnableToExtractPrivateKeyException
	 */
	public function testGetPrivateKeyEmptyFile()
	{
		if($this->emptyFileName == null)
		{
			$this->markTestSkipped('No empty file to test');
		}
		HealthVaultHelper::getPrivateKey($this->emptyFileName);
	}
	
	public function testCreateDigest()
	{
		$this->assertEquals(self::DIGEST_RESULT, 
				HealthVaultHelper::createDigest(self::SHARED_SECRET, self::SHARED_SECRET));
	}
	
	public function testCreateHashDigest()
	{
		$this->assertEquals(self::HASH_DIGEST_RESULT, HealthVaultHelper::getHashDigest(
					self::SHARED_SECRET));
	}
	
	public function testGetHmacSha1()
	{
		$this->assertEquals(self::HMAC_SHA1_RESULT, HealthVaultHelper::getHmacSha1(
					base64_encode(self::SHARED_SECRET), self::SHARED_SECRET));
	}
	
	public function testGetTimestampFormat()
	{
		// since the timestamp always changes, the best we can do is make sure it looks valid
		// and can be parsed
		$timestamp = HealthVaultHelper::getTimestamp();
		$this->assertRegexp('/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.01Z/', $timestamp);
		$this->assertNotEquals(false, date_parse($timestamp));
	}
}
?>