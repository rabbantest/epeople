<?php
/**
 * This file serves as the base include file for the PHP-HealthVault library.  It includes a few defined constants and some includes so the library can be accessed easily in any PHP script.  Alteration of these constants may be required for the library to find its members.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Globals
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * This is the only constant that might need attention; this should point to the full /path/to/library of the PHP-HealthVault API library.
 * This path should NOT have a slash at the end.
 *
 * @name HV_BASE_PATH
 */
define('HV_BASE_PATH', realpath(dirname(__FILE__)));

/**
 * Include global functions
 */
require_once(HV_BASE_PATH . '/globals/functions.php');

if (function_exists('__autoload'))
{
    spl_autoload_register('__autoload');
}

spl_autoload_register('healthvault_autoload');

?>