<?php
/**
 * This file houses the ResponseFactory class definition.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Responses
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The ResponseFactory class reads the incoming XML payload, determines the correct response object to create, and returns it.
 *
 * @category  PHP-Library
 * @package   HealthVault
 * @author    Dave Kiger <noemail@noneto.us>
 * @copyright 2008 TelaDoc, Inc.
 * @license   https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link      https://sourceforge.net/projects/healthvaultphp
 */
class ResponseFactory
{

    /**
     * The raw XML received from HealthVault.
     *
     * @var string
     */
    protected $rawResponseXml;
    
    /**
     * The method that was sent to generate the response
     *
     * @var string 
     *
     */
    protected $methodName;
    
    /**
     * A map of what request methods are handled by what response factory
     * 
     * Each supported method needs to map to a valid class name
     */
    private static $methodMap = 
        array(
        "GetPersonInfo" => "PersonResponseFactory",
        "GetThings"     => "GetThingsResponseFactory",
        "PutThings"     => "PutThingsResponseFactory",
        "RemoveThings"  => "RemoveThingsResponseFactory",
        "GetVocabulary" => "GetVocabularyResponseFactory"
        );

    /**
     * Object constructor.
     *
     * @todo currently just a stub
     *
     * @param string $rawResponseXml the raw XML received from HealthVault.
     *
     * @return ResponseFactory
     */
    public function __construct($rawResponseXml, $methodName)
    {
        $this->rawResponseXml = (string)$rawResponseXml;
        $this->methodName     = (string)$methodName;
    }

    /**
     * Returns an object which implements the IHealthVaultResponse interface.
     *
     * @return IHealthVaultResponse
     */
    public function getResponseObject()
    {
        if(isset(self::$methodMap[$this->methodName]))
        {
            return call_user_func(array(self::$methodMap[$this->methodName],'getResponseObject'),
                $this->rawResponseXml,$this->methodName);
        }
        throw new NotImplementedException("No response factory associated with {$this->methodName}");
    }

}

?>