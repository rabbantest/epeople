<?php
/**
 * This file houses the HealthVaultResponseFactory class definition.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Responses
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The HealthVaultResponseFactory class reads the incoming XML payload, determines the correct response object to create, and returns it.
 *
 * @category  PHP-Library
 * @package   HealthVault
 * @author    Dave Kiger <noemail@noneto.us>
 * @copyright 2008 TelaDoc, Inc.
 * @license   https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link      https://sourceforge.net/projects/healthvaultphp
 */
class HealthVaultResponseFactory
{
    
    /**
     * The raw XML response
     *
     * @var string 
     *
     */
    protected $rawXML     = null;
    
    /**
     * The name of the method that was called to generate this response
     *
     * @var string 
     *
     */
    protected $methodName = null;
    
    /**
     * An internal array that holds the mapping from methods to what ResponseFactory processes them
     *
     * @var array 
     *
     */
    protected static $methodToFactorMap = 
        array(
            "GetPersonInfo"   => "PersonResponseFactory",
            "GetThings"       => "GetThingsResponseFactory",
            "RemoveThings"    => "RemoveThingsResponseFactory",
            "PutThings"       => "PutThingsResponseFactory",
            "OverwriteThings" => "OverwriteThingsResponseFactory"
        );
        
        
    /**
     * Constructor the builds the factory with the provided xml and method initialized
     *
     * @param string $rawXML     The XML response that will be parsed
     * @param string $methodName The name of the method from which the response was generated
     * @return void 
     *
     */
    public function __construct($rawXML, $methodName)
    {
        $this->rawXML     = $rawXML;
        $this->methodName = $methodName;
    }
    
    
    /**
     * Gets the IHealthVaultResponse object for the resposne
     *
     * @return IHealthVaultResponse The response object for the response xml
     *
     */
    public function getResponseObj()
    {
        $responseFactory = new self::$methodToFactorMap[$this->methodName]();
        return $responseFactory->getResponseObj();
    }
}

?>