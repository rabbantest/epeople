<?php

/**
 * Response object from GetThings requests
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Responses
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

class GetThingsResponseObject implements IHealthVaultResponse
{
    
    /**
     * An array of ThingResponseGroups holding all the response data separated by group
     *
     * @var array[int]ThingResponseGroup 
     *
     */
    protected $thingResponseGroups = array();
    
    /**
     * Constructs the object with the provided ThingResponseGroup array (if given)
     *
     * @param array $groups The thing response group array
     * @return void 
     *
     */
    public function __construct(array $groups=null)
    {
        if($groups != null)
        {
            if(count($groups) < 1)
            {
                throw new InvalidParameterException('Must provide at least one group');
            }
            foreach($groups as $group)
            {
                if(!is_a($groups, 'ThingResponseGroup'))
                {
                    throw new InvalidParameterException('All elements in the thing key array '
                            . 'must be ThingResponseGroups');
                }
            }
            $this->thingResponseGroups = $groups;
        }
    }
    
    /**
     * Magic function to allow access to the groups via 'properties'
     *
     * @param string $key The requested member
     * @return mixed The value of that member
     *
     */
    public function __get($key)
    {
        switch ($key)
        {
            case "groups":
            case "Groups":
            case "thingResponseGroups":
            case "ThingResponseGroups":
                return $this->getThingResponseGroups();
        }
    }
    
    /**
     * Gets the response groups array
     *
     * @return array The array of thing response groups
     *
     */
    public function getThingResponseGroups()
    {
        return $this->thingResponseGroups;
    }
    
    /**
     * Static method to populate this object from an input SimpleXMLElement
     *
     * @param SimpleXMLElement $xmlObj The XML object to use for parsing
     * @return GetThingsResponseObject The object from parsing the given xml
     *
     */
    public static function fromXML(SimpleXMLElement $xmlObj)
    {
        $ptrs = new GetThingsResponseObject();
        $ptrs->parseXML($xmlObj);
        return $ptrs;
    }
    
    /**
     * Parses the given XML object and sets internal values from it
     *
     * @param SimpleXMLElement $xmlObj The xml object to parse (pointed at the wc:info element)
     * @return void 
     *
     */
    private function parseXML(SimpleXMLElement $xmlObj)
    {
        $groupElements = $xmlObj->xpath('group');
        if($groupElements === false || count($groupElements) < 1)
        {
            throw new InvalidParameterException('Must contain at least one group');
        }
        foreach($groupElements as $group)
        {
            $this->thingResponseGroups[] = ThingResponseGroup::fromXML($group);
        }
    }
}
?>