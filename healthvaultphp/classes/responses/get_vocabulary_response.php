<?php

class GetVocabularyResponse implements IHealthVaultResponse
{

    public static function fromXml($rawXml)
    {
        var_dump($rawXml); exit;
        $obj = simplexml_load_string($rawXml);
        $xml = array();
        convertXmlObjToArr($obj, $xml);
        $obj = new PutThingsResponse();
        $obj->parseXml($xml);
        return $obj;
    }

    private function parseXml(array $xml)
    {
        $thingIds = array();
        foreach ($xml as $element)
        {
            if ($element['@name'] == 'info')
            {
                foreach ($element['@children'] as $children)
                {
                    if ($children['@name'] == 'thing-id')
                    {
                        $thingIds[] = new ThingKey(new Guid($children['@text']), new Guid($children['@attributes']['version-stamp']));
                    }
                }
            }
        }
        $this->thingIds = $thingIds;
    }

}

?>