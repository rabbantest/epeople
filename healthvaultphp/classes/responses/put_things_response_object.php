<?php

/**
 * Response object from PutThings requests
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Responses
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

class PutThingsResponseObject implements IHealthVaultResponse
{
    /**
     * The array of thing keys that come from the put things request
     * 
     * @var array[int]ThingKey
     */
    protected $thingKeys = array();
    
    /**
     * Constructor from which the array of thing keys can be set
     *
     * @param array $thingKeys The array of thing keys to use if set
     * @return void 
     *
     */
    public function __construct(array $thingKeys=null)
    {
        if($thingKeys != null)
        {
            if(count($thingKeys) < 1)
            {
                throw new InvalidParameterException('Must provide at least one thing key');
            }
            foreach($thingKeys as $thingKey)
            {
                if(!is_a($thingKey, 'ThingKey'))
                {
                    throw new InvalidParameterException('All elements in the thing key array '
                            . 'must be Thing keys');
                }
            }
            $this->thingKeys = $thingKeys;
        }
    }
    
    /**
     * Magic method to allow access to ThingKeys as a property
     *
     * @param string $key The requested memeber
     * @return mixed The value of that memeber
     *
     */
    public function __get($key)
    {
        switch ($key)
        {
            case "thingKeys":
            case "ThingKeys":
                return $this->getThingKeys();
        }
    }
    
    /**
     * Gets the array of thing keys for this object
     *
     * @return array The ThingKeys from the response in an array
     *
     */
    public function getThingKeys()
    {
        return $this->thingKeys;
    }
    
    /**
     * Static method to populate this object from an input SimpleXMLElement
     *
     * @param SimpleXMLElement $xmlObj The XML object to use for parsing
     * @return GetThingsResponseObject The object from parsing the given xml
     *
     */
    public static function fromXML(SimpleXMLElement $xmlObj)
    {
        $ptrs = new PutThingsResponseObject();
        $ptrs->parseXML($xmlObj);
        return $ptrs;
    }
      
    /**
     * Parses the given XML object and sets internal values from it
     *
     * @param SimpleXMLElement $xmlObj The xml object to parse (pointed at the wc:info element)
     * @return void 
     *
     */
    private function parseXML(SimpleXMLElement $xmlObj)
    {
        $thingKeyElements = $xmlObj->xpath('thing-id');
        if($thingKeyElements === false || count($thingKeyElements) < 1)
        {
            throw new InvalidParameterException('Must contain at least one thing-id');
        }
        foreach($thingKeyElements as $thingKeyObj)
        {
            $this->thingKeys[] = ThingKey::fromXML($thingKeyObj);
        }
    }
}
?>