<?php

/**
 * Response object from GetPersonInfo requests
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Responses
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

class PersonInfoResponseObject implements IHealthVaultResponse
{
    
    /**
     * The unique identifyer for the person
     *
     * @var Guid 
     *
     */
    protected $personId;
    
    /**
     * The name of the person
     *
     * @var string 
     *
     */
    protected $name;
    
    /**
     * The app-specific settings xml
     *
     * @var string 
     *
     */
    protected $appSettings;
    
    /**
     * The id of the record selected for this person for the current application
     *
     * @var Guid 
     *
     */
    protected $selectedRecordId;
    
    /**
     * Whether or not more records exist for the person
     *
     * @var bool 
     *
     */
    protected $moreRecords;
    
    /**
     * The records that a person has authorized the application to use
     *
     * @var array 
     *
     */
    protected $record;
    
    /**
     * The set of groups the person is a member of
     *
     * @var string 
     *
     */
    protected $groups;
    
    /**
     * The preferred culture for use with comparisons, collation, currency, dates, etc.
     *
     * @var string 
     *
     */
    protected $preferredCulture;
    
    /**
     * The users's preferred UI culture for whoing the user images and text
     *
     * @var string 
     *
     */
    protected $preferredUICulture;
    
    /**
     * Constructor that takes in any/all parameters and sets them. Only this way or by parsing XML can the values be set
     *
     * @param Guid    $personId           The person's id
     * @param string  $name               The Person's name
     * @param string  $appSettings        The app-specific settings for this person (XML)
     * @param Guid    $selectedRecordId   The id of the selected record
     * @param bool    $moreRecords        Whether or not there are more records to return
     * @param array   $record             The records that were returned in an array
     * @param string  $groups             The groups the person belongs to
     * @param Culture $preferredCulture   The preferred culture to use in comparisons, etc
     * @param Culture $preferredUICulture The perferred culture for images and text
     * @return void 
     *
     */
    public function __construct(Guid $personId=null, $name=null, $appSettings=null, Guid $selectedRecordId=null, $moreRecords=null, array $record=null, $groups=null, Culture $preferredCulture=null, Culture $preferredUICulture=null)
    {
        $this->personId           = $personId;
        $this->name               = (string)$name;
        $this->appSettings        = (string)$appSettings;
        $this->selectedRecordId   = $selectedRecordId;
        $this->moreRecords        = (bool)$moreRecords;
        if($record != null)
        {
            foreach($record as $rec)
            {
                if(!is_a($rec, 'Record'))
                {
                    throw new InvalidParameterException("Record must be an array of Record objects");
                }
            }
        }
        $this->record             = $record==null?array():$record;
        $this->groups             = (string)$groups;
        $this->preferredCulture   = $preferredCulture;
        $this->preferredUICulture = $preferredUICulture;
    }
    
    /**
     * Magic method to retrieve member values
     *
     * @param string $key The member requested
     * 
     * @uses PersonInfoResponseObject::getPersonId()
     * @uses PersonInfoResponseObject::getName()
     * @uses PersonInfoResponseObject::getAppSettings()
     * @uses PersonInfoResponseObject::getSelectedRecordId()
     * @uses PersonInfoResponseObject::getIsMoreRecords()
     * @uses PersonInfoResponseObject::getRecords()
     * @uses PersonInfoResponseObject::getGroups()
     * @uses PersonInfoResponseObject::getPreferredCulture()
     * @uses PersonInfoResponseObject::getPreferredUICulture()
     * 
     * @return mixed The value of that member
     *
     */
    public function __get($key)
    {
        switch ($key)
        {
            case "personId":
            case "personID":
            case "PersonId":
            case "PersonID":
                return $this->getPersonId();
            case "name":
            case "Name":
                return $this->getName();
            case "appSettings":
            case "AppSettings":
                return $this->getAppSettings();
            case "selectedRecordId":
            case "selectedRecordID":
            case "SelectedRecordId":
            case "SelectedRecordID":
                return $this->getSelectedRecordId();
            case "isMoreRecords":
            case "IsMoreRecords":
                return $this->getIsMoreRecords();
            case "records":
            case "Records":
                return $this->getRecords();
            case "groups":
            case "Groups":
                return $this->getGroups();
            case "preferredCulture":
            case "PreferredCulture":
                return $this->getPreferredCulture();
            case "preferredUICulture":
            case "PreferredUICulture":
                return $this->getPreferredUICulture();
        }
    }
    
    /**
     * Returns the person id
     *
     * @return Guid The person's id
     *
     */
    public function getPersonId()
    {
        return $this->personId;
    }
    
    /**
     * Returns the person's name
     *
     * @return string The person's name
     *
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Gets the App settings XML
     *
     * @return string The app settings xml
     *
     */
    public function getAppSettings()
    {
        return $this->appSettings;
    }
    
    /**
     * Gets the id of the selected record
     *
     * @return Guid The selected record's id
     *
     */
    public function getSelectedRecordId()
    {
        return $this->selectedRecordId;
    }
    
    /**
     * Gets whether or not there are more records
     *
     * @return bool Whether or not there are more records
     *
     */
    public function getIsMoreRecords()
    {
        return $this->moreRecords;
    }
    
    /**
     * Gets the records for this person in an array
     *
     * @return array The records for this person
     *
     */
    public function getRecords()
    {
        return $this->record;
    }
    
    /**
     * Gets the groups for this person
     *
     * @return string The groups for the person
     *
     */
    public function getGroups()
    {
        return $this->groups;
    }
    
    /**
     * Gets the person's preferred culture
     *
     * @return Culture The person's preferred culture
     *
     */
    public function getPreferredCulture()
    {
        return $this->preferredCulture;
    }
    
    /**
     * Gets the person's preferred UI culture
     *
     * @return Culture The person's preferred UI culture
     *
     */
    public function getPreferredUICulture()
    {
        return $this->preferredUICulture;
    }
    
    /**
     * Parses the xml from the input object into values for the person
     *
     * @param SimpleXMLElement $personXmlObj The XML object to read and parse
     * 
     * @uses Record::fromXML()
     * @return void
     *
     */
    private function parseXML(SimpleXMLElement $xmlObj)
    {
        $personXmlObjs = $xmlObj->xpath('person-info');
        if($personXmlObjs === false || count($personXmlObjs) < 1)
        {
            throw new InvalidParameterException('XML must conform to HealthVault response schemas');
        }
        $personXmlObj = $personXmlObjs[0];
        $this->personId = new Guid((string)$personXmlObj->{'person-id'});
        $this->name = (string)$personXmlObj->name;
        if(isset($personXmlObj->{'app-settings'}))
        {
            $this->appSettings = '';
            foreach($personXmlObj->{'app-settings'}->xpath('*') as $node)
            {
                $this->appSettings .= $node->asXML();
            }
        }
        if(isset($personXmlObj->{'selected-record-id'}))
        {
            $this->selectedRecordId = new Guid((string)$personXmlObj->{'selected-record-id'});
        }
        if(isset($personXmlObj->{'more-records'}))
        {
            $this->moreRecords = $personXmlObj->{'more-records'} == "true" || 
                $personXmlObj->{'more-records'} == '1'; 
        }
        
        $this->record = array();
        foreach($personXmlObj->xpath('record') as $record)
        {
            $this->record[] = Record::fromXML($record);
        }
        
        if(isset($personXmlObj->groups))
        {
            $this->groups = (string)$personXmlObj->groups;
        }
        if(isset($personXmlObj->{'preferred-culture'}))
        {
            $this->preferredCulture = new Culture((string)$personXmlObj->{'preferred-culture'}->language,
                (string)$personXmlObj->{'preferred-culture'}->country);
        }
        if(isset($personXmlObj->{'preferred-uiculture'}))
        {
            $this->preferredUICulture = new Culture((string)$personXmlObj->{'preferred-uiculture'}->language,
                (string)$personXmlObj->{'preferred-uiculture'}->country);
        }
    }
    
    /**
     * Creates a PersonInfoResponseObject from the input XML
     *
     * @param SimpleXMLElement $xmlObj The XML object to parse
     * 
     * @uses PersonInfoResponseObject::parseXML()
     * 
     * @return PersonInfoResponseObject The object yielded from parsing the XML
     *
     */
    public static function fromXML(SimpleXMLElement $xmlObj)
    {
        $personObj = new PersonInfoResponseObject();
        $personObj->parseXML($xmlObj);
        return $personObj;
    }
}
?>