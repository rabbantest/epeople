<?php
/**
 * This file houses the HealthVaultHelper class definition.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Helpers
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * We need the PEAR Crypt_HMAC package.
 */
require_once('Crypt/HMAC.php');

/**
 * The HealthVaultHelper object contains a number of static methods which are used throughout the library.
 *
 * @category  PHP-Library
 * @package   HealthVault
 * @author    Dave Kiger <noemail@noneto.us>
 * @copyright 2008 TelaDoc, Inc.
 * @license   https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link      https://sourceforge.net/projects/healthvaultphp
 */
class HealthVaultHelper
{

    /**
     * Fetchs the application ID and stores it in the $appId property.
     *
     * @uses Settings::getInstance();
     * @uses Settings::get()
     *
     * @return void
     */
    static public function getAppId()
    {
        $obj = HvSettings::getInstance();
        return $obj->get('authentication', 'app_id');
    }

    /**
     * Reads the thumbprint file defined in the settings and stored in the keys directory.  Stores the value in the $thumbPrint property.
     *
     * @uses File::read()
     * @uses Settings::getInstance();
     * @uses Settings::get()
     *
     * @return void
     */
    static public function getThumbPrint()
    {
        $obj = HvSettings::getInstance();
        return $obj->get('authentication', 'thumbprint');
    }

    /**
     * Reads the key file defined in the settings and stored in the keys directory.  Converts the key using openssl_pkey_get_private() function and Stores the value in the $privateKey property.
     *
     * @uses File::read()
     * @uses Settings::getInstance();
     * @uses Settings::get()
     *
     * @return resource The private key
     */
    static public function getPrivateKey($keyFilePath=null)
    {
		if($keyFilePath == null)
		{
			$obj = HvSettings::getInstance();
			$keyFile = $obj->get('authentication', 'key_file');
		}
        try
        {
			$file = $keyFilePath == null ? new File(HV_BASE_PATH . '/authentication/' . $keyFile) : new File($keyFilePath);
            $keyData = $file->read();
        }
        catch (FileNotFoundException $e)
        {
            throw new FileNotFoundException('Unable to load key from file.  ' . $e->getMessage());
        }
		catch (FileNotReadableException $e)
		{
			throw new FileNotReadableException('Unable to load key from file. ' . $e->getMessage());
		}
        $privateKey = openssl_pkey_get_private($keyData);
        if ($privateKey === false)
        {
            throw new UnableToExtractPrivateKeyException('Unable to generate private encryption key from file.');
        }
        return $privateKey;
    }

    /**
     * Converts a string into an HMAC SHA1 digest (base64 encoded).
     *
     * @param string $secret the secret code to use to create the digest
     * @param string $str    the string to convert to an HMAC digest
     *
     * @return string
     */
    static public function createDigest($secret, $str)
    {
        $obj = new Crypt_HMAC($secret, 'sha1');
        return base64_encode($obj->hash($str));
    }

    /**
     * Returns a base64 encoded hash digest of a given string.
     *
     * @param string $str the string you want a hash digest of
     *
     * @return string
     */
    static public function getHashDigest($str)
    {
        return base64_encode(sha1($str, true));
    }

    /**
     * Returns the base64 encoded HMAC-SHA1 of the provided string.
     *
     * @param string $key the base64 encoded key
     * @param string $str the string to create the HMAC-SHA1 of
     *
     * @return string
     */
    static public function getHmacSha1($key, $str)
    {
        return base64_encode(hash_hmac('sha1', $str, base64_decode($key), true));
    }

    /**
     * Returns the special UTC timestamp.
     *
     * @return string
     */
    static public function getTimestamp()
    {
        return gmdate("Y-m-d\TH:i:s") . '.001';
    }
    
    /**
     * Returns true if the date matches the special UTC timestamp.
     *
     * @param string $date the UTC date
     *
     * @return integer
     */
    static public function validateTimestamp($date)
    {
        return preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2})\.([0-9]{1,3})$/", $date);
    }
    
    /**
     * Wrapper for the GetThings request.
     *
     * @param array  $thingTypeIds simple array of thing type ID's that you want to fetch; must be implemented by the library; should all be strings (not Guid objects)
     * @param string $recordId the record ID from which you want the things pulled from
     * @param string $personId the person ID from which you want things pulled from
     *
     * @return GetThingsResponseObject
     */
    static public function getThings(array $thingTypeIds, $recordId, $personId, $wcToken = null)
    {
        $recordIdObj = new Guid($recordId);
        $personIdObj = new Guid($personId);
        
        $thingTypeGuids = array();
        foreach ($thingTypeIds as $thingTypeId)
        {
            $thingTypeGuids[] = new Guid($thingTypeId);
        }
        
        // now we need to get the Personal Contact Information thing from HV
        // setup a thing request group, which tells HV what "things" we want and what format we want them in, in our case, XML
        $trg = new ThingRequestGroup();
        $trg->format = new ThingFormatSpec( array ( new ThingSectionSpec(ThingSectionSpec::CORE) ) );
        $trg->filter = array ( new ThingFilterSpec( $thingTypeGuids ) );

        // do a GetThings request
        $proxy = new HealthVaultProxy( HealthVaultConnection::getInstance() );
        $getThingsResponseObj = $proxy->call( new GetThingsRequest( array ( $trg ), $recordIdObj, $personIdObj, $wcToken ) );
        
        return $getThingsResponseObj;
    }
    
    static public function putThings(array $things, $recordId, $personId, $wcToken = null)
    {
        $recordIdObj = new Guid($recordId);
        $personIdObj = new Guid($personId);
        
        $proxy = new HealthVaultProxy( HealthVaultConnection::getInstance() );
        $obj = $proxy->call( new PutThingsRequest( $things, $recordIdObj, $personIdObj ) );
        
        return $obj;
    }

}

?>