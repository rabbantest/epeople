<?php
/**
 * @todo This file needs to be documented.
 */

class PersonalContactInformation extends Thing
{
    
    public $contact;
    
    const ID = '162dd12d-9859-4a66-b75f-96760d67072b';
    
    public function __construct($thingId = null)
    {
        $typeId = new ThingType();
        $typeId->id = new Guid(PersonalContactInformation::ID);
        $typeId->name = 'Personal Contact Information';
        parent::__construct($thingId, $typeId);
    }
    
    public function validateMe()
    {
        libxml_use_internal_errors(true);
        $xml = $this->writeXml('wc-contact:contact', true);
        //echo $xml; exit;
        $xsd = HV_BASE_PATH . '/xsds/personal-contact-information.xsd';
        $xdoc = new DomDocument();
        $xdoc->loadXML($xml);
        $response = true;
        if (!$xdoc->schemaValidate($xsd))
        {
            $errors = libxml_get_errors();
            $response = 'PersonalContactInformation Validation Errors:<br><ul>';
            foreach ($errors as $error)
            {
                $response .= '<li>' . $error->line . ':' . $error->column . ' -- ' . $error->message . "</li>";
            }
            $response .= '</ul>';
        }
        return $response;
    }
    
    public function writeMoreXml(XmlWriter $xw)
    {
        $xw->writeRaw($this->getAsXml('data-xml'));
    }
    
    public function getAsXml($elementName, $forValidation = false)
    {
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($elementName);
        if ($forValidation)
        {
            $x->writeAttribute('xmlns:wc-contact', 'urn:com.microsoft.wc.thing.contact');
        }
        else
        {
            $x->startElement('contact');
        }
        if ($this->contact != null)
        {
            $x->writeRaw($this->contact->writeXml('contact'));
        }
        if (!$forValidation)
        {
            $x->endElement();
        }
        $x->endElement();
        return $x->flush();
    }
    
    /**
     * Actually parses the XML and sets the internal variables
     *
     * @param SimpleXMLElement $xmlObj The XML to parse
     * @return void
     *
     */
    protected function parseXml(SimpleXMLElement $xmlObj)
    {
        if (isset($xmlObj->{'thing-id'}))
        {
            $versionStamp = '';
            foreach ($xmlObj->{'thing-id'}->attributes() as $a => $b)
            {
                if ($a == 'version-stamp')
                {
                    $versionStamp = (string)$b;
                }
            }
            $this->setThingId(new Guid((string)$xmlObj->{'thing-id'}), new Guid($versionStamp));
        }
        if (isset($xmlObj->{'thing-state'}))
        {
            $this->setThingState(new ThingState((string)$xmlObj->{'thing-state'}));
        }
        if (isset($xmlObj->{'eff-date'}))
        {
            $this->setEffDate((string)$xmlObj->{'eff-date'});
        }
        if (isset($xmlObj->{'data-xml'}->contact->contact))
        {
            $contact = $xmlObj->{'data-xml'}->contact->contact;
            $this->contact = $this->parseContactXml($contact);
        }
    }
    
    private function parseContactXml($contact)
    {
        $contactObj = new Contact();
        if (isset($contact->email))
        {
            $contactObj->email = $this->parseEmailXml($contact->email);
        }
        else
        {
            $contactObj->email = array();
        }
        if (isset($contact->address))
        {
            $contactObj->address = $this->parseAddressXml($contact->address);
        }
        else
        {
            $contactObj->address = array();
        }
        if (isset($contact->phone))
        {
            $contactObj->phone = $this->parsePhoneXml($contact->phone);
        }
        else
        {
            $contactObj->phone = array();
        }
        return $contactObj;
    }
    
    private function parseAddressXml($addressXml)
    {
        $addressArr = array();
        if (count($addressXml) > 1)
        {
            foreach ($addressXml as $address)
            {
                $obj = new Address();
                if (isset($address->{'is-primary'}))
                {
                    if ((string)$address->{'is-primary'} == 'true')
                    {
                        $obj->isPrimary = true;
                    }
                    else
                    {
                        $obj->isPrimary = false;
                    }
                }
                if (isset($address->description))
                {
                    $obj->description = (string) $address->description;
                }
                if (isset($address->city))
                {
                    $obj->city = (string) $address->city;
                }
                if (isset($address->state))
                {
                    $obj->state = (string) $address->state;
                }
                if (isset($address->postcode))
                {
                    $obj->postcode = (string) $address->postcode;
                }
                if (isset($address->country))
                {
                    $obj->country = (string) $address->country;
                }
                if (isset($address->street))
                {
                    $streetArr = array();
                    if (count($address->street) > 1)
                    {
                        foreach ($address->street as $street)
                        {
                            $streetArr[] = (string) $street;
                        }
                    }
                    else
                    {
                        $streetArr[] = (string) $address->street;
                    }
                    $obj->street = $streetArr;
                }
                $addressArr[] = $obj;
            }
        }
        else
        {
            $address = $addressXml;
            $obj = new Address();
            if (isset($address->{'is-primary'}))
            {
                if ((string)$address->{'is-primary'} == 'true')
                {
                    $obj->isPrimary = true;
                }
                else
                {
                    $obj->isPrimary = false;
                }
            }
            if (isset($address->description))
            {
                $obj->description = (string) $address->description;
            }
            if (isset($address->city))
            {
                $obj->city = (string) $address->city;
            }
            if (isset($address->state))
            {
                $obj->state = (string) $address->state;
            }
            if (isset($address->postcode))
            {
                $obj->postcode = (string) $address->postcode;
            }
            if (isset($address->country))
            {
                $obj->country = (string) $address->country;
            }
            $streetArr = array();
            if (isset($address->street))
            {
                if (count($address->street) > 1)
                {
                    foreach ($address->street as $street)
                    {
                        $streetArr[] = (string) $street;
                    }
                }
                else
                {
                    $streetArr[] = (string) $address->street;
                }
            }
            $obj->street = $streetArr;
            $addressArr[] = $obj;
        }
        return $addressArr;
    }
    
    private function parsePhoneXml($phoneXml)
    {
        $phoneArr = array();
        if (count($phoneXml) > 1)
        {
            foreach ($phoneXml as $phone)
            {
                $obj = new Phone();
                if (isset($phone->{'is-primary'}))
                {
                    if ((string)$phone->{'is-primary'} == 'true')
                    {
                        $obj->isPrimary = true;
                    }
                    else
                    {
                        $obj->isPrimary = false;
                    }
                }
                if (isset($phone->description))
                {
                    $obj->description = (string) $phone->description;
                }
                if (isset($phone->number))
                {
                    $obj->number = (string) $phone->number;
                }
                $phoneArr[] = $obj;
            }
        }
        else
        {
            $obj = new Phone();
            if (isset($phoneXml->{'is-primary'}))
            {
                $obj->isPrimary = (bool) $phoneXml->{'is-primary'};
            }
            if (isset($phoneXml->description))
            {
                $obj->description = (string) $phoneXml->description;
            }
            if (isset($phoneXml->number))
            {
                $obj->number = (string) $phoneXml->number;
            }
            $phoneArr[] = $obj;
        }
        return $phoneArr;
    }
    
    private function parseEmailXml($emailXml)
    {
        $emailArr = array();
        if (count($emailXml) > 1)
        {
            foreach ($emailXml as $email)
            {
                $obj = new HvEmail();
                if (isset($email->{'is-primary'}))
                {
                    if ((string)$email->{'is-primary'} == 'true')
                    {
                        $obj->isPrimary = true;
                    }
                    else
                    {
                        $obj->isPrimary = false;
                    }
                }
                if (isset($email->description))
                {
                    $obj->description = (string) $email->description;
                }
                if (isset($email->address))
                {
                    $obj->address = (string) $email->address;
                }
                $emailArr[] = $obj;
            }
        }
        else
        {
            $obj = new HvEmail();
            if (isset($emailXml->{'is-primary'}))
            {
                $obj->isPrimary = (bool) $emailXml->{'is-primary'};
            }
            if (isset($emailXml->description))
            {
                $obj->description = (string) $emailXml->description;
            }
            if (isset($emailXml->address))
            {
                $obj->address = (string) $emailXml->address;
            }
            $emailArr[] = $obj;
        }
        return $emailArr;
    }
    
}


?>