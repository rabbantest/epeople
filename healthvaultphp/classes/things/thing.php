<?php
/**
 * A holder for Thing Key entries for xml creation
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Complex-Types
 * @author     Patrick Sharp <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The Thing is used in a GetThings request.
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Complex-Types
 * @author     Patrick Sharp <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class Thing extends ComplexType
{

    /**
     * A ThingKey object.
     *
     * @var ThingKey
     */
    protected $thingId;
    
    /**
     * A ThingType object.
     *
     * @var ThingType
     */
    protected $typeId;
    
    /**
     * A ThingState object.
     *
     * @var ThingState
     */
    protected $thingState;
    
    /**
     * A hardcoded string value.
     *
     * @var string
     */
    protected $flags;
    
    /**
     * A string holding a dateTime.
     *
     * @var string
     */
    protected $effDate;
    
    /**
     * An Audit object.
     *
     * @var Audit
     */
    protected $created;
    
    /**
     * An Audit object.
     *
     * @var Audit
     */
    protected $updated;
    
    /**
     * An array of DataXml objects.
     *
     * @var array
     */
    protected $dataXml;
    
    /**
     * A DataOther object.
     *
     * @var DataOther
     */
    protected $dataOther;
    
    /**
     * An EffectivePermissions object.
     *
     * @var EffectivePermissions
     */
    protected $effPermissions;
    
    protected $versionStamp = null;
    
    /**
     * A string of tags.
     *
     * @var string
     */
    protected $tags;
    
    public static $idMap = array
    (
        '162dd12d-9859-4a66-b75f-96760d67072b' => 'PersonalContactInformation',
        '92ba621e-66b3-4a01-bd73-74844aed4f5b' => 'PersonalDemographicInformation'
    );
    
    protected function setVersionStamp($versionStamp)
    {
        if (false == ($versionStamp instanceof Guid))
        {
            throw new InvalidParameterException('Version stamp should be a Guid object');
        }
        $this->versionStamp = $versionStamp;
    }
    
    /**
     * Object constructor.
     *
     * @param array  $thingId a ThingKey object
     * @param array  $typeId  a ThingType object
     *
     * @return ThingRequestGroup
     */
    public function __construct($thingId = null, $typeId = null)
    {
        if($thingId == null)
        {
            $this->thingId = null;
        }
        else
        {
            $this->setThingId($thingId);
        }
        
        if($typeId == null)
        {
            $this->typeId = null;
        }
        else
        {
            $this->setTypeId($typeId);
        }
        
        $this->thingState       = null;
        $this->flags            = null;
        $this->effDate          = null;
        $this->created          = null;
        $this->updated          = null;
        $this->dataXml          = null;
        $this->dataOther        = null;
        $this->effPermissions   = null;
        $this->tags             = '';
    }

    /**
     * Sets the thingId property.
     *
     * @param ThingId $thingId a ThingId object
     *
     * @return void
     */
    protected function setThingId($thingId, $versionStamp = null)
    {
        if(false == ($thingId instanceof Guid))
        {
            throw new InvalidParameterException('ThingId must be a Guid object');
        }
        if ($versionStamp != null)
        {
            if(false == ($versionStamp instanceof Guid))
            {
                throw new InvalidParameterException('Version Stamp must be a Guid object');
            }
            $this->versionStamp = $versionStamp;
        }
        $this->thingId = $thingId;
    }
    
    
    /**
     * Sets the thingId property.
     *
     * @param ThingId $thingId a ThingId object
     *
     * @return void
     */
    protected function setTypeId($typeId)
    {
        if(false == ($typeId instanceof ThingType))
        {
            throw new InvalidParameterException('TypeId must be a ThingType object');
        }
        $this->typeId = $typeId;
    }
    
    /**
     * Sets the typeState property.
     *
     * @param TypeState $typeState a TypeState object
     *
     * @return void
     */
    protected function setTypeState($typeState)
    {
        if(false == ($typeState instanceof TypeState))
        {
            throw new InvalidParameterException('TypeState must be a TypeState object');
        }
        $this->typeState = $typeState;
    }
    
    /**
     * Sets the thingState property.
     *
     * @param ThingState $thingState a ThingState object
     *
     * @return void
     */
    protected function setThingState($thingState)
    {
        if(false == ($thingState instanceof ThingState))
        {
            throw new InvalidParameterException('ThingState must be a ThingState object');
        }
        $this->thingState = $thingState;
    }
    
    /**
     * Sets the effDate property.
     *
     * @param string $effDate a string containing the date
     *
     * @return void
     */
    protected function setEffDate($effDate)
    {
        //echo $effDate; exit;
        if(HealthVaultHelper::validateTimestamp($effDate) == 0)
        {
            throw new InvalidParameterException('effDate must be in 2008-01-01T13:45:23.001 format');
        }
        $this->effDate = $effDate;
    }
    
    /**
     * Sets the created property.
     *
     * @param Audit $created an Audit object
     *
     * @return void
     */
    protected function setCreated($created)
    {
        if(false == ($created instanceof Audit))
        {
            throw new InvalidParameterException('created must be a Audit object');
        }
        $this->created = $created;
    }
    
    /**
     * Sets the updated property.
     *
     * @param Audit $updated an Audit object
     *
     * @return void
     */
    protected function setUpdated($updated)
    {
        if(false == ($updated instanceof Audit))
        {
            throw new InvalidParameterException('created must be a Audit object');
        }
        $this->updated = $updated;
    }
    
    /**
     * Sets the dataXml property.
     *
     * @param array $dataXml array of DataXml objects
     *
     * @return void
     */
    protected function setDataXml($dataXml)
    {
        if(is_array($dataXml))
        {
            foreach ($dataXml as $dx)
            {
                if (false == ($dx instanceof DataXml))
                {
                    throw new InvalidParameterException('DataXml must be an array of DataXml objects');
                }
            }
        }
        else
        {
            throw new InvalidParameterException('DataXml must be an array of DataXml objects');
        }
        $this->dataXml = $dataXml;
    }
    
    /**
     * Sets the dataOther property.
     *
     * @param DataOther $dataOther an DataOther object
     *
     * @return void
     */
    protected function setDataOther($dataOther)
    {
        if(false == ($dataOther instanceof DataOther))
        {
            throw new InvalidParameterException('dataOther must be a DataOther object');
        }
        $this->dataOther = $dataOther;
    }
    
    /**
     * Sets the effPermisssions property.
     *
     * @param EffectivePermissions $effPermisssions an EffectivePermissions object
     *
     * @return void
     */
    protected function setEffPermissions($effPermisssions)
    {
        if(false == ($effPermisssions instanceof EffectivePermissions))
        {
            throw new InvalidParameterException('effPermisssions must be a EffectivePermissions object');
        }
        $this->effPermisssions = $effPermisssions;
    }
    
    /**
     * Sets the tags property.
     *
     * @param string $tags a string containing the tags
     *
     * @return void
     */
    protected function setTags($tags)
    {
        if(!is_string($tags))
        {
            throw new InvalidParameterException('tags must be a string');
        }
        $this->tags = $tags;
    }
    
    /**
     * Returns the thing request group in XML format, which is used in HealthVault requests (namely, GetThings)
     *
     * @param string $elementName the name of the top-most XML element
     * 
     * @return string
     */
    public function writeXml($elementName)
    {
        if (!is_string($elementName))
        {
            throw new InvalidParameterException('Element name must be a string');
        }
        
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->startElement($elementName);
        
        if($this->thingId != null)
        {
            if ($this->versionStamp != null)
            {
                $xmlWriter->startElement('thing-id');
                $xmlWriter->writeAttribute('version-stamp', $this->versionStamp->__toString());
                $xmlWriter->text($this->thingId->__toString());
                $xmlWriter->endElement();
            }
            else
            {
                $xmlWriter->writeElement('thing-id', $this->thingId->__toString());
            }
        }
        
        if($this->typeId != null)
        {
            $xmlWriter->writeRaw($this->typeId->writeXml('type-id'));
        }
        
        if($this->thingState != null)
        {
            $xmlWriter->writeRaw($this->thingState->writeXml('thing-state'));
        }
        
        if($this->flags != null)
        {
            $xmlWriter->writeElement('flags', $this->flags);
        }
        
        if($this->effDate != null)
        {
            $xmlWriter->writeElement('eff-date', $this->thing_id);
        }
        
        if($this->created != null)
        {
            $xmlWriter->writeRaw($this->created->writeXml('created'));
        }
        
        if($this->updated != null)
        {
            $xmlWriter->writeRaw($this->updated->writeXml('updated'));
        }

        if($this->dataXml != null)
        {
            foreach ($this->dataXml as $dx)
            {
                $xmlWriter->writeRaw($dx->writeXml('data-xml'));
            }
        }
        
        if($this->dataOther != null)
        {
            $xmlWriter->writeRaw($this->dataOther->writeXml('data-other'));
        }
        
        if($this->effPermissions != null)
        {
            $xmlWriter->writeRaw($this->effPermissions->writeXml('eff-permissions'));
        }
        
        if(strlen($this->tags) > 0)
        {
            $xmlWriter->writeElement('tags', $this->tags);
        }
        
        if (method_exists($this, 'writeMoreXml'))
        {
            $this->writeMoreXml($xmlWriter);
        }
        

        $xmlWriter->endElement();
        return $xmlWriter->flush();
    }
    
    /**
     * Creates a Thing from the provided XML object
     *
     * @param SimpleXMLElement $xmlObj The XML object to parse
     * @return Thing
     *
     */
    public static function fromXML(SimpleXMLElement $xmlObj)
    {
        if (!array_key_exists((string)$xmlObj->{'type-id'}, Thing::$idMap))
        {
            throw new NotImplementedException('Thing type ID ' . (string)$xmlObj->{'type-id'} . ' is not yet implemented by this library.');
        }
        $thingClass = Thing::$idMap[(string)$xmlObj->{'type-id'}];
        $thing = new $thingClass();
        $thing->parseXml($xmlObj);
        return $thing;
    }
    
    /**
     * Actually parses the XML and sets the internal variables
     *
     * @param SimpleXMLElement $xmlObj The XML to parse
     * @return void
     *
     */
    protected function parseXml(SimpleXMLElement $xmlObj)
    {
        throw new InvalidMethodException('Thing::parseXml() must be implemented by child classes.');
    }
    
}

?>