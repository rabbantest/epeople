<?php

class PersonalDemographicInformation extends Thing
{
    
    public $personal;
    
    const ID = '92ba621e-66b3-4a01-bd73-74844aed4f5b';
    
    public function __construct($thingId = null)
    {
        $typeId = new ThingType();
        $typeId->id = new Guid(PersonalDemographicInformation::ID);
        $typeId->name = 'Personal Demographic Information';
        parent::__construct($thingId, $typeId);
    }
    
    protected function parseXml(SimpleXMLElement $xmlObj)
    {
        if (isset($xmlObj->{'thing-id'}))
        {
            $this->setThingId(new Guid((string)$xmlObj->{'thing-id'}));
        }
        if (isset($xmlObj->{'thing-state'}))
        {
            $this->setThingState(new ThingState((string)$xmlObj->{'thing-state'}));
        }
        if (isset($xmlObj->{'eff-date'}))
        {
            $this->setEffDate((string)$xmlObj->{'eff-date'});
        }
        if (isset($xmlObj->{'data-xml'}->personal))
        {
            $this->personal = HvPersonal::fromXml($xmlObj->{'data-xml'}->personal);
        }
    }
    
}


?>