<?php

class ContinuityOfCareRecord extends Thing
{
    
    public $dataXml = null;
    public $xmlObj;
    
    const ID = '1e1ccbfc-a55d-4d91-8940-fa2fbf73c195';

    public function __construct($thingId = null)
    {
        $typeId = new ThingType();
        $typeId->id = new Guid(ContinuityOfCareRecord::ID);
        $typeId->name = 'Continuity of Care Record';
        parent::__construct($thingId, $typeId);
    }
    
    public static function fromXml($xmlObj)
    {
        $obj = new ContinuityOfCareRecord();
        $obj->xmlObj = $xmlObj;
        return $obj;
    }
    

}



?>