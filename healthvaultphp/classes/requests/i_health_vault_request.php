<?php
/**
 * This file houses the IHealthVaultRequest interface definition.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Requests
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The IHealthVaultRequest interface defines a set of common methods which must be implemented by all request objects which implement it.
 *
 * @category  PHP-Library
 * @package   HealthVault
 * @author    Dave Kiger <noemail@noneto.us>
 * @copyright 2008 TelaDoc, Inc.
 * @license   https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link      https://sourceforge.net/projects/healthvaultphp
 *
 */
interface IHealthVaultRequest
{

    /**
     * Builds the header portion of the XML request payload.
     *
     * @return string
     */
    public function getHeader();

    /**
     * Builds the body portion of the XML request payload.
     *
     * @return string
     */
    public function getBody();

    /**
     * Builds the footer portion of the XML request payload.
     *
     * @return string
     */
    public function getFooter();
    
    /**
     * Gets the name of the method for this request
     *
     * @return string The name of the method being used
     *
     */
    public function getMethodName();

}

?>