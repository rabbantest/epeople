<?php
/**
 * This file houses the GetPersonInfoRequest class definition.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Requests
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The GetPersonInfoRequest object contains the methods needed to build the XML payload to send a GetPersonInfo request to HealthVault.
 *
 * @category  PHP-Library
 * @package   HealthVault
 * @author    Dave Kiger <noemail@noneto.us>
 * @copyright 2008 TelaDoc, Inc.
 * @license   https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link      https://sourceforge.net/projects/healthvaultphp
 *
 */
class GetPersonInfoRequest implements IHealthVaultRequest
{

    /**
     * The wctoken response received from the HealthVault shell after user authentication.
     *
     * @var string
     */
    private $wcToken;

    /**
     * Object constructor.
     *
     * @param string $wcToken the wctoken response received from the HealthVault shell after user authentication
     *
     * @return GetPersonInfoRequest
     */
    public function __construct($wcToken)
    {
        $this->wcToken = $wcToken;
    }

    /**
     * Returns the header portion of the XML payload.
     *
     * @todo stub
     *
     * @return string
     */
    public function getHeader()
    {
        // fetch application authentication token
        $hvConn       = HealthVaultConnection::getInstance();
        $appAuthToken = $hvConn->getAuthenticationToken();

        // get the hash digest of the body
        $hashDigest   = HealthVaultHelper::getHashDigest($this->getBody());

		$headerXmlWriter = new XMLWriter();
		$headerXmlWriter->openMemory();
		$headerXmlWriter->startElement('header');
		$headerXmlWriter->writeElement('method','GetPersonInfo');
		$headerXmlWriter->writeElement('method-version','1');
		$headerXmlWriter->startElement('auth-session');
		$headerXmlWriter->writeElement('auth-token', $appAuthToken);
		$headerXmlWriter->writeElement('user-auth-token', $this->wcToken);
		$headerXmlWriter->endElement();
		$headerXmlWriter->writeElement('language', 'en');
		$headerXmlWriter->writeElement('country', 'US');
		$headerXmlWriter->writeElement('msg-time', HealthVaultHelper::getTimestamp());
		$headerXmlWriter->writeElement('msg-ttl', '1800');
		$headerXmlWriter->writeElement('version', '0.9.1712.2902');
		$headerXmlWriter->startElement('info-hash');
		$headerXmlWriter->startElement('hash-data');
		$headerXmlWriter->writeAttribute('algName','SHA1');
		$headerXmlWriter->text($hashDigest);
		$headerXmlWriter->endElement();
		$headerXmlWriter->endElement();
		$headerXmlWriter->endElement();
        // create header section

        // get HMAC-SHA1 of header
        $digest = $hvConn->getSecretDigest();
		$headerXML = $headerXmlWriter->flush();
        $hmacDigest = HealthVaultHelper::getHmacSha1($digest, $headerXML);

		$xmlWriter = new XMLWriter();
		$xmlWriter->openMemory();
		$xmlWriter->startElement('auth');
		$xmlWriter->startElement('hmac-data');
		$xmlWriter->writeAttribute('algName','HMACSHA1');
		$xmlWriter->text($hmacDigest);
		$xmlWriter->endElement();
		$xmlWriter->endElement();
		$xmlWriter->writeRaw($headerXML);
		return $xmlWriter->flush();
    }

    /**
     * Returns the body portion of the XML payload.
     *
     * @todo stub
     *
     * @return string
     */
    public function getBody()
    {
        $xmlWriter = new XMLWriter();
		$xmlWriter->openMemory();
		$xmlWriter->writeElement('info');
		return $xmlWriter->flush();
    }

    /**
     * Returns the footer portion of the XML payload.
     *
     * @todo stub
     *
     * @return string
     */
    public function getFooter()
    {
        return '';
    }
    
    /**
     * Gets the name of the method for this request
     *
     * @return string The name of the method being used
     *
     */
    public function getMethodName()
    {
        return 'GetPersonInfo';
    }
}

?>