<?php
/**
 * This file houses the PutThingsRequest class definition.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Requests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The PutThingsRequest object contains the methods needed to build the XML payload to send a PutThings request to HealthVault.
 *
 * This request allows your application to add or update a Thing.
 *
 * @category  PHP-Library
 * @package   HealthVault
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link      https://sourceforge.net/projects/healthvaultphp
 *
 */
class PutThingsRequest implements IHealthVaultRequest
{
    /**
     * An array of Things
     *
     * @var array[int]Thing 
     *
     */
    protected $things;
    
    /**
     * The guid of the record things are being put to
     *
     * @var Guid 
     *
     */
    protected $recordId;
    
    /**
     * The guid of the person things are being put to
     *
     * @var Guid 
     *
     */
    protected $targetPersonId;
    
    /**
     * The user auth token
     *
     * @var string 
     *
     */
    protected $wcToken = null;

    /**
     * Constructor taken in required parameters to make the request
     *
     * @param array $things An array of Things representing the things to be put
     * @param Guid $recordId The ID of the record from which things are being requested
     * @param string $wcToken The user auth token to use in the request
     * @return void 
     *
     */
    public function __construct(array $things, Guid $recordId, Guid $targetPersonId,  $wcToken = null)
    {
        if(count($things) < 1)
        {
            throw new InvalidParameterException('Must have at least one thing in the array');
        }
        foreach($things as $thing)
        {
            if(!is_a($thing, 'Thing'))
            {
                throw new InvalidParameterException('All elements in the array must be Things');
            }
        }
        $this->things   = $things;
        $this->recordId = $recordId;
        $this->targetPersonId = $targetPersonId;
        $this->wcToken  = $wcToken;
    }

    /**
     * Gets the header of the request
     *
     * @uses HealthVaultConnection::getInstance()
     * @uses HealthVaultConnection::getAuthenticationToken()
     * @uses HealthVaultConnection::getSecretDigest()
     * @uses HealthVaultHelper::getHashDigest()
     * @uses HealthVaultHelper::getTimestamp()
     * @uses HealthVaultHelper::getHmacSha1()
     * @uses PutThingsRequest::getBody()
     * 
     * @return string The header of the request
     *
     */
    public function getHeader()
    {
        // fetch application authentication token
        $hvConn       = HealthVaultConnection::getInstance();
        $appAuthToken = $hvConn->getAuthenticationToken();

        // get the hash digest of the body
        $hashDigest   = HealthVaultHelper::getHashDigest($this->getBody());

		$headerXmlWriter = new XMLWriter();
		$headerXmlWriter->openMemory();
		$headerXmlWriter->startElement('header');
		$headerXmlWriter->writeElement('method','PutThings');
		$headerXmlWriter->writeElement('method-version','1');
		$headerXmlWriter->writeElement('target-person-id', $this->targetPersonId->__toString());
		$headerXmlWriter->writeElement('record-id', $this->recordId->__toString());
		$headerXmlWriter->startElement('auth-session');
		$headerXmlWriter->writeElement('auth-token', $appAuthToken);
		
		// offline access
		if ($this->wcToken == null)
		{
		    $headerXmlWriter->startElement('offline-person-info');
		    $headerXmlWriter->writeElement('offline-person-id', $this->targetPersonId->__toString());
    		$headerXmlWriter->endElement();
	    }
		
		// online access -- must have wcToken
		if ($this->wcToken != null)
		{
		    $headerXmlWriter->writeElement('user-auth-token', $this->wcToken);
	    }
	    
		$headerXmlWriter->endElement();
		$headerXmlWriter->writeElement('language', 'en');
		$headerXmlWriter->writeElement('country', 'US');
		$headerXmlWriter->writeElement('msg-time', HealthVaultHelper::getTimestamp());
		$headerXmlWriter->writeElement('msg-ttl', '1800');
		$headerXmlWriter->writeElement('version', '0.9.1712.2902');
		$headerXmlWriter->startElement('info-hash');
		$headerXmlWriter->startElement('hash-data');
		$headerXmlWriter->writeAttribute('algName','SHA1');
		$headerXmlWriter->text($hashDigest);
		$headerXmlWriter->endElement();
		$headerXmlWriter->endElement();
		$headerXmlWriter->endElement();
        // create header section

        // get HMAC-SHA1 of header
        $digest = $hvConn->getSecretDigest();
		$headerXML = $headerXmlWriter->flush();
        $hmacDigest = HealthVaultHelper::getHmacSha1($digest, $headerXML);

		$xmlWriter = new XMLWriter();
		$xmlWriter->openMemory();
		$xmlWriter->startElement('auth');
		$xmlWriter->startElement('hmac-data');
		$xmlWriter->writeAttribute('algName','HMACSHA1');
		$xmlWriter->text($hmacDigest);
		$xmlWriter->endElement();
		$xmlWriter->endElement();
		$xmlWriter->writeRaw($headerXML);
		$xml = $xmlWriter->flush();
		return $xml;
    }

    public function validateMe()
    {
        libxml_use_internal_errors(true);
        $xml = $this->getBody(true);
        //echo $xml; exit;
        $xsd = HV_BASE_PATH . '/xsds/method-putthings.xsd';
        $xdoc = new DomDocument();
        $xdoc->loadXML($xml);
        $response = true;
        if (!$xdoc->schemaValidate($xsd))
        {
            $errors = libxml_get_errors();
            $response = 'PutThingsRequest Validation Errors:<br><ul>';
            foreach ($errors as $error)
            {
                $response .= '<li>' . $error->line . ':' . $error->column . ' -- ' . $error->message . "</li>";
            }
            $response .= '</ul>';
        }
        return $response;
    }

    /**
     * Gets the body of the request
     *
     * @uses ThingRequestGroup::writeXML()
     * 
     * @return string The body of the request
     *
     */
    public function getBody($forValidation = false)
    {
        if ($forValidation)
        {
            $xml = '<wc-request-putthings:info xmlns:wc-request-putthings="urn:com.microsoft.wc.methods.PutThings">';
        }
        else
        {
            $xml = '<info>';
        }
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        foreach($this->things as $thing)
        {
            $xmlWriter->writeRaw($thing->writeXML('thing'));
        }
        $xml .= $xmlWriter->flush();
        if ($forValidation)
        {
            $xml .= '</wc-request-putthings:info>';
        }
        else
        {
            $xml .= '</info>';
        }
        return $xml;
    }

    /**
     * Gets the footer of the request
     *
     * @return string The footer of the request
     *
     */
    public function getFooter()
    {
        return '';
    }

    /**
     * Gets the method being requested
     *
     * @return string The method being requested
     *
     */
    public function getMethodName()
    {
        return 'PutThings';
    }
}

?>