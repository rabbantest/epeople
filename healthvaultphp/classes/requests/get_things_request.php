<?php
/**
 * This file houses the PutThingsRequest class definition.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Requests
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The PutThingsRequest object contains the methods needed to build the XML payload to send a PutThings request to HealthVault.
 *
 * This request allows your application to add or update a Thing.
 *
 * @category  PHP-Library
 * @package   HealthVault
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link      https://sourceforge.net/projects/healthvaultphp
 *
 */
class GetThingsRequest implements IHealthVaultRequest
{
    
    /**
     * An array of ThingRequestGroups representing what is going to be requested
     *
     * @var array[int]ThingRequestGroup 
     *
     */
    protected $thingRequestGroups;
    
    /**
     * The id of the record from which these things will be requested
     *
     * @var Guid 
     *
     */
    protected $recordId;
    
    /**
     * Target Person ID
     *
     * @var Guid
     */
     protected $targetPersonId;
     
     protected $wcToken;
     
    
    /**
     * Constructor taken in required parameters to make the request
     *
     * @param array $thingRequestGroups An array of ThingRequestGroups representing the requests
     * @param Guid $recordId The ID of the record from which things are being requested
     * @param string $wcToken The user auth token to use in the request
     * @return void 
     *
     */
    public function __construct(array $thingRequestGroups, Guid $recordId, Guid $targetPersonId, $wcToken = null)
    {
        if(count($thingRequestGroups) < 1)
        {
            throw new InvalidParameterException('Must have at least one ThingRequestGroup in the array');
        }
        foreach($thingRequestGroups as $thingRequestGroup)
        {
            if(false == ($thingRequestGroup instanceof ThingRequestGroup))
            {
                throw new InvalidParameterException('All elements in the array must be ThingRequestGroups');
            }
        }
        $this->thingRequestGroups = $thingRequestGroups;
        $this->recordId = $recordId;
        $this->targetPersonId = $targetPersonId;
        $this->wcToken = $wcToken;
    }
    
    /**
     * Gets the header of the request
     *
     * @uses HealthVaultConnection::getInstance()
     * @uses HealthVaultConnection::getAuthenticationToken()
     * @uses HealthVaultConnection::getSecretDigest()
     * @uses HealthVaultHelper::getHashDigest()
     * @uses HealthVaultHelper::getTimestamp()
     * @uses HealthVaultHelper::getHmacSha1()
     * @uses GetThingsRequest::getBody()
     * 
     * @return string The header of the request
     *
     */
    public function getHeader()
    {
        // fetch application authentication token
        $hvConn       = HealthVaultConnection::getInstance();
        $appAuthToken = $hvConn->getAuthenticationToken();

        // get the hash digest of the body
        $hashDigest   = HealthVaultHelper::getHashDigest($this->getBody());

		$headerXmlWriter = new XMLWriter();
		$headerXmlWriter->openMemory();
		$headerXmlWriter->startElement('header');
		$headerXmlWriter->writeElement('method','GetThings');
		$headerXmlWriter->writeElement('method-version','1');
		$headerXmlWriter->writeElement('target-person-id', $this->targetPersonId->__toString());
		$headerXmlWriter->writeElement('record-id', $this->recordId->__toString());
		$headerXmlWriter->startElement('auth-session');
		$headerXmlWriter->writeElement('auth-token', $appAuthToken);
		
		// offline access
		if ($this->wcToken == null)
		{
		    $headerXmlWriter->startElement('offline-person-info');
		    $headerXmlWriter->writeElement('offline-person-id', $this->targetPersonId->__toString());
    		$headerXmlWriter->endElement();
	    }
		
		// online access -- must have wcToken
		if ($this->wcToken != null)
		{
		    $headerXmlWriter->writeElement('user-auth-token', $this->wcToken);
	    }
	    
		$headerXmlWriter->endElement();
		$headerXmlWriter->writeElement('language', 'en');
		$headerXmlWriter->writeElement('country', 'US');
		$headerXmlWriter->writeElement('msg-time', HealthVaultHelper::getTimestamp());
		$headerXmlWriter->writeElement('msg-ttl', '1800');
		$headerXmlWriter->writeElement('version', '0.9.1712.2902');
		$headerXmlWriter->startElement('info-hash');
		$headerXmlWriter->startElement('hash-data');
		$headerXmlWriter->writeAttribute('algName','SHA1');
		$headerXmlWriter->text($hashDigest);
		$headerXmlWriter->endElement();
		$headerXmlWriter->endElement();
		$headerXmlWriter->endElement();
        // create header section

        // get HMAC-SHA1 of header
        $digest = $hvConn->getSecretDigest();
		$headerXML = $headerXmlWriter->flush();
        $hmacDigest = HealthVaultHelper::getHmacSha1($digest, $headerXML);

		$xmlWriter = new XMLWriter();
		$xmlWriter->openMemory();
		$xmlWriter->startElement('auth');
		$xmlWriter->startElement('hmac-data');
		$xmlWriter->writeAttribute('algName','HMACSHA1');
		$xmlWriter->text($hmacDigest);
		$xmlWriter->endElement();
		$xmlWriter->endElement();
		$xmlWriter->writeRaw($headerXML);
		$xml = $xmlWriter->flush();
		return $xml;
    }
    
    
    /**
     * Gets the body of the request
     *
     * @uses ThingRequestGroup::writeXML()
     * 
     * @return string The body of the request
     *
     */
    public function getBody()
    {
        /*
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        
        $xmlWriter->startElement('info');
            $xmlWriter->startElement('group');
                $xmlWriter->startElement('filter');
                    $xmlWriter->writeElement('type-id', '162dd12d-9859-4a66-b75f-96760d67072b');
                    $xmlWriter->writeElement('thing-state', 'Active');
                $xmlWriter->endElement();
                $xmlWriter->startElement('format');
                    $xmlWriter->writeElement('section', 'core');
                    $xmlWriter->writeElement('xml', '');
                $xmlWriter->endElement();
            $xmlWriter->endElement();
        $xmlWriter->endElement();
        
        $xml = $xmlWriter->flush();
        return $xml;
        */
        // end test
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->startElement('info');
        foreach($this->thingRequestGroups as $thingRequestGroup)
        {
            $xmlWriter->writeRaw($thingRequestGroup->writeXML('group'));
        }
        $xmlWriter->endElement();
        $xml = $xmlWriter->flush();
        return $xml;
    }
    
    /**
     * Gets the footer of the request
     *
     * @return string The footer of the request
     *
     */
    public function getFooter()
    {
        return '';
    }
    
    /**
     * Gets the method being requested
     *
     * @return string The method being requested
     *
     */
    public function getMethodName()
    {
        return 'GetThings';
    }
}

?>