<?php

/**
 * This file houses the file class.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Helpers
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The file class is a wrapper for some common operations done on files local to the web server, including delete, save, append, and read.
 *
 * @category  PHP-Library
 * @package   HealthVault
 * @author    Dave Kiger <noemail@noneto.us>
 * @copyright 2008 TelaDoc, Inc.
 * @license   https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link      https://sourceforge.net/projects/healthvaultphp
 */
class File
{
    /**
     * The filename, including full path.
     * @name $fileName
     * @var string
     */
    private $fileName;

    /**
     * The data contained within the file.
     * @name $data
     * @var string
     */
    private $data;

    /**
     * Object constructor.
     *
     * @uses File::setFileName()
     * @uses File::setData()
     *
     * @param string $fileName the name of the file this object represents
     * @param string $data     the data to store in the file
     *
     * @return object
     */
    public function __construct($fileName = '', $data = '')
    {
        $this->setFileName($fileName);
        $this->setData($data);
        return;
    }

    /**
     * PHP magic method; called automatically whenever this object is cloned.  Resets the $fileName and $data properties to empty strings.
     *
     * @uses File::setFileName()
     * @uses File::setData()
     *
     * @return object
     */
    public function __clone()
    {
        $this->setFileName('');
        $this->setData('');
        return;
    }
	
	
	/**
	 * PHP magic method; called when trying to get the private properties
	 *
	 * @param string $key The property requested
	 * 
	 * @uses File::getData()
	 * @uses File::getFileName()
	 * 
	 * @return mixed The value of that property
	 *
	 */
	public function __get($key)
	{
		switch ($key)
		{
			case "fileName":
			case "FileName":
				return $this->getFileName();
			case "data":
			case "Data":
				return $this->getData();
		}
	}
	
	
	/**
	 * PHP magic function. Called when trying to set a private/protected property
	 *
	 * @param string $key   The property to update
	 * @param mixed  $value The value to assign to that property
	 * 
	 * @uses File::setData()
	 * @uses File::setFileName()
	 * 
	 * @return void
	 *
	 */
	public function __set($key, $value)
	{
		switch ($key)
		{
			case "fileName":
			case "FileName":
				$this->setFileName($value);
				break;
			case "data":
			case "Data":
				$this->setData($value);
				break;
		}
	}
	/**
	 * Gets the FileName stored in this object
	 *
	 * @return string The filename stored in this object
	 *
	 */
	public function getFileName()
	{
		return $this->fileName;
	}
	
    /**
     * Accessor method; sets the $fileName property.  Returns the old value of $fileName or empty string if not previously set.
     *
     * @param string $fileName the name of the file this object represents
     *
     * @return string
     */
    public function setFileName($fileName)
    {
        $oldFileName = $this->fileName;
        $this->fileName = (string)$fileName;
        return $oldFileName;
    }

	/**
	 * Gets the data from the file that is loaded
	 *
	 * @return string The data in the file
	 *
	 */
	public function getData()
	{
		return $this->data;
	}
	
    /**
     * Accessor method; sets the $data property.  Returns the old value of $data or empty string if not previously set.
     *
     * @param string $data sets the data which should be stored inside the file
     *
     * @return string
     */
    public function setData($data)
    {
        $oldData = $this->data;
        $this->data = (string)$data;
        return $oldData;
    }

    /**
     * If the file exists and is writable, it will be deleted.  Returns true on success, false on failure.
     * This method will return false if the file does not exist, which may not be an error depending on your use.
     *
     * @throws Exception if the file cannot be deleted.
     *
     * @return bool
     */
    public function delete()
    {
        $isOk = false;
        if (!file_exists($this->fileName))
        {
			throw new FileNotFoundException("Unable to delete file {$this->fileName}.  " .
				"It does not exist");
		} 
		if(is_writable($this->fileName))
        {
            unlink($this->fileName);
            $isOk = true;
        }
        else
        {
            throw new FileNotWritableException("Unable to delete file {$this->fileName}.  " .
				"Permissions are such that it is not writable.");
        }
        return $isOk;
    }

    /**
     * Writes the data in the $data property to the filename specified in the $fileName property.  If the file exists, it will be overwritten.  Returns true on success, false on failure.
     *
     * @throws Exception if the data cannot be written to the filename specified.
     *
     * @return bool
     */
    public function save()
    {
		$isOk = false;
		if (($fp = @fopen($this->fileName, 'wb')) !== false)
		{
			$isWritten = fwrite($fp, $this->data);
			fclose($fp);
			if ($isWritten !== false)
			{
                chmod($this->fileName, 0755);
                $isOk = true;
            }
            else
            {
                throw new FileIOException("Unable to write to file {$this->fileName}.");
            }
        }
        else
        {
            throw new FileNotWritableException("Unable to open file {$this->fileName} " .
				"for writing data.");
        }
        return $isOk;
    }

    /**
     * Writes the data in the $data property to the end of the filename specified in the $fileName property.  If the file does not exist, it will be created.  Returns true on success, false on failure.
     *
     * @throws Exception if the data cannot be written to the file.
     *
     * @return bool
     */
    public function append()
    {
        $isOk = false;
        $fp = @fopen($this->fileName, 'a+');
        if ($fp !== false)
        {
            $isWritten = fwrite($fp, $this->data);
            fclose($fp);
            if ($isWritten !== false)
            {
                chmod($this->fileName, 0755);
                $isOk = true;
            }
            else
            {
                throw new FileIOException("Unable to write to file {$this->fileName}.");
            }
        }
        else
        {
            throw new FileNotWritableException("Unable to open file {$this->fileName} " .
				"for appending data.");
        }
        return $isOk;
    }

    /**
     * Reads the contents of the file specified in the $fileName property, stores it in the $data property, and returns it.  If the file does not exist or contains no data, an empty string will be returned.
     *
     * @throws Exception if file cannot be read.
     *
     * @return string
     */
    public function read()
    {
        if (empty($this->data))
        {
			if(!file_exists($this->fileName))
			{
				throw new FileNotFoundException("Unable to read file.  " .
					"Filename {$this->fileName} does not exist.");
			}
            if (is_readable($this->fileName))
            {
                $fp = fopen($this->fileName, 'rb');
                if ($fp !== false)
                {
                    while (true !== feof($fp))
                    {
                        $this->data .= fread($fp, 1024);
                    }
                    fclose($fp);
                }
                else
                {
                    throw new FileNotReadableException("Unable to open file " .
						"{$this->fileName} for reading.");
                }
            }
            else
            {
                throw new FileNotReadableException("Unable to read file. Permissions are set in ".
					"such a way as to render the file un-readable.");
            }
        }
        return $this->data;
    }

}

?>