<?php
/**
 * This file houses the NetworkIOException class.
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Exceptions
 * @author     Jim Wordelman
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * Exception class for when a network error occurs
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Exceptions
 * @author     Jim Wordelman
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class NetworkIOException extends Exception {}

?>