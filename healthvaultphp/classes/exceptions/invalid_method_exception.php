<?php
/**
 * This file houses the InvalidMethodException class.
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Exceptions
 * @author     Dave Kiger
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * Exception class thrown when you try to use a HealthVault method which is not defined in their SDK.
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Exceptions
 * @author     Dave Kiger
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class InvalidMethodException extends Exception {}

?>