<?php
/**
 * This file houses the RequiredVariableIsNullException class.
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Exceptions
 * @author     Jim Wordelman
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * Exception class for when a needed variable is null when it must contain a value
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Exceptions
 * @author     Jim Wordelman
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class RequiredVariableIsNullException extends Exception {}

?>