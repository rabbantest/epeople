<?php
/**
 * This file houses the XmlRequestErrorException class.
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Exceptions
 * @author     Jim Wordelman
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * Exception class for when an error occurs when processing an xml request
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Exceptions
 * @author     Jim Wordelman
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class XmlRequestErrorException extends Exception {}

?>