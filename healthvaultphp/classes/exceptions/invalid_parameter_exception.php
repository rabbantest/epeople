<?php
/**
 * This file houses the InvalidParameterException class.
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Exceptions
 * @author     Jim Wordelman
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * Exception class for when an invalid parameter is provided to a method
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Exceptions
 * @author     Jim Wordelman
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class InvalidParameterException extends Exception {}

?>