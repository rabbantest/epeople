<?php
/**
 * This file houses the Settings class definition.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Helpers
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The Settings class is used to read, store, and write to the INI files in the settings directory.
 *
 * When this class is instantiated, all INI files in the settings directory will be read and stored in internal class properties.  Only one instance of this class should exist at any given time.
 *
 * @category  PHP-Library
 * @package   HealthVault
 * @author    Dave Kiger <noemail@noneto.us>
 * @copyright 2008 TelaDoc, Inc.
 * @license   https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link      https://sourceforge.net/projects/healthvaultphp
 */
class HvSettings
{

    /**
     * Static instance of a Settings object.  Implementation of the Singleton design.
     *
     * @name $instance
     * @var object
     */
    static private $instance;

    /**
     * Multiple associative array containing all of the settings stored in the various INI files.
     *
     * @name $settings
     * @var array
     */
    private $settings;

    /**
     * Object constructor.
     *
     * @uses Settings::loadSettings()
     *
     * @return Settings
     */
    private function __construct()
    {
        $this->loadSettings();
        return;
    }

    /**
     * Implementation of PHP magic method; immediately triggers an error as this is a Singleton class and only once instance should ever be in use.
     *
     * @return void
     */
    public function __clone()
    {
        trigger_error('Invalid use of the clone() method on a static Singleton class.', E_USER_ERROR);
    }

    /**
     * Returns a static instance of this class.  Must be used instead of constructing a new object since only one object can exist at a time.  Implementation of Singleton design.
     *
     * @return object
     */
    static public function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new HvSettings();
        }
        return self::$instance;
    }

    /**
     * Scans the HV_PATH_CONFIGS directory for INI files and parses them into the $settings property.
     *
     * @uses HV_PATH_CONFIGS
     *
     * @return void
     */
    private function loadSettings()
    {
        if (file_exists(HV_BASE_PATH . '/authentication/myapp.id'))
        {
            $app_id = file_get_contents(HV_BASE_PATH . '/authentication/myapp.id');
        }
        else if (file_exists(HV_BASE_PATH . '/authentication/app.id'))
        {
            $app_id = file_get_contents(HV_BASE_PATH . '/authentication/app.id');
        }
        else
        {
            throw new FileNotFoundException(HV_BASE_PATH . '/authentication/app.id or myapp.id is required but is missing.');
        }
        if (file_exists(HV_BASE_PATH . '/authentication/myapp.fp'))
        {
            $thumbprint = file_get_contents(HV_BASE_PATH . '/authentication/myapp.fp');
        }
        else if (file_exists(HV_BASE_PATH . '/authentication/app.fp'))
        {
            $thumbprint = file_get_contents(HV_BASE_PATH . '/authentication/app.fp');
        }
        else
        {
            throw new FileNotFoundException(HV_BASE_PATH . '/authentication/app.fp or myapp.fp is required but is missing.');
        }
        if (file_exists(HV_BASE_PATH . '/authentication/myapp.pem'))
        {
            $key_file = 'myapp.pem';
        }
        else if (file_exists(HV_BASE_PATH . '/authentication/app.pem'))
        {
            $key_file = 'app.pem';
        }
        else
        {
            throw new FileNotFoundException(HV_BASE_PATH . '/authentication/app.pem or myapp.pem is required but is missing.');
        }
        if (file_exists(HV_BASE_PATH . '/authentication/myapp.cer'))
        {
            $cert_file = 'myapp.cer';
        }
        else if (file_exists(HV_BASE_PATH . '/authentication/app.cer'))
        {
            $cert_file = 'app.cer';
        }
        else
        {
            throw new FileNotFoundException(HV_BASE_PATH . '/authentication/app.cer or myapp.cer is required but is missing.');
        }
        if (file_exists(HV_BASE_PATH . '/authentication/use.consumer'))
        {
            $use_consumer = true;
        }
        else
        {
            $use_consumer = false;
        }
        $this->settings['authentication'] = array
        (
            'app_id' => trim($app_id),
            'thumbprint' => trim($thumbprint),
            'key_file' => $key_file,
            'cert_file' => $cert_file,
            'use_consumer' => $use_consumer
        );
        return;
    }

    /**
     * Accessor method; returns the value for the given section and name.  Returns empty string if not set.
     *
     * @param string $section the section of settings the particular variable is stored within
     * @param string $name    the name of the variable for which you want the value
     *
     * @return mixed
     */
    public function get($section, $name)
    {
        $value = '';
        if (isset($this->settings[$section][$name]))
        {
            if (!is_array($this->settings[$section][$name]))
            {
                $value = html_entity_decode($this->settings[$section][$name], ENT_QUOTES);
            }
        }
        return $value;
    }
    
}

?>