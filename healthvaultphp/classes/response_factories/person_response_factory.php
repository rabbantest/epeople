<?php

/**
 * Response factory for person-related responses
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Response Factories
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

class PersonResponseFactory implements IHealthVaultResponseFactory
{
    /**
     * Generates the response object based on the provided XML and method name
     *
     * @param string $rawXML     The XML response to parse
     * @param string $methodName The method name that generated the XML
     * 
     * @uses PersonResponseFactory::parseGetPersonInfoResponse()
     * 
     * @return IHealthVaultResponse The response object from the XML
     *
     */
    public static function getResponseObject($rawXML, $methodName)
    {
        switch ($methodName)
        {
            case "GetPersonInfo":
                return self::parseGetPersonInfoResponse($rawXML);
        }
        throw new NotImplementedException("No handler is defined for $methodName");
    }
    
    /**
     * Creates a PersonInfoResponseObject from the provided XML (from a GetPersonInfo response)
     *
     * @param string $rawXML The XML to parse
     * 
     * @uses PersonInfoResponseObject::fromXml()
     * 
     * @return PersonInfoResponseObject The object representing the XML data
     *
     */
    private static function parseGetPersonInfoResponse($rawXML)
    {
        try
        {
            $xmlObj = new SimpleXMLElement($rawXML);
        }
        catch(Exception $e)
        {
            throw new InvalidParameterException('rawXML must be a valid XML string');
        }
        $xmlObj->registerXPathNamespace('wc', 'urn:com.microsoft.wc.methods.response.GetPersonInfo');
        $infoXMLObjs = $xmlObj->xpath('wc:info');
        if($infoXMLObjs === false || count($infoXMLObjs) !== 1)
        {
            throw new InvalidParameterFormatException('XML must conform to HealthVault response schemas');
        }
        return PersonInfoResponseObject::fromXml($infoXMLObjs[0]);
    }
}
?>