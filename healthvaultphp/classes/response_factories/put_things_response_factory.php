<?php
/**
 * This file houses the PutThingsResponseFactory.
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Response-Factories
 * @author     Dave Kiger
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * Response factory for person-related responses
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Response-Factories
 * @author     Dave Kiger
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */
class PutThingsResponseFactory implements IHealthVaultResponseFactory
{
    /**
     * Generates the response object based on the provided XML and method name
     *
     * @param string $rawXML     The XML response to parse
     * @param string $methodName The method name that generated the XML
     * 
     * @uses PersonResponseFactory::parseGetPersonInfoResponse()
     * 
     * @return IHealthVaultResponse The response object from the XML
     *
     */
    public static function getResponseObject($rawXML, $methodName)
    {
        switch ($methodName)
        {
            case "PutThings":
                return self::parsePutThingsResponse($rawXML);
        }
        throw new NotImplementedException("No handler is defined for $methodName");
    }
    
    /**
     * Creates a PersonInfoResponseObject from the provided XML (from a GetPersonInfo response)
     *
     * @param string $rawXML The XML to parse
     * 
     * @uses PersonInfoResponseObject::fromXml()
     * 
     * @return PersonInfoResponseObject The object representing the XML data
     *
     */
    private static function parsePutThingsResponse($rawXML)
    {
        $rawXML = str_replace('wc:info', 'info', $rawXML);
        $obj = PutThingsResponse::fromXml($rawXML);
        return $obj;
    }
}
?>