<?php

/**
 * Response factory for person-related responses
 * 
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Response Factories
 * @author     Jim Wordelman
 * @copyright  2008 Microsoft Corporation
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

interface IHealthVaultResponseFactory
{
    
    /**
     * Generates the response object based on the provided XML and method name
     *
     * @param string $rawXML     The XML response to parse
     * @param string $methodName The method name that generated the XML
     * @return IHealthVaultResponse The response object from the XML
     *
     */
    public static function getResponseObject($rawXML, $methodName);
}
?>