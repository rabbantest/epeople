<?php
/**
 * This file houses the HealthVaultConnection class definition.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Proxy
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The HealthVaultConnection object established a secure, authenticated offline connection to the HealthVault system.  It is used to send and receive the raw XML payloads.
 *
 * @category  PHP-Library
 * @package   HealthVault
 * @author    Dave Kiger <noemail@noneto.us>
 * @copyright 2008 TelaDoc, Inc.
 * @license   https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link      https://sourceforge.net/projects/healthvaultphp
 */
class HealthVaultConnection
{
    
    /**
     * The shared secret code used in HMAC authentication.
     *
     * @name $sharedSecret
     * @var string
     */
    private $sharedSecret;
    
    /**
     * The authenticated token returned from HealthVault.
     *
     * @name $token
     * @var string
     */
    private $token;
    
    /**
     * The certificate thumbprint/fingerprint.  This is stored in a file in the keys directory.
     *
     * @name $thumbPrint
     * @var string
     */
    private $thumbPrint;
    
    /**
     * The application ID.  This is stored/fetched using the Settings object.
     *
     * @name $appId
     * @var string
     */
    private $appId;
    
    /**
     * The private key used with OpenSSL to sign/encrypt data.
     *
     * @name $privateKey
     * @var string
     */
    private $privateKey;
    
    /**
     * A single instance of this object.
     *
     * @name $instance
     * @var HealthVaultConnection
     */
    static private $instance;
    
    /**
     * Created when obtaining an application authentication token.
     *
     * @var string
     */
    private $digest;
    
    /**
     * If set to true, then raw request and response XML payloads will be logged to the file specified in $logLocation.
     *
     * @name $logPayloads
     * @var bool
     */
    public $logPayloads;
    
    /**
     * If logging is on, this is the full /path/to/file of the file to save the data to.
     *
     * @name $logLocation
     * @var string
     */
    public $logLocation;
    
    /**
     * Object constructor.  Singleton design.
     *
     * @uses HealthVaultHelper::getThumbPrint()
     * @uses HealthVaultHelper::getAppId()
     * @uses HealthVaultHelper::getPrivateKey()
     *
     * @return HealthVaultConnection
     */
    private function __construct()
    {
        $this->sharedSecret = sha1(uniqid(rand(0, 1), true));
        $this->thumbPrint = HealthVaultHelper::getThumbPrint();
        $this->appId = HealthVaultHelper::getAppId();
        $this->privateKey = HealthVaultHelper::getPrivateKey();
        return;
    }
    
    static public function getManageUrl()
    {
        $obj = HvSettings::getInstance();
        $use_consumer = $obj->get('authentication', 'use_consumer');
        if ($use_consumer)
        {
            return 'https://account.healthvault.com/';
        }
        else
        {
            return 'https://account.healthvault-ppe.com/';
        }
    }
    
    /**
     * Returns the single instance of this object (singleton design).
     *
     * @return HealthVaultConnection
     */
    static public function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new HealthVaultConnection();
        }
        return self::$instance;
    }
    
    /**
     * Returns the shared secret; needed to get HMAC digest of headers in requests.
     *
     * @return string
     */
    public function getSharedSecret()
    {
        return $this->sharedSecret;
    }
    
    /**
     * Returns the digest of the shared secret; only set if an authentication token has been fetched.
     *
     * @return string
     */
    public function getSecretDigest()
    {
        return $this->digest;
    }
    
    /**
     * Sends a request to HealthVault to obtain an authentication token which is needed in order to further communicate with the HV service.
     *
     * @uses HealthVaultHelper::createDigest()
     * @uses HealthVaultConnection::sendRequestPayload()
     * @uses HealthVaultConnection::getContentXML()
     * @uses HealthVaultConnection::getPayloadSignature()
     * @uses HealthVaultConnection::getApplicationAuthenticationTokenRequest()
     * @uses getArrayFromResponseXML()
     *
     * @return string The application authentication token
     */
    public function getAuthenticationToken()
    {
        $digest = HealthVaultHelper::createDigest($this->sharedSecret, $this->sharedSecret);
        $this->digest = $digest;
        
        $contentPayload = self::getContentXML($digest, $this->appId);
        // sign the payload contents
        $payloadSignature = self::getPayloadSignature($contentPayload, $this->privateKey);
        
        // create payload
        $payload = self::getApplicationAuthenticationTokenRequest($contentPayload, 
                $payloadSignature, $this->appId, $this->thumbPrint);
        
        // send payload and get response
        $rawResponse = $this->sendRequestPayload($payload);
        
        // fixing a namespace issue which borks DOM; bit hackish, but oh well
        $rawResponse = str_replace('wc:info', 'wc_info', $rawResponse);
        
        // get an array equivalent of the XML
        $xmlSimple = getArrayFromResponseXML($rawResponse);
        
        // check for errors
        if (isset($xmlSimple['status']['error']['message']))
        {
            throw new XmlRequestErrorException('Error obtaining authentication token: ' . 
                    $xmlSimple['status']['error']['message']);
        }
        // get the token
        if (isset($xmlSimple['wc_info']['token']))
        {
            $this->token = $xmlSimple['wc_info']['token'];
        }
        else
        {
            throw new UnexpectedResponseXmlException('Invalid response XML - cannot find token.');
        }
        return $this->token;
    }
    
    /**
     * Uses cURL to send the raw XML request payload to HealthVault and return the raw XML response.
     *
     * @param string $payload     the raw XML request payload
     * @param string $overrideUrl the url to send to instead of the default
     *
     * @return string
     */
    public function sendRequestPayload($payload, $overrideUrl=null)
    {
        // log request payload if enabled
        $obj = HvSettings::getInstance();
        $use_consumer = $obj->get('authentication', 'use_consumer');
        if ($use_consumer == 'true')
        {
            $url = 'https://platform.healthvault.com/platform/wildcat.ashx';
        }
        else
        {
            $url = 'https://platform.healthvault-ppe.com/platform/wildcat.ashx';
        }
        if($overrideUrl != null)
        {
            $url = $overrideUrl;
        }
        if ($this->logPayloads)
        {
            try
            {
                $logFile = new File($this->logLocation, 'REQUEST: ' . $payload . "\n\n");
                $logFile->append();
            }
            catch (Exception $e)
            {
                // do nothing!
            }
        }
        $conn = curl_init($url);
        curl_setopt($conn, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($conn, CURLOPT_POST, true);
        curl_setopt($conn, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($conn, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($conn, CURLOPT_PORT, 443);
        curl_setopt($conn, CURLOPT_HTTPHEADER, array('Content-Type' => 'text/xml'));
        curl_setopt($conn, CURLOPT_POSTFIELDS, $payload);
        $rawResponse = curl_exec($conn);
        curl_close($conn);
        
        if($rawResponse === false)
        {
            throw new NetworkIOException("Error sending {$payload} to {$url}");
        }
        
        // trim whitespace
        $rawResponse = trim($rawResponse);
        
        // remove the trailing 1 added to the XML
        if ($rawResponse{strlen($rawResponse) - 1} == '1')
        {
            $rawResponse = substr($rawResponse, 0, strlen($rawResponse) - 1);
        }
        
        // remove any funky characters prepending the response xml
        if (stripos($rawResponse, '<?xml') !== false)
        {
            $rawResponse = strstr($rawResponse, '<?xml');
        }
        else
        {
            $rawResponse = '<?xml version="1.0" encoding="utf-8" ?>' . $rawResponse;
        }
        
        // log response payload if enabled
        if ($this->logPayloads)
        {
            try
            {
                $logFile = new File($this->logLocation, 'RESPONSE: ' . $rawResponse . "\n\n");
                $logFile->append();
            }
            catch (Exception $e)
            {
                // do nothing!
            }
        }
        
        return $rawResponse;
    }
    
    /**
     * Returns the URL needed to redirect someone to the HealthVault shell so the application can be authenticated.
     *
     * @param string $redirectTo the URL to redirect to after permissions granted
     *
     * @return string
     */
    public function getShellUrl($redirectTo = '')
    {
        if(is_object($redirectTo))
        {
            throw new InvalidParameterException('Cannot redirect to an object');
        }	
        $obj = HvSettings::getInstance();
        $use_consumer = $obj->get('authentication', 'use_consumer');
        if ($use_consumer)
        {
            $url = 'https://account.healthvault.com/redirect.aspx?target=AUTH&targetqs=?';
            $redirectTo = '';
        }
        else
        {
            $url = 'https://account.healthvault-ppe.com/redirect.aspx?target=AUTH&targetqs=?';
        }
        $url .= urlencode("appid={$this->appId}");
        if (!empty($redirectTo))
        {
            $url .= urlencode("&redirect={$redirectTo}");
        }
        return $url;
    }
    
    
    /**
     * Builds the content for a GetApplicationAuthenticationToken request
     *
     * @param string $digest The digest of the info data
     * @param string $appId The Guid of the application to authenticate
     * @return string The content section for the request
     *
     */
    private static function getContentXML($digest, $appId)
    {
        // this will be signed by the private key
        $contentXMLWriter = new XMLWriter();
        $contentXMLWriter->openMemory();
        $contentXMLWriter->startElement('content');
        $contentXMLWriter->writeElement('app-id', $appId);
        $contentXMLWriter->startElement('shared-secret');
        $contentXMLWriter->startElement('hmac-alg');
        $contentXMLWriter->writeAttribute('algName', 'HMACSHA1');
        $contentXMLWriter->text($digest);
        $contentXMLWriter->endElement();
        $contentXMLWriter->endElement();
        $contentXMLWriter->endElement();
        return $contentXMLWriter->flush();
    }
    
    
    /**
     * Returns the signature of the given payload against the given private key
     *
     * @param string $payload The payload to sign
     * @param resource $privateKey A handle from openssl_pkey_get_private call
     * @return string The signature for the payload
     *
     */
    private static function getPayloadSignature($payload, $privateKey)
    {
        $payloadSignature = '';
        openssl_sign($payload, $payloadSignature, $privateKey, 
                OPENSSL_ALGO_SHA1);
        $payloadSignature = trim(base64_encode($payloadSignature));
        return $payloadSignature;
    }
    
    
    /**
     * Returns a full ApplicationAuthenticationTokenRequest based on the parameters
     *
     * @param string $payload The payload of the content section
     * @param string $payloadSignature The signature of the provided content
     * @param string $appId The Guid of the app being used
     * @param string $thumbPrint The thumbprint for the certificate used
     * @return string The full XML request
     *
     */
    private static function getApplicationAuthenticationTokenRequest($payload, $payloadSignature, 
        $appId, $thumbPrint)
    {
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->startDocument();
        $xmlWriter->startElement('wc-request:request');
        $xmlWriter->writeAttribute('xmlns:wc-request', 'urn:com.microsoft.wc.request');
        $xmlWriter->startElement('header');
        $xmlWriter->writeElement('method', 'CreateAuthenticatedSessionToken');
        $xmlWriter->writeElement('method-version', '1');
        $xmlWriter->writeElement('app-id', $appId);
        $xmlWriter->writeElement('language', 'en');
        $xmlWriter->writeElement('country', 'US');
        $xmlWriter->writeElement('msg-time', HealthVaultHelper::getTimestamp());
        $xmlWriter->writeElement('msg-ttl', '36000');
        $xmlWriter->writeElement('version', '0.0.0.1');
        $xmlWriter->endElement();
        $xmlWriter->startElement('info');
        $xmlWriter->startElement('auth-info');
        $xmlWriter->writeElement('app-id', $appId);
        $xmlWriter->startElement('credential');
        $xmlWriter->startElement('appserver');
        $xmlWriter->startElement('sig');
        $xmlWriter->writeAttribute('digestMethod', 'SHA1');
        $xmlWriter->writeAttribute('sigMethod', 'RSA-SHA1');
        $xmlWriter->writeAttribute('thumbprint', $thumbPrint);
        $xmlWriter->text($payloadSignature);
        $xmlWriter->endElement();
        $xmlWriter->writeRaw($payload);
        $xmlWriter->endDocument();
        return $xmlWriter->flush();
    }
    
    
}

?>
