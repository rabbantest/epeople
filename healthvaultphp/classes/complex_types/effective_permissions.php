<?php
/**
 * 
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Patrick Sharp
 */

/**
 * 
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Patrick Sharp
 */
class EffectivePermissions extends ComplexType
{

    /**
     * A Permission object.
     *
     * @var Permission
     */
    protected $permission;
    
    /**
     * A boolean value holding an attribute.
     *
     * @var boolean
     */
    protected $immutable;
    
    /**
     * Object constructor.
     *
     * @param Permission    $permission a permission object
     * @param boolean       $immutable  a boolean value
     *
     * @return EffectivePermissions
     */
    public function __construct($permission = null, $immutable = null)
    {
        if($permission != null)
        {
            $this->setPermission($permission);
        }
        else
        {
            $this->permission = null;
        }
        
        if($immutable != null)
        {
            $this->setImmutable($immutable);
        }
        else
        {
            $this->immutable = null;
        }
    }

    /**
     * Sets the permission property.
     *
     * @param Permission $permission a permission object
     *
     * @return void
     */
    protected function setPermission($permission)
    {
        if(false == ($permission instanceof Permission))
        {
            throw new InvalidParameterException('Permission must be a Permission object');
        }
        $this->permission = $permission;
    }
    
    /**
     * Sets the immutable property.
     *
     * @param boolean $immutable boolean value of the immutable attribute
     *
     * @return void
     */
    protected function setImmutable($immutable)
    {
        if(!is_bool($immutable))
        {
            throw new InvalidParameterException('Immutable must be a boolean');
        }
        $this->immutable = $immutable;
    }
    
    /**
     * Returns the thing request group in XML format, which is used in HealthVault requests (namely, GetThings)
     *
     * @param string $elementName the name of the top-most XML element
     *
     * @return string
     */
    public function writeXml($elementName)
    {
        if (!is_string($elementName))
        {
            throw new InvalidParameterException('Element name must be a string');
        }
        
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->startElement($elementName);
        
        if($this->permission != null)
        {
            $xmlWriter->writeElement('permission', $this->permission);
        }
        
        if($this->immutable != null)
        {
            $xmlWriter->writeAttribute('immutable', (integer)$this->immutable);
        }

        $xmlWriter->endElement();
        return $xmlWriter->flush();
    }
    
}

?>