<?php
/** 
 * This file houses the DataXml class.
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 * The DataXml class is used in various method calls, particularly GetThings and PutThings.  It is simply a container for a string of XML data.
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class DataXml
{
    
    /**
     * The XML string.
     * @var string
     */
    public $value;
    
    /**
     * Object constructor.
     *
     * @param string $value the XML string
     *
     * @return DataXml 
     */
    public function __construct($value = '')
    {
        $this->value = $value;
    }
    
    /**
     * Converts the object into XML form that will validate against a HealthVault XSD.
     * 
     * @param string $tagName the root element name
     * 
     * @return string
     */
    public function writeXml($tagName)
    {
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($tagName);
        $x->writeRaw($this->value);
        $x->endElement();
        return $x->flush();
    }
    
}

?>