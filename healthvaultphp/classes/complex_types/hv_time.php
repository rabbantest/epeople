<?php
/**
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class HvTime extends ComplexType
{
    
    protected $h;
    protected $m;
    protected $s;
    protected $f;
    
    public function __construct()
    {
        $this->h = null;
        $this->m = null;
        $this->s = null;
        $this->f = null;
    }
    
    public static function fromXml($xmlObj)
    {
        $obj = new HvTime();
        $obj->parseXml($xmlObj);
        return $obj;
    }
    
    protected function parseXml($xmlObj)
    {
        if (isset($xmlObj->h))
        {
            $this->h = (integer) $xmlObj->h;
        }
        if (isset($xmlObj->m))
        {
            $this->m = (integer) $xmlObj->m;
        }
        if (isset($xmlObj->s))
        {
            $this->s = (integer) $xmlObj->s;
        }
        if (isset($xmlObj->f))
        {
            $this->f = (integer) $xmlObj->f;
        }
    }
    
    protected function setH($value)
    {
        if (is_int($value) && $value >= 0 && $value <= 23)
        {
            $this->h = $value;
        }
        else
        {
            throw new InvalidParameterException('Hour must be an integer between 0 and 23');
        }
    }
    
    protected function setM($value)
    {
        if (is_int($value) && $value >= 0 && $value <= 59)
        {
            $this->m = $value;
        }
        else
        {
            throw new InvalidParameterException('Minutes must be an integer between 0 and 59');
        }
    }
    
    protected function setS($value)
    {
        if (is_int($value) && $value >= 0 && $value <= 59)
        {
            $this->s = $value;
        }
        else
        {
            throw new InvalidParameterException('Seconds must be an integer between 0 and 59');
        }        
    }
    
    protected function setF($value)
    {
        if (is_int($value) && $value >= 0 && $value <= 999)
        {
            $this->f = $value;
        }
        else
        {
            throw new InvalidParameterException('Miliseconds must be an integer between 0 and 999');
        }
    }

    public function writeXml($tagName)
    {
        $x = new XmlWriter()
        $x->openMemory();
        $x->startElement($tagName);
        
        if ($this->h != null)
        {
            $x->writeElement('h', $this->h);
        }
        if ($this->m != null)
        {
            $x->writeElement('m', $this->m);
        }
        if ($this->s != null)
        {
            $x->writeElement('s', $this->s);
        }
        if ($this->f != null)
        {
            $x->writeElement('f', $this->f);
        }
        
        $x->endElement();
        return $x->flush();
    }
    
}

?>