<?php
/**
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class Phone extends ComplexType
{
    
    protected $description;
    protected $isPrimary;
    protected $number;
    
    protected function setDescription($value)
    {
        if (!is_string($value))
        {
            throw new InvalidParameterException('Value must be a string.');
        }
        $this->description = $value;
    }
    
    protected function setIsPrimary($value)
    {
        if (!is_bool($value))
        {
            throw new InvalidParameterException('Value must be a boolean.');
        }
        $this->isPrimary = $value;
    }
    
    protected function setNumber($value)
    {
        if (!is_string($value))
        {
            throw new InvalidParameterException('Value must be a string.');
        }
        $this->number = $value;
    }
    
    public function writeXml($tagName)
    {
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($tagName);
        if (strlen($this->description) > 0)
        {
            $x->writeElement('description', $this->description);
        }
        if ($this->isPrimary === true)
        {
            $x->writeElement('is-primary', 'true');
        }
        else
        {
            $x->writeElement('is-primary', 'false');
        }
        if (strlen($this->number) > 0)
        {
            $x->writeElement('number', $this->number);
        }
        $x->endElement();
        return $x->flush();
    }
    
}

?>