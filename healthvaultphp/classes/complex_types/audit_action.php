<?php
/** 
 * This file houses the AuditAction
 * 
 * @author     Dave Kiger
 */

/**
 * The AuditAction object
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class AuditAction extends EnumeratedType
{

    /**
     * Online
     */
    const CREATED = 'Created';
    
    /**
     * Offline
     */
    const DELETED = 'Deleted';
    
    /**
     * Offline
     */
    const UPDATED = 'Updated';

    /**
     * Validates that value is allowed type and sets the property.
     *
     * @param string $value the value of this type
     *
     * @return void
     */
    public function setValue($value)
    {
        switch ($value)
        {
            case AuditAction::CREATED:
            case AuditAction::DELETED:
            case AuditAction::UPDATED:
                $this->value = $value;
                break;
            default:
                throw new InvalidParameterException('Thing section spec is not a valid enumeration.');
        }
    }

}

?>