<?php
/** 
 * This file houses the ThingRequestGroup
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 * The EnumeratedType object is an abstract parent class to all objects which represent 
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger 
 */
abstract class EnumeratedType
{

    /**
     * The value of this type.
     *
     * @var string
     */
    protected $value;

    /**
     * Object constructor.
     *
     * @param string $value the value of this type
     *
     * @return EnumeratedType
     */
    public function __construct($value)
    {
        $this->setValue($value);
        return;
    }
    
    /**
     * Validates that value is allowed type and sets the property.
     *
     * @param string $value the value of this type
     *
     * @return void
     */
    abstract public function setValue($value);

    /**
     * Returns the value property.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
    
}

?>