<?php

class VocabularyKey extends ComplexType
{
    
    protected $name = null;
    protected $family = null;
    protected $version = null;
    protected $codeValue = null;
    
    protected function setName($value)
    {
        if (is_string($value))
        {
            $this->name = $value;
        }
        else
        {
            throw new InvalidParameterException('Value must be a string.');
        }
    }
    
    protected function setFamily($value)
    {
        if (is_string($value))
        {
            $this->family = $value;
        }
        else
        {
            throw new InvalidParameterException('Value must be a string.');
        }
    }
    
    protected function setVersion($value)
    {
        if (is_string($value))
        {
            $this->version = $value;
        }
        else
        {
            throw new InvalidParameterException('Value must be a string.');
        }
    }
    
    protected function setCodeValue($value)
    {
        if (is_string($value))
        {
            $this->codeValue = $value;
        }
        else
        {
            throw new InvalidParameterException('Value must be a string.');
        }
    }
    
    public function writeXml($elementName)
    {
        if ($this->name == null)
        {
            throw new RequiredVariableIsNullException('The name property must be set.');
        }
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($elementName);
        $x->writeElement('name', $this->name);
        if ($this->family != null)
        {
            $x->writeElement('family', $this->family);
        }
        if ($this->version != null)
        {
            $x->writeElement('version', $this->version);
        }
        if ($this->codeValue != null)
        {
            $x->writeElement('code-value', $this->codeValue);
        }
        $x->endElement();
        return $x->flush();
    }
    
    
}



?>