<?php
/** 
 * This file houses the ApproxDateTime class.
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger <no-email@please.net>
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * Represents an approximate date and time.  Same as HvDate object with a different name.
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger <no-email@please.net>
 */
class ApproxDateTime extends HvDate
{
}

?>