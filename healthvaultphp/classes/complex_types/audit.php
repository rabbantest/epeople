<?php
/**
 * This file houses the Audit class.
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Patrick Sharp <no-email@please.net>
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * Represents a portion of an Audit trail for things handled by the HealthVault system. 
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Patrick Sharp <no-email@please.net>
 */
class Audit extends ComplexType
{

    /**
     * A timestamp
     *
     * @var string
     */
    protected $timestamp;
    
    /**
     * The app ID that added/updated/changed/deleted, etc.
     *
     * @var GuidAndName
     */
    protected $appId;
    
    /**
     * The ID of the person who's account was modified.
     *
     * @var GuidAndName
     */
    protected $personId;
    
    /**
     * If the modification was made on behalf of the person, this is the ID of that person who made the change.
     *
     * @var GuidAndName
     */
    protected $impersonatorId;
    
    /**
     * Whether the change was made through an online or offline method.
     *
     * @var AccessAvenue
     */
    protected $accessAvenue;
    
    /**
     * What action was taken.
     *
     * @var AuditAction
     */
    protected $auditAction;
    
    /**
     * Object constructor.
     *
     * @return Audit
     */
    public function __construct()
    {
        $this->timestamp        = null;
        $this->appId            = null;
        $this->personId         = null;
        $this->impersonatorId   = null;
        $this->accessAvenue     = null;
        $this->auditAction      = null;
    }

    /**
     * Sets the timestamp property.
     *
     * @throws InvalidParameterException
     *
     * @param string $timestamp a timestamp in Y-m-dTH:i:s.01Z format
     *
     * @return void
     */
    protected function setTimestamp($timestamp)
    {
        if (HealthVaultHelper::validateTimestamp($timestamp) == 0)
        {
            throw new InvalidParameterException('Timestamp must be in 2008-01-01T13:45:23.01Z format');
        }
        $this->timestamp = $timestamp;
    }
    
    /**
     * Sets the appId property.
     *
     * @throws InvalidParameterException
     *
     * @param GuidAndName $app_id the app id making the change
     *
     * @return void
     */
    protected function setAppId($app_id)
    {
        if (false == ($app_id instanceof GuidAndName))
        {
            throw new InvalidParameterException('appId must be a GuidAndName');
        }
        $this->appId = $app_id;
    }
    
    /**
     * Sets the personId property.
     *
     * @throws InvalidParameterException
     *
     * @param GuidAndName $person_id the ID of the person who's account is being changed
     *
     * @return void
     */
    protected function setPersonId($person_id)
    {
        if (false == ($person_id instanceof GuidAndName))
        {
            throw new InvalidParameterException('personId must be a GuidAndName');
        }
        $this->personId = $person_id;
    }
    
    /**
     * Sets the impersonatorId property.
     *
     * @throws InvalidParameterException
     *
     * @param GuidAndName $impersonator_id if made on behalf of the person, this is the id of the person who made the change
     *
     * @return void
     */
    protected function setImpersonatorId($impersonator_id)
    {
        if (false == ($impersonator_id instanceof GuidAndName))
        {
            throw new InvalidParameterException('impersonatorId must be a GuidAndName');
        }
        $this->impersonatorId = $impersonator_id;
    }
    
    /**
     * Sets the accessAvenue property.
     *
     * @throws InvalidParameterException
     *
     * @param AccessAvenue $access_avenue the way the change was made (online or offline)
     *
     * @return void
     */
    protected function setAccessAvenue($access_avenue)
    {
        if (false == ($access_avenue instanceof AccessAvenue))
        {
            throw new InvalidParameterException('accessAvenue must be an AccessAvenue');
        }
        $this->accessAvenue = $access_avenue;
    }
    
    /**
     * Sets the auditAction property.
     *
     * @throws InvalidParameterException
     *
     * @param AuditAction $audit_action what change was made
     *
     * @return void
     */
    protected function setAuditAction($audit_action)
    {
        if (false == ($audit_action instanceof AuditAction))
        {
            throw new InvalidParameterException('auditAction must be an AuditAction');
        }
        $this->auditAction = $audit_action;
    }
    
    /**
     * Generates the XML equilivalent for this complex type (as defined by HealthVault XSDs).
     *
     * @throws InvalidParameterException if $element_name is not a string
     *
     * @param string $element_name the name of the root element
     *
     * @return string
     */
    public function writeXml($element_name)
    {
        if (!is_string($element_name))
        {
            throw new InvalidParameterException('Element name must be a string');
        }
        
        $xw = new XMLWriter();
        $xw->openMemory();
        $xw->startElement($element_name);
        
        if ($this->timestamp != null)
        {
            $xw->writeElement('timestamp', $this->timestamp);
        }
        
        if ($this->appId != null)
        {
            $xw->writeRaw($this->appId->writeXml('app-id'));
        }
        
        if ($this->personId != null)
        {
            $xw->writeRaw($this->personId->writeXml('person-id'));
        }
        
        if ($this->impersonatorId != null)
        {
            $xw->writeRaw($this->impersonatorId->writeXml('impersonator-id'));
        }
        
        if ($this->accessAvenue != null)
        {
            $xw->writeElement('access-avenue', $this->accessAvenue);
        }
        
        if ($this->auditAction != null)
        {
            $xw->writeElement('audit-action', $this->auditAction);
        }

        $xw->endElement();
        return $xw->flush();
    }
}

?>