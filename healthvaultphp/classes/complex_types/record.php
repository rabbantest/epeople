<?php
/**
 *
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Jim Wordelman
 */

/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Jim Wordelman
 */
class Record
{
    /**
     * The id of the record
     * 
     * @var Guid
     */
    protected $_id = null;
    /**
     * Whether or not the individual associated with the record is the custodian
     * 
     * @var bool
     */
    protected $_isRecordCustodian = null;
    /**
     * The relation type
     * 
     * @var int
     */
    protected $_relType = null;
    /**
     * The name of the relation type
     * 
     * @var string
     */
    protected $_relName = null;
    /**
     * When the auth expires
     * 
     * @var string
     */
    protected $_authExpires = null;
    /**
     * Whether or not the auth is already expired
     *
     * @var bool 
     *
     */
    protected $_authExpired = null;
    /**
     * The display name for this record (usually first name)
     * 
     * @var string
     */
    protected $_displayName = null;
    /**
     * The state of the record
     * 
     * @var string
     */
    protected $_state = null;
    /**
     * The date the record was created
     * 
     * @var string
     */
    protected $_dateCreated = null;
    /**
     * The max size in bytes the record can hold
     * 
     * @var int
     */ 
    protected $_maxSizeBytes = null;
    /**
     * The current size of the record in bytes
     * 
     * @var int
     */
    protected $_sizeBytes = null;
    /**
     * The value of the record
     *
     * @var string 
     *
     */
    protected $_value = null;
    
    /**
     * Constructor that takes all of the parameters for creating the record
     * 
     * @param Guid   $id                The id of the record
     * @param bool   $isRecordCustodian Whether or not the associated person is the custodian
     * @param int    $relType           The numeric relation type
     * @param string $relName           The textual relation type
     * @param string $authExpires       When the auth expires
     * @param bool   $authExpired       Whether or not the auth is expired
     * @param string $displayName       The display name for the record
     * @param string $state             The state of the record
     * @param string $dateCreated       The date the record was created
     * @param int    $maxSizeBytes      The maximum size of the record in bytes
     * @param int    $sizeBytes         The current size of the record in bytes
     * @param string $value             The value of the record
     * 
     * @uses Record::setID()
     * @uses Record::setRecordCustodian()
     * @uses Record::setRelType()
     * @uses Record::setRelName()
     * @uses Record::setAuthExpires()
     * @uses Record::setAuthExpired()
     * @uses Record::setDisplayName()
     * @uses Record::setState()
     * @uses Record::setDateCreated()
     * @uses Record::setMaxSizeBytes()
     * @uses Record::setSizeBytes()
     * @uses Record::setValue()
     * 
     * @return void
     */
    public function __construct(Guid $id=null, $isRecordCustodian=null, $relType=null, 
        $relName=null, $authExpires=null, $authExpired=null, $displayName=null, $state=null, 
        $dateCreated=null, $maxSizeBytes=null, $sizeBytes=null, $value=null)
    {
        if($id != null)
        {
            $this->setID($id);
        }
        $this->setRecordCustodian($isRecordCustodian);
        $this->setRelType($relType);
        $this->setRelName($relName);
        $this->setAuthExpires($authExpires);
        $this->setAuthExpired($authExpired);
        $this->setDisplayName($displayName);
        $this->setState($state);
        $this->setDateCreated($dateCreated);
        $this->setMaxSizeBytes($maxSizeBytes);
        $this->setSizeBytes($sizeBytes);
        $this->setValue($value);
    }
    /**
     * Magic function to simulate properties being public
     * 
     * @param string $key The requested key
     * 
     * @uses Record::getId()
     * @uses Record::getRecordCustodian()
     * @uses Record::getRelType()
     * @uses Record::getRelName()
     * @uses Record::getAuthExpires()
     * @uses Record::getAuthExpired()
     * @uses Record::getDisplayName()
     * @uses Record::getState()
     * @uses Record::getDateCreated()
     * @uses Record::getMaxSizeBytes()
     * @uses Record::getSizeBytes()
     * @uses Record::getValue()
     * 
     * @return mixed The value of the member
     */
    public function __get($key)
    {
        switch ($key){
            case "ID":
            case "Id":
            case "id":
                return $this->getID();
            case "isRecordCustodian":
            case "IsRecordCustodian":
            case "recordCustodian":
            case "RecordCustodian":
                return $this->getRecordCustodian();
            case "relType":
            case "RelType":
                return $this->getRelType();
            case "relName":
            case "RelName":
                return $this->getRelName();
            case "authExpires":
            case "AuthExpires":
                return $this->getAuthExpires();
            case "authExpired":
            case "AuthExpired":
                return $this->getAuthExpired();
            case "displayName":
            case "DisplayName":
                return $this->getDisplayName();
            case "state":
            case "State":
                return $this->getState();
            case "dateCreated":
            case "DateCreated":
                return $this->getDateCreated();
            case "maxSizeBytes":
            case "MaxSizeBytes":
                return $this->getMaxSizeBytes();
            case "sizeBytes":
            case "SizeBytes":
                return $this->getSizeBytes();
            case "value":
            case "Value":
                return $this->getValue();
        }
    }
    /**
     * Magic function to simulate 'set' feature of properties
     * 
     * @param string $key The property to attempt to set
     * @param mixed $value The value to attempt to set the property to
     * 
     * @uses Record::getId()
     * @uses Record::setRecordCustodian()
     * @uses Record::setRelType()
     * @uses Record::setRelName()
     * @uses Record::setAuthExpired()
     * @uses Record::setAuthExpires()
     * @uses Record::setDisplayName()
     * @uses Record::setState()
     * @uses Record::setValue()
     * 
     * @return void
     */
    public function __set($key, $value)
    {
        switch ($key){
            case "isRecordCustodian":
            case "IsRecordCustodian":
            case "recordCustodian":
            case "RecordCustodian":
                $this->setRecordCustodian($value);
                break;
            case "relType":
            case "RelType":
                $this->setRelType($value);
                break;
            case "relName":
            case "RelName":
                $this->setRelName($value);
                break;
            case "authExpires":
            case "AuthExpires":
                $this->setAuthExpires($value);
                break;
            case "authExpired":
            case "AuthExpired":
                $this->setAuthExpired($value);
                break;
            case "displayName":
            case "DisplayName":
                $this->setDisplayName($value);
                break;
            case "state":
            case "State":
                $this->setState($value);
                break;
            case "value":
            case "Value":
                $this->setValue($value);
                break;
        }
    }
    
    // public getter and setter methods
    /**
     * Gets the record id
     * 
     * @return Guid The record id
     */
    public function getID()
    {
        return $this->_id;
    }
    
    /**
     * Gets whether or not the associated person is the record custodian
     * 
     * @return bool
     */
    public function getRecordCustodian()
    {
        return $this->_isRecordCustodian;
    }
    
    /**
     * Sets wheter or not the associated persion is the record custodian
     * 
     * @param bool $isCustodian The new value for record custodian
     */
    public function setRecordCustodian($isCustodian)
    {
        $this->_isRecordCustodian = $isCustodian==true;
    }
    
    /**
     * Gets the relation type
     * 
     * @return int The relation type
     */
    public function getRelType()
    {
        return $this->_relType;
    }
    
    /**
     * Sets the relation type to the provided parameter
     * 
     * @param int $relType The new relation type
     */
    public function setRelType($relType)
    {
        $this->_relType = (int)$relType;
    }
    
    /**
     * Gets the relation name
     * 
     * @return string The relation name
     */
    public function getRelName()
    {
        return $this->_relName;
    }
    
    /**
     * Sets the relation name
     * 
     * @param string $relName The new relation name
     */
    public function setRelName($relName)
    {
        $this->_relName = (string)$relName;
    }
    
    /**
     * Gets when the authorization expires
     * 
     * @return string When the auth expires
     */
    public function getAuthExpires()
    {
        return $this->_authExpires;
    }
    
    /**
     * Sets when the authorization expires
     * 
     * @param string $authExpires When the auth expires now
     */
    public function setAuthExpires($authExpires)
    {
        $this->_authExpires = (string)$authExpires;
    }
    
    /**
     * Gets whether or not the auth is already expired
     *
     * @return bool Whether or not the auth is already expired
     *
     */
    public function getAuthExpired()
    {
        return $this->_authExpired;
    }
    
    /**
     * Sets whether or not the authorization is already expired
     *
     * @param bool $authExpired Whehter or not the auth is already expired
     * @return void 
     *
     */
    public function setAuthExpired($authExpired)
    {
        $this->_authExpired = $authExpired == true;
    }
    /**
     * Gets the display name
     * 
     * @return string The display name
     */
    public function getDisplayName()
    {
        return $this->_displayName;
    }
    
    /**
     * Sets the display name to the parameter
     * 
     * @param string $displayName The new display name
     */
    public function setDisplayName($displayName)
    {
        $this->_displayName = (string)$displayName;
    }
    
    /**
     * Gets the current record state
     * 
     * @return string The current record state
     */
    public function getState()
    {
        return $this->_state;
    }
    
    /**
     * Sets the current record state
     * 
     * @param string $state The new current record state
     */
    public function setState($state)
    {
        $this->_state = (string)$state;
    }
    
    /**
     * Gets the date the record was created
     * 
     * @return string The date the record was created
     */
    public function getDateCreated()
    {
        return $this->_dateCreated;
    }
    
    /**
     * Gets the max size of the record in bytes
     * 
     * @return int The max size of the record in bytes
     */
    public function getMaxSizeBytes()
    {
        return $this->_maxSizeBytes;
    }
    
    /**
     * Gets the size of the record in bytes
     * 
     * @return int The size of the record in bytes
     */
    public function getSizeBytes()
    {
        return $this->_sizeBytes;
    }
    
    /**
     * Gets the value for the record
     *
     * @return string The value of the record
     *
     */
    public function getValue()
    {
        return $this->_value;
    }
    
    /**
     * Sets the value for the record
     *
     * @param string $value The value for the record
     * @return void 
     *
     */
    public function setValue($value)
    {
        $this->_value = (string)$value;
    }
    
    // XML parsing methods
    
    /**
     * Parses the xml from the input object into values for the record
     *
     * @param SimpleXMLElement $personXmlObj The XML object to read and parse
     * 
     * @return void
     *
     */
    private function parseXML(SimpleXMLElement $xmlObj)
    {
        $this->_id = new Guid((string)$xmlObj['id']);
        if(isset($xmlObj['record-custodian']))
        {
            $this->_isRecordCustodian = $xmlObj['record-custodian']=="true" ||
                $xmlObj['record-custodian']=="1";
        }
        $this->_relType = (int)$xmlObj['rel-type'];
        if(isset($xmlObj['rel-name']))
        {
            $this->_relName = (string)$xmlObj['rel-name'];
        }
        if(isset($xmlObj['auth-expires']))
        {
            $this->_authExpires = (string)$xmlObj['auth-expires'];
        }
        if(isset($xmlObj['auth-expired']))
        {
            $this->_authExpired = $xmlObj['auth-expired'] == "true" ||
                $xmlObj['auth-expired'] == "1";
        }
        if(isset($xmlObj['display-name']))
        {
            $this->_displayName = (string)$xmlObj['display-name'];
        }
        $this->_state = (string)$xmlObj['state'];
        if(isset($xmlObj['date-created']))
        {
            $this->_dateCreated = (string)$xmlObj['date-created'];
        }
        if(isset($xmlObj['max-size-bytes']))
        {
            $this->_maxSizeBytes = (int)$xmlObj['max-size-bytes'];
        }
        if(isset($xmlObj['size-bytes']))
        {
            $this->_sizeBytes = (int)$xmlObj['size-bytes'];
        }
        $this->_value = trim((string)$xmlObj);
    }
    
    /**
     * Creates a Record from the input XML
     *
     * @param SimpleXMLElement $xmlObj The XML object to parse
     * 
     * @uses Record::parseXML()
     * 
     * @return Record The object yielded from parsing the XML
     *
     */
    public static function fromXML(SimpleXMLElement $xmlObj)
    {
        $record = new Record();
        $record->parseXML($xmlObj);
        return $record;
    }
    // private setters for read only properties
    
    /**
     * Sets the id of the record
     * 
     * @param Guid $id The id of the record
     */
    private function setID(Guid $id)
    {
        $this->_id = $id;
    }
    
    /**
     * Sets the date created for the record
     * 
     * @param string $dateCreated The date the record was created
     */
    private function setDateCreated($dateCreated)
    {
        $this->_dateCreated = (string)$dateCreated;
    }
    
    /**
     * Sets the max size in bytes of the record
     * 
     * @param int $maxSizeBytes The max size
     */
    private function setMaxSizeBytes($maxSizeBytes)
    {
        $this->_maxSizeBytes = (int)$maxSizeBytes;
    }
    
    /**
     * Sets the size of the record (in bytes)
     * 
     * @param int $sizeBytes The size of the record in bytes
     */
    private function setSizeBytes($sizeBytes)
    {
        $this->_sizeBytes = (int)$sizeBytes;
    }
    
}
?>