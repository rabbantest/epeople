<?php
/**
 * 
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Patrick Sharp
 */

/**
 * 
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Patrick Sharp
 */
class ThingFormatSpec extends ComplexType
{

    /**
     * An array of ThingSectionSpec objects.
     *
     * @var array
     */
    protected $section;
    
    /**
     * An array of strings.
     *
     * @var array
     */
    protected $xml;
    
    /**
     * Object constructor.
     *
     * @param array  $section an array of ThingSectionSpec objects
     * @param array  $xml     an array of strings
     *
     * @return ThingRequestGroup
     */
    public function __construct($section = null, $xml = null)
    {
        if($section == null)
        {
            $this->section = null;
        }
        else
        {
            $this->setSection($section);
        }
        
        if($xml == null)
        {
            $this->xml = null;
        }
        else
        {
            $this->setXml($xml);
        }
    }

    /**
     * Sets the section property.
     *
     * @param array $section an array of ThingSectionSpec objects
     *
     * @return void
     */
    protected function setSection($section)
    {
        if(is_array($section))
        {
            foreach($section as $s)
            {
                if(false == ($s instanceof ThingSectionSpec))
                {
                    throw new InvalidParameterException('Section must be an array of ThingSectionSpecs');
                }
            }
        }
        else
        {
            throw new InvalidParameterException('Section must be an array of ThingSectionSpecs');
        }
        $this->section = $section;
    }
    
    /**
     * Sets the xml property.
     *
     * @param array $xml array of strings
     *
     * @return void
     */
    protected function setXml($xml)
    {
        if(is_array($xml))
        {
            foreach ($xml as $x)
            {
                if (!is_string($x))
                {
                    throw new InvalidParameterException('Xml must be an array of strings');
                }
            }
        }
        else
        {
            throw new InvalidParameterException('Xml must be an array of strings');
        }
        $this->xml = $xml;
    }
    
    /**
     * Returns the thing request group in XML format, which is used in HealthVault requests (namely, GetThings)
     *
     * @param string $elementName the name of the top-most XML element
     *
     * @return string
     */
    public function writeXml($elementName = 'format')
    {
        if (!is_string($elementName))
        {
            throw new InvalidParameterException('Element name must be a string');
        }
        
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->startElement($elementName);
        
        if($this->section != null)
        {
            foreach ($this->section as $section)
            {
                $xmlWriter->writeElement('section', $section->__toString());
            }
        }
        
        if($this->xml != null)
        {
            foreach ($this->xml as $xml)
            {
                $xmlWriter->writeElement('xml', $xml);
            }
        }
        else
        {
            $xmlWriter->writeElement('xml', '');
        }

        $xmlWriter->endElement();
        return $xmlWriter->flush();
    }
    
}

?>