<?php
/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class CodableValue extends ComplexType
{
    
    protected $text;
    protected $code;
    
    public function __construct()
    {
        $this->text = null;
        $this->code = null;
    }
    
    public static function fromXml($xmlObj)
    {
        $obj = new CodableValue();
        $obj->parseXml($xmlObj);
        return $obj;
    }
    
    protected function parseXml($xmlObj)
    {
        if (isset($xmlObj->text))
        {
            $this->text = (string) $xmlObj->text;
        }
        if (isset($xmlObj->code))
        {
            $this->code = CodedValue::fromXml($xmlObj->code);
        }
    }
    
    public function setText($value)
    {
        if (is_string($value))
        {
            $this->text = $value;
        }
        else
        {
            throw new InvalidParameterException('Text must be a string');
        }
    }
    
    public function setCode($value)
    {
        if (is_array($value))
        {
            foreach ($value as $val)
            {
                if (false == ($val instanceof CodedValue))
                {
                    throw new InvalidParameterException('Code must be an array of CodedValue objects');
                }
            }
        }
        else
        {
            throw new InvalidParameterException('Code must be an array of CodedValue objects');
        }
        $this->code = $value;
    }
    
    public function writeXml($tagName)
    {
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($tagName);
        
        if ($this->text != null)
        {
            $x->writeElement('text', $this->text);
        }
        if ($this->code != null)
        {
            foreach ($this->code as $code)
            {
                $x->writeRaw($code->writeXml('code'));
            }
        }
        
        $x->endElement();
        return $x->flush();
    }
    
}



?>