<?php
/**
 * 
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Jim Wordelman
 */

/**
 * 
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Jim Wordelman
 */
class ThingKey extends ComplexType
{
    
	/**
	 * The Guid of the thing id
	 *
	 * @var Guid 
	 *
	 */
	protected $thingId;
    
	/**
	 * The Guid for the version stamp
	 *
	 * @var Guid 
	 *
	 */
	protected $versionStamp;
	
	/**
	 * Constructor that can take either the version stamp or thing id Guids and set them
	 *
	 * @param Guid $thingId The thing's id
	 * @param Guid $versionStamp The version stamp of this thing
	 * @return void
	 *
	 */
	public function __construct(Guid $thingId=null, Guid $versionStamp=null)
	{
		if($thingId != null)
		{
			$this->setThingID($thingId);
		}
		else
		{
		    $this->thingId = null;
		}
		if($versionStamp != null)
		{
			$this->setVersionStamp($versionStamp);
		}
		else
		{
		    $this->versionStamp = null;
		}
	}
	
	/**
	 * Sets the thing id
	 *
	 * @param Guid $thingId The new thing id
	 * @return void 
	 *
	 */
	public function setThingId(Guid $thingId)
	{
	    if ($thingId instanceof Guid)
	    {
		    $this->thingId = $thingId;
	    }
	    else
	    {
	        throw new InvalidParameterException('Thing ID must be a Guid');
	    }
	}
	
	/**
	 * Sets the version stamp
	 *
	 * @param Guid $versionStamp The new version stamp
	 * @return void 
	 *
	 */
	public function setVersionStamp($versionStamp)
	{
	    if ($versionStamp instanceof Guid)
	    {
		    $this->versionStamp = $versionStamp;
	    }
	    else
	    {
	        throw new InvalidParameterException('Version stamp must be a Guid');
	    }
	}
	
	/**
	 * Turns this element into an XML representation of the data starting with the opening tag as specified
	 *
	 * @param string $tagName The opening tag for the XML
	 * @return string The object represented as XML
	 *
	 */
	public function writeXml($tagName)
	{
		if($this->thingId == null)
		{
            throw new RequiredVariableIsNullException('ThingId must contain a value');
		}
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->startElement($tagName);
        if ($this->versionStamp != null)
        {
            $xmlWriter->writeAttribute('version-stamp', $this->versionStamp);
        }
        $xmlWriter->text($this->thingId);
        $xmlWriter->endElement();
        return $xmlWriter->flush();
	}
    
}
?>