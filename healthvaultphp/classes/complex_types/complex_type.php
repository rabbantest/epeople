<?php
/** 
 * This file houses the ComplexType
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 * The ComplexType object
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
abstract class ComplexType
{
    
    /**
     * Getter
     *
     * @param string $name the property to get
     *
     * @return mixed
     */
    public function __get($name)
    {
        if (!property_exists($this, $name))
        {
            throw new InvalidPropertyException('Property ' . $name . ' not defined.');
        }
        else
        {
            return $this->$name;
        }
        
    }
    
    /**
     * Setter
     *
     * @param string $name  the property to set
     * @param string $value the value to set it to
     *
     * @return mixed
     */
    public function __set($name, $value)
    {
        $function = 'set' . ucwords($name);
        if (!property_exists($this, $name))
        {
            throw new InvalidPropertyException('Property ' . $name . ' not defined.');
        }
        else
        {
            if (method_exists($this, $function))
            {
                $this->$function($value);
            }
            else
            {
                throw new InvalidMethodException('Method ' . $function . ' not defined.');
            }
        }
    }
        
}


?>