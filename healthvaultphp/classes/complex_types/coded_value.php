<?php
/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author Dave Kiger
 */
class CodedValue extends ComplexType
{
    
    protected $value = null;
    protected $family = null;
    protected $type = null;
    protected $version = null;
    
    public static function fromXml($xmlObj)
    {
        $obj = new CodedValue();
        $obj->parseXml($xmlObj);
        return $obj;
    }
    
    protected function parseXml($xmlObj)
    {
        if (isset($xmlObj->value))
        {
            $this->value = (string) $xmlObj->value;
        }
        if (isset($xmlObj->family))
        {
            $this->family = (string) $xmlObj->family;
        }
        if (isset($xmlObj->type))
        {
            $this->type = (string) $xmlObj->type;
        }
        if (isset($xmlObj->version))
        {
            $this->type = (string) $xmlObj->version;
        }
    }
    
    public function setValue($value)
    {
        if (is_string($value))
        {
            $this->value = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a string');
        }
    }
    
    public function setFamily($value)
    {
        if (is_string($value))
        {
            $this->family = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a string');
        }
    }
    
    public function setType($value)
    {
        if (is_string($value))
        {
            $this->type = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a string');
        }
    }
    
    public function setVersion($value)
    {
        if (is_string($value))
        {
            $this->version = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a string');
        }
    }
    
    public function writeXml($tagName)
    {
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($tagName);
        
        if ($this->value != null)
        {
            $x->writeElement('value', $this->value);
        }
        if ($this->family != null)
        {
            $x->writeElement('family', $this->family);
        }
        if ($this->type != null)
        {
            $x->writeElement('type', $this->type);
        }
        if ($this->version != null)
        {
            $x->writeElement('version', $this->version);
        }
        
        $x->endElement();
        return $x->flush();
    }
}

?>