<?php

class VocabularyParameters extends ComplexType
{
    protected $vocabularyKey = null;
    protected $fixedCulture = false;
    
    protected function setVocabularyKey($value)
    {
        if ($value instanceof VocabularyKey)
        {
            $this->vocabularyKey = $value;
        }
        else
        {
            throw new InvalidParameterException('Value must be a VocabularyKey object.');
        }
    }
    
    protected function setFixedCulture($value)
    {
        if (is_bool($value))
        {
            $this->fixedCulture = $value;
        }
        else
        {
            throw new InvalidParameterException('Value must be a boolean.');
        }
    }
    
    public function writeXml($elementName)
    {
        if ($this->vocabularyKey == null)
        {
            throw new RequiredVariableIsNullException('The vocabulary key must be set.');
        }
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($elementName);
        $x->writeRaw($this->vocabularyKey->writeXml('vocabulary-key'));
        if ($this->fixedCulture === true)
        {
            $x->writeElement('fixed-culture', 'true');
        }
        else
        {
            $x->writeElement('fixed-culture', 'false');
        }
        $x->endElement();
        return $x->flush();
    }
    
}


?>