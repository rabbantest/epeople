<?php
/**
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class HvName extends ComplexType
{
    
    protected $full = null;
    protected $title = null;
    protected $first = null;
    protected $middle = null;
    protected $last = null;
    protected $suffix = null;
    
    public function setFull($value)
    {
        if (is_string($value))
        {
            $this->full = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a string');
        }
    }

    public function setTitle($value)
    {
        if ($value instanceof CodableValue)
        {
            $this->title = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a CodableValue object');
        }
    }

    public function setFirst($value)
    {
        if (is_string($value))
        {
            $this->first = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a string');
        }
    }

    public function setMiddle($value)
    {
        if (is_string($value))
        {
            $this->middle = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a string');
        }
    }

    public function setLast($value)
    {
        if (is_string($value))
        {
            $this->last = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a string');
        }
    }

    public function setSuffix($value)
    {
        if ($value instanceof CodableValue)
        {
            $this->suffix = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a CodableValue object');
        }
    }

    public static function fromXml($xmlObj)
    {
        $obj = new HvName();
        $obj->parseXml($xmlObj);
        return $obj;
    }
    
    protected function parseXml($xmlObj)
    {
        if (isset($xmlObj->full))
        {
            $this->full = (string) $xmlObj->full;
        }
        if (isset($xmlObj->title))
        {
            $this->title = CodableValue::fromXml($xmlObj->title);
        }
        if (isset($xmlObj->first))
        {
            $this->first = (string) $xmlObj->first;
        }
        if (isset($xmlObj->middle))
        {
            $this->middle = (string) $xmlObj->middle;
        }
        if (isset($xmlObj->last))
        {
            $this->last = (string) $xmlObj->last;
        }
        if (isset($xmlObj->suffix))
        {
            $this->suffix = CodableValue::fromXml($xmlObj->suffix);
        }
    }
    
    public function writeXml($tagName)
    {
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($tagName);
        
        if ($this->full != null)
        {
            $this->writeElement('full', $this->full);
        }
        if ($this->title != null)
        {
            $this->writeRaw($this->title->writeXml('title'));
        }
        if ($this->first != null)
        {
            $this->writeElement('first', $this->first);
        }
        if ($this->middle != null)
        {
            $this->writeElement('middle', $this->middle);
        }
        if ($this->last != null)
        {
            $this->writeElement('last', $this->last);
        }
        if ($this->suffix != null)
        {
            $this->writeRaw($this->suffix->writeXml('suffix'));
        }
        
        $x->endElement();
        return $x->flush();
    }
    
}

?>