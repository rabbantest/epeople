<?php
/**
 * 
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Patrick Sharp
 */

/**
 * The DataOther is used in a GetThings request.
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Patrick Sharp
 */
class DataOther extends ComplexType
{

    /**
     * A string holding the content type.
     *
     * @var string
     */
    protected $contentType;
    
    /**
     * A string holding the content encoding.
     *
     * @var string
     */
    protected $contentEncoding;
    
    /**
     * Object constructor.
     *
     * @param string  $contentType      a string containing the type
     * @param string  $contentEncoding  a string containing the encoding
     *
     * @return DataOther
     */
    public function __construct($contentType = '', $contentEncoding = '')
    {
        $this->setContentType($contentType);
        
        $this->setContentEncoding($contentEncoding);
    }

    /**
     * Sets the contentType property.
     *
     * @param string $contentType a string containing the type
     *
     * @return void
     */
    protected function setContentType($contentType)
    {
        if(!is_string($contentType))
        {
            throw new InvalidParameterException('ContentType must be a string');
        }
        $this->contentType = $contentType;
    }
    
    /**
     * Sets the contentEncoding property.
     *
     * @param string $contentEncoding string containing the encoding
     *
     * @return void
     */
    protected function setContentEncoding($contentEncoding)
    {
        if(!is_string($contentEncoding))
        {
            throw new InvalidParameterException('ContentEncoding must be a string');
        }
        $this->contentEncoding = $contentEncoding;
    }
    
    /**
     * Returns the thing request group in XML format, which is used in HealthVault requests (namely, GetThings)
     *
     * @param string $elementName the name of the top-most XML element
     *
     * @return string
     */
    public function writeXml($elementName)
    {
        if (!is_string($elementName))
        {
            throw new InvalidParameterException('Element name must be a string');
        }
        
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->startElement($elementName);
        
        if(strlen($this->contentType) > 0)
        {
            $xmlWriter->writeElement('content-type', $this->contentType);
        }
        
        if(strlen($this->contentEncoding) > 0)
        {
            $xmlWriter->writeElement('content-encoding', $this->contentEncoding);
        }

        $xmlWriter->endElement();
        return $xmlWriter->flush();
    }
    
}

?>