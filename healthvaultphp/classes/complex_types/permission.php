<?php
/** 
 * This file houses the Permission
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 * The Permission object
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class Permission extends EnumeratedType
{

    /**
     * All
     */
    const ALL = 'All';
    
    /**
     * Read
     */
    const READ = 'Read';
    
    /**
     * Update
     */
    const UPDATE = 'Update';
    
    /**
     * Create
     */
    const CREATE = 'Create';
    
    /**
     * Delete
     */
    const DELETE = 'Delete';

    /**
     * Validates that value is allowed type and sets the property.
     *
     * @param string $value the value of this type
     *
     * @return void
     */
    public function setValue($value)
    {
        switch ($value)
        {
            case Permission::ALL:
            case Permission::READ:
            case Permission::UPDATE:
            case Permission::CREATE:
            case Permission::DELETE:
                $this->value = $value;
                break;
            default:
                throw new InvalidParameterException('Invalid enumeration.');
        }
    }

}

?>