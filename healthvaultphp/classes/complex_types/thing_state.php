<?php
/** 
 * This file houses the ThingState
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 * The ThingState object represents the current state of a Thing object.
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class ThingState extends EnumeratedType
{

    /**
     * Active state
     */
    const ACTIVE = 'Active';
    
    /**
     * Deleted state
     */
    const DELETED = 'Deleted';

    /**
     * Validates that value is allowed type and sets the property.
     *
     * @param string $value the value of this type
     *
     * @return void
     */
    public function setValue($value)
    {
        switch ($value)
        {
            case ThingState::ACTIVE:
            case ThingState::DELETED:
                $this->value = $value;
                break;
            default:
                throw new InvalidParameterException('Thing state is not a valid enumeration.');
        }
    }

}

?>