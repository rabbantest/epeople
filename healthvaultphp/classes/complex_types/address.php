<?php
/** 
 * This file houses the Address class.
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger <no-email@please.net>
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * Complex type representing an Address.
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger <no-email@please.net>
 */
class Address extends ComplexType
{
    
    /**
     * A user-provided description of this address.
     * @var string
     */
    protected $description = null;

    /**
     * Whether or not this is the primary contact address.
     * @var bool
     */
    protected $isPrimary = false;

    /**
     * Street address line.  It is an array of strings to allow for multiple-lined street addresses.
     * @var array
     */
    protected $street = null;

    /**
     * Town/city.
     * @var string
     */
    protected $city = null;

    /**
     * State/province.  Can be in any format -- do not expect a two-letter US state abbreviation.
     * @var string
     */
    protected $state = null;

    /**
     * Postal code.
     * @var string
     */
    protected $postcode = null;

    /**
     * Country.  Can be in any format -- do not exactly a two- or three-letter abbreviation.
     * @var string
     */
    protected $country = null;
    
    /**
     * Method used by the ComplexType::__set() method to set the description property.
     * 
     * @throws InvalidParameterException if $value is not a string
     * 
     * @param string $value the value to set this field to
     *
     * @return void
     */
    protected function setDescription($value) 
    {
        if (!is_string($value))
        {
            throw new InvalidParameterException('Value must be a string');
        }
        $this->description = $value;
    }
    
    /**
     * Method used by the ComplexType::__set() method to set the isPrimary property.
     * 
     * @throws InvalidParameterException if $value is not a boolean
     * 
     * @param bool $value the value to set this field to
     *
     * @return void
     */
    protected function setIsPrimary($value)
    {
        if (!is_bool($value))
        {
            throw new InvalidParameterException('Value must be a boolean');
        }
        $this->isPrimary = $value;
    }
    
    /**
     * Method used by the ComplexType::__set() method to set the street property.
     * 
     * @throws InvalidParameterException if $value is not an array of strings
     * 
     * @param array $value the value to set this field to
     *
     * @return void
     */
    protected function setStreet($value)
    {
        if (count($value) > 0)
        {
            foreach ($value as $val)
            {
                if (!is_string($val))
                {
                    throw new InvalidParameterException('Value must be an array of strings.');
                }
            }
            $this->street = $value;
        }
        else
        {
            $this->street = array();
        }
    }
    
    /**
     * Method used by the ComplexType::__set() method to set the city property.
     * 
     * @throws InvalidParameterException if $value is not a string
     * 
     * @param string $value the value to set this field to
     *
     * @return void
     */
    protected function setCity($value)
    {
        if (!is_string($value))
        {
            throw new InvalidParameterException('Value must be a string');
        }
        $this->city = $value;
    }
    
    /**
     * Method used by the ComplexType::__set() method to set the state property.
     * 
     * @throws InvalidParameterException if $value is not a string
     * 
     * @param string $value the value to set this field to
     *
     * @return void
     */
    protected function setState($value)
    {
        if (!is_string($value))
        {
            throw new InvalidParameterException('Value must be a string');
        }
        $this->state = $value;
    }
    
    /**
     * Method used by the ComplexType::__set() method to set the postcode property.
     * 
     * @throws InvalidParameterException if $value is not a string
     * 
     * @param string $value the value to set this field to
     *
     * @return void
     */
    protected function setPostcode($value)
    {
        if (!is_string($value))
        {
            throw new InvalidParameterException('Value must be a string');
        }
        $this->postcode = $value;
    }
    
    /**
     * Method used by the ComplexType::__set() method to set the country property.
     * 
     * @throws InvalidParameterException if $value is not a string
     * 
     * @param string $value the value to set this field to
     *
     * @return void
     */
    protected function setCountry($value)
    {
        if (!is_string($value))
        {
            throw new InvalidParameterException('Value must be a string');
        }
        $this->country = $value;
    }
    
    /**
     * Generates the XML equilivalent for this complex type (as defined by HealthVault XSDs).
     *
     * @throws InvalidParameterException if $tag_name is not a string
     *
     * @param string $tag_name the name of the root element
     *
     * @return string
     */
    public function writeXml($tag_name)
    {
        if (!is_string($tag_name))
        {
            throw new InvalidParameterException('Element name must be a string');
        }
        
        $xw = new XmlWriter();
        $xw->openMemory();
        
        $xw->startElement($tag_name);
        
        if (strlen($this->description) > 0)
        {
            $xw->writeElement('description', $this->description);
        }
        if ($this->isPrimary === true)
        {
            $xw->writeElement('is-primary', 'true');
        }
        else
        {
            $xw->writeElement('is-primary', 'false');
        }
        if (is_array($this->street) && count($this->street) > 0)
        {
            foreach ($this->street as $street)
            {
                $xw->writeElement('street', $street);
            }
        }
        if (strlen($this->city) > 0)
        {
            $xw->writeElement('city', $this->city);
        }
        if (strlen($this->state) > 0)
        {
            $xw->writeElement('state', $this->state);
        }
        if (strlen($this->postcode) > 0)
        {
            $xw->writeElement('postcode', $this->postcode);
        }
        if (strlen($this->country) > 0)
        {
            $xw->writeElement('country', $this->country);
        }
        $xw->endElement();
        return $xw->flush();
    }
}

?>