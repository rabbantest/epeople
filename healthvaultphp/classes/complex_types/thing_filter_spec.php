<?php
/**
 * 
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Jim Wordelman
 */

/**
 * 
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Jim Wordelman
 */
class ThingFilterSpec extends ComplexType
{
    
    /**
     * Type ID
     *
     * @var Guid
     */
    protected $typeId;
    
    /**
     * Thing state
     *
     * @var ThingState
     */
    protected $thingState;
    
    /**
     * Effective date lower bound
     *
     * @var string
     */
    protected $effDateMin;
    
    /**
     * Effective date upper bound
     *
     * @var string
     */
    protected $effDateMax;
    
    /**
     * Created by Application ID
     *
     * @var Guid
     */
    protected $createdAppId;
    
    /**
     * Created by Person ID
     *
     * @var Guid
     */
    protected $createdPersonId;
    
    /**
     * Updated by App ID
     * 
     * @var Guid
     */
    protected $updatedAppId;
    
    /**
     * Updated by Person ID
     *
     * @var Guid
     */
    protected $updatedPersonId;
    
    /**
     * Created date lower bound
     *
     * @var string
     */
    protected $createdDateMin;
    
    /**
     * Created date upper bound
     *
     * @var string
     */
    protected $createdDateMax;
    
    /**
     * Updated date lower bound
     *
     * @var string
     */
    protected $updatedDateMin;
    
    /**
     * Updated date upper bound
     *
     * @var string
     */
    protected $updatedDateMax;
    
    /**
     * XPath
     *
     * @var string
     */
    protected $xpath;
    
    /**
     * Object constructor
     *
     * @return ThingFilterSpec
     */
    public function __construct($typeId = null)
    {
        if ($typeId != null)
        {
            $this->setTypeId($typeId);
        }
        $this->thingState = array ( new ThingState('Active') );
        $this->effDateMin = '';
        $this->effDateMax = '';
        $this->createdAppId = null;
        $this->createdPersonId = null;
        $this->updatedAppId = null;
        $this->updatedPersonId = null;
        $this->createdDateMin = '';
        $this->createdDateMax = '';
        $this->updatedDateMin = '';
        $this->updatedDateMax = '';
        $this->xpath = '';
    }
    
    /**
     * Returns the XML for this object
     *
     * @param string $tagName the name of the top-most XML tag
     *
     * @return string
     */
    public function writeXml($tagName)
    {
        $xmlWriter = new XmlWriter();
        $xmlWriter->openMemory();
        $xmlWriter->startElement($tagName);
        if (is_array($this->typeId) && count($this->typeId) > 0)
        {
            foreach ($this->typeId as $typeId)
            {
                $xmlWriter->writeElement('type-id', $typeId);
            }
        }
        
        for ($i = 0; $i < 2; $i++)
        {
            if (isset($this->thingState[$i]))
            {
                $xmlWriter->writeElement('thing-state', $this->thingState[$i]);
            }
        }
        
        if (!empty($this->effDateMin))
        {
            $xmlWriter->writeElement('eff-date-min', $this->effDateMin);
        }
        
        if (!empty($this->effDateMax))
        {
            $xmlWriter->writeElement('eff-date-max', $this->effDateMax);
        }
        
        if ($this->createdAppId != null)
        {
            $xmlWriter->writeElement('created-app-id', $this->createdAppId);
        }
        
        if ($this->createdPersonId != null)
        {
            $xmlWriter->writeElement('created-person-id', $this->createdPersonId);
        }
        
        if ($this->updatedAppId != null)
        {
            $xmlWriter->writeElement('updated-app-id', $this->updatedAppId);
        }
        
        if ($this->updatedPersonId != null)
        {
            $xmlWriter->writeElement('updated-person-id', $this->updatedPersonId);
        }
        
        if (!empty($this->createdDateMin))
        {
            $xmlWriter->writeElement('created-date-min', $this->createdDateMin);
        }
        
        if (!empty($this->createdDateMax))
        {
            $xmlWriter->writeElement('created-date-max', $this->createdDateMax);
        }
        
        if (!empty($this->updatedDateMin))
        {
            $xmlWriter->writeElement('updated-date-min', $this->updatedDateMin);
        }
        
        if (!empty($this->updatedDateMax))
        {
            $xmlWriter->writeElement('updated-date-max', $this->updatedDateMax);
        }
        
        if (!empty($this->xpath))
        {
            $xmlWriter->writeElement('xpath', $this->xpath);
        }
        
        $xmlWriter->endElement();
        $xml =  $xmlWriter->flush();
        return $xml;
    }
    
    /**
     * Sets the typeID property
     *
     * @param Guid $value the value to set
     *
     * @return void
     */
    protected function setTypeId($value)
    {
        if (!is_array($value))
        {
            throw new InvalidParameterException('Value must be an array of Guid objects');
        }
        foreach ($value as $val)
        {
            if (false == ($val instanceof Guid))
            {
                throw new InvalidParameterException('Value must be an array of Guid objects');
            }
        }
        $this->typeId = $value;
    }
    
    /**
     * Sets the thingState property
     *
     * @param ThingState $value the value to set
     *
     * @return void
     */
    protected function setThingState($value)
    {
        if (false == ($value instanceof ThingState))
        {
            throw new InvalidParameterException('Value must be a ThingState');
        }
        else
        {
            $this->thingState = $value;
        }
    }
    
    /**
     * Sets the effDateMin property
     *
     * @param string $value the value to set
     *
     * @return void
     */
    protected function setEffDateMin($value)
    {
        if (!HealthVaultHelper::validateTimestamp($value))
        {
            throw new InvalidParameterException('Value must be a valid dateTime.');
        }
        else
        {
            $this->effDateMin = $value;
        }
    }
    
    /**
     * Sets the effDateMax property
     *
     * @param string $value the value to set
     *
     * @return void
     */
    protected function setEffDateMax($value)
    {
        if (!HealthVaultHelper::validateTimestamp($value))
        {
            throw new InvalidParameterException('Value must be a valid dateTime.');
        }
        else
        {
            $this->effDateMax = $value;
        }
    }
    
    /**
     * Sets the createdAppId property
     *
     * @param Guid $value the value to set
     *
     * @return void
     */
    protected function setCreatedAppId($value)
    {
        if (false == ($value instanceof Guid))
        {
            throw new InvalidParameterException('Value must be a Guid');
        }
        else
        {
            $this->createdAppId = $value;
        }
    }
    
    /**
     * Sets the createdPersonId property
     *
     * @param Guid $value the value to set
     *
     * @return void
     */
    protected function setCreatedPersonId($value)
    {
        if (false == ($value instanceof Guid))
        {
            throw new InvalidParameterException('Value must be a Guid');
        }
        else
        {
            $this->createdPersonId = $value;
        }
    }
    
    /**
     * Sets the updatedAppId property
     *
     * @param Guid $value the value to set
     *
     * @return void
     */
    protected function setUpdatedAppId($value)
    {
        if (false == ($value instanceof Guid))
        {
            throw new InvalidParameterException('Value must be a Guid');
        }
        else
        {
            $this->updatedAppId = $value;
        }
    }
    
    /**
     * Sets the updatedPersonId property
     *
     * @param Guid $value the value to set
     *
     * @return void
     */
    protected function setUpdatedPersonId($value)
    {
        if (false == ($value instanceof Guid))
        {
            throw new InvalidParameterException('Value must be a Guid');
        }
        else
        {
            $this->updatedPersonId = $value;
        }
    }
    
    /**
     * Sets the createdDateMin property
     *
     * @param string $value the value to set
     *
     * @return void
     */
    protected function setCreatedDateMin($value)
    {
        if (!HealthVaultHelper::validateTimestamp($value))
        {
            throw new InvalidParameterException('Value must be a valid dateTime.');
        }
        else
        {
            $this->createdDateMin = $value;
        }
    }
    
    /**
     * Sets the createdDateMax property
     *
     * @param string $value the value to set
     *
     * @return void
     */
    protected function setCreatedDateMax($value)
    {
        if (!HealthVaultHelper::validateTimestamp($value))
        {
            throw new InvalidParameterException('Value must be a valid dateTime.');
        }
        else
        {
            $this->createdDateMax = $value;
        }
    }
    
    /**
     * Sets the updatedDateMin property
     *
     * @param string $value the value to set
     *
     * @return void
     */
    protected function setUpdatedDateMin($value)
    {
        if (!HealthVaultHelper::validateTimestamp($value))
        {
            throw new InvalidParameterException('Value must be a valid dateTime.');
        }
        else
        {
            $this->updatedDateMin = $value;
        }
    }
    
    /**
     * Sets the updatedDateMax property
     *
     * @param string $value the value to set
     *
     * @return void
     */
    protected function setUpdatedDateMax($value)
    {
        if (!HealthVaultHelper::validateTimestamp($value))
        {
            throw new InvalidParameterException('Value must be a valid dateTime.');
        }
        else
        {
            $this->updatedDateMax = $value;
        }
    }
    
    /**
     * Sets the xpath property
     *
     * @param string $value the value to set
     *
     * @return void
     */
    protected function setXpath($value)
    {
        if (!is_string($value))
        {
            throw new InvalidParameterException('Value must be a string');
        }
        else
        {
            $this->xpath = $value;
        }
    }
    
    
}

?>