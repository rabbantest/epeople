<?php
/**
 * A holder for Thing Key entries for xml creation
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Jim Wordelman
 */
 
/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Jim Wordelman
 */
class ThingResponseGroup 
{
    /**
     * An array of things that are part of this group
     *
     * @var array[int]Thing 
     *
     */
    protected $things = array();
    /**
     * An array of unprocessedThingKeyInfos
     *
     * @var array[int]UnprocessedThingKeyInfo 
     *
     */
    protected $unprocessedThingKeyInfos = array();
    /**
     * Whether or not this group was filtered
     *
     * @var bool 
     *
     */
    protected $filtered;
    /**
     * The name of the group if one is provided
     *
     * @var string 
     *
     */
    protected $name;
    
    /**
     * Constructor allow the variables for this group to be set
     *
     * @param array $things An array of things
     * @param array $unprocessedThingKeyInfos An array of UnprocessedThingKeyInfos
     * @param mixed $filtered Whether or not the group is filtered
     * @param mixed $name This is a description
     * @return mixed This is the return value description
     *
     */
    public function __construct(array $things=null, array $unprocessedThingKeyInfos=null, $filtered=null, $name=null)
    {
        if($things != null)
        {
            foreach($things as $thing)
            {
                if(!is_a($thing, 'Thing'))
                {
                    throw new InvalidParameterException('Things must be an array of Things');
                }
            }
            $this->things = $things;
        }
        if($unprocessedThingKeyInfos != null)
        {
            foreach($unprocessedThingKeyInfos as $info)
            {
                if(!is_a($info, 'UnprocessedThingKeyInfo'))
                {
                    throw new InvalidParameterException('UnprocessedThingKeyInfos must be an array of UnprocessedThingKeyInfo objects');
                }
            }
            $this->unprocessedThingKeyInfos = $unprocessedThingKeyInfos;
        }
        $this->filtered = (bool)$filtered;
        $this->name     = (string)$name;
       
    }
    
    /**
     * Magic method to allow reading of protected members
     *
     * @param string $key The member being requested
     * @return mixed The value of that member
     *
     */
    public function __get($key)
    {
        switch ($key)
        {
            case "things":
            case "Things":
                return $this->getThings();
            case "unprocessedThingKeyInfos":
            case "UnprocessedThingKeyInfos":
                return $this->getUnprocessedThingKeyInfos();
            case "filtered":
            case "Filtered":
            case "isFiltered":
            case "IsFiltered":
                return $this->getIsFiltered();
            case "name":
            case "Name":
                return $this->getName();
        }
    }
    
    /**
     * Gets the set of things
     *
     * @return array The array of things
     *
     */
    public function getThings()
    {
        return $this->things;
    }
    
    /**
     * Gets the list of UnprocessedThingKeyInfo objects
     *
     * @return array The list of UnprocessedThingKeyInfo objects
     *
     */
    public function getUnprocessedThingKeyInfos()
    {
        return $this->unprocessedThingKeyInfos;
    }
    
    /**
     * Returns whether or not this group is filtered
     *
     * @return bool Whether or not the group is filtered
     *
     */
    public function getIsFiltered()
    {
        return $this->filtered;
    }
    
    /**
     * Gets the name of of the group if there is one
     *
     * @return string The name of the group
     *
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Writes the XML for this object starting with the provided tag name
     *
     * @param string $startTag The name for the starting tag
     * 
     * @uses Thing::writeXML()
     * @uses UnprocessedThingKeyInfo::writeXML()
     * 
     * @return string The XML for this object
     *
     */
    public function writeXML($startTag)
    {
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->startElement($startTag);
        if($this->name != null)
        {
            $xmlWriter->writeAttribute('name', $this->name);
        }
        foreach($this->things as $thing)
        {
            $xmlWriter->writeRaw($thing->writeXML('thing'));
        }
        foreach($this->unprocessedThingKeyInfos as $info)
        {
            $xmlWriter->writeRaw($info->writeXML('unprocessed-thing-key-info'));
        }
        $xmlWriter->writeElement('filtered', $this->filtered?'true':'false');
        $xmlWriter->endDocument();
        return $xmlWriter->flush();
    }
    
    /**
     * Creates a ThingResposneGroup from the provided XML object
     *
     * @param SimpleXMLElement $xmlObj The XML object to parse
     * @return ThingResponseGroup The response group created from the provided XML
     *
     */
    public static function fromXML(SimpleXMLElement $xmlObj)
    {
        $trg = new ThingResponseGroup();
        $trg->parseXml($xmlObj);
        return $trg;
    }
    
    /**
     * Actually parses the XML and sets the internal variables
     *
     * @param SimpleXMLElement $xmlObj The XML to parse
     * @return void
     *
     */
    private function parseXml(SimpleXMLElement $xmlObj)
    {
        if(isset($xmlObj['name']))
        {
            $this->name = (string)$xmlObj['name'];
        }
        $thingElements = $xmlObj->xpath('thing');
        foreach($thingElements as $thingElement)
        {
            $this->things[] = Thing::fromXML($thingElement);
        }
        $unprocessedKeyInfoElements = $xmlObj->xpath('unprocessed-thing-key-info');
        foreach($unprocessedKeyInfoElements as $infoElement)
        {
            $this->unprocessedThingKeyInfos[] = UnprocessedKeyInfo::fromXML($infoElement);
        }
        if(isset($xmlObj->filtered))
        {
            $this->filtered = $xmlObj->filtered == "true" || $xmlObj->filtered == "1";
        }
    }
}
?>