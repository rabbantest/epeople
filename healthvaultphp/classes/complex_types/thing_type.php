<?php
/**
 * 
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Patrick Sharp
 */

/**
 * 
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Patrick Sharp
 */
class ThingType extends ComplexType
{

    /**
     * A Guid object.
     *
     * @var Guid
     */
    protected $id;
    
    /**
     * A string value holding an attribute.
     *
     * @var string
     */
    protected $name;
    
    /**
     * Object constructor.
     *
     * @param Guid    $id   a Guid object
     * @param string  $name a name attribute
     *
     * @return GuidAndName
     */
    public function __construct($id = null, $name = '')
    {
        if($id == null)
        {
            $this->id = null;
        }
        else
        {
            $this->setId($id);
        }
        
        $this->setName($name);
    }

    /**
     * Sets the id property.
     *
     * @param Guid $id a Guid object
     *
     * @return void
     */
    protected function setId($id)
    {
        if(false == ($id instanceof Guid))
        {
            throw new InvalidParameterException('id must be a Guid object');
        }
        $this->id = $id;
    }
    
    /**
     * Sets the name property.
     *
     * @param string $name string value of the name attribute
     *
     * @return void
     */
    protected function setName($name)
    {
        if(!is_string($name))
        {
            throw new InvalidParameterException('name must be a string');
        }
        $this->name = $name;
    }
    
    /**
     * Returns the thing request group in XML format, which is used in HealthVault requests (namely, GetThings)
     *
     * @param string $elementName the name of the top-most XML element
     *
     * @return string
     */
    public function writeXml($elementName)
    {
        if (!is_string($elementName))
        {
            throw new InvalidParameterException('Element name must be a string');
        }
        
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->startElement($elementName);
        
        if(strlen($this->name) > 0)
        {
            $xmlWriter->writeAttribute('name', $this->name);
        }
        if($this->id != null)
        {
            $xmlWriter->text($this->id);
        }

        $xmlWriter->endElement();
        return $xmlWriter->flush();
    }
    
    public function __toString()
    {
        return $this->id;
    }
    
}

?>