<?php
/**
 * A Guid
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Jim Wordelman
 */

/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Jim Wordelman
 */
class Guid
{
    
    /**
     * The guid being stored
     *
     * @var string 
     *
     */
    public $guid = '';
    
    
    /**
     * Constructs a Guid object out of a string guid that is validated
     *
     * @param string $guid The guid to use
     * @return void 
     *
     */
    public function __construct($guid)
    {
        //if(!self::verifyGuid($guid))
        //{
        //    throw new InvalidParameterFormatException("Invalid format for a Guid");
        //}
        $this->guid = $guid;
    }
    
    /**
     * Verifies the provided string is a properly formed Guid
     *
     * @param string $toVerify The Guid as a string to verify
     * @return bool Whether or not the string is a valid Guid
     *
     */
    public static function verifyGuid($toVerify)
    {
        return (0 < preg_match("@^(\{){0,1}[0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12}(\}){0,1}$@i", $toVerify)) && 
            ((strlen($toVerify)==38 && $toVerify{0} == "{" && $toVerify{37} == "}") ||
                (strlen($toVerify)==36 && $toVerify{0} != "{" && $toVerify{35} != "}"));
    }
    
    
    /**
     * Turns the Guid into a string (which is the Guid)
     *
     * @return string The Guid
     *
     */
    public function __toString()
    {
        return $this->guid;
    }
}
?>