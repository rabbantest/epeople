<?php
/**
 *
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class HvPersonal extends ComplexType
{

    protected $name = null;
    protected $birthdate = null;
    protected $bloodType = null;
    protected $ethnicity = null;
    protected $ssn = null;
    protected $maritalStatus = null;
    protected $employmentStatus = null;
    protected $isDeceased = null;
    protected $dateOfDeath = null;
    protected $religion = null;
    protected $isVeteran = null;
    protected $highestEducationLevel = null;
    protected $isDisabled = null;
    protected $organDonor = null;
    
    public function setName($value)
    {
        if ($value instanceof HvName)
        {
            $this->name = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be an HvName object');
        }
    }
    
    public function setBirthdate($value)
    {
        if ($value instanceof HvDateTime)
        {
            $this->birthdate = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be an HvDateTime object');
        }
    }

    public function setBloodType($value)
    {
        if ($value instanceof CodableValue)
        {
            $this->bloodType = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a CodableValue object');
        }
    }
    
    public function setEthnicity($value)
    {
        if ($value instanceof CodableValue)
        {
            $this->ethnicity = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be an CodableValue object');
        }
    }
    
    public function setSsn($value)
    {
        if (is_string($value))
        {
            $this->ssn = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a string');
        }
    }
    
    public function setMaritalStatus($value)
    {
        if ($value instanceof CodableValue)
        {
            $this->maritalStatus = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be an CodableValue object');
        }
    }
    
    public function setEmploymentStatus($value)
    {
        if (is_string($value))
        {
            $this->employmentStatus = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a string');
        }
    }
    
    public function setIsDeceased($value)
    {
        if (is_bool($value))
        {
            $this->isDeceased = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a boolean');
        }
    }
    
    public function setDateOfDeath($value)
    {
        if ($value instanceof ApproxDateTime)
        {
            $this->dateOfDeath = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be an ApproxDateTime object');
        }
    }
    
    public function setReligion($value)
    {
        if ($value instanceof CodableValue)
        {
            $this->religion = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be an CodableValue object');
        }
    }
    
    public function setIsVeteran($value)
    {
        if (is_bool($value))
        {
            $this->isVeteran = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a boolean');
        }
    }
    
    public function setHighestEducationLevel($value)
    {
        if ($value instanceof CodableValue)
        {
            $this->highestEducationLevel = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be an CodableValue object');
        }
    }

    public function setIsDisabled($value)
    {
        if (is_bool($value))
        {
            $this->isDisabled = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a boolean');
        }
    }
    
    public function setOrganDonor($value)
    {
        if (is_string($value))
        {
            $this->organDonor = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a string');
        }
    }
    
    public static function fromXml($xmlObj)
    {
        $obj = new HvPersonal();
        $obj->parseXml($xmlObj);
        return $obj;
    }

    protected function parseXml($xmlObj)
    {
        if (isset($xmlObj->name))
        {
            $this->name = HvName::fromXml($xmlObj->name);
        }
        if (isset($xmlObj->birthdate))
        {
            $this->birthdate = HvDateTime::fromXml($xmlObj->birthdate);
        }
        if (isset($xmlObj->{'blood-type'}))
        {
            $this->bloodType = CodableValue::fromXml($xmlObj->{'blood-type'});
        }
        if (isset($xmlObj->ethnicity))
        {
            $this->ethnicity = CodableValue::fromXml($xmlObj->ethnicity);
        }
        if (isset($xmlObj->ssn))
        {
            $this->ssn = (string) $xmlObj->ssn;
        }
        if (isset($xmlObj->{'marital-status'}))
        {
            $this->maritalStatus = CodableValue::fromXml($xmlObj->{'marital-status'});
        }
        if (isset($xmlObj->{'employment-status'}))
        {
            $this->employmentStatus = (string) $xmlObj->{'employment-status'};
        }
        if (isset($xmlObj->{'is-deceased'}))
        {
            $value = (string) $xmlObj->{'is-deceased'};
            if (strcasecmp($value, 'true') == 0)
            {
                $this->isDeceased = true;
            }
            else
            {
                $this->isDeceased = false;
            }
        }
        if (isset($xmlObj->{'date-of-death'}))
        {
            $this->dateOfDeath = ApproxDateTime::fromXml($xmlObj->{'date-of-death'});
        }
        if (isset($xmlObj->religion))
        {
            $this->religion = CodableValue::fromXml($xmlObj->religion);
        }
        if (isset($xmlObj->{'is-veteran'}))
        {
            $value = (string) $xmlObj->{'is-veteran'};
            if (strcasecmp($value, 'true') == 0)
            {
                $this->isVeteran = true;
            }
            else
            {
                $this->isVeteran = false;
            }
        }
        if (isset($xmlObj->{'is-disabled'}))
        {
            $value = (string) $xmlObj->{'is-disabled'};
            if (strcasecmp($value, 'true') == 0)
            {
                $this->isDisabled = true;
            }
            else
            {
                $this->isDisabled = false;
            }
        }
        if (isset($xmlObj->{'highest-education-level'}))
        {
            $this->highestEducationLevel = CodableValue::fromXml($xmlObj->{'highest-education-level'});
        }
        if (isset($xmlObj->{'organ-donor'}))
        {
            $this->organDonor = (string) $xmlObj->{'organ-donor'};
        }        
    }
    
    public function writeXml($tagName)
    {
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($tagName);
        
        if ($this->name != null)
        {
            $x->writeRaw($this->name->writeXml('name'));
        }
        if ($this->birthdate != null)
        {
            $x->writeRaw($this->birthdate->writeXml('birthdate'));
        }
        if ($this->bloodType != null)
        {
            $x->writeRaw($this->bloodType->writeXml('blood-type'));
        }
        if ($this->ethnicity != null)
        {
            $x->writeRaw($this->ethnicity->writeXml('ethnicity'));
        }
        if ($this->ssn != null)
        {
            $x->writeElement('ssn', $this->ssn);
        }
        if ($this->maritalStatus != null)
        {
            $x->writeRaw($this->ethnicity->writeXml('marital-status'));
        }
        if ($this->elmploymentStatus != null)
        {
            $x->writeElement('employment-status', $this->employmentStatus);
        }
        if ($this->isDeceased != null)
        {
            $x->writeElement('is-deceased', (integer) $this->isDeceased);
        }
        if ($this->dateOfDeath != null)
        {
            $x->writeRaw($this->dateOfDeath->writeXml('date-of-death'));
        }
        if ($this->religion != null)
        {
            $x->writeRaw($this->religion->writeXml('religion'));
        }
        if ($this->isVeteran != null)
        {
            $x->writeElement('is-veteran', (integer) $this->isVeteran);
        }
        if ($this->highestEducationLevel != null)
        {
            $x->writeRaw($this->highestEducationLevel->writeXml('highest-education-level'));
        }
        if ($this->isDisabled != null)
        {
            $x->writeElement('is-disabled', (integer) $this->isDisabled);
        }
        if ($this->organDonor != null)
        {
            $x->writeElement('organ-donor', $this->organDonor);
        }
        
        $x->endElement();
        return $x->flush();
    }
    
}

?>