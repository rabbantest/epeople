<?php
/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class Contact extends ComplexType
{
    
    protected $address;
    protected $phone;
    protected $email;
    
    protected function setAddress($value)
    {
        if (!is_array($value))
        {
            throw new InvalidParameterException('Value must be an array of Address objects.');
        }
        foreach ($value as $val)
        {
            if (false == ($val instanceof Address))
            {
                throw new InvalidParameterException('Value must be an array of Address objects.');
            }
        }
        $this->address = $value;
    }
    
    protected function setPhone($value)
    {
        if (!is_array($value))
        {
            throw new InvalidParameterException('Value must be an array of Phone objects.');
        }
        foreach ($value as $val)
        {
            if (false == ($val instanceof Phone))
            {
                throw new InvalidParameterException('Value must be an array of Phone objects.');
            }
        }
        $this->phone = $value;        
    }
    
    protected function setEmail($value)
    {
        if (!is_array($value))
        {
            throw new InvalidParameterException('Value must be an array of HvEmail objects.');
        }
        foreach ($value as $val)
        {
            if (false == ($val instanceof HvEmail))
            {
                throw new InvalidParameterException('Value must be an array of HvEmail objects.');
            }
        }
        $this->email = $value;
    }
    
    public function writeXml($tagName)
    {
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($tagName);
        if (is_array($this->address) && count($this->address) > 0)
        {
            foreach ($this->address as $address)
            {
                $x->writeRaw($address->writeXml('address'));
            }
        }
        if (is_array($this->phone) && count($this->phone) > 0)
        {
            foreach ($this->phone as $phone)
            {
                $x->writeRaw($phone->writeXml('phone'));
            }
        }
        if (is_array($this->email) && count($this->email) > 0)
        {
            foreach ($this->email as $email)
            {
                $x->writeRaw($email->writeXml('email'));
            }
        }
        $x->endElement();
        return $x->flush();
    }
    
}

?>