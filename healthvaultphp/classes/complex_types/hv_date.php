<?php
/**
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/** 
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class HvDate extends ComplexType
{
    
    protected $y;
    protected $m;
    protected $d;
    
    public static function fromXml($xmlObj)
    {
        $obj = new HvDate();
        $obj->parseXml($xmlObj);
        return $obj;
    }
    
    protected function parseXml($xmlObj)
    {
        if (isset($xmlObj->y))
        {
            $this->y = (integer) $xmlObj->y;
        }
        if (isset($xmlObj->m))
        {
            $this->m = (integer) $xmlObj->m;
        }
        if (isset($xmlObj->d))
        {
            $this->d = (integer) $xmlObj->d;
        }
    }
    
    protected function setY($value)
    {
        if (!is_int($value))
        {
            throw new InvalidParameterException('Value must be an integer.');
        }
        if ($value < 1000 || $value > 9999)
        {
            throw new InvalidParameterException('Value must be between 1000 and 9999');
        }
        $this->y = $value;
    }

    protected function setM($value)
    {
        if (!is_int($value))
        {
            throw new InvalidParameterException('Value must be an integer.');
        }
        if ($value < 1 || $value > 12)
        {
            throw new InvalidParameterException('Value must be between 1 and 12');
        }
        $this->m = $value;
    }

    protected function setD($value)
    {
        if (!is_int($value))
        {
            throw new InvalidParameterException('Value must be an integer.');
        }
        if ($value < 1 || $value > 31)
        {
            throw new InvalidParameterException('Value must be between 1 and 31');
        }
        $this->d = $value;
    }
    
    public function writeXml($tagName)
    {
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($tagName);
        if (strlen($this->y) > 0)
        {
            $x->writeElement('y', $this->y);
        }
        if (strlen($this->m) > 0)
        {
            $x->writeElement('m', $this->m);
        }
        if (strlen($this->d) > 0)
        {
            $x->writeElement('d', $this->d);
        }
        $x->endElement();
        return $x->flush();
    }
    
}


?>