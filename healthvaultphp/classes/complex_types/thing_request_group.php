<?php
/** 
 * This file houses the ThingRequestGroup
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 * 
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class ThingRequestGroup extends ComplexType
{

    /**
     * An array of Thing objects.
     *
     * @var array
     */
    protected $things;
    
    /**
     * Client assigned ids of things that need to be returned.
     *
     * @var array
     */
    protected $clientThingIds;
    
    /**
     * An array of ThingFilterSpec objects.  Used for finding things to be returned.
     *
     * @var array
     */
    protected $filter;
    
    /**
     * A ThingFormatSpec object.  Used to format the returned thing.
     *
     * @var ThingFormatSpec
     */
    protected $format;
    
    /**
     * Specifies whether or not we should return only the current version of things that satisfy the filter. 
     * This is true by default. If set to false all the versions of the thing are returned.
     *
     * @var bool
     */
    protected $currentVersionOnly;
    
    /**
     * Name of this request group
     * This is used to distinguish this group from other groups that are specified as part of this same search.
     *
     * @var string
     */
    protected $name;
    
    /**
     * The maximum number of items to be returned for this group.
     *
     * @var int
     */
    protected $max;
    
    /**
     * Maximum number of "full" items to be returned for this group.
     *
     * @var int
     */
    protected $maxFull;
    
    /**
     * Object constructor.
     *
     * @param array  $things an array of Thing objects which should be returned
     * @param string $name   name of request group; this is used to distinguish this group from other groups that are specified as part of this same search
     *
     * @return ThingRequestGroup
     */
    public function __construct(array $things = null, $name = '')
    {
        if($things != null)
        {
            $this->setThings($things);
        }
        $this->setName($name);
        $this->clientThingIds = '';
        $this->filter = null;
        $this->format = null;
        $this->currentVersionOnly = true;
        $this->max = null;
        $this->maxFull = null;
    }

    
    /**
     * Magic method - getter
     *
     * @param string $name the name of the property to fetch
     *
     * @return mixed
     */
     /*
    public function __get($name)
    {
        if (property_exists($this, $name))
        {
            return $this->$name;
        }
        else
        {
            throw new InvalidPropertyException('Property ' . $name . ' does not exist');
        }
    }
    */
    
    /**
     * Magic method - setter
     *
     * @param string $name  the name of the property to set
     * @param mixed  $value the value to set the property to
     *
     * @return void
     */
     /*
    public function __set($name, $value)
    {
        switch ($name)
        {
            case 'things':
                return $this->setThings($value);
            case 'clientThingIds':
                return $this->setClientThingId($value);
            case 'filters':
                return $this->setFilters($value);
            case 'format':
                return $this->setFormat($value);
            case 'currentVersionOnly':
                return $this->setCurrentVersionOnly($value);
            case 'name':
                return $this->setName($value);
            case 'max':
                return $this->setMax($value);
            case 'maxFull':
                return $this->setMaxFull($value);
            default:
                throw new InvalidPropertyException('Property ' . $name . ' does not exist');                
        }
    }
    */

    /**
     * Sets the things property.
     *
     * @param array $things an array of Thing objects
     *
     * @return void
     */
    protected function setThings(array $things)
    {
        foreach($things as $thing)
        {
            if(false == ($thing instanceof Thing))
            {
                throw new InvalidParameterException('Things must be an array of Things');
            }
        }
        $this->things = $things;
    }
    
    /**
     * Sets the client thing id property.
     *
     * @param array $id client assigned ids of things that need to be returned
     *
     * @return void
     */
    protected function setClientThingId(array $ids)
    {
        foreach ($ids as $id)
        {
            if (!is_string($id))
            {
                throw new InvalidParameterException('Ids must be an array of strings');
            }
        }
        $this->clientThingIds = $ids;
    }
    
    /**
     * Sets the filters property.
     *
     * @param array $filters an array of ThingFilterSpec objects
     *
     * @return void
     */
    protected function setFilter(array $filters)
    {
        foreach ($filters as $filter)
        {
            if (false == ($filter instanceof ThingFilterSpec))
            {
                throw new InvalidParameterException('Filters must be an array of ThingFilterSpec objects');
            }
        }
        $this->filter = $filters;
    }
    
    /**
     * Sets the format property.
     *
     * @param ThingFormatSpec $format the format to return the things in
     * 
     * @return void
     */
    protected function setFormat(ThingFormatSpec $format)
    {
        $this->format = $format;
    }
    
    /**
     * Sets the current version only property.
     *
     * @param bool $value whether or not to return the most current version of the thing
     * 
     * @return void
     */
    protected function setCurrentVersionOnly($value)
    {
        if (!is_bool($value))
        {
            throw new InvalidParameterException('Value must be boolean');
        }
        $this->currentVersionOnly($value);
    }
    
    /**
     * Sets the name property for this thing group.
     *
     * @param string $name the name for this thing grouping
     * 
     * @return void
     */
    protected function setName($name)
    {
        if (!is_string($name))
        {
            throw new InvalidParameterException('Name must be a string');
        }
        $this->name = $name;
    }
    
    /**
     * Sets the max property
     *
     * @param int $max the maximum number of records to return
     *
     * @return void
     */
    protected function setMax($max)
    {
        if (is_int($max) && $max >= 0)
        {
            $this->max = $max;
        }
        else
        {
            throw new InvalidParameterException('Max must be a non-negative integer');
        }
    }
    
    /**
     * Sets the max full property
     *
     * @param int $max the maximum number of full records to return
     *
     * @return void
     */
    protected function setMaxFull($max)
    {
        if (is_int($max) && $max >= 0)
        {
            $this->maxFull = $max;
        }
        else
        {
            throw new InvalidParameterException('Max must be a non-negative integer');
        }
    }
    
    /**
     * Returns the thing request group in XML format, which is used in HealthVault requests (namely, GetThings)
     *
     * @param string $elementName the name of the top-most XML element
     *
     * @return string
     */
    public function writeXml($elementName)
    {
        if (!is_string($elementName))
        {
            throw new InvalidParameterException('Element name must be a string');
        }
        
        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->startElement($elementName);
        
        if (is_array($this->things) && count($this->things) > 0)
        {
            foreach ($this->things as $thing)
            {
                $xmlWriter->writeElement('id', $thing->id);
                $xmlWriter->writeRaw($thing->key->writeXml('key'));
            }
        }
        
        if (is_array($this->clientThingIds) && count($this->clientThingIds) > 0)
        {
            foreach ($this->clientThingIds as $clientThingId)
            {
                $xmlWriter->writeElement('client-thing-id', $clientThingId);
            }
        }
        
        if (is_array($this->filter) && count($this->filter) > 0)
        {
            foreach ($this->filter as $filter)
            {
                $xmlWriter->writeRaw($filter->writeXml('filter'));
            }
        }
        
        if ($this->format instanceof ThingFormatSpec)
        {
            $xmlWriter->writeRaw($this->format->writeXml('format'));
        }
        
        $xmlWriter->writeElement('current-version-only', (int) $this->currentVersionOnly);
        
        if (strlen($this->name) > 0)
        {
            $xmlWriter->writeElement('name', $this->name);
        }
        
        if (!is_null($this->max))
        {
            $xmlWriter->writeElement('max', $this->max);
        }

        if (!is_null($this->maxFull))
        {
            $xmlWriter->writeElement('max-full', $this->maxFull);
        }

        $xmlWriter->endElement();
        return $xmlWriter->flush();
    }
    
}

?>