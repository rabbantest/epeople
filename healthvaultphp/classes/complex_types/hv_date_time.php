<?php
/**
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class HvDateTime extends ComplexType
{
    
    protected $date = null;
    protected $time = null;
    protected $tz = null;
    
    public static function fromXml($xmlObj)
    {
        $obj = new HvDateTime();
        $obj->parseXml($xmlObj);
        return $obj;
    }
    
    protected function parseXml($xmlObj)
    {
        if (isset($xmlObj->date))
        {
            $this->date = HvDate::fromXml($xmlObj->date);
        }
        if (isset($xmlObj->time))
        {
            $this->time = HvTime::fromXml($xmlObj->time);
        }
        if (isset($xmlObj->tz))
        {
            $this->tz = CodableValue::fromXml($xmlObj->tz);
        }
    }
    
    public function setDate($value)
    {
        if ($value instanceof HvDate)
        {
            $this->date = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be an HvDate object');
        }
    }
    
    public function setTime($value)
    {
        if ($value instanceof HvTime)
        {
            $this->time = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be an HvTime object');
        }
    }
    
    public function setTz($value)
    {
        if ($value instanceof CodableValue)
        {
            $this->tz = $value;
        }
        else
        {
            throw new InvalidParameterException('Parameter must be a CodableValue object');
        }
    }
    
    public function writeXml($tagName)
    {
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($tagName);
        
        if ($this->date != null)
        {
            $x->writeRaw($this->date->writeXml('date'));
        }
        if ($this->time != null)
        {
            $x->writeRaw($this->time->writeXml('time'));
        }
        if ($this->tz != null)
        {
            $x->writeRaw($this->tz->writeXml('tz'));
        }
        
        $x->endElement();
        return $x->flush();
    }
    
}

?>