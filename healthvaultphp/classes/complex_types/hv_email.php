<?php
/**
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class HvEmail extends ComplexType
{
    
    protected $description;
    protected $isPrimary = false;
    protected $address;
    
    protected function setDescription($value)
    {
        if (!is_string($value))
        {
            throw new InvalidParameterException('Value must be a string.');
        }
        $this->description = $value;
    }
    
    protected function setIsPrimary($value)
    {
        if (!is_bool($value))
        {
            throw new InvalidParameterException('Value must be a boolean.');
        }
        $this->isPrimary = $value;
    }
    
    protected function setAddress($value)
    {
        if (!is_string($value))
        {
            throw new InvalidParameterException('Value must be a string.');
        }
        $this->address = $value;
    }
    
    public function writeXml($tagName)
    {
        $x = new XmlWriter();
        $x->openMemory();
        $x->startElement($tagName);
        if (strlen($this->description) > 0)
        {
            $x->writeElement('description', $this->description);
        }
        if ($this->isPrimary === true)
        {
            $x->writeElement('is-primary', 'true');
        }
        else
        {
            $x->writeElement('is-primary', 'false');
        }
        if (strlen($this->address) > 0)
        {
            $x->writeElement('address', $this->address);
        }
        $x->endElement();
        return $x->flush();
    }
    
}

?>