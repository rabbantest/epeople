<?php
/** 
 * This file houses the ThingSectionSpec
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Dave Kiger
 */

/**
 * The ThingSectionSpec object
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger
 */
class ThingSectionSpec extends EnumeratedType
{

    /**
     * core
     */
    const CORE = 'core';
    
    /**
     * audits
     */
    const AUDITS = 'audits';

    /**
     * other data
     */
    const OTHER_DATA = 'otherdata';

    /**
     * digital signatures
     */
    const DIGITAL_SIGNATURES = 'digitalsignatures';

    /**
     * tags
     */
    const TAGS = 'tags';

    /**
     * Validates that value is allowed type and sets the property.
     *
     * @param string $value the value of this type
     *
     * @return void
     */
    public function setValue($value)
    {
        switch ($value)
        {
            case ThingSectionSpec::CORE:
            case ThingSectionSpec::AUDITS:
            case ThingSectionSpec::OTHER_DATA:
            case ThingSectionSpec::DIGITAL_SIGNATURES:
            case ThingSectionSpec::TAGS:
                $this->value = $value;
                break;
            default:
                throw new InvalidParameterException('Thing section spec is not a valid enumeration.');
        }
    }
    
}

?>