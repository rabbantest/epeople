<?php
/**
 * A Culture representation (like en-US)
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @author     Jim Wordelman
 */

/**
 *
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Jim Wordelman
 */ 
class Culture extends ComplexType
{
    
    /**
     * The language for the culture (iso639-1)
     *
     * @var string 
     *
     */
    protected $language = null;
    
    /**
     * The country or region of the culture (iso3166)
     *
     * @var string 
     *
     */
    protected $country = null;
    
    /**
     * An array of the valid values for language (per ISO639-1)
     *
     * @var array 
     *
     */
    protected static $validISO639 = 
        array(
            "aa",
            "ab",
            "af",
            "am",
            "ar",
            "as",
            "ay",
            "az",
            "ba",
            "be",
            "bg",
            "bh",
            "bi",
            "bn",
            "bo",
            "br",
            "ca",
            "co",
            "cs",
            "cy",
            "da",
            "de",
            "dz",
            "el",
            "en",
            "eo",
            "es",
            "et",
            "eu",
            "fa",
            "fi",
            "fj",
            "fo",
            "fr",
            "fy",
            "ga",
            "gd",
            "gl",
            "gn",
            "gu",
            "ha",
            "hi",
            "hr",
            "hu",
            "hy",
            "ia",
            "ie",
            "ik",
            "in",
            "is",
            "it",
            "iw",
            "ja",
            "ji",
            "jw",
            "ka",
            "kk",
            "kl",
            "km",
            "kn",
            "ko",
            "ks",
            "ku",
            "ky",
            "la",
            "ln",
            "lo",
            "lt",
            "lv",
            "mg",
            "mi",
            "mk",
            "ml",
            "mn",
            "mo",
            "mr",
            "ms",
            "mt",
            "my",
            "na",
            "ne",
            "nl",
            "no",
            "oc",
            "om",
            "or",
            "pa",
            "pl",
            "ps",
            "pt",
            "qu",
            "rm",
            "rn",
            "ro",
            "ru",
            "rw",
            "sa",
            "sd",
            "sg",
            "sh",
            "si",
            "sk",
            "sl",
            "sm",
            "sn",
            "so",
            "sq",
            "sr",
            "ss",
            "st",
            "su",
            "sv",
            "sw",
            "ta",
            "te",
            "tg",
            "th",
            "ti",
            "tk",
            "tl",
            "tn",
            "to",
            "tr",
            "ts",
            "tt",
            "tw",
            "uk",
            "ur",
            "uz",
            "vi",
            "vo",
            "wo",
            "xh",
            "yo",
            "zh",
            "zu"
        );
        
        /**
         * An array of the valid values for country (per ISO 3166)
         *
         * @var array 
         *
         */
        protected static $validISO3166 =
        array(
            "AF",
            "AX",
            "AL",
            "DZ",
            "AS",
            "AD",
            "AO",
            "AI",
            "AQ",
            "AG",
            "AR",
            "AM",
            "AW",
            "AU",
            "AT",
            "AZ",
            "BS",
            "BH",
            "BD",
            "BB",
            "BY",
            "BE",
            "BZ",
            "BJ",
            "BM",
            "BT",
            "BO",
            "BA",
            "BW",
            "BV",
            "BR",
            "IO",
            "BN",
            "BG",
            "BF",
            "BI",
            "KH",
            "CM",
            "CA",
            "CV",
            "KY",
            "CF",
            "TD",
            "CL",
            "CN",
            "CX",
            "CC",
            "CO",
            "KM",
            "CG",
            "CD",
            "CK",
            "CR",
            "CI",
            "HR",
            "CU",
            "CY",
            "CZ",
            "DK",
            "DJ",
            "DM",
            "DO",
            "EC",
            "EG",
            "SV",
            "GQ",
            "ER",
            "EE",
            "ET",
            "FK",
            "FO",
            "FJ",
            "FI",
            "FR",
            "GF",
            "PF",
            "TF",
            "GA",
            "GM",
            "GE",
            "DE",
            "GH",
            "GI",
            "GR",
            "GL",
            "GD",
            "GP",
            "GU",
            "GT",
            "GN",
            "GW",
            "GY",
            "HT",
            "HM",
            "VA",
            "HN",
            "HK",
            "HU",
            "IS",
            "IN",
            "ID",
            "IR",
            "IQ",
            "IE",
            "IL",
            "IT",
            "JM",
            "JP",
            "JO",
            "KZ",
            "KE",
            "KI",
            "KP",
            "KR",
            "KW",
            "KG",
            "LA",
            "LV",
            "LB",
            "LS",
            "LR",
            "LY",
            "LI",
            "LT",
            "LU",
            "MO",
            "MK",
            "MG",
            "MW",
            "MY",
            "MV",
            "ML",
            "MT",
            "MH",
            "MQ",
            "MR",
            "MU",
            "YT",
            "MX",
            "FM",
            "MD",
            "MC",
            "MN",
            "MS",
            "MA",
            "MZ",
            "MM",
            "NA",
            "NR",
            "NP",
            "NL",
            "AN",
            "NC",
            "NZ",
            "NI",
            "NE",
            "NG",
            "NU",
            "NF",
            "MP",
            "NO",
            "OM",
            "PK",
            "PW",
            "PS",
            "PA",
            "PG",
            "PY",
            "PE",
            "PH",
            "PN",
            "PL",
            "PT",
            "PR",
            "QA",
            "RE",
            "RO",
            "RU",
            "RW",
            "SH",
            "KN",
            "LC",
            "PM",
            "VC",
            "WS",
            "SM",
            "ST",
            "SA",
            "SN",
            "CS",
            "SC",
            "SL",
            "SG",
            "SK",
            "SI",
            "SB",
            "SO",
            "ZA",
            "GS",
            "ES",
            "LK",
            "SD",
            "SR",
            "SJ",
            "SZ",
            "SE",
            "CH",
            "SY",
            "TW",
            "TJ",
            "TZ",
            "TH",
            "TL",
            "TG",
            "TK",
            "TO",
            "TT",
            "TN",
            "TR",
            "TM",
            "TC",
            "TV",
            "UG",
            "UA",
            "AE",
            "GB",
            "US",
            "UM",
            "UY",
            "UZ",
            "VU",
            "VE",
            "VN",
            "VG",
            "VI",
            "WF",
            "EH",
            "YE",
            "ZM",
            "ZW"
        );
        
        
    /**
     * Constructor that allows setting of any of language or country
     *
     * @param string $language The language of the culture
     * @param string $country  The country of the culture
     * 
     * @uses Culture::setLanguage()
     * @uses Culture::setCountry()
     * 
     * @return void 
     *
     */
    public function __construct($language=null, $country=null)
    {
        if($language != null)
        {
            $this->setLanguage($language);
        }
        if($country != null)
        {
            $this->setCountry($country);
        }
    }
        
    /**
     * Sets the language of the culture
     *
     * @param string $language The new language for the culture
     * @return void 
     *
     */
    public function setLanguage($language)
    {
        if(!in_array(strtolower($language), self::$validISO639))
        {
            throw new InvalidParameterException("Languages must be in the ISO639-1 standard");
        }
        $this->language = strtolower($language);
    }
    
    /**
     * Sets the country for the culture
     *
     * @param string $country The new country for the culture
     * @return void 
     *
     */
    public function setCountry($country)
    {
        if(!in_array(strtoupper($country), self::$validISO3166))
        {
            throw new InvalidParameterException("Countries must be in the ISO3166 standard");
        }
        $this->country = strtoupper($country);
    }
    
    /**
     * Turns the culture into a string like 'en-US' or 'en' or 'US' depending on what is set
     *
     * @return string The culture as a string
     *
     */
    public function __toString()
    {
        $toReturn = '';
        if($this->language != null)
        {
            $toReturn = $this->language;
            if($this->country != null)
            {
                $toReturn .= '-' . $this->country;
            }
        }
        else
        {
            $toReturn = (string)$this->country;
        }
        return $toReturn;
    }
}
?>