<?php
/** 
 * This file houses the AccessAvenue class.
 * 
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger <no-email@please.net>
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The AccessAvenue object is an enumerated type which describes the method through which something is accessed or changed (either online or offline).
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger <no-email@please.net>
 */
class AccessAvenue extends EnumeratedType
{

    /**
     * Online access avenue flag.
     */
    const ONLINE = 'Online';
    
    /**
     * Offline access avenue flag.
     */
    const OFFLINE = 'Offline';

    /**
     * Validates that value is allowed type and sets the property.
     *
     * @throws InvalidParameterException if $value is not an allowed type.
     *
     * @param string $value the value of this type
     *
     * @return void
     */
    public function setValue($value)
    {
        switch ($value)
        {
            case AccessAvenue::ONLINE:
            case AccessAvenue::OFFLINE:
                $this->value = $value;
                break;
            
            default:
                throw new InvalidParameterException('Value is not a valid enumeration.');
                break;
        }
    }

}

?>