<?php
/**
 * This file houses the RequestBuilder class definition.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Requests
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The RequestBuilder object is used by the HealthVaultProxy class to build the XML request payload which will be sent to the HealthVault system.
 *
 * @category  PHP-Library
 * @package   HealthVault
 * @author    Dave Kiger <noemail@noneto.us>
 * @copyright 2008 TelaDoc, Inc.
 * @license   https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link      https://sourceforge.net/projects/healthvaultphp
 */
class RequestBuilder
{

    /**
     * An object which implements the IHealthVaultRequest interface.
     *
     * @var IHealthVaultRequest
     */
    private $requestObj;

    /**
     * Object constructor.
     *
     * @param IHealthVaultRequest $obj an object which will be used to build the XML payload
     *
     * @return RequestBuilder
     */
    public function __construct(IHealthVaultRequest $obj)
    {
        $this->requestObj = $obj;
    }

    /**
     * Builds the XML payload and returns it.
     *
     * @uses IHealthVaultRequest::getHeader()
     * @uses IHealthVaultRequest::getBody()
     * @uses IHealthVaultRequest::getFooter()
     *
     * @return string
     */
    public function getXml()
    {
        $xml  = '<?xml version="1.0" encoding="utf-8"?>';
        $xml .= '<wc-request:request xmlns:wc-request="urn:com.microsoft.wc.request">';
        $xml .= $this->requestObj->getHeader();
        $xml .= $this->requestObj->getBody();
        $xml .= $this->requestObj->getFooter();
        $xml .= '</wc-request:request>';
        //echo str_replace('>', ">\n", $xml); exit;
        return $xml;
    }
    
    public function validateMe()
    {
        // first validate the request object, if possible
        if (method_exists($this->requestObj, 'validateMe'))
        {
            $result = $this->requestObj->validateMe();
            if ($result !== true)
            {
                return $result;
            }
        }
        $xsd = HV_BASE_PATH . '/xsds/request.xsd';
        $xml = $this->getXml();
        libxml_use_internal_errors(true);
        $xdoc = new DomDocument();
        $xdoc->loadXML($xml);
        $response = true;
        if (!$xdoc->schemaValidate($xsd))
        {
            $errors = libxml_get_errors();
            $response = 'Validation Errors:<br><ul>';
            foreach ($errors as $error)
            {
                $response .= '<li>' . $error->line . ':' . $error->column . ' -- ' . $error->message . "</li>";
            }
            $response .= '</ul>';
        }
        return $response;
    }

}

?>