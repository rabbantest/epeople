<?php
/**
 * This file houses the HealthVaultProxy class definition.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Proxy
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * The HealthVaultProxy class creates a connection to HealthVault, sends a request, and returns a response object.
 *
 * @category  PHP-Library
 * @package   HealthVault
 * @author    Dave Kiger <noemail@noneto.us>
 * @copyright 2008 TelaDoc, Inc.
 * @license   https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link      https://sourceforge.net/projects/healthvaultphp
 */
class HealthVaultProxy
{
    
    /**
     * A HealthVaultConnection object.
     *
     * @var HealthVaultConnection
     */
    private $hvConnection;
    
    /**
     * Object constructor.
     *
     * @param HealthVaultConnection $obj a HealthVaultConnection object
     *
     * @return HealthVaultProxy
     */
    public function __construct(HealthVaultConnection $obj)
    {
        $this->hvConnection = $obj;
    }
    
    public function schemaValidate(IHealthVaultRequest $obj)
    {
        $requestBuilder = new RequestBuilder($obj);
        return $requestBuilder->validateMe();
    }
    
    /**
     * Sends the request to HealthVault and returns a response object
     *
     * @uses RequestBuilder::getXml()
     * @uses HealthVaultConnection::sendRequestPayload()
     * @uses ResponseFactory::getResponseObject()
     *
     * @param IHealthVaultRequest $obj an object which implements the IHealthVaultRequest interface
     *
     * @return IHealthVaultResponse
     */
    public function call(IHealthVaultRequest $obj)
    {
        $requestBuilder = new RequestBuilder($obj);
        $payload = $requestBuilder->getXml();
        $rawResponse = $this->hvConnection->sendRequestPayload($payload);
        
        // get an array equivalent of the XML
        $xmlSimple = getArrayFromResponseXML($rawResponse);
        
        // check for errors
        if (isset($xmlSimple['status']['error']['message']))
        {
            if (stripos($xmlSimple['status']['error']['message'], 'Access is denied'))
            {
                throw new SessionExpiredException('The online session has expired.  Please login to your HealthVault account to continue.');
            }
            else
            {
                throw new XmlRequestErrorException('HealthVault request error: ' . $xmlSimple['status']['error']['message']);
            }
        }
        $responseFactory = new ResponseFactory($rawResponse, $obj->getMethodName());
        return $responseFactory->getResponseObject();
    }
    
}

?>
