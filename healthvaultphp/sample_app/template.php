<?php
    echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>HealthVault PHP Sample Application</title>
        <link rel="stylesheet" type="text/css" href="main.css" />
    </head>
    <body>
        <div class="main">
            <table cellpadding="0" cellspacing="0" class="main">
                <tr>
                    <td class="header">HealthVault PHP Sample Application</td>
                </tr>
                <tr>
                    <td class="body">
                        <?php echo $body; ?>
                    </td>
                </tr>
                <tr>
                    <td class="footer">&nbsp;</td>
                </tr>
            </table>
            <div class="footer">The <a href="https://sourceforge.net/projects/healthvaultphp">HealthVault PHP Library</a> is not supported or sponsored by Microsoft.<br />Licensed under the terms of the <a href="http://www.microsoft.com/opensource/licenses.mspx#Ms-PL">Microsoft Public License.</a></div>
        </div>
    </body>
</html>