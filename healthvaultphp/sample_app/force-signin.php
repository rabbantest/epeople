<?php
/**
 * This is part of a test application that allows you to sign-in and manage
 * things.
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger <no-email@please.net>
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

// make sure user is signed in
if (!isset($_SESSION['wctoken']))
{
    header('Location: index.php');
    exit;
}

?>