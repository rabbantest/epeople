<?php
/**
 * This is part of a test application that allows you to sign-in and manage
 * things.
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger <no-email@please.net>
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

// initialize
require('init.php');

// require sign in
require('force-signin.php');

$body .= '<a href="personal-contact-info.php">Personal Contact Information</a><br /><br />';
$body .= '<a href="personal-demographic-info.php">Personal Demographic Information</a><br /><br />';
$body .= '<a href="logout.php">Logout</a>';

require('template.php');

?>