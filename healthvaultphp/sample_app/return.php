<?php
/**
 * This file is a test of the HealthVault authentication process.  It
 * demonstrates how to use the returned wctoken to obtain information
 * about the user which can be used later for all other HV methods.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Tests
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 * @see        index.php
 */

/**
 * Include error handling for debug purposes.
 */
require_once('../globals/error-handler.php');

/**
 * Include the PHP-HealthVault API library.
 */
require_once('../health_vault_library.php');

// get wctoken
//$wcToken = $_GET['wctoken'];

$personId = new Guid('6ea7117d-9c56-46ea-a5c1-f2cc3a026c54');
$recordId = new Guid('d686fd7e-9576-484e-834f-365e89ca9d33');
$ccrFile = '/home/dkiger/www/public_html/hvphp/trunk/sample_app/samples/ccr-sample.xml';
$ccrXml = file_get_contents($ccrFile);
$ccrXml = str_replace("\n", "", $ccrXml);
$ccrXml = str_replace("\t", " ", $ccrXml);
$ccrXml = preg_replace("/( ){2,}/"," ", $ccrXml);
$ccrXml = str_replace("> <", "><", $ccrXml);

$ccrThing = new ContinuityOfCareRecord();
$ccrThing->dataXml = array ( new DataXml($ccrXml) );

$things = array ( $ccrThing );

$proxy = new HealthVaultProxy( HealthVaultConnection::getInstance() );

/*
$results = $proxy->schemaValidate( new PutThingsRequest( $things, $recordId, $personId ) );
if ($results !== true)
{
    echo $results;
}
else
{
    echo 'Schema OK.';
}
exit;
*/

$obj = $proxy->call( new PutThingsRequest( $things, $recordId, $personId ) );
var_dump($obj);
exit;


$trg = new ThingRequestGroup();
$trg->format = new ThingFormatSpec( array ( new ThingSectionSpec(ThingSectionSpec::CORE) ) );
$trg->filter = array ( new ThingFilterSpec( array ( new Guid(PersonalDemographicInformation::ID) ) ) );

$proxy = new HealthVaultProxy( HealthVaultConnection::getInstance() );
$obj = $proxy->call( new GetThingsRequest( array ( $trg ), $recordId, $personId ) );
var_dump($obj); exit;
//$personInfoObj = $proxy->call(new GetPersonInfoRequest($wcToken));

//var_dump($personInfoObj); exit;

//echo "Hello, {$personInfoObj->name}. Welcome to a HealthVault Application";

?>