<?php
/**
 * This is part of a test application that allows you to sign-in and manage
 * things.
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger <no-email@please.net>
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * Include error handling for debug purposes.
 */
require_once('../globals/error-handler.php');

/**
 * Include the PHP-HealthVault API library.
 */
require_once('../health_vault_library.php');

// start sessions
session_start();

// displayed in template
$body = '';


?>