<?php
/**
 * This is part of a test application that allows you to sign-in and manage
 * things.
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger <no-email@please.net>
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

// initialize
require('init.php');

// grab the wctoken
if (isset($_GET['wctoken']))
{
    $_SESSION['wctoken'] = $_GET['wctoken'];
}

// logged into healthvault?
if (!isset($_SESSION['wctoken']))
{
    // determine current URL
    if (strcasecmp($_SERVER['HTTPS'], 'on') == 0)
    {
        $url = 'https://';
    }
    else
    {
        $url = 'http://';
    }
    $url .= $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    
    // display authentication link to shell
    $hvConn = HealthVaultConnection::getInstance();
    $shellUrl = htmlentities($hvConn->getShellUrl($url));
    $manageUrl = $hvConn->getManageUrl();
    $body .= 'You need to be signed in to HealthVault in order to use this application.<br /><br />';
    $body .= '<a href="' . $shellUrl . '">Click here to login to your HealthVault account.</a><br /><br />';
    $body .= '<a href="'. $manageUrl .'">Click here to login to the HealthVault management center.</a>';
}
else
{
    $body .= 'You are currently signed in.<br /><br />';
    $body .= '<a href="things.php">Manage Things</a><br /><br />';
    $body .= '<a href="logout.php">Logout</a><br />';
}

require('template.php');

?>