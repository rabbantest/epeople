<?php
/**
 * This is part of a test application that allows you to sign-in and manage
 * things.
 *
 * @package    HealthVault-PHP-Lib
 * @subpackage Complex-Types
 * @author     Dave Kiger <no-email@please.net>
 * @license    http://www.microsoft.com/opensource/licenses.mspx#Ms-PL  Microsoft Public License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

// initialize
require('init.php');

// require sign in
require('force-signin.php');

// we need a connection to HealthVault; the HealthVaultProxy class does all the heavy lifting of making requests
$proxy = new HealthVaultProxy( HealthVaultConnection::getInstance() );

// first we need to get a person id and record id from HV; this is done using the GetPersonInfoRequest
try
{
    $person_info = $proxy->call( new GetPersonInfoRequest($_SESSION['wctoken'] ) );
}
catch (SessionExpiredException $e)
{
    $body .= $e->getMessage();
    require('template.php');
    exit;
}

// are we saving data?
if (isset($_POST['submit']) && strcasecmp($_POST['submit'], 'save contact information') == 0)
{
    // create thing object that we want to send back
    $address = new Address();
    $address->isPrimary = true;
    $address->street = $_POST['street'];
    $address->city = $_POST['city'];
    $address->state = $_POST['state'];
    $address->postcode = $_POST['postcode'];
    $address->country = $_POST['country'];
    
    $phone = new Phone();
    $phone->isPrimary = true;
    $phone->number = $_POST['phone'];
    
    $email = new HvEmail();
    $email->isPrimary = true;
    $email->address = $_POST['email'];
    
    $contact = new Contact();
    $contact->address = array ( $address );
    $contact->phone = array ( $phone );
    $contact->email = array ( $email );

    $thingId = null;
    if (isset($_POST['thingId']) && !empty($_POST['thingId']))
    {
        $thingId = new Guid($_POST['thingId']);
    }
    
    $version_stamp = null;
    if (isset($_POST['versionStamp']) && !empty($_POST['versionStamp']))
    {
        $version_stamp = new Guid($_POST['versionStamp']);
    }
    
    if ($thingId != null && $version_stamp != null)
    {
        $personal_contact_info = new PersonalContactInformation($thingId);
        $personal_contact_info->versionStamp = $version_stamp;
        $personal_contact_info->contact = $contact;
    }
    else
    {
        $personal_contact_info = new PersonalContactInformation();
        $personal_contact_info->contact = $contact;
    }
    
    try
    {
        $response = $proxy->call( new PutThingsRequest( array ( $personal_contact_info ), $person_info->selectedRecordId, $person_info->personId, $_SESSION['wctoken'] ) );
    }
    catch (SessionExpiredException $e)
    {
        $body .= $e->getMessage();
        require('template.php');
        exit;
    }

    $body .= 'Your Personal Contact Information has been saved.<br /><br /><a href="things.php">Manage Things</a>';
}
else if (isset($_POST['submit']) && strcasecmp($_POST['submit'], 'delete this information') == 0)
{
    $thingId = null;
    if (isset($_POST['thingId']) && !empty($_POST['thingId']))
    {
        $thingId = new Guid($_POST['thingId']);
    }
    
    $version_stamp = null;
    if (isset($_POST['versionStamp']) && !empty($_POST['versionStamp']))
    {
        $version_stamp = new Guid($_POST['versionStamp']);
    }
    
    $thingKey = new ThingKey($thingId, $version_stamp);
    try
    {
        $response = $proxy->call( new RemoveThingsRequest ( array ( $thingKey ), $person_info->selectedRecordId, $person_info->personId, $_SESSION['wctoken'] ) );
    }
    catch (SessionExpiredException $e)
    {
        $body .= $e->getMessage();
        require('template.php');
        exit;
    }

    $body .= 'Your personal contact information has been removed from HealthVault.<br /><br /><a href="things.php">Manage Things</a>';
}
else
{
    /*
    In order to use the GetThings method, we need a ThingRequestGroup.  This tells HV what things we
    want and in what format.  Once we create this object, we'll need to tell which ThingFormatSpec to use
    and with ThingFilterSpec to use.  
    */
    $trg = new ThingRequestGroup();

    // this tells HV that we want it formatted in XML; this will always be the case when using this library
    $trg->format = new ThingFormatSpec( array ( new ThingSectionSpec(ThingSectionSpec::CORE) ) );

    // this tells HV which things we want; specifically, we tell HV we want the things by thing type ID
    $trg->filter = array ( new ThingFilterSpec( array ( new Guid(PersonalContactInformation::ID ) ) ) );

    // do a GetThings request
    $getThingsResponseObj = $proxy->call( new GetThingsRequest( array ( $trg ), $person_info->selectedRecordId, $person_info->personId, $_SESSION['wctoken'] ) );

    // get contact object
    if (isset($getThingsResponseObj->thingResponseGroups[0]->things[0]->contact))
    {
        $contact = $getThingsResponseObj->thingResponseGroups[0]->things[0]->contact;
    }
    else
    {
        $contact = new Contact();
    }

    // we can have multiple addresses -- loop through and find primary
    $primary_address = new Address(); // empty if not returned
    if (is_array($contact->address))
    {
        foreach ($contact->address as $address)
        {
            if ($address->isPrimary)
            {
                $primary_address = $address;
            }
        }
    }

    // we can have multiple phones -- loop through and find primary
    $primary_phone = new Phone();
    if (is_array($contact->phone))
    {
        foreach ($contact->phone as $phone)
        {
            if ($phone->isPrimary)
            {
                $primary_phone = $phone;
            }
        }
    }

    // we can have multiple email addresses -- loop through and find primary
    $primary_email = new HvEmail();
    if (is_array($contact->email))
    {
        foreach ($contact->email as $email)
        {
            if ($email->isPrimary)
            {
                $primary_email = $email;
            }
        }
    }

    // create a form to display this information
    $body .= '<h2>Personal Contact Information<br />(Primary Only)</h2>';
    $body .= '<form action="personal-contact-info.php" method="post">';
    if (isset($getThingsResponseObj->thingResponseGroups[0]->things[0]))
    {
        $body .= '<input type="hidden" name="thingId" value="' . $getThingsResponseObj->thingResponseGroups[0]->things[0]->thingId->__toString() . '" />'; // we need this to do an update
        $body .= '<input type="hidden" name="versionStamp" value="' . $getThingsResponseObj->thingResponseGroups[0]->things[0]->versionStamp->__toString() . '" />'; // we need this to do an update
    }
    $body .= '<table style="margin: auto;">';
    $body .= '<tr>';
    $body .= '<td class="label">Street Address, Line 1:</td>';
    $body .= '<td><input type="text" name="street[0]" size="30" value="'; if (isset($primary_address->street[0])) { $body .= $primary_address->street[0]; } $body .= '" /></td>';
    $body .= '</tr>';
    $body .= '<tr>';
    $body .= '<td class="label">Street Address, Line 2:</td>';
    $body .= '<td><input type="text" name="street[1]" size="30" value="'; if (isset($primary_address->street[1])) { $body .= $primary_address->street[1]; } $body .= '" /></td>';
    $body .= '</tr>';
    $body .= '<tr>';
    $body .= '<td class="label">Town/City:</td>';
    $body .= '<td><input type="text" name="city" size="30" value="'; if ($primary_address->city) { $body .= $primary_address->city; } $body .= '" /></td>';
    $body .= '</tr>';
    $body .= '<tr>';
    $body .= '<td class="label">State/Province:</td>';
    $body .= '<td><input type="text" name="state" size="30" value="'; if ($primary_address->state) { $body .= $primary_address->state; } $body .= '" /></td>';
    $body .= '</tr>';
    $body .= '<tr>';
    $body .= '<td class="label">Postal Code:</td>';
    $body .= '<td><input type="text" name="postcode" size="30" value="'; if ($primary_address->postcode) { $body .= $primary_address->postcode; } $body .= '" /></td>';
    $body .= '</tr>';
    $body .= '<tr>';
    $body .= '<td class="label">Country:</td>';
    $body .= '<td><input type="text" name="country" size="30" value="'; if ($primary_address->country) { $body .= $primary_address->country; } $body .= '" /></td>';
    $body .= '</tr>';
    $body .= '<tr>';
    $body .= '<td class="label">Phone Number:</td>';
    $body .= '<td><input type="text" name="phone" size="30" value="'; if ($primary_phone->number) { $body .= $primary_phone->number; } $body .= '" /></td>';
    $body .= '</tr>';
    $body .= '<tr>';
    $body .= '<td class="label">Email Address:</td>';
    $body .= '<td><input type="text" name="email" size="30" value="'; if ($primary_email->address) { $body .= $primary_email->address; } $body .= '" /></td>';
    $body .= '</tr>';
    $body .= '<tr><td colspan="2" style="padding: 12px;"><input type="submit" name="submit" value="Save Contact Information" /></form></td></tr>';
    if (isset($getThingsResponseObj->thingResponseGroups[0]->things[0]))
    {
        $body .= '<tr><td colspan="2" style="padding: 12px;"><form action="personal-contact-info.php" method="post">';
        $body .= '<input type="hidden" name="thingId" value="' . $getThingsResponseObj->thingResponseGroups[0]->things[0]->thingId->__toString() . '" />'; // we need this to do a delete
        $body .= '<input type="hidden" name="versionStamp" value="' . $getThingsResponseObj->thingResponseGroups[0]->things[0]->versionStamp->__toString() . '" />'; // we need this to do a delete
        $body .= '<input type="submit" name="submit" value="Delete This Information" /></form></td></tr>';
    }
    $body .= '</table>';
    $body .= '<a href="things.php">Return to Things</a>';
}

// include template
require('template.php');

?>