<?php
/**
 * This file houses global functions used library-wide.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Globals
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

 /**
  * PHP5 magic function to automatically include class/interface files on instantiation.
  * Do not call this function directly.
  *
  * @uses PATH_SMARTY
  * @uses PATH_MODULES
  *
  * @param string $className the name of the class which is being instantiated
  *
  * @return void
  */
 function healthvault_autoload($className)
 {
     // where do we store the classes?
     $classPath = HV_BASE_PATH . '/classes';

     // Split up the string into an array according to the uppercase characters
     $array = preg_split('/([A-Z][^A-Z]*)/', $className, (-1), PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
     $array = array_map('strtolower', $array);

     // create our filename
     $filename = '';
     foreach ($array as $part)
     {
         $filename .= $part . '_';
     }
     $filename = substr($filename, 0, strlen($filename) - 1);
     $filename .= '.php';

     // first look in the base classes directory
     if (file_exists($classPath . '/' . $filename))
     {
         require_once($classPath . '/' . $filename);
     }
     else
     {
         // need to look a little deeper in the class tree
         $dir = dir($classPath);
         while (false != ($file = $dir->read()))
         {
             if ($file != '..' && $file != '.' && is_dir($classPath . '/' . $file))
             {
                 if (file_exists($classPath . '/' . $file . '/' . $filename))
                 {
                     require_once($classPath . '/' . $file . '/' . $filename);
                 }
             }
         }
     }

     // double check that the class actually exists now
     if (false === class_exists($className, false))
     {
         if (false === interface_exists($className, false))
         {
             trigger_error('Class or interface "' . $className . '" does not exist.', E_USER_ERROR);
         }
     }
     return;
 }

/**
 * This function converts the object returned from the simplexml PHP functions to a usable PHP array, preserving the overall XML structure and order.
 * Note: this is a recursive function.
 *
 * @param SimpleXMLElement $obj  a simplexml object
 * @param array  &$arr passed by reference - the array to use to store the converted structure
 *
 * @return void
 */
function convertXmlObjToArr(SimpleXMLElement $obj, array &$arr)
{
    $children = $obj->children();
    foreach ($children as $elementName => $node)
    {
        $nextIdx = count($arr);
        $arr[$nextIdx] = array();
        $arr[$nextIdx]['@name'] = strtolower( (string) $elementName);
        $arr[$nextIdx]['@attributes'] = array();
        $attributes = $node->attributes();
        foreach ($attributes as $attributeName => $attributeValue)
        {
            $attribName = strtolower(trim( (string) $attributeName));
            $attribVal = trim( (string) $attributeValue);
            $arr[$nextIdx]['@attributes'][$attribName] = $attribVal;
        }
        $text = (string) $node;
        $text = trim($text);
        if (strlen($text) > 0)
        {
            $arr[$nextIdx]['@text'] = $text;
        }
        $arr[$nextIdx]['@children'] = array();
        convertXmlObjToArr($node, $arr[$nextIdx]['@children']);
    }
    return;
}

/**
 * Converts an array created using convertXmlObjToArr() and creates a simpler array without attributes and other features.
 *
 * @param array $obj  the array created using convertXmlObjToArr() function
 * @param array &$arr an empty array which will hold the new simpler array
 *
 * @return void
 */
function convertXmlArrToSimpleArr(array $obj, array &$arr)
{
    foreach ($obj as $item)
    {
		if(!is_array($item))
		{
			throw new InvalidParameterException('Input array must be an array of arrays');
		}
        if (isset($item['@children']) && count($item['@children']) > 0)
        {
            $arr[$item['@name']] = array();
            convertXmlArrToSimpleArr($item['@children'], $arr[$item['@name']]);
        }
        if (isset($item['@text']) && isset($item['@name']))
        {
            $arr[$item['@name']] = $item['@text'];
        }
    }
    return;
}

/**
 * Converts an xml string into a simple array
 *
 * @param string $rawResponse The XML to convert
 * @return array The XML as an array
 *
 */
function getArrayFromResponseXML($rawResponse)
{
	if(!is_string($rawResponse))
	{
		throw new InvalidParameterException('rawResponse must be a string');
	}
	$xmlObj = @simplexml_load_string($rawResponse);
	if($xmlObj === false)
	{
		throw new InvalidParameterException('rawResponse must be well formed XML');
	}
	$xmlArr = array();
	$xmlSimple = array();
	convertXmlObjToArr($xmlObj, $xmlArr);
	convertXmlArrToSimpleArr($xmlArr, $xmlSimple);
	return $xmlSimple;
}

/**
 * Sort of a hackish function.  HealthVault uses xml elements with dashes in the names, which when converted to 
 * SimpleXML objects, create unusable properties because the property names have dashes (which PHP does not support).
 * End result is this function -- convert the object into a simple array and use that.
 *
 * @param object $xmlObj a SimpleXMLElement object
 *
 * @return array
 */
function xmlObjToArr(&$xmlObj)
{
    $arr = array();
    $xmlArr = array();
    convertXmlObjToArr($xmlObj, $xmlArr);
    convertXmlArrToSimpleArr($xmlArr, $arr);
    return $arr;
}

if(!defined('CRLF'))
{
	define('CRLF', "\r\n");
}

?>
