<?php
/**
 * This file houses error and exception handling functions.  It is only included for testing/debugging purposes.
 *
 * PHP version 5
 *
 * @category   PHP-Library
 * @package    HealthVault
 * @subpackage Globals
 * @author     Dave Kiger <noemail@noneto.us>
 * @copyright  2008 TelaDoc, Inc.
 * @license    https://it.teladoc.com/dkiger/hvphplicense.php BSD License
 * @link       https://sourceforge.net/projects/healthvaultphp
 */

/**
 * If set to true, errors will be handled using this error handler.  Otherwise, the standard alert system will be used.
 * NEVER SET THIS TO TRUE ON A PRODUCTION SITE!!!!!!!
 */
define('ERR_FORCE_OVERRIDE', false);

/**
 * Whether or not to keep an error log file.
 * NEVER SET THIS TO TRUE ON A PRODUCTION SITE!!!!!!!
 */
define('ERR_KEEP_LOG', false);

/**
 * The filename of the system error log (plain text).  Do not include a path.
 */
define('ERR_SYS_LOG', 'errors.txt');

/**
 * The filename of the HTML error log file.  Do not include a path.
 */
define('ERR_HTML_LOG', 'errors.html');

/**
 * Whether or not to send the error via email to the ERR_EMAIL_ADDRESS.
 */
define('ERR_SEND_EMAIL', false);

/**
 * If errors are sent via email, this is the address to send them to.
 */
define('ERR_EMAIL_ADDRESS', '');

/**
 * The full /path/to/directory of where the error log files should be saved.
 */
define('PATH_DATA', '');

/**
 * This function can be used to override the built-in PHP error handler.  This function will be called when the trigger_error() function is called or when an error occurs.
 * This function should not be called directly.
 *
 * @uses ERR_HTML_LOG
 * @uses ERR_KEEP_LOG
 * @uses ERR_SEND_EMAIL
 * @uses ERR_EMAIL_ADDRESS
 * @uses PATH_DATA
 *
 * @param int    $num     error number
 * @param string $msg     error message
 * @param string $file    file in which error occured
 * @param int    $line    line on which error occured
 * @param array  $context variable dump at time of error
 *
 * @return void
 */
function errorHandler($num, $msg, $file, $line, $context)
{
    // ignoring some problems
    if (stripos($msg, 'ftp_login') === false
        && stripos($msg, 'ftp_get') === false
        && stripos($msg, 'chmod') === false
        && stripos($msg, 'chown') === false
        && (stripos($file, 'smarty') === false && $num != E_STRICT))
    {
        $errorTypes = array(
                       E_ERROR           => 'Fatal Error',
                       E_WARNING         => 'Warning',
                       E_PARSE           => 'Parsing Error',
                       E_NOTICE          => 'Notice',
                       E_CORE_ERROR      => 'Core Error',
                       E_CORE_WARNING    => 'Core Warning',
                       E_COMPILE_ERROR   => 'Compile Error',
                       E_COMPILE_WARNING => 'Compile Warning',
                       E_USER_ERROR      => 'Fatal Error',
                       E_USER_WARNING    => 'Warning',
                       E_USER_NOTICE     => 'Silent Notice',
                       E_STRICT          => 'PHP5 Strict Non-Compliance',
                       4096              => 'Fatal Error (4096)',
                      );

        $errorType = $errorTypes[$num];

        // backtrace info
        $backTrace = debug_backtrace();
        $backList = '';
        $totalBackTrace = count($backTrace);
        if ($totalBackTrace > 0)
        {
            $backList = '<em>Back Trace:</em><ul>';
            for ($i = 0; $i < $totalBackTrace; $i++)
            {
                $item = $backTrace[$i];
                if (stripos($item['function'], 'errorHandler') === false && stripos($item['function'], 'exceptionHandler') === false)
                {
                    $btLine = 'Unknown';
                    if (isset($item['line']))
                    {
                        $btLine = $item['line'];
                    }
                    $backList .= "<li>Line <strong>$btLine</strong> using {$item['function']}()";
                    if (isset($item['file']))
                    {
                        $item['file'] = str_replace('\\', '/', $item['file']);
                        $backList .= " in <strong>{$item['file']}</strong>";
                    }
                    $backList .= '</li>';
                }
            }
            $backList .= '</ul>';
        }

        // error string
        $date = date('m/d/Y - g:i A T');
        $currentUrl = '';
        if (isset($_SERVER) && isset($_SERVER['HTTP_HOST']))
        {
            $currentUrl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        }
        else {
            $currentUrl = "$errorType at line $line in file $file";
        }
        $errorString = "<span style=\"font-weight: bold;\">$errorType at line $line in file $file<br /><br /></span>$msg<br /><br />";
        if (isset($context['this']))
        {
            $className = get_class($context['this']);
            $parentClass = get_parent_class($context['this']);
            $errorString .= "<li>Object/Class: '$className', Parent Class: '$parentClass'</li>";
        }
        $errorString .= $backList . $date . ' - ' . $currentUrl . '<br />';
        $html = '<?xml version="1.0" encoding="iso-8859-1"?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
            <head>
                <title>500 Internal Server Error</title>
                <meta http-equiv="expires" content="0" />
                <meta http-equiv="Content-Type" content="text/xml; charset=iso-8859-1" />
                <style type="text/css">
                    table { font-family: "Arial", sans-serif; font-size: 10pt; }
                </style>
        </head>
        <body>
            ' . $errorString . '
        </body>
        </html>';
        if (ERR_KEEP_LOG === true)
        {
            if (is_writable(PATH_DATA . '/'))
            {
                if (file_exists(ERR_HTML_LOG) && is_writable(ERR_HTML_LOG))
                {
                    error_log($errorString . '<br /><hr /><br />', 3, ERR_HTML_LOG);
                }
                else if (!file_exists(ERR_HTML_LOG))
                {
                    error_log($errorString . '<br /><hr /><br />', 3, ERR_HTML_LOG);
                }
            }
        }
        if (ERR_SEND_EMAIL === true)
        {
            $headers = 'From: "Error Handling System" <no-reply@reflexsolutions.ws>' . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $boundary = uniqid('HTMLDEMO');
            $headers .= "Content-Type: multipart/alternative; boundary = $boundary\r\n\r\n";

            $msg = "--$boundary\r\n";
            $msg .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $msg .= "Content-Disposition: inline;\r\n";
            $msg .= "Content-Transfer-Encoding: base64\r\n\r\n";
            $msg .= chunk_split(base64_encode($errorString));
            $msg .= "\r\n\r\n";
            $msg .= "--$boundary--";
            @mail(ERR_EMAIL_ADDRESS, 'Error Report - ' . $currentUrl, $msg, $headers);
            echo "\n\nAn error has occurred.  Please check your email for details.\n\n";
            exit;
        }
        if ($num != E_USER_NOTICE)
        {
            if (!headers_sent())
            {
                header('Status: 500 Internal Server Error');
            }
            echo $html;
            exit;
        }
    }
    return;
}

/**
 * Default exception handler - if an exception is thrown but not caught, it will be sent to this function.
 * Do not call this function directly.
 *
 * @param object $ex an instance of the Exception class
 *
 * @return void
 */
function exceptionHandler($ex)
{
    $error_type = 'Exception';
    $line = $ex->getLine();
    $file = $ex->getFile();
    $msg = $ex->getMessage();

    // backtrace info
    $back_trace = $ex->getTrace();
    $back_list = '';
    $total_back_trace = count($back_trace);
    if ($total_back_trace > 0)
    {
        $back_list = '<em>Back Trace:</em><ul>';
        for ($i = 0; $i < $total_back_trace; $i++)
        {
            $item = $back_trace[$i];
            if (stripos($item['function'], 'error_handler') === false && stripos($item['function'], 'exception_handler') === false)
            {
                $bt_line = 'Unknown';
                if (isset($item['line']))
                {
                    $bt_line = $item['line'];
                }
                $back_list .= "<li>Line <strong>$bt_line</strong> using {$item['function']}()";
                if (isset($item['file']))
                {
                    $item['file'] = str_replace('\\', '/', $item['file']);
                    $back_list .= " in <strong>{$item['file']}</strong>";
                }
                $back_list .= '</li>';
            }
        }
        $back_list .= '</ul>';
    }

    // error string
    $date = date('m/d/Y - g:i A T');
    $current_url = '';
    if (isset($_SERVER) && isset($_SERVER['HTTP_HOST']))
    {
        $current_url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }
    else
    {
        $current_url = "$error_type at line $line in file $file";
    }
    $error_string = "<span style=\"font-weight: bold;\">$error_type at line $line in file $file<br /><br /></span>$msg<br /><br />";
    $error_string .= $back_list . $date . ' - ' . $current_url . '<br />';
    $html = '<?xml version="1.0" encoding="iso-8859-1"?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
        <head>
            <title>500 Internal Server Error</title>
            <meta http-equiv="expires" content="0" />
            <meta http-equiv="Content-Type" content="text/xml; charset=iso-8859-1" />
            <style type="text/css">
                table { font-family: "Arial", sans-serif; font-size: 10pt; }
            </style>
    </head>
    <body>
        <div style="color: red; font-weight: bold;">An error has occurred.<br /><br /></div>
        ' . $error_string . '
    </body>
    </html>';

    if (ERR_SEND_EMAIL === true)
    {
        $headers  = 'From: "Error Handling System" <no-reply@teladoc.com>' . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $boundary = uniqid(md5(rand(0,100)));
        $headers .= "Content-Type: multipart/alternative; boundary = $boundary\r\n\r\n";

        $msg  = "--$boundary\r\n";
        $msg .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $msg .= "Content-Disposition: inline;\r\n";
        $msg .= "Content-Transfer-Encoding: base64\r\n\r\n";
        $msg .= chunk_split(base64_encode($error_string));
        $msg .= "\r\n\r\n";
        $msg .= "--$boundary--";
        @mail(ERR_EMAIL_ADDRESS, 'Error Report - ' . $current_url, $msg, $headers);
    }
    if (!headers_sent())
    {
        header('Status: 500 Internal Server Error');
    }
    echo $html;
    exit;
}

// ##########
// Changing some settings
// ##########

error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('log_errors', true);
if (is_writable(PATH_DATA . '/'))
{
    if (file_exists(ERR_SYS_LOG) && is_writable(ERR_SYS_LOG))
    {
        ini_set('error_log', ERR_SYS_LOG);
    }
    else if (!file_exists(ERR_SYS_LOG))
    {
        ini_set('error_log', ERR_SYS_LOG);
    }
}
set_error_handler('errorHandler');
set_exception_handler('exceptionHandler');


?>
