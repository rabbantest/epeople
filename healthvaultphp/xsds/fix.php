<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

libxml_use_internal_errors(true);
$xsd = 'method-putthings.xsd';
$xml = file_get_contents('info.xml');

$xdoc = new DomDocument();
$xdoc->loadXML($xml);
if (!$xdoc->schemaValidate($xsd))
{
    $errors = libxml_get_errors();
    var_dump($errors); exit;
}
else
{
    echo 'Ok!';
}

?>
