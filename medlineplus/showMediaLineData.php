
<?php
/*
  Place code to connect to your DB here.
 */
include_once './config2.php';

//include('config2.php');	// include your code to connect to DB.

$tbl_name = "c10_description_table";  //your table name
// How many adjacent pages should be shown on each side?
$adjacents = 3;

/*
  First get total number of rows in data table.
  If you have a WHERE clause in your query, make sure you mirror it here.
 */
$query = "SELECT COUNT(*) as num FROM $tbl_name";
$total_pages = mysql_fetch_array(mysql_query($query));
$total_pages = $total_pages[num];

/* Setup vars for query. */
$targetpage = "showMediaLineData.php";  //your file name  (the name of this file)
$limit = 10;         //how many items to show per page
$page = $_GET['page'];
if ($page)
    $start = ($page - 1) * $limit;    //first item to display on this page
else
    $start = 0;        //if no page var is given, set start to 0

/* Get data. */
$sql = "SELECT * FROM $tbl_name LIMIT $start, $limit";
$result = mysql_query($sql);

/* Setup page vars for display. */
if ($page == 0)
    $page = 1;     //if no page var is given, default to 1.
$prev = $page - 1;       //previous page is page - 1
$next = $page + 1;       //next page is page + 1
$lastpage = ceil($total_pages / $limit);  //lastpage is = total pages / items per page, rounded up.
$lpm1 = $lastpage - 1;      //last page minus 1

/*
  Now we apply our rules and draw the pagination object.
  We're actually saving the code to a variable in case we want to draw it more than once.
 */
$pagination = "";
if ($lastpage > 1) {
    $pagination .= "<div class=\"pagination\">";
    //previous button
    if ($page > 1)
        $pagination.= "<a href=\"$targetpage?page=$prev\">previous</a>";
    else
        $pagination.= "<span class=\"disabled\"previous</span>";

    //pages	
    if ($lastpage < 7 + ($adjacents * 2)) { //not enough pages to bother breaking it up
        for ($counter = 1; $counter <= $lastpage; $counter++) {
            if ($counter == $page)
                $pagination.= "<span class=\"current\">$counter</span>";
            else
                $pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";
        }
    }
    elseif ($lastpage > 5 + ($adjacents * 2)) { //enough pages to hide some
        //close to beginning; only hide later pages
        if ($page < 1 + ($adjacents * 2)) {
            for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                if ($counter == $page)
                    $pagination.= "<span class=\"current\">$counter</span>";
                else
                    $pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";
            }
            $pagination.= "...";
            $pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
            $pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";
        }
        //in middle; hide some front and some back
        elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
            $pagination.= "<a href=\"$targetpage?page=1\">1</a>";
            $pagination.= "<a href=\"$targetpage?page=2\">2</a>";
            $pagination.= "...";
            for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                if ($counter == $page)
                    $pagination.= "<span class=\"current\">$counter</span>";
                else
                    $pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";
            }
            $pagination.= "...";
            $pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
            $pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";
        }
        //close to end; only hide early pages
        else {
            $pagination.= "<a href=\"$targetpage?page=1\">1</a>";
            $pagination.= "<a href=\"$targetpage?page=2\">2</a>";
            $pagination.= "...";
            for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                if ($counter == $page)
                    $pagination.= "<span class=\"current\">$counter</span>";
                else
                    $pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";
            }
        }
    }

    //next button
    if ($page < $counter - 1)
        $pagination.= "<a href=\"$targetpage?page=$next\">next</a>";
    else
        $pagination.= "<span class=\"disabled\">next</span>";
    $pagination.= "</div>\n";
}
?>
<html>
    <head>
        <title>Mhealth Analyzer </title>
        <link href="css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
 <a href="http://clinicaltrials.gov" title="Information on Clinical Trials and Human Research Studies">ClinicalTrials.gov</a> provides patients, their family members, and the public with easy and free access to information on clinical studies for a wide range of diseases and conditions.
<?php
$fform = "<table class='main-Table'>
    
    ";

while ($row = mysql_fetch_array($result)) {
    //print_r($row);exit;
    $fform.="<tr><td>Title</td><td>$row[title]</td></tr>";
    $fform.="<tr><td>Code</td><td>$row[uniq_id]</td></tr>";
    $fform.="<tr><td>Summary</td><td>$row[summary]</td></tr>";
    $fform.="<tr><td>Author Name</td><td>$row[author_name]</td></tr>";
    $fform.="<tr><td>Author Url</td><td><a href='$row[author_uri]'>$row[author_uri]</a></td></tr>";
    $fform.="<tr><td>Updated Date</td><td>$row[updated]</td></tr>";
    $fform.="<tr style='background:red; height:25px;'><td colspan='2'></td></tr>";
}
$fform.="</tr>
            </table>";
echo $fform;
?>

        <?= $pagination ?>
    </body>
</html>