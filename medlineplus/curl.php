<?php

function curlUsingPost($url, $data) {

    if (empty($url) OR empty($data)) {
        return 'Error: invalid Url or Data';
    }


    //url-ify the data for the POST
    $fields_string = '';
    foreach ($data as $key => $value) {
        $fields_string .= $key . '=' . $value . '&';
    }
    $fields_string = rtrim($fields_string, '&');
    
//   $mypath = getcwd();
//            $mypath = preg_replace('/\\\\/', '/', $mypath);
//            $rand = rand(1, 15000);
//           $cookie_file_path = "$mypath/cookies/cookie$rand.txt";

    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($data));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); # timeout after 10 seconds, you can increase it
    //curl_setopt($ch,CURLOPT_HEADER,false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  # Set curl to return the data instead of printing it to the browser.
     curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);   // Cookie management.
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file_path);
    curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);

    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)"); # Some server may refuse your request if you dont pass user agent

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


    //execute post
    $result = curl_exec($ch);

    //close connection
    curl_close($ch);
    return $result;
}


 function curlUsingGet($url, $data)
    {

    if(empty($url) OR empty($data))
    {
    return 'Error: invalid Url or Data';
    }

    //url-ify the data for the get  : Actually create datastring
    $fields_string = '';

    foreach($data as $key=> $value){
    $fields_string[]=$key.'='.urlencode($value).'&amp;'; }
    $urlStringData = $url.'?'.implode('&amp;',$fields_string);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,10); # timeout after 10 seconds, you can increase it
   curl_setopt($ch, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($ch, CURLOPT_URL, $urlStringData ); #set the url and get string together

    $return = curl_exec($ch);
    curl_close($ch);

    return $return;
}

function getCurldata($url) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
    curl_setopt($ch, CURLOPT_URL, $url);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}