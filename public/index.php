<?php

/* * ***********************
 * PAGE: USE TO MANAGE THE BASIC CONFIGRATION FOR MHEALTH.
 * #COPYRIGHT: APPSTUDIOZ
 * @Rabban Ahmad
 * CREATOR: 12/11/2014.
 * UPDATED: --/--/----.
 * Zend Framework (http://framework.zend.com/)
 *
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * *** */

//error_reporting(E_ALL);
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

define("SITE_NAME", "QuantextualHealth");
define('MongoConnect', 'localhost:27017');
defined('BASE_PATH') || define('BASE_PATH', realpath(dirname(__FILE__) . '/../public'));
define('INVITESUBJECT', SITE_NAME . 'Invitation To Join health-ePeople?');
define('INVITEMSG', 'Hi,<br> You are invited to join health-ePeople<br>'
        . '<a href="' . $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . '/individual">Click Here To Join</a>');

define("ADMIN_DOMAIN_ID", "1"); // Setup autoloading
define("Currency", " USD"); // 
define('Host', "http://health-epeople.com/");
define('SITE_URL', $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST']);
//define("TANGOCARD_PLATEFORM", "quantextualhealth"); // 
//define("TANGOCARD_KEY", "2IHFgTNqFKfSLZvjNA9md2gkyELen1QuaFHR6M0S19jWgM2taH5Vtxdc");
//define("TANGOCARD_URL", "https://api.tangocard.com/raas/v1/");

define("EMAIL_TEMPLATE", "<html><body><img src='http://my.health-epeople.com/img/mail_header_image.jpeg' alt='Company Logo'><br> <br>"
        . "<br><br>{message}"
        . "<br><br> <b>Thanks</b> <br> <b>" . SITE_NAME . "</b></body></html>"); // 


define("TANGOCARD_PLATEFORM", "TangoTest"); // 
define("TANGOCARD_KEY", "5xItr3dMDlEWAa9S4s7vYh7kQ01d5SFePPUoZZiK/vMfbo3A5BvJLAmD4tI=");
define("TANGOCARD_URL", "https://sandbox.tangocard.com/raas/v1/");
define("VALIDIC_ORGANIZATION_ID", "55c0cb3f6362310018360000");
define("VALIDIC_ACCESS_TOKEN", "5520b18f020d85b3ded445f8c6c821165e80ce273c8da3e550f95dd67e981d43");

define("POINTAMOUNT", 1);


require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();

