//layout JS For left and top section

//Equal height column function
function equalHeight(group) {
 tallest = 0;
 group.each(function() {
  thisHeight = $(this).height();
  if(thisHeight > tallest) {
   tallest = thisHeight;
  }
 });
 group.height(tallest);
}

$(function(){
  //$('.left_menu_section').mouseenter(function(){
    //$('body').addClass('fixedBody');  
  //});
  //$('.left_menu_section').mouseleave(function(){
      //$('body').removeClass('fixedBody');  
  //});
  $('ul.menuWrap .ui-tab .anchor').click(function(){
  var nextul = $(this).parents('li.ui-tab').find('.submenu');
  $('li.ui-tab').removeClass('active');
  //$('.submenu').slideUp();
  $(this).parents('li.ui-tab').addClass('active');
  if(nextul.css('display')=='none'){
   $('.submenu').slideUp();
   nextul.slideDown();
  }else{
   nextul.slideUp();
   $('li.ui-tab').removeClass('active');
  }
  });
 
  $('.history_symtom .symtoms_list li.syms_item').click(function(){
	  var nextul = $(this).find('.desc_paragraph');
	  $('li.syms_item').removeClass('active');
	  $('.desc_paragraph').slideUp();
	  $(this).addClass('active');
	  if(nextul.css('display')=='none'){
	   $('.desc_paragraph').slideUp();
	   nextul.slideDown();
	  }else{
	   nextul.slideUp();
	   $('li.syms_item').removeClass('active');
	  }
	});
  $('.history_symtom .symtoms_list li.syms_item:first-child').addClass('active');
 
  $('.header .uinfo_ddr').mouseup(function(uinfo){
    $(this).parents('.userInfo').stop(true,true).toggleClass('active');
    $('.header .uiddr_menu').stop(true,true).slideToggle('slow');
    $('.header .notification_popup').stop(true,true).slideUp('slow');
    return false;
   });
   /*$('#head .settingPopup').mouseup(function(){
    return false;
   });*/
   $(this).mouseup(function(uinfo){
    if(!($(uinfo.target).parent('.header .uinfo_ddr').length > 0)){
     $('.header .userInfo').stop(true,true).removeClass('active');
     $('.header .uiddr_menu').stop(true,true).slideUp('slow');
    }
   });

  $('.header .notification').mouseup(function(uinfo){
    $(this).toggleClass('active');
    $('.header .notification_popup').stop(true,true).slideToggle('slow');
     $('.header .uiddr_menu').stop(true,true).slideUp('slow');
    return false;
   });
   /*$('#head .settingPopup').mouseup(function(){
    return false;
   });*/
   $(this).mouseup(function(uinfo){
    if(!($(uinfo.target).parent('.header .notification').length > 0)){
     $('.header .notification').stop(true,true).removeClass('active');
     $('.header .notification_popup').stop(true,true).slideUp('slow');
    }
   });

   
  $('.left_panel_menu_button').click(function(){
    $('html').toggleClass('slide_pannelOpen');
   });
	  
   $('.left_menu_section .overlay').hover(function(){
    $('html').removeClass('slide_pannelOpen');
   });
	  
	  
	$('.formContainer .row .input input,.formContainer .row select,.srchUser .su_col .input input,.srchUser .su_col .input select').focus(function(){
		$(this).parents('.input').addClass('focus');	
	});
	$('.formContainer .row .input input,.formContainer .row select,.srchUser .su_col .input input,.srchUser .su_col .input select').blur(function(){
		$(this).parents('.input').removeClass('focus');	
	});
	
	
	$('.checkbox').click(function(){
		
		if($(this).hasClass('checkbxNo')){
			return false;
		}
		
		$(this).toggleClass('checked');
			if($(this).hasClass('checked')) {
			  $(this).find('input:checkbox').prop("checked", true);
		  	}
		  else {
			$(this).find('input:checkbox').prop("checked", false);
		  }
	});

	 $('.radioButtonWrap .radio').click(function(){
		 var radioBtnParent = $(this).parents('.row');
		 radioBtnParent.find('.radio').removeClass('selected');
		 $(this).addClass('selected');
		 radioBtnParent.find('input').prop('checked',false);
		 $(this).find('input').prop('checked',true);
	 });

	 $('.radioWrapperNew .radio').click(function(){
		 var radioBtnParent = $(this).parents('.radioWrapper');
		 radioBtnParent.find('.radio').removeClass('selected');
		 $(this).addClass('selected');
		 radioBtnParent.find('input').prop('checked',false);
		 $(this).find('input').prop('checked',true);
	 });

	$('.formContainer .row .input input.individual_userage').blur(function(){
		var Ageval = $('.input input.individual_userage').val();
		if(Ageval!=="" && Ageval<16){
			gppOpen('age_confirm');
		}
	});
	custom_selectbox();
});

//end

//Login Page Window height
$(document).ready(function(){
	var footerHeight = $('#footer').outerHeight(true);
	$('#ui-content-wrapper').css({'minHeight':($(window).height()-($('.main_header').innerHeight())-footerHeight)});
	$('#content-wrapper').css({'minHeight':($(window).height()-($('.main_header .header').innerHeight())-footerHeight)});
	$('.pghFixed').css({'width':($(window).width()-$('.left_menu_section').width())});
	
	
	if($('.pghFixed').length==1){
		$('#content-wrapper').css({'minHeight':($(window).height()-($('.main_header .header').innerHeight())-footerHeight - $('.pghFixed').height())});
		$('#content-wrapper').css({'marginTop':($('.main_header .header').innerHeight()+$('.pghFixed').height())+'px'});
	}
	
});

$(window).resize(function(){
	var footerHeight = $('#footer').outerHeight( true );
	$('#ui-content-wrapper').css({'minHeight':($(window).height()-($('.main_header').innerHeight())-footerHeight)});
	$('#content-wrapper').css({'minHeight':($(window).height()-($('.main_header .header').innerHeight())-footerHeight)});
	$('.pghFixed').css({'width':($(window).width()-$('.left_menu_section').width())});
	
	
	if($('.pghFixed').length==1){
		$('#content-wrapper').css({'minHeight':($(window).height()-($('.main_header .header').innerHeight())-footerHeight - $('.pghFixed').height())});
		$('#content-wrapper').css({'marginTop':($('.main_header .header').innerHeight()+$('.pghFixed').height())+'px'});
	}
	
});

//Custom Select Box
function custom_selectbox(){
	  $('select.select').each(function(){
	  if($(this).next('span.select').length=='0'){
	  
	  var title = $(this).attr('title');
	  if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
	  $(this)
		  .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
		  .after('<span class="select">' + title + '</span>')
		  .change(function(){
			  val = $('option:selected',this).text();
			  $(this).next().text(val);
		});
	  }
	  });
	  
	  $('select.select option').change(function() {
		 var ClickSelect = $(this).text();
			 if(ClickSelect.length > 100) 
			  {
				  ClickSelect = ClickSelect.substring(0,100);
				  $(this).parents('select.select').next('span.select').html(ClickSelect+'...');
			  }else{
				  $(this).parents('select.select').next('span.select').html(ClickSelect);
			  }
	  });
   $("select.select").bind('keypress', function(e)
  {
	var strSelectKp = $(this).find('option:selected').text();
	if(e.keyCode == 40 || e.keyCode == 38)
	{
	if(strSelectKp.length > 100) 
	  {
	  strSelectKp = strSelectKp.substring(0,100);
	  $(this).next('span.select').html(strSelectKp+'...');
	  }else{
	   $(this).next('span.select').html(strSelectKp);
	  }
	}
   });	
}

function custom_selectboxNew(){
	  $('#ajaxstate select.select').each(function(){
	  var title = $(this).attr('title');
	  if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
	  $(this)
		  .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
		  .after('<span class="select">' + title + '</span>')
		  .change(function(){
			  val = $('option:selected',this).text();
			  $(this).next().text(val);
		})
	  });
	  $('#ajaxstate select.select option').change(function() {
		 var ClickSelect = $(this).text();
			 if(ClickSelect.length > 100) 
			  {
				  ClickSelect = ClickSelect.substring(0,100);
				  $(this).parents('select.select').next('span.select').html(ClickSelect+'...');
			  }else{
				  $(this).parents('select.select').next('span.select').html(ClickSelect);
			  }
	  });
   $("#ajaxstate select.select").bind('keypress', function(e)
  {
	var strSelectKp = $(this).find('option:selected').text();
	if(e.keyCode == 40 || e.keyCode == 38)
	{
	if(strSelectKp.length > 100) 
	  {
	  strSelectKp = strSelectKp.substring(0,100);
	  $(this).next('span.select').html(strSelectKp+'...');
	  }else{
	   $(this).next('span.select').html(strSelectKp);
	  }
	}
   });	
}
function custom_selectboxcounty(){
	  $('#ajaxcounty select.select').each(function(){
	  
	  var title = $(this).attr('title');
	  if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
	  $(this)
		  .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
		  .after('<span class="select">' + title + '</span>')
		  .change(function(){
			  val = $('option:selected',this).text();
			  $(this).next().text(val);
		})
	  });
	  $('#ajaxcounty select.select option').change(function() {
		 var ClickSelect = $(this).text();
			 if(ClickSelect.length > 100) 
			  {
				  ClickSelect = ClickSelect.substring(0,100);
				  $(this).parents('select.select').next('span.select').html(ClickSelect+'...');
			  }else{
				  $(this).parents('select.select').next('span.select').html(ClickSelect);
			  }
	  });
   $("#ajaxcounty select.select").bind('keypress', function(e)
  {
	var strSelectKp = $(this).find('option:selected').text();
	if(e.keyCode == 40 || e.keyCode == 38)
	{
	if(strSelectKp.length > 100) 
	  {
	  strSelectKp = strSelectKp.substring(0,100);
	  $(this).next('span.select').html(strSelectKp+'...');
	  }else{
	   $(this).next('span.select').html(strSelectKp);
	  }
	}
   });	
}
function custom_selectboxEthnicity(){
	  $('#ajaxethnicity select.select').each(function(){
	  
	  var title = $(this).attr('title');
	  if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
	  $(this)
		  .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
		  .after('<span class="select">' + title + '</span>')
		  .change(function(){
			  val = $('option:selected',this).text();
			  $(this).next().text(val);
		})
	  });
	  $('#ajaxethnicity select.select option').change(function() {
		 var ClickSelect = $(this).text();
			 if(ClickSelect.length > 100) 
			  {
				  ClickSelect = ClickSelect.substring(0,100);
				  $(this).parents('select.select').next('span.select').html(ClickSelect+'...');
			  }else{
				  $(this).parents('select.select').next('span.select').html(ClickSelect);
			  }
	  });
   $("#ajaxethnicity select.select").bind('keypress', function(e)
  {
	var strSelectKp = $(this).find('option:selected').text();
	if(e.keyCode == 40 || e.keyCode == 38)
	{
	if(strSelectKp.length > 100) 
	  {
	  strSelectKp = strSelectKp.substring(0,100);
	  $(this).next('span.select').html(strSelectKp+'...');
	  }else{
	   $(this).next('span.select').html(strSelectKp);
	  }
	}
   });	
}

//Custom Select Box end

function readURL(input) {
if (input.files && input.files[0]) {
		/*code for validate image */
		var mim = document.getElementById('UploadProfilePic').files[0].type;
		if($.inArray(mim, ['image/gif','image/png','image/jpg','image/jpeg']) == -1) {
				$(".err").css("display", "block");
				/* setTimeout(function(){
					location.reload();
				}, 2000); */
				
		}else{
				var file_size = $('#UploadProfilePic')[0].files[0].size;
                                
				if(file_size>2097152) {
					$("#file_error").html("<font color='red'>File size is greater than 2MB</font>");
                                        $('#UploadProfilePic').val('');
					return false;
				}else{
				var reader = new FileReader();
				reader.onload = function (e) {
					$('.uploadProfilePic_img').attr('src', e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
				$(".err").css("display", "none");
				}
		}
		/* end */
		
	}
}


function gppResize(){
	$('.grobalPopup').each(function(){
	   $(this).find('.gppbxContainer').css({'marginTop':'-'+($(this).find('.gppbxContainer').height()/2)+'px'},100,'linear');
       var gppDiv = $(this).find('.gppbxContainer');
	   var gppHeight = gppDiv.height();
	   if(gppHeight >= ($(window).height())){
		   $(this).addClass('relative');
		   $('html').css({'overflow-y':'hidden'});
	   }else{
		   $(this).removeClass('relative');
		   $('html').css({'overflow-y':'scroll'});
	   }
    });
}
$(function(){
	gppResize();
	$(window).resize(function(){
		gppResize();
		$('.pgh_mWrap').css({'width':$(window).width()-$('.left_menu_section').width()-95});
		if($(window).width()<940){
			$('.pgh_mWrap').css({'width':$(window).width()-95});	
		}
	});
});
function gppOpen(e){
	$('.popuBxWrap').fadeOut('fast');
        $('body').addClass('fixedBody');
	$('#'+e).fadeIn('fast');
	$('#'+e).find('.gppbxContainer_pis').css({'height':($(window).height()-50)});
	$('#'+e).find('.gppbxContainer').css({'marginTop':'-'+($('#'+e).find('.gppbxContainer').height()/2)+'px'},100,'linear');
	gppResize();
}
function gppClose(e){	
    $('body').removeClass('fixedBody');
	$('#'+e).fadeOut('fast');
	$('#'+e).find('.successMsg,.errorMsg').remove();
	$('html').css({'overflow-y':'scroll'});
}
$(function(){
	$('.pgh_mWrap').css({'width':$(window).width()-$('.left_menu_section').width()-95});
	if($(window).width()<940){
		$('.pgh_mWrap').css({'width':$(window).width()-95});	
	}
	var CarosalLen = $(".pgh_menu").length;
	if(CarosalLen>0){
		$(".pgh_menu").owlCarousel({
			items : 5,
			itemsDesktop : [1199, 3],
			itemsDesktopSmall : [979, 3],
			itemsTablet : [768, 2],
        	itemsTabletSmall : [640, 2],
        	itemsMobile : [479, 2],
			autoPlay    : false,
			stopOnHover : false,
			navigation  : true,
			pagination  : false,
			autoHeight  : true,
			transitionStyle:"slide",
			URLhashListener:true,
			startPosition: 'URLHash'
		});
	}
	var subplanListLen = $(".subplanList").length;
	if(subplanListLen>0){
		$(".subplanList").owlCarousel({
			items : 3,
			itemsDesktop : [1299, 2],
			itemsDesktopSmall : [979, 2],
			itemsTablet : [768, 2],
        	itemsTabletSmall : [640, 1],
        	itemsMobile : [479, 1],
			autoPlay    : false,
			stopOnHover : false,
			navigation  : true,
			pagination  : true,
			autoHeight  : true,
			transitionStyle:"slide",
		});
	}
});



$(function(){
	if($('.sc_leftpanel .addSymptoms').length!=''){
		$('.scl_menu').css({'maxHeight':($('.sc_rightpanel').outerHeight() - $('.scl_srch').outerHeight() - $('.sc_leftpanel .addSymptoms').outerHeight()) +'px'});
	}else{
		$('.scl_menu').css({'maxHeight':($('.sc_rightpanel').outerHeight() - $('.scl_srch').outerHeight()) +'px'});
	}
	$(window).resize(function(e) {
    if($('.sc_leftpanel .addSymptoms').length!=''){
		$('.scl_menu').css({'maxHeight':($('.sc_rightpanel').outerHeight() - $('.scl_srch').outerHeight() - $('.sc_leftpanel .addSymptoms').outerHeight()) +'px'});
	}else{
		$('.scl_menu').css({'maxHeight':($('.sc_rightpanel').outerHeight() - $('.scl_srch').outerHeight()) +'px'});
	}
    });
    var ActivePositionLen = $('.left_menu_section ul.menuWrap .ui-tab.active').length;
	if(ActivePositionLen>0){
            var ActivePosition = $('.left_menu_section ul.menuWrap .ui-tab.active').position().top;
            $('.left_menu_section ul.menuWrap').scrollTop(ActivePosition);
    }
});

$(window).load(function(e){
	$('.pgh_menu').each(function(index, element) {
        owl = $(this).data('owlCarousel');
		var jump = parseInt($(this).find('a.item.selected').parents('.owl-item').index());
		owl.jumpTo(jump);  
		//alert(jump);
    }); 
});

function setEqualHeight(columns) {
	var tallestcolumn = 0;
	columns.each(
	function() {
		currentHeight = $(this).height();
		if(currentHeight > tallestcolumn) {
			tallestcolumn  = currentHeight;
			}
		}
	);
	columns.height(tallestcolumn);
	}
	var delay = (function(){
		var timer = 0;
		return function(callback, ms){
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
	};
})();