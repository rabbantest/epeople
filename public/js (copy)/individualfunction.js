$(function () {
    $('.number_dot').keypress(function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which > 57)) {
            event.preventDefault();
        }
    });
    /* Start code for number validation */
    $(".number_validate, .onlynumber").bind("keydown", function (event) {

        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                // Allow: Ctrl+A

                        (event.keyCode == 65 && event.ctrlKey === true) ||
                        // Allow: home, end, left, right

                                (event.keyCode >= 35 && event.keyCode <= 39)) {

                    // let it happen, don't do anything

                    return;

                } else {

                    // Ensure that it is a number and stop the keypress

                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                        event.preventDefault();

                    }

                }

            });
    /* end code for number validation */

    /* Start code for number and dot (.) validation */
    $(".dot_number_validate, .number").bind("keydown", function (event) {

        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                // Allow: Ctrl+A

                        (event.keyCode == 65 && event.ctrlKey === true) ||
                        // Allow: home, end, left, right

                                (event.keyCode >= 35 && event.keyCode <= 39)) {

                    // let it happen, don't do anything

                    return;

                } else {

                    // Ensure that it is a number and stop the keypress

                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                        event.preventDefault();

                    }

                }

            });

    /*
     * Filter form submit for Individual Body measurment
     */

    $('#filter_select').change(function () {
        var filterval = $(this).val().trim();
        if (filterval != '') {
            $("#filter_form").submit();
        }
    });
    $('.nospecial_char').keyup(function ()
    {
        var yourInput = $(this).val();
        re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
        var isSplChar = re.test(yourInput);
        if (isSplChar)
        {
            var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
            $(this).val(no_spl_char);
        }
    });
    
     $('.nohtml_char').keyup(function ()
    {
        var yourInput = $(this).val();
        re = /<(.|\n)*?>/g;
        var isSplChar = re.test(yourInput);
        if (isSplChar)
        {
            var no_spl_char = yourInput.replace(/<(.|\n)*?>/g, '');
            $(this).val(no_spl_char);
        }
    });


});
 function disableHtml(element) {
  element.value = element.value.replace(/[<>]/g, '');
}


function redirect(url)
{
    window.location(url);
}

