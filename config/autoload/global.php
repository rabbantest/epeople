<?php
/*************************
    * PAGE: USE TO MANAGE THE GLOBAL CONFIGURATION OVERRIDE
    * #COPYRIGHT: APPSTUDIOZ
    * @AUTHOR: Rabban Ahmad
    * CREATOR: 13/11/2014.
    * UPDATED: --/--/----.
    *
    * @license   http://framework.zend.com/license/new-bsd New BSD License
*****/

return array(
/******For local server Database *******/
   'db' => array(
	    'driver'         => 'Pdo',
	    'dsn'            => 'mysql:dbname=health_ematrix;host=localhost',
	    'username' => 'root',
	    'password' => 'appstudioz',
	    'driver_options' => array(
	        'PDO::MYSQL_ATTR_INIT_COMMAND' => 'SET NAMES \'UTF8\''
	    ),
	), 
	/******For live server Database *******/
	/* 
	'db' => array(
       'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=health_metrix;host=healtmetrix.cw0divbfaenz.us-east-1.rds.amazonaws.com',
        'username' => 'healthmetrix',
        'password' => 'health12',
        'driver_options' => array(
            'PDO::MYSQL_ATTR_INIT_COMMAND' => 'SET NAMES \'UTF8\''
        ),

    ),
	*/
    /******For live development server Database *******/
	/*
	'db' => array(
       'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=health_metrix;host=mhealthdev.cw0divbfaenz.us-east-1.rds.amazonaws.com',
        'username' => 'healthmetrix',
        'password' => 'health1234',
        'driver_options' => array(
            'PDO::MYSQL_ATTR_INIT_COMMAND' => 'SET NAMES \'UTF8\''
        ),

    ),*/

    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
        'module_layouts' => array(
       'Individual' => 'layout/layout.phtml',
       'Caregiver' => 'layout/caregiverlayout.phtml',
       'Nonmember' => 'layout/nonmemberlayout.phtml',
       'Researcher' => 'layout/researcherlayout.phtml',
       'Admin' => 'layout/layout.phtml',
   ),
);
